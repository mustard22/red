//
//  FECircleParam.swift
//  Red
//
//  Created by MAC on 2020/4/2.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

public struct FECircleListParam: Encodable {
    // 1all 2friend
    var status = "1"
    var pageIndex = "1"
    var pageSize = "15"
    
    // 进评论详情后 调接口传该参数 用于刷新新评论数
    var topic_id = ""
}


public struct FECirclePublishParam: Encodable {
    /// 1所有人 2朋友 3仅自己
    var status = "1"
    var content = ""
    var pics = ""
    // 0是发布动态 1是邀请入群(>0)
    var share_group_id = "0"
}

public struct FECricleUpdateTopicStatusParam: Encodable {
    var topic_id = ""
    var status = ""
}


public struct  FECircleCommentListParam: Encodable {
    var topic_id = ""
    var parent_id = "0"
    /// 0第一层 1第二层
    var type = "0"
    var pageIndex = "1"
}


/// 写评论参数
public struct  FECircleCommentTopicParam: Encodable {
    var topic_id = ""
    var comment_id = ""
    /// 0第一层 1第二层
    var type = "0"
    var content = ""
    
    mutating func reset() {
        self.topic_id = ""
        self.comment_id = ""
        self.type = ""
        self.content = ""
    }
}
