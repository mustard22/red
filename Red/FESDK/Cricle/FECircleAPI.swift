//
//  FECircleAPI.swift
//  Red
//
//  Created by MAC on 2020/4/2.
//  Copyright © 2020 MAC. All rights reserved.
//

import Alamofire

enum FECircleAPI {
    /// 获取圈子列表
    case getRingsList(FECircleListParam)
    
    /// 更新点赞（topic_id）
    case updateClickNum(String)
    
    /// 发布动态
    case publicRings(FECirclePublishParam)
    
    /// 更新动态的状态
    case updateTopicStatus(FECricleUpdateTopicStatusParam)
    
    /// 获取评论列表
    case getCommentList(FECircleCommentListParam)
    
    /// 评论
    case commentTopic(FECircleCommentTopicParam)
    
    /// 删除评论
    case updateCommentStatus(String)
    
    /// 上传图片
    case uploadRingsPics
}

extension FECircleAPI: APITarget {
    static var group: String = "apirings/"
    
    var methodPath: MethodPath {
        switch self {
        case .getRingsList:
            return (.post, "getRingsList")
        case .updateClickNum:
            return (.post, "updateClickNum")
        case .publicRings:
            return (.post, "publicRings")
        case .updateTopicStatus:
            return (.post, "updateTopicStatus")
        case .getCommentList:
            return (.post, "getCommentList")
        case .commentTopic:
            return (.post, "commentTopic")
        case .updateCommentStatus:
            return (.post, "updateCommentStatus")
        case .uploadRingsPics:
            return (.post, "uploadRingsPics")
        }
    }
    var parameters: Parameters? {
        switch self {
        case .getRingsList(let param):
            return param.dictionary
        case .updateClickNum(let p):
            return ["topic_id": p]
        case .publicRings(let p):
            return p.dictionary
        case .updateTopicStatus(let p):
            return p.dictionary
        case .getCommentList(let p):
            return p.dictionary
        case .commentTopic(let p):
            return p.dictionary
        case .updateCommentStatus(let p):
            return ["comment_id": p]
        case .uploadRingsPics:
            return nil
        }
    }
}



