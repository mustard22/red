//
//  HttpClient[Circle].swift
//  Red
//
//  Created by MAC on 2020/4/2.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation
public extension HttpClient {
    func circleList(param: FECircleListParam, handler: @escaping (_ data: FECircleListData?, _ msg: String)->Void) {
        HttpClient.default.request(FECircleAPI.getRingsList(param)).responseObject(to: FECircleListData.self){(flag, messge, item) in
            handler(item, messge.text)
        }
    }
    
    func circleUpdateLikeNumber(topicId: String, handler: @escaping (_ isLiked: Bool?, _ msg: String)->Void) {
        HttpClient.default.request(FECircleAPI.updateClickNum(topicId)).responseObject(to: [String: String].self){(flag, messge, value) in
            if flag, let v = value, let like = v["result"] {
                if like == "success" {
                    handler(true, messge.text)
                } else {
                    handler(false, messge.text)
                }
            } else {
                handler(nil, messge.text)
            }
        }
    }
    
    func circlePublish(param: FECirclePublishParam, handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        HttpClient.default.request(FECircleAPI.publicRings(param)).responseObject(to: String.self){(flag, messge, _) in
            handler(flag, messge.text)
        }
    }
    
    func circleEditPublishStatus(param: FECricleUpdateTopicStatusParam, handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        HttpClient.default.request(FECircleAPI.updateTopicStatus(param)).responseObject(to: String.self){(flag, messge, _) in
            handler(flag, messge.text)
        }
    }
    
    func circleGetCommentList(param: FECircleCommentListParam, handler: @escaping (_ data: FECircleCommentListData?, _ msg: String)->Void) {
        HttpClient.default.request(FECircleAPI.getCommentList(param)).responseObject(to: FECircleCommentListData.self){(flag, messge, data) in
            
            handler(data, messge.text)
        }
    }
    
    //MARK: 写评论
    func circleCommentTopic(param: FECircleCommentTopicParam, handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        HttpClient.default.request(FECircleAPI.commentTopic(param)).responseObject(to: String.self){(flag, messge, _) in
            handler(flag, messge.text)
        }
    }
    
    //MARK: del评论
    func circleDelteComment(comment_id: String, handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        HttpClient.default.request(FECircleAPI.updateCommentStatus(comment_id)).responseObject(to: String.self){(flag, messge, _) in
            handler(flag, messge.text)
        }
    }
    
    //MARK: 上传图片
    func circleUploadFile(_ data: Data?, _ completionHandler: ((_ item: UploadAvatar?)->Void)?) {
        guard let data = data else {
            completionHandler?(nil)
            return
        }
        
        HttpClient.default.upload(FECircleAPI.uploadRingsPics, data){(isSuccess, msg, item) in
            completionHandler?(item)
        }
    }
}
