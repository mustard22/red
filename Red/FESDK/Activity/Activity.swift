//
//  Activity.swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

public struct Activity: Codable {
    public var id: Int = 0
    public var title: String = ""
    public var vice_title: String = ""
    public var type: Int = 0
    public var url: String = ""
    public var condition_id: String = ""
    public var act_img: String = ""
    public var start_time: String = ""
    public var end_time: String = ""
}

