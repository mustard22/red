//
//  ActivityAPI.swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire

enum ActivityAPI {
    case list
}

extension ActivityAPI: APITarget {
    static var group: String = "apisite/"
    
    var parameters: Parameters? {
        switch self {
        case .list:
            return nil
        }
    }
    
    var methodPath: MethodPath {
        switch self {
        case .list:
            return (.get,"activity")
            
        }
    }
    
}
