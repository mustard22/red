//
//  HttpClient[Activity].swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

public extension HttpClient {
    func getActivityList(completionHandler:YYRequestCompletionHandler<[Activity]>? = nil) {
        if let list = self.getObjectCache(type: [Activity].self, forKey: "activity_list") {
            completionHandler?(true,getCacheSuccessMessage,list)
        }
        
        
        HttpClient.default.request(ActivityAPI.list).responseObject(to: [Activity].self){(isSuccess,messge,activityList) in
            
            if let activityList = activityList {
                HttpClient.default.saveObjectCache(object: activityList, forKey: "activity_list")
            }
            completionHandler?(isSuccess,messge.text,activityList.or([]))
        }
    }
}
