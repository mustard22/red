//
//  User[Info]API.swift
//  Red
//
//  Created by MAC on 2019/8/23.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire

enum UserInfoAPI {
    case getInfo // 获取用户信息
    case getAvatar // 获取用户头像接口(暂时没用到)
    case bindMobile(BindPhoneParam) // 绑定手机号
    case changeMobileNumber(ChangePhoneParam) // 修改手机号
    case sendEmail(SendEmailParam) // 发送邮箱验证码
    case changeEmail(ChangeEmailParam) // 更换邮箱
    case changePassword(ChangeLoginPasswdParam) // 修改登录密码
    case checkFundPassword(params: CheckFundPasswdParam) // 检测资金密码
    case setFundPassword(params: SetFundPasswdParam) // 设置资金密码
    case changeFundPassword(params: ChangeFundPasswdParam) // 重置资金密码
    case setUserinfo(params: UpdateInfoParam) // 更新用户信息
    case packetlist(parameters: SendPackageParam) //个人发包记录
    case fetchedList(GetPackageParam) // 个人抢包记录
    case bindCard(params: BindCardParam) // 绑定银行卡
    case cardList // 获取银行卡列表
    case getProvinceCity // 获取省份城市数据
    case childBrokerageList(ProxyReportListParam) // 代理报表
    case levelList // 获取等级列表（代理列表界面）
    case childList(ProxyChildParam) // 获取直属下级（代理列表界面）
    case accountChangeType // 获取零钱类型
    case accountChange(MoneyListParam) // 零钱列表
    
}

extension UserInfoAPI:APITarget {
    static var group: String = "apiplayer/"
    
    var methodPath: MethodPath {
        switch self {
        case .getInfo:
            return (.get, "getUserInfo")
        case .getAvatar:
            return (.get, "getAvatar")
        case .bindMobile:
            return (.post, "bindMobile")
        case .changeMobileNumber:
            return (.post, "changeMobileNumber")
        case .sendEmail:
            return (.post, "sendEmail")
        case .changeEmail:
            return (.post, "changeEmail")
        case .changePassword:
            return (.post, "changePassword")
        case .checkFundPassword:
            return (.post, "checkFundPassword")
        case .setFundPassword:
            return (.post, "setFundPassword")
        case .changeFundPassword:
            return (.post, "changeFundPassword")
        case .setUserinfo:
            return (.post, "setUserinfo")
        case .packetlist:
            return (.post, "packetList")
        case .fetchedList:
            return (.post, "fetchedList")
        case .bindCard:
            return (.post, "bindCard")
        case .cardList:
            return (.get, "cardList")
        case .getProvinceCity:
            return (.get, "getProvinceCity")
        case .levelList:
            return (.get, "levelList")
        case .childList:
            return (.post, "childList")
        case .accountChangeType:
            return (.get, "accountChangeType")
        case .accountChange:
            return (.post, "accountChange")
        case .childBrokerageList:
            return (.post, "childBrokerageList")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getInfo:
            return nil
        case .getAvatar:
            return nil
        case .bindMobile(let params):
            return params.dictionary
        case .changeMobileNumber(let params):
            return params.dictionary
        case .sendEmail(let params):
            return params.dictionary
        case .changeEmail(let params):
            return params.dictionary
        case .changePassword(let params):
            return params.dictionary
        case .checkFundPassword(let params):
            return params.dictionary
        case .setFundPassword(let params):
            return params.dictionary
        case .changeFundPassword(let params):
            return params.dictionary
        case .setUserinfo(let params):
            return params.dictionary
        case .packetlist(let parameters):
            return parameters.dictionary
        case .fetchedList(let parameters):
            return parameters.dictionary
        case .bindCard(let params):
            return params.dictionary
        case .cardList:
            return nil
        case .getProvinceCity:
            return nil
        case .levelList:
            return nil
        case .childList(let params):
            return params.dictionary
        case .accountChangeType:
            return nil
        case .accountChange(let params):
            return params.dictionary
        
        case .childBrokerageList(let params):
            return params.dictionary
        }
        
    }
    
}
