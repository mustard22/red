//
//  UserCenter.swift
//  Red
//
//  Created by MAC on 2019/8/22.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
//import SwiftyJSON


public typealias CompletionHandler = ((_ isSuccess: Bool,_  message: String) -> Void)

public struct UserCenter {
    public static var `default` = UserCenter()
    
    public var me = User()
    public var lbanner = LoginBanner()
    public let rsaPublicTag = "com.fe.rsa.pubic"
}

extension UserCenter {
    //MARK: 获取用户信息(完整信息)
    public mutating func refreshSelfInfo(_ completionHandler: CompletionHandler? = nil) {
        
        let unsafe = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.request(UserInfoAPI.getInfo).responseObject(to: User.self){(isSuccess,message,user) in
            guard let info = user else {
                completionHandler?(false,"获取失败")
                return
            }
           
            unsafe.pointee.me = info
            completionHandler?(true, message.text)
        }
    }
    
    
    //MARK: 获取用户部分信息(用于更新频率比较大的信息)
    public func getSectionInfo(_ completionHandler: CompletionHandler? = nil) {
        HttpClient.default.request(UserInfoAPI.getAvatar).responseObject(to: User.self){(isSuccess,message,user) in
            
            guard let newInfo = user else {
                completionHandler?(false, message.text)
                return
            }
            
            // 判断以是否返回user信息为准
            User.default.avatar = newInfo.avatar
            User.default.nickname = newInfo.nickname
            User.default.balance = newInfo.balance
            User.default.speak_status = newInfo.speak_status
            User.default.frozen_code = newInfo.frozen_code
            User.default.gender = newInfo.gender
            User.default.proxy_level = newInfo.proxy_level
            User.default.mobile = newInfo.mobile
            User.default.user_id = newInfo.user_id
            
            completionHandler?(isSuccess, message.text)
        }
    }
    
    //MARK: 微信注册绑定手机号
    public func bindPhone(_ params: BindPhoneParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.bindMobile(params)).responseObject(to: [String:String].self){(isSuccss,message,data) in
            handler?(isSuccss, message.text)
        }
    }
    
    //MARK: 自动登陆
    public func autoLogin( _ completionHandler: @escaping CompletionHandler) {
        getSectionInfo { (isSuccess, message) in
            completionHandler(isSuccess, message)
        }
    }
    
    //MARK: 登录
    public func login(parame:LoginParam,_ completionHandler:@escaping (_ status: Bool, _ msg: String, _ info: Login?)->Void) {
        HttpClient.default.request(LoginAPI.login(parameters: parame)).responseObject(to: Login.self) {(isSuccess,message,info) in
            guard let login = info else {
                completionHandler(false, message.text, nil)
                return
            }
            
            // 保存token
            HttpClient.default.tokenData = (login.token_type, login.token)
//            dprint("login token: \(login.token)")
            
            UserCenter.default.autoLogin({ (status,message) in
                if !status {
                    completionHandler(false, message, login)
                    return
                }
                completionHandler(true, message, login)
            })
            
            // NOTE: 微信首次登录 成功绑定手机号后 接口会返回login_first
            // 缓存新用户奖励金额值
            let money = login.login_first
            
            dprint("new user money is: \(money)")
            
            guard let m = Double(money), m > 0.0 else {
                UserDefaults.Common.newUserMoney = nil
                return
            }
            
            UserDefaults.Common.newUserMoney = money
        }
    }
    
    
    //MARK: 用户注册
    public func regist(parame:RegistParam, _ completionHandler:@escaping CompletionHandler) {
        HttpClient.default.request(LoginAPI.register(parameters: parame)).responseObject(to: String.self) {(isSuccess,message,_) in
            completionHandler(isSuccess,message.text)
        }
    }
    
    //MARK: 找回密码
    public func findPasswd(param: FindPasswdParam,  _ completionHandler:@escaping CompletionHandler) {
        HttpClient.default.request(LoginAPI.findPassword(param)).responseObject(to: String.self) {(isSuccess,message,_) in
            completionHandler(isSuccess, message.text)
        }
    }
    
    //MARK: 用户退出
    public func logout(_ completionHadler:@escaping CompletionHandler) {
        HttpClient.default.request(LoginAPI.logout).responseObject(to: String.self){(isSuccess,messge,_) in
            completionHadler(isSuccess,messge.text)
        }
    }
}
