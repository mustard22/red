//
//  User.swift
//  Red
//
//  Created by MAC on 2019/8/22.
//  Copyright © 2019 MAC. All rights reserved.
//

//import Foundation
import Foundation

public struct User: Codable {
    public static var `default`:User {
        get {
            return UserCenter.default.me
        }
        set (new){
            UserCenter.default.me = new
        }
    }
    
    public var user_id: String = ""
    /// 用户余额
    public var balance: String = ""
    /// 打码量
    public var frozen_code: String = ""
    /// 昵称
    public var nickname: String = ""
    /// 头像
    public var avatar: String = ""
    /// 代理等级
    public var proxy_level: String = ""
    /// 是否禁言(0未禁言，1禁言)
    public var speak_status: String = ""
    /// 网站标题
    public var web_site_title: String = ""
    public var frozen: String = ""
    public var download_url: String = ""
    public var mobile: String = ""
    /// 用户等级
    public var level: String = ""
    public var username: String = ""
    /// 邮箱地址
    public var email: String = ""
    /// 性别（0保密 1男 2女）
    public var gender: String = ""
    /// 是否设置资金密码（0否 1是）
    public var has_funds_password: String = ""
    public var version: String = ""
    /// 邀请码
    public var invite_code: String = ""
    
    /// 代理中心界面顶部的图片url
    public var proxy_list: String = ""
    /// 代理中心顶部图片点击跳转url（若为空，点击图片跳转到推广赚钱界面）
    public var proxy_url: String = ""
    
    public var bank_list: [Dictionary<String,String>] = [Dictionary<String,String>]()
    
    /// 添加好友开关
    public var add_friend_switch: String = ""
    
    /// 代理中心界面返佣排行榜翻滚数据
    public var proxy_notice = [ProxyNoticeItem]()
    
    /// 牛包过期时间
    public var niu_expire_minute: String = ""
    
    public var isLogin: Bool {
        return user_id.count > 0
    }
}


//MARK: 上传头像的返回结果
public struct UploadAvatar:Codable {
    // 修改用户信息时，把path当做头像参数传过去
    public var path = ""
    public var url = ""
}


/// 银行卡列表item
public struct BankCardItem:Codable {
    public var id = 0
    public var create_time = ""
    public var bank_name = ""
    public var username = ""
    public var show_card = ""
    public var logo = ""
    public var color = ""
}



/// 代理列表的等级列表数据
public struct ProxyListLevelItem: Codable {
    public var alias = ""
    public var id: String = ""
    public var level: String = ""
}

//MARK: 代理中心界面返佣排行榜翻滚数据
public struct ProxyNoticeItem:Codable {
    public var title = ""
    public var content = ""
    public var draw_water = ""
    public var sort = ""
}

/// 零钱类型数据
public struct AccountChangeTypeItem:Codable {
    public var sign = ""
    public var name = ""
}



/// 零钱列表数据
public struct MoneyListData:Codable {
    public var total = 0
    public var totalPage = 0
    public var currentPage = 0
    public var data = [MoneyListItem]()
    
    public struct MoneyListItem:Codable {
        public var id = 0
        public var type = ""
        public var typeName = ""
        /// 变动金额
        public var val = ""
        /// 变动前金额
        public var before_balance = ""
        /// 变动后金额
        public var current_balance = ""
        public var time = ""
    }
}

/// 代理列表的直属下级代理数据item
public struct ProxyListChildData: Codable {
    public var total = ""
    public var totalPage = ""
    public var currentPage = ""
    public var data = ProxyChildItem()
    
    public struct ProxyChildItem:Codable {
        public var child = [ChildItem]()
        /// 直属下级
        public var child_count = ""
        /// 团队人数
        public var total_count = ""
        
        public struct ChildItem: Codable {
            public var user_id = ""
            public var nickname = ""
            public var avatar = ""
            public var level = ""
            /// 洗码量
            public var frozen_code = ""
            /// 性别：0保密 1男 2女
            public var gender = ""
            /// 贡献的抽水
            public var user_profit = ""
            /// 红包流水
            public var project_water = ""
        }
    }
}



public struct UserSignRecordItem: Codable {
    // 已签到日期
    var newDay: [String] = []
    
    var ruleText = ""
    
    // 积分数
    var integral = "0"
}

/// 签到返回item
public struct UserSignItem: Codable {
    /// 签到成功增加的积分
    var integral = "0"
    /// 连续签到获得的钱数
    var money = "0"
    /// 连续签到天数
    var day = ""
}

