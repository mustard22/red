//
//  Notification[User].swift
//  Red
//
//  Created by MAC on 2019/8/22.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

public extension Notification.Name {
    
    /// 刷新用户信息
    static let refreshUserProfile = Notification.Name(rawValue: "fe.user.info.refresh")
    
   
    
}
