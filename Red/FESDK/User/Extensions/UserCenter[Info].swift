//
//  UserCenter[Info].swift
//  Red
//
//  Created by MAC on 2019/8/22.
//  Copyright © 2019 MAC. All rights reserved.
//

//import SwiftyJSON

public extension UserCenter {
    //MARK: 获取银行卡列表
    func getBankCardList(_ completionHandler: ((_ status: Bool, _ msg: String, _ cards: [BankCardItem]?)->Void)?) {
        HttpClient.default.request(UserInfoAPI.cardList).responseObject(to: [BankCardItem].self){(isSuccess,msg,items) in
            completionHandler?(isSuccess, msg.text, items)
        }
    }
    
    //MARK: 绑定银行卡
    func bindBankCard(_ params: BindCardParam, _ completionHandler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.bindCard(params: params)).responseObject(to: String.self){(isSuccess,msg,_) in
            completionHandler?(isSuccess, msg.text)
        }
    }

    
    //MARK: 更换邮箱
    func changeEmail(_ params: ChangeEmailParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.changeEmail(params)).responseObject(to: String.self){(isSuccss,message,_) in
            handler?(isSuccss, message.text)
        }
    }
    
    //MARK: 发送邮箱验证码
    func sendEmailCode(_ params: SendEmailParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.sendEmail(params)).responseObject(to: String.self){(isSuccss,message,_) in
            handler?(isSuccss, message.text)
        }
    }
    
    //MARK: 修改登录密码
    func changeLoginPasswd(_ params: ChangeLoginPasswdParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.changePassword(params)).responseObject(to: String.self){(isSuccss,message,_) in
            handler?(isSuccss, message.text)
        }
    }
    
    //MARK: 修改手机号
    func changePhone(_ params: ChangePhoneParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.changeMobileNumber(params)).responseObject(to: String.self){(isSuccss,message,_) in
            handler?(isSuccss, message.text)
        }
    }
    
    //MARK: 检测资金密码是否正确
    func checkFundPassword(_ params: CheckFundPasswdParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.checkFundPassword(params: params)).responseObject(to: String.self){(isSuccess,message,_) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 设置资金密码(123321)
    func setFundPassword(_ params: SetFundPasswdParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.setFundPassword(params: params)).responseObject(to: String.self){(isSuccess,message,_) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 修改资金密码
    func changeFundPassword(_ params: ChangeFundPasswdParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(UserInfoAPI.changeFundPassword(params: params)).responseObject(to: String.self){(isSuccess,message,_) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 上传接口
    func uploadFile(_ params: UploadParam, _ completionHandler: ((_ text: UploadAvatar?)->Void)?) {
        HttpClient.default.upload(UserAPI.upload, params.file!){(isSuccess, msg, item) in
            if isSuccess {
                completionHandler?(item)
            } else {
                completionHandler?(nil)
            }
        }
    }
    
    //MARK: 更新用户信息
    func updateUserInfo(_ params: UpdateInfoParam, _ completionHandler: CompletionHandler? = nil) {
        HttpClient.default.request(UserInfoAPI.setUserinfo(params: params)).responseObject(to: String.self){(isSuccess,message,_) in
            completionHandler?(isSuccess, message.text)
        }
    }
    
    //MARK: 获取登录界面轮播图
    func getCyclePicture(_ completionHandler: CompletionHandler? = nil) {
        var hasCache = false
        
        if let banner = HttpClient.default.getObjectCache(type: LoginBanner.self, forKey: "login_banner") {
            
            UserCenter.default.lbanner = banner
            completionHandler?(true, "")
            hasCache = true
        }

        HttpClient.default.request(UserAPI.loginbanner).responseObject(to: LoginBanner.self){(isSuccess,message,banner) in
            guard let lb = banner else {
                completionHandler?(false, message.text)
                return
            }
            
            UserCenter.default.lbanner = lb
            
            // 缓存圈子开关
            UserDefaults.Common.circleSwitch = lb.web_rings_switch
            
            HttpClient.default.saveObjectCache(object: lb, forKey: "login_banner")
            if hasCache {
                return
            }
            completionHandler?(true, message.text)
        }
    }
    
    //MARK: 获取公钥
    func getPubkey(_ completionHandler:CompletionHandler? = nil) {
        HttpClient.default.request(UserAPI.getPubkey).responseObject(to: String.self) {(isSuccess,message,key) in
            guard let pubkey = key else {
                dprint("获取公钥失败")
                return
            }

            if !RSAUtils.getRSAKeyFromKeychain(UserCenter.default.rsaPublicTag).isNone {
                RSAUtils.deleteRSAKeyFromKeychain(UserCenter.default.rsaPublicTag)
            }
            
            var k = ""
            _ = pubkey.components(separatedBy: "\r\n").map { k += $0 }
            _ = try! RSAUtils.addRSAPublicKey(k, tagName: UserCenter.default.rsaPublicTag)
        }
    }
}
