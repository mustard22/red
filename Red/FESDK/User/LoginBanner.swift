//
//  LoginBanner.swift
//  Red
//
//  Created by MAC on 2019/10/15.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation


/// 登录界面轮播图数据
public struct LoginBanner: Codable {
//    public var id: Int = 0
//    public var title: String = ""
//    public var url: String = ""
//    public var click_count: String = ""
//    public var type: String = ""
    public var child: [LoginChildBanner] = [LoginChildBanner]()
    /// 客服链接
    public var customer_link: String = ""
    /// appid(weixin)
    public var appid: String = ""
    /// APP下载链接
    public var download_url: String = ""
    /// 后台返回最新APP版本
    public var system_ios_version: String = ""
    public var system_app_version: String = ""
    
    //MARK: 开关
    /// 微信登录注册开关：1开启 0关闭
    public var wechat_switch: String = ""
    /// 显示‘注册’按钮开关(0不显示 1显示)
    public var mobile_register_switch: String = ""
    /// 首页列表显示样式(0全部显示不分类型 1按照房间类型分 2按照系统、自建分)
    public var room_class_show = "0"
    /// 签到开关(0隐藏 1显示)
    public var user_sign_switch = "0"
    /// 自建群开关(0隐藏 1显示)
    public var web_owner_group_switch = "0"
    /// 圈子模块开关(0隐藏 1显示)
    public var web_rings_switch = "0"
    
    ///test code: 代理报表（cell: 1默认样式 2新样式; 位置：我的-代理中心-代理报表）
    public var rebate_mode = "1"
}

// 登录轮播图
public struct LoginChildBanner: Codable {
    public var id: Int = 0
    public var title: String = ""
    public var link_url: String = ""
    public var url: String = ""
}
