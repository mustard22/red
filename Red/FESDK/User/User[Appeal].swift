//
//  User[Appeal]API.swift
//  Red
//
//  Created by MAC on 2020/1/3.
//  Copyright © 2020 MAC. All rights reserved.
//

import Alamofire
import Foundation

enum UserAppealAPI {
    /// 用户申诉
    case appeal([String: String])
    /// 签到记录
    case signRecord
    /// 签到
    case sign
}

extension UserAppealAPI: APITarget {
    static var group: String = "user/"
    
    var methodPath: MethodPath {
        switch self {
        case .appeal:
            return (.post, "appeal")
        case .signRecord:
            return (.get, "signRecord")
        case .sign:
            return (.get, "sign")
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .appeal(let p):
            return p
        case .signRecord, .sign:
            return nil
        }
    }
}



public extension HttpClient {
    //MARK: 用户申诉
    func userAppeal(param: [String: String], _ completionHadler: CompletionHandler? = nil) {
        HttpClient.default.request(UserAppealAPI.appeal(param)).responseObject(to: String.self) {(isSuccess,message,_) in
            completionHadler?(isSuccess, message.text)
        }
    }
    
    //MARK: 签到
    func userSign(_ completionHadler: @escaping ((_ flag: Bool, _ msg: String, _ item: UserSignItem)->Void)) {
        HttpClient.default.request(UserAppealAPI.sign).responseObject(to: UserSignItem.self) {(isSuccess,message,item) in
            completionHadler(isSuccess, message.text, item ?? UserSignItem())
        }
    }
    
    //MARK: 获取签到记录
    func userSignRecord(_ completionHadler: @escaping ((_ flag: Bool, _ msg: String, _ item: UserSignRecordItem)->Void)) {
        HttpClient.default.request(UserAppealAPI.signRecord).responseObject(to: UserSignRecordItem.self) {(isSuccess,message,item) in
            completionHadler(isSuccess, message.text, item ?? UserSignRecordItem())
        }
    }
}
