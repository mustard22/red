//
//  UserAPI.swift
//  Red
//
//  Created by MAC on 2019/8/22.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire

enum UserAPI {
    /// 获取公钥
    case getPubkey
    /// 获取登录轮播图
    case loginbanner
    /// 上传
    case upload
}

extension UserAPI:APITarget {
    static var group: String = "common/"
    
    var methodPath: MethodPath {
        switch self {
        case .getPubkey:
            return (.get,"getPubkey")
            
        case .loginbanner:
            return (.get, "loginbanner")
        
        case .upload:
            return (.post, "upload")
        }
    }
    var parameters: Parameters? {
        switch self {
            case .getPubkey:
                return nil
            case .loginbanner:
                return nil
            case .upload:
                return nil
        }
        
    }
}

