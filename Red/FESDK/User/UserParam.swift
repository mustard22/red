//
//  UserParam.swift
//  Red
//
//  Created by MAC on 2019/10/17.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

/// 上传接口参数
public struct UploadParam: Encodable {
    public var file: Data?
}

/// 修改用户信息参数
public struct UpdateInfoParam: Encodable {
    public var avatar: String?
    public var nickname: String?
    public var gender: String?
}


/// 检测资金密码参数
public struct CheckFundPasswdParam: Encodable {
    public var fund_password: String?
}

/// 设置资金密码参数
public struct SetFundPasswdParam: Encodable {
    public var password: String?
    public var sure_pwd: String?
}

/// 修改资金密码参数
public struct ChangeFundPasswdParam: Encodable {
    public var password: String?
    public var sure_pwd: String?
    public var old_password: String?
}



/// 修改登录密码参数
public struct ChangeLoginPasswdParam: Encodable {
    public var password: String?
    public var sure_pwd: String?
    public var old_password: String?
}

/// 绑定手机号参数
public struct BindPhoneParam: Encodable {
    public var mobile: String?
    public var verify_code: String?
}

/// 修改手机号参数
public struct ChangePhoneParam: Encodable {
    public var fund_password: String?
    public var mobile: String?
    public var verify_code: String?
}


/// 发送邮箱验证码参数
public struct SendEmailParam: Encodable {
    public var email: String?
}

/// 更换邮箱参数
public struct ChangeEmailParam: Encodable {
    public var email: String?
    public var verify_code: String?
}


/// 绑定银行卡参数
public struct BindCardParam: Encodable {
    /// 银行标识
    public var bank_sign: String = ""
    /// 省份编码
    public var province_id: String = ""
    /// 城市编码
    public var city_id: String = ""
    /// 开户银行名称
    public var branch: String = ""
    /// 持卡人姓名
    public var true_name: String = ""
    /// 银行卡号
    public var card_number: String = ""
    /// 资金密码
    public var fund_password: String = ""
}


/// 直属下级参数
public struct ProxyChildParam: Encodable {
    public var username: String?
    public var page: Int?
    public var level: String?
}

/// 零钱列表参数
public struct MoneyListParam: Encodable {
    public var type: String?
    public var start_time: String?
    public var end_time: String?
    public var pageIndex: String?
    
    // 0:system 1:owner
    public var is_owner: String = "0"
}
