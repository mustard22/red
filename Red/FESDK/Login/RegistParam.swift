//
//  RegistParam.swift
//  Red
//
//  Created by MAC on 2019/8/26.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

/// 登录加密参数
public struct LoginParam: Encodable {
    public var type: LoginType? //登录类型
    public var code: String? // 手机号 | 账号
    public var verify_code: String? // 验证码号
    public var password: String? // 密码
    public var invite_code: String? // 邀请码
    
    public init() {
    }
    
    enum CodingKeys: String, CodingKey {
        case type = "login_method"
        case code
        case verify_code
        case password
        case invite_code
    }
}


/// 注册参数
public struct RegistParam: Encodable {
    public var mobile: String? // 手机号
    public var verify_code: String? //验证码
    public var invite_code: String? //邀请码
    public var password: String? //密码
    public var sure_pwd: String? //确认密码
}


/// 找回密码参数
public struct FindPasswdParam: Encodable {
    
    public var mobile: String? // 手机号
    public var verify_code: String? //验证码
    public var password: String? //密码
    public var sure_pwd: String? //确认密码
}

