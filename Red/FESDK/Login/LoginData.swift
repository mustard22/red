//
//  LoginData.swift
//  Red
//
//  Created by MAC on 2019/8/21.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
//import SwiftyJSON

public extension HttpClient {
    
     func GetSMSCode(phone:String, _ completionHadler: CompletionHandler? = nil){
        HttpClient.default.request(LoginAPI.getSMSCode(phone: phone)).responseObject(to: String.self) {(isSuccess,message,_) in
            completionHadler?(isSuccess, message.text)
        }
    }
    
}
