//
//  LoginAPI.swift
//  Red
//
//  Created by MAC on 2019/8/21.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire


public enum LoginType:Int,Codable {
    case wechat = 0
    case sms = 1
    case account = 2
    
    public var zh:String {
        switch self {
        case .account:
            // 账号登录
            return "2"
        case .sms:
            // 手机验证码登录
            return "1"
        case .wechat :
            // 微信登录
            return "0"
        }
    }
}
enum LoginAPI {
    /// 获取验证码
    case getSMSCode(phone:String)
    /// 注册
    case register(parameters: RegistParam)
    /// 登录
    case login(parameters: LoginParam)
    /// 找回密码
    case findPassword(FindPasswdParam)
    /// 退出登录
    case logout
    
}

extension LoginAPI:APITarget {
    
     static var group: String  = "login/"
    
    var methodPath: MethodPath {
        switch self {
        case .login:
            return (.post, "login")
        case .getSMSCode:
            return (.post, "sendCode")
        case .register:
            return (.post, "register")
        case .logout:
            return (.get, "logout")
        case .findPassword:
            return (.post, "findPassword")
        }
        
    }
    var parameters: Parameters? {
        switch self {
        case .login(let parameters):
            return parameters.dictionary
        case .getSMSCode(let phoneNum):
            return ["mobile":phoneNum]
        case .register(let parameters):
            return parameters.dictionary
        case .logout:
            return nil
        case .findPassword(let param):
            return param.dictionary
        }
        
    }
}
