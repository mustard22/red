//
//  Login.swift
//  Red
//
//  Created by MAC on 2019/8/23.
//  Copyright © 2019 MAC. All rights reserved.
//

public struct Login: Codable {
    public var mobile: String = ""
    /// 标记房间大类是否显示（1显示 0隐藏）
//    public var room_class_show: String = ""
    public var username: String = ""
    public var token: String = ""
    public var token_type: String = ""
    public var expires_in = 0
    
    /// 新用户奖励金额
    public var login_first: String = ""
}
