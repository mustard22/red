//
//  GroupParam.swift
//  Red
//
//  Created by MAC on 2019/10/28.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
/// 进入房间参数
public struct goRoomParam: Encodable {
    var room_id: String? // 房间号
    var room_passwd: String? //房间密码
    var room_class: String? //房间类型
}


/// 获取群组成员参数
public struct GroupAllMemberParam:Encodable {
    var room_id: String? // 房间号
    var pageIndex: String?
    var pageSize: String?
}



/// 发包参数
public struct GroupSendThunderParam: Encodable {
    /// 房间号
    var room_id: String?
    /// 金额
    var amount: String?
    /// 数量
    var count: String?
    /// 雷号(非雷包时传0)
    var landmine_number: String?
}


/// 群聊天记录列表参数
public struct GroupChatRecordParam: Encodable {
    var pageSize: String = "15"
    var pageIndex: String?
    var room_id: String?
    var msg_id: String?
}
