//
//  GroupList.swift
//  Red
//
//  Created by MAC on 2019/8/24.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

/// 群类型
public struct GroupType: Codable {
    var id: String = "all"
    var name: String = ""
    var logo: String = ""
//    var alias: String = ""
//    var status: String = ""
    
}

/// 大厅列表对象
public struct GroupList: Codable {
    var id: String = ""
    /// 房间名称
    var name: String = ""
    /// 须知
    var know: String = ""
    
    var img: String = ""
    
    /// 红包类型: 1牛牛法 2福利法 3扫雷法 4多雷
//    var room_class: String = ""
    var `class` = ""
    
    /// 是否禁言，0未禁言，1禁言
    var speak_status: String = ""
    
    /// 是否设置密码，0无密码，其他表示设置了相应密码
    var room_passwd: String = ""
    
    /// 红包玩法item（固定数据 根据classType设置）
    var playMethodItem: GroupPlayMethodItem = GroupPlayMethodItem()
    
    /// 群主id
    var user_id: String = ""
    
    /// 是否群主
    var is_owner: String = "0"
    
    /// 是否是系统群（0否 1是）
    var is_system: String = "0"
}



/// 群组房间信息数据
public struct roomMessage: Codable {
    /// 房间名称
    public var room_name: String = ""
    /// 须知
    public var room_know: String = ""
    /// 公告
    public var room_notice: String = ""
    /// 规则
    public var room_rule: String = ""
    public var room_play_rule: String = ""
    
    public var max_money: String = ""
    public var min_money: String = ""
    public var min_package_num: String = ""
    public var max_package_num: String = ""
    
    /// 是否禁言（0正常 1已禁言）
    public var speak_status: String = ""
    /// 是否在黑名单（1正常 2已被拉黑）
    public var user_status: String = ""
    
    public var room_user: RoomUserData = RoomUserData()
    public var room_packet: [Int: String] = [:]
    public var back_list: [RoomUserItem] = []
    
    /// 入群等级
    public var level_limit: String = ""
    /// 发包余额限制
    public var balance_limit: String = ""
    /// 入群流水
    public var limit_flow_num: String = ""

}

// 红包玩法item
public struct GroupPlayMethodItem: Codable{
    public var url: String = ""
    public var title: String = ""
}

public struct RoomUserData: Codable {
    public var total: String = ""
    public var totalPage: String = ""
    public var currentPage: String = ""
    public var member: [RoomUserItem] = []
}


public struct RoomUserItem: Codable {
    public var nickname: String = ""
    public var avatar: String = ""
    public var level: String = ""
    public var user_id: String = ""
    /// 1 群主 0 不是
    public var is_admin: String = ""
    
    /// 1正常 2黑名单
    public var status: String = ""
    /// 禁言 0正常 1已禁言
    public var speak_status: String = "0"
}


/// 群组发包返回结果
public struct GroupSendPackageItem: Codable {
    var create_time: String = ""
    var expire_time: String = ""
    var id: String = ""
}


/// 群组红包信息
public struct GroupPaperInfoItem: Codable {
    public var username: String = ""
    public var id: String = ""
    public var status: String = ""
    public var amount: String = ""
    /// 抢到的钱数
    public var money: String = ""
    public var user_avatar: String = ""
}


/// 群组抢包结果
public struct GroupFetchPaperResult: Codable {
    //
    public var money: String = ""
    // 红包ID
    public var paper_id: String = ""
    // 是否中雷(1中雷 )
    public var is_landmine: String = ""
    
    /// 是否中牛(计算中牛方式：只计算后3位)
    public var isCattleLucky: Bool {
        get {
            var count = 0
            var caculateCount = 0
            for c in money.reversed() {
                let testc = "\(c)"
                if let n = Double(testc) {
                    count += Int(n)
                    caculateCount += 1
                    if caculateCount == 3 {
                        break
                    }
                }
            }
            
            return (count > 0 && count%10==0) ? true : false
        }
    }
    
    // 0默认值 1-中幸运雷
    public var is_lucky: String = ""
}



//MARK: - 群组红包详情信息
public struct GroupPaperDetailData: Codable {
    public var project: ProjectItem = ProjectItem()
    public var data: [ListItem] = []
    // 0sys 1owner
    public var is_owner: String = ""
    
    public struct ProjectItem: Codable {
        public var amount: String = ""
        public var room_type: String = ""
        public var user_avatar: String = ""
        public var nickname: String = ""
        public var packet_count: String = ""
        public var landmine_number: String = ""
        public var stat_fetched_amount: String = ""
        public var stat_fetched_count: String = ""
        /// 牛包倒计时
        public var add_time: String = ""
        
        /// 过期倒计时
        public var expire_time: String = ""
        
    }
    
    
    public struct ListItem: Codable {
        public var receive_name: String = ""
        /// 抢包金额
        public var val: String = ""
        /// 奖励金额
        public var lucky_amount: String = ""
        /// 是否中雷(true or false)
        public var is_landmine: String = ""
        public var receive_id: String = ""
        public var id: String = ""
        /// (sys[系统群没有免死号]: 1为庄家 2闲家); (owner: 1为免死号 2闲家 3为庄家)
        public var type: String = ""
        /// 手气最佳（true 1 or false 0）
        public var is_first: String = ""
        
        public var receive_img: String = ""
        /// 牛号
        public var niu_number: String = ""
        /// 抢包时间
        public var receive_time: String = ""
        /// 中雷金额
        public var landmine_amount: String = ""
    }

}
