//
//  HttpClient[Group].swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import SwiftyJSON

public extension HttpClient {
    /// 群组类型
    func getGroupType(completionHandler: YYRequestCompletionHandler<[GroupType]>? = nil) {
        if let items = self.getObjectCache(type: [GroupType].self, forKey: "group_type") {
            completionHandler?(true, getCacheSuccessMessage, items)
        }
            HttpClient.default.request(GroupAPI.getRoomClass).responseObject(to: [GroupType].self){(isSuccess, messge, data) in
                if let data = data {
                    HttpClient.default.saveObjectCache(object: data, forKey: "group_type")
                }
                completionHandler?(isSuccess, messge.text, data.or([]))
            }
    }
    
    /// 群组广告
   func GroupAD(completionHandler:YYRequestCompletionHandler<HomeTopData>? = nil) {
        
        if let list = self.getObjectCache(type: HomeTopData.self, forKey: "group_banner") {
            completionHandler?(true,getCacheSuccessMessage,list)
        }
        
        HttpClient.default.request(GroupAPI.getAdvert).responseObject(to: HomeTopData.self){(isSuccess,messge,data) in
            if let data = data {
                HttpClient.default.saveObjectCache(object: data, forKey: "group_banner")
            }
            completionHandler?(isSuccess,messge.text,data.or(HomeTopData()))
        }
        
    }
    
    /// 群组列表
    func GroupListData(class_id: String = "all", completionHandler: YYRequestCompletionHandler<[GroupList]>? = nil) {
        HttpClient.default.request(GroupAPI.groupList(class_id: class_id)).responseObject(to: [GroupList].self){(isSuccess,messge,groupList) in
            completionHandler?(isSuccess,messge.text,groupList.or([]))
        }
    }
    
    /// 进入房间
    func roomMessageData(parame:goRoomParam,completionHandler: YYRequestCompletionHandler<roomMessage>? = nil) {
        
        HttpClient.default.request(GroupAPI.goRoom(parameters: parame)).responseObject(to: roomMessage.self) { (isSuccess,message,messageList) in
            if let messageList = messageList {
                completionHandler?(isSuccess,message.text,messageList)
            }
            else {
                completionHandler?(isSuccess,message.text,roomMessage())
            }
        }
    }
    
    /// 获取群成员
    func getGroupAllMembers(parame: GroupAllMemberParam, completionHandler: YYRequestCompletionHandler<RoomUserData>? = nil) {
        
        HttpClient.default.request(GroupAPI.getRoomMember(parame)).responseObject(to: RoomUserData.self){(isSuccess,message,messageList) in
            
            if let messageList = messageList {
                completionHandler?(isSuccess,message.text,messageList)
            } else {
                completionHandler?(isSuccess,message.text,RoomUserData())
            }
        }
        
    }
    
    /// 退群
    func outGroup(param: String, handler: CompletionHandler? = nil) {
        HttpClient.default.request(GroupAPI.outRoom(param)).responseObject(to: JSON.self){(isSuccess, message, _) in
                handler?(isSuccess, message.text)
        }
    }
    
    /// 发包(雷包 牛包 福利包)
    func sendThunderPackage(param: GroupSendThunderParam, handler: ((_ status: Bool, _ msg: String, _ item: GroupSendPackageItem?)->Void)? = nil) {
        HttpClient.default.request(GroupAPI.sendProject(param)).responseObject(to: GroupSendPackageItem.self){(isSuccess, message, item) in
            handler?(isSuccess, message.text, item)
        }
    }
    
    /// 红包是否可以抢
    func isFetchPaper(param: String, handler: ((_ status: Bool, _ msg: String, _ item: String?)->Void)? = nil) {
        HttpClient.default.request(GroupAPI.isFetchPaper(param)).responseObject(to: String.self){(isSuccess, message, item) in
            handler?(isSuccess, message.text, item)
        }
    }
    
    /// 抢雷包
    func groupFetchPaper(project_id: String, senderId: String, handler: ((_ status: Bool, _ msg: String, _ item: GroupFetchPaperResult?)->Void)? = nil) {
        let p = ["project_id":project_id, "senderId":senderId]
        HttpClient.default.request(GroupAPI.fetchPaper(p)).responseObject(to: GroupFetchPaperResult.self){(isSuccess, message, item) in
            handler?(isSuccess, message.text, item)
        }
    }
    
    /// 抢福利包
    func groupFetchFuPaper(project_id: String, senderId: String, handler: ((_ status: Bool, _ msg: String, _ item: GroupFetchPaperResult?)->Void)? = nil) {
        self.groupFetchPaper(project_id: project_id, senderId: senderId, handler: handler)
//        let p = ["project_id":project_id, "senderId":senderId]
//        HttpClient.default.request(GroupAPI.fetchFuPaper(p)).responseObject(to: GroupFetchPaperResult.self){(isSuccess, message, item) in
//            handler?(isSuccess, message.text, item)
//        }
    }
    
    /// 抢牛包
    func groupFetchNiuPaper(project_id: String, senderId: String, handler: ((_ status: Bool, _ msg: String, _ item: GroupFetchPaperResult?)->Void)? = nil) {
        self.groupFetchPaper(project_id: project_id, senderId: senderId, handler: handler)
//        let p = ["project_id":project_id, "senderId":senderId]
//        HttpClient.default.request(GroupAPI.fetchNiuPaper(p)).responseObject(to: GroupFetchPaperResult.self){(isSuccess, message, item) in
//            handler?(isSuccess, message.text, item)
//        }
    }
    
    /// 红包信息
    func groupGetPaperInfo(project_id: String, senderId: String, handler: ((_ status: Bool, _ msg: WEMessage, _ item: GroupPaperInfoItem?)->Void)? = nil) {
        let p = ["project_id":project_id, "senderId":senderId]
        HttpClient.default.request(GroupAPI.paperInfo(p)).responseObject(to: GroupPaperInfoItem.self){(isSuccess, message, item) in
            handler?(isSuccess, message, item)
        }
    }
    
    /// 红包detail
    func groupGetPaperDetail(project_id: String, senderId: String, handler: ((_ status: Bool, _ msg: String, _ item: GroupPaperDetailData?)->Void)? = nil) {
        
        let p = ["project_id":project_id, "senderId":senderId]
        
        HttpClient.default.request(GroupAPI.projectDetail(p)).responseObject(to: GroupPaperDetailData.self){(isSuccess, message, item) in
            
            guard var item = item else {
                handler?(isSuccess, message.text, nil)
                return
            }
            
            // 把免死号数据放到开头
            if item.data.count > 1, let index = item.data.lastIndex(where: {$0.type == "1"}), index > 0 {
                var datas = item.data
                let last = datas[index]
                datas.remove(at: index)
                datas.insert(last, at: 0)
                item.data = datas
            }
            
            // 对牛包数据进行错乱排序(2020-01-10)
            if item.project.room_type == "1"/*牛包类型*/,
                item.data.count > 1/*至少有2个数据*/,
                let first = item.data.first(where: {$0.type == "1"}/*拿到群主对应的数据*/)
            {
                let setItems = NSSet(array: item.data)
                var newItems = setItems.filter({($0 as! GroupPaperDetailData.ListItem).id != first.id})
                newItems.insert(first, at: 0)
                item.data = newItems as! [GroupPaperDetailData.ListItem]
            }
            
            handler?(isSuccess, message.text, item)
        }
    }
    
    /// 群聊天记录
    func groupChatRecordList(param: GroupChatRecordParam, handler: ((_ status: Bool, _ msg: String, _ items: MessageListData?)->Void)? = nil) {
        HttpClient.default.request(GroupAPI.projectList(param)).responseObject(to: MessageListData.self){(isSuccess, message, item) in
            handler?(isSuccess, message.text, item)
        }
    }
}
