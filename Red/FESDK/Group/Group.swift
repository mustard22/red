//
//  Group.swift
//  Red
//
//  Created by MAC on 2019/8/24.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

//MARK: 首页顶部数据（轮播图+提示语）
public struct HomeTopData: Codable {
    var advert: HomeBannerData?
    var notice: [HomeNoticeData]?
}

/// 首页顶部公告item
public struct HomeNoticeData: Codable {
    var title: String = ""
    var content: String = ""
}

/// 首页顶部轮播图数据
public struct HomeBannerData: Codable {
    var id = 0
    var click_count: String = ""
    var title: String = ""
    var type: String = ""
    var url: String = ""
    var child = [HomeBanner]()
    
    /// 首页顶部轮播图item
    public struct HomeBanner: Codable {
        public var id = 0
        public var title: String = ""
        public var url: String = ""
        public var link_url: String = ""
    }
}


