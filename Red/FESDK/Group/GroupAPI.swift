//
//  GroupAPI.swift
//  Red
//
//  Created by MAC on 2019/8/21.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire

enum GroupAPI {
    /// 首页轮播图
    case getAdvert
    /// 系统群列表
    case groupList(class_id: String)
    /// 获取群成员
    case getRoomMember(GroupAllMemberParam)
    /// 系统群详情
    case goRoom(parameters: goRoomParam)
    /// 退群
    case outRoom(String)
    /// 发包
    case sendProject(GroupSendThunderParam)
    /// 红包是否可以抢
    case isFetchPaper(String)
    /// 抢雷包
    case fetchPaper([String: String])
    /// 抢福利包
    case fetchFuPaper([String: String])
    /// 抢牛包
    case fetchNiuPaper([String: String])
    /// 获取红包信息
    case paperInfo([String: String])
    /// 红包详情
    case projectDetail([String: String])
    /// 群聊天记录
    case projectList(GroupChatRecordParam)
    /// 群类型
    case getRoomClass
}

extension GroupAPI: APITarget {
    static var group: String = "apigame/"
    
    var parameters: Parameters? {
        switch self {
        case .getAdvert:
            return nil
        case .groupList(let param):
            return ["class_id":param]
        case .goRoom(let parameters):
            return parameters.dictionary
        case .getRoomMember(let param):
            return param.dictionary
        case .outRoom(let param):
            return ["room_id":param]
        case .sendProject(let param):
            return param.dictionary
        case .isFetchPaper(let param):
            return ["project_id":param]
        case .fetchPaper(let param):
            return param
        case .fetchFuPaper(let param):
            return param
        case .fetchNiuPaper(let param):
            return param
        case .paperInfo(let param):
            return param
        case .projectDetail(let param):
            return param
        case .projectList(let param):
            return param.dictionary
        case .getRoomClass:
            return nil
        }
        
    }
    
    var methodPath: MethodPath {
        switch self {
        case .getAdvert:
            return (.get,"getAdvert")
        case .groupList:
            return (.post,"roomList")
        case .goRoom:
            return (.post,"goRoom")
        case .getRoomMember:
            return (.post, "getRoomMember")
        case .outRoom:
            return (.post, "outRoom")
        case .sendProject:
            return (.post, "sendProject")
        case .isFetchPaper:
            return (.post, "isFetchPaper")
        case .fetchPaper:
            return (.post, "fetchPaper")
        case .fetchFuPaper:
            return (.post, "fetchFuPaper")
        case .fetchNiuPaper:
            return (.post, "fetchNiuPaper")
        case .paperInfo:
            return (.post, "paperInfo")
        case .projectDetail:
            return (.post, "projectDetail")
        case .projectList:
            return (.post, "projectList")
        case .getRoomClass:
            return (.get, "getRoomClass")
        }
    }
}
