//
//  HttpClient[OwnerGroup].swift
//  Red
//
//  Created by MAC on 2020/1/3.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

//MARK: - 用户自建群的相关接口
public extension HttpClient {
    //MARK: 退群
    func ownerGroupOutroom(param: String, handler: CompletionHandler? = nil) {
        HttpClient.default.request(OwnerGroupAPI.outRoom(param)).responseObject(to: String.self){(isSuccess, message, _) in
                handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 创建的房间列表
    func ownerGroupCreateRoomList(class_id: String = "all", handler: ((_ status: Bool, _ msg: String, _ items: [GroupList])->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.ownerGroupList(class_id)).responseObject(to: [GroupList].self){(isSuccess, message, items) in
            if let datas = items {
                handler?(isSuccess, message.text, datas)
            } else {
                handler?(isSuccess, message.text, [])
            }
        }
    }
    
    //MARK: 创建房间
    func ownerGroupCreateRoom(param: OwnerCreateParam, handler: CompletionHandler? = nil) {
        HttpClient.default.request(OwnerGroupAPI.createGroup(param)).responseObject(to: String.self){(isSuccess, message, _) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 创建房间时需要的赔率数据
    func ownerGroupRedPacketConfig(type: String, handler: ((_ status: Bool, _ msg: String, _ items: [[String]]?)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.redPacketConfig(type)).responseObject(to: [[String]].self){(isSuccess, message, items) in
            handler?(isSuccess, message.text, items)
        }
    }
    
    //MARK: 发包
    func ownerGroupSendPaper(param: OwnerSendPaperParam, handler: ((_ status: Bool, _ msg: String, _ item: GroupSendPackageItem?)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.sendRedPacket(param)).responseObject(to: GroupSendPackageItem.self){(isSuccess, message, data) in
            handler?(isSuccess, message.text, data)
        }
    }
    
    //MARK: 红包信息（paper_id：红包id）
    func ownerGroupPaperInfo(paper_id: String, handler: ((_ status: Bool, _ msg: String, _ item: OwnerGroupPaperInfoResult?)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.paperInfo(paper_id)).responseObject(to: OwnerGroupPaperInfoResult.self){(isSuccess, message, item) in
            handler?(isSuccess, message.text, item)
        }
    }
    
    //MARK: 红包详情（paper_id：红包id）
    func ownerGroupPaperDetail(paper_id: String, handler: ((_ status: Bool, _ msg: String, _ item: GroupPaperDetailData?)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.projectDetail(paper_id)).responseObject(to: GroupPaperDetailData.self){(isSuccess, message, item) in
            
            guard var item = item else {
                handler?(isSuccess, message.text, nil)
                return
            }
            
            // 把免死号数据放到开头
            if item.data.count > 1, let index = item.data.lastIndex(where: {$0.type == "1"}), index > 0 {
                var datas = item.data
                let last = datas[index]
                datas.remove(at: index)
                datas.insert(last, at: 0)
                item.data = datas
            }
            
            // 对牛包数据进行错乱排序(2020-01-10)
            if item.project.room_type == "1"/*牛包类型*/,
                item.data.count > 3/*至少有4个数据*/
            {
                // 把免死号(type=1) 和 庄家(type=3)放到前面, 后面的数据错乱排序
                let preItems = item.data.filter({$0.type == "1" || $0.type == "3"})
                let set = NSSet(array: item.data)
                let sitems = set.filter { (m) -> Bool in
                    if let m2 = m as? GroupPaperDetailData.ListItem, m2.type != "1", m2.type != "3" {
                        return true
                    }
                    return false
                }
                
                item.data = preItems + sitems as! [GroupPaperDetailData.ListItem]
            }
            
            handler?(isSuccess, message.text, item)
        }
    }
    
    //MARK: 抢包（paper_id：红包id）
    func ownerGroupFetchPaper(paper_id: String, handler: ((_ status: Bool, _ msg: String, _ item: OwnerGroupFetchPaperResult?)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.fetchRedPacket(paper_id)).responseObject(to: OwnerGroupFetchPaperResult.self){(isSuccess, message, item) in
            handler?(isSuccess, message.text, item)
        }
    }
    
    //MARK: 房间详情（group_id：房间id, room_passwd: 房间密码）
    func ownerGroupRoomDetail(group_id: String, room_passwd: String, handler: ((_ status: Bool, _ msg: String, _ item: roomMessage?)->Void)? = nil) {
        let param = ["group_id": group_id, "room_passwd": room_passwd]
        HttpClient.default.request(OwnerGroupAPI.groupDetail(param)).responseObject(to: roomMessage.self){(isSuccess, message, item) in
            handler?(isSuccess, message.text, item)
        }
    }
    
    //MARK: 申请进群(parent_id: 邀请人id, group_id：房间id)
    func ownerGroupApplyRoom(group_id: String, parent_id: String, handler: ((_ flag: Bool, _ msg: WEMessage)->Void)? = nil) {
        let param = ["group_id": group_id, "parent_id": parent_id]
        HttpClient.default.request(OwnerGroupAPI.applyGoGroup(param)).responseObject(to: String.self){(isSuccess, message, _) in
            handler?(isSuccess, message)
        }
    }
    
    //MARK: 群主邀请成员（group_id：房间id）
    func ownerGroupInviteMember(group_id: String, user_id: String, handler: CompletionHandler? = nil) {
        let param = ["group_id": group_id, "user_id": user_id] as [String : Any]
        HttpClient.default.request(OwnerGroupAPI.inviteMember(param)).responseObject(to: String.self){(isSuccess, message, _) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 成员间邀请（group_id：房间id）
    func ownerGroupShareGroup(group_id: String, user_id: String, handler: CompletionHandler? = nil) {
        let param = ["group_id": group_id, "user_id": user_id] as [String : Any]
        HttpClient.default.request(OwnerGroupAPI.shareGroup(param)).responseObject(to: String.self){(isSuccess, message, _) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 禁言
    func ownerGroupMuted(param: OwnerAdminMutedParam, handler: CompletionHandler? = nil) {
        HttpClient.default.request(OwnerGroupAPI.saveSpeak(param)).responseObject(to: String.self){(isSuccess, message, _) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 黑名单
    func ownerGroupBlacklist(param: OwnerBlacklistParam, handler: CompletionHandler? = nil) {
        HttpClient.default.request(OwnerGroupAPI.saveStatus(param)).responseObject(to: String.self){(isSuccess, message, _) in
            handler?(isSuccess, message.text)
        }
    }
    
    //MARK: 自建群收益列表数据
    func ownerGroupIncomeList(param: OwnerGroupIncomeParam, handler: ((_ lists: [OwnerGroupIncomeListItem])->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.earnCenter(param)).responseObject(to: OwnerGroupIncomeListData.self){(isSuccess, message, item) in

            if let item = item {
                handler?(item.data)
            } else {
                handler?([])
            }
        }
    }
    
    //MARK: 自建群收益界面顶部数据
    func ownerGroupIncomeTopData(handler: ((_ data: OwnerGroupIncomeTopData)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.totalEarnAmount).responseObject(to: OwnerGroupIncomeTopData.self) {(isSuccess, message, item) in
            handler?(item.or(OwnerGroupIncomeTopData()))
        }
    }
    
    //MARK: 自建群成员列表
    func ownerGroupMemberList(param: OwnerGroupMemberParam, handler: ((_ members: RoomUserData)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.getRoomMember(param)).responseObject(to: RoomUserData.self) {(isSuccess, message, item) in
            handler?(item.or(RoomUserData()))
        }
    }
    
    //MARK: 自建群可以邀请的好友数据
    func ownerGroupAllowInviteFriends(param: OwnerGroupInviteFriendListParam, handler: ((_ members: FriendListData)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.inviteFriendList(param)).responseObject(to: FriendListData.self) {(isSuccess, message, item) in
            handler?(item.or(FriendListData()))
        }
    }
    
    //MARK: 建群的规则说明
    func ownerGroupGetOwnerRule(handler: ((_ content: OwnerGroupCreateRuleData?)->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.getOwnerRule).responseObject(to: OwnerGroupCreateRuleData.self) {(isSuccess, message, item) in
            handler?(item)
        }
    }
    
    //MARK: 黑名单列表
    func ownerGroupBlackList(group_id: String, handler: ((_ items: [RoomUserItem])->Void)? = nil) {
        HttpClient.default.request(OwnerGroupAPI.blackList(group_id)).responseObject(to: [RoomUserItem].self) {(isSuccess, message, items) in
            handler?(items ?? [])
        }
    }
}
