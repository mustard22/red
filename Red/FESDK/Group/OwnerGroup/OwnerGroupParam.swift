//
//  OwnerGroupParam.swift
//  Red
//
//  Created by MAC on 2020/1/3.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation


//MARK: - 创建群参数
public struct OwnerCreateParam: Encodable {
    var group_id = "0"
    /// 群主ID
    var user_id = ""
    /// 群名称
    var name = ""
    /// 房间大类：1牛牛 2福利(普通) 3雷包 4多雷
    var room_class = ""
    /// 须知
    var know = ""
    /// 公告
    var notice = ""
    /// 群密码
    var room_password = ""
    /// 红包最小包数
    var min_package_num = ""
    /// 红包最大包数
    var max_package_num = ""
    /// 最大金额
    var max_money = ""
    /// 最小金额
    var min_money = ""
    /// 余额限制
    var balance_limit = "0"
    /// 入群等级限制(0：全部)
    var level_limit = "0"
    /// 发包配置
    var room_packet_count = ""
    /// 群规
    var rule = ""
    /// 图片链接（先把图片数据上传到服务器，拿到返回的url）
    var img = ""
}


//MARK: - 发包参数
public struct OwnerSendPaperParam: Encodable {
    /// 群ID
    var group_id = ""
    /// 金额
    var amount = ""
    /// 包数
    var count = ""
    /// 雷号/非雷房填-1
    var packet_number = "-1"
}

/// 禁言参数结构
public struct OwnerAdminMutedParam: Encodable {
    var group_id = ""
    // user_id 单个用户ID禁言，禁言群此参数传0
    var user_id = "0"
    // 1禁言0解禁
    var speak_status = ""
}

/// 黑名单参数结构
public struct OwnerBlacklistParam: Encodable {
    var group_id = ""
    // 用户ID
    var user_id = ""
    /// 类型：1踢出成员 2加入黑名单 3解除黑名单
    var type = ""
}


/// 自建群收益列表就扣参数结构
public struct OwnerGroupIncomeParam: Encodable {
    /// 群类型：0全部 1牛牛 2福利 3雷
    var type: String = "0"
    var start_time: String = ""
    var end_time: String = ""
    var pageSize: String = "15"
    var pageIndex: String = "1"
}

/// 自建群成员请求接口参数
public struct OwnerGroupMemberParam: Encodable {
    var group_id: String = ""
    var pageIndex: String = "1"
    var pageSize: String = "20"
    // 用于搜索单个用户
    var keyWord: String = ""
}


/// 自建群中可邀请的好友接口参数
public struct OwnerGroupInviteFriendListParam: Encodable {
    var groupId: String = ""
    var pageIndex: String = "1"
    var pageSize: String = "20"
    // 用于搜索单个用户
    var keyWord: String = ""
}
