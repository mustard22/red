//
//  OwnerGroupAPI.swift
//  Red
//
//  Created by MAC on 2020/1/3.
//  Copyright © 2020 MAC. All rights reserved.
//

import Alamofire

enum OwnerGroupAPI {
    /// 创建群
    case createGroup(OwnerCreateParam)
    
    /// 退群(group_id 群组ID)
    case outRoom(String)
    
    /// 发包
    case sendRedPacket(OwnerSendPaperParam)
    
    /// 创建的群列表
    case ownerGroupList(String)
    
    /// 抢红包(packet_id 红包ID)
    case fetchRedPacket(String)
    
    /// 群组详情(group_id 群组ID，room_passwd 群组密码)
    case groupDetail([String: String])
    
    /// 申请入群(parent_id 邀请人，group_id 群组ID)
    case applyGoGroup([String: String])
    
    /// 群主邀请(group_id: String 群组ID user_id: [String] 被邀请人)
    case inviteMember([String:Any])
    
    /// 好友间邀请(group_id: String 群组ID user_id: [String] 被邀请人)
    case shareGroup([String:Any])
    
    /// 红包信息(packet_id 红包id)
    case paperInfo(String)
    
    /// 红包详情(packet_id 红包id)
    case projectDetail(String)
    
    /// 赔率配置(type 群组类型)
    case redPacketConfig(String)
    
    /// 禁言
    case saveSpeak(OwnerAdminMutedParam)
    
    /// 黑名单
    case saveStatus(OwnerBlacklistParam)
    
    /// 自建群收益列表
    case earnCenter(OwnerGroupIncomeParam)
    
    /// 自建群收益界面 顶部的显示数据
    case totalEarnAmount
    
    /// 获取自建群成员数据
    case getRoomMember(OwnerGroupMemberParam)
    
    /// 自建群中可邀请的好友数据(groupId: 自建群id)
    case inviteFriendList(OwnerGroupInviteFriendListParam)
    
    /// 获取创建群聊的规则说明
    case getOwnerRule
    
    /// 自建群黑名单列表（group_id）
    case blackList(String)
}

extension OwnerGroupAPI: APITarget {
    static var group: String {
        return "ownergroup/"
    }
    
    var parameters: Parameters? {
        switch self {
        case .createGroup(let p):
            return p.dictionary
        case .outRoom(let p):
            return ["group_id": p]
        case .sendRedPacket(let p):
            return p.dictionary
        case .ownerGroupList(let p):
            return ["class_id": p]
        case .fetchRedPacket(let p):
            return ["packet_id": p]
        case .groupDetail(let p):
            return p
        case .applyGoGroup(let p):
            return p
        case .inviteMember(let p):
            return p
        case .shareGroup(let p):
            return p
        case .paperInfo(let p):
            return ["packet_id": p]
        case .projectDetail(let p):
            return ["packet_id": p]
        case .redPacketConfig(let p):
            return ["type": p]
        case .saveSpeak(let p):
            return p.dictionary
        case .saveStatus(let p):
            return p.dictionary
        case .earnCenter(let p):
            return p.dictionary
        case .totalEarnAmount:
            return nil
        case .getRoomMember(let p):
            return p.dictionary
        case .inviteFriendList(let p):
            return p.dictionary
        case .getOwnerRule:
            return nil
        case .blackList(let p):
            return ["group_id": p]
        }
    }
    
    var methodPath: MethodPath {
        switch self {
        case .createGroup:
            return (.post, "createGroup")
        case .outRoom:
            return (.post, "outRoom")
        case .sendRedPacket:
            return (.post, "sendRedPacket")
        case .ownerGroupList:
            return (.post, "ownerGroupList")
        case .fetchRedPacket:
            return (.post, "fetchRedPacket")
        case .groupDetail:
            return (.post, "groupDetail")
        case .applyGoGroup:
            return (.post, "applyGoGroup")
        case .inviteMember:
            return (.post, "inviteMember")
        case .shareGroup:
            return (.post, "shareGroup")
        case .paperInfo:
            return (.post, "paperInfo")
        case .projectDetail:
            return (.post, "projectDetail")
        case .redPacketConfig:
            return (.post, "redPacketConfig")
        case .saveSpeak:
            return (.post, "saveSpeak")
        case .saveStatus:
            return (.post, "saveStatus")
        case .earnCenter:
            return (.post, "earnCenter")
        case .totalEarnAmount:
            return (.get, "totalEarnAmount")
        case .getRoomMember:
            return (.post, "getRoomMember")
        case .inviteFriendList:
            return (.post, "inviteFriendList")
        case .getOwnerRule:
            return (.post, "getOwnerRule")
        case .blackList:
            return (.post, "blackList")
        }
    }
}
