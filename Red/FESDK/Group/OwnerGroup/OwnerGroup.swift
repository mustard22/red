//
//  OwnerGroup.swift
//  Red
//
//  Created by MAC on 2020/1/7.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

//public struct OwnerGroupSendPaperResult: Codable {
//    var project_id: String = ""
//}


public struct OwnerGroupPaperInfoResult: Codable {
    var amount: String = ""
    var id: String = ""
    var money: String = ""
    /// 红包状态（1领取中 2已领取 3已过期）
    var status: String = ""
    var user_avatar: String = ""
    var username: String = ""
}


public struct OwnerGroupFetchPaperResult: Codable {
    var money: String = ""
    var id: String = ""
    /// 是否赔付（雷包中雷、庄家赔付）0否 1是'
    var is_pay: String = ""
    
    /// 是否中牛(计算中牛方式：只计算后3位)
    public var isCattleLucky: Bool {
        get {
            var count = 0
            var caculateCount = 0
            for c in money.reversed() {
                let testc = "\(c)"
                if let n = Double(testc) {
                    count += Int(n)
                    caculateCount += 1
                    if caculateCount == 3 {
                        break
                    }
                }
            }
            
            return (count > 0 && count%10==0) ? true : false
        }
    }
}


/// 自建群收益界面顶部数据
public struct OwnerGroupIncomeTopData: Codable {
    var self_room_flow: String = "0"
    var self_room_profit: String = "0"
}



//MARK: 自建群收益列表数据
public struct OwnerGroupIncomeListData: Codable {
    var data: [OwnerGroupIncomeListItem] = []
    var currentPage: String = ""
    var total: String = ""
}

/// 自建群收益列表单元格数据对象
public struct OwnerGroupIncomeListItem: Codable {
    var add_time: String = ""
    var day: String = ""
    /// 收益金额
    var earn_amount: String = ""
    /// 收佣比例
    var earn_rate: String = ""
    /// 抢包金额（总流水）
    var fetch_amount: String = ""
    /// 群组名称
    var groupName: String = ""
    
    var nickname: String = ""
    /// 自建群ID
    var ow_id: String = ""
    /// 发包金额
    var send_amount: String = ""
    /// 自建群类型
    var type: String = ""
    var user_id: String = ""
    var username: String = ""
}



//MARK: 建群说明数据
public struct OwnerGroupCreateRuleData: Codable {
    var content: String = ""
    var title: String = ""
}
