//
//  FESetParam.swift
//  Red
//
//  Created by MAC on 2019/10/24.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

//MARK: 个人发包请求参数
public struct SendPackageParam: Encodable {
    public var start_time: String? // 开始时间
    public var end_time: String? // 结束时间
    public var pageIndex: String? // 页数
    /// 0sys 1owner
    public var type: String = "0"
}


//MARK: 个人抢包请求参数
public struct GetPackageParam: Encodable {
    public var start_time: String? // 开始时间
    public var end_time: String? // 结束时间
    public var pageIndex: String? // 页数
    /// 0sys 1owner
    public var type: String = "0"
}


//MARK: 充值列表请求参数
public struct ChargeListParam: Encodable {
    public var start_time: String? // 开始时间
    public var end_time: String? // 结束时间
    public var pageIndex: String? // 页数
    public var pageSize: String?
}


//MARK: 充值列表请求参数
public struct WithdrawListParam: Encodable {
    public var start_time: String? // 开始时间
    public var end_time: String? // 结束时间
    public var pageIndex: String? // 页数
}


//MARK: 代理报表请求参数
public struct ProxyReportListParam: Encodable {
    public var start_time: String? // 开始时间
    public var end_time: String? // 结束时间
    public var pageIndex: String? // 页数
}


//MARK: 用户提现参数
public struct UserWithdrawParam: Encodable {
    /// 金额
    public var amount: String?
    /// 银行卡ID
    public var card_id: String?
    /// web,iphone
    public var from: String = "iphone"
    /// 资金密码
    public var fund_password: String?
}


//MARK: 用户发起充值参数
public struct UserChargeParam: Encodable {
    /// 渠道ID
    public var pay_id: String?
    /// 充值金额
    public var amount: String?
    /// 备注信息
    public var remark: String?
    /// 充值类型
    public var pay_class: String?
}
