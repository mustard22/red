//
//  FESetAPI.swift
//  Red
//
//  Created by MAC on 2019/10/24.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire

enum FESetAPI {
    /// 用户提现
    case withdraw(UserWithdrawParam)
    /// 获取支付分类
    case payClass
    /// 发起充值
    case recharge(UserChargeParam)
    /// 提现列表
    case withdrawList(WithdrawListParam)
    /// 充值列表
    case rechargeList(ChargeListParam)
}

extension FESetAPI:APITarget {
    static var group: String = "apifinance/"
    
    var methodPath: MethodPath {
        switch self {
        case .withdraw:
            return (.post, "withdraw")
        case .payClass:
            return (.get, "payClass")
            
        case .recharge:
            return (.post, "recharge")
        case .withdrawList:
            return (.post, "withdrawList")
        case .rechargeList:
            return (.post, "rechargeList")
        }
    }
    var parameters: Parameters? {
        switch self {
        case .withdraw(let param):
            return param.dictionary
        case .payClass:
            return nil
        case .recharge(let param):
            return param.dictionary
        case .withdrawList(let params):
            return params.dictionary
        case .rechargeList(let params):
            return params.dictionary
        }
        
    }
}


