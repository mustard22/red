//
//  HttpClient[Set].swift
//  Red
//
//  Created by MAC on 2019/8/28.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
//import SwiftyJSON
public extension HttpClient {
    
//    func packetLiastData(parameter:PrecordParam ,completionHandler:YYRequestCompletionHandler<[FESet]>? = nil) {
//        
//        HttpClient.default.request(UserInfoAPI.packetlist(parameters: parameter)).responseObject(to: [FESet].self){[weak self](isSuccess,messge,activityList) in
//            
//            if let activityList = activityList {
//               //是否存储y
//            }
//            completionHandler?(isSuccess,messge.text,activityList.or([]))
//        }
//    }
    
    //MARK: 支付相关接口
    /// 用户提现
    func userWithdraw(_ param: UserWithdrawParam, _ handler: CompletionHandler?) {
        HttpClient.default.request(FESetAPI.withdraw(param)).responseObject(to: String.self){(isSuccess,messge,_) in
            handler?(isSuccess,messge.text)
        }
    }
    
    /// 获取支付分类
    func getPayClass(_ handler: ((_ status: Bool, _ items: [PayClassItem]?)->Void)?) {
        HttpClient.default.request(FESetAPI.payClass).responseObject(to: [PayClassItem].self){(isSuccess,messge,items) in
            if isSuccess {
                handler?(isSuccess, items!)
            } else {
                handler?(isSuccess, nil)
            }
        }
    }
    
    /// 发起充值接口
    func userCharge(_ param: UserChargeParam, _ handler: ((_ status: Bool, _ msg: String, _ data: UserChargeData?)->Void)?) {
        HttpClient.default.request(FESetAPI.recharge(param)).responseObject(to: UserChargeData.self){(isSuccess,messge,data) in
            if isSuccess {
                handler?(isSuccess, messge.text, data!)
            } else {
                handler?(isSuccess, messge.text, nil)
            }
        }
    }
    
    /// 零钱类型
    func getMoneyType(_ completionHadler: ((_ status: Bool, _ items: [AccountChangeTypeItem])->Void)?) {
        if let items = self.getObjectCache(type: [AccountChangeTypeItem].self, forKey: "accountChangeType") {
            completionHadler?(true, items)
        }
        HttpClient.default.request(UserInfoAPI.accountChangeType).responseObject(to: [AccountChangeTypeItem].self){(isSuccess,msg,items) in
            if let datas = items, datas.count > 0 {
                completionHadler?(true, datas)
                self.saveObjectCache(object: datas, forKey: "accountChangeType")
            } else {
                completionHadler?(isSuccess, [])
            }
        }
    }
    
    /// 零钱列表数据
    func getMoneyListData(_ params: MoneyListParam, _ completionHadler: ((_ status: Bool, _ data: MoneyListData?)->Void)?) {
        HttpClient.default.request(UserInfoAPI.accountChange(params)).responseObject(to: MoneyListData.self){(isSuccess,msg,data) in
            if isSuccess {
                completionHadler?(isSuccess, data)
            } else {
                completionHadler?(isSuccess, nil)
            }
        }
    }
    
    /// 发包列表数据
    func getSendPackageListData(_ params: SendPackageParam, _ completionHadler: ((_ status: Bool, _ data: SendPackageListData?)->Void)?) {
        HttpClient.default.request(UserInfoAPI.packetlist(parameters: params)).responseObject(to: SendPackageListData.self){(isSuccess,msg,data) in
            if isSuccess {
                completionHadler?(isSuccess, data)
            } else {
                completionHadler?(isSuccess, nil)
            }
        }
    }
    
    /// 抢包列表数据
    func getGetPackageListData(_ params: GetPackageParam, _ completionHadler: ((_ status: Bool, _ data: GetPackageListData?)->Void)?) {
        HttpClient.default.request(UserInfoAPI.fetchedList(params)).responseObject(to: GetPackageListData.self){(isSuccess,msg,data) in
            if isSuccess {
                completionHadler?(isSuccess, data)
            } else {
                completionHadler?(isSuccess, nil)
            }
        }
    }
    
    /// 充值列表数据
    func getChargeListData(_ params: ChargeListParam, _ completionHadler: ((_ status: Bool, _ data: ChargeListData?)->Void)?) {
        HttpClient.default.request(FESetAPI.rechargeList(params)).responseObject(to: ChargeListData.self){(isSuccess,msg,data) in
            if isSuccess {
                completionHadler?(isSuccess, data)
            } else {
                completionHadler?(isSuccess, nil)
            }
        }
    }
    
    /// 提现列表数据
    func getWithdrawListData(_ params: WithdrawListParam, _ completionHadler: ((_ status: Bool, _ data: WithdrawListData?)->Void)?) {
        HttpClient.default.request(FESetAPI.withdrawList(params)).responseObject(to: WithdrawListData.self){(isSuccess,msg,data) in
            if isSuccess {
                completionHadler?(isSuccess, data)
            } else {
                completionHadler?(isSuccess, nil)
            }
        }
    }
    
    
    //MARK: 代理相关接口
    /// 获取等级列表
    func getLevelList(_ completionHadler: ((_ status: Bool, _ items: [ProxyListLevelItem])->Void)?) {
        if let items = self.getObjectCache(type: [ProxyListLevelItem].self, forKey: "level_list") {
            completionHadler?(true, items)
        }
        HttpClient.default.request(UserInfoAPI.levelList).responseObject(to: [ProxyListLevelItem].self){(isSuccess,msg,items) in
            if let datas = items, datas.count > 0 {
                completionHadler?(isSuccess, datas)
                self.saveObjectCache(object: datas, forKey: "level_list")
            } else {
                completionHadler?(isSuccess, [])
            }
        }
    }
    
    /// 获取代理列表
    func getProxyChildList(_ param: ProxyChildParam, _ completionHadler: ((_ status: Bool, _ items: ProxyListChildData?)->Void)?) {
        HttpClient.default.request(UserInfoAPI.childList(param)).responseObject(to: ProxyListChildData.self){(isSuccess,msg,item) in
            completionHadler?(isSuccess, item)
        }
    }
    
    /// 获取代理报表
    func getProxyReportList(_ param: ProxyReportListParam, _ completionHadler: ((_ msg: String, _ item: ProxyReportListData?)->Void)?) {
        HttpClient.default.request(UserInfoAPI.childBrokerageList(param)).responseObject(to: ProxyReportListData.self){(isSuccess,msg,data) in
            completionHadler?(msg.text, data)
        }
    }
}
