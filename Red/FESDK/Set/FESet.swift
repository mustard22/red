//
//  FESet.swift
//  Red
//
//  Created by MAC on 2019/8/28.
//  Copyright © 2019 MAC. All rights reserved.
//


import Foundation

//MARK: 个人发包请求
public struct SendPackageListData: Codable {
    public var total = 0
    public var totalPage = 0
    public var currentPage = 0
    public var data = [SendPackageItem]()
    
    public struct SendPackageItem: Codable {
        public var room_id: String = ""
        /// 房间名称
        public var room_name:String = ""
        /// 红包金额
        public var project_amount: String = ""
        /// 包数
        public var packet_count: String = ""
        /// 1领取中 2已领完 3已过期
        public var status: String = ""
        /// 红包类型(默认为1，牛牛法 2，福利法 3，扫雷法)
        public var room_class: String = ""
        public var created_at: String = ""
    }
}



//MARK: 个人抢包请求
public struct GetPackageListData: Codable {
    public var total = 0
    public var totalPage = 0
    public var currentPage = 0
    public var data = [GetPackageItem]()
    
    public struct GetPackageItem: Codable {
        public var created_at: String = ""
        /// 房间名称
        public var room_name: String = ""
        /// 金额
        public var project_amount:String = ""
        /// 抢包金额
        public var fetched_amount: String = ""
        /// 红包类型(默认为1，牛牛法 2，福利法 3，扫雷法)
        public var room_class: String = ""
        
        public var room_id: String = ""
        
        /// 系统房用
        public var is_landmine: String = ""
        /// 自建房用
        public var is_pay: String = ""
    }
}



//MARK: 充值列表请求
public struct ChargeListData: Codable {
    public var total = 0
    public var totalPage = 0
    public var currentPage = 0
    public var data = [ChargeListItem]()
    
    public struct ChargeListItem: Codable {
        /// 实际到账金额
        public var real_amount:String = ""
        /// 充值金额
        public var amount:String = ""
        /// 客户IP
        public var client_ip:String = ""
        /// 订单号
        public var order_id:String = ""
        /// 充值时间
        public var request_time:String = ""
        /// 处理状态 0:未处理(默认); 1审核通过; 2 拒绝通过
        public var status:String = ""
    }
}





//MARK: 充值列表请求
public struct WithdrawListData: Codable {
    public var total = 0
    public var totalPage = 0
    public var currentPage = 0
    public var data = [WithdrawListItem]()
    
    
    public struct WithdrawListItem: Codable {
        /// 实际到账金额
        public var real_amount:String = ""
        /// 充值金额
        public var amount:String = ""
        /// 客户IP
//        public var client_ip:String = ""
        /// 订单号
        public var order_id:String = ""
        /// 充值时间
        public var request_time:String = ""
        /// 提现通道
        public var channel:String = ""
        
    }
}




//MARK: 代理报表请求
public struct ProxyReportListData: Codable {
    public var total = 0
    public var totalPage = 0
    public var currentPage = 0
    
    /// 总收益
    public var total_income: String = "0"
    /// 总流水
    public var total_turnover: String = "0"
    
    public var data = [ProxyReportListItem]()
    
    public struct ProxyReportListItem: Codable {
        
        /// 下级总数
        public var agent_subordinate_total: String = "0"
        /// 推广返佣金额
        public var agent_brokerage_total:String = "0"
        /// 抢包返水总额
        public var agent_packet_total:String = "0"
        /// 中雷返水总额
        public var agent_mine_total:String = "0"
        /// 中牛返水总额
        public var agent_niureturn_total:String = "0"
        
        public var day: String = ""
        
        
        
        /// 直营流水 / 当天流水
        public var day_turnover: String = "0"
        /// 下级流水
        public var sub_day_turnover: String = "0"
        /// 直营收益
        public var direct_income: String = "0"
        /// 下级佣金
        public var sub_commission: String = "0"
        
    }
}


//MARK: 充值返回数据(request_url)
public struct UserChargeData: Codable {
    // 跳转链接（支付页面url）
    public var request_url:String = ""
}

//MARK: 支付分类数据
public struct PayClassItem: Codable {
    public var id = 0
    public var title:String = ""
    public var alias:String = ""
    public var img:String = ""
    public var level:String = ""
    public var pay_type:[PayTypeItem] = [PayTypeItem]()
    
    public struct PayTypeItem: Codable {
        public var account:String = ""
        public var account_name:String = ""
        public var bank_name:String = ""
        public var name:String = ""
        public var notice:String = ""
        public var pay_img:String = ""
        
        public var max_money = ""
        public var min_money = ""
        // 渠道ID
        public var id = 0
        // 充值类型
        public var pay_class = ""
        public var pay_type = ""
    }
}
