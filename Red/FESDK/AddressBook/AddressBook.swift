//
//  AddressBook.swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

public struct FriendListData: Codable {
    public var total: String = ""
    public var currentPage: String = ""
    public var totalPage: String = ""
    public var data: [FriendItem]?
}

/// 好友列表item
public struct FriendItem: Codable {
    /// 好友昵称
    public var friend_nickname: String = ""
    /// 好友头像链接
    public var friend_avatar:String = ""
    /// 1下级 2上级 3客服 4普通好友 5管理员
    public var friend_type:String = ""
    /// 好友ID
    public var friend_id: String = ""
    /// 好友备注
    public var remark_name: String = ""
    public var user_id: String = ""
    public var username: String = ""
    public var nickname:String = ""
    public var type_name:String = ""
    public var status: String = ""
    
    public var on_line: String = ""
    public var id: String = ""
    public var friend_status: String = ""
}

// 搜索好友item
public struct SearchFriendItem: Codable {
    
    public var friend_nickname: String = ""
    public var friend_avatar:String = ""
    public var id: String = ""
    public var gender: String = ""
    public var mobile: String = ""
    public var status: String = ""
}


/// 好友请求列表数据
public struct FriendRequestListData: Codable {
    public var total: String = ""
    public var totalPage:String = ""
    public var currentPage:String = ""
    public var data:[RequestItem] = []
    
    public struct RequestItem: Codable {
        /// 0未处理 1同意 2拒绝 3黑名单
        public var status: String = ""
        public var uid:String = ""
        public var friend_id:String = ""
        public var id: String = ""
        
        public var type_name: String = ""
        public var friend_status: String = ""
        public var friend_nickname: String = ""
        public var friend_avatar: String = ""
        public var user_id: String = ""
        public var on_line: String = ""
        public var friend_type: String = ""
    }
}



//MARK: 转账结果item
public struct FriendTransferItem: Codable {
    
    public var from_id: String = ""
    public var to_id:String = ""
    public var id: String = ""
    public var content: String = ""
    public var type: String = ""
    public var transfer_amount: String = ""
    public var transfer_status: String = ""
    public var updated_at: String = ""
    public var created_at: String = ""
}


//MARK: 转账领取情况item
public struct TransferReceiveStatusItem: Codable {
    /// 0未领取 1已领取 2已过期
    public var transfer_status: String = ""
    /// 转账金额
    public var transfer_amount:String = ""
    /// 发送人ID
    public var from_id: String = ""
    /// 接收人ID
    public var to_id: String = ""
}


//MARK: 好友聊天列表数据
public struct MessageListData: Codable {
    public var total: String = ""
    public var totalPage: String = ""
    public var currentPage: String = ""
    public var data: [ChatMessage] = []
}

public struct ChatMessage: Codable {
    public var status: String = ""
    public var id:String = ""
    public var from_id:String = ""
    public var from_nickname: String = ""
    public var from_avatar: String = ""
    public var from_username: String = ""
    
    public var to_id: String = ""
    public var to_nickname: String = ""
    public var to_avatar: String = ""
    public var to_username: String = ""
    
    public var content: String = ""
    /// 转账金额
    public var transfer_amount: String = ""
    /// 转账状态：0未领取 1已领取 2已过期
    public var transfer_status: String = ""
    
    public var process_time: String = ""
    public var is_read: String = ""
    public var created_at: String = ""
    public var updated_at: String = ""
    /// 1常规 2转账(红包)
    public var type: String = ""
    
    public var packer_count: String = ""
    public var project_amount: String = ""
    public var project_id: String = ""
    public var room_id: String = ""
}



