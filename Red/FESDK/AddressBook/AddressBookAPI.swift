//
//  AddressBookAPI.swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire

enum AddressBookAPI {
    /// 好友列表
    case friends(FriendListParam)
    /// 搜索好友
    case searchFriend(String)
    /// 转账
    case transfer(FriendTransferParam)
    /// 转账领取状态
    case isReceiveTransfer(String)
    /// 领取转账
    case processTransfer(String)
    /// 请求加好友
    case addFriend([String: String])
    /// 1通过 2拒绝
    case applyFriend([String: String])
    /// 好友请求列表（1好友请求 2添加好友）
    case friendList(String)
    /// 获取聊天好友列表
    case getMsgFriend
    /// 好友聊天记录
    case messages(FriendChatRecordParam)
    /// 删除好友(friendId)
    case deleteFriend(String)
    /// 修改备注
    case updateRemarkName(FriendUpdateRemarkParam)
    
    
}

extension AddressBookAPI: APITarget {
    static var group: String = "apiplayer/"
    
    var parameters: Parameters? {
        switch self {
        case .friends(let p):
            return p.dictionary
        case .searchFriend(let p):
            return ["search":p]
        case .transfer(let p):
            return p.dictionary
        case .isReceiveTransfer(let p):
            return ["id":p]
        case .processTransfer(let p):
            return ["id":p]
        case .addFriend(let p):
            return p
        case .applyFriend(let p):
            return p
        case .friendList(let p):
            return ["type": p]
        case .getMsgFriend:
            return nil
        case .messages(let p):
            return p.dictionary
        case .deleteFriend(let p):
            return ["friendId": p]
        case .updateRemarkName(let p):
            return p.dictionary
        }
    }
    
    var methodPath: MethodPath {
        switch self {
        case .friends:
            return (.post, "friends")
        case .searchFriend:
            return (.post, "searchFriend")
        case .transfer:
            return (.post, "transfer")
        case .isReceiveTransfer:
            return (.post, "isReceiveTransfer")
        case .processTransfer:
            return (.post, "processTransfer")
        case .addFriend:
            return (.post, "addFriend")
        case .applyFriend:
            return (.post, "applyFriend")
        case .friendList:
            return (.post, "friendList")
        case .getMsgFriend:
            return (.post, "getMsgFriend")
        case .messages:
            return (.post, "messages")
        case .deleteFriend:
            return (.post, "deleteFriend")
        case .updateRemarkName:
            return (.post, "updateRemarkName")
        }
    }
    
}
