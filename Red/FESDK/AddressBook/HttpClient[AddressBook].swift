//
//  HttpClient[AddressBook].swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
//import SwiftyJSON

public extension HttpClient {
    // 获取好友列表
    func getFriendList(param: FriendListParam, showCache: Bool = true, completionHandler:YYRequestCompletionHandler<FriendListData>? = nil) {
        HttpClient.default.request(AddressBookAPI.friends(param)).responseObject(to: FriendListData.self){(isSuccess,messge,addressList) in
           completionHandler?(isSuccess,messge.text,addressList ?? FriendListData())
        }
    }
    
    // 搜索好友
    func searchFriend(param: String, handler: ((_ status: Bool, _ msg: String, _ item: [SearchFriendItem]?)->Void)?) {
        HttpClient.default.request(AddressBookAPI.searchFriend(param)).responseObject(to: [SearchFriendItem].self){(isSuccess,messge,items) in
            handler?(isSuccess,messge.text, items)
        }
    }
    
    // 发送的添加请求
    
    // 添加 好友请求
    func addFriend(param: [String: String], handler: ((_ status: Bool, _ msg: String, _ item:String)->Void)?) {
        HttpClient.default.request(AddressBookAPI.addFriend(param)).responseObject(to: String.self){(isSuccess,messge,str) in
            handler?(isSuccess,messge.text, str ?? "")
        }
    }
    
    // 同意 拒绝 好友请求
    func applyFriend(param: [String: String], handler: ((_ status: Bool, _ msg: String, _ item:String)->Void)?) {
        HttpClient.default.request(AddressBookAPI.applyFriend(param)).responseObject(to: String.self){(isSuccess,messge,str) in
            handler?(isSuccess,messge.text, str ?? "")
        }
    }
    
    // 好友请求列表(1好友请求 2自己添加别人)
    func friendRequestList(param: String = "1", handler: ((_ status: Bool, _ msg: String, _ item: FriendRequestListData?)->Void)?) {
        HttpClient.default.request(AddressBookAPI.friendList(param)).responseObject(to: FriendRequestListData.self){(isSuccess,messge,data) in
            handler?(isSuccess,messge.text, data)
        }
    }
    
    // 获取好友消息列表
    func getFriendMessageList(handler: ((_ status: Bool, _ msg: String, _ item: MessageListData?)->Void)?) {
        HttpClient.default.request(AddressBookAPI.getMsgFriend).responseObject(to: MessageListData.self){(isSuccess,messge,data) in
            handler?(isSuccess,messge.text, data)
        }
    }
    
    // 获取好友聊天记录列表
    func getFriendChatRecordList(param: FriendChatRecordParam, handler: ((_ status: Bool, _ msg: String, _ item: MessageListData?)->Void)?) {
        HttpClient.default.request(AddressBookAPI.messages(param)).responseObject(to: MessageListData.self){(isSuccess,messge,data) in
            handler?(isSuccess,messge.text, data)
        }
    }
    
    // 转账
    func friendTransfer(param: FriendTransferParam, handler: ((_ status: Bool, _ msg: String, _ item: FriendTransferItem?)->Void)?) {
        HttpClient.default.request(AddressBookAPI.transfer(param)).responseObject(to: FriendTransferItem.self){(isSuccess,messge,item) in
            handler?(isSuccess,messge.text,item)
        }
    }
    
    // 转账领取状态
    func friendTransferReceiveStatus(param: String, handler: ((_ status: Bool, _ msg: String, _ item: TransferReceiveStatusItem?)->Void)?) {
        HttpClient.default.request(AddressBookAPI.isReceiveTransfer(param)).responseObject(to: TransferReceiveStatusItem.self){(isSuccess,messge,item) in
            handler?(isSuccess,messge.text,item)
        }
    }
    
    // 领取好友转账
    func receiveFriendTransfer(param: String, handler: ((_ status: Bool, _ msg: String, _ item: String?)->Void)?) {
        HttpClient.default.request(AddressBookAPI.processTransfer(param)).responseObject(to: String.self){(isSuccess,messge,item) in
            handler?(isSuccess,messge.text,item)
        }
    }
    
    //MARK: 删除好友
    func deleteFriend(friendId: String, handler: ((_ status: Bool, _ msg: String)->Void)?) {
        HttpClient.default.request(AddressBookAPI.deleteFriend(friendId)).responseObject(to: String.self){(isSuccess,messge,_) in
            handler?(isSuccess,messge.text)
        }
    }
    
    //MARK: 修改备注
    func updateFriendRemark(param: FriendUpdateRemarkParam, handler: ((_ status: Bool, _ msg: String)->Void)?) {
        HttpClient.default.request(AddressBookAPI.updateRemarkName(param)).responseObject(to: String.self){(isSuccess,messge,_) in
            handler?(isSuccess,messge.text)
        }
    }
}
