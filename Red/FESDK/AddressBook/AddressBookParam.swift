//
//  AddressBookParam.swift
//  Red
//
//  Created by MAC on 2019/10/29.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

///
public struct FriendListParam:Encodable {
    public var pageIndex: String?
    public var pageSize: String?
    
}


/// 转账参数
public struct FriendTransferParam:Encodable {
    /// 资金密码
    public var fund_password: String?
    /// 收款人ID
    public var to_id: String?
    /// 转账金额
    public var amount: String?
    /// 备注消息（不超过64个汉字）
    public var remark: String?
}

/// 好友聊天记录列表参数
public struct FriendChatRecordParam:Encodable {
    public var pageSize: String = "15"
    public var pageIndex: String?
    public var friend_id: String?
    public var msg_id: String?
}


/// 修改备注
public struct FriendUpdateRemarkParam: Encodable {
    // 好友id
    public var friendId = ""
    // 备注昵称
    public var remarkName = ""
}
