//
//  Duration.swift
//  Red
//
//  Created by MAC on 2019/8/20.
//  Copyright © 2019 MAC. All rights reserved.//

//import WCDBSwift
//
//public struct Duration: WCDBSwift.ColumnCodable {
//
//    public init?(with value: FundamentalValue) {
//        let time = value.int32Value
//        let seconds = Int(time % 60)
//        let hours = Int(time / 3600)
//        let minutes = Int(time % 3600 / 60)
//        var list: [String] = []
//        if hours > 0 {
//            list.append(String(format: "%.2d", hours))
//        }
//        if hours > 0 || minutes > 0 {
//            list.append(String(format: "%.2d", minutes))
//        }
//        list.append(String(format: "%.2d", seconds))
//        self.seconds = seconds
//        self.hours = hours
//        self.minutes = minutes
//        displayText = list.joined(separator: ":")
//        
//    }
//
//    public func archivedValue() -> FundamentalValue {
//        return FundamentalValue(seconds + minutes * 60 + hours * 3600)
//    }
//    
//    public static var columnType: ColumnType {
//        return .integer32
//    }
//
//    public let seconds: Int
//    public let minutes: Int
//    public let hours: Int
//    public var displayText: String
//}
