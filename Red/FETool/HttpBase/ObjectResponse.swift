//
//  ObjectResponse.swift
//  Red
//
//  Created by MAC on 2019/8/20.
//  Copyright © 2019 MAC. All rights reserved.
//

import Alamofire
import SwiftyJSON
import SwiftDate
import CleanJSON

extension String {
    var stringByEncodingUserInputQuery: String? {
        var set = NSCharacterSet.urlQueryAllowed
        set.remove(charactersIn: "#&=")
        return self.addingPercentEncoding(withAllowedCharacters: set)?.removingPercentEncoding
    }
}

public extension String {
    var url: URL? {
        guard let urlStr = self.stringByEncodingUserInputQuery?.trimmingCharacters(in: .whitespaces) else {
            return nil
        }
        let urlObject = URL(string: urlStr)
        if (urlObject?.host).isNone {
            return nil
        }
        return urlObject
    }
}

public struct WEMessage: LocalizedError {
    public let text: String
    public let code: Int
    
    public var errorDescription: String? {
        return text
    }
    
    public var isSuccess: Bool {
        return code == 200
    }
    
    public init(text: String, code: Int) {
        self.code = code
        self.text = text
    }
}

public struct ListResponse<T: Codable>: Codable {
    public let current: Int
    public let total: Int
    public var list: [T]
    public let size: Int
    public var hasNext: Bool {
        return (size * (current - 1) + list.count) < total
    }
}


public struct ObjectResponse<T: Decodable>: Decodable {
    public let err_code: Int
    public let err_msg: String
    public let data: T
}

public extension Notification.Name {
    static let onForceOffline = Notification.Name(rawValue: "fe.user.offline")
}

extension DataRequest {
    private static func decodableObjectSerializer<T: Decodable>(_ decoder: JSONDecoder) -> DataResponseSerializer<ObjectResponse<T>> {
        return DataResponseSerializer { _, response, data, error in
            #if DEBUG
            if let data = data {
                let dict = try? (JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary)
                dprint(dict as Any)
                dprint(response ?? "")
            }
            #endif
            
            if let error = error {
                return .failure(error)
            }
            return DataRequest.decodeToObject(decoder: decoder, response: response, data: data)
        }
    }

    private static func updateServerTime(response: HTTPURLResponse?) {
        guard let response = response else { return }
        if let headers = response.allHeaderFields as? [String: Any],
            let timeStr = headers["Date"] as? String {
            if let serverNow = timeStr.toDate(style: .httpHeader, region: Region(calendar: Calendars.gregorian.toCalendar(), zone: Zones.gmt.toTimezone(), locale: Locales.englishUnitedStatesComputer)) {
                HttpClient.default.severTimeDifference = serverNow.timeIntervalSince(SwiftDate.defaultRegion.nowInThisRegion())
            }
        }
    }

    private static func decodeToObject<T: Decodable>(decoder: JSONDecoder, response: HTTPURLResponse?, data: Data?) -> Result<ObjectResponse<T>> {
        let result = Request.serializeResponseData(response: response, data: data, error: nil)
        updateServerTime(response: response)
        switch result {
        case let .success(data):
            do {
                // 解析http code
                if response?.statusCode == 500 {
//                    HttpClient.default.tokenData = nil
                    throw WEMessage(text: "网络繁忙", code: response!.statusCode)
                } else if response?.statusCode == 401 {
                    let msg = WEMessage(text: "登录过期", code: response!.statusCode)
                    NotificationCenter.default.post(name: .onForceOffline, object: nil , userInfo:  [Notification.Name.onForceOffline.rawValue: msg])
                    throw msg
                } else if response?.statusCode != 200 {
                    throw WEMessage(text: "加载失败", code: response!.statusCode)
                }
                let response = try decoder.decode(ObjectResponse<T>.self, from: data)
                guard response.err_code == 0 else {
                    if response.err_code == 401 || response.err_code == 402 || response.err_code == 403 {
                        // 其他设备登录
                        let msg = WEMessage(text: "\(response.err_msg)", code: response.err_code)
                        NotificationCenter.default.post(name: .onForceOffline, object: nil , userInfo:  [Notification.Name.onForceOffline.rawValue: msg])
                        throw msg
                    }
                    
                    throw WEMessage(text: response.err_msg, code: response.err_code)
                }
                return .success(response)
            } catch {
                return .failure(error)
            }
        case let .failure(error):
            return .failure(error)
        }
    }
    
    @discardableResult
    func responseObject<T: Decodable>(to object: T.Type,
                                      completionHandler: ((_ isSuccess: Bool, _ message: WEMessage,_ data: T?) -> Void)? = nil) -> Self {
        let decoder = HttpClient.default.jsonDecoder
        return self.response(responseSerializer: DataRequest.decodableObjectSerializer(decoder)) { (response: DataResponse<ObjectResponse<T>>) in
            switch response.result {
            case let .success(resp):
                completionHandler?(true, WEMessage(text: resp.err_msg, code: resp.err_code), resp.data)
                
            case let .failure(error):
                if let weError = error as? WEMessage {
                    completionHandler?(false, weError, nil)
                    return
                }
                
                let err: NSError = error as NSError
                var err_msg = "网络异常"
                switch err.code {
                case -1200:
                    err_msg = "连接服务器失败"
                case -1001:
                    err_msg = "请求超时"
                default: break
                }
                completionHandler?(false, WEMessage(text: err_msg, code: err.code), nil)
            }
        }
    }

    @discardableResult
    func responsePageList<T: Decodable>(to object: T.Type,
                                        completionHandler: ((_ message: WEMessage,_ listData: ListResponse<T>?) -> Void)? = nil) -> Self {
        let decoder = HttpClient.default.jsonDecoder
        return self.response(responseSerializer: DataRequest.decodableObjectSerializer(decoder)) { (response: DataResponse<ObjectResponse<ListResponse<T>>>) in
            switch response.result {
            case let .success(resp):
                completionHandler?(WEMessage(text: resp.err_msg, code: resp.err_code), resp.data)
            case let .failure(error):
                if let weError = error as? WEMessage {
                    completionHandler?(weError, nil)
                } else if let nsError = error as NSError? {
                    let message = nsError.debugDescription
                    completionHandler?(WEMessage(text: message, code: nsError.code), nil)
                }
            }
        }
    }
}
