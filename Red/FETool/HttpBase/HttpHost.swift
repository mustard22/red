//
//  HttpHost.swift
//  Red
//
//  Created by MAC on 2019/8/20.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

// 把对应的api根地址配置在了info.plist
fileprivate let kApiRootUrl = "ApiRootUrl"

public enum HttpHost {
    case qa
    case test
    
    public var url: URL {
        switch self {
        case .qa:
            return URL(string: "\(HttpHost.rootUrl)/api/")!
        case .test:
            return URL(string: "http://192.168.0.163:90/api/")!
        }
    }

    public static var rootUrl: String {
        get {
//            return "http://api.xaslswlkj.com"
//                return "http://192.168.0.175:9500"
//            return "http://192.168.0.108:9500"
            
            if let root = Bundle.main.object(forInfoDictionaryKey: kApiRootUrl) as? String {
                //                dprint("api root url is: \(root)")
                return root
            }
            
            dprint("api root url 未在对应info.plist文件配置")
            return "https://api.xasls.com"
        }
    }
}
