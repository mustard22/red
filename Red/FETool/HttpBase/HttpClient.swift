//
//  HttpClient.swift
//  Red
//
//  Created by MAC on 2019/8/20.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
//import SwiftyJSON
import MMKV
import SwiftDate
import CryptoSwift
import Alamofire
import CleanJSON

public typealias YYRequestCompletionNoData = ((_ isSuccess: Bool, _ message: String) -> Void)
public typealias YYRequestCompletionHandler<T> = ((_ isSuccess: Bool, _ message: String,_ data: T) -> Void)

public extension NetworkReachabilityManager.NetworkReachabilityStatus {
    var zh: String {
        switch self {
        case .unknown: return "未知"
        case .notReachable: return "无网络"
        case .reachable(.ethernetOrWiFi):
            return "WIFI"
        case .reachable(NetworkReachabilityManager.ConnectionType.wwan):
            return "蜂窝网络"
        }
    }
}

public extension MMKV {
    /// 由缓存中获取数据
    ///
    /// - Parameters:
    ///   - type: 数据类型
    ///   - key: 缓存标志
    func  getObjectCache<T: Codable>(type: T.Type, forKey key: String) -> T? {
        guard let data = self.data(forKey: key) else {
            return nil
        }
        return try? JSONDecoder().decode(T.self, from: data)
    }
    
    
    /// 缓存数据
    ///
    /// - Parameters:
    ///   - object: 将要缓存的对象
    ///   - key: 缓存标志
    func saveObjectCache<T: Codable>(object: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(object) {
            self.set(data, forKey: key)
        }
    }
}

extension Dictionary where Key == String, Value == Any {
    @discardableResult
    public mutating func safeUpdate(_ value: Dictionary.Value?, for key: Dictionary.Key) -> Dictionary.Value? {
        if let value = value {
            return updateValue(value, forKey: key)
        }
        return nil
    }
}

struct HttpCache<T: Codable>: Codable {
    let data: T
    let date: Date
}

public struct HttpClient {
    
    public static var `default` = HttpClient()

    public var host = HttpHost.qa
    
    public mutating func unsafePointer() -> UnsafeMutablePointer<HttpClient> {
        return UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
    }
    
//    #if DEBUG
//    public var host = HttpHost.test
//    #else
//    public var host = HttpHost.qa
//    #endif
    
    /// 是否打印网络调试信息。默认为 true
    public var debugLog: Bool = true
    var headers: HTTPHeaders = Alamofire.SessionManager.defaultHTTPHeaders
    public let jsonEncoder = JSONEncoder()
    public let jsonDecoder = CleanJSONDecoder()//JSONDecoder()
    private var preloadingList: [APITarget] = []
    var severTimeDifference: TimeInterval?
    public let httpCache = MMKV(mmapID: "httpcache")!
    public let reachability = Reachability()
    let getCacheSuccessMessage = "加载数据成功"
    lazy var urlEncoding: URLEncoding = {
        let encoding = URLEncoding(destination: .methodDependent, arrayEncoding: .noBrackets, boolEncoding: .literal)
        return encoding
    }()
    
    /// 服务器时间
    public var serverTime: Date {
        return Date().addingTimeInterval(severTimeDifference.or(0))
    }
    
    public var yzToken: String?
    /// 是否已上线
    public var isOnline: Bool = false
    
    /// 用户凭证(token_type, token)
    public var tokenData: (String, String)? {
        get {
            return (UserDefaults.Common.tokenData) ?? ("","")
        }
        set {
            UserDefaults.Common.tokenData = newValue
            if let data = newValue {
                headers["Authorization"] = data.0 + " " + data.1
            } else {
                headers.removeValue(forKey: "Authorization")
            }
        }
    }
    
    func aes(string: String) -> String? {
        do {
            let aes = try AES(key: "EoS266CddoOo7lzk".bytes, blockMode: ECB())
            let ciphertext = try aes.encrypt(Array(string.utf8)).toBase64()?.base64URLEscaped()
            return ciphertext
        } catch {
            return nil
        }
    }
    
    private init() {

        headers.updateValue("application/prs.red.v1+json", forKey: "Accept")
        
        let dateFomatter = DateFormatter()
        dateFomatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        jsonEncoder.dateEncodingStrategy = .formatted(dateFomatter)
        jsonDecoder.dateDecodingStrategy = .formatted(dateFomatter)
        
        if let t = UserDefaults.Common.tokenData {
            self.tokenData = t
        }
        else if let cookie = HTTPCookieStorage.shared.cookies(for: self.host.url)?.first {
            let sections = cookie.value.components(separatedBy: " ")
            if sections.count == 2 {
                self.tokenData = (sections[0], sections[1])
            }
        }
        
        try? self.reachability?.startNotifier()
    }
    
    // 上传
    mutating func upload<T: APITarget>(_ target: T, _ data: Data, _ handler: ((_ isSuccess:Bool, _ message: WEMessage?, _ avatarItem: UploadAvatar?)->Void)?) {
        let url = self.host.url.appendingPathComponent(T.group).appendingPathComponent(target.methodPath.path)
        
        requestManager.upload(multipartFormData: { (multipartFormData) in
            let fname = Date.init().toString() + ".png"
            multipartFormData.append(data, withName: "file", fileName: fname, mimeType: "image/png")
        }, to: url, headers: self.headers) { (encodingResult) in
            switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseObject(to: UploadAvatar.self){(status, msg, item) in
                        handler?(status,msg,item)
                    }
                case .failure(let err):
                    let e = WEMessage(text: err.localizedDescription, code: 0)
                    handler?(false, e, nil)
            }
        }
    }
    
    lazy var requestManager: SessionManager = {
        let con = URLSessionConfiguration.default
        con.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        con.timeoutIntervalForRequest = 10
        con.timeoutIntervalForResource = 10
        let man = SessionManager(configuration: con)
        return man
    }()
    
    /// test(测试的那个接口)
    #if DEBUG
    mutating func request2() -> DataRequest {
        let encoding: ParameterEncoding = JSONEncoding.default
        let url = (URL(string: HttpHost.rootUrl)!).appendingPathComponent("timeOut")
        let request = requestManager.request(url, method: .get, parameters: nil, encoding: encoding, headers: self.headers)
            
            return request
    }
    #endif
    
    
    mutating func request<T: APITarget>(_ target: T) -> DataRequest {
        let url = self.host.url.appendingPathComponent(T.group).appendingPathComponent(target.methodPath.path)
        var encoding: ParameterEncoding = JSONEncoding.default
        var param: [String: Any]?
        
        if target.methodPath.method == .get || target.methodPath.method == .delete {
            encoding = urlEncoding
        }
        else if target.methodPath.method == .post, let token = self.tokenData, !token.1.isEmpty {
            // post 请求参数加密
            if var dict = target.parameters {
                //MARK: 生成sign
                // 1. 参数排序(对字典的key升序排序)
                let keys = dict.keys.map({$0}).sorted(by: <)
                // 2. 拼接参数（p1&p2&p3）, 转成md5, md5转成大写
                let sign = keys.map({"\($0)=\(dict[$0] ?? "")"}).joined(separator: "&").md5().uppercased()
                // 把生成的sign作为参数加入字典
                dict["sign"] = sign
                
                dprint("加密前请求参数：\(dict)")
                
                // rsa加密
                if let rsa = self.dowithRSA(dict as NSDictionary) {
                    param = ["encrypt": rsa]
                }
            }
        }
        
        
        // 加密
//        let request = Alamofire.request(url, method: target.methodPath.method, parameters: param ?? target.parameters, encoding: encoding, headers: self.headers)
        let request = requestManager.request(url, method: target.methodPath.method, parameters: param ?? target.parameters, encoding: encoding, headers: self.headers)
        
        return request
    }
    
    /// 由缓存中获取数据
    ///
    /// - Parameters:
    ///   - type: 数据类型
    ///   - key: 缓存标志
    ///   - date: 缓存时间
    func  getObjectCache<T: Codable>(type: T.Type, forKey key: String, date: Date? = nil) -> T? {
        guard let data = httpCache.data(forKey: key) else {
            return nil
        }
        if let object = try? self.jsonDecoder.decode(HttpCache<T>.self, from: data) {
            if let getDate = date,object.date < getDate {
                return nil
            }
            return object.data
        }
        return nil
    }
    
    /// 缓存数据
    ///
    /// - Parameters:
    ///   - object: 将要缓存的对象
    ///   - key: 缓存标志
    ///   - date: 缓存时间
    func saveObjectCache<T: Codable>(object: T, forKey key: String, date: Date? = nil) {
        let saveDate = date ?? Date()
        let cacheObject = HttpCache(data: object, date: saveDate)
        if let data = try? self.jsonEncoder.encode(cacheObject) {
            httpCache.set(data, forKey: key)
        }
    }
    
    func removeObjectCache(key: String) {
        httpCache.removeValue(forKey: key)
    }
}

//MARK: 参数加密
extension HttpClient {
    // dict -> rsa string
    public func dowithRSA(_ param: NSDictionary?) -> String? {
        guard let para = param else {
            return nil
        }
        let str = self.getJSONStringFromDictionary(dictionary: para)
        return RSAUtils.encryptWithRSAKey(str: str, tagName: "com.fe.rsa.pubic")?.base64EncodedString()
    }
    
    public func getJSONStringFromDictionary(dictionary:NSDictionary) -> String {
        if (!JSONSerialization.isValidJSONObject(dictionary)) {
            dprint("无法解析出JSONString")
            return ""
        }
        
        let data : NSData! = try? JSONSerialization.data(withJSONObject: dictionary, options: []) as NSData?
        let JSONString = NSString(data:data as Data,encoding: String.Encoding.utf8.rawValue)
        return JSONString! as String
    }
}

//MARK: extension String
extension String {
    
    public func base64URLUnescaped() -> String {
        let replaced = replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        let padding = replaced.count % 4
        if padding > 0 {
            return replaced + String(repeating: "=", count: 4 - padding)
        } else {
            return replaced
        }
    }
    
    public func base64URLEscaped() -> String {
        return replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: "")
    }
    
    public mutating func base64URLUnescape() {
        self = base64URLUnescaped()
    }
    
    public mutating func base64URLEscape() {
        self = base64URLEscaped()
    }
}

