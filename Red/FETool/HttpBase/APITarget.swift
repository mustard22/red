//
//  APITarget.swift
//  Red
//
//  Created by MAC on 2019/8/20.
//  Copyright © 2019 MAC. All rights reserved.
//


import Alamofire

typealias MethodPath = (method: HTTPMethod,path: String)

extension Encodable {
    var dictionary: Parameters? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? Parameters }
    }
}

protocol APITarget {
    static var group: String { get }
    var parameters: Parameters? { get }
    var methodPath: MethodPath { get }
}
