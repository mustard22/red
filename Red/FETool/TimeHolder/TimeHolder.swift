//
//  TimeHolder.swift
//  Red
//
//  Created by MAC on 2019/8/27.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

@objc protocol TimeHolderDelegate {
    @objc optional func refresh(time now: Date)
    @objc optional func smsRefresh(coolingTime: Int)
    @objc optional func examRefresh(duration: Int)
}

class TimeHolder: NSObject {
    private var timer: Timer?
    /// 验证码刷新时间
    private(set) var smsCoolingTime: Int = 0
    /// 计时时间
    private(set) var examTime: Int = 0
    static let `default` = TimeHolder()
    @objc dynamic weak var delegate: TimeHolderDelegate?
    
    override init() {
        super.init()
        self.qmui_multipleDelegatesEnabled = true
        let newTimer = Timer(timeInterval: 1,
                             target: self,
                             selector: #selector(handleTimeRefreshEvent(sender:)),
                             userInfo: nil,
                             repeats: true)
        timer = newTimer
        RunLoop.main.add(newTimer, forMode: .common)
    }
    
    /// 重置短信验证码时间
    func resetSMSCoolingTime() {
        smsCoolingTime = 60
    }
    
    func resetExamTime() {
        examTime = 0
    }
    @objc
    private func handleTimeRefreshEvent(sender: Any) {
        let now = Date()
        self.delegate?.refresh?(time: now)
        if smsCoolingTime > 0 {
            smsCoolingTime -= 1
            self.delegate?.smsRefresh?(coolingTime: smsCoolingTime)
        }
        examTime += 1
        self.delegate?.examRefresh?(duration: examTime)
    }
}
