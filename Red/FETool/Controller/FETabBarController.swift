//
//  FETabBarController.swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class FETabBarController: YYTabBarController {
    // 标记是否第一次进来
    private var isFirstEnter = true
    
    //MARK: 充值按钮
    private lazy var chargeButton: UIButton = {
        let back = UIButton()
        back.size = CGSize(width: self.tabBar.width/CGFloat(self.vcs.count), height: self.tabBar.height)
        back.center = CGPoint(x: self.tabBar.width/2, y: self.tabBar.height/2)
        back.isHidden = true
//        back.backgroundColor = .orange
        back.addTarget(self, action: #selector(chargeButtonAction), for: .touchUpInside)
        self.tabBar.addSubview(back)
        
        let imgv = UIImageView()
        imgv.image = UIImage(named: "ic_tabbar_charge")
        imgv.size = CGSize(width: 28*kScreenScale, height: 28*kScreenScale)
        imgv.top = 1
        imgv.centerX = back.width/2
        back.addSubview(imgv)
        
        let lab = UILabel()
        lab.text = "充值"
        lab.textColor = UIColor.RGBColor(22, 139, 21)
        lab.font = UIFont.systemFont(ofSize: 12)
        lab.textAlignment = .center
        lab.frame = CGRect(x: 0, y: imgv.bottom, width: back.width, height: back.height-imgv.bottom)
        back.addSubview(lab)
        return back
    }()
    
    private var showCircle: Bool {
        guard let show = UserDefaults.Common.circleSwitch, show == "1" else {
            return false
        }
        
        return true
    }
    
    private lazy var vcs: [UINavigationController] = {
        var titles = ["大厅", "通讯", "", "动态", "我的"]
        var vs = [UINavigationController]()
        let v1 = FETabBarController.tabbarItemVC(for: FEGroupViewController(), image: UIImage(named: "ic_msg_nor"), selectedImage: UIImage(named: "ic_msg_sel"))
        vs.append(v1)
        let v2 = FETabBarController.tabbarItemVC(for: FEAddressViewController(),image: UIImage(named: "ic_contact_nor"),selectedImage: UIImage(named: "ic_contact_sel"))
        vs.append(v2)
        let v3 = FETabBarController.tabbarItemVC(for: FEBaseViewController(), image: nil, selectedImage: nil)
        vs.append(v3)
        if showCircle {
            let v4 = FETabBarController.tabbarItemVC(for: FECircleViewController(),image: UIImage(named: "ic_group_nor"),selectedImage: UIImage(named: "ic_group_sel"))
            vs.append(v4)
        } else {
            titles.remove(at: 3)
        }
        
        let v5 = FETabBarController.tabbarItemVC(for: FECenterViewController2(),image: UIImage(named: "ic_me_nor"),selectedImage: UIImage(named: "ic_me_sel"))
        vs.append(v5)
        
        for i in 0..<vs.count {
            vs[i].title = titles[i]
        }
        return vs
    }()
    
    override func didInitialize() {
        super.didInitialize()
        
//        viewControllers = self.vcs
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(offLineNotification(_:)), name: NSNotification.Name.onForceOffline, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addressBookMessageTipsNotification), name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
        
        // 请求USerInfo
        UserCenter.default.refreshSelfInfo()
        
        self.setViewControllers(vcs, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstEnter == false {
            return
        }
        
        // 检测是否有新版本
        self.checkNewAppVersion()
        
        // 检测是否新用户 弹框提醒新用户金额奖励
        if self.showMoneyLayerOfNewUser() == false {
            // 检测是否微信首次登录
            _ = self.checkDefaultLoginPasswdOfWechatLogin()
        }
        
        // NOTE: 登录进来主动调用一次 检测是否有未读消息
        addressBookMessageTipsNotification()
        isFirstEnter = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        guard self.chargeButton.isHidden else {
            return
        }

        self.chargeButton.isHidden = false
        
        let width = self.tabBar.width/CGFloat(self.vcs.count)
        self.chargeButton.size = CGSize(width: width, height: self.tabBar.height)
        if showCircle {
            self.chargeButton.center = CGPoint(x: self.tabBar.width/2, y: self.tabBar.height/2)
        } else {
            self.chargeButton.origin = CGPoint(x: width * 2, y: 0)
        }
//        self.chargeButton.size = CGSize(width: self.tabBar.height, height: self.tabBar.height)
//        self.chargeButton.center = CGPoint(x: self.tabBar.width/2, y: self.tabBar.height/2)//self.tabBar.center
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.onForceOffline, object: nil)
        NotificationCenter.default.removeObserver(self, name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
    }
}


fileprivate extension FETabBarController {
    //MARK: check new app version
    func checkNewAppVersion() {
        
        if UIApplication.appVersion >= UserCenter.default.lbanner.system_ios_version {
            return
        }
        
        let alert = UIAlertController(title: "更新提醒", message: "有新版本了，是否去更新？", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "下次提醒", style: .cancel, handler: nil)
        let update = UIAlertAction(title: "立即更新", style: .default) { (action) in
            guard let url = URL(string: UserCenter.default.lbanner.download_url) else {
                QMUITips.show(withText: "数据异常")
                return
            }
            guard UIApplication.shared.canOpenURL(url) else {
                QMUITips.show(withText: "链接无法打开")
                return
            }
            UIApplication.shared.open(url, options: [.universalLinksOnly: false], completionHandler: nil)
        }
        alert.addAction(cancel)
        alert.addAction(update)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: 微信首次登录成功后 显示默认密码
    func checkDefaultLoginPasswdOfWechatLogin() -> Bool {
        // 首次微信登录，且绑定手机号成功，提示默认密码
        guard let wx_first_login_pwd = UserDefaults.Common.firstWXLoginPasswd else {
            return false
        }
        
        let ac = UIAlertController(title: "提示", message: "首次微信登录，默认密码\"\(wx_first_login_pwd)\"", preferredStyle: .alert)
        let action = UIAlertAction(title: "知道了", style: .default) { (a) in
            UserDefaults.Common.firstWXLoginPasswd = nil
        }
        ac.addAction(action)
        self.present(ac)
        
        return true
    }
    
    //MARK: 检测是否新用户
    func showMoneyLayerOfNewUser() -> Bool {
        guard let money = UserDefaults.Common.newUserMoney, money.count>0 else {
            return false
        }
        
        let layer = LoginGetMoneyLayerView()
        layer.money.text = "\(money)元"
        layer.getMoneyHandler = {[weak self] (isObtain) in
            guard let self = self else {
                return
            }
            
            _ = self.checkDefaultLoginPasswdOfWechatLogin()
            
            if !isObtain {
                return
            }
            // 点击领取后 跳到个人中心
            self.selectedIndex = self.vcs.count-1
        }
        layer.show()
        
        // 移除旧值
        UserDefaults.Common.newUserMoney = nil
        
        return true
    }
    
    //MARK: 点击充值按钮
    @objc func chargeButtonAction() {
        self.push(to: FECenterChargeMoneyViewController())
    }
    
    //MARK: 其他设备登录通知
    @objc func offLineNotification(_ n: Notification) {
        DispatchQueue.main.async {
            QMUITips.hideAllTips()
            // 清空token
            HttpClient.default.tokenData = nil
            // 重置登录用户
            UserCenter.default.me = User()
            // 断开socket
            FEWebSocketManager.destroyWebSocket()
            // 关闭mmkv
            MMKVManager.shared.destory()
    
            
            guard let info = n.userInfo else {
                dprint("userInfo为空，是用户主动退出登录事件")
                self.switchRootController()
                return
            }
            
            let title = "登录异常"
            var text = "您的账号已在其他设备登录，请重新登录"
            if let item = info[Notification.Name.onForceOffline.rawValue] {
                if let wemsg = item as? WEMessage {
                    // 后台返回401 || 402 || 403
                    text = wemsg.text
                } else if let content = item as? String {
                    // socket推送"logout"
                    text = content
                }
            }
            
            let ac = UIAlertController(title: title, message: text, preferredStyle: .alert)
            let action = UIAlertAction(title: "知道了", style: .default) {[weak self] (a) in
                self?.switchRootController()
            }
            ac.addAction(action)
            self.present(ac)
        }
    }
    
    private func switchRootController() {
        UIApplication.shared.switchRootViewController(YYNavigationViewController(rootViewController: FELoginViewController()))
    }
    
    //MARK: 好友消息数提醒通知
    @objc func addressBookMessageTipsNotification() {
        let nav = self.vcs[1]
        
        let count = MessageTipsManager.shared.msgCount()
        if count == 0 {
            nav.tabBarItem.badgeValue = nil
        } else {
            nav.tabBarItem.badgeValue = "\(count)"
            nav.tabBarItem.badgeColor = Color.red
        }
    }
}
