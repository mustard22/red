//
//  UIImageViewExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Kingfisher
import QMUIKit

extension UIImageView {
    
    public func setImage(with urlStr: String,size: CGSize? = nil,mode: Int = 0) {
        if  var rowStr = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.removingPercentEncoding?.appending("?imageView2/format/yjpeg") {
            if let rectSize = size {
                rowStr = rowStr.appending("/\(max(mode, 1))/w/\(ceil(UIScreen.main.scale * rectSize.width))/h/\(ceil(UIScreen.main.scale * rectSize.width))")
            }
            
            self.setImage(with: URL(string: urlStr))
        }else {
            self.image = nil
        }
    }
    
    public func setImage(with url: URL? = nil, placeHolder: UIImage? = nil) {
        if url == nil && placeHolder == nil {
            return
        }
        
        self.kf.setImage(with: url, placeholder: placeHolder, options: [.transition(.fade(0.1))])
    }
}

public extension ImageCache {
    var diskCacheSize: UInt {
        let cache = self
        let diskCacheURL = URL(fileURLWithPath: cache.diskStorage.directoryURL.absoluteString)
        let resourceKeys: Set<URLResourceKey> = [.isDirectoryKey, .contentAccessDateKey, .totalFileAllocatedSizeKey]
        var diskCacheSize: UInt = 0
        for fileUrl in (try? FileManager.default.contentsOfDirectory(at: diskCacheURL, includingPropertiesForKeys: Array(resourceKeys), options: .skipsHiddenFiles)) ?? [] {
            do {
                let resourceValues = try fileUrl.resourceValues(forKeys: resourceKeys)
                if let fileSize = resourceValues.totalFileAllocatedSize {
                    diskCacheSize += UInt(fileSize)
                }
            } catch {}
        }
        return diskCacheSize
    }
    
    var displayDiskCacheSize: String {
        return ByteCountFormatter.string(fromByteCount: Int64(diskCacheSize),
                                         countStyle: ByteCountFormatter.CountStyle.file)
    }
}
