//
//  UnsafeMutablePointerExtensions.swift
//  Red
//
//  Created by MAC on 2020/5/26.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

extension UnsafeMutablePointer {
    public static func unsafeMutablePointer<T>(item: inout T) -> UnsafeMutablePointer<T> {
        return withUnsafeMutablePointer(to: &item) { (unsafe) -> UnsafeMutablePointer<T> in
            return unsafe
        }
    }
}
