//
//  CommonFile.swift
//  Red
//
//  Created by MAC on 2019/11/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
//MARK: 屏幕相关
/// 导航栏高度
public let kNaviBarHeight : CGFloat = UIApplication.shared.statusBarFrame.height == 44 ? 88 : 64

/// 屏幕scale（相对于4.7的屏幕）
public let kScreenScale: CGFloat = UIScreen.main.bounds.size.width/375.0

public let kScreenSize = UIScreen.main.bounds.size

public let kIsIphoneX = UIApplication.shared.statusBarFrame.height == 44 ? true : false


//MARK: - ddprint
/// debug下的dprint
func dprint(_ item: @autoclosure () -> Any) {
    #if DEBUG
    print(item())
    #endif
}
