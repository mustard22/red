//
//  UITableViewExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import QMUIKit
//import IGListKit
public extension UITableView {
    
    func registerCell<T: UITableViewCell>(withClass name: T.Type) {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        register(T.self, forCellReuseIdentifier: identifier)
    }
    
    func register<T: UITableViewHeaderFooterView>(headerFooterViewClassWith name: T.Type) {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        register(T.self, forHeaderFooterViewReuseIdentifier: identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(withClass name: T.Type) -> T {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        guard let cell = dequeueReusableCell(withIdentifier: identifier) as? T else {
            fatalError("\(identifier)未注册")
        }
        return cell
    }
    
    func heightForCell<T: UITableViewCell>(withClass name: T.Type, for indexPath: IndexPath,configuration: @escaping (T) -> Void) -> CGFloat {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        return qmui_heightForCell(withIdentifier: identifier, cacheBy: indexPath, configuration: { (cell) in
            guard let cell = cell as? T else { return }
            configuration(cell)
        })
    }
    
    func dequeueReusableCell<T: UITableViewCell>(withClass name: T.Type, for indexPath: IndexPath) -> T {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        guard let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T else {
            fatalError("\(identifier)未注册")
        }
        if let qCell = cell as? QMUITableViewCell {
            qCell.parentTableView = self
        }
        return cell
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(withClass name: T.Type) -> T {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        guard let view = dequeueReusableHeaderFooterView(withIdentifier: identifier) as? T else {
            fatalError("\(identifier)未注册")
        }
        if let qView = view as? QMUITableViewHeaderFooterView {
            qView.parentTableView = self
        }
        return view
    }
    
    func heightForCell<T: UITableViewCell>(withClass name: T.Type,
                                           cacheBy indexPath: IndexPath,
                                           configuration: @escaping (T) -> Void) -> CGFloat {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        return self.qmui_heightForCell(withIdentifier: identifier, cacheBy: indexPath, configuration: { (cell) in
            if let cell = cell as? T {
                configuration(cell)
            }
        })
    }
    
    func heightForCell<T: UITableViewCell>(withClass name: T.Type,
                                           cacheBy key: String,
                                           configuration: @escaping (T) -> Void) -> CGFloat {
        var identifier = String(describing: name)
        if let themeName = ThemeManager.default.currentTheme?.name {
            identifier = "\(identifier)\(themeName)"
        }
        return self.qmui_heightForCell(withIdentifier: identifier, cacheByKey: key as NSCopying, configuration: { (cell) in
            if let cell = cell as? T {
                configuration(cell)
            }
        })
    }
    
//    func reload<C:Differentiable>(from old: [C],to new: [C],setData: ([C]) -> Void) {
//        let changeSet = StagedChangeset(source: old, target: new)
//        //        changeSet.forEach { (set) in
//        //            set.data
//        //        }
//        //        changeSet.flatMap({$0.data}).forEach({self.qmui_keyedHeightCache.invalidateHeight(forKey: "\($0.differenceIdentifier)" as NSCopying )})
//        reload(using: changeSet,
//               deleteSectionsAnimation: .fade,
//               insertSectionsAnimation: .fade,
//               reloadSectionsAnimation: .none,
//               deleteRowsAnimation: .fade,
//               insertRowsAnimation: .fade,
//               reloadRowsAnimation: .none, setData: setData)
//    }
    
    func refreshHeaderView() {
        guard let headerView = self.tableHeaderView else { return }
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        headerView.size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        tableHeaderView = headerView
    }
    
    func refreshFooterView() {
        guard let footerView = self.tableFooterView else { return }
        footerView.setNeedsLayout()
        footerView.layoutIfNeeded()
        footerView.size = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        tableFooterView = footerView
    }
    
    func reloadSections(sections: IndexSet, animationed: Bool = true, _ completion: (() -> Void)? = nil) {
        CATransaction.begin()
        self.beginUpdates()
        CATransaction.setCompletionBlock(completion)
        self.reloadSections(sections, with: animationed ? .fade : .none)
        self.endUpdates()
        CATransaction.commit()
    }
}

public extension UITableView {
    
    
    /// 改变所有 Cell 的选中状态
    ///
    /// - Parameter selected: 是否选中
    func changeAllCellsSelectStatuse(_ selected: Bool) {
        let sectionCount = numberOfSections
        guard sectionCount > 0 else { return }
        for section in 0..<sectionCount {
            let rowCount = numberOfRows(inSection: section)
            guard rowCount > 0 else { continue }
            for row in 0..<rowCount {
                if selected {
                    selectRow(at: IndexPath(row: row, section: section), animated: true, scrollPosition: .none)
                } else {
                    deselectRow(at: IndexPath(row: row, section: section), animated: true)
                }
            }
        }
    }
}
