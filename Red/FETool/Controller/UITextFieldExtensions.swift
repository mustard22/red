//
//  UITextFieldExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/20.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
public extension UITextField {
    
    
    /// 便捷初始化方法
    ///
    /// - Parameters:
    ///   - placeholder: 占位字符
    ///   - font: 字体
    ///   - color: 字体颜色
    convenience init(_ placeholder: String? = nil, font: UIFont = UIFont.systemFont(ofSize: 17), color: UIColor? = nil) {
        self.init(frame: CGRect.zero)
        self.font = font
        if let color = color {
            textColor = color
        }
        self.placeholder = placeholder
    }
    
    func changeClearButtonStyle() {
        //        guard let clearButton = self.value(forKey: "_clearButton") as? UIButton else { return }
    }
}
