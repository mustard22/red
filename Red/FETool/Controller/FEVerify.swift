//
//  FEVerify.swift
//  Red
//
//  Created by MAC on 2019/11/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

struct FEVerify {
    /// 邮箱验证
    static func email(_ text: String) -> Bool {
        guard text.count > 0 else {
            return false
        }
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: text)
    }
    
    /// 手机号验证
    static func phoneNumber(_ text: String) -> Bool {
        guard text.count > 0 else {
            return false
        }
        
        let MOBILE = "^1(3|4|5|6|7|8|9)\\d{9}$"
        let regextestmobile = NSPredicate(format: "SELF MATCHES %@", MOBILE)
        
        if regextestmobile.evaluate(with: text) {
            return true
        }
        
        return false
    }
}


extension FEVerify {
    /// 验证资金密码(好几处操作都需要验证资金密码 抽取出来验证)
    static func verifyFundPasswd(_ currentViewController: UIViewController?, _ completionHandler: ((_ status: Bool, _ pwd: String?, _ text: String?)->Void)?) {
        let ac = UIAlertController(title: "密码验证", message: nil, preferredStyle: .alert)
        let a1 = UIAlertAction(title: "确定", style: .default) {(action) in
            guard let input = (ac.textFields)?.first else {
                completionHandler?(false, nil, "程序异常")
                return
            }
            
            guard let pwd = input.text, pwd.count>0 else {
                completionHandler?(false, nil, "密码未输入")
                return
            }
            
            var item = CheckFundPasswdParam()
            item.fund_password = input.text
            UserCenter.default.checkFundPassword(item, { (status, msg) in
                completionHandler?(status, item.fund_password, msg)
            })
        }
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        ac.addAction(a1)
        ac.addAction(cancel)
        
        ac.addTextField { (tf) in
            tf.placeholder = "请输入资金密码"
            tf.isSecureTextEntry = true
        }
        
        if let vc = currentViewController {
            vc.present(ac, animated: true, completion: nil)
        } else {
            if let vc = UIApplication.shared.keyWindow?.rootViewController {
                vc.present(ac, animated: true, completion: nil)
            }
        }
    }
}
