//
//  ThemeManager.swift
//  Red
//
//  Created by MAC on 2019/8/15.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
extension Notification.Name {
    static let themeChanged = Notification.Name("we.theme.changed")
}


/// 应用主题管理
public class ThemeManager: ChangingThemeDelegate {
    public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        if let newThemeName = newTheme?.name {
            UserDefaults.Common.appTheme = newThemeName
        }
    }
    
    public static let `default` = ThemeManager()
    
    
    /// QMUI 配置
    public var configuration: QMUIConfiguration {
        return QMUIConfiguration.sharedInstance()!
    }
    
    /// 当前主题
    public var currentTheme: ThemeProtocol? {
        didSet {
            guard let newTheme = currentTheme
                else { return }
            guard oldValue?.name != newTheme.name else {
                return
            }
            currentTheme?.applyConfigurationTemplate()
            NotificationCenter.default.post(name: .themeChanged, object: self, userInfo: ["oldTheme": oldValue as Any,
                                                                                          "newTheme": newTheme])
        }
    }
    
    private init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleThemeChanged(notification:)),
                                               name: .themeChanged,
                                               object: nil)
    }
    
    @objc
    private func handleThemeChanged(notification: Notification) {
        let oldTheme = notification.userInfo?["oldTheme"] as? ThemeProtocol
        let newTheme = notification.userInfo?["newTheme"] as? ThemeProtocol
        self.themChanged(form: oldTheme, to: newTheme)
    }
    
}
