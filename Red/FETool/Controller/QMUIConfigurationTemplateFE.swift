//
//  QMUIConfigurationTemplate.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//
//


import QMUIKit

/// MBA大师主题样式配置
class QMUIConfigurationTemplateFE: NSObject, ThemeProtocol {

    /// 主题名
    var name: String {
        return "default"
    }

    /// 主题色
    var tintColor: UIColor {
        return #colorLiteral(red: 0.1254901961, green: 0.4, blue: 1, alpha: 1)
    }

    /// 第二主题色
    var secondaryColor: UIColor {
        return #colorLiteral(red: 0.1921568627, green: 0.568627451, blue: 1, alpha: 1)
    }

    /// 文本颜色
    var textColor: UIColor {
        return #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 1)
    }

    /// 次要文本颜色
    var text2Color: UIColor {
        return #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    }

    /// 更为次要文本颜色
    var text3Color: UIColor {
        return #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
    }

    /// 全局 'UIViewController'背景色
    var backgrountColor: UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }

    /// 列表标题颜色
    var listTitleColor: UIColor {
        return #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    }

    ///当返回 true 时，启动 App 的时候 QMUIConfiguration 会自动应用这份配置表。但启动 App 时自动应用的配置表最多只允许一份，如果有多份则其他的会被忽略，需要在某些时机手动应用
    func shouldApplyTemplateAutomatically() -> Bool {
        return true
    }

    /// 应用主题,使之生效
    func applyConfigurationTemplate() {
        let QMUICMI = ThemeManager.default.configuration
        QMUICMI.clearColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0) // 透明色
        QMUICMI.whiteColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // 白色
        QMUICMI.blackColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) // 黑色
        QMUICMI.grayColor = #colorLiteral(red: 0.7019607843, green: 0.7019607843, blue: 0.7019607843, alpha: 1) // 最长用的灰色
        QMUICMI.grayDarkenColor = #colorLiteral(red: 0.9450980392, green: 0.9607843137, blue: 0.9843137255, alpha: 1) // 深一点的灰色
        QMUICMI.grayLightenColor = #colorLiteral(red: 0.8274509804, green: 0.8274509804, blue: 0.8274509804, alpha: 1) // 浅一点的灰色
        QMUICMI.redColor = #colorLiteral(red: 0.9960784314, green: 0.3490196078, blue: 0.3019607843, alpha: 1) // 红色
        QMUICMI.greenColor = #colorLiteral(red: 0.2862745098, green: 0.8980392157, blue: 0.5843137255, alpha: 1) // 绿色
        
        QMUICMI.blueColor = #colorLiteral(red: 0.1529411765, green: 0.7411764706, blue: 0.9529411765, alpha: 1) // 蓝色
        QMUICMI.yellowColor = #colorLiteral(red: 1, green: 0.8117647059, blue: 0.2784313725, alpha: 1) // 黄色

        QMUICMI.linkColor = #colorLiteral(red: 0.2196078431, green: 0.4549019608, blue: 0.6705882353, alpha: 1)
        QMUICMI.disabledColor = #colorLiteral(red: 0.7960784314, green: 0.8156862745, blue: 0.862745098, alpha: 1)
        QMUICMI.backgroundColor = backgrountColor
        QMUICMI.maskDarkColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.35)
        QMUICMI.maskLightColor = #colorLiteral(red: 0.9961728454, green: 0.9902502894, blue: 1, alpha: 0.5)
        QMUICMI.separatorColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        QMUICMI.separatorDashedColor = #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 1)
        QMUICMI.placeholderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)

        // 测试用的颜色
        QMUICMI.testColorRed = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.3)
        QMUICMI.testColorGreen = #colorLiteral(red: 0, green: 1, blue: 0, alpha: 0.3)
        QMUICMI.testColorBlue = #colorLiteral(red: 0, green: 0, blue: 1, alpha: 0.3)

        // UIControlHighlightedAlpha : UIControl 系列控件在 highlighted 时的 alpha，默认用于 QMUIButton、 QMUINavigationTitleView
        QMUICMI.controlHighlightedAlpha = 1
        // UIControlDisabledAlpha : UIControl 系列控件在 disabled 时的 alpha，默认用于 QMUIButton
        QMUICMI.controlDisabledAlpha = 1
        // ButtonHighlightedAlpha : QMUIButton 在 highlighted 时的 alpha，不影响系统的 UIButton
        QMUICMI.buttonHighlightedAlpha = QMUICMI.controlHighlightedAlpha
        // ButtonDisabledAlpha : QMUIButton 在 disabled 时的 alpha，不影响系统的 UIButton
        QMUICMI.buttonDisabledAlpha = QMUICMI.controlDisabledAlpha
        // ButtonTintColor : QMUIButton 默认的 tintColor，不影响系统的 UIButton
        QMUICMI.buttonTintColor = tintColor
        // GhostButtonColorBlue : QMUIGhostButtonColorBlue 的颜色
        QMUICMI.ghostButtonColorBlue = QMUICMI.blueColor
        // GhostButtonColorRed : QMUIGhostButtonColorRed 的颜色
        QMUICMI.ghostButtonColorRed = QMUICMI.redColor
        // GhostButtonColorGreen : QMUIGhostButtonColorGreen 的颜色
        QMUICMI.ghostButtonColorGreen = QMUICMI.greenColor
        // GhostButtonColorGray : QMUIGhostButtonColorGray 的颜色
        QMUICMI.ghostButtonColorGray = QMUICMI.grayColor
        // GhostButtonColorWhite : QMUIGhostButtonColorWhite 的颜色
        QMUICMI.ghostButtonColorWhite = QMUICMI.whiteColor
        // FillButtonColorBlue : QMUIFillButtonColorBlue 的颜色
        QMUICMI.fillButtonColorBlue = QMUICMI.blueColor
        // FillButtonColorRed : QMUIFillButtonColorRed 的颜色
        QMUICMI.fillButtonColorRed = QMUICMI.redColor
        // FillButtonColorGreen : QMUIFillButtonColorGreen 的颜色
        QMUICMI.fillButtonColorGreen = QMUICMI.greenColor
        // FillButtonColorGray : QMUIFillButtonColorGray 的颜色
        QMUICMI.fillButtonColorGray = QMUICMI.grayColor
        // FillButtonColorWhite : QMUIFillButtonColorWhite 的颜色
        QMUICMI.fillButtonColorWhite = QMUICMI.whiteColor

        // TextFieldTintColor : QMUITextField、QMUITextView 的 tintColor，不影响 UIKit 的输入框
        QMUICMI.textFieldTintColor = nil
        // TextFieldTextInsets : QMUITextField 的内边距，不影响 UITextField
        QMUICMI.textFieldTextInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        // NavBarHighlightedAlpha : QMUINavigationButton 在 highlighted 时的 alpha
        QMUICMI.navBarHighlightedAlpha = 0.2
        // NavBarDisabledAlpha : QMUINavigationButton 在 disabled 时的 alpha
        QMUICMI.navBarDisabledAlpha = 0.2
        // NavBarButtonFont : QMUINavigationButtonTypeNormal 的字体（由于系统存在一些 bug，这个属性默认不对 UIBarButtonItem 生效）
        QMUICMI.navBarButtonFont = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.medium)
        // NavBarButtonFontBold : QMUINavigationButtonTypeBold 的字体
        QMUICMI.navBarButtonFontBold = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        // NavBarBackgroundImage : UINavigationBar 的背景图
//        QMUICMI.navBarBackgroundImage = UIImage.qmui_image(with: Color.white)
        // NavBarShadowImage : UINavigationBar.shadowImage，也即导航栏底部那条分隔线
        QMUICMI.navBarShadowImage = UIImage.qmui_image(with: UIColor.clear)
        // NavBarBarTintColor : UINavigationBar.barTintColor，也即背景色
        QMUICMI.navBarBarTintColor = QMUICMI.whiteColor
        // NavBarTintColor : QMUINavigationController.navigationBar 的 tintColor，也即导航栏上面的按钮颜色，由于 tintColor 不支持 appearance，所以这里只支持 QMUINavigationController
        QMUICMI.navBarTintColor = text2Color
        // NavBarTitleColor : UINavigationBar 的标题颜色，以及 QMUINavigationTitleView 的默认文字颜色
//        QMUICMI.navBarTitleColor = Color.text
        // NavBarTitleFont : UINavigationBar 的标题字体，以及 QMUINavigationTitleView 的默认字体
        QMUICMI.navBarTitleFont = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.medium)
        // NavBarLargeTitleColor : UINavigationBar 在大标题模式下的标题颜色，仅在 iOS 11 之后才有效
        QMUICMI.navBarLargeTitleColor = QMUICMI.navBarTitleColor
        // NavBarLargeTitleFont : UINavigationBar 在大标题模式下的标题字体，仅在 iOS 11 之后才有效
        QMUICMI.navBarLargeTitleFont = nil
        // NavBarBarBackButtonTitlePositionAdjustment : 导航栏返回按钮的文字偏移
        QMUICMI.navBarBackButtonTitlePositionAdjustment = UIOffset.zero
        // SizeNavBarBackIndicatorImageAutomatically : 是否要自动调整 NavBarBackIndicatorImage 的 size 为 (13, 21)
        QMUICMI.sizeNavBarBackIndicatorImageAutomatically = true
        // NavBarBackIndicatorImage : 导航栏的返回按钮的图片，图片尺寸建议为(13, 21)，否则最终的图片位置无法与系统原生的位置保持一致
        QMUICMI.navBarBackIndicatorImage = #imageLiteral(resourceName: "icon_nav_back")
        // NavBarCloseButtonImage : QMUINavigationButton 用到的 × 的按钮图片
        QMUICMI.navBarCloseButtonImage = #imageLiteral(resourceName: "icon_nav_close")
        // NavBarLoadingMarginRight : QMUINavigationTitleView 里左边 loading 的右边距
        QMUICMI.navBarLoadingMarginRight = 3
        // NavBarAccessoryViewMarginLeft : QMUINavigationTitleView 里右边 accessoryView 的左边距
        QMUICMI.navBarAccessoryViewMarginLeft = 5
        // NavBarActivityIndicatorViewStyle : QMUINavigationTitleView 里左边 loading 的主题
        QMUICMI.navBarActivityIndicatorViewStyle = UIActivityIndicatorView.Style.gray
        // NavBarAccessoryViewTypeDisclosureIndicatorImage : QMUINavigationTitleView 右边箭头的图片
        if let image = UIImage.qmui_image(with: .triangle, size: CGSize(width: 8, height: 5), tintColor: QMUICMI.whiteColor) {
            QMUICMI.navBarAccessoryViewTypeDisclosureIndicatorImage = image
        }

        QMUICMI.automaticCustomNavigationBarTransitionStyle = true

        // TabBarBackgroundImage : UITabBar 的背景图
        //    QMUICMI.tabBarBackgroundImage = UIImage.qmui_image(with: QMUICMI.whiteColor, size: CGSize(width: 1, height: 1), cornerRadius: 0)
        // TabBarBarTintColor : UITabBar 的 barTintColor
        QMUICMI.tabBarBarTintColor = QMUICMI.whiteColor
        // TabBarShadowImageColor : UITabBar 的 shadowImage 的颜色，会自动创建一张 1px 高的图片
        QMUICMI.tabBarShadowImageColor = nil
        // TabBarTintColor : UITabBar 的 tintColor
//        QMUICMI.tabBarTintColor = tintColor
        // TabBarItemTitleColor : 未选中的 UITabBarItem 的标题颜色
        QMUICMI.tabBarItemTitleColor = QMUICMI.grayColor
        // TabBarItemTitleColorSelected : 选中的 UITabBarItem 的标题颜色
        QMUICMI.tabBarItemTitleColorSelected =  tintColor
        QMUICMI.tabBarItemImageColorSelected = tintColor
        QMUICMI.tabBarItemImageColor = QMUICMI.grayColor
        // TabBarItemTitleFont : UITabBarItem 的标题字体
//        QMUICMI.tabBarItemTitleFont = nil
        UITabBar.appearance().isTranslucent = false
        // ToolBarHighlightedAlpha : QMUIToolbarButton 在 highlighted 状态下的 alpha
        QMUICMI.toolBarHighlightedAlpha = 0.4
        // ToolBarDisabledAlpha : QMUIToolbarButton 在 disabled 状态下的 alpha
        QMUICMI.toolBarDisabledAlpha = 0.4
        // ToolBarTintColor : UIToolbar 的 tintColor，以及 QMUIToolbarButton normal 状态下的文字颜色
        QMUICMI.toolBarTintColor = nil
        // ToolBarTintColorHighlighted : QMUIToolbarButton 在 highlighted 状态下的文字颜色
        QMUICMI.toolBarTintColorHighlighted = QMUICMI.toolBarTintColor?.withAlphaComponent(QMUICMI.toolBarHighlightedAlpha)
        // ToolBarTintColorDisabled : QMUIToolbarButton 在 disabled 状态下的文字颜色
        QMUICMI.toolBarTintColorDisabled = QMUICMI.toolBarTintColor?.withAlphaComponent(QMUICMI.toolBarDisabledAlpha)
        // ToolBarBackgroundImage : UIToolbar 的背景图
        QMUICMI.toolBarBackgroundImage = nil
        // ToolBarBarTintColor : UIToolbar 的 tintColor
        QMUICMI.toolBarBarTintColor = nil
        // ToolBarShadowImageColor : UIToolbar 的 shadowImage 的颜色，会自动创建一张 1px 高的图片
        QMUICMI.toolBarShadowImageColor = nil
        // ToolBarButtonFont : QMUIToolbarButton 的字体
        QMUICMI.toolBarButtonFont = nil

        // SearchBarTextFieldBackground : QMUISearchBar 里的文本框的背景颜色
//        QMUICMI.searchBarTextFieldBackground = Color.darkGray
        // SearchBarTextFieldBorderColor : QMUISearchBar 里的文本框的边框颜色
        QMUICMI.searchBarTextFieldBorderColor = nil
        // SearchBarBottomBorderColor : QMUISearchBar 底部分隔线颜色
//        QMUICMI.searchBarBottomBorderColor = nil
        // SearchBarBarTintColor : QMUISearchBar 的 barTintColor，也即背景色
//        QMUICMI.searchBarBarTintColor = Color.white
        // SearchBarTintColor : QMUISearchBar 的 tintColor，也即上面的操作控件的主题色
        QMUICMI.searchBarTintColor = QMUICMI.navBarTintColor
        // SearchBarTextColor : QMUISearchBar 里的文本框的文字颜色
        QMUICMI.searchBarTextColor = nil
        // SearchBarPlaceholderColor : QMUISearchBar 里的文本框的 placeholder 颜色
        QMUICMI.searchBarPlaceholderColor = #colorLiteral(red: 0.7333333333, green: 0.7411764706, blue: 0.768627451, alpha: 1)
        // SearchBarFont : QMUISearchBar 里的文本框的文字字体及 placeholder 的字体
        QMUICMI.searchBarFont = UIFont.systemFont(ofSize: 14)
        // SearchBarSearchIconImage : QMUISearchBar 里的放大镜 icon
        QMUICMI.searchBarSearchIconImage = #imageLiteral(resourceName: "icon_nav_search").qmui_imageResized(inLimitedSize: CGSize(width: 14, height: 14))
        // SearchBarClearIconImage : QMUISearchBar 里的文本框输入文字时右边的清空按钮的图片
        QMUICMI.searchBarClearIconImage = nil
        // SearchBarTextFieldCornerRadius : QMUISearchBar 里的文本框的圆角大小
        QMUICMI.searchBarTextFieldCornerRadius = 14.0
        
        UICollectionView.appearance().showsHorizontalScrollIndicator = false
        UICollectionView.appearance().showsVerticalScrollIndicator = false

        // TableViewEstimatedHeightEnabled : 是否要开启全局 UITableView 的 estimatedRow(Section/Footer)Height
        QMUICMI.tableViewEstimatedHeightEnabled = true
        // TableViewBackgroundColor : Plain 类型的 QMUITableView 的背景色颜色
        QMUICMI.tableViewBackgroundColor = backgrountColor
        // TableViewGroupedBackgroundColor : Grouped 类型的 QMUITableView 的背景色
        QMUICMI.tableViewGroupedBackgroundColor = backgrountColor
        // TableSectionIndexColor : 列表右边的字母索引条的文字颜色
        QMUICMI.tableSectionIndexColor = nil
        // TableSectionIndexBackgroundColor : 列表右边的字母索引条的背景色
        QMUICMI.tableSectionIndexBackgroundColor = nil
        // TableSectionIndexTrackingBackgroundColor : 列表右边的字母索引条在选中时的背景色
        QMUICMI.tableSectionIndexTrackingBackgroundColor = nil
        // TableViewSeparatorColor : 列表的分隔线颜色
        QMUICMI.tableViewSeparatorColor = UIColor.clear
        // TableViewCellNormalHeight : QMUITableView 的默认 cell 高度
        QMUICMI.tableViewCellNormalHeight = UITableView.automaticDimension
        // TableViewCellTitleLabelColor : QMUITableViewCell 的 textLabel 的文字颜色
//        QMUICMI.tableViewCellTitleLabelColor = Color.text
        // TableViewCellDetailLabelColor : QMUITableViewCell 的 detailTextLabel 的文字颜色
//        QMUICMI.tableViewCellDetailLabelColor = Color.text
        // TableViewCellBackgroundColor : QMUITableViewCell 的背景色
        QMUICMI.tableViewCellBackgroundColor = QMUICMI.whiteColor
        // TableViewCellSelectedBackgroundColor : QMUITableViewCell 点击时的背景色
        QMUICMI.tableViewCellSelectedBackgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
        // TableViewCellWarningBackgroundColor : QMUITableViewCell 用于表示警告时的背景色，备用
        QMUICMI.tableViewCellWarningBackgroundColor = QMUICMI.yellowColor
        // TableViewCellDisclosureIndicatorImage : QMUITableViewCell 当 accessoryType 为 UITableViewCellAccessoryDisclosureIndicator 时的箭头的图片
        QMUICMI.tableViewCellDisclosureIndicatorImage = nil
        // TableViewCellCheckmarkImage : QMUITableViewCell 当 accessoryType 为 UITableViewCellAccessoryCheckmark 时的打钩的图片
        QMUICMI.tableViewCellCheckmarkImage = nil
        // TableViewCellDetailButtonImage : QMUITableViewCell 当 accessoryType 为 UITableViewCellAccessoryDetailButton 或 UITableViewCellAccessoryDetailDisclosureButton 时右边的 i 按钮图片
        QMUICMI.tableViewCellDetailButtonImage = nil
        // TableViewCellSpacingBetweenDetailButtonAndDisclosureIndicator : 列表 cell 右边的 i 按钮和向右箭头之间的间距（仅当两者都使用了自定义图片并且同时显示时才生效）
        QMUICMI.tableViewCellSpacingBetweenDetailButtonAndDisclosureIndicator = 12
        // TableViewSectionHeaderBackgroundColor : Plain 类型的 QMUITableView sectionHeader 的背景色
//        QMUICMI.tableViewSectionHeaderBackgroundColor = Color.white
        // TableViewSectionFooterBackgroundColor : Plain 类型的 QMUITableView sectionFooter 的背景色
//        QMUICMI.tableViewSectionFooterBackgroundColor = Color.white
        // TableViewSectionHeaderFont : Plain 类型的 QMUITableView sectionHeader 里的文字字体
        QMUICMI.tableViewSectionHeaderFont = UIFont.systemFont(ofSize: 12, weight: .bold)
        // TableViewSectionFooterFont : Plain 类型的 QMUITableView sectionFooter 里的文字字体
        QMUICMI.tableViewSectionFooterFont = UIFont.systemFont(ofSize: 12, weight: .bold)
        // TableViewSectionHeaderTextColor : Plain 类型的 QMUITableView sectionHeader 里的文字颜色
        QMUICMI.tableViewSectionHeaderTextColor = self.textColor
        // TableViewSectionFooterTextColor : Plain 类型的 QMUITableView sectionFooter 里的文字颜色
        QMUICMI.tableViewSectionFooterTextColor = QMUICMI.grayColor
        // TableViewSectionHeaderAccessoryMargins : Plain 类型的 QMUITableView sectionHeader accessoryView 的间距
        QMUICMI.tableViewSectionHeaderAccessoryMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        // TableViewSectionFooterAccessoryMargins : Plain 类型的 QMUITableView sectionFooter accessoryView 的间距
        QMUICMI.tableViewSectionFooterAccessoryMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        // TableViewSectionHeaderContentInset : Plain 类型的 QMUITableView sectionHeader 里的内容的 padding
        QMUICMI.tableViewSectionHeaderContentInset = UIEdgeInsets(top: 4, left: 15, bottom: 4, right: 15)
        // TableViewSectionFooterContentInset : Plain 类型的 QMUITableView sectionFooter 里的内容的 padding
        QMUICMI.tableViewSectionFooterContentInset = UIEdgeInsets(top: 4, left: 15, bottom: 4, right: 15)
        // TableViewGroupedSectionHeaderFont : Grouped 类型的 QMUITableView sectionHeader 里的文字字体
        QMUICMI.tableViewGroupedSectionHeaderFont = UIFont.systemFont(ofSize: 12)
        // TableViewGroupedSectionFooterFont : Grouped 类型的 QMUITableView sectionFooter 里的文字字体
        QMUICMI.tableViewGroupedSectionFooterFont = UIFont.systemFont(ofSize: 12)
        // TableViewGroupedSectionHeaderTextColor : Grouped 类型的 QMUITableView sectionHeader 里的文字颜色
        QMUICMI.tableViewGroupedSectionHeaderTextColor = QMUICMI.grayDarkenColor
        // TableViewGroupedSectionFooterTextColor : Grouped 类型的 QMUITableView sectionFooter 里的文字颜色
        QMUICMI.tableViewGroupedSectionFooterTextColor = QMUICMI.grayColor
        // TableViewGroupedSectionHeaderAccessoryMargins : Grouped 类型的 QMUITableView sectionHeader accessoryView 的间距
        QMUICMI.tableViewGroupedSectionHeaderAccessoryMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        // TableViewGroupedSectionFooterAccessoryMargins : Grouped 类型的 QMUITableView sectionFooter accessoryView 的间距
        QMUICMI.tableViewGroupedSectionFooterAccessoryMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        // TableViewGroupedSectionHeaderDefaultHeight : Grouped 类型的 QMUITableView sectionHeader 的默认高度（也即没使用自定义的 sectionHeaderView 时的高度），注意如果不需要间距，请用 CGFLOAT_MIN
        QMUICMI.tableViewGroupedSectionHeaderDefaultHeight = UITableView.automaticDimension
        // TableViewGroupedSectionFooterDefaultHeight : Grouped 类型的 QMUITableView sectionFooter 的默认高度（也即没使用自定义的 sectionFooterView 时的高度），注意如果不需要间距，请用 CGFLOAT_MIN
        QMUICMI.tableViewGroupedSectionFooterDefaultHeight = UITableView.automaticDimension
        // TableViewGroupedSectionHeaderContentInset : Grouped 类型的 QMUITableView sectionHeader 里的内容的 padding
        QMUICMI.tableViewGroupedSectionHeaderContentInset = UIEdgeInsets(top: 16, left: 15, bottom: 8, right: 15)
        // TableViewGroupedSectionFooterContentInset : Grouped 类型的 QMUITableView sectionFooter 里的内容的 padding
        QMUICMI.tableViewGroupedSectionFooterContentInset = UIEdgeInsets(top: 8, left: 15, bottom: 2, right: 15)

        // UIWindowLevelQMUIAlertView : QMUIModalPresentationViewController、QMUIPopupContainerView 里使用的 UIWindow 的 windowLevel
        QMUICMI.windowLevelQMUIAlertView = UIWindow.Level.alert.rawValue - 4.0
        // UIWindowLevelQMUIImagePreviewView : QMUIImagePreviewViewController 里使用的 UIWindow 的 windowLevel

        // ShouldprintDefaultLog : 是否允许输出 QMUILogLevelDefault 级别的 log
        QMUICMI.shouldPrintDefaultLog = false
        // ShouldprintInfoLog : 是否允许输出 QMUILogLevelInfo 级别的 log
        QMUICMI.shouldPrintInfoLog = false
        // ShoulddprintInfoLog : 是否允许输出 QMUILogLevelWarn 级别的 log
        QMUICMI.shouldPrintWarnLog = false

        // BadgeBackgroundColor : UIBarButtoItem、UITabBarItem 上的未读数的背景色
        QMUICMI.badgeBackgroundColor = QMUICMI.redColor
        // BadgeTextColor : UIBarButtoItem、UITabBarItem 上的未读数的文字颜色
        QMUICMI.badgeTextColor = QMUICMI.whiteColor
        // BadgeFont : UIBarButtoItem、UITabBarItem 上的未读数的字体
        QMUICMI.badgeFont = UIFont.systemFont(ofSize: 11, weight: .bold)
        // BadgeContentEdgeInsets : UIBarButtoItem、UITabBarItem 上的未读数与圆圈之间的 padding
        QMUICMI.badgeContentEdgeInsets = UIEdgeInsets(top: 2, left: 4, bottom: 2, right: 4)
        // BadgeCenterOffset : UIBarButtoItem、UITabBarItem 上的未读数相对于 item 中心的偏移
        QMUICMI.badgeCenterOffset = CGPoint(x: 10, y: -9)
        // BadgeCenterOffsetLandscape : UIBarButtoItem、UITabBarItem 上的未读数在横屏下相对于 item 中心的偏移
        QMUICMI.badgeCenterOffsetLandscape = CGPoint(x: 10, y: -9)
        // UpdatesIndicatorColor : UIBarButtoItem、UITabBarItem 上的未读红点的颜色
        QMUICMI.updatesIndicatorColor = QMUICMI.redColor
        // UpdatesIndicatorSize : UIBarButtoItem、UITabBarItem 上的未读红点的大小
        QMUICMI.updatesIndicatorSize = CGSize(width: 7, height: 7)
        // UpdatesIndicatorCenterOffset : UIBarButtoItem、UITabBarItem 上的未读红点相对于 item 中心的偏移
        QMUICMI.updatesIndicatorCenterOffset = CGPoint(x: 10, y: -9)
        // UpdatesIndicatorCenterOffsetLandscape : UIBarButtoItem、UITabBarItem 上的未读红点在横屏下相对于 item 中心的偏移
        QMUICMI.updatesIndicatorCenterOffsetLandscape = CGPoint(x: 10, y: -9)
        // SupportedOrientationMask : 默认支持的横竖屏方向
        QMUICMI.supportedOrientationMask = .portrait
        // AutomaticallyRotateDeviceOrientation : 是否在界面切换或 viewController.supportedOrientationMask 发生变化时自动旋转屏幕
        QMUICMI.automaticallyRotateDeviceOrientation = true
        // StatusbarStyleLightInitially : 默认的状态栏内容是否使用白色，默认为 NO，也即黑色
        QMUICMI.statusbarStyleLightInitially = false
        // NeedsBackBarButtonItemTitle : 全局是否需要返回按钮的 title，不需要则只显示一个返回image
        QMUICMI.needsBackBarButtonItemTitle = false
        // HidesBottomBarWhenPushedInitially : QMUICommonViewController.hidesBottomBarWhenPushed 的初始值，默认为 NO，以保持与系统默认值一致，但通常建议改为 YES，因为一般只有 tabBar 首页那几个界面要求为 NO
        QMUICMI.hidesBottomBarWhenPushedInitially = true
        // PreventConcurrentNavigationControllerTransitions : 自动保护 QMUINavigationController 在上一次 push/pop 尚未结束的时候就进行下一次 push/pop 的行为，避免产生 crash
        QMUICMI.preventConcurrentNavigationControllerTransitions = false
        // NavigationBarHiddenInitially : QMUINavigationControllerDelegate preferredNavigationBarHidden 的初始值，默认为NO
        QMUICMI.navigationBarHiddenInitially = false
        // ShouldFixTabBarTransitionBugInIPhoneX : 是否需要自动修复 iOS 11 下，iPhone X 的设备在 push 界面时，tabBar 会瞬间往上跳的 bug
        QMUICMI.shouldFixTabBarTransitionBugInIPhoneX = true
        // ShouldPrintQMUIWarnLogToConsole : 是否在出现 QMUILogWarn 时自动把这些 log 以 QMUIConsole 的方式显示到设备屏幕上
        QMUICMI.shouldPrintQMUIWarnLogToConsole = true
        applyAlertStyle()
        applySwitchStyle()
        applyEmptyViewStyle()
        applyMoreOperationStyle()
        applyDialog()
        ThemeManager.default.currentTheme = self
    }
}

extension QMUIConfigurationTemplateFE {
    // MARK: - 组件样式配置
    ///弹出框样式配置
    func applyDialog() {
        let style = QMUIDialogViewController.appearance()
        style.headerViewBackgroundColor = UIColor.white
        style.footerViewBackgroundColor = UIColor.white
        style.headerSeparatorColor = UIColor.clear
        style.titleLabelFont = UIFont.systemFont(ofSize: 18)
        style.cornerRadius = 10
        style.headerViewHeight = 45
        style.footerViewHeight = 50
        style.footerSeparatorColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        style.buttonTitleAttributes = [.font: UIFont.systemFont(ofSize: 18),
                                       .foregroundColor: tintColor]
    }

    ///空页面样式配置
    func applyEmptyViewStyle() {
        let style = QMUIEmptyView.appearance()
        style.textLabelFont = UIFont.systemFont(ofSize: 20, weight: .bold)
        style.detailTextLabelFont = UIFont.systemFont(ofSize: 15)
        style.detailTextLabelTextColor = self.text3Color
        style.actionButtonFont = UIFont.systemFont(ofSize: 13, weight: .bold)
        style.actionButtonTitleColor = UIColor.white
        style.backgroundColor = UIColor.white
    }

    ///开关控件样式配置
    func applySwitchStyle() {
//        UISwitch.appearance().tintColor = Color.text3
        UISwitch.appearance().onTintColor = self.tintColor
//        UISwitch.appearance().thumbTintColor = Color.text3
    }

    ///提示框样式配置
    func applyAlertStyle() {
        let style = QMUIAlertController.appearance()

        style.alertCancelButtonAttributes = [.font: UIFont.systemFont(ofSize: 18),
                                             .foregroundColor: self.textColor]
        style.alertButtonAttributes = [.font: UIFont.systemFont(ofSize: 18),
                                       .foregroundColor: self.tintColor]
//        style.alertDestructiveButtonAttributes = [.font: UIFont.systemFont(ofSize: 18),
//                                                  .foregroundColor: Color.red]
//        style.alertSeparatorColor = Color.separator
        style.alertContentCornerRadius = 10
//        style.sheetContentCornerRadius = 10
        style.alertTitleMessageSpacing = 9
        style.alertButtonHeight = 50
        style.alertTextFieldFont = UIFont.systemFont(ofSize: 14)
//        style.alertTextFieldTextColor = Color.text
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.center
        style.alertTitleAttributes = [.font: UIFont.systemFont(ofSize: 18),
                                      .foregroundColor: self.textColor,
                                      .paragraphStyle: paragraph]
        style.alertMessageAttributes = [.font: UIFont.systemFont(ofSize: 15),
                                        .foregroundColor: self.text2Color,
                                        .paragraphStyle: paragraph]

        style.sheetButtonHeight = 50
        style.sheetCancelButtonMarginTop = 0
        style.sheetContentCornerRadius = 0
        style.sheetContentMargin = UIEdgeInsets.zero
        style.isExtendBottomLayout = true
        style.sheetButtonAttributes = [.font: UIFont.systemFont(ofSize: 16),
                                       .foregroundColor: self.text2Color]
        style.sheetCancelButtonAttributes = [.font: UIFont.systemFont(ofSize: 16),
                                             .foregroundColor: self.text3Color]
    }

    ///底部分享样式配置
    func applyMoreOperationStyle() {
        let style = QMUIMoreOperationController.appearance()
//        style.contentEdgeMargin = 0
        style.contentCornerRadius = 0
        style.contentBackgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9764705882, alpha: 1)
        style.isExtendBottomLayout = true
        style.cancelButtonHeight = 50
        style.cancelButtonFont = UIFont.systemFont(ofSize: 17)
        style.itemTitleColor = self.text2Color
        style.contentBackgroundColor = UIColor.white
        style.cancelButtonBackgroundColor = style.contentBackgroundColor
        style.cancelButtonSeparatorColor = UIColor.clear
        style.cancelButtonTitleColor = self.text3Color
        style.scrollViewSeparatorColor = #colorLiteral(red: 0.8666666667, green: 0.8666666667, blue: 0.8666666667, alpha: 1)
        style.contentMaximumWidth = 768
    }
}
