//
//  AppShare.swift
//  Red
//
//  Created by MAC on 2019/11/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

public enum ShareType: Int, CaseIterable {
    case circle = 0
    case wechat
    case system
    
    typealias rawValue = Int
    
    public var name: String {
        switch self {
        case .circle: return "动态"
        case .wechat: return "微信"
        case .system: return "系统"
        }
    }
    
    public var imageUrl: String {
        switch self {
        case .circle: return "ic_share_circle"
        case .wechat: return "ic_share_wx"
        case .system: return "ic_share_sys"
        }
    }
}

//MARK: 分享view
class ShareView: UIView {
    
    private lazy var backButton: UIButton = {
        let v = UIButton()
        v.backgroundColor = UIColor(white: 0, alpha: 0.3)
        v.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    private lazy var content: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        backButton.addSubview(v)
        return v
    }()
    
    // 分享类型
    private var sitems = ShareType.allCases
    // 分享model
    private var shareModel: AppShare?
    
    // 点击事件（需要外部去处理）
    var clickHandler: ((_ type: ShareType)->Void)?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: public methods
    init(types: [ShareType] = [.wechat, .system], shareModel: AppShare) {
        self.sitems = types
        self.shareModel = shareModel
        super.init(frame: .zero)
    }
    
    public func show() {
        let win = UIApplication.shared.keyWindow
        self.frame = win!.bounds
        
        self.createUI()
        
        win?.addSubview(self)
        
        self.content.top = backButton.bottom
        self.backButton.alpha = 0
        UIView.animate(withDuration: 0.2) {
            self.content.top = self.backButton.bottom - self.content.height
            self.backButton.alpha = 1
        }
    }
    
    //MARK: private methods
    private func createUI() {
        let hmargin: CGFloat = (kScreenSize.width - ItemView.size.width*CGFloat(sitems.count))/CGFloat(sitems.count+1)
        let vmargin: CGFloat = 15
        
        var rect = self.bounds
        backButton.frame = rect
        
        rect.size.height = vmargin + ItemView.size.height + vmargin
        rect.origin.y = backButton.height - rect.size.height
        content.frame = rect
        
        for (i, item) in sitems.enumerated() {
            let bv = ItemView(orgin: CGPoint(x: hmargin + CGFloat(i)*(ItemView.size.width+hmargin), y: vmargin))
            bv.refreshItem(item.imageUrl, item.name)
            content.addSubview(bv)
            
            bv.buttonActionHandler = {
                self.shareAction(item)
            }
        }
    }
    
    private func hide() {
        UIView.animate(withDuration: 0.2, animations: {
            self.content.top = self.backButton.bottom
            self.backButton.alpha = 0
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    @objc
    private func backButtonAction() {
        hide()
    }
    
   
    private func shareAction(_ type: ShareType) {
        switch type {
        case .circle:
            clickHandler?(type)
        case .wechat:
            if let m = shareModel {
                AppShare.shareWeixin(m)
            }
        case .system:
            if let m = shareModel {
                AppShare.shareSystem(m)
            }
        }
        
        hide()
    }
}


//MARK: 分享操作
struct AppShare {
    var text: String = ""
    var image: Data? = nil
    var url: String = ""
    
    static func shareWeixin(_ item: AppShare) {
        let res = SendMessageToWXReq()
        let obj = WXWebpageObject()
        obj.webpageUrl = item.url
        
        let msg = WXMediaMessage()
        msg.mediaObject = obj
        msg.description = item.text
        msg.thumbData = item.image
        
        res.message = msg
        WXApi.send(res)
    }
    
    static func shareSystem(_ item: AppShare) {
        let ac = UIActivityViewController(activityItems: [item.text, item.image as Any, item.url], applicationActivities: [])
        ac.excludedActivityTypes = [.message, .mail, .airDrop, .print, .postToFacebook, .postToTwitter, .postToWeibo]

         UIApplication.shared.keyWindow?.rootViewController?.present(ac, animated: true, completion: nil)
    }
}


