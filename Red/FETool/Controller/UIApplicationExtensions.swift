//
//  UIApplicationExtension.swift
//  Red
//
//  Created by MAC on 2019/8/8.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
extension UIApplication {
    /// 当前版本号
    class var appVersion: String {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    }
    
    /// 是否显示slider
    class var showSlider: Bool {
        let cversion = Self.appVersion
        guard let v = UserDefaults.Common.appVersion else {
            // 第一次安装
            UserDefaults.Common.appVersion = cversion
            return true
        }
        
        if v != cversion {
            // app 更新
            UserDefaults.Common.appVersion = cversion
            return true
        }
        
        return false
    }
    
    /// 切换主视图控制器
    ///
    /// - Parameters:
    ///   - rootViewController: 将要切换至的视图控制器
    ///   - completion: 切换回调
    func switchRootViewController(_ rootViewController: UIViewController,
                                  options: UIView.AnimationOptions = UIView.AnimationOptions.transitionCrossDissolve,
                                  completion: ((Bool) -> Void)? = nil) {
        guard let keyWindow = self.keyWindow else { return }
        UIView.transition(with: keyWindow,
                          duration: 1,
                          options: options,
                          animations: {
                            let oldState = UIView.areAnimationsEnabled
                            UIView.setAnimationsEnabled(false)
                            UIApplication.shared.keyWindow?.rootViewController = rootViewController
                            UIView.setAnimationsEnabled(oldState)
        },
                          completion: completion)
    }
}


//MARK: 第三方
fileprivate let kCustomAppInfo = "CustomAppInfo"
fileprivate let kCustomAppInfoAppidKey = "Appid"
fileprivate let kCustomAppInfoUniversalLink = "UniversalLinks"

extension AppDelegate: WXApiDelegate {
    // 注册第三方
    func registerThirdPlatform() {
        if let info = Bundle.main.object(forInfoDictionaryKey: kCustomAppInfo) as? Dictionary<String,String>, let appid = info[kCustomAppInfoAppidKey], let ulinks = info[kCustomAppInfoUniversalLink] {
            WXApi.registerApp(appid, universalLink: ulinks)
            dprint("--------------------\ninfo.plist 配置的wexin应用信息：\(appid), \(ulinks)\n---------------")
        }
        
        WXApi.startLog(by: WXLogLevel.detail) { (log) in
            dprint("微信log: \(log)")
        }
    }
    
    //MARK:
    func onReq(_ req: BaseReq) {
        
    }
    
    func onResp(_ resp: BaseResp) {
        if resp.isKind(of: SendAuthResp.self) {
            // login auth
            let res = resp as! SendAuthResp
            NotificationCenter.default.post(name: .checkWeixinAuth, object: self, userInfo: [NSNotification.Name.checkWeixinAuth: res])
        } else if resp.isKind(of: SendMessageToWXResp.self) {
            // share
            let res = resp as! SendMessageToWXResp
            NotificationCenter.default.post(name: .weixinShareResult, object: self, userInfo: [NSNotification.Name.weixinShareResult: res])
        }
    }
}


extension Notification.Name {
    /// 检测微信授权
    static let checkWeixinAuth = Notification.Name("checkWeixinAuth")
    
    /// 微信分享
    static let weixinShareResult = Notification.Name("weixinShareResult")
    
    /// 自建群好友邀请事件key
    static let ownerGroupFriendInvite = Notification.Name("ownerGroupFriendInvite")
    
    /// 刷新首页列表key
    static let refreshHomeOwnerGroupList = Notification.Name("refreshHomeOwnerGroupList")
    
    /// 刷新通讯录列表key
    static let refreshAddressBookFriendList = Notification.Name("refreshAddressBookFriendList")
}
