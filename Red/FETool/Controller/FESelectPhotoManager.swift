//
//  FESelectPhotoManager.swift
//  Red
//
//  Created by MAC on 2019/12/31.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 选择图片manager
final class FESelectPhotoManager: NSObject {
    private static var manager = FESelectPhotoManager()
    
    // 选择数量
    private var selCount = 1
    
    // 回传选择完毕的图片
    private var selectHandler: ((_ images: [UIImage])->Void)?
    
    // 发起调用的当前VC
    private var currentViewController: UIViewController!
}


//MARK: - static methods
extension FESelectPhotoManager {
    static func selectPhoto(with currentVC: UIViewController,  selectCount: Int = 1, completionHandler: ((_ image: [UIImage])->Void)?) {
        manager.selectHandler = completionHandler
        manager.currentViewController = currentVC
        
        manager.selCount = max(1, selectCount)
        
        manager.selectPhoto()
    }
    
    /// 根据输入的size压缩图片(image: 原图片，size:想要压缩的大小)
    static func scaleToSize(image: UIImage, size: CGSize) -> UIImage {
        // 得到图片上下文，指定绘制范围
        //UIGraphicsBeginImageContext(size);
        
        /*
         *  UIGraphicsBeginImageContextWithOptions(CGSize size, BOOL opaque, CGFloat scale)
         *  CGSize size：指定将来创建出来的bitmap的大小
         *  BOOL opaque：设置透明YES代表透明，NO代表不透明
         *  CGFloat scale：代表缩放,0代表不缩放
         *  创建出来的bitmap就对应一个UIImage对象
         *  为了不影响像素，将图片放大了2倍
         */
        UIGraphicsBeginImageContextWithOptions(size, false, 2.0)
        // 将图片按照指定大小绘制
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        // 从当前图片上下文中导出图片
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        // 当前图片上下文出栈
        UIGraphicsEndImageContext()
        // 返回新的改变大小后的图片
        return img
    }
    
    //MARK: 保存图片
    static func saveImage(_ image: UIImage, handler: ((_ error: NSError?)->Void)?) {
        // 检测权限
        manager.setupPhotoStatus { (allow) in
            if allow {
                let assetManager = QMUIAssetsManager()
                // 获取所有相册
                var groups = [QMUIAssetsGroup]()
                assetManager.enumerateAllAlbums(with: .onlyPhoto, using: { (group) in
                    if let g = group {
                        groups.append(g)
                    }
                })
                
                let appGroup = groups.first
                QMUIImageWriteToSavedPhotosAlbumWithAlbumAssetsGroup(image, appGroup) { (asset, err) in
                    if let e = err {
                        handler?(e as NSError)
                    } else {
                        handler?(nil)
                    }
                }
            }
        }
    }
}

//MARK: - select photo
extension FESelectPhotoManager: QMUIAlbumViewControllerDelegate, QMUIImagePickerViewControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate {
    //MARK: 设置相册权限
    private func setupPhotoStatus(handler: @escaping ((_ isAllowDowith: Bool)->Void)) {
        // 检测权限
        let status = QMUIAssetsManager.authorizationStatus()
        switch status {
        case .notAuthorized:
            DispatchQueue.main.async {
                let ac = QMUIAlertController(title: "相册授权未开启", message: "请在系统设置中开启相册授权", preferredStyle: .alert)
                let action = QMUIAlertAction(title: "知道了", style: .default, handler: nil)
                ac.addAction(action)
                ac.showWith(animated: true)
            }
            handler(false)
        case .notDetermined:
            QMUIAssetsManager.requestAuthorization { (res) in
                if res != .notAuthorized {
                    handler(false)
                } else {
                    handler(true)
                }
            }
        case .authorized: handler(true)
        default : break
        }
    }
    
    private func selectPhoto() {
        let actionsheet = UIAlertController(title: "选择照片", message: nil, preferredStyle: .actionSheet)
        let a1 = UIAlertAction(title: "相册", style: .default) { (action) in
            self.infoImage()
        }
        let a2 = UIAlertAction(title: "拍照", style: .default) { (action) in
            DispatchQueue.main.async {
                self.takePhoto()
            }
        }
        let a3 = UIAlertAction(title: "取消", style: .cancel) {(action) in
        }
        actionsheet.addAction(a1)
        actionsheet.addAction(a2)
        actionsheet.addAction(a3)
        
        currentViewController.present(actionsheet, animated: true, completion: nil)
    }
    
    // 从相册
    private func infoImage() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .denied, .restricted :
            // 没授权
            QMUITips.show(withText: "请前往设置中打开相册权限", in: self.currentViewController.view)
        case .notDetermined :
            // 不确定
            PHPhotoLibrary.requestAuthorization {[weak self] (status2) in
                guard let self = self else {
                    return
                }
                DispatchQueue.main.async {
                    if status2 == .authorized {
                        let vc = QMUIAlbumViewController(style: .plain)
                        vc.albumViewControllerDelegate = self
                        vc.contentType = .onlyPhoto
                        
                        let nav = QMUINavigationController(rootViewController: vc)
                        self.currentViewController.present(nav)
                    } else {
                        QMUITips.show(withText: "您已拒绝授权", in: self.currentViewController.view)
                    }
                }
            }
        case .authorized :
            // 已经授权
            let vc = QMUIAlbumViewController(style: .plain)
            vc.albumViewControllerDelegate = self
            vc.contentType = .onlyPhoto
            
            let nav = QMUINavigationController(rootViewController: vc)
            currentViewController.present(nav)
            
         default: break
        }
    }
    
    //MARK: 拍照
    private func takePhoto() {
        // 判断有无打开照相机的权限
        if self.cameraPermissions() {
            let picker = FETakePhotoViewController(nibName: "FETakePhotoViewController", bundle: nil)
            currentViewController.present(picker, animated: true, completion: nil)
            picker.didFinishHandler = {[weak self] (img) in
                guard let img = img else {
                    return
                }
                var image: UIImage = img
                if let data  = img.jpegData(compressionQuality: 1), let i = UIImage(data: data) {
                    image = i
//                    dprint("拍照图片(jpeg)size: \(data.count)")
//                    dprint("拍照图片(png)size: \(img.pngData()?.count)")
                }
                
                self?.selectHandler?([image])
            }
        } else {
            QMUITips.show(withText: "请前往设置中打开摄像头权限", in: self.currentViewController.view)
        }
    }
    
    // 判断相机访问权限
    private func cameraPermissions() -> Bool {
        let authStatus:AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if(authStatus == AVAuthorizationStatus.denied || authStatus == AVAuthorizationStatus.restricted) {
            return false
        }
        return true
    }
    
    // 判断相册访问权限
    private func PhotoLibraryPermissions() -> Bool {
        let library:PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if(library == PHAuthorizationStatus.denied || library == PHAuthorizationStatus.restricted){
            return false
        }
        return true
    }
    
    
    //MARK: 相册界面代理
    internal func imagePickerViewController(for albumViewController: QMUIAlbumViewController) -> QMUIImagePickerViewController {
        let picker = QMUIImagePickerViewController.init()
        picker.imagePickerViewControllerDelegate = self
        picker.allowsMultipleSelection = false
        if selCount > 1 {
            picker.allowsMultipleSelection = true
            picker.maximumSelectImageCount = UInt(selCount)
            picker.minimumSelectImageCount = 1
        }
        
        return picker
    }
    
    // 图片选择完毕
    internal func imagePickerViewController(_ imagePickerViewController: QMUIImagePickerViewController, didSelectImageWithImagesAsset imageAsset: QMUIAsset, afterImagePickerPreviewViewControllerUpdate imagePickerPreviewViewController: QMUIImagePickerPreviewViewController) {
       
        if selCount > 1 {
            return
        }
       
       // 单选时，点击collectionCell直接退出选择界面
        imagePickerViewController.navigationController!.dismiss(animated: true, completion: nil)
        
        self.selectHandler?([imageAsset.originImage()])
    }
    
    // 多选完成时调用
    func imagePickerViewController(_ imagePickerViewController: QMUIImagePickerViewController, didFinishPickingImageWithImagesAssetArray imagesAssetArray: NSMutableArray) {
        if let assets = imagesAssetArray as? [QMUIAsset] {
            self.selectHandler?(assets.map({$0.originImage()}))
        }
    }
    
    // 设置预览界面
    func imagePickerPreviewViewController(for imagePickerViewController: QMUIImagePickerViewController) -> QMUIImagePickerPreviewViewController {
        let vc = QMUIImagePickerPreviewViewController()
        return vc
    }
    
    // 取消图片选择
    internal func albumViewControllerDidCancel(_ albumViewController: QMUIAlbumViewController) {
        albumViewController.navigationController!.dismiss(animated: true, completion: nil)
    }
}
