//
//  UIViewControllerExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/10.
//  Copyright © 2019 MAC. All rights reserved.
//

import QMUIKit

public extension UIViewController {
    /// 如果视图是 presented 出来的可通过此方法快捷添加关闭按钮
    func setLeftCloseNavigationItemIfNeed(title: String?, action: Selector? = nil) {
        guard qmui_isPresented() else { return }
        guard title.isSome else {
            navigationItem.leftBarButtonItem = UIBarButtonItem.qmui_close(withTarget: self, action: action ?? #selector(handleCloseEvent(sender:)))
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem.qmui_item(withTitle: title,
                                                                     target: self,
                                                                     action: action ?? #selector(handleCloseEvent(sender:)))
    }
    
    /// 如果视图是 presented 出来的可通过此方法快捷添加关闭按钮
    func setRightCloseNavigationItemIfNeed(title: String?, action: Selector? = nil) {
        guard qmui_isPresented() else { return }
        guard title.isSome else {
            navigationItem.leftBarButtonItem = UIBarButtonItem.qmui_close(withTarget: self, action: action ?? #selector(handleCloseEvent(sender:)))
            return
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem.qmui_item(withTitle: title,
                                                                      target: self,
                                                                      action: action ?? #selector(handleCloseEvent(sender:)))
    }
    
    @objc
    func close(animated: Bool = true,completion: (() -> Void)? = nil) {
        self.view.endEditing(true)
        if let vc = self as? QMUIModalPresentationViewController {
            vc.hideWith(animated: animated) { (_) in
                completion?()
                
            }
        }else if qmui_isPresented() {
            dismiss(animated: animated, completion: completion)
        } else {
            if completion.isNone {
                navigationController?.popViewController(animated: animated)
            }else {
                navigationController?.qmui_popViewController(animated: animated, completion: completion)
            }
        }
    }
    
    /// 视图控制器跳转
    ///
    /// - Parameters:
    ///   - vc: 将要跳转到的 vc
    ///   - completion:  跳转后且数据加载完成时调用
    func push(to vc: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        let safePush = {
            if let navc = self as? UINavigationController {
                if completion.isNone {
                    navc.pushViewController(vc, animated: animated)
                }else {
                    navc.qmui_pushViewController(vc, animated: animated, completion: completion)
                }
            } else if let tabvc = self as? UITabBarController {
                tabvc.selectedViewController?.push(to: vc)
            } else if let navc = self.navigationController {
                if completion.isNone {
                    navc.pushViewController(vc, animated: animated)
                }else {
                    navc.qmui_pushViewController(vc, animated: animated, completion: completion)
                }
            } else {
                self.present(vc, animated: animated, completion: completion)
            }
        }
        if Thread.isMainThread {
            safePush()
        }else {
            DispatchQueue.main.async {
                safePush()
            }
        }
    }
    
    /// 模态弹出带有导航栏的视图控制器
    ///
    /// - Parameters:
    ///   - viewControllerToPresent: 将要被弹出的视图控制器
    ///   - animated: 是否带有动画
    func present(_ viewControllerToPresent: UIViewController, animated: Bool = true, isInNavigationController: Bool = false, completion: (() -> Void)? = nil) {
        if isInNavigationController {
            let navc = YYNavigationViewController(rootViewController: viewControllerToPresent)
            present(navc, animated: animated, completion: completion)
        }else {
            present(viewControllerToPresent, animated: animated, completion: completion)
        }
    }
}

// MARK: - Event
public extension UIViewController {
    @objc
     func handleCloseEvent(sender _: Any) {
        close()
    }
}

public extension UIViewController {
    
    var navigationBarBottom: CGFloat {
        return self.qmui_navigationBarMaxYInViewCoordinator
    }
    
    
    /// 当前可见视图控制器
    static var current: UIViewController? {
        return QMUIHelper.visibleViewController()
    }
    //    var safeAreaInsets: UIEdgeInsets {
    //        if  {
    //            <#code#>
    //        }
    //    }
}

