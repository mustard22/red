//
//  FriendMessageTips.swift
//  Red
//
//  Created by MAC on 2019/11/13.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

// 消息提醒manager
public struct MessageTipsManager {
    public enum TipsType {
        case group
        case friend
    }
    
//    var groupTips2: [Int: Int]  = [:]
//
//    // ["系统群":["雷房":0]]
//    var groupTypeTips: [String:[String:Int]] {
//        get {
//            return UserDefaults.Current.groupTypeMessageTips
//        }
//
//        set (new) {
//            UserDefaults.Current.groupTypeMessageTips = new
//        }
//    }
//
//    // ["groupId": num]
//    var groupTips: [String: Int] {
//        get {
//            return UserDefaults.Current.groupMessageTips
//        }
//
//        set (new) {
//            UserDefaults.Current.groupMessageTips = new
//        }
//    }
    
    // ["friendId": num]
//    var friendMsgTips: [String: Int] {
//        get {
//            return UserDefaults.Current.friendMsgTips
//        }
//
//        set (new) {
//            UserDefaults.Current.friendMsgTips = new
//        }
//    }
    
    var friendMsgTips: [FriendMsgTipsItem] {
        get {
            return MMKVManager.shared.getCache([FriendMsgTipsItem].self, MMKVCacheKey.friendMessageTips) ?? []
        }
        set (new) {
            MMKVManager.shared.setCache(new, MMKVCacheKey.friendMessageTips)
        }
    }
    
    var groupMsgTips: [GroupMsgTipsItem] {
        get {
            return MMKVManager.shared.getCache([GroupMsgTipsItem].self, MMKVCacheKey.groupMessageTips) ?? []
        }
        set (new) {
            MMKVManager.shared.setCache(new, MMKVCacheKey.groupMessageTips)
        }
    }
    
    
    static var shared = MessageTipsManager()
    
    
    public struct FriendMsgTipsItem: Codable {
        public var id = ""
        public var count = 0
    }
    
    public struct GroupMsgTipsItem: Codable {
        public var id: String = ""
        public var count: Int = 0
        
        /// 群组：1牛 2福利 3单雷 。。(RoomType.rawValue)
        public var type: String = ""
        
        /// 系统群 自建群 （FEGroupListCellModel2.SectionType.type）
        public var isSys: String = ""
    }
}

extension MessageTipsManager {
    static let friendMsgTipsNotificaiton = Notification.Name.init("friendMsgTipsNotificaiton")
    
    static let groupMsgTipsNotificaiton = Notification.Name.init("groupMsgTipsNotificaiton")
    
    static let groupTypeMsgTipsNotificaiton = Notification.Name.init("groupTypeMsgTipsNotificaiton")
    
}

//MARK: - public methods
extension MessageTipsManager {
    // 未读总数
    func msgCount(_ type: MessageTipsManager.TipsType = .friend) -> Int {
        
        var count = 0
        switch type {
        case .friend:
            // 目前只需要好友消息总数
            for (_,item) in friendMsgTips.enumerated() {
                count += item.count
            }
        default: break
        }
        return count
    }
    
    func isContains(id: String, _ type: MessageTipsManager.TipsType = .friend, _ handler: @escaping (((Bool,Int)))->Void) {
        DispatchQueue.global().async {
            var tuple = (false, 0)
            switch type {
            case .friend:
                if let m = self.friendMsgTips.first(where: {$0.id == id}) {
                    tuple = (true, m.count)
                }
            case .group:
                if let m = self.groupMsgTips.first(where: {$0.id == id}) {
                    tuple = (true, m.count)
                }
            }
            
            DispatchQueue.main.async {
                handler(tuple)
            }
        }
    }
    
    // 检查对应ID是否有提醒
    func isContains(_ id: String, _ type: MessageTipsManager.TipsType = .friend) -> (Bool, Int) {
        var tuple = (false, -1)
        switch type {
        case .friend:
            if let m = friendMsgTips.first(where: {$0.id == id}) {
                tuple = (true, m.count)
            }
        case .group:
            if let m = groupMsgTips.first(where: {$0.id == id}) {
                tuple = (true, m.count)
            }
        }
        return tuple
    }
    
    ///
    func msgCountOfGroupType(roomClass: String, isSys: String, _ handler: @escaping (Int)->Void) {
        DispatchQueue.global().async {
            var count = 0
            for (_, m) in self.groupMsgTips.enumerated() where (m.isSys == isSys && roomClass == m.type && m.count > 0) {
                count += m.count
            }
            DispatchQueue.main.async {
                handler(count)
            }
        }
    }
    
    mutating func addTips(_ msg: Message, _ type: MessageTipsManager.TipsType = .friend) {
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        DispatchQueue.global().async {
            var id = ""
            
            switch type {
            case .friend:
                id = msg.body.senderId
                if let index = unsafePointSelf.pointee.friendMsgTips.firstIndex(where: {$0.id == id}) {
                    unsafePointSelf.pointee.friendMsgTips[index].count += 1
                } else {
                    let item = FriendMsgTipsItem(id: id, count: 1)
                    unsafePointSelf.pointee.friendMsgTips += [item]
                }
                 unsafePointSelf.pointee.__dowithPostNotification(MessageTipsManager.friendMsgTipsNotificaiton)
            case .group:
                id = msg.body.groupId
                
                var item: GroupMsgTipsItem!
                if let index = unsafePointSelf.pointee.groupMsgTips.firstIndex(where: {$0.id == id}) {
                    item = unsafePointSelf.pointee.groupMsgTips[index]
                    unsafePointSelf.pointee.groupMsgTips.remove(at: index)
                } else {
                    item = GroupMsgTipsItem()
                    item.id = id
                    item.type = msg.body.group_class
                    item.isSys = FEGroupListCellModel2.SectionType.sys.type
                    if let double = Double(id), double >= 10000 {
                        item.isSys = FEGroupListCellModel2.SectionType.owner.type
                    }
                }
                
                item.count += 1
                unsafePointSelf.pointee.groupMsgTips += [item]
                unsafePointSelf.pointee.__dowithPostNotification(MessageTipsManager.groupMsgTipsNotificaiton, ["id":id])
                unsafePointSelf.pointee.__dowithPostNotification(MessageTipsManager.groupTypeMsgTipsNotificaiton, ["style":item.isSys, "class":item.type])
            }
        }
    }
    
    private func __dowithPostNotification(_ name: Notification.Name, _ info: [String: Any]? = nil) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: name, object: nil, userInfo: info)
        }
    }
    
    mutating func removeTips(_ id: String, _ type: MessageTipsManager.TipsType = .friend) {
        switch type {
        case .friend:
            if let index = friendMsgTips.firstIndex(where: {$0.id == id}) {
                friendMsgTips[index].count = 0
            }
        case .group:
            if let index = groupMsgTips.firstIndex(where: {$0.id == id}) {
                groupMsgTips[index].count = 0
            }
        }
    }
    
    mutating func removeAllTips(_ type: MessageTipsManager.TipsType = .friend) {
        switch type {
        case .friend:
            var tips = friendMsgTips
            tips = tips.map({ (m) -> FriendMsgTipsItem in
                var item = m
                item.count = 0
                return item
            })
            friendMsgTips = tips
        case .group:
            var tips = groupMsgTips
            tips = tips.map({ (m) -> GroupMsgTipsItem in
                var item = m
                item.count = 0
                return item
            })
            groupMsgTips = tips
        }
    }
}
