//
//  UserdefaultManager.swift
//  Red
//
//  Created by MAC on 2020/4/16.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

public enum UserdefaultKeyType {
    case common
    case user(uid: String)
    
    var name: String {
        switch self {
        case .common: return "common"
        case .user(let uid): return uid
        }
    }
}


extension JSONEncoder {
    func itemToData<T: Codable>(_ item: T) -> Data? {
        guard let d = try? self.encode(item) else {
            return nil
        }
        
        return d
    }
}

extension JSONDecoder {
    func dataToItem<T: Codable>(_ type: T.Type, _ data: Data?) -> T? {
        guard let d = data, let item = try? self.decode(type, from: d) else {
            return nil
        }
        return item
    }
}


public protocol UserDefaultsProtocol {
    static func value(for key: String) -> Any?
    
    static func setValue(with key: String, value: Any?)
    
    static func value2<T: Codable>(for key: String, type: T.Type) -> T?
    
    static func setValue2<T: Codable>(with key: String, value: T?)
}

extension UserDefaults {
    /// 公共
    public struct Common: UserDefaultsProtocol {
        public static func value2<T: Codable>(for key: String, type: T.Type) -> T? {
            guard let d = UserDefaults.value(type: .common, key: key) as? Data else {
                return nil
            }
            
            return JSONDecoder().dataToItem(type, d)
        }
        
        public static func setValue2<T: Codable>(with key: String, value: T?) {
            guard let item = value, let d = JSONEncoder().itemToData(item) else {
                UserDefaults.save(type: .common, key: key, value: nil)
                return
            }
            UserDefaults.save(type: .common, key: key, value: d)
        }
        
        public static func value(for key: String) -> Any? {
            return UserDefaults.value(type: .common, key: key)
        }
        
        public static func setValue(with key: String, value: Any?) {
            UserDefaults.save(type: .common, key: key, value: value)
        }
    }
    
    /// 当前登录用户
    public struct Current: UserDefaultsProtocol  {
        public static func value2<T: Codable>(for key: String, type: T.Type) -> T? {
            guard let d = UserDefaults.value(type: .user(uid: uid), key: key) as? Data else {
                return nil
            }
            
            return JSONDecoder().dataToItem(type, d)
        }
        
        public static func setValue2<T: Codable>(with key: String, value: T?) {
            guard let item = value, let d = JSONEncoder().itemToData(item) else {
                UserDefaults.save(type: .user(uid: uid), key: key, value: nil)
                return
            }
            UserDefaults.save(type: .user(uid: uid), key: key, value: d)
        }
        
        private static var uid: String {
            return User.default.user_id
        }
        
        public static func value(for key: String) -> Any? {
            let uid = Self.uid
            return UserDefaults.value(type: .user(uid: uid), key: key)
        }
        
        public static func setValue(with key: String, value: Any?) {
            let uid = Self.uid
            UserDefaults.save(type: .user(uid: uid), key: key, value: value)
        }
    }
    
    
    private static func getDictOfType(type: UserdefaultKeyType = .common) -> [String:Any] {
        let standard = UserDefaults.standard
        var dict: [String: Any] = [:]
        if let d = standard.dictionary(forKey: type.name) {
            dict = d
        }
        return dict
    }
    
    private static func save(type: UserdefaultKeyType = .common, key: String, value: Any?) {
        let standard = UserDefaults.standard
        
        var dict = UserDefaults.getDictOfType(type: type)
        dict[key] = value

        standard.set(dict, forKey: type.name)
        standard.synchronize()
    }
    
    private static func value(type: UserdefaultKeyType = .common, key: String) -> Any? {
        let dict = UserDefaults.getDictOfType(type: type)
        return dict[key]
    }
}

extension UserDefaults.Common {
    /// app version
    static var appVersion: String? {
        get {
            return Self.value(for: "appVersion") as? String
        }
        set (new) {
            Self.setValue(with: "appVersion", value: new)
        }
    }
    
    /// 第一次安装时的邀请码
    static var firstInstallInviteCode: String? {
        get {
            return Self.value(for: "firstInstallInviteCode") as? String
        }
        set (new) {
            Self.setValue(with: "firstInstallInviteCode", value: new)
        }
    }
    
    /// 好友的自建群邀请
    static var ownerGroupFriendInvite: String? {
        get {
            return Self.value(for: "ownerGroupFriendInvite") as? String
        }
        set (new) {
            Self.setValue(with: "ownerGroupFriendInvite", value: new)
        }
    }
    
    /// 第一次微信登录的默认密码
    static var firstWXLoginPasswd: String? {
        get {
            return Self.value(for: "firstWXLoginPasswd") as? String
        }
        set (new) {
            Self.setValue(with: "firstWXLoginPasswd", value: new)
        }
    }
    
    /// 应用主题名称
    static var appTheme: String? {
        get {
            return Self.value(for: "appTheme") as? String
        }
        set (new) {
            Self.setValue(with: "appTheme", value: new)
        }
    }
    
    /// token data
    static var tokenData: (String,String)? {
        get {
            guard let dict = Self.value2(for: "tokenData", type: [String:String].self), let key = dict["key"], let value = dict["value"] else {
                return nil
            }
            
            return (key, value)
        }
        set (new) {
            if let v = new {
                let dict = ["key":v.0, "value":v.1]
                Self.setValue2(with: "tokenData", value: dict)
            } else {
                Self.setValue(with: "tokenData", value: nil)
            }
        }
    }
    
    /// 上一次登录手机号
    static var lastLoginMobile: String? {
        get {
            
            return Self.value(for: "lastLoginMobile") as? String
        }
        set (new) {
            Self.setValue(with: "lastLoginMobile", value: new)
        }
    }
    
    /// 朋友圈模块开关
    static var circleSwitch: String? {
        get {
            return Self.value(for: "circleSwitch") as? String
        }
        set (new) {
            Self.setValue(with: "circleSwitch", value: new)
        }
    }
    
    /// 新用户奖励
    static var newUserMoney: String? {
        get {
            return Self.value(for: "newUserMoney") as? String
        }
        set (new) {
            Self.setValue(with: "newUserMoney", value: new)
        }
    }
    
    /// APP打开次数
    static var appOpenTime: Int {
        get {
            guard let time = Self.value(for: "appOpenTime") as? Int else {
                return 0
            }
            return time
        }
        set (new) {
            Self.setValue(with: "appOpenTime", value: new)
        }
    }
}

extension UserDefaults.Current {
    /// 该设备用户登录次数
    static var userLoginTime: Int {
        get {
            guard let time = Self.value(for: "userLoginTime") as? Int else {
                return 0
            }
            return time
        }
        set (new) {
            Self.setValue(with: "userLoginTime", value: new)
        }
    }
    
    /// 红包声音开关
    static var paperVoiceSwitch: Bool {
        get {
            if let v = Self.value(for: "paperVoiceSwitch") as? Bool {
                return v
            } else {
                // set default value = true
                Self.setValue(with: "paperVoiceSwitch", value: true)
            }
            return true
        }
        set (new) {
            Self.setValue(with: "paperVoiceSwitch", value: new)
        }
    }
    
    /// 自建群分享到圈子模块的分享记录(groupId: (Date, shareCount))
    static var shareOwnerGroupToCircle: [FEShareOwnerGroupCircle] {
        get {
            return Self.value2(for: "shareOwnerGroupToCircle", type: [FEShareOwnerGroupCircle].self) ?? []
        }
        set (new) {
            Self.setValue2(with: "shareOwnerGroupToCircle", value: new)
        }
    }
}


