//
//  ChatTableView.swift
//  Red
//
//  Created by MAC on 2019/12/31.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 聊天列表cell点击事件类型
enum ChatTableCellAction: Int {
    /// 点击图片
    case image
    /// 点击红包
    case sendPaper
    /// 点击转账
    case transfer
    /// 点击音频
    case voice
    /// 同意入群
    case ownerFriendInvite
}


//MARK: -
//MARK: -----------------------------
//MARK: ChatTableViewDelegate(聊天界面列表代理)
//MARK: -----------------------------
protocol ChatTableViewDelegate: AnyObject {
    /// cell点击事件
    func chatTable(_ chatTable: ChatTableView, cellAction: ChatTableCellAction, msg: Message)
    /// 单击列表
    func chatTableTapped()
    
    /// 开始拖拽
    func chatTableWillBeginDragging(_ chatTable: ChatTableView)
    
    /// 滑动手指离开屏幕
    func chatTableDidEndDecelerating(_ chatTable: ChatTableView)
}


//MARK: -
//MARK: -----------------------------
//MARK: ChatTableView(聊天界面列表)
//MARK: -----------------------------
class ChatTableView: QMUITableView {
    var dataArray: [Message] = []
    
    weak var CTVDelegate: ChatTableViewDelegate?
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        self.rowHeight = UITableView.automaticDimension
        self.estimatedRowHeight = 100
        self.delegate = self
        self.dataSource = self
        self.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.tableFooterView = UIView()
        self.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.registerAllCells()
        self.addTapGesture()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ChatTableView {
    // add single tap
    private func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(singleTap))
        self.addGestureRecognizer(tap)
    }
    
    // dowith tap gesture
    @objc func singleTap() {
        CTVDelegate?.chatTableTapped()
    }
    
    // register cells
    private func registerAllCells() {
        self.register(ChatTextCell.self, forCellReuseIdentifier: "ChatTextCell")
        self.register(ChatImageCell.self, forCellReuseIdentifier: "ChatImageCell")
        self.register(ChatSendPackageCell.self, forCellReuseIdentifier: "ChatSendPackageCell")
        self.register(ChatServerTipsCell.self, forCellReuseIdentifier: "ChatServerTipsCell")
        self.register(ChatFriendTransferCell.self, forCellReuseIdentifier: "ChatFriendTransferCell")
        self.register(ChatOwnerFriendInviteCell.self, forCellReuseIdentifier: "ChatOwnerFriendInviteCell")
        
    }
}

//MARK: - QMUITableViewDelegate, QMUITableViewDataSource
extension ChatTableView: QMUITableViewDelegate, QMUITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: BaseChatCell!
        let msg = dataArray[indexPath.row]
        
        var showTime = true
        
        switch msg.type {
        case MessageType.fetchPaper.rawValue, /*抢包消息*/
        MessageType.luckyMoney.rawValue, /*幸运奖励消息*/
        MessageType.getTransfer.rawValue, /*领取转账消息*/
        MessageType.thunderMoney.rawValue, /*中雷奖励*/
        MessageType.cattleLucky.rawValue, /*牛包房对局*/
        MessageType.comeGroupRoom.rawValue, /*进群欢迎语*/
        MessageType.ownerMuted.rawValue, /*禁言*/
        MessageType.ownerBlacklist.rawValue /*黑名单 踢群*/,
        MessageType.solitaireTipsAfterEnd.rawValue /* 接龙红包已抢完 */,
        MessageType.solitaireEnded.rawValue /* 接龙红包本轮结束*/:
            showTime = false
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatServerTipsCell") as! ChatServerTipsCell
            
        case MessageType.sendPaper.rawValue/*发包消息*/:
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatSendPackageCell") as! ChatSendPackageCell
            
        case MessageType.friendTransfer.rawValue/*好友转账消息*/:
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatFriendTransferCell") as! ChatFriendTransferCell
            
        case MessageType.ownerFriendInvite.rawValue/*自建群好友邀请消息*/:
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatOwnerFriendInviteCell") as! ChatOwnerFriendInviteCell
            
        default:
            if msg.body.content.hasPrefix("img:::http") || msg.body.imageData != nil {
                // image
                cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageCell") as! ChatImageCell
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "ChatTextCell") as! ChatTextCell
            }
        }
        
        cell.contentView.backgroundColor = tableView.backgroundColor
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.delegate = self
        
        // 检测是否显示时间(提示消息不显示时间)
        var timeString = ""
        if showTime {
            let upmsgTime = indexPath.row > 0 ? self.dataArray[indexPath.row - 1].body.sendTime : ""
            timeString = cell.updateTime(upmsgTime, msg.body.sendTime)
        }
        
        cell.messageTimeLabel.isHidden = timeString.isEmpty
        cell.messageTimeLabel.text = timeString
        cell.updateMessage(msg, idx: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // 检测语音是否结束
//        if let oldMessage = oldChatVoiceMessage {
//            if oldMessage.messageId == msg.messageId {
//                (cell as! ChatVoiceCell).messageAnimationVoiceImageView.startAnimating()
//            }
//        }

    }
}


// MARK: - UIScrollViewDelegate
extension ChatTableView: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        CTVDelegate?.chatTableWillBeginDragging(self)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        CTVDelegate?.chatTableDidEndDecelerating(self)
    }
}


//MARK: - BaseChatCellDelegate
extension ChatTableView: BaseChatCellDelegate {
    func epDidVoiceClick(_ message: Message) {
        CTVDelegate?.chatTable(self, cellAction: .voice, msg: message)
    }
    
    func epDidImageClick(_ message: Message, _ showImage: UIImage?) {
        if let img = showImage, message.body.imageData == nil {
            message.body.imageData = img.jpegData(compressionQuality: 0.8)
        }
        CTVDelegate?.chatTable(self, cellAction: .image, msg: message)
    }
    
    func epDidSendPackageClick(_ message: Message) {
        CTVDelegate?.chatTable(self, cellAction: .sendPaper, msg: message)
    }
    
    func epDidTransferClick(_ message: Message) {
        CTVDelegate?.chatTable(self, cellAction: .transfer, msg: message)
    }
    
    func epDidHandleOwnerFriendInvite(_ message: Message, _ isAgree: Bool) {
        message.body.friendInviteStatus = isAgree ? "1" : "2"
        CTVDelegate?.chatTable(self, cellAction: .ownerFriendInvite, msg: message)
    }
}
