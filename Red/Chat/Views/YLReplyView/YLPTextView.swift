//
//  YLPTextView.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit

class YLPTextView: UITextView {
}

extension YLPTextView: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.tintColor = Color.red
        return true
    }
}
