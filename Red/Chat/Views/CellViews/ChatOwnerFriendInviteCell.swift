//
//  ChatOwnerFriendInviteCell.swift
//  Red
//
//  Created by MAC on 2020/3/10.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation
import UIKit
import YYText

/// 自建群朋友邀请cell
class ChatOwnerFriendInviteCell: BaseChatCell {
    private lazy var backView: UIView = {
        let v = UIView()
        v.frame = .zero
        v.backgroundColor = .white
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var sepline: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        backView.addSubview(v)
        return v
    }()
    
    lazy var tipsLabel: UILabel = {
        let v = UILabel()
        v.numberOfLines = 0
        v.textColor = .black
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 13)
        backView.addSubview(v)
        return v
    }()
    
    lazy var typeLabel: YYLabel = {
        let v = YYLabel()
        v.numberOfLines = 0
        v.textColor = .red
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 15)
        backView.addSubview(v)
        return v
    }()
    
    @objc func buttonAction(_ sender: UIButton) {
        guard let msg = message else {
            return
        }
        
        if sender == agreeButton {
            delegate?.epDidHandleOwnerFriendInvite(msg, true)
        } else {
            delegate?.epDidHandleOwnerFriendInvite(msg, false)
        }
    }
    
    private lazy var agreeButton: UIButton = {
        let v = UIButton()
        v.setTitle("同意", for: .normal)
        v.setTitleColor(Color.green, for: .normal)
        v.titleLabel!.font = UIFont.systemFont(ofSize: 15)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        backView.addSubview(v)
        return v
    }()
    
    private lazy var rejectButton: UIButton = {
        let v = UIButton()
        v.setTitle("拒绝", for: .normal)
        v.setTitleColor(.red, for: .normal)
        v.titleLabel!.font = UIFont.systemFont(ofSize: 15)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        backView.addSubview(v)
        return v
    }()
   
    
    override func layoutUI() {
        isNeedBubbleBackground = false
        super.layoutUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func updateMessage(_ m: Message, idx: IndexPath) {
        super.updateMessage(m, idx: idx)
 
        DispatchQueue.global().async {
            let str = m.body.content
            let sections = str.components(separatedBy: ",")
            let attr = NSMutableAttributedString(string: sections.joined(separator: ""))
            var location = 0
            for (i, sub) in sections.enumerated() {
                // 偶数对应位置设置红色
                if i%2 != 0 && sub.count>0 {
                    let range = NSRange(location: location, length: sub.count)
                    attr.addAttributes([.foregroundColor: Color.red], range: range)
                }
                location += sub.count
            }
             
            DispatchQueue.main.async {
                self.tipsLabel.attributedText = attr
            }
        }
        
        
        if let status = m.body.friendInviteStatus {
            agreeButton.isHidden = true
            rejectButton.isHidden = true
            typeLabel.isHidden = false
            switch status {
            case "0":
                typeLabel.isHidden = true
                agreeButton.isHidden = false
                rejectButton.isHidden = false
            case "1":
                typeLabel.text = "已同意"
                typeLabel.textColor = Color.green
            case "2":
                typeLabel.text = "已拒绝"
                typeLabel.textColor = .red
            case "3":
                typeLabel.text = "已邀请"
                typeLabel.textColor = .black
            default: break
            }
        }
        
        setupLayout()
    }
    
    private func setupLayout() {
        if message?.direction == MessageDirection.send.rawValue {
            backView.snp.remakeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: (showTime ? 36 : 8)+messageUserNameLabel.height, left: YLScreenWidth-52-180, bottom: 8, right: 52))
                make.width.equalTo(180)
                make.height.equalTo(100)
            }
            
        } else {
            backView.snp.remakeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: (showTime ? 36 : 8)+messageUserNameLabel.height, left: 52, bottom: 8, right: YLScreenWidth-52-180))
                make.width.equalTo(180)
                make.height.equalTo(100)
            }
            
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(backView)
            make.bottom.equalTo(backView)
            make.height.equalTo(40)
        }
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(typeLabel)
            make.right.equalTo(typeLabel)
            make.bottom.equalTo(typeLabel.snp.top)
            make.height.equalTo(1)
        }
        
        tipsLabel.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(backView)
            make.bottom.equalTo(sepline.snp.top)
            make.top.equalTo(0)
        }
        
        agreeButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(typeLabel.snp.centerX)
            make.bottom.equalTo(typeLabel)
            make.height.equalTo(typeLabel)
        }
        
        rejectButton.snp.makeConstraints { (make) in
            make.left.equalTo(typeLabel.snp.centerX)
            make.right.equalTo(typeLabel)
            make.bottom.equalTo(typeLabel)
            make.height.equalTo(typeLabel)
        }
        
        layoutIfNeeded()
    }
}
