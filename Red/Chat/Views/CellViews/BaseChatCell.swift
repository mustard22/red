//
//  BaseChatCell.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
import YYText
import SnapKit

protocol BaseChatCellDelegate: NSObjectProtocol {
    func epDidVoiceClick(_ message: Message)
    func epDidImageClick(_ message: Message, _ showImage: UIImage?)
    func epDidSendPackageClick(_ message: Message)
    func epDidTransferClick(_ message: Message)
    func epDidHandleOwnerFriendInvite(_ message: Message, _ isAgree: Bool)
}

class BaseChatCell: UITableViewCell {
    
    var isNeedBubbleBackground = true
    
    lazy var msgSendIndicator: UIActivityIndicatorView = {
        let v = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        self.contentView.addSubview(v)
        return v
    }()
    //
    lazy var msgRetryButton: UIButton = {
        let v = UIButton(type: UIButton.ButtonType.custom)
        v.bounds = CGRect(x: 0, y: 0, width: 30, height: 30)
        v.setTitle("!", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.titleLabel!.font = UIFont.boldSystemFont(ofSize: 14)
        v.layer.cornerRadius = 15
        v.layer.backgroundColor = Color.red.cgColor
        self.contentView.addSubview(v)
        return v
    }()
    
    // 消息时间
    lazy var messageTimeLabel:UILabel = {
        let v = UILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textColor = UIColor.darkGray
        v.textAlignment = NSTextAlignment.center
        contentView.addSubview(v)
        return v
    }()
    // 用户头像
    lazy var messageAvatarsImageView:UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_user_default")
        contentView.addSubview(v)
        return v
    }()
    // 用户名
    lazy var messageUserNameLabel:UILabel = {
        let v = UILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textColor = UIColor.init(hex:0xb9b9bb)
        contentView.addSubview(v)
        return v
    }()
    lazy var messagebubbleBackImageView:UIImageView = {
        let v = UIImageView()
        v.isUserInteractionEnabled = true
        contentView.addSubview(v)
        return v
    }()
    
    var message:Message?
    var indexPath:IndexPath?
    weak var delegate:BaseChatCellDelegate?
    var showTime: Bool {
        return !messageTimeLabel.isHidden
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // 初始化
    func layoutUI() {
        if isNeedBubbleBackground {
            // 气泡
        }
    }
    
    public func remakeConstraintsOfIndicator(_ offsetView: UIView, _ sendStatus: MessageStatus) {
        if sendStatus == MessageStatus.failed {
            msgRetryButton.isHidden = false
            msgRetryButton.snp.remakeConstraints { (make) in
                make.size.equalTo(CGSize(width: 30, height: 30))
                make.centerY.equalTo(offsetView)
                make.right.equalTo(offsetView.snp.left).offset(-5)
            }
        } else if sendStatus == MessageStatus.sending {
            msgSendIndicator.isHidden = false
            msgSendIndicator.startAnimating()
            
            msgSendIndicator.snp.remakeConstraints { (make) in
                make.size.equalTo(CGSize(width: 30, height: 30))
                make.centerY.equalTo(offsetView)
                make.right.equalTo(offsetView.snp.left).offset(-5)
            }
        } else {
            msgSendIndicator.stopAnimating()
        }
    }
    
    // 加载cell内容
    public func updateMessage(_ msg: Message, idx: IndexPath) {
        
        message = msg
        indexPath = idx
        
        msgRetryButton.isHidden = true
        msgSendIndicator.isHidden = true
        
        messageTimeLabel.snp.remakeConstraints({ (make) in
            make.top.equalTo(8)
            make.height.equalTo(showTime ? 20 : 0)
            make.width.equalTo(contentView)
            make.centerX.equalTo(contentView)
        })
        
        // 发送的消息
        if message?.direction == MessageDirection.send.rawValue {
            messageAvatarsImageView.snp.remakeConstraints({ (make) in
                make.top.equalTo(showTime ? 36 : 8)
                make.width.height.equalTo(36)
                make.right.equalTo(-8)
            })
            
            messageAvatarsImageView.setImage(with: URL(string: User.default.avatar), placeHolder: UIImage(named: "ic_user_default"))
            
            messageUserNameLabel.snp.remakeConstraints({ (make) in
                make.top.equalTo(messageAvatarsImageView)
                make.right.equalTo(messageAvatarsImageView.snp.left).offset(-8)
                make.height.equalTo(15)
                make.width.equalTo(150)
            })
            messageUserNameLabel.text = User.default.nickname
            messageUserNameLabel.textAlignment = .right
            
            if isNeedBubbleBackground {
                messagebubbleBackImageView.image = UIImage(named: "bg_bubble_blue")?.resizableImage(withCapInsets: UIEdgeInsets(top: 30, left: 28, bottom: 85, right: 28), resizingMode: UIImage.ResizingMode.stretch)
                messagebubbleBackImageView.snp.remakeConstraints({ (make) in
                    make.right.equalTo(messageAvatarsImageView.snp.left).offset(-8)
                    make.top.equalTo(messageUserNameLabel.snp.bottom)
                    make.bottom.equalTo(-10)
                })
                
            }
            
        }
        // 接收的消息
        else if message?.direction == MessageDirection.receive.rawValue{
            messageAvatarsImageView.snp.remakeConstraints({ (make) in
                make.top.equalTo(showTime ? 36 : 8)
                make.width.height.equalTo(36)
                make.left.equalTo(8)
            })
            messageAvatarsImageView.setImage(with: URL(string: msg.body.avatar), placeHolder: UIImage(named: "ic_user_default"))
            
            messageUserNameLabel.snp.remakeConstraints({ (make) in
                make.height.equalTo(15)
                make.top.equalTo(messageAvatarsImageView)
                make.width.equalTo(150)
                make.left.equalTo(messageAvatarsImageView.snp.right).offset(8)
            })
            messageUserNameLabel.text = msg.body.username
            messageUserNameLabel.textAlignment = .left
            
            if isNeedBubbleBackground {
                
                messagebubbleBackImageView.image = UIImage(named: "bg_bubble_white")?.resizableImage(withCapInsets: UIEdgeInsets(top: 30, left: 28, bottom: 85, right: 28), resizingMode: UIImage.ResizingMode.stretch)
                
                messagebubbleBackImageView.snp.remakeConstraints({ (make) in
                    make.left.equalTo(messageAvatarsImageView.snp.right).offset(8)
                    make.top.equalTo(messageUserNameLabel.snp.bottom)
                    make.bottom.equalTo(-10)
                })
            }
        }
        // 中间位置显示的提示消息
        else {
            messageUserNameLabel.isHidden = true
            messageAvatarsImageView.isHidden = true
        }
        layoutIfNeeded()
    }
    
    
    // 检测是否显示时间
    func updateTime(_ upTime: String, _ currentTime: String) -> String {
        if upTime.count > 0 {
            if var upTime = TimeInterval(upTime), var time2 = TimeInterval(currentTime) {
                if upTime > 140000000000 {
                    upTime = upTime / 1000
                }
                
                if time2 > 140000000000 {
                    time2 = time2 / 1000
                }
                
                if time2 - upTime < 2 * 60 {
                    return ""
                }
            }
        }
        
        if currentTime.count > 0 {
            if let time = TimeInterval(currentTime) {
                let date = Date(timeIntervalSince1970: time)
                let timeStr = date.getShowFormat()
                return timeStr
            } else {
                return currentTime
            }
        }
        
        return ""
    }
}

