//
//  ChatFriendTransferCell.swift
//  Red
//
//  Created by MAC on 2019/10/31.
//  Copyright © 2019 MAC. All rights reserved.
//


import Foundation
import UIKit
import YYText

/// 朋友转账cell
class ChatFriendTransferCell: BaseChatCell {
    lazy var messagePhotoImageView:FriendTransferImageView = {
        let v = FriendTransferImageView(frame: CGRect.zero)
        v.isUserInteractionEnabled = true
        v.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(ChatFriendTransferCell.tapHandle)))
        contentView.addSubview(v)
        return v
    }()
    
    lazy var tipsLabel: YYLabel = {
        let v = YYLabel()
        v.numberOfLines = 0
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 13)
        messagePhotoImageView.addSubview(v)
        return v
    }()
    lazy var typeLabel: YYLabel = {
        let v = YYLabel()
        v.textColor = .black
        v.font = UIFont.systemFont(ofSize: 10)
        messagePhotoImageView.addSubview(v)
        return v
    }()
   
    
    override func layoutUI() {
        isNeedBubbleBackground = false
        super.layoutUI()
    }
    
    @objc private func tapHandle() {
        if let message = message {
            delegate?.epDidTransferClick(message)
        }
    }
    
    override func updateMessage(_ m: Message, idx: IndexPath) {
        super.updateMessage(m, idx: idx)
        
        tipsLabel.text = "￥\(m.body.transfer_amount)\n查看红包"
        
        typeLabel.text = "好友转账"
        if m.direction == MessageDirection.receive.rawValue {
            messagePhotoImageView.updateMessagePhoto(UIImage(named: "transferShou"), isSendMessage: false)
            messagePhotoImageView.snp.makeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: (showTime ? 36 : 8)+messageUserNameLabel.height, left: 52*kScreenScale, bottom: 8, right: 100*kScreenScale))
                make.width.equalTo(180)
                make.height.equalTo(80)
                
            }
        } else if m.direction == MessageDirection.send.rawValue {
            messagePhotoImageView.updateMessagePhoto(UIImage(named: "transferFa"), isSendMessage: true)
            messagePhotoImageView.snp.remakeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: (showTime ? 36 : 8)+messageUserNameLabel.height, left: 100*kScreenScale, bottom: 8, right: 52*kScreenScale))
                make.width.equalTo(180)
                make.height.equalTo(80)
            }
            
           remakeConstraintsOfIndicator(messagePhotoImageView, MessageStatus(rawValue: (message?.status)!)!)
        }
        
        
        tipsLabel.snp.makeConstraints { (make) in
            make.left.equalTo(80)
            make.width.equalTo(100)
            make.height.equalTo(40)
            make.top.equalTo(15)
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.width.equalTo(100)
            make.height.equalTo(16)
            make.bottom.equalTo(messagePhotoImageView.snp.bottom)
        }
        
        layoutIfNeeded()
    }
    
}


class FriendTransferImageView: UIView {
    lazy var messagePhotoImageView:UIImageView = {
        let v = UIImageView()
        v.contentMode = UIView.ContentMode.scaleAspectFill
        v.clipsToBounds = true
        self.addSubview(v)
        return v
    }()
//    lazy var layerImageView:UIImageView = {
//        let v = UIImageView()
//        messagePhotoImageView.addSubview(v)
//        return v
//    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutUI() {
        // 图片
        messagePhotoImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
//        // 图片层
//        layerImageView.snp.makeConstraints { (make) in
//            make.edges.equalTo(messagePhotoImageView)
//        }
    }
    
    func updateMessagePhoto(_ image: UIImage?,isSendMessage: Bool) {
        
//        if isSendMessage {
//            layerImageView.image = UIImage(named: "bg_talk_bubble_photo")?.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 20), resizingMode: UIImage.ResizingMode.stretch)
//        }
//        else {
//            layerImageView.image = UIImage(named: "bg_talk_bubble_photo_left")?.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 10), resizingMode: UIImage.ResizingMode.stretch)
//        }
        
        messagePhotoImageView.image = image
    }
}
