//
//  ChatTextCell.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
import YYText

class ChatTextCell: BaseChatCell {
    
    lazy var messageTextLabel: YYLabel = {
        let v = YYLabel()
        v.numberOfLines = 0
        messagebubbleBackImageView.addSubview(v)
        v.yl_autoW()
        
        return v
    }()
    
    override func layoutUI() {
        super.layoutUI()
    }
    
    override func updateMessage(_ m: Message, idx: IndexPath) {
        super.updateMessage(m, idx: idx)
        
        let MsgBody = message?.body
        
        let text = MsgBody?.content.yl_conversionAttributedString() ?? NSAttributedString(string: "")
        
        let layout = YYTextLayout(containerSize: CGSize.init(width: YLScreenWidth*0.648, height: CGFloat.greatestFiniteMagnitude), text: text)
        
        messageTextLabel.textLayout = layout
        messageTextLabel.highlightTapAction = tapHighlightAction
        
        msgRetryButton.isHidden = true
        msgSendIndicator.isHidden = true
        if message?.direction == MessageDirection.send.rawValue {
            messageTextLabel.snp.remakeConstraints({ (make) in
                make.edges.equalTo(UIEdgeInsets(top: 11, left: 10, bottom: 11, right: 15))
                make.width.lessThanOrEqualTo(YLScreenWidth*0.648)
                make.height.equalTo((layout?.textBoundingSize.height)!)
            })
            
            remakeConstraintsOfIndicator(messagebubbleBackImageView, MessageStatus(rawValue: (message?.status)!)!)
            
        }
        else if message?.direction == MessageDirection.receive.rawValue {
            messageTextLabel.snp.remakeConstraints({ (make) in
                make.edges.equalTo(UIEdgeInsets(top: 11, left: 15, bottom: 11, right: 10))
                make.width.lessThanOrEqualTo(YLScreenWidth*0.648)
                make.height.equalTo((layout?.textBoundingSize.height)!)
            })
        }
        
        layoutIfNeeded()
    }
    
    fileprivate func tapHighlightAction(_ containerView:UIView, text:NSAttributedString, range:NSRange, rect:CGRect) {
        UIApplication.shared.open(URL(string: text.attributedSubstring(from: range).string)!, options: [:], completionHandler: nil)
    }
}
