//
//  ChatSendPackageCell.swift
//  Red
//
//  Created by MAC on 2019/10/30.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
import YYText

/// 发包cell(雷包，牛包，福利包)
class ChatSendPackageCell: BaseChatCell {
    
    private lazy var messagePhotoImageView: SendPackageImageView = {
        let v = SendPackageImageView(frame: CGRect.zero)
        v.isUserInteractionEnabled = true
        v.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(ChatSendPackageCell.tapHandle)))
        contentView.addSubview(v)
        return v
    }()
    
    private lazy var tipsLabel: YYLabel = {
        let v = YYLabel()
        v.numberOfLines = 0
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 13)
        
        messagePhotoImageView.addSubview(v)
        return v
    }()
    private lazy var typeLabel: YYLabel = {
        let v = YYLabel()
        v.textColor = .black
        v.font = UIFont.systemFont(ofSize: 10)
        messagePhotoImageView.addSubview(v)
        return v
    }()
    
    private lazy var timeLabel: YYLabel = {
        let v = YYLabel()
        v.isHidden = true
        v.textColor = Color.red
        v.textAlignment = .center
        v.backgroundColor = UIColor(white: 1, alpha: 0.5)
        v.font = UIFont.boldSystemFont(ofSize: 10)
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        messagePhotoImageView.addSubview(v)
        return v
    }()
    
    /// 时间倒计时完成时调用
    public var timerCompletionHandler: ((_ c: ChatSendPackageCell) -> Void)?
    
    private var timer: Timer?
    
    private lazy var queue: DispatchQueue = {
        let q = DispatchQueue.global()
        
        return q
    }()
    
    deinit {
        stopTimer()
        dprint("\(self): 计时停止")
    }
    
    override func layoutUI() {
        isNeedBubbleBackground = false
        super.layoutUI()
        tipsLabel.snp.makeConstraints { (make) in
            make.left.equalTo(60)
            make.width.equalTo(100)
            make.height.equalTo(40)
            make.top.equalTo(15)
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.width.equalTo(100)
            make.height.equalTo(16)
            make.bottom.equalTo(messagePhotoImageView)
        }
 
    }
    
    @objc private func tapHandle() {
        if let message = message {
            delegate?.epDidSendPackageClick(message)
        }
    }
    
    override func updateMessage(_ m: Message, idx: IndexPath) {
        super.updateMessage(m, idx: idx)
        tipsLabel.text = "\(m.body.amount)-\(m.body.packet_count)包\n查看红包"
        
        var backImgSufName = "" // 背景图的后缀

        let value = m.body.group_class
        if  let type = RoomType.type(value: value, isOwner: m.body.groupId.count > 3 ? true : false) {
            switch type {
            case .cattle:
                typeLabel.text = "牛牛红包"
                backImgSufName = "_niu"
            case .welfare:
                typeLabel.text = "福利红包"
                backImgSufName = "_fu"
            case .general:
                typeLabel.text = "普通红包"
            case .singleThunder:
                typeLabel.text = "单雷红包"
                backImgSufName = "_danlei"
            case .multipleThunder:
                typeLabel.text = "多雷红包"
                backImgSufName = "_duolei"
            case .solitaire:
                typeLabel.text = "接龙红包"
                backImgSufName = "_long"
                self.startTimer(self.getSeconds(push_time: m.body.push_time, expire_time: m.body.expire_time))
            }
        }
        
        
        let isSend = m.direction == MessageDirection.send.rawValue ? true : false
        
        let imgName = (isSend ? "fa" : "shou") + backImgSufName
        messagePhotoImageView.updateMessagePhoto(UIImage(named: imgName), isSendMessage: isSend)
        
        let edges = UIEdgeInsets(top: (showTime ? 36 : 8) +  messageUserNameLabel.height, left: (isSend ? 100 : 52)*kScreenScale, bottom: 10, right: (isSend ? 52 : 100)*kScreenScale)
        messagePhotoImageView.snp.remakeConstraints { (make) in
            make.edges.equalTo(edges)
            make.width.equalTo(180)
            make.height.equalTo(80)
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.right.equalTo(messagePhotoImageView).offset(isSend ? -15 : -5)
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.top.equalTo(messagePhotoImageView).offset(5)
        }
        
        if isSend {
            remakeConstraintsOfIndicator(messagePhotoImageView, MessageStatus(rawValue: (message?.status)!)!)
        }
        
        layoutIfNeeded()
    }
    
}

//MARK: do timer
extension ChatSendPackageCell {
    private func getSeconds(push_time:String, expire_time: String) -> Int {
        let ptimes = push_time.components(separatedBy: ",")
        guard let et = TimeInterval(expire_time), ptimes.count == 2, let server_push = TimeInterval(ptimes.first!), let locale_push = TimeInterval(ptimes.last!) else {
            return 0
        }
        
        let server_offset: Int = Int(et - server_push)
        if server_offset <= 0 {
            return 0
        }
        
        let locale_current = Date().timeIntervalSince1970
        let locale_offset: Int = Int(locale_push-locale_current) + server_offset
        if locale_offset <= 0 {
            return 0
        }
        
        return locale_offset
    }
    
    private func startTimer(_ count: Int) {
        timeLabel.text = "\(count)"
        timeLabel.isHidden = false
        guard count > 0 else {
            timeLabel.isHidden = true
            stopTimer()
            return
        }
        
        queue.activate()
        var d = count
        queue.async {
            self.timer = Timer(timeInterval: 1, repeats: true, block: {[weak self] (t) in
                guard let self = self else {
                    return
                }
                
                d -= 1
             
             DispatchQueue.main.async {
                self.timeLabel.text = "\(d)"
             }
                if d == 0 {
                    self.stopTimer()
                    
                    DispatchQueue.main.async {
                     self.timeLabel.isHidden = true
                    }
                }
            })
            // 避免倒计时期间，拖动列表使得倒计时失效
            RunLoop.current.add(self.timer!, forMode: .common)
            RunLoop.current.run()
        }
    }
       
   private func stopTimer() {
       if let _ = self.timer?.isValid {
           self.timer!.invalidate()
           self.timer = nil
       }
    self.queue.suspend()
   }
}


class SendPackageImageView: UIView {
    
    lazy var messagePhotoImageView: UIImageView = {
        let v = UIImageView()
        v.contentMode = UIView.ContentMode.scaleToFill
//        v.clipsToBounds = true
        self.addSubview(v)
        return v
    }()
//    lazy var layerImageView:UIImageView = {
//        let v = UIImageView()
//        messagePhotoImageView.addSubview(v)
//        return v
//    }()
//    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutUI() {
        // 图片
        messagePhotoImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
//        // 图片层
//        layerImageView.snp.makeConstraints { (make) in
//            make.edges.equalTo(messagePhotoImageView)
//        }
    }
    
    func updateMessagePhoto(_ image: UIImage?,isSendMessage: Bool) {
        
//        if isSendMessage {
//            layerImageView.image = UIImage(named: "bg_talk_bubble_photo")?.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 20), resizingMode: UIImage.ResizingMode.stretch)
//        }
//        else {
//            layerImageView.image = UIImage(named: "bg_talk_bubble_photo_left")?.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 10), resizingMode: UIImage.ResizingMode.stretch)
//        }
        
        messagePhotoImageView.image = image
    }
    
}
