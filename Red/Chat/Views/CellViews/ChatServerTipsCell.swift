//
//  ChatServerTipsCell.swift
//  Red
//
//  Created by MAC on 2019/10/30.
//  Copyright © 2019 MAC. All rights reserved.
//
import Foundation
import UIKit
import YYText

/// 抢包信息cell（显示在中间部分，两侧无用户头像，类似时间cell）
class ChatServerTipsCell: BaseChatCell {
    
    lazy var messageTextLabel: UILabel = {
        let v = UILabel()
        v.textAlignment = .center
        v.textColor = .darkGray
        v.font = UIFont.systemFont(ofSize: 13)
        v.numberOfLines = 0
        contentView.addSubview(v)
        return v
    }()
    
    override func layoutUI() {
        super.layoutUI()
    }
    
    override func updateMessage(_ m: Message, idx: IndexPath) {
        super.updateMessage(m, idx: idx)
        
        self.messageTextLabel.numberOfLines = 1
        let value = m.body.group_class
        if let type = RoomType.type(value: value) {
            switch type {
            case .solitaire:
                self.messageTextLabel.numberOfLines = 0
            default: break
            }
        }
        
        let body = message?.body
        DispatchQueue.global().async {
            var str = ""
            if let land = body?.content, land.count>0 {
                str = land
            }
            let sections = str.components(separatedBy: ",")
            let attr = NSMutableAttributedString(string: sections.joined(separator: ""))
            var location = 0
            for (i, sub) in sections.enumerated() {
                // 偶数对应位置设置红色
                if i%2==0 && sub.count>0 {
                    let range = NSRange(location: location, length: sub.count)
                    attr.addAttributes([.foregroundColor: Color.red], range: range)
                }
                location += sub.count
            }
             
            DispatchQueue.main.async {
                self.messageTextLabel.attributedText = attr
            }
        }
        
        messageTextLabel.snp.makeConstraints { (make) in
            make.width.equalTo(YLScreenWidth-40)
            make.height.equalTo(40)
            make.edges.equalTo(UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20))
            
        }
        
        layoutIfNeeded()
    }
}


