//
//  ChatImageCell.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit

class ChatImageCell: BaseChatCell {
    
    lazy var messagePhotoImageView:ChatPhotoImageView = {
        let v = ChatPhotoImageView(frame: CGRect.zero)
        v.isUserInteractionEnabled = true
        v.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(ChatImageCell.tapHandle)))
        contentView.addSubview(v)
        return v
    }()
    
    override func layoutUI() {
        isNeedBubbleBackground = false
        super.layoutUI()
    }
    
    @objc private func tapHandle() {
        if let message = message {
            delegate?.epDidImageClick(message, messagePhotoImageView.messagePhotoImageView.image)
        }
    }
    
    override func updateMessage(_ m: Message, idx: IndexPath) {
        super.updateMessage(m, idx: idx)
        
        let MsgBody = message?.body
        
        let sections = MsgBody?.content.components(separatedBy: ":::")
        guard let s = sections, s.count == 2 else {
            return
        }
        
        if let imgData = m.body.imageData {
            messagePhotoImageView.messagePhotoImageView.image = UIImage.init(data: imgData)
        } else {
            let url = s[1] as String
            messagePhotoImageView.messagePhotoImageView.kf.indicatorType = .activity // 显示菊花加载
            messagePhotoImageView.messagePhotoImageView.setImage(with: url, size: CGSize(width: 200, height: 200), mode: 0)
        }
            
        msgSendIndicator.isHidden = true
        msgRetryButton.isHidden = true
        if message?.direction == MessageDirection.send.rawValue {
            messagePhotoImageView.snp.remakeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: (showTime ? 36 : 8)+messageUserNameLabel.height, left: YLScreenWidth-52-180, bottom: 8, right: 52))
                make.width.equalTo(180)
                make.height.equalTo(150)
            }
            messagePhotoImageView.updateMessagePhoto(nil, isSendMessage: true)
            
            remakeConstraintsOfIndicator(messagePhotoImageView, MessageStatus(rawValue: (message?.status)!)!)
            
        } else {
            messagePhotoImageView.snp.remakeConstraints { (make) in
                make.edges.equalTo(UIEdgeInsets(top: (showTime ? 36 : 8)+messageUserNameLabel.height, left: 52, bottom: 8, right: YLScreenWidth-52-180))
                make.width.equalTo(180)
                make.height.equalTo(150)
            }
            messagePhotoImageView.updateMessagePhoto(nil, isSendMessage: false)
        }
        
        layoutIfNeeded()
        
    }
    
    
}


class ChatPhotoImageView: UIView {
    lazy var messagePhotoImageView:UIImageView = {
        let v = UIImageView()
        v.contentMode = UIView.ContentMode.scaleAspectFill
        v.clipsToBounds = true
        v.backgroundColor = UIColor(white: 1, alpha: 0.3)
        self.addSubview(v)
        return v
    }()
    lazy var layerImageView:UIImageView = {
        let v = UIImageView()
        messagePhotoImageView.addSubview(v)
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutUI() {
        // 图片
        messagePhotoImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        // 图片层
        layerImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(messagePhotoImageView)
        }
    }
    
    func updateMessagePhoto(_ image: UIImage?, isSendMessage: Bool) {
        if isSendMessage {
            layerImageView.image = UIImage(named: "bg_talk_bubble_photo")?.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 20), resizingMode: UIImage.ResizingMode.stretch)
        }
        else {
            layerImageView.image = UIImage(named: "bg_talk_bubble_photo_left")?.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 10), resizingMode: UIImage.ResizingMode.stretch)
        }
        
        if image != nil {
            messagePhotoImageView.image = image
        }
    }
}
