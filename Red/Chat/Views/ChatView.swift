//
//  ChatView.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit

protocol ChatViewDelegate: NSObjectProtocol {
    /// 发送文本代理
    func epSendMessageText(_ text: String)
    /// 发送图片代理
    func epSendMessageImage(_ images: [UIImage]?)
    /// 发送声音代理
    func ePSendMessageVoice(_ path: String?,duration: Int)
    
    /// 事件类型代理
    func epActionType(_ action: ChatViewActionType)
    
    /// 底部弹框状态（isOut: true处于编辑弹框 false普通状态）
    func inputBarOutStatus(_ isOut: Bool)
}

/// 事件类型
enum ChatViewActionType {
    /// 图片
    case photo
    /// 玩法
    case play
    /// 客服
    case customer
    /// 我的红包
    case redPackage
    /// 发包
    case sendPackage
    /// 充值
    case charge
    /// 转账
    case transfer
    /// 音量开关
    case openVoice(Bool)
}

enum ChatViewType {
    /// 群组消息
    case group
    /// 好友消息
    case friend
    /// 客服消息
    case customer
    /// 管理员消息
    case admin
}

class ChatView: YLReplyView {
    weak var delegate: ChatViewDelegate?
    // 群组消息提醒开关
    private var isOpenVoice = false
    private var type = ChatViewType.group
    // 默认未禁止输入
    private var forbidInput = false
    
    private var buttons: [UIButton] = []
    
    init(type: ChatViewType) {
        self.type = type
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ChatView {
    func setGroupVoiceStatus(_ open: Bool) {
        if type != .group {
            return
        }
        
        self.isOpenVoice = open
        
        let button = self.buttons.last!
        for v in button.subviews {
            if v.isKind(of: UIImageView.self), let imgv = v as? UIImageView {
                imgv.image = UIImage(named: "ic_group_voice"+(self.isOpenVoice ? "1" : "0"))
                break
            }
        }
    }
    
    //MARK: 禁言设置
    func forbidInput(_ status: Bool) {
        forbidInput = status
        if status {
            // 禁言关闭输入、表情、键盘事件
            self.evInputView.inputTextView.textColor = .lightGray
            self.evInputView.inputTextView.textAlignment = .center
            self.evInputView.inputTextView.text = "禁言中..."
            self.evInputView.inputTextView.isUserInteractionEnabled = false
            self.evInputView.faceBtn.isEnabled = false
            self.evInputView.keyboardBtn.isEnabled = false
        } else {
            self.evInputView.inputTextView.textAlignment = .left
            self.evInputView.inputTextView.text = ""
            self.evInputView.inputTextView.isUserInteractionEnabled = true
            self.evInputView.faceBtn.isEnabled = true
            self.evInputView.keyboardBtn.isEnabled = true
        }
    }
}



// MARK: - 重写父类方法
extension ChatView {
    
    override func efSendMessageText(_ text: String) {
        delegate?.epSendMessageText(text)
    }
    
    override func efSendMessageImage(_ images:[UIImage]?) {
        delegate?.epSendMessageImage(images)
    }
    
    override func efSendMessageVoice(_ path: String?,duration: Int) {
        delegate?.ePSendMessageVoice(path,duration: duration)
    }
    
    // 已经恢复普通状态
    override func efDidRecoverReplyViewStateForNormal() {
        delegate?.inputBarOutStatus(false)
    }
    
    // 已经恢复编辑状态
    override func efDidRecoverReplyViewStateForEdit() {
        delegate?.inputBarOutStatus(true)
    }
    
    
    // 添加更多面板
    @objc override func efAddMorePanelView() -> UIView {
        let panelView = UIView()
        panelView.backgroundColor = UIColor.white
        
        let scroll = UIScrollView()
        scroll.isScrollEnabled = true
        scroll.alwaysBounceHorizontal = true
        panelView.addSubview(scroll)
        scroll.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(panelView.snp.right)
            make.top.equalTo(0)
            make.bottom.equalTo(panelView.snp.bottom)
        }
        
        let cols = 4, rows = 2
        let itemSize = CGSize(width: 50, height: 70)
        let hmargin = (replyViewWidth - itemSize.width*CGFloat(cols))/CGFloat(cols+1)
        let vmargin = (defaultPanelViewH - itemSize.height*CGFloat(rows))/CGFloat(rows+1)
        
        var values: [(UIImage, String)] = []
        if type != .group {
            values = [(UIImage(named: "ic_tab_photo"), "图片"),
                      (UIImage(named: "ic_tab_join"), "转账"),
                      (UIImage(named: "ic_tab_red"), "我的红包"),
                      (UIImage(named: "ic_tab_recharge"), "充值"),
                      (UIImage(named: "ic_tab_custom"), "客服")
                    ] as! [(UIImage, String)]
        } else {
            values = [(UIImage(named: "ic_tab_photo"), "图片"),
                      (UIImage(named: "ic_tab_reward"), "发包"),
                      (UIImage(named: "ic_tab_red"), "我的红包"),
                      (UIImage(named: "ic_tab_recharge"), "充值"),
                      (UIImage(named: "icon_plugin_rp"), "玩法"),
                      (UIImage(named: "ic_tab_custom"), "客服"),
                      (UIImage(named: "ic_group_voice"+(isOpenVoice ? "1" : "0")), "红包提醒")
                    ] as! [(UIImage, String)]
        }
        let count = values.count
        
        for i in 0..<count {
            let v = UIButton(type: .custom)
            v.bounds = CGRect(x: 0, y: 0, width: itemSize.width, height: itemSize.height)
            v.tag = i
            v.addTarget(self, action: #selector(clickButtonAction(_:)), for: .touchUpInside)
            scroll.addSubview(v)
            self.buttons.append(v)
            
            let img = UIImageView()
            img.image = values[i].0
            img.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            v.addSubview(img)
            
            let lab = UILabel()
            lab.text = values[i].1
            lab.textColor = .black
            lab.textAlignment = .center
            lab.font = UIFont.systemFont(ofSize: 12)
            lab.frame = CGRect(x: 0, y: img.bottom, width: itemSize.width, height: itemSize.height-img.height)
            v.addSubview(lab)
            
            let x = hmargin + CGFloat(i%cols) * (hmargin+itemSize.width)
            let y = vmargin + CGFloat(i/cols) * (vmargin+itemSize.height)
            
            v.snp.makeConstraints { (make) in
                make.size.equalTo(itemSize)
                make.top.equalTo(y)
                make.left.equalTo(x)
            }
        }
        
        return panelView
    }
}


extension ChatView {
    @objc func clickButtonAction(_ btn: UIButton) {
        switch btn.tag {
        // 图片
        case 0:
            if forbidInput {
                // 禁止输入时 同样禁止发图片
                return
            }
            delegate?.epActionType(.photo)
        case 1:
            if type == .friend {
                // 转账
                delegate?.epActionType(.transfer)
            } else {
                // 发包
                delegate?.epActionType(.sendPackage)
            }
        case 2:
            // 红包记录
            delegate?.epActionType(.redPackage)
        case 3:
            // 充值
            delegate?.epActionType(.charge)
        case 4:
            if type == .friend {
                // 客服
                delegate?.epActionType(.customer)
            } else {
                // 玩法
                delegate?.epActionType(.play)
            }
        case 5:
            delegate?.epActionType(.customer)
        case 6:
            self.isOpenVoice = !self.isOpenVoice
            self.setGroupVoiceStatus(self.isOpenVoice)
            delegate?.epActionType(.openVoice(self.isOpenVoice))
        default: break
        }
    }
}
