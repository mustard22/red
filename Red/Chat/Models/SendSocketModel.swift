//
//  SendSocketModel.swift
//  Red
//
//  Created by MAC on 2019/12/17.
//  Copyright © 2019 MAC. All rights reserved.
//

/** 消息发送到服务器的处理文件
*/
import Foundation

public enum SendSocketMethod: String {
    /// 用户聊天
    case user = "user"
    /// 群组聊天
    case group = "group"
    
    /// 获取个别系统房（福利 接龙）离线消息
    case fetchPackage = "fetchPackage"
}

/// 发送消息到socket服务器的model
public struct SendSocketModel: Codable {
    var method: String = SendSocketMethod.user.rawValue
    var content: SendSocketContent = SendSocketContent()
    var sendTime: String = String(Int(Date().timeIntervalSince1970))
}

/// 发送消息体
public struct SendSocketContent: Codable {
    // 好友id
    var friendId: String?
    // 房间id
    var groupId: String?
    // 消息内容
    var message: String?
    
    // 获取离线消息的个数
    var count: String?
    
    /// 区分系统群与自建群(0:sys, 1:owner)
    var is_owner: String = "0"
}
