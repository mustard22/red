////
////  ServerSocketModels.swift
////  Red
////
////  Created by MAC on 2019/10/30.
////  Copyright © 2019 MAC. All rights reserved.
////
//
//import Foundation
//import RealmSwift
//
////MARK: socket服务端消息类型
//enum MessageType:String {
//    /// 群组消息
//    case group = "group"
//    /// 好友消息
//    case friendStatus = "friend"
//    /// 转账消息
//    case transfer = "transfer"
//    /// 群组发包消息
//    case sendLandPaper = "sendLandPaper"
//    /// 雷包房间：中雷 和 幸运号
//    case fetchPaper = "fetchPaper"
//    /// 牛包房间幸运牛
//    case cattleBestLuck = "cattleBestLuck"
//}
//
//
//// 自定义拷贝
//extension Object {
//    @objc func clone() -> Object {
//        return Object()
//    }
//}
//
///// 消息方向类型
//enum MessageDirection:Int{
//    case send = 1 // 发送的消息
//    case receive  // 接收的消息
//    case serverTips // 中间位置显示的提示消息
//}
//
///// 消息发送状态
//enum MessageStatus: Int {
//    case failed = -1
//    case sending
//    case success
//}
//
//// 消息体类型
//enum MsgBodyType:Int{
//    case text = 1
//    case image
//    case voice
//    
//}
//
//class GroupConversation: Object {
//    @objc dynamic var cid = NSUUID().uuidString // 用户id
//    
//    let messages = List<Message>()  // 用户对应的聊天消息
//    
//    override static func primaryKey() -> String? {
//        return "cid"
//    }
//    
//    override func clone() -> GroupConversation {
//        let conversation = GroupConversation()
//        conversation.cid = cid
//        for message in messages {
//            conversation.messages.append(message.clone())
//        }
//        
//        return conversation
//    }
//}
//
//class FriendConversation: Object {
//    @objc dynamic var cid = NSUUID().uuidString // 用户id
//    
//    @objc dynamic var nickname = "" // 昵称
//    
//    let messages = List<Message>()  // 用户对应的聊天消息
//    
//    override static func primaryKey() -> String? {
//        return "cid"
//    }
//    
//    override func clone() -> FriendConversation {
//        let conversation = FriendConversation()
//        conversation.cid = cid
//        conversation.nickname = nickname
//        for message in messages {
//            conversation.messages.append(message.clone())
//        }
//        
//        return conversation
//    }
//}
//
//
//// 消息model
//class Message: Object, Decodable {
//    
//    @objc dynamic var messageId = NSUUID().uuidString // 消息id
//    
//    @objc dynamic var timestamp = ""   // 时间戳
//    
//    @objc dynamic var direction = 0    // 消息方向
//    
//    @objc dynamic var sendStatus = 0   // 发送状态
//    
//    @objc dynamic var MsgBody:MsgBody! // 消息体
//    
//    override static func primaryKey() -> String? {
//        return "messageId"
//    }
//    
//    override func clone() -> Message {
//        let message = Message()
//        message.messageId = messageId
//        message.timestamp = timestamp
//        message.direction = direction
//        message.status = sendStatus
//        message.body = MsgBody.clone()
//        return message
//    }
//}
//
//// 消息体
//class MsgBody: Object, Decodable {
//    @objc dynamic var id = ""
//    @objc dynamic var type = ""
//    @objc dynamic var username = ""
//    @objc dynamic var sendPaperUserId = ""
//    @objc dynamic var avatar = ""
//    @objc dynamic var fromid = ""
//    @objc dynamic var project_id = ""
//    @objc dynamic var record_id = ""
//    @objc dynamic var content = ""
//    @objc dynamic var total_price = ""
//    @objc dynamic var total_number = ""
//    @objc dynamic var cid = ""
//    @objc dynamic var fetch_landmine_content = ""
//    @objc dynamic var fetch_luck_content = ""
//    @objc dynamic var transfer_amount = ""
//    
//    
//    override func clone() -> MsgBody {
//        let b = MsgBody()
//        b.id = id
//        b.type = type
//        b.username = username
//        b.sendPaperUserId = sendPaperUserId
//        b.avatar = avatar
//        b.fromid = fromid
//        b.project_id = project_id
//        b.record_id = record_id
//        b.content = content
//        b.total_price = total_price
//        b.total_number = total_number
//        b.cid = cid
//        b.fetch_landmine_content = fetch_landmine_content
//        b.fetch_luck_content = fetch_luck_content
//        b.transfer_amount = transfer_amount
//        
//        return b
//    }
//}
