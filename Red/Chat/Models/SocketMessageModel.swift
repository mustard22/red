//
//  SocketMessageModel.swift
//  Red
//
//  Created by MAC on 2019/12/14.
//  Copyright © 2019 MAC. All rights reserved.
//

/** socket服务器推送过来的消息处理文件
 */

import Foundation


//MARK: - 消息方向
enum MessageDirection: Int {
    /// 接收的消息
    case receive = 0
    /// 服务器消息
    case server = 1
    /// 发送的消息
    case send = 2
}

//MARK: - 消息状态
enum MessageStatus: Int {
    /// 发送失败
    case failed = -1
    /// 正在发送
    case sending = 0
    /// 发送成功
    case success = 1
}

//MARK: - 消息类型
enum MessageType: String {
    /// 消息确认
    case sendTime = "100"
    /// 添加好友
    case addFriend = "101"
    /// 用户聊天
    case friendChat = "102"
    /// 用户转账
    case friendTransfer = "103"
    /// 转账领取
    case getTransfer = "104"
    
    /// 对方已经同意加好友请求
    case friendAgreeRequest = "107"
    
    /// 群组聊天
    case groupChat = "120"
    /// 发送红包
    case sendPaper = "121"
    /// 踩雷赔付
    case fetchPaper = "122"
    /// 中雷奖励(自己的雷包，别人中雷3次就推送一个)
    case thunderMoney = "123"
    /// 幸运奖励(牛房)
    case luckyMoney = "124"
    /// 进群提醒
    case comeGroupRoom = "125"
    
    /// 牛包房对局消息（抢完牛包会推送：0胜0负）
    case cattleLucky = "126"
    
    /// 前台-后台聊天
    case adminOnline = "140"
    case adminOffline = "141"
    
    /// 登录过期
    case tokenExpired = "150"
    /// 下线
    case offline = "106"
    
    /// 好友的群邀请
    case ownerFriendInvite = "160"
    
    /// （取消）禁言
    case ownerMuted = "162"
    
    /// (取消)黑名单、踢出群聊
    case ownerBlacklist = "163"
    
    /// 编辑群信息或群主邀请后刷新自建群列表
    case refreshOwnerGroup = "164"
    
    /// 接龙红包未抢完，到时间了(20s)结束
    case solitaireEnded = "170"
    /// 接龙红包的最佳、最差手气(抢完推送)
    case solitaireTipsAfterEnd = "171"
}

// MARK: - 会话类型
public enum ConversationType: Int {
    public typealias RawValue = Int
    /// 群组会话
    case group = 0
    /// 好友会话
    case friend = 1
    /// 客服会话
    case customer = 2
    /// 管理员会话
    case admin = 3
}



//MARK: - 会话
class Conversation: NSObject, Codable {
    
    /// 会话id
    var cid: String = ""
    
    /// 会话类型(0:group, 1:friend)
    var type: Int? = nil
    
    /// 会话名称(群组或好友)
    var cname: String?
    
    /// 会话头像(群组或好友)
    var cavatar: String?
    
    var msgs: [Message] = []
}


//MARK: - 消息
class Message: NSObject, Codable {
    var direction: Int = MessageDirection.receive.rawValue
    var status: Int = MessageStatus.sending.rawValue
    var type: String = ""
    var body: MsgBody = MsgBody()
    
    override init() {}
    
    enum CodingKeys:String,CodingKey {
        case type = "code"
        case body = "data"
        case direction
        case status
    }
}

//MARK: - 消息体
public struct MsgBody: Codable {
    // 管理员ID; 若是好友转账，即转账id
    var id = ""
    
    var groupId = ""
    
    var senderId = ""
    var avatar = ""
    var username = ""
    
    var content = ""
    
    var sendTime = ""
    
    var transfer_amount = ""
    var project_id = ""

    var amount = ""
    var packet_count = ""
    
    /// 禁言
    var speak_status: String = ""
    /// 禁言 拉黑 踢群 的用户id
    var userId: String = ""
    /// 黑名单 踢群(1踢出群聊 2拉入黑名单 3解除黑名单)
    var status: String = ""
    
    /// 接龙包推送时间(拿到该时间后，后面拼上当前对应的本地时间)
    var push_time: String = ""
    /// 接龙包过期时间
    var expire_time: String = ""
    
    /// 群类型
    var group_class: String = ""
    
    /// 好友邀请消息处理状态（0未处理 1已同意 2已拒绝 3已邀请）
    var friendInviteStatus: String?
    
    //MARK: - 自己加的 socket不推送该字段
    var imageData: Data?
    
}


//MARK: - socket error msg
public struct SocketErrorMsg: Codable {
    var err_msg: String
    var err_code: String
}

public struct SocketErrorData<T:Codable>: Codable {
    var code: String
    var data: T
}
