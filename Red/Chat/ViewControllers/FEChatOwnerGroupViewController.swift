//
//  FEChatOwnerGroupViewController.swift
//  Red
//
//  Created by MAC on 2019/10/29.
//  Copyright © 2019 MAC. All rights reserved.
//


import Foundation
import UIKit
import QMUIKit
import SnapKit
import YYText

/// 自建群聊界面
class FEChatOwnerGroupViewController: FEChatVC {
    var groupItem: GroupList!
    
    var groupDetail: roomMessage!
    
    // 所有群组共用一个开关
    var isOpenVoice: Bool {
        get {
            return UserDefaults.Current.paperVoiceSwitch
        }
        
        set (newValue) {
            UserDefaults.Current.paperVoiceSwitch = newValue
        }
    }
    
    /// 默认不允许播放抢包声音(保证在离开页面时不播放声音<硬控>)
    fileprivate var isAllowFetchPaperVoice = false
    
    lazy var topLayerButton: QMUIButton = {
        let v = QMUIButton("充值", image: UIImage(named: "ic_group_charge"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        v.isUserInteractionEnabled = true
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    lazy var midLayerButton: QMUIButton = {
        let v = QMUIButton("玩法", image: UIImage(named: "ic_group_play"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    lazy var bottomLayerButton: QMUIButton = {
        let v = QMUIButton("分享", image: UIImage(named: "ic_group_share"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    
    lazy var inviteLayerButton: QMUIButton = {
        let v = QMUIButton("邀请", image: UIImage(named: "ic_owner_invite"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    
    // 右上角按钮（群房间）
    private lazy var rightTopButton: QMUIButton = {
        let v = QMUIButton("", image: "")
        v.size = CGSize(width: 40, height: 40)
        v.addTarget(self, action: #selector(handlerRightButton(_:)), for: .touchUpInside)
        
        let imgv = UIImageView()
        imgv.frame = CGRect(x: 5, y: 5, width: 30, height: 30)
        imgv.image = UIImage(named: "ic_group_nor")?.sls_tintColor_image(.white)
        v.addSubview(imgv)
        return v
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTopButton)
        
        self.title = groupItem?.name ?? ""
        
        self.chatView.setGroupVoiceStatus(self.isOpenVoice)
        
        self.setupSpeakStatus()
        
        createUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isAllowFetchPaperVoice = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isAllowFetchPaperVoice = false
    }
    
    deinit {
        MessageTipsManager.shared.removeTips(chat_id, .group)
    }
    
    //MARK: - override methods
    override func chatViewClickAction(_ action: ChatViewActionType) {
        dowithChatViewClickAction(action: action)
    }
    
    override func clickGroupFetchPaper(_ msg: Message) {
        dowithGroupFetchPaperAction(msg: msg)
    }
    
    override func otherLandMine(_ msg: Message) {
        dowithOtherLandMineAnimate(msg)
    }
    
    override func setupNewConversationInfo() {
        self.conversation.cname = self.groupItem?.name
        self.conversation.cavatar = self.groupItem?.img
        SMManager.manager.updateConversation(con: self.conversation)
    }
    
    //TODO: send群组文本消息
    override func sendMsgOfText(_ text: String, _ imgData: Data? = nil) {
        super.sendMsgOfText(text, imgData)
        
        var send = SendSocketModel()
        send.method = SendSocketMethod.group.rawValue
        
        send.content.groupId = chat_id
        send.content.message = text
        send.content.is_owner = "1"
        FEWebSocketManager.websocketWriteToServer(send: send)
        
        // 本地推送
        let msg = Message()
        msg.direction = MessageDirection.send.rawValue
        msg.type = MessageType.groupChat.rawValue
        
        var body = MsgBody()
        body.groupId = groupItem.id
        body.sendTime = send.sendTime
        body.content = text
        body.senderId = User.default.user_id
        
        if let d = imgData {
            body.imageData = d
        }
        
        msg.body = body
        
        dowithInsertNewMsg(msg)
    }
    
    override func insertNewMsg(_ msg: Message) {
        dowithInsertNewMsg(msg)
    }
    
    override func loadListData() {
        dowithLoadListData()
    }
    
    override func chatViewInputBarStatus(_ isInput: Bool) {
        dowithChatViewInputStatusChanged(isInput)
    }
}

//MARK: - UI
extension FEChatOwnerGroupViewController {
    func createUI() {
        topLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(view.snp.right).offset(-5)
            make.top.equalTo(kNaviBarHeight+20)
            make.size.equalTo(CGSize(width: 50, height: 70))
        }
        midLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(topLayerButton.snp.right)
            make.top.equalTo(topLayerButton.snp.bottom).offset(10)
            make.size.equalTo(topLayerButton.snp.size)
        }
        bottomLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(topLayerButton.snp.right)
            make.top.equalTo(midLayerButton.snp.bottom).offset(10)
            make.size.equalTo(topLayerButton.snp.size)
        }
        inviteLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(topLayerButton.snp.right)
            make.top.equalTo(bottomLayerButton.snp.bottom).offset(10)
            make.size.equalTo(topLayerButton.snp.size)
        }
    }
}

//MARK: - 事件处理
extension FEChatOwnerGroupViewController {
    //MARK: 设置浮层按钮的状态
    func dowithChatViewInputStatusChanged(_ isInput: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.topLayerButton.isHidden = isInput
            self.midLayerButton.isHidden = isInput
            self.bottomLayerButton.isHidden = isInput
            self.inviteLayerButton.isHidden = isInput
        }
    }
    
    //MARK: 拉取缓存消息
    func dowithLoadListData() {
        let array = self.conversation.msgs
        if self.activityIndicatorView.superview == nil || array.count == 0 {
            // 已经没有更多数据了
            return
        }
        
        self.activityIndicatorView.startAnimating()
        self.tableView.isScrollEnabled = false
        
        let from = loadCount * loadTime
        if from > array.count - 1 {
            // 没有更多消息了
            self.tableView.isScrollEnabled = true
            self.activityIndicatorView.stopAnimating()
            self.activityIndicatorView.removeFromSuperview()
            return
        }
        
        let end = array.count - 1 - from
        let offsetValue = end - loadCount + 1
        let start =  offsetValue >= 0 ? offsetValue : 0
        
        let insertMsgs = (start...end).map({array[$0]})
        tableView.dataArray = insertMsgs + tableView.dataArray
        loadTime += 1
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.activityIndicatorView.stopAnimating()
            self.tableView.isScrollEnabled = true
            if insertMsgs.count < self.loadCount {
                self.activityIndicatorView.removeFromSuperview()
            }
        }
        
        self.tableView.dataArray = self.tableView.dataArray.map({(m) -> Message in
            // cell上用来区分不同类型的发包
            m.body.group_class = self.groupItem.class
            return m
        })
        
        self.tableView.reloadData()
        let rect = self.tableView.rectForRow(at: IndexPath(row: insertMsgs.count - 1, section: 0))
        UIView.performWithoutAnimation {
            self.tableView.setContentOffset(CGPoint(x: 0, y: rect.origin.y), animated: false)
        }
    }
    
    //MARK: 群主编辑群组配置后调用
    func showAlertAfterAdminEdited() {
        let vc = UIAlertController(title: nil, message: "群主已修改群组配置，需要退出群聊再进入", preferredStyle: .alert)
        let action = UIAlertAction(title: "知道了", style: .default) { (ac) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        vc.addAction(action)
        self.present(vc)
    }
        
    //MARK: 消息插入列表处理
    func dowithInsertNewMsg(_ msg: Message) {
        if msg.type == MessageType.refreshOwnerGroup.rawValue {
            // 群主编辑群组配置socket
            self.showAlertAfterAdminEdited()
            return
        }
        
        // cell上用来区分不同类型的发包
        msg.body.group_class = self.groupItem.class
        
        // 这三种消息需要插入到对应包的后面
        if msg.type == MessageType.luckyMoney.rawValue || msg.type == MessageType.fetchPaper.rawValue || msg.type == MessageType.thunderMoney.rawValue || msg.type == MessageType.cattleLucky.rawValue || msg.type == MessageType.solitaireTipsAfterEnd.rawValue || msg.type == MessageType.solitaireEnded.rawValue {
            // 群组红包提示消息
            guard let index = tableView.dataArray.lastIndex(where: {$0.body.project_id == msg.body.project_id}) else {
                // 没找到对应红包 不显示该提示消息
                return
            }
            
            if index == tableView.dataArray.count - 1 {
                tableView.dataArray.append(msg)
                tableView.insertRows(at: [IndexPath.init(row: index+1, section: 0)], with: .top)
            } else {
                tableView.dataArray.insert(msg, at: index+1)
                tableView.insertRows(at: [IndexPath.init(row: index+1, section: 0)], with: .none)
            }
            
            // 判断是否自己发的包, 是否是中雷奖励
            guard msg.body.senderId == User.default.user_id, msg.type == MessageType.thunderMoney.rawValue else {
                return
            }
            
            // 显示奖励动画（自己发的包 其他人踩雷3次 才会推送该类型消息）
            self.otherLandMine(msg)
        }
        else if msg.type == MessageType.ownerMuted.rawValue || msg.type == MessageType.ownerBlacklist.rawValue {
            if msg.type == MessageType.ownerMuted.rawValue {
                let status = msg.body.speak_status
                guard !status.isEmpty else {
                    return
                }
                
                // 检测消息类型： 禁言
                let uid = msg.body.userId
                if !uid.isEmpty {
                    // 禁言个人
                    msg.body.content = msg.body.username + (status == "0" ? ", 已被群主解禁" : ", 已被群主禁言")
                    if uid == User.default.user_id {
                        self.groupItem.speak_status = status
                        self.groupDetail.speak_status = status
                    }
                } else {
                    // 群禁言
                    msg.body.content = status == "0" ? "群主取消了全员禁言" : "群主开启了全员禁言"
                    self.groupItem.speak_status = status
                    self.groupDetail.speak_status = status
                }
                
                self.setupSpeakStatus()
            } else {
                let status = msg.body.speak_status
                guard !status.isEmpty else {
                    return
                }
                
                // 检测消息类型：黑名单 踢群
                let uid = msg.body.userId
                guard !uid.isEmpty else {
                    return
                }
                
                if status == "1" {
                    // 踢群
                    msg.body.content = msg.body.username + ", 被踢出群聊"
                    if uid == User.default.user_id {
                        self.groupDetail.user_status = "2"
                    }
                } else if status == "2" {
                    // 黑名单
                    msg.body.content = msg.body.username + ", 被群主拉入黑名单"
                    if uid == User.default.user_id {
                        self.groupDetail.user_status = "2"
                    }
                } else {
                    // 取消黑名单
                    msg.body.content = msg.body.username + ", 被群主解除黑名单"
                    if uid == User.default.user_id {
                        self.groupDetail.user_status = "1"
                    }
                }
                
                self.setupSpeakStatus()
            }
            
            tableView.dataArray.append(msg)
            tableView.insertRows(at: [IndexPath.init(row: tableView.dataArray.count-1, section: 0)], with: UITableView.RowAnimation.none)
        }
        else {
            tableView.dataArray.append(msg)
            tableView.insertRows(at: [IndexPath.init(row: tableView.dataArray.count-1, section: 0)], with: UITableView.RowAnimation.none)
            
            if msg.direction == MessageDirection.send.rawValue {
                // 缓存自己发的消息
                SMManager.manager.saveMessageToConversation(msg)
            }
        }
        
        efScrollToLastCell()
        
        // 当前界面设置是否允许播放（进入页面，离开页面）
        if !isAllowFetchPaperVoice {
            return
        }
        
        // 自己发的红包消息 不播放提示音
        if msg.direction == MessageDirection.send.rawValue {
            return
        }
        
        // 非红包消息 不播放提示音
        if msg.type != MessageType.sendPaper.rawValue {
            return
        }
        
        // 判断设置里面的开关是否打开
        if self.isOpenVoice {
            // 播放前先进行停止操作
            VoiceManager.shared.stopPlay()
            // 获取提示音资源路径
            guard let path = Bundle.main.path(forResource: "msg", ofType: "mp3") else {
                return
            }
            // 播放提示音
            VoiceManager.shared.play(path, nil)
        }
    }
    
    //MARK: 点击更多面板的事件处理
    func dowithChatViewClickAction(action: ChatViewActionType) {
        switch action {
        case .photo:
            self.selectPhoto()
        case .sendPackage:
            //MARK: 发包
            self.requestServerOfSendPackage()
        case .redPackage:
            // 红包记录
            let vc = FECenterRedPackageViewController()
            self.push(to: vc, animated: true, completion: nil)
        case .charge:
            // 充值
            let vc = FECenterChargeMoneyViewController()
            self.push(to: vc, animated: true, completion: nil)
        case .play:
            // 玩法
            self.actionOfPlay()
            break
        case .customer:
            // 客服
            ContactCustomer.startContact(currentvc: self, enterChat: true)
        case .openVoice(let open):
            // 声音提醒开关
            self.isOpenVoice = open
        default :
            break
        }
    }
    
    //MARK: 玩法
    private func actionOfPlay() {
        guard let item = groupItem, item.playMethodItem.url.hasPrefix("http") else {
            return
        }
        let vc = FEBaseWebViewController()
        vc.title = item.playMethodItem.title
        vc.url = item.playMethodItem.url
        self.push(to: vc, animated: true, completion: nil)
    }
    
    //MARK: 点击群组红包
    func dowithGroupFetchPaperAction(msg: Message) {
        if let m = self.groupDetail, m.user_status == "2" {
            QMUITips.show(withText: "您已被群主拉入黑名单，禁止抢包", in: self.view)
            return
        }
        
        if msg.direction == MessageDirection.receive.rawValue {
            //MARK: 领别人发的包
            self.groupPaperInfo(msg.body.project_id)
        }
        else if msg.direction == MessageDirection.send.rawValue {
            //MARK: 领自己发的包
            self.groupPaperInfo(msg.body.project_id)
        }
        else{}
    }
    
    //MARK: 群组发包
    func requestServerOfSendPackage() {
        if let m = self.groupDetail, m.user_status == "2" {
            QMUITips.show(withText: "您已被群主拉入黑名单，禁止发包", in: self.view)
            return
        }
        
        let vc = OwnerGroupSendPaperViewController()
        vc.msg = groupDetail
        vc.group = groupItem
        if let type = RoomType.type(value: groupItem!.class, isOwner: true)  {
            vc.sendType = type
        }
        vc.sendPackageHandler = {[weak self] (item, param) in
            guard let self = self else {
                return
            }
            // cache sendPaper message for locale
            self.sendMsgOfGroupSendPackage(item, param)
        }
        self.push(to: vc, animated: true, completion: nil)
    }
    
    //MARK: 本地缓存发包消息
    func sendMsgOfGroupSendPackage(_ item: GroupSendPackageItem, _ param: OwnerSendPaperParam) {
        // 本地推送
        let msg = Message()
        msg.status = MessageStatus.success.rawValue
        msg.direction = MessageDirection.send.rawValue
        msg.type = MessageType.sendPaper.rawValue
        
        var body = MsgBody()
        body.senderId = User.default.user_id
        body.groupId = self.groupItem.id
        body.sendTime = String(Int(Date().timeIntervalSince1970))
        body.project_id = item.id
        body.push_time = item.create_time + "," + body.sendTime
        body.expire_time = item.expire_time
        body.packet_count = param.count
        body.amount = param.amount
        body.group_class = self.groupItem.class
        msg.body = body
        
        dowithInsertNewMsg(msg)
    }
}

//MARK: 抢包相关的动画
extension FEChatOwnerGroupViewController {
    // 幸运雷动画
    func dowithOtherLandMineAnimate(_ msg: Message) {
        let view = GroupFetchPaperAnimateLayerView2(frame: .zero)
        view.show()
    }
    
    // 中牛动画
    func dowithCattleLuckyAnimate() {
        let view = GroupFetchPaperAnimateLayerView1(frame: .zero)
        view.show()
    }
    
    // 踩雷动画
    func dowithThunderAnimate() {
        let view = GroupFetchPaperAnimateLayerView3(frame: .zero)
        view.show()
    }
}


/// 接口请求
extension FEChatOwnerGroupViewController {
    // 获取红包信息
    func groupPaperInfo(_ project_id: String) {
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.ownerGroupPaperInfo(paper_id: project_id) {[weak self] (status, msg, item) in
            load.hide(animated: true)
            guard let self = self else {
                return
            }
            guard let item = item else {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            
            let l = OwnerGroupPaperInfoLayerView.shared
            l.updateBeforeFetchPaper(item)
            l.show()
            l.actionHandler = {(action) in
                if action == .detailPaper {
                    self.enterDetailOfPaper(project_id)
                } else {
                    self.groupFetchPaper(item, l)
                }
            }
        }
    }
    
    // 领包
    func groupFetchPaper(_ item: OwnerGroupPaperInfoResult, _ layer: OwnerGroupPaperInfoLayerView) {
        guard let groupItem = self.groupItem, let roomType = RoomType.type(value: groupItem.class, isOwner: true) else {
            return
        }
        HttpClient.default.ownerGroupFetchPaper(paper_id: item.id) {[weak self](status, msg, item) in
            guard let self = self else {
                return
            }
            guard let item = item else {
                layer.hide() // failed 
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            
            layer.updateAfterFetchPaper(item.money, msg)
            
            switch roomType {
            case .cattle:
                //MARK: 判断是否中牛 中牛播放动画
                if item.isCattleLucky {
                    self.dowithCattleLuckyAnimate()
                }
            case .singleThunder, .multipleThunder:
                if item.is_pay != "1" {
                    return
                }
                self.dowithThunderAnimate()
            default: break
            }
        }
    }
    
    
    // 红包详情
    func enterDetailOfPaper(_ project_id: String) {
        let vc = OwnerGroupPaperDetailViewController()
        vc.project_id = project_id
        vc.groupClass = RoomType.type(value: groupItem.class) ?? .cattle
        vc.group_id = groupItem.id
        self.push(to: vc, animated: true, completion: nil)
    }
}

//MARK: 点击事件
extension FEChatOwnerGroupViewController {
    @objc func handlerRightButton(_ btn: UIButton) {
        let vc = OwnerGroupDetailViewController()
        vc.message = groupDetail
        vc.groupItem = groupItem
        self.push(to: vc, animated: true, completion: nil)
        
        vc.clearChatRecordsHandler = {[weak self] in
            guard let self = self else {
                return
            }
            
            self.conversation.msgs.removeAll()
            self.tableView.dataArray.removeAll()
            self.tableView.reloadData()
        }
        
        vc.updateOwnerGroupHandler = {(detail, group) in
            if let d = detail {
                self.groupDetail = d
            }
            if let g = group {
                self.groupItem = g
            }
        }
    }
    
    @objc func layerButtonAction(_ btn: QMUIButton) {
        if btn == topLayerButton {
            // 充值
            let vc = FECenterChargeMoneyViewController()
            self.push(to: vc, animated: true, completion: nil)
        } else if btn == midLayerButton {
            // 玩法
            self.actionOfPlay()
        } else if btn == bottomLayerButton {
            // 分享
            let shareUrl = User.default.download_url + "&" + "GROUPID=\(self.chat_id)" + "&" + "USERID=\(User.default.user_id)" + "&version=\(UIApplication.appVersion)"
            let shareModel = AppShare(text: "拼手气", image: UIImage(named: "AppIcon")?.jpegData(compressionQuality: 0.5), url: shareUrl)
            
            var share: ShareView
            
            if UserCenter.default.lbanner.web_rings_switch == "1" {
                share = ShareView(types:  ShareType.allCases, shareModel: shareModel)
            } else {
                share = ShareView(shareModel: shareModel)
            }
        
            share.show()
            
            share.clickHandler = {[weak self](type) in
                if type == .circle {
                    self?.dowithCirclePublish()
                }
            }
        } else {
            // 邀请
            var isGroupAdmin = false
            if let groupItem = groupItem, groupItem.user_id == User.default.user_id {
                isGroupAdmin = true
            }
            let vc = OwnerGroupInviteMemberViewController()
            vc.isGroupAdmin = isGroupAdmin
            vc.members = groupDetail.room_user.member
            vc.groupId = groupItem?.id ?? ""
            self.push(to: vc, animated: true, completion: nil)
        }
    }
    
    private func dowithCirclePublish() {
        var isAllowShare = true
        var items = UserDefaults.Current.shareOwnerGroupToCircle
        
        let gid = groupItem.id
        var citem = FEShareOwnerGroupCircle(groupId: gid, date: Date(), count: 0)
        if !items.isEmpty, let index = items.firstIndex(where: {$0.groupId == gid}) {
            citem = items[index]
            items.remove(at: index)
            if citem.date.day == Date().day, citem.count > 0 {
                // 一个自建群一天分享一次
                isAllowShare = false
            }
        }


        if !isAllowShare {
            QMUITips.showInfo("您今天已经分享过该群了", in: self.view)
            return
        }
        
        var p = FECirclePublishParam()
        p.share_group_id = gid
        p.pics = groupItem.img
        
        p.content = "\(groupItem.name)\("GROUP".md5())\(groupItem.know)"
        
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.circlePublish(param: p) { (flag, msg) in
            load.hide(animated: true)
            if !flag {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            citem.count += 1
            citem.date = Date()
            items.append(citem)
            UserDefaults.Current.shareOwnerGroupToCircle = items
            
            let alert = UIAlertController(title: "分享成功", message: "是否立即查看？", preferredStyle: .alert)
            let no = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            let ok = UIAlertAction(title: "确定", style: .default) { (a) in
                self.tabBarController?.selectedIndex = 3
            }
            alert.addAction(no)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
}


extension FEChatOwnerGroupViewController {
    //MARK: 设置禁言
    private func setupSpeakStatus() {
        guard let item = self.groupDetail else {
            return
        }
        
        var isNotSpeak = false
        self.title = item.room_name
        if item.speak_status == "1"/*已禁言*/ || item.user_status == "2"/*已拉黑*/ || User.default.speak_status == "1" {
            isNotSpeak = true
        }
        
        self.chatView.forbidInput(isNotSpeak)
    }
}
