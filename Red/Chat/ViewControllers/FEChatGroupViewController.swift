//
//  FEChatGroupViewController.swift
//  Red
//
//  Created by MAC on 2019/10/29.
//  Copyright © 2019 MAC. All rights reserved.
//


import Foundation
import UIKit
import QMUIKit
import SnapKit
import YYText

/// 群聊界面
class FEChatGroupViewController: FEChatVC {
    
    var groupItem: GroupList?
    
    var roomMessage: roomMessage?
    
    // 所有群组共用一个开关
    var isOpenVoice: Bool {
        get {
            return UserDefaults.Current.paperVoiceSwitch
        }
        
        set (newValue) {
            UserDefaults.Current.paperVoiceSwitch = newValue
        }
    }
    
    /// 默认不允许播放抢包声音(保证在离开页面时不播放声音<硬控>)
    fileprivate var isAllowFetchPaperVoice = false
    
    lazy var topLayerButton: QMUIButton = {
        let v = QMUIButton("充值", image: UIImage(named: "ic_group_charge"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        v.isUserInteractionEnabled = true
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    lazy var midLayerButton: QMUIButton = {
        let v = QMUIButton("玩法", image: UIImage(named: "ic_group_play"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    lazy var bottomLayerButton: QMUIButton = {
        let v = QMUIButton("分享", image: UIImage(named: "ic_group_share"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    
    // 右上角按钮（群房间）
    private lazy var rightTopButton: QMUIButton = {
        let v = QMUIButton("", image: "")
        v.size = CGSize(width: 40, height: 40)
        v.addTarget(self, action: #selector(handlerRightButton(_:)), for: .touchUpInside)
        
        let imgv = UIImageView()
        imgv.frame = CGRect(x: 5, y: 5, width: 30, height: 30)
        imgv.image = UIImage(named: "ic_group_nor")?.sls_tintColor_image(.white)
        v.addSubview(imgv)
        return v
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTopButton)
        
        self.title = groupItem?.name ?? ""
        
        self.chatView.setGroupVoiceStatus(self.isOpenVoice)
        
        self.setupSpeakStatus()
        
        self.loadGroupInfo()
        
        createUI()
        
        self.sendSocketForSomeRooms()
    }
    
    //MARK: 福利房，接龙房获取离线消息（主动发送socket获取）
    private func sendSocketForSomeRooms() {
        guard let gitem = self.groupItem, let type = RoomType.type(value: gitem.class), self.conversation.msgs.isEmpty else {
            return
        }
        
        // 无历史消息时主动获取
        switch type {
        case .solitaire, .welfare:
            var send = SendSocketModel()
            send.method = SendSocketMethod.fetchPackage.rawValue
            send.content.groupId = gitem.id
            send.content.count = "1"
            FEWebSocketManager.websocketWriteToServer(send: send)
        default: break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isAllowFetchPaperVoice = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isAllowFetchPaperVoice = false
    }
    
    deinit {
        MessageTipsManager.shared.removeTips(chat_id, .group)
    }
    
    //MARK: - override methods
    override func chatViewClickAction(_ action: ChatViewActionType) {
        dowithChatViewClickAction(action: action)
    }
    
    override func clickGroupFetchPaper(_ msg: Message) {
        dowithGroupFetchPaperAction(msg: msg)
    }
    
    override func otherLandMine(_ msg: Message) {
        dowithOtherLandMineAnimate(msg)
    }
    
    override func setupNewConversationInfo() {
        self.conversation.cname = self.groupItem?.name
        self.conversation.cavatar = self.groupItem?.img
        SMManager.manager.updateConversation(con: self.conversation)
    }
    
    //TODO: send群组文本消息
    override func sendMsgOfText(_ text: String, _ imgData: Data? = nil) {
        super.sendMsgOfText(text, imgData)
        
        var send = SendSocketModel()
        send.method = SendSocketMethod.group.rawValue
        
        send.content.groupId = chat_id
        send.content.message = text
        send.content.is_owner = "0"
        FEWebSocketManager.websocketWriteToServer(send: send)
        
        // 本地推送
        let msg = Message()
        msg.direction = MessageDirection.send.rawValue
        msg.type = MessageType.groupChat.rawValue
        
        var body = MsgBody()
        body.sendTime = send.sendTime
        body.content = text
        body.senderId = User.default.user_id
        body.groupId = self.chat_id
        
        if let d = imgData {
            body.imageData = d
        }
        
        msg.body = body
        
        dowithInsertNewMsg(msg)
    }
    
    override func insertNewMsg(_ msg: Message) {
        dowithInsertNewMsg(msg)
    }
    
    override func loadListData() {
        dowithLoadListData()
    }
    
    override func chatViewInputBarStatus(_ isInput: Bool) {
        dowithChatViewInputStatusChanged(isInput)
    }
}

//MARK: - UI
extension FEChatGroupViewController {
    func createUI() {
        topLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(view.snp.right).offset(-5)
            make.top.equalTo(kNaviBarHeight+20)
            make.size.equalTo(CGSize(width: 50, height: 70))
        }
        midLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(topLayerButton.snp.right)
            make.top.equalTo(topLayerButton.snp.bottom).offset(10)
            make.size.equalTo(topLayerButton.snp.size)
        }
        bottomLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(topLayerButton.snp.right)
            make.top.equalTo(midLayerButton.snp.bottom).offset(10)
            make.size.equalTo(topLayerButton.snp.size)
        }
    }
}

//MARK: - 事件处理
extension FEChatGroupViewController {
    func dowithChatViewInputStatusChanged(_ isInput: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.topLayerButton.isHidden = isInput
            self.midLayerButton.isHidden = isInput
            self.bottomLayerButton.isHidden = isInput
        }
    }
    
    // 加载数据
    fileprivate func loadGroupInfo() {
        guard roomMessage == nil else {
            // 从外面已经传过了
            return
        }
        
        var parate = goRoomParam()
        parate.room_id = groupItem?.id ?? ""
        parate.room_class = groupItem?.class ?? ""
        
        HttpClient.default.roomMessageData(parame: parate) { (isSuccess, message, messData) in
            if isSuccess {
                self.roomMessage = messData
            }
        }
    }
    
    // 获取聊天记录
    func dowithLoadListData() {
        let array = self.conversation.msgs
        if self.activityIndicatorView.superview == nil || array.count == 0 {
            // 已经没有更多数据了
            return
        }
        
        self.activityIndicatorView.startAnimating()
        self.tableView.isScrollEnabled = false
        
        let from = loadCount * loadTime
        if from > array.count - 1 {
            // 没有更多消息了
            self.tableView.isScrollEnabled = true
            self.activityIndicatorView.stopAnimating()
            self.activityIndicatorView.removeFromSuperview()
            return
        }
        
        let end = array.count - 1 - from
        let offsetValue = end - loadCount + 1
        let start =  offsetValue >= 0 ? offsetValue : 0
        
        let insertMsgs = (start...end).map({array[$0]})
        tableView.dataArray = insertMsgs + tableView.dataArray
        loadTime += 1
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.activityIndicatorView.stopAnimating()
            self.tableView.isScrollEnabled = true
            if insertMsgs.count < self.loadCount {
                self.activityIndicatorView.removeFromSuperview()
            }
        }
        
        self.tableView.dataArray = self.tableView.dataArray.map({ (m) -> Message in
            m.body.group_class = self.groupItem?.class ?? ""
            return m
        })
        
        self.tableView.reloadData()
        let rect = self.tableView.rectForRow(at: IndexPath(row: insertMsgs.count - 1, section: 0))
        self.tableView.setContentOffset(CGPoint(x: 0, y: rect.origin.y), animated: false)
    }
        
    //MARK: 消息插入列表处理
    func dowithInsertNewMsg(_ msg: Message) {
        // cell上用来区分不同类型的发包
        msg.body.group_class = self.groupItem?.class ?? ""

        if msg.type == MessageType.luckyMoney.rawValue || msg.type == MessageType.fetchPaper.rawValue || msg.type == MessageType.thunderMoney.rawValue || msg.type == MessageType.cattleLucky.rawValue || msg.type == MessageType.solitaireTipsAfterEnd.rawValue || msg.type == MessageType.solitaireEnded.rawValue  {
            // 群组红包提示消息
            guard let index = tableView.dataArray.lastIndex(where: {$0.body.project_id == msg.body.project_id}) else {
                // 没找到对应红包 不显示该提示消息
                return
            }
            
            if index == tableView.dataArray.count - 1 {
                tableView.dataArray.append(msg)
                tableView.insertRows(at: [IndexPath.init(row: index+1, section: 0)], with: .top)
            } else {
                tableView.dataArray.insert(msg, at: index+1)
                tableView.insertRows(at: [IndexPath.init(row: index+1, section: 0)], with: .none)
            }
            
            // 判断是否自己发的包, 是否是中雷奖励
            guard msg.body.senderId == User.default.user_id, msg.type == MessageType.thunderMoney.rawValue else {
                return
            }
            
            // 显示奖励动画（自己发的包 其他人踩雷）
            self.otherLandMine(msg)
        }
        else {
            tableView.dataArray.append(msg)
            tableView.insertRows(at: [IndexPath.init(row: tableView.dataArray.count-1, section: 0)], with: UITableView.RowAnimation.none)
            if msg.direction == MessageDirection.send.rawValue {
                // 缓存自己发的消息
                SMManager.manager.saveMessageToConversation(msg)
            }
        }
        
        efScrollToLastCell()
        
        // 当前界面设置是否允许播放（进入页面，离开页面）
        if !isAllowFetchPaperVoice {
            return
        }
        
        // 自己发的红包消息 不播放提示音
        if msg.direction == MessageDirection.send.rawValue {
            return
        }
        
        // 非红包消息 不播放提示音
        if msg.type != MessageType.sendPaper.rawValue {
            return
        }
        
        // 判断设置里面的开关是否打开
        if self.isOpenVoice {
            // 播放前先进行停止操作
            VoiceManager.shared.stopPlay()
            // 获取提示音资源路径
            guard let path = Bundle.main.path(forResource: "msg", ofType: "mp3") else {
                return
            }
            // 播放提示音
            VoiceManager.shared.play(path, nil)
        }
    }
    
    //MARK: 点击更多面板的事件处理
    func dowithChatViewClickAction(action: ChatViewActionType) {
        switch action {
        case .photo:
            self.selectPhoto()
        case .sendPackage:
            // 发包
            self.requestServerOfSendPackage()
        case .redPackage:
            // 红包记录
            self.push(to: FECenterRedPackageViewController(), animated: true, completion: nil)
        case .charge:
            // 充值
            self.push(to: FECenterChargeMoneyViewController(), animated: true, completion: nil)
        case .play:
            // 玩法
            self.actionOfPlay()
        case .customer:
            // 客服
            ContactCustomer.startContact(currentvc: self, enterChat: true)
        case .openVoice(let open):
            // 声音提醒开关
            self.isOpenVoice = open
        default :
            break
        }
    }
    
    //MARK: 玩法
    private func actionOfPlay() {
        guard let item = groupItem, item.playMethodItem.url.hasPrefix("http") else {
            return
        }
        let vc = FEBaseWebViewController()
        vc.title = item.playMethodItem.title
        vc.url = item.playMethodItem.url
        self.push(to: vc, animated: true, completion: nil)
    }
    
    //MARK: 点击群组红包
    func dowithGroupFetchPaperAction(msg: Message) {
        self.groupPaperInfo(project_id: msg.body.project_id, senderId: msg.body.senderId)
    }
    
    //MARK: 群组发包
    func requestServerOfSendPackage() {
        let vc = FEGroupSendPackageViewController()
        vc.msg = roomMessage
        vc.group = groupItem
        
        if let type = RoomType.type(value: groupItem!.class) {
            vc.sendType = type
        }
        
        vc.sendPackageHandler = {[weak self] (item, param) in
            guard let self = self else {
                return
            }
            // cache sendPaper message for locale
            self.sendMsgOfGroupSendPackage(item, param)
        }
        self.push(to: vc, animated: true, completion: nil)
    }
    
    //MARK: 本地缓存发包消息
    func sendMsgOfGroupSendPackage(_ item: GroupSendPackageItem, _ param: GroupSendThunderParam) {
        // 本地推送
        let msg = Message()
        msg.status = MessageStatus.success.rawValue
        msg.direction = MessageDirection.send.rawValue
        msg.type = MessageType.sendPaper.rawValue
        
        var body = MsgBody()
        body.sendTime = String(Int(Date().timeIntervalSince1970))
        body.project_id = item.id
        body.push_time = item.create_time + "," + body.sendTime
        body.expire_time = item.expire_time
        body.packet_count = param.count ?? "0"
        body.amount = param.amount ?? "0"
        body.senderId = User.default.user_id
        body.groupId = self.chat_id
        body.group_class = self.groupItem?.class ?? ""
        msg.body = body
        
        dowithInsertNewMsg(msg)
    }
}

//MARK: 抢包相关的动画
extension FEChatGroupViewController {
    // 幸运雷动画
    func dowithOtherLandMineAnimate(_ msg: Message) {
        let view = GroupFetchPaperAnimateLayerView2(frame: .zero)
        view.show()
    }
    
    // 中牛动画
    func dowithCattleLuckyAnimate() {
        let view = GroupFetchPaperAnimateLayerView1(frame: .zero)
        view.show()
    }
    
    // 踩雷动画
    func dowithThunderAnimate() {
        let view = GroupFetchPaperAnimateLayerView3(frame: .zero)
        view.show()
    }
}


//MARK: 接口请求
extension FEChatGroupViewController {
    //MARK: 获取红包信息
    // status（状态：1领取中2已领完3已过期）如果status等于1时在判断money是否为0，为0请求抢包接口，有值显示金额及红包详情
    func groupPaperInfo(project_id: String, senderId: String) {
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.groupGetPaperInfo(project_id: project_id, senderId: senderId) {[weak self] (status, msg, item) in
            load.hide(animated: true)
            guard let self = self else {
                return
            }
            guard let data = item else {
                QMUITips.show(withText: msg.text, in: self.view)
                return
            }
            
            let l = GroupPaperInfoLayerView.shared
            l.updateBeforeFetchPaper(data)
            l.show()
            l.actionHandler = {(action) in
                if action == .detailPaper {
                    // 进入红包详情
                    self.enterDetailOfPaper(project_id, senderId)
                } else {
                    // 抢包
                    self.groupFetchPaper(data, senderId, l)
                }
            }
        }
    }
    
    // 领包
    func groupFetchPaper(_ item: GroupPaperInfoItem, _ senderId: String, _ layer: GroupPaperInfoLayerView) {
        guard let groupItem = self.groupItem, let type = RoomType.type(value: groupItem.class) else {
            return
        }
        
        HttpClient.default.groupFetchPaper(project_id: item.id, senderId: senderId) {[weak self] (status, msg, data) in
            layer.updateAfterFetchPaper(data?.money, msg)
            guard let res = data else {
                return
            }
            switch type {
            case .cattle :
                //MARK: 判断是否中牛 中牛播放动画
                if res.isCattleLucky {
                    self?.dowithCattleLuckyAnimate()
                }
            case .singleThunder, .multipleThunder:
                // 是否中雷
                if res.is_landmine == "1" {
                    self?.dowithThunderAnimate()
                }
            default: break
            }
        }
    }
    
    
    //MARK: 红包详情
    func enterDetailOfPaper(_ project_id: String, _ senderId: String) {
        let vc = FEGroupPaperDetailViewController()
        vc.project_id = project_id
        vc.senderId = senderId
        vc.group_id = groupItem!.id
        vc.groupClass = RoomType.type(value: groupItem!.class) ?? .cattle
        self.push(to: vc, animated: true, completion: nil)
    }
}

//MARK: 点击事件
extension FEChatGroupViewController {
    @objc func handlerRightButton(_ btn: UIButton) {
        let vc = FEGroupInfoViewController()
        vc.message = roomMessage
        vc.groupItem = groupItem
        self.push(to: vc, animated: true, completion: nil)
        
        vc.clearChatRecordsHandler = {[weak self] in
            guard let self = self else {
                return
            }
            
            self.conversation.msgs.removeAll()
            self.tableView.dataArray.removeAll()
            self.tableView.reloadData()
        }
    }
    
    @objc func layerButtonAction(_ btn: QMUIButton) {
        if btn == topLayerButton {
            // 充值
            let vc = FECenterChargeMoneyViewController()
            self.push(to: vc, animated: true, completion: nil)
        } else if btn == midLayerButton {
            // 玩法
            self.actionOfPlay()
        } else {
            // 分享
            let sendUrl: String = User.default.download_url + "&version=\(UIApplication.appVersion)"
            let shareModel = AppShare(text: "拼手气", image: UIImage(named: "AppIcon")?.jpegData(compressionQuality: 0.5), url: sendUrl)
            let share = ShareView(shareModel: shareModel)
            share.show()
        }
    }
}


extension FEChatGroupViewController {
    //MARK: 设置禁言
    private func setupSpeakStatus() {
        guard let item = groupItem else {
            return
        }
        
        self.title = item.name
        if item.speak_status == "1" || User.default.speak_status == "1" {
            // 禁言
            self.chatView.forbidInput(true)
        } else {
            self.chatView.forbidInput(false)
        }
    }
}
