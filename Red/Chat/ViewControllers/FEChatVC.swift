//
//  FEChatVC.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
import QMUIKit
import SnapKit
import YYText


class FEChatVC: FEBaseViewController {
    /// 聊天室id
    var chat_id = ""
    var chat_type = ChatViewType.group
    
    lazy var conversation: Conversation = {
        let ctype = chat_type == .group ? true : false
        var con: Conversation? = SMManager.manager.getSingleChat(cid: chat_id, isGroup: ctype)
        
        var type: ConversationType
        switch chat_type {
        case .group:
            type = .group
        case .friend:
            type = .friend
        case .customer:
            type = .customer
        case .admin:
            type = .admin
        }
        
        if con == nil {
            var c = Conversation()
            c.cid = chat_id
            c.type = type.rawValue
            con = c
            self.setupNewConversationInfo()
        } else if con!.type == nil || (con!.type != type.rawValue) {
            con!.type = type.rawValue
            SMManager.manager.updateConversation(con: con!)
        }
        return con!
    }()
    
    /// 是否允许自动滚动到最后一个cell
    var isAllowAutoScrollToLast: Bool {
        if tableView.isScrollEnabled == false {
            return false
        }
        
        if loadTime > 0, let rows = tableView.indexPathsForVisibleRows,
            rows.filter({$0.row >= (tableView.dataArray.count - 1 - 5)}).count == 0 {
            dprint("\(rows) : \(tableView.dataArray.count-1)")
            return false
        }
        
        return true
    }
    
    let loadCount = 15
    /// 加载次数
    var loadTime = 0
    
    // 上一次播放的语音
    fileprivate var oldChatVoiceMessage:Message? = nil
    
    // 表单
    public lazy var tableView: ChatTableView = {
        let v = ChatTableView(frame: CGRect.zero, style: UITableView.Style.plain)
        v.CTVDelegate = self
        chatView.insertSubview(v, at: 0)
        return v
    }()
    
    lazy var chatView: ChatView = {
        let v = ChatView(type: chat_type)
        v.backgroundColor = view.backgroundColor
        v.delegate = self
        view.addSubview(v)
        return v
    }()
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let v = UIActivityIndicatorView(frame: CGRect.zero)
        v.style = UIActivityIndicatorView.Style.gray
        chatView.addSubview(v)
        return v
    }()

    //MARK: - init & deinit
    // 初始化
    // chat_id: 聊天ID（群组ID 或 好友ID）
    // chat_type: 根据不同type设置更多面板的不同样式
    init(chat_id: String, chat_type: ChatViewType? = .group) {
        self.chat_id = chat_id
        self.chat_type = chat_type!
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
//        NotificationCenter.default.removeObserver(self, name: .receiveServerSocket, object: nil)
        UIDevice.current.isProximityMonitoringEnabled = false
        NotificationCenter.default.removeObserver(self)
        
        tableView.removeGestureRecognizers()
        
        dprint("====\(self)=====>被释放")
    }
    
    //MARK: - 留给子类处理
    func chatViewInputBarStatus(_ isInput: Bool){}
    func chatViewClickAction(_ action: ChatViewActionType){}
    /// 点击群组红包
    func clickGroupFetchPaper(_ msg: Message){}
    /// 点击好友转账
    func clickFriendTransfer(_ msg: Message){}
    /// 同意好友的自建群邀请
    func clickOwnerFriendInvite(_ msg: Message){}
    /// 别人踩雷（自己发的雷包）
    func otherLandMine(_ msg: Message){}
    /// 处理新消息
    func insertNewMsg(_ msg: Message){}
    /// 加载数据
    func loadListData(){}
    /// 设置新会话的信息
    func setupNewConversationInfo(){}
    
    private func doinitialConversation() {
        let ctype = self.chat_type == .group ? true : false
        var con: Conversation? = SMManager.manager.getSingleChat(cid: chat_id, isGroup: ctype)
        
        var type: ConversationType
        switch self.chat_type {
        case .group:
            type = .group
        case .friend:
            type = .friend
        case .customer:
            type = .customer
        case .admin:
            type = .admin
        }
        
        if con == nil {
            let c = Conversation()
            c.cid = chat_id
            c.type = type.rawValue
            con = c
            SMManager.manager.updateConversation(con: con!)
        } else if con!.type == nil || (con!.type != type.rawValue) {
            con!.type = type.rawValue
            SMManager.manager.updateConversation(con: con!)
        }
        self.conversation = con!
        
        if con!.cavatar == nil {
            // 新创建的会话信息不全 需要子类补齐
            self.setupNewConversationInfo()
        }
    }
    
    //MARK: - override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 初始化会话对象
        self.doinitialConversation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationOfReceiveServerSocket(_:)), name: SMManager.receiveSocketNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationOfMsgSendResultSocket(_:)), name: SMManager.msgSendTimeNotification, object: nil)
        
        layoutUI()
        loadListData()
    }
    
    override func viewSafeAreaInsetsDidChange() {
        if #available(iOS 11.0, *) {
            self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: kIsIphoneX ? 34 : 0, right: 0)
        }
    }
}

extension FEChatVC {
    //MARK: Notification: socket服务器推送的消息
    @objc func notificationOfReceiveServerSocket(_ n: Notification) {
        guard let info = n.userInfo, let msg = info[SMManager.receiveSocketNotification] as? Message else {
            return
        }
       
        if chat_type == .group && msg.body.groupId != chat_id {
            return
        }
        
        if chat_type != .group && !msg.body.groupId.isEmpty && msg.body.senderId != chat_id {
            return
        }
        
        insertNewMsg(msg)
    }
    
    // 消息发送成功反馈消息
    @objc func notificationOfMsgSendResultSocket(_ n: Notification) {
        guard let info = n.userInfo, let sendTime = info[SMManager.msgSendTimeNotification] as? String else {
            return
        }
        
        guard let index = tableView.dataArray.firstIndex(where: {$0.body.sendTime == sendTime && $0.direction == MessageDirection.send.rawValue }) else {
            return
        }
        
        tableView.dataArray[index].status = MessageStatus.success.rawValue
        
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.none)
        
        efScrollToLastCell()
        
        SMManager.manager.updateSendMessageSattus(cid: conversation.cid, time: sendTime, isGroup: chat_type == .group ? true : false)
    }
    
    
    fileprivate func layoutUI() {
        chatView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(0)
            make.bottom.equalTo(chatView.evInputView.snp.top)
        }
        
        activityIndicatorView.snp.makeConstraints { (make) in
            make.centerX.equalTo(chatView)
            make.top.equalTo(74)
        }
        
        
        // 监听用户耳朵靠近手机或者远离手机
        //        NotificationCenter.default.addObserver(self, selector: #selector(FEChatVC.proximitySensorChanged), name: NSNotification.Name.UIDevice.proximityStateDidChangeNotification, object: nil)
        
    }
  
    // 监听用户耳朵靠近手机或者远离手机
    @objc fileprivate func proximitySensorChanged() {
        
        if UIDevice.current.proximityState == true {
            VoiceManager.shared.isProximity(false)
        }else {
            VoiceManager.shared.isProximity(true)
        }
        
    }
    
    // 滚到最后一行
    func efScrollToLastCell() {
        if isAllowAutoScrollToLast == false {
            return
        }
        
        tableView.scrollToRow(at: IndexPath(row: tableView.dataArray.count-1, section: 0), at: UITableView.ScrollPosition.middle, animated: true)
    }
    
    // 根据message 获取 cell
    fileprivate func getCellByMessage(_ message: Message) -> BaseChatCell? {
        
//        if let index = dataArray.firstIndex(of: message) {
//            if index >= 0 && index < dataArray.count {
//                return tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! BaseChatCell?
//            }
//        }
        return nil
    }
}


// MARK: - ChatTableViewDelegate
extension FEChatVC: ChatTableViewDelegate {
    // cell点击事件
    func chatTable(_ chatTable: ChatTableView, cellAction: ChatTableCellAction, msg: Message) {
        switch cellAction {
        case .image:
            // action: 点击图片
            guard let data = msg.body.imageData, let img = UIImage(data: data) else {
                dprint("图片还在下载中。。。")
                return
            }

            let vc = FEShowBigPictureView2(images: [img])
            self.present(vc, animated: true, completion: nil)
            
        case .voice:
            if oldChatVoiceMessage != nil {
//                if message.messageId == oldMessage.messageId {
//                    stopPlaying()
//                    return
//                }
            }
            startPlaying(msg)
        case .sendPaper:
            // action: 点击红包
            clickGroupFetchPaper(msg)
        case .transfer:
            // action: 点击转账
            clickFriendTransfer(msg)
        case .ownerFriendInvite:
            // 自建群好友邀请
            clickOwnerFriendInvite(msg)
        }
    }
    
    // 单击列表table
    func chatTableTapped() {
        // 结束编辑
        chatView.efPackUpInputView()
    }
    
    func chatTableWillBeginDragging(_ chatTable: ChatTableView) {
        // 结束编辑
        chatView.efPackUpInputView()
    }
    
    func chatTableDidEndDecelerating(_ chatTable: ChatTableView) {
        dprint("chatTable.contentOffset.y = \(chatTable.contentOffset.y)")
        if (chatTable.contentOffset.y <= -kNaviBarHeight) {
            chatTable.setContentOffset(CGPoint(x: 0, y: -(kNaviBarHeight)), animated: true)
            loadListData()
        }
    }
}


//MARK: - 发送消息到服务器
extension FEChatVC {
    // 子类实现
    @objc func sendMsgOfText(_ text: String, _ imgData: Data? = nil) {
    }
}

//MARK: - 图片选择 | 拍照
extension FEChatVC {
    open func selectPhoto() {
        FESelectPhotoManager.selectPhoto(with: self) {[weak self] (imgs) in
            if let image = imgs.first {
                self?.uploadImage(image)
            }
        }
    }
    
    
    func uploadImage(_ img: UIImage) {
        // 上传头像
        var item = UploadParam.init()
        item.file = img.jpegData(compressionQuality: 0.8)
        UserCenter.default.uploadFile(item) {[weak self] (res) in
            guard let self = self else {
                return
            }
            
            if let res = res {
                let url = "img"+":::"+res.url
                self.sendMsgOfText(url, item.file)
            } else {
                QMUITips.showInfo("发送失败")
            }
        }
    }
}



// MARK: - ChatViewDelegate(输入消息处理)
extension FEChatVC: ChatViewDelegate {
    func inputBarOutStatus(_ isOut: Bool) {
        chatViewInputBarStatus(isOut)
    }
    
    // 点击更多面板上的按钮(照片，转账，红包...)
    func epActionType(_ action: ChatViewActionType) {
        chatViewClickAction(action)
    }
    
    //MARK: 发送文本消息
    func epSendMessageText(_ text: String) {
        sendMsgOfText(text)
    }
    
    func epSendMessageImage(_ images:[UIImage]?) {

    }
    
    func ePSendMessageVoice(_ path: String? ,duration: Int) {
//
//        if let path = path {
//
//            let message = Message()
//            message.timestamp = String(Int(Date().timeIntervalSince1970))
//            message.direction = MessageDirection.send.rawValue
//
//            let MsgBody = MsgBody()
////            MsgBody.type = MsgBodyType.voice.rawValue
////            MsgBody.voicePath = path
////            MsgBody.voiceDuration = duration
//
//            message.body = MsgBody
//
//            conversation.messages.append(message)
//            RealmManagers.shared.addSynModel(conversation.clone())
//
//            dataArray.append(conversation.messages.last!)
//            tableView.insertRows(at: [IndexPath.init(row: dataArray.count-1, section: 0)], with: UITableView.RowAnimation.bottom)
//
//            efScrollToLastCell()
//
//        }
    }
}

//MARK: 播放声音
extension FEChatVC {
    // 开始播放录音
    fileprivate func startPlaying(_ message:Message) {
        
        //        if message.body.type == MsgBodyType.voice.rawValue {
        //
        //            stopPlaying()
        //            oldChatVoiceMessage = message
        //
        //            if let cell = getCellByMessage(message) as? ChatVoiceCell {
        //
        //                cell.messageAnimationVoiceImageView.startAnimating()
        //
        //                if let range = message.body.voicePath.range(of: "Caches") {
        //                    let path = NSHomeDirectory() + "/Library/" + message.body.voicePath.dropFirst(range.lowerBound.encodedOffset)
        //                    VoiceManager.shared.play(path, {[weak self] in
        //                        self?.stopPlaying()
        //                    })
        //                }
        //
        //            }
        //        }
        
    }
    
    // 停止播放录音
    fileprivate func stopPlaying() {
        
        //        if let oldMessage = oldChatVoiceMessage {
        //            if let oldChatVoiceCell = getCellByMessage(oldMessage) as? ChatVoiceCell {
        //                oldChatVoiceCell.messageAnimationVoiceImageView.stopAnimating()
        //                oldChatVoiceMessage = nil
        //                VoiceManager.shared.stopPlay()
        //            }
        //        }
        
    }
    
}
