//
//  FEChatFriendViewController.swift
//  Red
//
//  Created by MAC on 2019/10/29.
//  Copyright © 2019 MAC. All rights reserved.
//
import QMUIKit

/// 好友聊天界面
class FEChatFriendViewController: FEChatVC {
    var friend: FriendItem?
    
    lazy var topLayerButton: QMUIButton = {
        let v = QMUIButton("充值", image: UIImage(named: "ic_group_charge"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        v.isUserInteractionEnabled = true
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    lazy var bottomLayerButton: QMUIButton = {
        let v = QMUIButton("分享", image: UIImage(named: "ic_group_share"), fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(layerButtonAction(_:)), for: .touchUpInside)
        v.imagePosition = .top
        v.spacingBetweenImageAndTitle = -10.0
        v.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        self.view.addSubview(v)
        self.view.bringSubviewToFront(v)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let f = friend {
            self.title = f.friend_nickname
        }
        
        createUI()
    }
    
    deinit {
        if MessageTipsManager.shared.isContains(self.friend!.friend_id).0 {
            MessageTipsManager.shared.removeTips(self.friend!.friend_id)
            NotificationCenter.default.post(name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
        }
    }
    
    override func chatViewClickAction(_ action: ChatViewActionType) {
        dowithChatViewClickAction(action: action)
    }
    
    override func clickFriendTransfer(_ msg: Message) {
        dowithFriendTransferAction(msg)
    }
    
    override func clickOwnerFriendInvite(_ msg: Message) {
        dowithOwnerFriendInvite(msg)
    }
    
    override func setupNewConversationInfo() {
        self.conversation.cname = self.friend?.friend_nickname
        self.conversation.cavatar = self.friend?.friend_avatar
        SMManager.manager.updateConversation(con: self.conversation)
    }
    
    //MARK: 发送普通聊天消息
    override func sendMsgOfText(_ text: String, _ imgData: Data? = nil) {
        super.sendMsgOfText(text, imgData)
        
        var send = SendSocketModel()
        send.content.friendId = chat_id
        send.content.message = text
        FEWebSocketManager.websocketWriteToServer(send: send)

        // 本地推送
        let msg = Message()
        msg.direction = MessageDirection.send.rawValue
        msg.type = MessageType.friendChat.rawValue
        
        var body = MsgBody()
        body.sendTime = send.sendTime
        body.content = text
        body.avatar = friend?.friend_avatar ?? ""
        body.username = friend?.friend_nickname ?? ""
        body.senderId = friend?.friend_id ?? ""
        
        if let d = imgData {
            body.imageData = d
        }
        
        msg.body = body
        
        insertNewMsg(msg)
    }
    
    //MARK: 好友转账消息
    func sendMsgOfFriendTransfer(_ item: FriendTransferItem) {
        // 本地推送
        let msg = Message()
        msg.status = MessageStatus.success.rawValue
        msg.direction = MessageDirection.send.rawValue
        msg.type = MessageType.friendTransfer.rawValue
        
        var body = MsgBody()
        body.sendTime = String(Int(Date().timeIntervalSince1970))
        body.content = item.content
        body.transfer_amount = item.transfer_amount
        body.id = item.id
        body.avatar = friend?.friend_avatar ?? ""
        body.username = friend?.friend_nickname ?? ""
        body.senderId = friend?.friend_id ?? ""
        msg.body = body
        
        insertNewMsg(msg)
    }

    //MARK: 客服欢迎语
    func insertMsgOfCustomerGreetings()->Message {
        let msg = Message()
        msg.direction = MessageDirection.receive.rawValue
        msg.type = MessageType.friendChat.rawValue
        
        var body = MsgBody()
        body.content = (self.friend?.friend_nickname ?? "") + "很高兴为您服务: ☺☺"
        body.avatar = self.friend?.friend_avatar ?? ""
        body.username = self.friend?.friend_nickname ?? ""
        body.senderId = self.friend?.friend_id ?? ""
        msg.body = body
        return msg
    }
    
    override func insertNewMsg(_ msg: Message) {
        dowithInsertNewMsg(msg)
    }
    
    override func loadListData() {
        dowithLoadListData()
    }
    
    override func chatViewInputBarStatus(_ isInput: Bool) {
        dowithChatViewInputStatusChanged(isInput)
    }
}

extension FEChatFriendViewController {
    @objc func handlerRightButton(_ btn: UIButton) {
        
    }
    
    @objc func layerButtonAction(_ btn: QMUIButton) {
        if btn == topLayerButton {
            // 充值
            let vc = FECenterChargeMoneyViewController()
            self.push(to: vc, animated: true, completion: nil)
        } else {
            // 分享
            let sendUrl: String = User.default.download_url + "&version=\(UIApplication.appVersion)"
            let sm = AppShare(text: "拼手气", image: UIImage(named: "AppIcon")?.jpegData(compressionQuality: 0.5), url: sendUrl)
            let share = ShareView(shareModel: sm)
            share.show()
        }
    }
    
    func createUI() {
        topLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(view.snp.right).offset(-5)
            make.top.equalTo(kNaviBarHeight+20)
            make.size.equalTo(CGSize(width: 50, height: 70))
        }
        bottomLayerButton.snp.makeConstraints { (make) in
            make.right.equalTo(topLayerButton.snp.right)
            make.top.equalTo(topLayerButton.snp.bottom).offset(10)
            make.size.equalTo(topLayerButton.snp.size)
        }
    }
    
    func dowithChatViewInputStatusChanged(_ isInput: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.topLayerButton.isHidden = isInput
            self.bottomLayerButton.isHidden = isInput
        }
    }
    
    //MARK: 消息插入列表处理
    func dowithInsertNewMsg(_ msg: Message) {
        if msg.type == MessageType.getTransfer.rawValue {
            
            guard let index = tableView.dataArray.lastIndex(where: {$0.body.project_id == msg.body.project_id}) else {
                // 没找到对应红包 不显示该提示消息
                return
            }
            
            if index == tableView.dataArray.count - 1 {
                tableView.dataArray.append(msg)
                tableView.insertRows(at: [IndexPath.init(row: index+1, section: 0)], with: .top)
            } else {
                tableView.dataArray.insert(msg, at: index+1)
                tableView.insertRows(at: [IndexPath.init(row: index+1, section: 0)], with: .none)
            }
        }
        else {
            tableView.dataArray.append(msg)
            tableView.insertRows(at: [IndexPath.init(row: tableView.dataArray.count-1, section: 0)], with: UITableView.RowAnimation.none)
            if msg.direction == MessageDirection.send.rawValue {
                // 缓存自己发的消息
                SMManager.manager.saveMessageToConversation(msg)
            }
        }
        
        if self.tableView.isScrollEnabled {
            efScrollToLastCell()
        }
    }
    
    func dowithLoadListData() {
        if self.activityIndicatorView.superview == nil {
            // 已经没有更多数据了
            return
        }
        
        if loadTime == 0, chat_type == .customer {
            tableView.dataArray.insert(self.insertMsgOfCustomerGreetings(), at: 0)
        }
        
        let array = self.conversation.msgs
        let from = loadCount * loadTime
        if from > array.count - 1 {
            // 没有更多消息了
            self.activityIndicatorView.stopAnimating()
            self.activityIndicatorView.removeFromSuperview()
            return
        }
        
        if from > 0 {
            self.activityIndicatorView.startAnimating()
        }
        
        self.tableView.isScrollEnabled = false
        
        let end = array.count - 1 - from
        let offsetValue = end - loadCount
        let start =  offsetValue >= 0 ? offsetValue : 0
        let insertMsgs = (start...end).map({array[$0]})
        
        tableView.dataArray = insertMsgs + tableView.dataArray
        
        loadTime += 1
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.activityIndicatorView.stopAnimating()
            self.tableView.isScrollEnabled = true
            if offsetValue <= 0 {
                self.activityIndicatorView.removeFromSuperview()
            }
        }
        
        self.tableView.reloadData()
        let rect = self.tableView.rectForRow(at: IndexPath(row: insertMsgs.count - 1, section: 0))
        self.tableView.setContentOffset(CGPoint(x: 0, y: rect.origin.y), animated: false)
    }
        
    
    /// 点击更多面板的事件处理
    func dowithChatViewClickAction(action: ChatViewActionType) {
        switch action {
        case .photo:
            self.selectPhoto()
        case .transfer:
            self.dowithFriendTransfer()
        case .redPackage:
            let vc = FECenterRedPackageViewController()
            self.push(to: vc, animated: true, completion: nil)
        case .charge:
            let vc = FECenterChargeMoneyViewController()
            self.push(to: vc, animated: true, completion: nil)
        case .customer:
            ContactCustomer.startContact(currentvc: self, enterChat: true)
        default :
            break
        }
    }
    
    //MARK: 处理请求：同意好友的自建群邀请
    func dowithOwnerFriendInvite(_ msg: Message) {
        if msg.body.friendInviteStatus == "2" {
            self.refreshTableAfterClickOwnerFriendInvite(msg)
            return
        }
        
        let load = QMUITips.showLoading(in: self.view)
        //TODO: group_id
        HttpClient.default.ownerGroupApplyRoom(group_id: msg.body.id, parent_id: chat_id) { (flag, wmsg) in
            load.hide(animated: true)
            if flag {
                self.refreshTableAfterClickOwnerFriendInvite(msg)
                // 同意加入群聊
                NotificationCenter.default.post(name: NSNotification.Name.refreshHomeOwnerGroupList, object: nil) // 刷新首页自建群列表
            } else {
                QMUITips.showInfo(wmsg.text, in: self.view)
            }
        }
    }
    
    private func refreshTableAfterClickOwnerFriendInvite(_ msg: Message) {
        if let index = self.tableView.dataArray.firstIndex(where: {$0.body.sendTime == msg.body.sendTime}) {
            // 更新列表
            self.tableView.dataArray[index] = msg
            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            // 更新本地缓存
            SMManager.manager.updateSingleMessage(cid: chat_id, msg: msg, isGroup: false)
        }
    }
    
    //MARK: 领朋友转账 或 查看自己发包信息
    func dowithFriendTransferAction(_ msg: Message) {
        if msg.direction == MessageDirection.receive.rawValue {
            //MARK: 点击领取好友转账
        }
        else if msg.direction == MessageDirection.send.rawValue {
            //MARK: 查看转账情况(跳到转账详情界面)
        }
        let vc = FEFriendSeeTransferDetailViewController()
        vc.msg = msg
        self.push(to: vc, animated: true, completion: nil)
    }
    
    // 转账成功消息
    func dowithFriendTransfer() {
        let vc = FEFriendTransferViewController()
        vc.friendItem = self.friend
        vc.transferHandler = {[weak self] (item) in
            guard let self = self else {
                return
            }
            self.sendMsgOfFriendTransfer(item)
        }
        self.push(to: vc, animated: true, completion: nil)
    }
}
