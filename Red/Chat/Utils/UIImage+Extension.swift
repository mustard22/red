//
//  UIImage+Extension.swift
//  Red
//
//  Created by MAC on 2019/8/12.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIImage 拓展
extension UIImage {

    // yl_tag
    var yl_tag : String? {
        
        get {
            return accessibilityIdentifier
        }
        
        set(newVal) {
            accessibilityIdentifier = newVal
        }
    }
    
}
