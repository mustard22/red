//
//  FEWebSocket.swift
//  Red
//
//  Created by MAC on 2020/1/2.
//  Copyright © 2020 MAC. All rights reserved.
//
import Starscream

class FEWebSocketDelegate: NSObject, WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        dprint("socket connect success")
        FEWebSocketManager.startSocketHeart()
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        if let err = error as? WSError {
            if err.code == 401 {
                // 登录过期
                NotificationCenter.default.post(name: NSNotification.Name.onForceOffline, object: nil, userInfo: [NSNotification.Name.onForceOffline: err.message])
                return
            }
        }
        dprint("socket disconnect: \(error.debugDescription)")
        
        FEWebSocketManager.cycleReconnect()
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
//        dprint(Thread.current.debugDescription)
        dprint("socket data: \(text)")
        switch text {
        case "LOSE":
            FEWebSocketManager.immediateReconnect()
        case "PONG":
            FEWebSocketManager.receivePongFlag()
            break
        default:
            SMManager.manager.socketJsonToMessage(text, nil)
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
//        let str = String(data: data, encoding: String.Encoding.utf16)
    }
}


//MARK: - FEWebSocketManager
final class FEWebSocketManager: NSObject {
    public static var shared: FEWebSocketManager? = nil
    
    private var webSocketDelegate = FEWebSocketDelegate()
    private var webSocket: WebSocket?
    private var reachability: Reachability?
    private var heartTimer: Timer?
    private var reconnectTimer: Timer?
    private var recount = 0
    
    private var pingFlag: Bool = false
    private var sendMsgArray: [String] = []
    
    
    override init() {
        reachability = Reachability()
        recount = 0
    }
    
    deinit {
        webSocket?.disconnect()
        webSocket?.delegate = nil
        webSocket = nil
        
        self.stopHeart()
        
        reachability = nil
        
        self.stopCycleReconnectTimer()
        
        dprint("销毁websocketManager")
    }
}

//MARK: - private methods
extension FEWebSocketManager {
    /// 重连
    private func reconnect() {
        webSocket?.disconnect()
        webSocket?.connect()
    }
    
    /// send PING
    private func sendPingFlag() {
        self.pingFlag = false
        self.webSocket?.write(string: "PING")
        dprint("PING")
    }

    // MARK: 发送消息到服务器
    private func writeToServer(send: SendSocketModel) {
        guard let writeString = send.toJSONString() else {
            dprint("SendSocketModel to jsonString failed")
            return
        }
        
        if reachability?.connection == Reachability.Connection.none || !(webSocket!.isConnected) {
            // 无网络 || 未连接
            sendMsgArray.append(writeString)
        } else {
            webSocket?.write(string: writeString)
            startHeart()
        }
    }
    
    private func retryWriteToServer() {
        _ = sendMsgArray.map { (value) in
            self.webSocket?.write(string: value)
            self.startHeart()
        }
        
        sendMsgArray.removeAll()
    }
    
    
    //MARK: 心跳监测
    private func startHeart() {
        self.stopHeart()
        
        self.sendPingFlag()
        heartTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: {[weak self] (timer) in
            self?.sendPingFlag()
        })
    }
    
    private func stopHeart() {
        if let _ = heartTimer, heartTimer!.isValid {
            heartTimer!.invalidate()
            heartTimer = nil
        }
    }
    
    //MARK: 循环重连（每隔5秒）
    private func startCycleReconnectTimer() {
        if recount > 0 {
            return
        }
        
        self.reconnect() // 立即重连一次
        dprint("开始循环检测重连状态")
        self.stopCycleReconnectTimer()
        
        recount = 1
        reconnectTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(reconnectTimerActive), userInfo: nil, repeats: true)
    }
    
    private func stopCycleReconnectTimer() {
        if let _ = reconnectTimer, reconnectTimer!.isValid {
            reconnectTimer!.invalidate()
            reconnectTimer = nil
        }
    }
    
    @objc private func reconnectTimerActive() {
        recount += 1
        dprint("重连次数count = \(recount)")
        if let ws = webSocket, ws.isConnected {
            dprint("已连接，停止循环检测重连状态")
            self.stopCycleReconnectTimer()
            recount = 0
        } else {
            self.reconnect()
        }
    }
}

//MARK: - public static methods
extension FEWebSocketManager {
    /// 登录时初始化
    static func initialWebSocket() {
        shared = FEWebSocketManager()
        shared!.__initialWebSocket()
    }
    
    private func __initialWebSocket() {
        guard let areaName = HttpHost.rootUrl.components(separatedBy: "//").last else {
            return
        }
        
        let url =  "wss://" + "\(areaName)" + "/ws?token=" + "\(HttpClient.default.tokenData?.1 ?? "")"
        webSocket = WebSocket(url: URL(string: url)!)
        webSocket?.delegate = webSocketDelegate
        webSocket?.connect()
    }
    
    /// 退出时销毁
    static func destroyWebSocket() {
        shared?.stopHeart()
        shared?.stopCycleReconnectTimer()
        shared = nil
    }
    
    static func startSocketHeart() {
        shared?.startHeart()
    }
    
    // 立即重新连接
    static func immediateReconnect() {
        shared?.reconnect()
    }
    
    //MARK: 连接断开 循环监测
    static func cycleReconnect() {
        shared?.startCycleReconnectTimer()
    }
    
    static func receivePongFlag() {
        shared?.pingFlag = true
    }
    
    static func websocketWriteToServer(send: SendSocketModel) {
        shared?.writeToServer(send: send)
    }
}

