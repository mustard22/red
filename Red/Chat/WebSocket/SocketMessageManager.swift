//
//  SocketMessageManager.swift
//  Red
//
//  Created by MAC on 2019/12/14.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import CleanJSON

//MARK: -
//MARK: ------------------------------------------
//MARK: socket message manager
//MARK: ------------------------------------------
final class SMManager: NSObject {
    private let jsonEncoder = JSONEncoder()
    private let jsonDecoder = CleanJSONDecoder()
    
    private let mutex = NSLock()
    
    public lazy var msgQueue: DispatchQueue = DispatchQueue(label: "com.red.msg.queue")
    
    class var manager: SMManager {
        struct Shared {
            static let instance = SMManager()
        }
        return Shared.instance
    }
    
    /// 临时存放管理员上线下线消息
    private var __onlineAdmins: [Message] = []
    public var onlineAdmins: [Message] {
        return __onlineAdmins
    }
}


//MARK: - notifications
extension SMManager {
    /// socket 消息
    static let receiveSocketNotification = Notification.Name.init(rawValue: "receiveSocketNotification")
    /// 刷新最近会话通知
    static let refreshRecentChatsNotification = Notification.Name.init(rawValue: "refreshRecentChatsNotification")
    /// 消息确认
    static let msgSendTimeNotification = Notification.Name.init(rawValue: "msgSendTimeNotification")
    /// 加好友请求（别人加自己）
    static let friendRequestNotification = Notification.Name.init(rawValue: "friendRequestNotification")
    /// 对方同意加好友请求（自己加别人，对方同意）
    static let friendAgreeRequestNotification = Notification.Name.init(rawValue: "friendAgreeRequestNotification")
    
    /// 管理员上线
    static let adminOnlineNotification = Notification.Name.init(rawValue: "adminOnlineNotification")
    /// 管理员下线
    static let adminOfflineNotification = Notification.Name.init(rawValue: "adminOfflineNotification")
    
    /// 被踢出群聊消息
    static let outedOwnerGroupNotification = Notification.Name.init(rawValue: "outedOwnerGroupNotification")
    /// 编辑群组信息 || 群主强邀进群， 刷新自建群列表消息
    static let refreshOwnerGroupNotification = Notification.Name.init(rawValue: "refreshOwnerGroupNotification")
}


// MARK: - Public Methods
extension SMManager {
    
    func socketJsonToItem<T: Codable>(_ type: T.Type, _ json: String) -> T? {
        guard let data = json.data(using: .utf8) else {
            __doprint(some: "json to data failed.")
            return nil
        }
        
        guard let item = try? self.jsonDecoder.decode(T.self, from: data) else {
            __doprint(some: "data to \(T.self) failed.")
            return nil
        }
        return item
    }
    
    /// socket data -> Message
    func socketJsonToMessage(_ json: String, _ handler:((_ msg: Message?)->Void)?) {
        msgQueue.async {
            dprint(Thread.current.debugDescription)
            
            self.__jsonToMessage(json) { (msg) in
                DispatchQueue.main.async {
                    handler?(msg)
                }
            }
        }
    }
    
    //MARK: 获取单个会话
    func getSingleChat(cid: String, isGroup: Bool = true) -> Conversation? {
        let key: MMKVCacheKey = isGroup ? .groupConversation(cid) : .friendChats(cid)
        return __getSingleChat(key: key)
    }
    
    //MARK: 获取所有的好友会话
    func getRecentFriendChats() -> [Conversation]? {
        return MMKVManager.shared.getCache([Conversation].self, .recentFriendChats)
    }
    
    //MARK: save msg
    func saveMessageToConversation(_ msg: Message) {
        msgQueue.async {
            self.__insertMsgToConversation(msg)
        }
    }
    
    // update send msg's status
    func updateSendMessageSattus(cid: String, time: String, isGroup: Bool = true) {
        let key: MMKVCacheKey = isGroup ? MMKVCacheKey.groupConversation(cid) : MMKVCacheKey.friendChats(cid)
        __refreshSendMessageStatus(key: key, time: time)
    }
    
    // 更新消息
    func updateSingleMessage(cid: String, msg: Message, isGroup: Bool = true) {
        let key: MMKVCacheKey = isGroup ? MMKVCacheKey.groupConversation(cid) : MMKVCacheKey.friendChats(cid)
        __refreshSingleMessage(key: key, msg: msg)
    }
    
    func updateConversation(con: Conversation) {
        __updateConversation(con: con)
    }
    
    // delete
    func deleteConversation(cid: String, isGroup: Bool = true) {
        __deleteSingleChat(cid: cid, isGroup: isGroup)
    }
}

//MARK: - Private Methods
extension SMManager {
    
    // delete
    private func __deleteSingleChat(cid: String, isGroup: Bool) {
        msgQueue.async {
            self.mutex.lock()
            
            if isGroup {
                MMKVManager.shared.remove(.groupConversation(cid))
            } else {
                MMKVManager.shared.remove(.friendChats(cid))
                
                if let cons = MMKVManager.shared.getCache([Conversation].self, .recentFriendChats) {
                    let recents = cons.filter({$0.cid != cid})
                    MMKVManager.shared.setCache(recents, .recentFriendChats)
                }
            }
            
            self.mutex.unlock()
        }
    }
    
    /// 获取单个会话
    private func __getSingleChat(key: MMKVCacheKey) -> Conversation? {
        self.mutex.lock()
        
        let con = MMKVManager.shared.getCache(Conversation.self, key)
        
        self.mutex.unlock()
        return con
    }
    
    private func __jsonToMessage(_ json: String, _ handler:((_ msg: Message?)->Void)) {
        guard let data = json.data(using: .utf8) else {
            __doprint(some: "json to data failed.")
            handler(nil)
            return
        }
        
        guard let msg = try? self.jsonDecoder.decode(Message.self, from: data) else {
            __doprint(some: "data to Message failed.")
            handler(nil)
            return
        }
        
        //MARK: 使用过程中token过期 或 被其他设备挤掉
        if msg.type == MessageType.tokenExpired.rawValue || msg.type == MessageType.offline.rawValue {
            var err_msg = "会话过期，请重新登录"
            if !msg.body.content.isEmpty {
                err_msg = msg.body.content
            }
            
            __postNotification(name: NSNotification.Name.onForceOffline, userInfo: [NSNotification.Name.onForceOffline: err_msg])
            
            return
        }
            
        // 设置消息时间（若未携带时间戳，赋值当前推送过来的时间戳）
        if msg.body.sendTime.isEmpty {
            msg.body.sendTime = String(Int(Date().timeIntervalSince1970))
        }
        
        // 设置服务器类型消息的方向
        msg.direction = MessageDirection.receive.rawValue
        
        // 该消息是否需要缓存本地
        var needCache = true
        
        switch msg.type {
        case MessageType.sendPaper.rawValue :
            // 接龙红包：推送时间后面拼接当前本地时间
            if !msg.body.push_time.isEmpty, !msg.body.expire_time.isEmpty {
                msg.body.push_time = msg.body.push_time + "," + msg.body.sendTime
            }
            
            if msg.body.senderId == User.default.user_id {
                // 处理发包方向（自己发的包）
                msg.direction = MessageDirection.send.rawValue
                // 标记消息状态
                msg.status = MessageStatus.success.rawValue
            }
            
        case MessageType.fetchPaper.rawValue,
             MessageType.luckyMoney.rawValue,
             MessageType.thunderMoney.rawValue,
             MessageType.solitaireTipsAfterEnd.rawValue,
             MessageType.solitaireEnded.rawValue,
             MessageType.cattleLucky.rawValue :
            /// 抢包消息; 幸运奖励消息; 中雷奖励; 接龙红包的最佳、最差手气; 接龙红包游戏结束; 牛包房对局
            msg.direction = MessageDirection.server.rawValue
            
        case MessageType.comeGroupRoom.rawValue :
            /// 进群欢迎语
            msg.direction = MessageDirection.server.rawValue
            needCache = false
            
        case MessageType.getTransfer.rawValue :
            /// 领取转账消息
            msg.direction = MessageDirection.server.rawValue
            
        case MessageType.ownerMuted.rawValue :
            /// 禁言
            msg.direction = MessageDirection.server.rawValue
            needCache = false
            
        case MessageType.ownerBlacklist.rawValue :
            /// 黑名单 踢群
            msg.direction = MessageDirection.server.rawValue
            if msg.body.status == "1" {
                /// 踢出群聊消息(发送通知到首页)
                __postNotification(name: SMManager.outedOwnerGroupNotification, userInfo: [SMManager.outedOwnerGroupNotification: msg])
            }
            
            needCache = false
            
        case MessageType.addFriend.rawValue :
            /// 加好友请求消息
            needCache = false
            __postNotification(name: SMManager.friendRequestNotification, userInfo: nil)
            return
        
        case MessageType.friendAgreeRequest.rawValue :
            /// 对方同意加好友请求
            __postNotification(name: SMManager.friendAgreeRequestNotification, userInfo: nil)
            needCache = false
            return
        
        case MessageType.sendTime.rawValue :
            /// 消息确认(服务器发的时间戳)
            __postNotification(name: SMManager.msgSendTimeNotification, userInfo: [SMManager.msgSendTimeNotification: msg.body.sendTime])
            needCache = false
            return
            
        case MessageType.adminOnline.rawValue :
            /// 后台管理员上线
            if self.__onlineAdmins.contains(where: {$0.body.id == msg.body.id}) {
                return
            }
        
            self.__onlineAdmins.append(msg)
            __dowithAdminNotification(true, msg.body.id)
            
            needCache = false
            return
            
        case MessageType.adminOffline.rawValue :
            /// 后台管理员下线消息
            guard let index = self.__onlineAdmins.firstIndex(where: {$0.body.id == msg.body.id}) else {
                return
            }
            self.__onlineAdmins.remove(at: index)
            __dowithAdminNotification(true, msg.body.id)
            
            needCache = false
            return
            
        case MessageType.ownerFriendInvite.rawValue :
            /// 自建群好友邀请消息设置默认值
            msg.body.friendInviteStatus = "0"
            
        case MessageType.refreshOwnerGroup.rawValue :
            /// 刷新首页自建群列表
            __postNotification(name: SMManager.refreshOwnerGroupNotification, userInfo: nil)
            needCache = false
        
        case MessageType.friendAgreeRequest.rawValue :
            /// 对方已经同意加好友请求
            needCache = false

        default : break
        }
        
        handler(msg)
        
        // 推送到聊天界面
        __postMessage(msg: msg)
        
        if !needCache {
            return
        }
        
        // 消息本地缓存
        __insertMsgToConversation(msg)
    }
    
    // 处理管理员的上下线消息
    private func __dowithAdminNotification(_ isOnline: Bool, _ uid: String) {
        __postNotification(name: isOnline ? SMManager.adminOnlineNotification : SMManager.adminOfflineNotification, userInfo: ["uid": uid])
    }
    
    private func __postMessage(msg: Message) {
        // 好友发送的转账或文本消息
        if (msg.type == MessageType.friendChat.rawValue || msg.type == MessageType.friendTransfer.rawValue || msg.type == MessageType.ownerFriendInvite.rawValue),
            msg.direction == MessageDirection.receive.rawValue {
            // 好友消息提醒
            DispatchQueue.main.async {
                MessageTipsManager.shared.addTips(msg)
            }
        }
        // 群组发包
        else if msg.type == MessageType.sendPaper.rawValue || msg.type == MessageType.groupChat.rawValue {
            DispatchQueue.main.async {
                MessageTipsManager.shared.addTips(msg, .group)
            }
        }
        
        // 该通知发送到当前的聊天界面
        __postNotification(name: SMManager.receiveSocketNotification, userInfo: [SMManager.receiveSocketNotification: msg])
    }
    
    // send notificaiton
    private func __postNotification(name: NSNotification.Name , userInfo: [AnyHashable : Any]? = nil) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
        }
    }
    
    private func __refreshSendMessageStatus(key: MMKVCacheKey, time: String) {
        msgQueue.async {
            if let con = self.__getSingleChat(key: key), let index = con.msgs.firstIndex(where: {$0.body.sendTime == time}) {
                con.msgs[index].status = MessageStatus.success.rawValue
                MMKVManager.shared.setCache(con, key)
            }
        }
    }
    
    private func __refreshSingleMessage(key: MMKVCacheKey, msg: Message) {
        msgQueue.async {
            if let con = self.__getSingleChat(key: key), let index = con.msgs.firstIndex(where: {$0.body.sendTime == msg.body.sendTime}) {
                con.msgs[index] = msg
                MMKVManager.shared.setCache(con, key)
            }
        }
    }
    
    private func __insertMsgToConversation(_ msg: Message) {
        if msg.body.senderId == "", msg.body.groupId == "" {
            return
        }
        
        // 消息加入会话(普通消息，红包，转账，领取转账，踩雷赔付，中雷奖励，幸运奖励, 接龙红包消息)
        var con: Conversation!
        if msg.body.groupId.count > 0 {
            // 群组消息
            let cid = msg.body.groupId
            if let c = getSingleChat(cid: cid) {
                con = c
            } else {
                con = Conversation()
                con.cid = cid
                con.type = 0
            }
            __insertMsgToGroupConersation(con, msg)
        } else {
            // 好友消息
            let cid = msg.body.senderId
            if let c = getSingleChat(cid: cid, isGroup: false) {
                con = c
            } else {
                con = Conversation()
                con.cid = cid
                con.type = 1
                
                con.cavatar = msg.body.avatar
                con.cname = msg.body.username
            }
            __insertMsgToFriendConersation(con, msg)
        }
    }
        
    //MARK: 插入消息群组会话
    private func __insertMsgToGroupConersation(_ con: Conversation, _ msg: Message) {
        switch msg.type {
        case MessageType.fetchPaper.rawValue, MessageType.luckyMoney.rawValue,  MessageType.thunderMoney.rawValue, MessageType.cattleLucky.rawValue, MessageType.solitaireTipsAfterEnd.rawValue:
            guard let index = con.msgs.lastIndex(where: {$0.body.project_id == msg.body.project_id}) else {
                return
            }
            if index == con.msgs.count-1 {
                con.msgs.append(msg)
            } else {
                con.msgs.insert(msg, at: index+1)
            }
        case MessageType.comeGroupRoom.rawValue:
            // 不存储进房间欢迎语
            return
        default:
            con.msgs.append(msg)
        }
        __updateConversation(con: con)
    }
        
    private func __insertMsgToFriendConersation(_ con: Conversation, _ msg: Message) {
        switch msg.type {
        case MessageType.getTransfer.rawValue:
            guard let index = con.msgs.lastIndex(where: {$0.body.id == msg.body.id}) else {
                return
            }
            if index == con.msgs.count-1 {
                con.msgs.append(msg)
            } else {
                con.msgs.insert(msg, at: index+1)
            }
        default:
            con.msgs.append(msg)
        }
        __updateConversation(con: con)
    }
    
    // update
    func __updateConversation(con: Conversation) {
        self.mutex.lock()
        
        // 群组会话
        if con.type == 0 {
            MMKVManager.shared.setCache(con, .groupConversation(con.cid))
        } else {
            MMKVManager.shared.setCache(con, .friendChats(con.cid))
            
            var cons = MMKVManager.shared.getCache([Conversation].self, .recentFriendChats) ?? [con]
            if cons.filter({$0.cid == con.cid}).count == 0 {
                // 新会话 放到最前面
                cons.insert(con, at: 0)
            }
            let recents = cons.map({(item) -> Conversation in
                if item.cid != con.cid {
                    return item
                }
                
                if let last = con.msgs.last {
                    // 最近会话列表 （只拿最新的一条）
                    item.msgs.removeAll()
                    item.msgs.append(last)
                    con.msgs = item.msgs
                }
                return con
            })
            
            MMKVManager.shared.setCache(recents, .recentFriendChats)
            
            let freshs = recents.filter({$0.cid == con.cid})
            guard let item = freshs.first else {
                return
            }
            __postNotification(name: SMManager.refreshRecentChatsNotification, userInfo: [SMManager.refreshRecentChatsNotification: item])
        }
        
        self.mutex.unlock()
    }
    
    private func __doprint(some: Any) {
        dprint("\(#file) \(#function) [\(#line)]: \(some)")
    }
}
