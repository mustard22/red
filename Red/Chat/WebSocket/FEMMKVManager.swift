//
//  FEMMKVManager.swift
//  Red
//
//  Created by MAC on 2020/1/2.
//  Copyright © 2020 MAC. All rights reserved.
//

import MMKV

enum MMKVCacheKey {
    /// 群组会话key
    case groupConversation(String)
    
    /// 群组消息提醒
    case groupMessageTips
    
    /// 好友会话key
    case friendChats(String)
    
    /// 好友消息提醒
    case friendMessageTips
    
    /// 后台管理员key
    case admins
    
    /// 最近会话(消息列表使用)
    case recentFriendChats
    
    /// 上次发包的钱数
    case lastSendPaperMoney
    
    var rawValue: String {
        switch self {
        case .groupConversation(let key):
            return "groupConversation-\(key)"
        case .groupMessageTips:
            return "groupMessageTips"
        case .admins:
            return "admins"
        case .friendChats(let key):
            return "friendChats-\(key)"
        case .friendMessageTips:
            return "friendMessageTips"
        case .recentFriendChats:
            return "recentFriendChats"
        case .lastSendPaperMoney:
            return "lastSendPaperMoney"
        }
    }
}

enum MMKVCacheSize {
    case GB(Double)
    case MB(Double)
    case KB(Double)
}

//MARK: - 使用mmkv缓存数据
final class MMKVManager: NSObject {
    //MARK: Private
    private let bundleIdentifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String ?? "com.slschat"
    private let cryptKey = "MMKVManager"+"CryptKey"
    private var mmkv: MMKV?
    
    private let jsonEncoder = JSONEncoder()
    private let jsonDecoder = JSONDecoder()
    
    //MARK: static
    public static let shared = MMKVManager()
}

//MARK: -
extension MMKVManager: MMKVHandler {
    func onMMKVCRCCheckFail(_ mmapID: String!) -> MMKVRecoverStrategic {
        dprint("mmkv crc<\(mmapID!)> check failed.")
        return MMKVOnErrorRecover
    }
    
    func onMMKVFileLengthError(_ mmapID: String!) -> MMKVRecoverStrategic {
        dprint("mmkv file<\(mmapID!)> length error.")
        return MMKVOnErrorRecover
    }
    
    func mmkvLog(with level: MMKVLogLevel, file: UnsafePointer<Int8>!, line: Int32, func funcname: UnsafePointer<Int8>!, message: String!) {
        dprint("level: \(level), file: \(file!), msg: \(message ?? "")")
    }
}

//MARK: -
extension MMKVManager {
    private func __doprint(some: Any) {
        dprint("\(#file) \(#function) [\(#line)]: \(some)")
    }
    
    //MARK: static methods
    static func initial(_ userId: String) {
        let mmapID = shared.bundleIdentifier + "-" + userId
//        let ckey = shared.cryptKey + userId
        guard let mmkv = MMKV(mmapID: mmapID) else {
            shared.__doprint(some:"mmkv初始化失败")
            return
        }
        
        guard MMKV.isFileValid(for: mmapID) else {
            shared.__doprint(some:"file<\(mmapID)> is not valid.")
            return
        }
        
        #if DEBUG
        MMKV.setLogLevel(MMKVLogDebug)
        #else
        MMKV.setLogLevel(MMKVLogInfo)
        #endif
        
        shared.mmkv = mmkv
    }
    
    /// 关闭mmkv
    func destory() {
        self.mmkv?.close()
        self.mmkv = nil
    }
    
    func allCacheSize() -> String {
        guard let mm = self.mmkv else {
            return "0KB"
        }
        let s = caculateCacheSize(size: mm.actualSize())
        switch s {
        case .GB(let value):
            return "\(value)GB"
        case .MB(let value):
            return "\(value)MB"
        case .KB(let value):
            return "\(value)KB"
        }
    }
    
    private func caculateCacheSize(size: Int) -> MMKVCacheSize {
        
        // GB
        let GB = 1024*1024*1024, MB = 1024*1024, KB = 1024
        let csize = size
        if csize >= GB {
            let gb = Double(csize/GB) + Double(csize % GB / GB)
            return .GB(gb)
        } else if csize >= MB {
            let mb = Double(csize/MB) + Double(csize % MB / MB)
            return .MB(mb)
        } else if csize >= KB {
            let kb = Double(csize/KB) + Double(csize % KB / KB)
            return .KB(kb)
        } else {
            return .KB(Double(csize % KB / KB))
        }
    }
    
    //MARK: instance methods
    func setCache<T: Codable>(_ value: T, _ key: MMKVCacheKey) {
        guard let data = try? jsonEncoder.encode(value) else {
            __doprint(some:"\(T.self) to data failed")
            return
        }
        
        self.remove(key)
        self.mmkv?.set(data, forKey: key.rawValue)
    }
    
    func getCache<T: Codable>(_ value: T.Type, _ key: MMKVCacheKey) -> T? {
        guard let data = self.mmkv?.data(forKey: key.rawValue) else {
            return nil
        }
        let res = try? jsonDecoder.decode(T.self, from: data)
        return res
    }
    
    func getCache2<T: Codable>(_ value: T.Type, _ key: String) -> T? {
        guard let data = self.mmkv?.data(forKey: key) else {
            return nil
        }
        let res = try? jsonDecoder.decode(T.self, from: data)
        return res
    }
    
    func remove(_ key: MMKVCacheKey) {
        self.mmkv?.removeValue(forKey: key.rawValue)
    }
    
    func remove(_ keys: [MMKVCacheKey]) {
        self.mmkv?.removeValues(forKeys: keys.map({$0.rawValue}))
    }
    
    func removeAllCache() {
        guard let m = self.mmkv else {
            return
        }
        m.clearAll()
    }
    
}
