//
//  UIButtonExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
public extension UIButton {
    
    
    
    /// 设置按钮为主题适应ghost 样式
    func setGhostStyle(_ color: UIColor? = Color.tint) {
        setTitleColor(color, for: .normal)
        layer.cornerRadius = 4
        layer.borderWidth = 1
        backgroundColor = color?.qmui_transition(to: UIColor.white, progress: 0.9)
        if let qBtn = self as? QMUIButton {
            qBtn.highlightedBackgroundColor = color?.qmui_transition(to: UIColor.white, progress: 0.75)
            qBtn.highlightedBorderColor = backgroundColor?.qmui_transition(to: color, progress: 0.9)
        }
        layer.borderColor = backgroundColor?.qmui_transition(to: color, progress: 0.5).cgColor
        
    }
    
    /// 设置按钮为主题适应渐变样式
    ///
    /// - Parameters:
    ///   - size: 按钮大小
    ///   - offset: 阴影偏移
    func setGradationStyle(with size: CGSize,shadow offset: CGSize = CGSize.zero, colors: [UIColor]? = nil) {
        let tintColor1 = colors?.first ?? Color.tint
        let tintColor2 = colors?.last ?? Color.tint2
        setTitleColor(ThemeManager.default.configuration.whiteColor, for: .normal)
        self.size = CGSize(width: size.width + abs(offset.width), height: size.height + abs(offset.height))
        if let qBtn = self as? QMUIButton {
            qBtn.adjustsButtonWhenHighlighted = false
        }
        
        var normalImage = UIImage(gradientColors: [tintColor1, tintColor2],
                                  size: CGSize(width: size.width, height: size.height))?.qmui_image(withClippedCornerRadius: size.height)
        var disabledImage = UIImage.qmui_image(with: Color.disabled,
                                               size: CGSize(width: size.width, height: size.height),
                                               cornerRadius: size.height)
        var highlightedImage = UIImage(gradientColors: [tintColor1.qmui_transition(to: Color.black, progress: 0.15),
                                                        tintColor2.qmui_transition(to: Color.black, progress: 0.15)],
                                       size: CGSize(width: size.width, height: size.height))?.qmui_image(withClippedCornerRadius: size.height)
        if offset.height != 0 || offset.width != 0 {
            normalImage = normalImage?.withShadow(color: tintColor2.withAlphaComponent(0.6),
                                                  offset: offset,
                                                  blur: 5,
                                                  cornerRadius: size.height)
            disabledImage = disabledImage?.withShadow(color: Color.disabled.withAlphaComponent(0.6),
                                                      offset: offset,
                                                      blur: 5,
                                                      cornerRadius: size.height)
            highlightedImage = highlightedImage?.withShadow(color: tintColor2.withAlphaComponent(0.6),
                                                            offset: offset,
                                                            blur: 5,
                                                            cornerRadius: size.height)
        }
        setBackgroundImage(normalImage, for: .normal)
        setBackgroundImage(disabledImage, for: .disabled)
        setBackgroundImage(highlightedImage, for: UIControl.State.highlighted)
    }
    
    /// 快速添加单击事件
    func addTouchUp(action: Selector, target: Any?) {
        addTarget(target, action: action, for: .touchUpInside)
    }
}

public extension UIButton {
    
    /// 快速初始化方法
    ///
    /// - Parameters:
    ///   - title: 按钮文字
    ///   - image: 按钮图像名称
    convenience init(_ title: String? = nil, image: String? = nil) {
        self.init(frame: CGRect.zero)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        setTitle(title, for: UIControl.State.normal)
        guard let name = image else { return }
        let imageFile = UIImage.image(with: name)
        setImage(imageFile, for: UIControl.State.normal)
    }
    
    func setImage(with name: String,for state: UIControl.State ) {
        let imageFile = UIImage.image(with: name)
        setImage(imageFile, for: state)
    }
    
    convenience init(_ title: String? = nil, image: UIImage? = nil, fontSize: CGFloat = 17) {
        self.init(frame: CGRect.zero)
        self.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        setTitle(title, for: UIControl.State.normal)
        setImage(image, for: UIControl.State.normal)
    }
    
    /// 快速初始化
    ///
    /// - Parameters:
    ///   - title: 按钮文字
    ///   - fontSize: 字体大小
    convenience init(_ title: String? = nil, fontSize: CGFloat = 17) {
        self.init(frame: CGRect.zero)
        self.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        setTitle(title, for: UIControl.State.normal)
    }
}

public extension UIButton {
    var title: String {
        return (self.titleLabel?.text).or("")
    }
}
