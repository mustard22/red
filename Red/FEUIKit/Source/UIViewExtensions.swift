//
//  UIViewExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
struct Scale {
    static let width: CGFloat = {
        min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
    }()
    
    static let height: CGFloat = {
        max(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
    }()
    
    static let scale: CGFloat = {
        let screenScale = Scale.width / 375.0
        return screenScale
    }()
}

public extension Double {
    var px: CGFloat {
        return flat(CGFloat(self) / 2 * Scale.scale)
    }
}

public extension Int {
    var px: CGFloat {
        return flat(CGFloat(self) / 2 * Scale.scale)
    }
}

public extension UIView {
    convenience init(size: CGSize) {
        self.init(frame: CGRectMakeWithSize(size))
    }
    
    /// 删除所有子视图
    func removeAllSubviews() {
        subviews.forEach({ $0.removeFromSuperview() })
    }
    
    /// 删除所有手势
    func removeGestureRecognizers() {
        gestureRecognizers?.forEach(removeGestureRecognizer)
    }
    
    /// 批量添加子视图
    ///
    /// - Parameter views: 子视图
    func addSubViews(_ views: UIView ...) {
        views.forEach(addSubview)
    }
    
    /// 批量添加子视图
    ///
    /// - Parameter views: 子视图
    func addSubViews(_ views: [UIView]) {
        views.forEach(addSubview)
    }
    
    /// 获取视图所在的视图控制器
    var parentViewController: UIViewController? {
        weak var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

/**
 *  对 view.frame 操作的简便封装，注意 view 与 view 之间互相计算时，需要保证处于同一个坐标系内。
 */

// MARK: - Layout

public extension UIView {
    
    var top: CGFloat {
        get {
            return frame.minY
        }
        set {
            qmui_top = newValue
        }
    }
    
    /// 等价于 CGRectGetMinX(frame)
    var left: CGFloat {
        get {
            return frame.minX
        }
        set {
            qmui_left = newValue
        }
    }
    
    /// 等价于 CGRectGetMaxY(frame)
    var bottom: CGFloat {
        get {
            return frame.maxY
        }
        set {
            qmui_bottom = newValue
        }
    }
    
    /// 等价于 CGRectGetMaxX(frame)
    var right: CGFloat {
        get {
            return frame.maxX
        }
        set {
            qmui_right = newValue
        }
    }
    
    /// 等价于 CGRectGetHeight(frame)
    var height: CGFloat {
        get {
            return frame.height
        }
        set {
            qmui_height = newValue
        }
    }
    
    /// 等价于 CGRectGetWidth(frame)
    var width: CGFloat {
        get {
            return frame.width
        }
        set {
            qmui_width = newValue
        }
    }
    
    var size: CGSize {
        get {
            return frame.size
        }
        set {
            frame = CGRectSetSize(frame, newValue)
        }
    }
    
    var centerX: CGFloat {
        get {
            return self.frame.midX
        }
        set {
            left = newValue - width / 2
        }
    }
    
    var centerY: CGFloat {
        get {
            return self.frame.midY
        }
        set {
            top = newValue - height / 2
        }
    }
    
    var origin: CGPoint {
        get {
            return frame.origin
        }
        set {
            frame.origin = newValue
        }
    }
    
    /// 保持其他三个边缘的位置不变的情况下，将顶边缘拓展到某个指定的位置，注意高度会跟随变化。
    var extendToTop: CGFloat {
        get {
            return qmui_extendToTop
        }
        set {
            qmui_extendToTop = newValue
        }
    }
    
    /// 保持其他三个边缘的位置不变的情况下，将左边缘拓展到某个指定的位置，注意宽度会跟随变化。
    var extendToLeft: CGFloat {
        get {
            return qmui_extendToLeft
        }
        set {
            qmui_extendToLeft = newValue
        }
    }
    
    /// 保持其他三个边缘的位置不变的情况下，将底边缘拓展到某个指定的位置，注意高度会跟随变化。
    var extendToBottom: CGFloat {
        get {
            return qmui_extendToBottom
        }
        set {
            qmui_extendToBottom = newValue
        }
    }
    
    /// 保持其他三个边缘的位置不变的情况下，将右边缘拓展到某个指定的位置，注意宽度会跟随变化。
    var extendToRight: CGFloat {
        get {
            return qmui_extendToRight
        }
        set {
            qmui_extendToRight = newValue
        }
    }
    
    /// 获取当前 view 在 superview 内水平居中时的 left
    var leftWhenCenterInSuperview: CGFloat {
        return qmui_leftWhenCenterInSuperview
    }
    
    /// 获取当前 view 在 superview 内垂直居中时的 top
    var topWhenCenterInSuperview: CGFloat {
        return qmui_topWhenCenterInSuperview
    }
    
    func layout(_ block: (inout CGRect) -> Void) {
        var rect = self.frame
        block(&rect)
        self.frame = rect
    }
}

public extension UIView {
    
    /// 方便地将某个 UIView 截图并转成一个 UIImage，注意如果这个 UIView 本身做了 transform，也不会在截图上反映出来，截图始终都是原始 UIView 的截图。
    var snapshotLayerImage: UIImage? {
        guard self.size.height != 0 && self.size.width != 0 else {
            return nil
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0)
        guard let currentContext = UIGraphicsGetCurrentContext() else {
            return nil
        }
        layer.render(in: currentContext)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

public extension UIView {
    
    enum ShakeDirection {
        case horizontal
        case vertical
    }
    
    enum ShakeAnimationType {
        case linear
        case easeIn
        case easeOut
        case easeInOut
    }
    
    func shake(direction: ShakeDirection = .horizontal, duration: TimeInterval = 1, animationType: ShakeAnimationType = .easeOut, completion:(() -> Void)? = nil) {
        CATransaction.begin()
        let animation: CAKeyframeAnimation
        switch direction {
        case .horizontal:
            animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        case .vertical:
            animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        }
        switch animationType {
        case .linear:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        case .easeIn:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        case .easeOut:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        case .easeInOut:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        }
        CATransaction.setCompletionBlock(completion)
        animation.duration = duration
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
        CATransaction.commit()
    }
    
    func fadeIn(duration: TimeInterval = 0.625, completion: ((Bool) -> Void)? = nil) {
        if isHidden {
            self.alpha = 0
            isHidden = false
        }
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.625, completion: ((Bool) -> Void)? = nil) {
        if isHidden {
            self.alpha = 1
            isHidden = false
        }
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0
        }) { (isFinish) in
            self.isHidden = true
            completion?(isFinish)
        }
    }
}

public extension UIWindow.Level {
    static var belowStatusBar: UIWindow.Level {
        return UIWindow.Level(rawValue: UIWindow.Level.statusBar.rawValue - 1)
    }
}
