//
//  FEClassModel.swift
//  Red
//
//  Created by MAC on 2019/8/6.
//  Copyright © 2019 MAC. All rights reserved.
//

import QMUIKit
//import IGListKit
//
//public extension ListCollectionContext {
//    func dequeueReusableCell<T: UICollectionViewCell>(cell name: T.Type = T.self, for sectionController: ListSectionController, at index: Int) -> T {
//        let bareCell =  self.dequeueReusableCell(of: name, for: sectionController, at: index)
//        guard let cell = bareCell as? T else {
//            fatalError(
//                "Failed to dequeue a cell with identifier \(String(describing: name)) matching type \(T.self). "
//                    + "Check that the reuseIdentifier is set properly in your XIB/Storyboard "
//                    + "and that you registered the cell beforehand"
//            )
//        }
//        return cell
//    }
//}

public extension UICollectionView {
    final func registerCell<T: UICollectionViewCell>(cell name: T.Type) {
        let identifier = String(describing: name)
//        if let themeName = ThemeManager.default.currentTheme?.name {
//            identifier = "\(identifier)\(themeName)"
//        }
        register(T.self, forCellWithReuseIdentifier: identifier)
    }

    final func registerHeaderView<T: UICollectionReusableView>(view name: T.Type) {
        let identifier = String(describing: name)
//        if let themeName = ThemeManager.default.currentTheme?.name {
//            identifier = "\(identifier)\(themeName)"
//        }
        register(T.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: identifier)
    }

    final func dequeueReusableCell<T: UICollectionViewCell>(cell name: T.Type = T.self, for indexPath: IndexPath) -> T {
        let identifier = String(describing: name)
//        if let themeName = ThemeManager.default.currentTheme?.name {
//            identifier = "\(identifier)\(themeName)"
//        }
        let bareCell = dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        guard let cell = bareCell as? T else {
            fatalError(
                "\(T.self)未注册\(String(describing: name))"
            )
        }
        return cell
    }
    
    final func dequeueReusableHeaderView<T: UICollectionReusableView>(view name: T.Type = T.self, for indexPath: IndexPath) -> T {
        let identifier = String(describing: name)
//        if let themeName = ThemeManager.default.currentTheme?.name {
//            identifier = "\(identifier)\(themeName)"
//        }
        let bareHeader = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: identifier, for: indexPath)
        guard let headerView = bareHeader as? T else {
            fatalError(
                "\(T.self)未注册\(String(describing: name))"
            )
        }
        return headerView
    }
}
