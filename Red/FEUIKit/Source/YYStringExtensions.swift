//
//  YYStringExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/17.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

public extension String {
    var nsRange: NSRange {
        return NSRange(location: 0, length: count)
    }
    
    /// NSRange 转 Range
    func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
            let from = String.Index(from16, within: self),
            let to = String.Index(to16, within: self)
            else { return nil }
        return from ..< to
    }
    /// 正则表达式
    var regex: NSRegularExpression {
        return (try? NSRegularExpression(pattern: self, options: .anchorsMatchLines)) ?? NSRegularExpression()
    }
    
    
    /// 正则匹配
    ///
    /// - Parameter string: 待匹配的字符串
    /// - Returns: 是否匹配
    func match(string: String) -> Bool {
        return (((try? NSRegularExpression(pattern: self, options: .anchorsMatchLines)) ?? NSRegularExpression()).firstMatch(in: string, options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds, range: string.nsRange) != nil)
    }
}
