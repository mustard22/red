//
//  UIImageExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
// MARK: - Resources
public extension UIImage {
    
    static func image(with name: String) -> UIImage? {
        return UIImage(named: name, in: Bundle.we, compatibleWith: nil)
    }
}

// MARK: - ImageLabel
public extension UIImage {
    
    convenience init?(gradientColors:[UIColor], size:CGSize = CGSize(width: 10, height: 10)) {
        UIGraphicsBeginImageContextWithOptions(size, true, 0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colors = gradientColors.map {(color: UIColor) -> AnyObject? in return color.cgColor as AnyObject? } as NSArray
        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: colors, locations: nil) else { return nil }
        // 第二个参数是起始位置，第三个参数是终止位置
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.15 * size.width, y: 0.62 * size.height), end: CGPoint(x: 0.88 * size.width, y: 0.13 * size.height), options: CGGradientDrawingOptions(rawValue: 3))
        guard let cgImage = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
        self.init(cgImage: cgImage)
        UIGraphicsEndImageContext()
    }
    
    func withShadow(color: UIColor,offset: CGSize,blur: CGFloat,cornerRadius: CGFloat) -> UIImage? {
        let width = self.size.width + abs(offset.width)
        let height = self.size.height + abs(offset.height)
        return UIImage.qmui_image(with: CGSize(width: width, height: height), opaque: self.qmui_opaque(), scale: self.scale) { (contextRef) in
            let x = offset.width > 0 ? 0 : -offset.width
            let y = offset.height > 0 ? 0 : -offset.height
            let rect = CGRect(origin: CGPoint(x: x, y: y), size: self.size)
            let path: UIBezierPath
            if cornerRadius > 0 {
                path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
            }else {
                path = UIBezierPath(rect: rect)
            }
            contextRef.setShadow(offset: offset, blur: blur, color: color.cgColor)
            contextRef.setFillColor(UIColor.clear.cgColor)
            path.fill()
            self.draw(in: rect)
        }
    }
    
    func drawImage(size: CGSize) -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        self.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

public extension UIImage {
    static var navigationBarShadowImage: UIImage? {
        return UIImage.qmui_image(with: Color.separator, size: CGSize(width: QMUIHelper.pixelOne(), height: QMUIHelper.pixelOne()), cornerRadius: 0)
    }
    
    /// 通过颜色生成新图片
    func sls_tintColor_image(_ tintColor: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
        tintColor.setFill()
        let bounds = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        UIRectFill(bounds);
        
        self.draw(in: bounds, blendMode: CGBlendMode.destinationIn, alpha: 1)
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}
