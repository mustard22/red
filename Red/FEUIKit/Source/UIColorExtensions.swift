//
//  UIColorExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
public extension UIColor {
    
    /// 返回随机颜色
    static var random: UIColor {
        return qmui_random()
    }
    
    convenience init(r: Int, g: Int, b: Int) {
        self.init(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff)
    }
}
