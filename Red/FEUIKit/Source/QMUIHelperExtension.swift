//
//  QMUIHelperExtension.swift
//  Red
//
//  Created by MAC on 2019/8/17.
//  Copyright © 2019 MAC. All rights reserved.
//


import QMUIKit

public extension QMUIHelper {
    //// 状态栏高度(来电等情况下，状态栏高度会发生变化，所以应该实时计算)
    static var statusBarHeight: CGFloat {
        return UIApplication.shared.isStatusBarHidden ? 0 : UIApplication.shared.statusBarFrame.size.height
    }
    //// 状态栏高度(如果状态栏不可见，也会返回一个普通状态下可见的高度)
    static var statusBarHeightConstant: CGFloat {
        return UIApplication.shared.isStatusBarHidden ? (QMUIHelper.isNotchedScreen() ? (isLandscape ? 0 : 44) : 20) : UIApplication.shared.statusBarFrame.size.height
    }
    //// tabBar相关frame
    static var tabBarHeight: CGFloat {
        return (QMUIHelper.isNotchedScreen() ? (isLandscape ? 32 : 49) : 49) + QMUIHelper.safeAreaInsetsForDeviceWithNotch().bottom
    }
    //// tabBar相关frame
    static var toolBarHeight: CGFloat {
        return (isLandscape ? (QMUIHelper.isRegularScreen() ? 44 : 32) :  44) + (QMUIHelper.isNotchedScreen() ? 39 : 0)
    }
    //// navigationBar 的静态高度
    static var navigationBarHeight: CGFloat {
        return isLandscape ? (QMUIHelper.isRegularScreen() ? 44 : 32) : 44
    }
    //// 应用是否横屏
    static var isLandscape: Bool {
        return UIApplication.shared.statusBarOrientation.isLandscape
    }
    //// 无论支不支持横屏，只要设备横屏了，就会返回true
    static var isDeviceLandscape: Bool {
        return UIDevice.current.orientation.isLandscape
    }
    //// 代表(导航栏+状态栏)，这里用于获取其高度
    static var navigationContentTop: CGFloat {
        return navigationBarHeight + statusBarHeight
    }
    //// 同上，这里用于获取它的静态常量值
    static var navigationContentTopConstant: CGFloat {
        return navigationBarHeight + statusBarHeightConstant
    }
    
    static func imageSizeFromURL(string: String) -> CGSize? {
        if let sizeNSRange = "(?<=_)[0-9]*X[0-9]*".regex.firstMatch(in: string, options: .withoutAnchoringBounds, range: string.nsRange)?.range,
            let sizeRange = string.range(from: sizeNSRange) {
            let sizeStr = String(string[sizeRange])
            if let widthStr = sizeStr.split(separator: "X").first,
                let heightStr = sizeStr.split(separator: "X").last {
                let   width = CGFloat(Double(widthStr) ?? 0)
                let   height = CGFloat(Double(heightStr) ?? 0)
                return CGSize(width: width, height: height)
            }
        }
        return nil
    }
}
