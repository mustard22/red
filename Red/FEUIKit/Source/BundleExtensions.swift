//
//  BundleExtensions.swift
//  Red
//
//  Created by MAC on 2019/8/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
var WEUIBundle: Bundle? = nil

public extension Bundle {
    static var we: Bundle {
        let frameworkBundle = Bundle(for: FEBaseViewController.self)
        if WEUIBundle.isNone {
            guard let bundlePath = frameworkBundle.path(forResource: "WEUIBundle", ofType: "bundle") else { return frameworkBundle }
            WEUIBundle = Bundle(path: bundlePath)
        }
        return WEUIBundle.or(frameworkBundle)
    }
    
    func loadView<T>(_ viewType: T.Type) -> T {
        let name = String(describing: T.self)
        let nib = UINib(nibName: name, bundle: self)
        if let view = nib.instantiate(withOwner: nil, options: nil).first as? T {
            return view
        }else {
            fatalError("\(name).xib 无法找到")
        }
    }
}
