//
//  UIColor[Theme].swift
//  Red
//
//  Created by MAC on 2019/8/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
public class Color: UIColor {
    
    override public class var darkGray: UIColor {
        return ThemeManager.default.configuration.grayDarkenColor
    }
    
    /// #D3D3D3
    override public class var lightGray: UIColor {
        return ThemeManager.default.configuration.grayLightenColor
    }
    
    override public class var white: UIColor {
        return ThemeManager.default.configuration.whiteColor
    }
    
    override public class var gray: UIColor {
        return ThemeManager.default.configuration.grayColor
    }
    
    override public class var red: UIColor {
        return ThemeManager.default.configuration.redColor
    }
    
    override public class var green: UIColor {
        return ThemeManager.default.configuration.greenColor
    }
    
    override public class var blue: UIColor {
        return ThemeManager.default.configuration.blueColor
    }
    
    override public class var yellow: UIColor {
        return ThemeManager.default.configuration.yellowColor
    }
    
    override public class var clear: UIColor {
        return ThemeManager.default.configuration.clearColor
    }
    
    /// 金额用色 #FF7043
    public class var money: UIColor {
        return #colorLiteral(red: 1, green: 0.4392156863, blue: 0.262745098, alpha: 1)
    }
    /// 金色 #E0A153
    public class var golden: UIColor {
        return #colorLiteral(red: 0.8784313725, green: 0.631372549, blue: 0.3254901961, alpha: 1)
    }
    /// 主题色 2066FF
    public static var tint: UIColor {
        return ThemeManager.default.currentTheme?.tintColor ?? #colorLiteral(red: 0.1254901961, green: 0.4, blue: 1, alpha: 1)
    }
    
    /// 次要主题色 3191FF
    public static var tint2: UIColor {
        return ThemeManager.default.currentTheme?.secondaryColor ?? #colorLiteral(red: 0.1921568627, green: 0.568627451, blue: 1, alpha: 1)
    }
    
    /// 文字链接颜色
    override public static var link: UIColor {
        return ThemeManager.default.configuration.linkColor
    }
    
    /// #cbd0dc 全局 disabled 的颜色，一般用于 UIControl 等控件
    public static var disabled: UIColor {
        return ThemeManager.default.configuration.disabledColor
    }
    
    /// 界面背景色，默认用于 QMUICommonViewController.view 的背景色
    public static var background: UIColor? {
        return ThemeManager.default.configuration.backgroundColor
    }
    /// 深色的背景遮罩，默认用于 QMAlertController、QMUIDialogViewController 等弹出控件的遮罩
    public static var mask: UIColor {
        return ThemeManager.default.configuration.maskDarkColor
    }
    /// 浅色的背景遮罩，QMUIKit 里默认没用到，只是占个位
    public static var maskWhite: UIColor {
        return ThemeManager.default.configuration.maskLightColor
    }
    
    /// 全局默认的分割线颜色，默认用于列表分隔线颜色、UIView (QMUI_Border) 分隔线颜色
    override public static var separator: UIColor {
        return ThemeManager.default.configuration.separatorColor
    }
    
    /// UIColorSeparatorDashed : 全局默认的虚线分隔线的颜色，默认 QMUIKit 暂时没用到
    public static var separatorDashed: UIColor {
        return ThemeManager.default.configuration.separatorDashedColor
    }
    
    /// UIColorPlaceholder，全局的输入框的 placeholder 颜色，默认用于 QMUITextField、QMUITextView，不影响系统 UIKit 的输入框
    public static var placeholder: UIColor {
        return ThemeManager.default.configuration.placeholderColor
    }
    // NavBarTintColor : QMUINavigationController.navigationBar 的 tintColor，也即导航栏上面的按钮颜色，由于 tintColor 不支持 appearance，所以这里只支持 QMUINavigationController
    public static var navBarTint: UIColor? {
        return ThemeManager.default.configuration.navBarTintColor
    }
    
    public static var tableViewCellBackgroundColor: UIColor? {
        return ThemeManager.default.configuration.tableViewCellBackgroundColor
    }
}


extension UIColor {
    static func RGBColor(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
    
    static func RGBAColor(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ a: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
}
