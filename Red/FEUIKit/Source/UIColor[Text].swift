//
//  UIColor[Text].swift
//  Red
//
//  Created by MAC on 2019/8/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
public extension UIColor {
    
    /// 主要的字体颜色 111111
    static var text: UIColor {
        return ThemeManager.default.currentTheme?.textColor ?? UIColor.black
    }
    /// 不那么重要的字体颜色 555555
    static var text2: UIColor {
        return ThemeManager.default.currentTheme?.text2Color ?? UIColor.black
    }
    /// 更不重要的字体颜色 999999
    static var text3: UIColor {
        return ThemeManager.default.currentTheme?.text3Color ?? UIColor.black
    }
    /// 列表标题颜色
    static var listTitleColor: UIColor {
        return ThemeManager.default.currentTheme?.listTitleColor ?? UIColor.black
    }
}
