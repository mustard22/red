//
//  FETakePhotoViewController.swift
//  Red
//
//  Created by MAC on 2020/4/11.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import CoreGraphics

class FETakePhotoViewController: UIViewController {
    
    @IBOutlet weak var contentLayerView: UIView!
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    // 切换摄像头
    @IBOutlet weak var exchangeButton: UIButton!
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var contentImageView: UIView!
    @IBOutlet weak var showImageView: UIImageView!
    @IBOutlet weak var useImageButton: UIButton!
    @IBOutlet weak var retakeButton: UIButton!
    
    var didFinishHandler: ((_ image: UIImage?)->Void)?
    
    
    private var device: AVCaptureDevice!
    private var input: AVCaptureDeviceInput!
    private var imageOutput: AVCapturePhotoOutput!
    private var session = AVCaptureSession()
    private var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dosomeInitial()
        
        addTapGesture()
        
        cameraDistrict()
        
        focusAtPoint(layerView.center)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.previewLayer.frame = layerView.bounds
    }

    deinit {

    }
    
    private func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dotap(_:)))
        self.layerView.addGestureRecognizer(tap)
    }
    
    @objc
    private func dotap(_ res: UITapGestureRecognizer) {
        let point = res.location(in: self.layerView)
        focusAtPoint(point)
    }
    
    
}

extension FETakePhotoViewController {
    @IBAction func buttonAction(_ sender: UIButton) {
        if (sender == takePhotoButton) {
            // 拍照
            photoBtnDidClick()
        }
        else if (sender == flashButton) {
            
        }
        else if (sender == cancelButton) {
            // 取消
            close()
        }
        else if (sender == retakeButton) {
            // 重新拍照
            UIView.animate(withDuration: 0.3, animations: {
                self.contentImageView.isHidden = true
            }) { (f) in
                self.session.startRunning()
            }
        }
        else if (sender == useImageButton) {
            // 返回图片
            didFinishHandler?(showImageView.image)
            close()
        }
        else if (sender == exchangeButton) {
            // 切换摄像头
            changeCamera()
        }
        else {
            
        }
    }
}

extension FETakePhotoViewController: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        guard let buffer = photoSampleBuffer, let prebuffer = previewPhotoSampleBuffer else {
            return
        }
        
        guard let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer, previewPhotoSampleBuffer: prebuffer), let image = UIImage(data: data) else {
            return
        }
        
        showImageView.image = fixOrientation(image)
        contentImageView.isHidden = false
        session.stopRunning()
    }
}

//MARK: - private methods
extension FETakePhotoViewController {
    private func cameraDistrict() {
        device = cameraWithPosition(.back)
        input = try? AVCaptureDeviceInput(device: device)
        imageOutput = AVCapturePhotoOutput()
        
        session.sessionPreset = .medium
        if session.canAddInput(input) {
            session.addInput(input)
        }
        
        if session.canAddOutput(imageOutput) {
            session.addOutput(imageOutput)
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.frame = layerView.bounds
        previewLayer.videoGravity = .resizeAspectFill
        layerView.layer.addSublayer(previewLayer)
        
        session.startRunning()
        
        try? device.lockForConfiguration()
        
        if device.isWhiteBalanceModeSupported(.autoWhiteBalance) {
            device.whiteBalanceMode = .autoWhiteBalance
        }
        
        device.unlockForConfiguration()
    }
    
    private func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func dosomeInitial() {
        takePhotoButton.setBackgroundImage(UIImage(named: "TakePhoto.bundle/button_shutter.png"), for: .normal)
        exchangeButton.setBackgroundImage(UIImage(named: "TakePhoto.bundle/camera_switch.png"), for: .normal)
    }
    
    //MARK: 获取相机图片
    private func photoBtnDidClick() {
        guard let con = imageOutput.connection(with: .video) else {
            dprint("拍照失败!")
            return
        }
        
        if con.isVideoStabilizationSupported {
            con.videoOrientation = self.getCaptureVideoOrientation()
        }
        
        let settings = AVCapturePhotoSettings()
        settings.flashMode = .auto

        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 200,
            kCVPixelBufferHeightKey as String: 200
        ]
        settings.previewPhotoFormat = previewFormat
        
        imageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    //MARK: 旋转方向
    private func getCaptureVideoOrientation() -> AVCaptureVideoOrientation {
        let ori = UIDevice.current.orientation
        var resOri: AVCaptureVideoOrientation = .portrait
        switch ori {
        case .landscapeLeft:
            resOri = .landscapeRight
        case .landscapeRight:
            resOri = .landscapeLeft
        default:
            resOri = .portrait
        }
        return resOri
    }
    
    private func fixOrientation(_ image: UIImage) -> UIImage {
        let orientation = image.imageOrientation
        if orientation == .up {
            return image
        }
        
        var transform = CGAffineTransform.identity
        switch orientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: image.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2))
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: image.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi/2))
            
        default:break
        }
        
        switch orientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: image.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default: break
        }
        
        
        guard let cgimage = image.cgImage, let space = cgimage.colorSpace else {
            return image
        }
        
        let bitsPerComponent: Int = cgimage.bitsPerComponent
        let bitmap = cgimage.bitmapInfo
        let ctx: CGContext? = CGContext.init(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: bitsPerComponent, bytesPerRow: 0, space: space, bitmapInfo: bitmap.rawValue)
        
        ctx?.concatenate(transform)
        
        switch orientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx?.draw(cgimage, in: CGRect(x: 0, y: 0, width: image.size.height, height: image.size.width))
        default:
            ctx?.draw(cgimage, in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        }
        
        let cgimg: CGImage? = ctx?.makeImage()
        if let cimg = cgimg {
            let img = UIImage(cgImage: cimg)
            return img
        }
        return image
    }
    
    //MARK: 保存图片
    private func saveImageToPhotoAlbum(_ img: UIImage) {
        
        PHPhotoLibrary.requestAuthorization { (status) in
            if (status == .notDetermined || status == .authorized) {
            UIImageWriteToSavedPhotosAlbum(img, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
            }
            else {
                DispatchQueue.main.async {
                    let ac = UIAlertController(title: "相册授权未开启", message: "请在系统设置中开启相册授权", preferredStyle: .alert)
                    let action = UIAlertAction(title: "知道了", style: .default, handler: nil)
                    ac.addAction(action)
                    self.present(ac, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if let e = error as NSError? {
            dprint("系统问题，保存失败 {\(e)}")
        } else {
            dprint("保存成功")
        }
    }
    
    //MARK: 前后置摄像头的切换
    private func changeCamera() {
        
        let count = AVCaptureDevice.devices().filter({$0.hasMediaType(.video)}).count
        if count < 2 {
            return
        }
        
        let animation = CATransition()
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.type = CATransitionType(rawValue: "oglFlip")
        
        
        var newCamera: AVCaptureDevice!
        var newInput: AVCaptureDeviceInput!
        
        let position = self.input.device.position
        if position == .front {
            if let c = self.cameraWithPosition(.back) {
                newCamera = c
                animation.subtype = .fromLeft
            }
        } else {
            if let c = self.cameraWithPosition(.front) {
                newCamera = c
                animation.subtype = .fromRight
            }
        }
        
        if let _ = newCamera {
            newInput = try? AVCaptureDeviceInput.init(device: newCamera)
        }
        
        previewLayer.add(animation, forKey: nil)
        
        if newInput == nil {
            return
        }
        
        session.beginConfiguration()
        session.removeInput(self.input)
        if session.canAddInput(newInput) {
            session.addInput(newInput)
            self.input = newInput
        } else {
            session.addInput(self.input)
        }
        session.commitConfiguration()
    }
    
    private func cameraWithPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices = AVCaptureDevice.devices(for: .video)
        if devices.isEmpty {
            return nil
        }
        
        return devices.filter({$0.position == position}).first
    }
    
    //对焦设置
    //AVCaptureFlashMode  闪光灯
    //AVCaptureFocusMode  对焦
    //AVCaptureExposureMode  曝光
    //AVCaptureWhiteBalanceMode  白平衡
    //闪光灯和白平衡可以在生成相机时候设置
    //曝光要根据对焦点的光线状况而决定,所以和对焦一块写
    //point为点击的位置
    private func focusAtPoint(_ point: CGPoint) {
        let size = self.layerView.bounds.size
        let focusPoint = CGPoint(x: point.y/size.height, y: 1-point.x/size.width)
        
        if let _ = try? device.lockForConfiguration() {
            return
        }
        
        //对焦模式和对焦点
        if device.isFocusModeSupported(.autoFocus) {
            device.focusPointOfInterest = focusPoint
            device.focusMode = .autoFocus
        }
        
        //曝光模式和曝光点
        if device.isExposureModeSupported(.autoExpose) {
            device.exposurePointOfInterest = focusPoint
            device.exposureMode = .autoExpose
        }
        
        device.unlockForConfiguration()
    }
}
