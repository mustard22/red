//
//  SegementContentCell.swift
//  Red
//
//  Created by MAC on 2019/8/17.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit
public class SegementContentCell: UICollectionViewCell {
    private var mainView: UIView?
    var scrollView: UIScrollView?
    
    @objc public func update(content: UIView, scrollView: UIScrollView?) {
        mainView?.removeFromSuperview()
        mainView = content
        self.scrollView = scrollView
        contentView.addSubview(content)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        mainView?.frame = contentView.bounds
        scrollView?.frame = contentView.bounds
    }
}
