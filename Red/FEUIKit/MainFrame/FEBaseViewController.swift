//
//  FEBaseViewController.swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import QMUIKit

open class FEBaseViewController: QMUICommonViewController,ChangingThemeDelegate{
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    
    override open func didInitialize() {
        super.didInitialize()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleThemeChanged(notification:)),
                                               name: .themeChanged,
                                               object: nil)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
        #if DEBUG
        dprint("\(type(of: self)) 正常释放")
        #endif
    }
    override open func preferredNavigationBarHidden() -> Bool {
        return false
    }
    override open func shouldCustomizeNavigationBarTransitionIfHideable() -> Bool {
        return true
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    open override func showEmptyView() {
        if self.emptyView.isNone {
//            self.emptyView = WEEmptyView()
        }
        if let emptyView = self.emptyView {
            self.view.addSubview(emptyView)
        }
    }
    open override func hideEmptyView() {
        if isEmptyViewShowing {
            super.hideEmptyView()
        }
    }
    
    @objc
    func handleThemeChanged(notification: Notification) {
        let oldTheme = notification.userInfo?["oldTheme"] as? ThemeProtocol
        let newTheme = notification.userInfo?["newTheme"] as? ThemeProtocol
        self.themChanged(form: oldTheme, to: newTheme)
    }
    
    public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        view.backgroundColor = ThemeManager.default.configuration.backgroundColor
        titleView?.tintColor = ThemeManager.default.configuration.navBarTitleColor
    }
//    public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
//        view.backgroundColor = ThemeProtocol.
//    }
//

}
