//
//  FEBaseWebViewController.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import WebKit

fileprivate let kEstimatedProgress = "estimatedProgress"
/// web界面
class FEBaseWebViewController: FEBaseViewController {
   
    private var content: WKWebView?
    
    private lazy var progress: UIProgressView = {
        let top: CGFloat = UIApplication.shared.statusBarFrame.height == 44 ? 88 : 64
        let v = UIProgressView(progressViewStyle: UIProgressView.Style.bar)
        v.frame = CGRect(x: 0, y: top, width: view.frame.size.width, height: 2)
        v.progressTintColor = UIColor.RGBColor(68, 187, 251)
        self.view.addSubview(v)
        return v
    }()
    
    
    
    //MARK: public var
    /// web页面链接字符串
    public var url: String = "" {
        didSet {
            if self.url.hasPrefix("http") {
                self.startLoad()
            }
        }
    }
    
    //MARK: methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
    }
    
    deinit {
        if content != nil {
            content!.stopLoading()
            content!.removeObserver(self, forKeyPath: kEstimatedProgress)
            content!.navigationDelegate = nil
        }
        
        dprint("\(self) 已释放")
    }
    
    
}

extension FEBaseWebViewController {
    // 开始加载web页面
    private func startLoad() {
        content = WKWebView(frame: self.view.bounds, configuration: WKWebViewConfiguration())
        content!.navigationDelegate = (self as WKNavigationDelegate)
        view.addSubview(content!)
        content!.addObserver(self, forKeyPath: kEstimatedProgress, options: .new, context: nil)
        
        let res = URLRequest(url: URL(string: url)!)
        content!.load(res)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == kEstimatedProgress {
            let currentProgress = Float(content!.estimatedProgress)
            if currentProgress < progress.progress {
                return
            }
            
            progress.setProgress(currentProgress, animated: true)
            
            if progress.progress >= 1.0 {
                UIView.animate(withDuration: 0.3, animations: {[weak self] in
                    self?.progress.isHidden = true
                }) {[weak self] (fin) in
                    self?.progress.setProgress(0, animated: false)
                }
            }
        }
    }
}


//MARK: WKNavigationDelegate
extension FEBaseWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
    }
}
