//
//  SegementContentCollectionViewDelegate.swift
//  Red
//
//  Created by MAC on 2019/8/17.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

@objc protocol SegementContentCollectionViewDelegate: NSObjectProtocol {
    /**
     *  联动 SegementSlideView 的方法
     *
     *  @param pageContentCollectionView      SegementContentCollectionView
     *  @param progress             SegementContentCollectionView 内部视图滚动时的偏移量
     *  @param originalIndex        原始视图所在下标
     *  @param targetIndex          目标视图所在下标
     */
    @objc optional func pageContentCollectionView(pageContentCollectionView: SegementContentCollectionView, progress: CGFloat, originalIndex: Int, targetIndex: Int)
    
    /**
     *  获取 SegementContentCollectionView 当前子控制器的下标值
     *
     *  @param pageContentCollectionView     SegementContentCollectionView
     *  @param index                         SegementContentCollectionView 当前子控制器的下标值
     */
    @objc optional func pageContentCollectionView(pageContentCollectionView: SegementContentCollectionView, index: Int)
    
    /// SegementContentCollectionView 内容开始拖拽方法
    @objc optional func pageContentCollectionViewWillBeginDragging()
    /// SegementContentCollectionView 内容结束拖拽方法
    @objc optional func pageContentCollectionViewDidEndDecelerating()
}

private let cellID = "cellID"


public class SegementContentCollectionView: UIView {
    /**
     *  拓展 init 方法
     *
     *  @param frame        frame
     *  @param parentVC     当前控制器
     *  @param childVCs     子控制器个数
     */
    init(frame: CGRect, parentVC: UIViewController, childVCs: [SegementSlideContentScrollViewDelegate & UIViewController]) {
        super.init(frame: frame)
        self.parentVC = parentVC
        self.childVCs = childVCs
        self.setupSubViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 给外界提供的属性
    /// SegementContentCollectionViewDelegate
    weak var delegateCollectionView: SegementContentCollectionViewDelegate?
    /// SegementContentCollectionView 是否可以滚动，默认为 true
    public var isScrollEnabled: Bool {
        get {
            return collectionView.isScrollEnabled
        }
        
        set {
            collectionView.isScrollEnabled = newValue
        }
    }
    /// 点击标题触发动画切换滚动内容，默认为 false
    var isAnimated: Bool = false
    
    public var contentInset: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            if let cells = self.collectionView.visibleCells as? [SegementContentCell] {
                cells.forEach({$0.scrollView?.contentInset = contentInset })
            }
        }
    }
    
    // MARK: - 私有属性
    private weak var parentVC: UIViewController?
    public var childVCs: [SegementSlideContentScrollViewDelegate & UIViewController] = []
    
    public var currentVC: (SegementSlideContentScrollViewDelegate & UIViewController)? {
        guard previousCVCIndex >= 0 && previousCVCIndex < childVCs.count else {
            return nil
        }
        return childVCs[previousCVCIndex]
    }
    
    private var startOffsetX: CGFloat = 0.0
    private var previousCVCIndex: Int = -1
    
    private var scroll: Bool?
    
    lazy var flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = self.bounds.size
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    public lazy var collectionView: UICollectionView = {
        
        let rect = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        let collectionView = UICollectionView(frame: rect, collectionViewLayout: flowLayout)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.bounces = false
        collectionView.backgroundColor = UIColor.white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        }
        collectionView.registerCell(cell: SegementContentCell.self)
        return collectionView
    }()
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = self.bounds
        flowLayout.itemSize = self.bounds.size
    }
}

// MARK: - 给外界提供的方法
public extension SegementContentCollectionView {
    /// 根据 SegementSlideView 标题选中时的下标并显示相应的子控制器
    func setPageContentCollectionView(index: Int) {
        let offsetX = CGFloat(index) * collectionView.frame.size.width
        startOffsetX = offsetX
        if collectionView.contentOffset.x != offsetX {
            collectionView.setContentOffset(CGPoint(x: offsetX, y: 0), animated: isAnimated)
        }
        previousCVCIndex = index
        if (delegateCollectionView != nil) && (delegateCollectionView?.responds(to: #selector(delegateCollectionView?.pageContentCollectionView(pageContentCollectionView:index:))))! {
            delegateCollectionView?.pageContentCollectionView!(pageContentCollectionView: self, index: index)
        }
    }
}

// Mark: - 添加子控件
extension SegementContentCollectionView {
    private func setupSubViews() {
        let tempView = UIView()
        self.addSubview(tempView)
        self.addSubview(self.collectionView)
    }
}

// MARK: - UICollectionView - 数据源方法
extension SegementContentCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return childVCs.count
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(cell: SegementContentCell.self, for: indexPath)
        let childVC = childVCs[indexPath.item]
        if childVC.parent.isSome {
            childVC.removeFromParent()
        }
        parentVC?.addChild(childVC)
        cell.update(content: childVC.view, scrollView: childVC.scrollView)
        childVC.view.setNeedsLayout()
        childVC.view.layoutIfNeeded()
        childVC.didMove(toParent: parentVC)
        childVC.scrollView.contentInset = contentInset
        childVC.scrollView.scrollIndicatorInsets = contentInset
        ///下拉
//        childVC.scrollView.mj_footer?.ignoredScrollViewContentInsetBottom = contentInset.bottom
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let childVC = childVCs[indexPath.item]
        childVC.view.frame = cell.contentView.bounds
        childVC.scrollView.frame = childVC.view.bounds
        childVC.scrollView.contentInset = contentInset
        childVC.scrollView.scrollIndicatorInsets = contentInset
        ///上啦
//        childVC.scrollView.mj_footer?.ignoredScrollViewContentInsetBottom = contentInset.bottom
    }
}

// MARK: - UIScrollView - 代理方法
public extension SegementContentCollectionView {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        startOffsetX = scrollView.contentOffset.x
        scroll = true
        if (delegateCollectionView != nil) && (delegateCollectionView?.responds(to: #selector(delegateCollectionView?.pageContentCollectionViewWillBeginDragging)))! {
            delegateCollectionView?.pageContentCollectionViewWillBeginDragging!()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scroll = false
        let offsetX = scrollView.contentOffset.x
        previousCVCIndex = Int(offsetX / scrollView.frame.size.width)
        if (delegateCollectionView != nil) && (delegateCollectionView?.responds(to: #selector(delegateCollectionView?.pageContentCollectionView(pageContentCollectionView:index:))))! {
            delegateCollectionView?.pageContentCollectionView!(pageContentCollectionView: self, index: previousCVCIndex)
        }
        if (delegateCollectionView != nil) && (delegateCollectionView?.responds(to: #selector(delegateCollectionView?.pageContentCollectionViewDidEndDecelerating)))! {
            delegateCollectionView?.pageContentCollectionViewDidEndDecelerating!()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isAnimated == true && scroll == false {
            return
        }
        // 1、定义获取需要的数据
        var progress: CGFloat = 0.0
        var originalIndex: Int = 0
        var targetIndex: Int = 0
        
        // 2、判断是左滑还是右滑
        let currentOffsetX: CGFloat = scrollView.contentOffset.x
        let scrollViewW: CGFloat = scrollView.bounds.size.width
        if currentOffsetX > startOffsetX { // 左滑
            // 1、计算 progress
            progress = currentOffsetX / scrollViewW - floor(currentOffsetX / scrollViewW);
            // 2、计算 originalIndex
            originalIndex = Int(currentOffsetX / scrollViewW);
            // 3、计算 targetIndex
            targetIndex = originalIndex + 1;
            if targetIndex >= childVCs.count {
                progress = 1;
                targetIndex = originalIndex;
            }
            // 4、如果完全划过去
            if currentOffsetX - startOffsetX == scrollViewW {
                progress = 1;
                targetIndex = originalIndex;
            }
        } else { // 右滑
            // 1、计算 progress
            progress = 1 - (currentOffsetX / scrollViewW - floor(currentOffsetX / scrollViewW));
            // 2、计算 targetIndex
            targetIndex = Int(currentOffsetX / scrollViewW);
            // 3、计算 originalIndex
            originalIndex = targetIndex + 1;
            if originalIndex >= childVCs.count {
                originalIndex = childVCs.count - 1;
            }
        }
        
        if (delegateCollectionView != nil) && (delegateCollectionView?.responds(to: #selector(delegateCollectionView?.pageContentCollectionView(pageContentCollectionView:progress:originalIndex:targetIndex:))))! {
            delegateCollectionView?.pageContentCollectionView!(pageContentCollectionView: self, progress: progress, originalIndex: originalIndex, targetIndex: targetIndex)
        }
    }
}
