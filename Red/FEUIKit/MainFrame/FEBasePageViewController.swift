//
//  FEBasePageViewController.swift
//  Red
//
//  Created by MAC on 2019/12/24.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit


@objc protocol FEBasePageViewControllerDataSource: NSObjectProtocol {
    /// 标题
    @objc optional
    func itemText(index: Int) -> String

    /// 一共有多少页
    func numbersOfPage() -> Int

    /// 每页对应的VC
    func previewController(formPage index: Int) -> UIViewController
    
    @objc optional
    func currentPageIndex(index: Int)
}


class FEBasePageViewController: FEBaseViewController {

    weak var dataSource: FEBasePageViewControllerDataSource?
    
    /// 标题字体
    var headerFont = UIFont.systemFont(ofSize: 16)
    /// 标题颜色
    var headerColor = UIColor.darkGray
    /// 标题选中颜色
    var headerHigColor = Color.red
    
    /// 当前页码
    var currentPage: Int {
        get {
            return self.index
        }
        
        set (value) {
            if value == self.index {
                return
            }
            
            self.exchangeControllersAfterIndexChanged(value)
        }
    }
    
    /// 获取某个菜单按钮
    func menuButton(with index: Int) -> UIButton? {
        guard index >= 0, index < buttons.count else {
            return nil
        }
        return buttons[index]
    }

    // 标题高度(<=0时，则没有标题)
    private var headerHeight: CGFloat = 55.0
    private var startingIndex: Int = 0
    private var style: UIPageViewController.TransitionStyle = .scroll
    private var index: Int = 0
    private var number: Int = 0
    private var buttons: [UIButton] = [UIButton]()
    
    /// 是否显示菜单栏
    private var isShowMenuBar: Bool {
        return headerHeight > 0 ? true : false
    }

    private lazy var pageController: UIPageViewController = {
        let op = [UIPageViewController.OptionsKey.spineLocation: UIPageViewController.SpineLocation.min]
        let vc = UIPageViewController(transitionStyle: style, navigationOrientation: .horizontal, options: op)
        vc.dataSource = self
        vc.delegate = self
        return vc
    }()

    private lazy var headerView: UIScrollView = {
        let view = UIScrollView()
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: headerHeight)
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    
    private lazy var redline: UIView = {
        let view = UIView()
        view.bounds = CGRect(x: 0, y: headerHeight-2, width: 60, height: 2)
        view.backgroundColor = headerHigColor
        return view
    }()

    /// pageVC初始化方法
    init(start: Int = 0, headerHeight: CGFloat = 55.0, style: UIPageViewController.TransitionStyle = .scroll) {
        super.init(nibName: nil, bundle: nil)
        self.style = style
        self.index = start
        self.headerHeight = headerHeight
        self.startingIndex = start
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.number = self.dataSource?.numbersOfPage() ?? 0
        self.setupUI()
    }

    private func setupUI() {
        self.addChild(pageController)
        self.view.addSubview(pageController.view)

        // 标题
        if isShowMenuBar {
            self.view.addSubview(headerView)
            self.view.addSubview(redline)
            
            self.pageController.view.frame.origin.y += headerHeight
            self.pageController.view.frame.size.height -= headerHeight
            self.setupHeaderItems(number: number)
            
            let startSelectBtn = (startingIndex<0 || startingIndex>=number) ? nil : buttons[startingIndex]
            self.selected(startSelectBtn ?? UIButton(type: .custom))
        }

        // 初始界面
        guard let start = self.dataSource?.previewController(formPage: index) else {
            return
        }
        self.pageController.setViewControllers([start], direction: .forward, animated: false, completion: nil)
    }
    
    /// 外部修改index，内部切换页面
    private func exchangeControllersAfterIndexChanged(_ cindex: Int) {
        guard let current = self.dataSource?.previewController(formPage: cindex) else {
            return
        }
        self.pageController.setViewControllers([current], direction: cindex > self.index ? .forward : .reverse, animated: false, completion: nil)
        
        // 设置菜单栏
        if isShowMenuBar {
            self.selected(self.buttons[cindex])
        }
        
        self.index = cindex
    }

    private func setupHeaderItems(number: Int) {
        let w1 = UIScreen.main.bounds.width / CGFloat(number)
        for i in 0..<number {
            let text = (self.dataSource?.itemText?(index: i) ?? "") as NSString
            let w2 = text.boundingRect(with: CGSize(width: CGFloat(MAXFLOAT), height: headerFont.lineHeight),
                                       options: .usesLineFragmentOrigin,
                                       attributes: [NSAttributedString.Key.font: headerFont],
                                       context: nil).width + 10
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: buttons.last?.frame.maxX ?? 0, y: 0, width: max(w1, w2), height: headerHeight)
            button.setTitle(text as String, for: .normal)
            button.titleLabel?.font = headerFont
            button.setTitleColor(headerColor, for: .normal)
            button.setTitleColor(headerHigColor, for: .selected)
            button.setTitleColor(headerHigColor, for: UIControl.State(rawValue: UIControl.State.highlighted.rawValue | UIControl.State.selected.rawValue))
            button.addTarget(self, action: #selector(buttonClick(_:)), for: .touchUpInside)
            headerView.addSubview(button)
            headerView.contentSize = CGSize(width: button.frame.maxX, height: 0)
            buttons.append(button)
        }
    }

    @objc private func buttonClick(_ button: UIButton) {
        self.selected(button)
        let index = buttons.firstIndex(of: button) ?? 0
        let vc = self.dataSource!.previewController(formPage: index)
        pageController.setViewControllers([vc], direction: index > self.index ? .forward : .reverse, animated: true) { _ in
            self.index = index
        }
    }

    private func selected(_ button: UIButton) {
        for btn in buttons where btn.isSelected == true {
            btn.isSelected = false
        }
        button.isSelected = true
        
        let btnRect = button.convert(button.bounds, to: self.view)
        var rect = redline.frame
        rect.origin.x = btnRect.origin.x + (btnRect.size.width - rect.size.width)/2
        rect.origin.y =  (btnRect.origin.y + btnRect.size.height) - rect.size.height
        redline.frame = rect
        
        let minX = CGFloat(0.0)
        let maxX = headerView.contentSize.width - UIScreen.main.bounds.width
        let x = min(max(button.frame.midX - UIScreen.main.bounds.width/2, minX), maxX)
        headerView.setContentOffset(CGPoint(x: x, y: 0.0), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension FEBasePageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let index = viewController.view.tag
        if index == 0 {
            return nil
        }
        return self.dataSource?.previewController(formPage: index - 1)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let index = viewController.view.tag
        if index == number - 1 {
            return nil
        }
        return self.dataSource?.previewController(formPage: index + 1)
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.index = pageViewController.viewControllers?.first?.view.tag ?? 0
        guard self.index < self.number, self.index >= 0 else {
            return
        }
        
        if isShowMenuBar {
            self.selected(self.buttons[self.index])
        }
        
        guard let source = self.dataSource else {
            return
        }
        
        source.currentPageIndex?(index: self.index)
    }
}
