//
//  YYTabBarController..swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import QMUIKit

class YYTabBarController: QMUITabBarViewController,UITabBarControllerDelegate {
    
    static public func tabbarItemVC<T: UIViewController>(for rootVC: T,
                                                         image: UIImage? = nil,
                                                         selectedImage: UIImage? = nil) -> UINavigationController {
        let viewController = rootVC
        viewController.hidesBottomBarWhenPushed = false
        let navc = YYNavigationViewController(rootViewController: viewController)
        let tabBarItem = UITabBarItem(title: viewController.title, image: image, selectedImage: selectedImage)
        navc.tabBarItem = tabBarItem
        return navc
    }
    
    open override func didInitialize() {
        super.didInitialize()
        self.delegate = self
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.selectedViewController?.viewWillAppear(animated)
    }
    
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        viewController.viewWillAppear(false)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
