//
//  SegementSlideViewController.swift
//  Red
//
//  Created by MAC on 2019/8/17.
//  Copyright © 2019 MAC. All rights reserved.
//

import QMUIKit

public class MultiTableView:QMUITableView,UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gesturRecognizer: UIGestureRecognizer,shouldRecognizeSimultaneouslyWith otherGestureRecogizer: UIGestureRecognizer) -> Bool {
        guard gesturRecognizer is UIPanGestureRecognizer && !(otherGestureRecogizer.view is UICollectionView) else { return false }
        return true
    }
}

public class MultiCollectionView: UICollectionView,UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer is UIPanGestureRecognizer && !(otherGestureRecognizer.view is UICollectionView) else { return false}
        return true
    }
}

public class MultiScrollView: UIScrollView,UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer is UIPanGestureRecognizer && !(otherGestureRecognizer.view is UICollectionView) else { return false}
        return true
    }
}

public protocol SegementSlideContentScrollViewDelegate: class {
    
    var scrollView: UIScrollView { get }
    
    ///
    var parentSegementController: SegementSlideViewController? { get set }
    
    var isScrollEnabled: Bool { get set }
    
    func didScroll()
}

extension SegementSlideContentScrollViewDelegate {
    func didScroll(){
        guard self.parentSegementController.isSome else {
            return
        }
        guard isScrollEnabled else {
            //            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: 0)
            return
        }
        if scrollView.contentOffset.y <= 0 && isScrollEnabled {
            self.isScrollEnabled = false
            self.parentSegementController?.isSegementSlideScrollEnable = true
        }
    }
}

open class SegementSlideViewController: FEBaseViewController,UIScrollViewDelegate,SegementContentCollectionViewDelegate,SegementSlideViewDelegate {
    
    open var headerHeight: CGFloat {
        return 0
    }
    
    open var headerInsetTop: CGFloat {
        return 0
    }
    
    open var headerView: UIView? {
        return nil
    }
    
    open var segementHeight: CGFloat {
        return 45
    }
    
    open var segementConfig: SegementSlideViewConfigure {
        return SegementSlideViewConfigure()
    }
    
    open var titlesInSegement: [String] {
        #if DEBUG
        assert(false, "must override this variable")
        #endif
        return []
    }
    
    open var segementSlideContentViewControllers: [SegementSlideContentScrollViewDelegate & UIViewController] {
        #if DEBUG
        assert(false, "must override this variable")
        #endif
        return []
    }
    
    var mSegementSlideContentViewControllers: [SegementSlideContentScrollViewDelegate & UIViewController] = []
    
    public lazy var scrollView: UIScrollView = {
        let aScrollView = UIScrollView()
        aScrollView.bounces = false
        aScrollView.showsVerticalScrollIndicator = false
        aScrollView.showsHorizontalScrollIndicator = false
        aScrollView.size = UIScreen.main.bounds.size
        return aScrollView
    }()
    
    public var contentView: SegementContentCollectionView? = nil
    public var segementSliderView: SegementSlideView? = nil
    
    public var isSegementSlideScrollEnable: Bool = true
    
    public var contentInset: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            contentView?.contentInset = contentInset
            mSegementSlideContentViewControllers.forEach { (vc) in
                if self.headerHeight > 0 {
                    vc.parentSegementController = self
                }
            }
        }
    }
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: (NSCoder?)) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func initSubviews() {
        super.initSubviews()
        view.addSubview(scrollView)
        if let headerView = headerView {
            scrollView.addSubview(headerView)
        }
    }
    
    
    public func reload(index: Int = 0) {
        mSegementSlideContentViewControllers = segementSlideContentViewControllers
        mSegementSlideContentViewControllers.forEach { (vc) in
            if self.headerHeight > 0 {
                vc.parentSegementController = self
            }
        }
        let height = view.height - navigationBarBottom - segementHeight - self.headerInsetTop
        let contentView = SegementContentCollectionView(frame: CGRect(x: 0, y: 0, width: view.width, height: height),
                                                        parentVC: self, childVCs: mSegementSlideContentViewControllers)
        contentView.delegateCollectionView = self
        contentView.contentInset = contentInset
        self.contentView?.removeFromSuperview()
        self.contentView = contentView
        scrollView.addSubview(contentView)
        let segementSliderView = SegementSlideView(frame: CGRect(x: 0, y: navigationBarBottom + headerHeight, width: view.width, height: segementHeight),
                                                   delegate: self,
                                                   titleNames: self.titlesInSegement,
                                                   configure: self.segementConfig)
        segementSliderView.selectedIndex = index
        self.segementSliderView?.removeFromSuperview()
        self.segementSliderView = segementSliderView
        scrollView.addSubview(segementSliderView)
        scrollView.delegate = self
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = view.bounds
        headerView?.frame = CGRect(x: 0, y: 0, width: view.width, height: headerHeight)
        if !QMUIHelper.isLandscape {
            segementSliderView?.size = CGSize(width: view.width, height: segementHeight)
        }
        segementSliderView?.top = headerHeight
        var height = view.height - segementHeight - self.headerInsetTop
        if !self.preferredNavigationBarHidden() {
            height = height - navigationBarBottom
        }
        if QMUIHelper.isLandscape {
            contentView?.top = (segementSliderView?.bottom).or(0)
            return
        }
        
        contentView?.frame = CGRect(x: 0, y: (segementSliderView?.bottom).or(0), width: view.width, height: height)
        contentView?.contentInset = contentInset
        contentView?.flowLayout.itemSize = (contentView?.size).or(CGSize.zero)
        scrollView.contentSize = CGSize(width: view.width, height: (contentView?.bottom).or((segementSliderView?.bottom).or(navigationBarBottom)))
        //        contentView?.collectionView.contentOffset = CGPoint(x: CGFloat((self.segementSliderView?.selectedIndex).or(0)) * view.width, y: 0)
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard self.scrollView === scrollView else { return }
        guard self.headerHeight > 0 else {
            return
        }
        let offsetY = scrollView.contentOffset.y
        didScroll(offsetY: offsetY)
        if !isSegementSlideScrollEnable {
            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: self.headerHeight - headerInsetTop - navigationBarBottom)
            return
        }
        let isScrollEnable = offsetY < (self.headerHeight - navigationBarBottom - headerInsetTop)
        if isScrollEnable != self.isSegementSlideScrollEnable {
            isSegementSlideScrollEnable = isScrollEnable
            self.mSegementSlideContentViewControllers.forEach({$0.isScrollEnabled = !isSegementSlideScrollEnable})
        }
        
    }
    
    open func didScroll(offsetY: CGFloat) {
        
    }
    
    open func pageContentCollectionView(pageContentCollectionView: SegementContentCollectionView, progress: CGFloat, originalIndex: Int, targetIndex: Int) {
        segementSliderView?.update(progress: progress, originalIndex: originalIndex, targetIndex: targetIndex)
    }
    
    open func segementSlideView(segementSlideView: SegementSlideView, index: Int) {
        contentView?.setPageContentCollectionView(index: index)
    }
}
