//
//  FEShowBigPictureView.swift
//  Red
//
//  Created by MAC on 2019/11/21.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import Kingfisher

/// 图片预览
class FEShowBigPictureView2: QMUIImagePreviewViewController {
    private var images: [Any] = []
    private var thumbileImages: [String] = []
    
    private var defaultIndex: Int = 0
    private var currentIndex: Int = 0
    
    private lazy var btoomBar: UIView = {
        let v = UIView()
        v.layer.backgroundColor = UIColor(white: 0, alpha: 0.1).cgColor
        view.addSubview(v)
        return v
    }()
    
    private lazy var indexLabel: QMUILabel = {
        let v = QMUILabel()
        v.textColor = .white
        v.font = UIFont.boldSystemFont(ofSize: 15)
        v.textAlignment = .left
        btoomBar.addSubview(v)
        return v
    }()
    
    private lazy var saveButton: QMUIButton = {
        let v = QMUIButton()
        v.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        v.setTitle("保存", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(saveButtonAction), for: .touchUpInside)
        btoomBar.addSubview(v)
        return v
    }()
    @objc
    private func saveButtonAction() {
        self.save()
    }
    
    init(images: [Any], index: Int = 0, placeHolder: [String] = []) {
        super.init(nibName: nil, bundle: nil)
        self.images = images
        self.defaultIndex = index
        self.currentIndex = index
        
        self.thumbileImages = placeHolder
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePreviewView.delegate = self
        self.imagePreviewView.setCurrentImageIndex(UInt(defaultIndex), animated: true)
        
        indexLabel.text = "\(defaultIndex+1) / \(images.count)"
        
        btoomBar.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.width.equalTo(view)
            make.height.equalTo(50)
            make.bottom.equalTo(view)
        }
        
        indexLabel.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.width.equalTo(150)
            make.height.equalTo(50)
            make.centerY.equalTo(btoomBar)
        }
        
        saveButton.snp.makeConstraints { (make) in
            make.width.equalTo(60)
            make.height.equalTo(40)
            make.centerY.equalTo(btoomBar)
            make.right.equalTo(btoomBar).offset(-10)
        }
    }
    
    private func hide() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func save() {
        let img = self.imagePreviewView.zoomImageView(at: UInt(currentIndex))?.imageView.image
        guard let simg = img else {
            return
        }
        
        FESelectPhotoManager.saveImage(simg) { (err) in
            if let e = err {
                dprint(e)
            } else {
                QMUITips.showInfo("保存成功", in: self.view)
            }
        }
    }
}

extension FEShowBigPictureView2: QMUIImagePreviewViewDelegate {
    func numberOfImages(in imagePreviewView: QMUIImagePreviewView!) -> UInt {
        dprint("numberOfImages")
        return UInt(images.count)
    }
    
    // 单击图片退出
    func singleTouch(inZooming zoomImageView: QMUIZoomImageView!, location: CGPoint) {
        self.hide()
    }
    
    func imagePreviewView(_ imagePreviewView: QMUIImagePreviewView!, assetTypeAt index: UInt) -> QMUIImagePreviewMediaType {
        return .image
    }
    
    func imagePreviewView(_ imagePreviewView: QMUIImagePreviewView!, willScrollHalfTo index: UInt) {
        currentIndex = Int(index)
        
        indexLabel.text = "\(index+1) / \(images.count)"
    }
    
    func imagePreviewView(_ imagePreviewView: QMUIImagePreviewView!, didScrollTo index: UInt) {
    }
    
    func imagePreviewView(_ imagePreviewView: QMUIImagePreviewView!, renderZoomImageView zoomImageView: QMUIZoomImageView!, at index: UInt) {
        
        let ind = Int(index)
        let img = images[ind]
        if let image = img as? UIImage {
            zoomImageView.image = image
        }
        else if let url = img as? String {
            guard let imgUrl = URL(string: url) else {
                return
            }
            
            if !thumbileImages.isEmpty, ind < thumbileImages.count, ind >= 0 {
                // 当前运行内存查找小图片缓存
                zoomImageView.image = ImageCache.default.retrieveImageInMemoryCache(forKey: thumbileImages[ind])
            }

            let activity = UIActivityIndicatorView(style: .whiteLarge)
            activity.size = CGSize(width: 60, height: 60)
            activity.center = zoomImageView.imageView.center
            activity.startAnimating()
            zoomImageView.imageView.addSubview(activity)
            
            var imgv: UIImageView? = UIImageView()
            // download large img
            imgv!.kf.setImage(with: imgUrl) {(res) in
                activity.stopAnimating()
                activity.removeFromSuperview()
                
                zoomImageView.image = imgv!.image
                imgv = nil
            }
        }
    }
    
}
