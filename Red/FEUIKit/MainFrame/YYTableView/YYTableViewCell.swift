//
//  YYTableViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class YYTableViewCell: QMUITableViewCell {

    open override func didInitialize(with style: UITableViewCell.CellStyle) {
        super.didInitialize(with: style)
        self.tintColor = Color.tint
    }
    
    open func fixSubViewColors() {}
    
    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        fixSubViewColors()
    }
    
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        fixSubViewColors()
    }


}
