//
//  YYListViewController.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class YYListSectionHeaderView: QMUITableViewHeaderFooterView {
}

public typealias YYListSection = (title: String, items:[YYListItem])
/// 可作为项目中所有**列表视图**的基类
open class YYListViewController: YYTableViewController {
    public var sections: [YYListSection] = []
    
    public func item(for indexPath: IndexPath) -> YYListItem {
        return sections[indexPath.section].items[indexPath.row]
    }
    
    public func item(for title: String) -> YYListItem? {
        return sections.flatMap({$0.items}).first(where: {$0.title == title})
    }
    
    public func indexPath(for title: String) -> IndexPath? {
        for (section,sectionItem) in sections.enumerated() {
            for (row,rowItem) in sectionItem.items.enumerated() {
                if rowItem.title == title {
                    return IndexPath(row: row, section: section)
                }
            }
        }
        return nil
    }
    
    public func reload(for title: String) {
        guard let indexPath = indexPath(for: title) else { return }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: UITableView.Style.grouped)
    }
    
    required public init?(coder aDecoder: (NSCoder?)) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: YYListTableViewCell.self)
        self.tableView.register(headerFooterViewClassWith: YYListSectionHeaderView.self)
    }
    
    override open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNonzeroMagnitude : 20
    }
    
    override open func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    override open func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].items.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50*kScreenScale
    }
    
    override open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section].title
    }
    
    override open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withClass: YYListSectionHeaderView.self)
        return header
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.item(for: indexPath)
        let cell = tableView.dequeueReusableCell(withClass: YYListTableViewCell.self, for: indexPath)
        cell.update(with: item)
        return cell
    }
    
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: YYListTableViewCell.self)
        tableView.reloadData()
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.item(for: indexPath)
        self.didSelectetItem(item)
    }
    
    open func didSelectetItem(_ item: YYListItem) {}
}

// MARK: - QMUINavigationTitleViewDelegate
extension YYListViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}
