//
//  YYListItem.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import UIKit

open class YYListItem {
    public enum ActionType: Equatable {
        case none
        case push
        case `switch`
        case image
        case disable
    }
    
    public var image: UIImage?
    public let title: String
    public var action: ActionType
    public let titlePosition: NSTextAlignment
    
    /// 圆角倍数默认为0.5(即为设置圆形)
    public var imageCorner: CGFloat? = 0.5
    
    public var value: Any? {
        didSet {
            guard let newValue = value else { return }
            self.valueChangedBLock?(newValue)
        }
    }
    
    public var valueChangedBLock: ((Any)->Swift.Void)?
    public init(title: String,
                image: UIImage? = nil,
                titlePosition: NSTextAlignment = .left,
                action: ActionType = .push,
                value: Any? = nil) {
        self.image = image
        self.title = title
        self.action = action
        self.value = value
        self.titlePosition = titlePosition
    }
    
    public init(title: String,
                image: UIImage? = nil,
                imageCorner: CGFloat? = nil,
                titlePosition: NSTextAlignment = .left,
                action: ActionType = .push,
                value: Any? = nil) {
        self.image = image
        self.title = title
        self.action = action
        self.value = value
        self.titlePosition = titlePosition
        self.imageCorner = imageCorner
    }
}

extension YYListItem: CustomDebugStringConvertible {
    public var debugDescription: String {
        return """
        标题:\(title)
        事件:\(action)
        值:\(value ?? "")
        """
    }
}
