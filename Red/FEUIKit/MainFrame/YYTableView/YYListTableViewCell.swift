//
//  YYListTableViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
class YYListTableViewCell: YYTableViewCell {

    private lazy var iSwitch: UISwitch = {
        let aSwitch = UISwitch()
        return aSwitch
    }()
    
    private lazy var rightImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        self.contentView.addSubview(imageView)
        return imageView
    }()
    
    public weak var item: YYListItem?
    
    public lazy var infoLabel: QMUILabel = {
        let label = QMUILabel()
        label.clipsToBounds = true
        label.contentEdgeInsets = UIEdgeInsets(top: 1, left: 6, bottom: 1, right: 6)
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = Color.text2
        self.contentView.addSubview(label)
        return label
    }()
    
    open override func didInitialize(with style: UITableViewCell.CellStyle) {
        super.didInitialize(with: style)
        self.textLabel?.font = UIFont.systemFont(ofSize: 15)
    }
    
    override open func updateAppearance(with indexPath: IndexPath!) {
        super.updateAppearance(with: indexPath)
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        infoLabel.sizeToFit()
        if infoLabel.width > 150 {
            infoLabel.width = 150
        }
        infoLabel.layer.cornerRadius = infoLabel.height/2
        infoLabel.centerY = contentView.height/2
        if case YYListItem.ActionType.none? = item?.action {
            infoLabel.right = contentView.width - 38
        }else if case YYListItem.ActionType.disable? = item?.action {
            infoLabel.right = contentView.width - 38
        }
        else {
            infoLabel.right = contentView.width - 2
        }
        
        let imageHeight = contentView.height - 14
        rightImageView.size = CGSize(width: imageHeight, height: imageHeight)
        rightImageView.layer.cornerRadius = rightImageView.height * item!.imageCorner!
        rightImageView.centerY = contentView.height/2
        rightImageView.right = contentView.width - 2
    }
    
}


extension YYListTableViewCell {
    open func update(with item: YYListItem) {
        self.item = item
        self.textLabel?.text = item.title
        self.textLabel?.textAlignment = item.titlePosition
        self.imageView?.image = item.image
        self.infoLabel.textColor = Color.text2
        if let value = item.value {
            if let attrValue = value as? NSAttributedString {
                self.infoLabel.attributedText = attrValue
                self.infoLabel.backgroundColor = attrValue.attributes(at: 0, effectiveRange: nil)[.backgroundColor] as? UIColor
            }else if let subTitle = value as? String {
                self.infoLabel.text = subTitle
            }else {
                self.infoLabel.text = ""
            }
        }
        switch item.action {
        case .none:
            self.selectionStyle = .none
            self.accessoryType = .none
            self.rightImageView.isHidden = true
        case .push:
            self.selectionStyle = .default
            self.accessoryType = .disclosureIndicator
            self.rightImageView.isHidden = true
        case .switch:
            self.selectionStyle = .none
            self.iSwitch.onTintColor = Color.red
            self.iSwitch.addTarget(self, action: #selector(handleValueChangeEvent(sender:)), for: UIControl.Event.valueChanged)
            self.iSwitch.isOn = item.value as! Bool
            self.accessoryView = iSwitch
            self.rightImageView.isHidden = true
        case .image:
            self.selectionStyle = .default
            self.rightImageView.isHidden = false
            if let url = item.value as? URL {
                rightImageView.setImage(with: url)
            } else if let image = item.value as? UIImage {
                rightImageView.image = image
            }
            self.selectionStyle = .default
            self.accessoryType = .disclosureIndicator
        case .disable:
            self.selectionStyle = .none
            self.accessoryType = .none
            self.rightImageView.isHidden = true
            self.infoLabel.textColor = Color.text3
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    @objc
    open func handleValueChangeEvent(sender: Any) {
        if let control = sender as? UISwitch {
            self.item?.value = control.isOn
        }
    }

}
