//
//  YYTableViewController.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import MJRefresh

//tableView的基类
open class YYTableViewController: QMUICommonTableViewController,ChangingThemeDelegate {
    
    /// 默认加载数量
    var loadCount = 20
    
    lazy var nodataView: UILabel = {
        let v = UILabel()
        v.text = "暂无数据"
        v.textColor = UIColor.RGBColor(136, 136, 136)
        v.font = UIFont.systemFont(ofSize: 25)
        v.textAlignment = .center
        v.numberOfLines = 0
        v.size = CGSize(width: kScreenSize.width, height: 80)
        v.center = CGPoint(x: v.size.width/2, y: self.tableView.height/2)
        self.tableView.insertSubview(v, at: 0)
        return v
    }()
    
    override open func didInitialize(with  style:UITableView.Style) {
        super.didInitialize(with: style)
        NotificationCenter.default.addObserver(self, selector: #selector(handleThemeChanged(notification:)), name: .themeChanged, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override open func preferredNavigationBarHidden() -> Bool {
        return false
    }
    
    override open func shouldCustomizeNavigationBarTransitionIfHideable() -> Bool {
        return true
    }
    
    open var isNeedLoadMore:Bool {
        return false
    }
    
    open var isNeedReload: Bool {
        return false
    }
    
//    public var refreshFooterView: MJRefreshAutoStateFooter? {
//        return tableView.mj_footer as? MJRefreshAutoStateFooter
//    }
    
//    open lazy var refreshFooterView: MJRefreshAutoNormalFooter = {
//        let f = MJRefreshAutoNormalFooter(
//    }
    
    /// 下拉刷新
    public lazy var mj_head: MJRefreshNormalHeader = {
        let h = MJRefreshNormalHeader()
        h.lastUpdatedTimeLabel.isHidden = true
        h.stateLabel.isHidden = true
        self.tableView.mj_header = h
        return h
    }()
    
    /// 上拉刷新
    public lazy var mj_foot: MJRefreshAutoNormalFooter = {
        let f = MJRefreshAutoNormalFooter()
        f.stateLabel.text = ""
        self.tableView.mj_footer = f
        return f
    }()
    
    open override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: animated)
    }
    
    public func hidHeaderView(animated: Bool) {
        let contentOffset = CGPoint(x: self.tableView.contentOffset.x, y: (self.tableView.tableHeaderView?.height).or(0)  - self.tableView.qmui_contentInset.top)
        self.tableView.setContentOffset(contentOffset, animated: animated)
    }
    
    open override func initTableView() {
        super.initTableView()
        self.tableView.showsVerticalScrollIndicator = false
        if isNeedReload {
            ///TODO 自定义头
           
        }
        if isNeedLoadMore {
            
        }
        self.tableView.keyboardDismissMode = .interactive
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        self.tableView.cellLayoutMarginsFollowReadableWidth = false
        self.tableView.estimatedRowHeight = 0
        
    }
    
    open override func showEmptyView() {
        if self.emptyView.isNone {
            ///TODO 添加网络错误页
        }
        if let emptyView = self.emptyView {
            self.view.insertSubview(emptyView, aboveSubview: self.tableView)
        }
    }
    
    open override func hideEmptyView() {
        if isEmptyViewShowing {
            super.hideEmptyView()
        }
    }
    
    @objc
    public func handleRefreshEvent(sender _: Any) {
        
        refreshData()
    }
    
    @objc
    public func handleLoadMoreDataEvent(sender _: Any) {
       
        loadMoreData()
    }
    
    public func fixTableViewLayout() {
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    @objc
    open func refreshData() {
        
    }
    
    @objc
    open func loadMoreData() {
        
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        if isNeedReload  {
            //替换其他组件
//            self.tableView.mj_header?.executeRefreshingCallback()
        }
    }
    
    @objc
    func handleThemeChanged(notification: Notification) {
        guard let oldTheme = notification.userInfo?["oldTheme"] as? ThemeProtocol,
            let newTheme = notification.userInfo?["newTheme"] as? ThemeProtocol
            else { return }
        self.themChanged(form: oldTheme, to: newTheme)
    }
    
    public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        view.backgroundColor = ThemeManager.default.configuration.backgroundColor
        tableView.backgroundColor = ThemeManager.default.configuration.backgroundColor
        titleView?.tintColor = ThemeManager.default.configuration.navBarTitleColor
        tableView.separatorColor = ThemeManager.default.configuration.separatorColor
    }
}
