//
//  ThemeProtocol.swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import QMUIKit

public protocol ThemeProtocol: QMUIConfigurationTemplateProtocol {
    ///主题名称
    var name: String { get }
    /// 主题颜色
    var tintColor: UIColor { get }
    /// 次要主题色
    var secondaryColor: UIColor { get }
    /// 主题主要字体颜色
    var textColor: UIColor { get }
    ///
    var text2Color: UIColor { get }
    
    var text3Color: UIColor { get }
    
    /// 主题背景色
    var backgrountColor: UIColor { get }
    /// 列表标题颜色
    var listTitleColor: UIColor { get }
}

public protocol ChangingThemeDelegate {
    func themChanged(form oldTheme:ThemeProtocol? ,to newTheme:ThemeProtocol?)
}
