//
//  FESliderView.swift
//  Red
//
//  Created by MAC on 2019/12/3.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FESliderView: UIView {
    fileprivate var sliders: [String] = []
    
    fileprivate var removeHandler: (() -> Void)?
    
    /// PageControl
    fileprivate lazy var pageControl: UIPageControl = {
        let pc = UIPageControl(frame: CGRect(x: 0, y: self.frame.size.height - 40 - (kIsIphoneX ? 20 : 0), width: self.frame.size.width, height: 40))
        pc.numberOfPages = sliders.count
        pc.currentPage = 0
        pc.currentPageIndicatorTintColor = .white
        pc.pageIndicatorTintColor = UIColor(white: 0, alpha: 0.2)
        self.addSubview(pc)
        return pc
    }()
    
    fileprivate lazy var contentSlider: UIScrollView = {
        let v = UIScrollView()
        v.frame = self.bounds
        v.delegate = self
        v.isPagingEnabled = true
        v.bounces = false
        v.showsHorizontalScrollIndicator = false
        v.showsVerticalScrollIndicator = false
        v.contentSize = CGSize(width: v.width * CGFloat(sliders.count), height: 0)
        self.addSubview(v)
        return v
    }()
    
    public lazy var enterButton: UIButton = {
        let v = UIButton(type: .custom)
        v.bounds = CGRect(x: 0, y: 0, width: 160, height: 35)
        v.setTitle("立即体验", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.titleLabel!.font = UIFont.boldSystemFont(ofSize: 18)
        v.layer.cornerRadius = 5
        v.layer.borderWidth = 2
        v.layer.borderColor = UIColor.white.cgColor
        v.addTarget(self, action: #selector(enterAction), for: .touchUpInside)
        return v
    }()
    
    public static func initialSlider(_ images: [String], _ inView: UIView, handler: (() -> Void)? = nil) {
        let slider = FESliderView(images, inView.bounds)
        slider.removeHandler = handler
        inView.addSubview(slider)
    }
    
    init(_ images: [String], _ frame: CGRect) {
        sliders = images
        super.init(frame: frame)
        self.createUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FESliderView {
    fileprivate func createUI() {
        var x: CGFloat = 0
        for (i,s) in sliders.enumerated() {
            let v = UIImageView()
            v.frame = CGRect(x: x, y: 0, width: contentSlider.width, height: contentSlider.height)
            v.image = UIImage(named: s)
            v.isUserInteractionEnabled = false
            contentSlider.addSubview(v)
            x += v.width
            
            if i == sliders.count-1 {
                contentSlider.addSubview(self.enterButton)
                self.enterButton.left = v.left + (v.width-self.enterButton.width)/2
                self.enterButton.bottom = v.bottom - 50
            }
        }
        self.bringSubviewToFront(pageControl)
    }
}

extension FESliderView: UIScrollViewDelegate, CAAnimationDelegate {
    @objc fileprivate func enterAction() {
        doremove()
    }
    
    fileprivate func doremove() {
        self.removeHandler?()
        
        UIView.animate(withDuration: 1, animations: {
            self.alpha = 0.001
            self.transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x/scrollView.bounds.size.width)
        pageControl.currentPage = page
    }
}
