//
//  FELaunchViewController.swift
//  Red
//
//  Created by MAC on 2019/8/23.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FELaunchViewController: YYNavigationViewController {
    lazy var sliders: [String] = {
        return ["slider-1.png","slider-2.png"]
    }()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.defaultThemeConfigure()
        
        if setupSlider() {
            return
        }
         
        showLogin()
    }
}

extension FELaunchViewController {
    /// 默认主题设置
    func defaultThemeConfigure() {
        // 默认配置
        
        let redColor = UIColor.RGBColor(243, 52, 81)
        ThemeManager.default.configuration.navBarBackgroundImage = UIImage(named: "nav_bar_image")!.sls_tintColor_image(redColor)
        ThemeManager.default.configuration.navBarTitleColor = .white
        ThemeManager.default.configuration.navBarTintColor = .white
        ThemeManager.default.configuration.tabBarItemTitleColorSelected = redColor
        ThemeManager.default.configuration.tabBarItemImageColorSelected = redColor
        ThemeManager.default.configuration.redColor = redColor
        ThemeManager.default.configuration.greenColor = UIColor.RGBColor(37, 155, 78)
    }
    
    static func parsePasteboardData() {
        //MARK: 检测粘贴板有没有邀请码
        // 注意：邀请码固定8位，且以GB开头
        let sysPasteBoard = UIPasteboard.general
        guard let items = sysPasteBoard.string?.components(separatedBy: "&") else {
            return
        }
        
        var groupId: String?
        var userid: String?
        for (_, seg) in items.enumerated() {
            if (seg.hasPrefix("CODE") || seg.hasPrefix("CODE".lowercased())), let code = seg.components(separatedBy: "=").last {
                // 邀请码
                UserDefaults.Common.firstInstallInviteCode = code
            } else if (seg.hasPrefix("GROUPID") || seg.hasPrefix("GROUPID".lowercased())), let gid = seg.components(separatedBy: "=").last {
                groupId = gid
            } else if (seg.hasPrefix("USERID") || seg.hasPrefix("USERID".lowercased())), let uid = seg.components(separatedBy: "=").last {
                userid = uid
            }
        }
        
        if let gid = groupId, let uid = userid {
            // 缓存邀请群的信息
            UserDefaults.Common.ownerGroupFriendInvite = "\(gid),\(uid)"
        }
    }
    
    func setupSlider() -> Bool {
        guard UIApplication.showSlider else {
            return false
        }

        // 第一次安装
        // 解析粘贴板数据
        FELaunchViewController.parsePasteboardData()
        
        // 显示slider
        FESliderView.initialSlider(sliders, self.view) {[weak self] in
            self?.showLogin()
        }
        return true
    }
    
    func showLogin() {
        let login = FELoginViewController()
        login.isAllowAutoLogin = true
        self.setViewControllers([login], animated: true)
    }
}

