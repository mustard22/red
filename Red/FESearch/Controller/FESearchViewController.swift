//
//  FESearchViewController.swift
//  Red
//
//  Created by MAC on 2020/3/13.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

enum FESearchActionType: Int {
    case none
    /// 搜索自建群的可邀请好友
    case ownerGroupInviteFriend
    /// 搜索自建群成员
    case ownerGroupMember
}

class FESearchView: UIView {
    private lazy var tableView: QMUITableView = {
        let v = QMUITableView(frame: .zero, style: .plain)
        v.delegate = self
        v.dataSource = self
        self.addSubview(v)
        return v
    }()
    
    lazy var input: QMUITextField = {
        let v = QMUITextField("搜索", font: UIFont.systemFont(ofSize: 15), color: UIColor.darkGray)
        v.delegate = self
        v.returnKeyType = .search
        v.backgroundColor = .white
        v.layer.masksToBounds = true
        v.layer.cornerRadius = 2
        
        let lback = UIView()
        lback.size = CGSize(width: 50, height: 20)
        let left = UIImageView()
        left.image = UIImage(named: "icon_nav_search")
        left.frame = CGRect(x: 15, y: 0, width: 20, height: 20)
        lback.addSubview(left)
        v.leftView = lback
        v.leftViewMode = .always
        
        return v
    }()
    
    lazy var cancelButton: QMUIButton = {
        let v = QMUIButton("取消", fontSize: 14)
        v.bounds = CGRect(x: 0, y: 0, width: 60, height: 35)
        v.setTitleColor(.darkGray, for: .normal)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
        return v
    }()
    
    @objc private func cancelButtonAction() {
        hide()
    }
    
    lazy var topbar: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: self.width
        , height: kNaviBarHeight)
        
        
        self.input.frame = CGRect(x: 10, y: kNaviBarHeight - 35 - 5, width: kScreenSize.width - 10 - self.cancelButton.width, height: 35)
        v.addSubview(self.input)
        
        self.cancelButton.centerY = self.input.centerY
        self.cancelButton.left = self.input.right
        v.addSubview(self.cancelButton)
        
        return v
    }()
    
    //MARK: public vars
    private var searchType = FESearchActionType.none
    private var groupId: String?
    
    public var selectHandler: ((_ item: Any)->Void)?
    
    private var datas: [Any] = []
    
    init(searchType: FESearchActionType, groupId: String? = nil) {
        self.searchType = searchType
        self.groupId = groupId
        
        super.init(frame: UIScreen.main.bounds)
        
//        self.backgroundColor = UIColor.RGBColor(240, 240, 240)
        
        self.addSubview(self.topbar)
        
        self.initialTableView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    
    
    func initialTableView() {
        if searchType == .none {
            return
        }
        
        self.tableView.frame = CGRect(x: 0, y: self.topbar.height, width: self.width, height: self.height-self.topbar.height)
        
        registerAllCells()
    }
    
    func registerAllCells() {
        self.tableView.registerCell(withClass: GroupAllMemberCell.self)
        self.tableView.register(UINib(nibName: "OwnerGroupInviteMemberCell", bundle: nil), forCellReuseIdentifier: "OwnerGroupInviteMemberCell")
    }
    
    public func show() {
        let win = UIApplication.shared.keyWindow!
        win.addSubview(self)
        
        self.input.becomeFirstResponder()
        UIView.animate(withDuration: 0.3) {
            self.backgroundColor = UIColor.RGBColor(240, 240, 240)
            self.tableView.backgroundColor = UIColor.RGBColor(240, 240, 240)
        }
    }
    
    private func hide() {
        self.endEditing(true)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
}


extension FESearchView: QMUITableViewDelegate, QMUITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.searchType == .ownerGroupInviteFriend {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerGroupInviteMemberCell", for: indexPath) as! OwnerGroupInviteMemberCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withClass: GroupAllMemberCell.self, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.searchType == .ownerGroupInviteFriend {
            let cell0 = cell as! OwnerGroupInviteMemberCell
            let item = self.datas[indexPath.row] as! OwnerGroupInviteItem
            cell0.update(isSelected: item.flag, avatar: item.avatar, name: item.name)
            return
        }
        
        let cell0 = cell as! GroupAllMemberCell
        let item = self.datas[indexPath.row] as! RoomUserItem
        cell0.update(item)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let handler = self.selectHandler else {
            return
        }
        
        handler(self.datas[indexPath.row])
        self.hide()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.input.resignFirstResponder()
    }
}


//MARK: 网络请求
extension FESearchView: QMUITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dowithSearch()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.datas.removeAll()
        self.tableView.reloadData()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.tintColor = Color.red
        return true
    }
    
    private func dowithSearch() {
        if searchType == .ownerGroupMember {
            searchOwnerGroupMember()
        } else if searchType == .ownerGroupInviteFriend {
            searchOwnerGroupInviteFriends()
        } else {
            
        }
    }
    
    private func searchOwnerGroupInviteFriends() {
        guard let gid = self.groupId, gid.count > 0 else {
            return
        }
        
        guard let key = self.input.text, key.count > 0 else {
            return
        }
        
        let p = OwnerGroupInviteFriendListParam(groupId: gid, pageIndex: "1", pageSize: "1", keyWord: key)
        HttpClient.default.ownerGroupAllowInviteFriends(param: p) { (item) in
            guard let items = item.data else {
                return
            }
            
            self.datas.removeAll()
            self.datas = items.map({(fitem) -> OwnerGroupInviteItem in
                var item = OwnerGroupInviteItem()
                item.name = fitem.friend_nickname
                item.uid = fitem.friend_id
                item.avatar = fitem.friend_avatar
                return item
            })
            
            self.tableView.reloadData()
        }
    }
    
    private func searchOwnerGroupMember() {
        guard let gid = self.groupId, gid.count > 0 else {
            return
        }
        
        guard let key = self.input.text, key.count > 0 else {
            return
        }
        
        let p = OwnerGroupMemberParam(group_id: gid, pageIndex: "1", pageSize: "1", keyWord: key)
        HttpClient.default.ownerGroupMemberList(param: p) { (ms) in
            self.datas.removeAll()
            self.datas = ms.member
            self.tableView.reloadData()
        }
    }
}
