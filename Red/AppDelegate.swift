//
//  AppDelegate.swift
//  Red
//
//  Created by MAC on 2019/8/1.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    // bundle id : FEChatRedEnvelope.com
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // 记录APP打开次数
        UserDefaults.Common.appOpenTime = UserDefaults.Common.appOpenTime + 1
        
        HttpClient.default.host = .qa

        UserCenter.default.getPubkey()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = FELaunchViewController()
        self.window?.makeKeyAndVisible()
        
        self.registerThirdPlatform()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        dprint("--------\nfunc applicationWillTerminate(_ application: UIApplication)\n--------")
    }

    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return WXApi.handleOpen(url, delegate: self)
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        //MARK: Safari跳转APP
        if url.absoluteString.hasPrefix("sls://") || url.absoluteString.hasPrefix("psq://") || url.absoluteString.hasPrefix("qhb://") {
            // 解析粘贴板数据
            FELaunchViewController.parsePasteboardData()
            if User.default.user_id.count > 1 {
                // 已经登录的才会发送通知 进行操作
                NotificationCenter.default.post(name: NSNotification.Name.ownerGroupFriendInvite, object: nil, userInfo: nil)
            } else {
                // 未登录的走登录(注册)流程
            }
            
            return true
        }
        return WXApi.handleOpen(url, delegate: self)
    }
    
    
//    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//        return WXApi.handleOpenUniversalLink(userActivity, delegate: self)
//    }
    
//
//    func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
////        dprint(userActivityType)//NSUserActivityTypeBrowsingWeb
//        return true
//    }
}


