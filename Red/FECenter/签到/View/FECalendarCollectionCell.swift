//
//  FECalendarCollectionCell.swift
//  Red
//
//  Created by MAC on 2020/4/10.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

class FECalendarCollectionCell: UICollectionViewCell {
    public lazy var titleLabel: UILabel = {
        let v = UILabel()
        v.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 20)
        v.textColor = .darkGray
        
        v.layer.cornerRadius = 20
        v.layer.masksToBounds = true
        v.layer.backgroundColor = UIColor.white.cgColor
        self.contentView.addSubview(v)
        return v
    }()
    
    public lazy var selectImage: UIImageView = {
        let v = UIImageView()
        v.bounds = CGRect(x: 0, y: 0, width: 14, height: 14)
        v.image = UIImage(named: "ic_selected")
        v.isHidden = true
        self.contentView.addSubview(v)
        return v
    }()
    
    public lazy var todaySignImageView: UIImageView = {
        let v = UIImageView()
        v.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
        v.image = UIImage(named: "ic_sign2")
        v.isHidden = true
        self.contentView.addSubview(v)
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.backgroundColor = .white
        titleLabel.center = contentView.center
        
        todaySignImageView.center = contentView.center
        
        selectImage.top = contentView.top
        selectImage.right = contentView.right
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
