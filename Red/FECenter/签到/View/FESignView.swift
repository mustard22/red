//
//  FESignView.swift
//  Red
//
//  Created by MAC on 2020/4/9.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

class FESignView: UIView {
    
    @IBOutlet weak var barBackView: UIView!
    @IBOutlet weak var topbar: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var weekView: UIView!
    
    private var weekButtons: [UIButton] = []
    
    let layout = WaterfallFlowLayout()
    
    let identifier = "FECalendarCollectionCell"
    
    private let itemHeight: CGFloat = 50
    
    var model = CalendarModel()
    
    var didselectHandler: ((_ item: DateItem)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        topbar.layer.backgroundColor = Color.red.cgColor
        layout.delegate = self
        
        collectionView.register(FECalendarCollectionCell.self, forCellWithReuseIdentifier: identifier)
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    private func initialWeekButtons() {
        if self.weekButtons.count == WeekDay.allCases.count {
            return
        }
        
        let size = self.width / CGFloat(WeekDay.allCases.count)
        var x: CGFloat = 0
        for (i, w) in WeekDay.allCases.enumerated() {
            let btn = UIButton()
            btn.tag = i
            btn.setTitle(w.name, for: .normal)
            btn.setTitleColor(UIColor.darkGray, for: .normal)
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
            btn.frame = CGRect(x: x, y: 0, width: size, height: self.weekView.height)
            self.weekView.addSubview(btn)
            self.weekButtons.append(btn)
            
            x += size
        }
    }
    
    func dowithInitial() {
        self.initialWeekButtons()
        
        model.loadCurrentMonthData()
        
        collectionView.reloadData()
    }
    
    func refreshCalendar(_ signed: [String]) {
        model.data = model.data.map({(item) -> DateItem in
            if item.title.isEmpty || !signed.contains(item.title) {
                return item
            }
            var m = item
            m.signed = true
            return m
        })
        
        collectionView.reloadData()
    }
    
    deinit {
        layout.delegate = nil
    }
}

//MARK: WaterFlowLayoutProtocol
extension FESignView: WaterFlowLayoutProtocol {
    // 计算item高度
    func waterFallLayout(waterFallLayout: WaterfallFlowLayout, heightForItemAtIndexPath indexPath: Int, itemWidth: CGFloat) -> CGFloat {
        return itemHeight
    }
    
    // 自定义列数
    func columnCountInWaterFallLayout(waterFallLayout: WaterfallFlowLayout) -> Int {
        return WeekDay.allCases.count
    }
    
    func rowMarginInWaterFallLayout(waterFallLayout: WaterfallFlowLayout) -> CGFloat {
        return 0
    }
    
    func columnMarginInWaterFallLayout(waterFallLayout: WaterfallFlowLayout) -> CGFloat {
        return 0
    }
    
    /// 每个item的内边距
    func edgeInsetdInWaterFallLayout(waterFallLayout: WaterfallFlowLayout) -> UIEdgeInsets {
        return .zero
    }
}


extension FESignView: UICollectionViewDelegate, UICollectionViewDataSource {
    // 设置item个数
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! FECalendarCollectionCell
        
        let ritem = model.data[indexPath.row]
        cell.titleLabel.text = ritem.title
        cell.titleLabel.layer.backgroundColor = UIColor.white.cgColor
        cell.titleLabel.textColor = .darkGray
        
        cell.selectImage.isHidden = true
        cell.todaySignImageView.isHidden = true
        if ritem.isToday {
            if ritem.signed {
                cell.todaySignImageView.isHidden = false
                cell.titleLabel.isHidden = true
            }
        } else {
            if ritem.signed {
                cell.titleLabel.textColor = Color.red
                cell.selectImage.isHidden = false
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.didselectHandler?(model.data[indexPath.row])
    }
}
