//
//  FECenterSignModel.swift
//  Red
//
//  Created by MAC on 2020/4/10.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

public struct FESignModel {
    var signRecord = UserSignRecordItem()
    var signItem = UserSignItem()
    
    mutating func loadSignRecordData(_ handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.userSignRecord { (flag, msg, item) in
            unsafePointSelf.pointee.signRecord = item
            handler(flag, msg)
        }
    }
    
    mutating func dosign(_ handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.userSign { (flag, msg, item) in
            unsafePointSelf.pointee.signItem = item
            handler(flag, msg)
        }
    }
}


public struct CalendarModel {
    private let calendar = Calendar(identifier: .gregorian)
    
    /// 日历显示的date
    public var date = Date()
    
    func year() -> Int {
        return calendar.dateComponents([.year], from: date).year ?? 1970
    }
    
    func month() -> Int {
        return calendar.dateComponents([.month], from: date).month ?? 1
    }
    
    func today() -> Int {
        return calendar.dateComponents([.day], from: Date()).day ?? 1
    }
    
    //MARK: 获取当月第一天是周几
    private func getFirstDayInDateMonth() -> Int {
        var specifiedDateCom = calendar.dateComponents([.year,.month], from: date)
        specifiedDateCom.setValue(1, for: .day)
        let startOfMonth = calendar.date(from: specifiedDateCom)
        let weekDayCom = calendar.component(.weekday, from: startOfMonth!)
        return weekDayCom
    }
    
    //MARK: 获取指定月份天数
    private func calculateDaysInDateMonth() -> Int {
        // 指定日期转换
        let specifiedDateCom = calendar.dateComponents([.year,.month,.day], from: date)
        // 指定日期所在月的第一天
        var startCom = DateComponents()
        startCom.day = 1
        startCom.month = specifiedDateCom.month
        startCom.year = specifiedDateCom.year
        
        let startDate = calendar.date(from: startCom)
        // 指定日期所在月的下一个月第一天
        var endCom = DateComponents()
        endCom.day = 1
        endCom.month = specifiedDateCom.month == 12 ? 1 : specifiedDateCom.month! + 1
        endCom.year = specifiedDateCom.month == 12 ? specifiedDateCom.year! + 1 : specifiedDateCom.year
        let endDate = calendar.date(from: endCom)
        //计算指定日期所在月的总天数
        let days = calendar.dateComponents([.day], from: startDate!, to: endDate!)
        let count = days.day ?? 0
        return count
    }
    
    var data: [DateItem] = []
    
    mutating func loadCurrentMonthData() {
        if data.count > 0 {
            return
        }
        
        let firstDay = self.getFirstDayInDateMonth()
        let count = self.calculateDaysInDateMonth()
        let today = self.today()
        data.removeAll()
        if firstDay - 1 > 0 {
            data = (0..<firstDay-1).map({_ in DateItem()})
        }
        
        data.append(contentsOf: (1...count).map({DateItem(title: "\($0)", signed: false, isToday: $0 == today ? true : false)}))
    }
}

//MARK: - 日历显示的day item
public struct DateItem {
    var title = ""
    /// 是否已签到
    var signed: Bool = false
    /// 是不是今天
    var isToday: Bool = false
}


public enum WeekDay: CaseIterable {
    case Sunday
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    
    var name: String {
        switch self {
        case .Sunday: return "日"
        case .Monday: return "一"
        case .Tuesday: return "二"
        case .Wednesday: return "三"
        case .Thursday: return "四"
        case .Friday: return "五"
        case .Saturday: return "六"
        }
    }
}
