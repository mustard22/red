//
//  FECenterSignViewController.swift
//  Red
//
//  Created by MAC on 2020/4/9.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit


class FECenterSignViewController: FEBaseViewController {
    private lazy var calendarView: FESignView = {
        let v = Bundle.main.loadNibNamed("FESignView", owner: self, options: nil)?.first as! FESignView
        v.backgroundColor = .white
        v.frame = CGRect(x: 0, y: kNaviBarHeight, width: view.bounds.size.width, height: v.height)
        content.addSubview(v)
        return v
    }()
    
    private lazy var content: UIScrollView = {
        let v = UIScrollView()
        v.frame = view.bounds
        v.showsVerticalScrollIndicator = false
        view.addSubview(v)
        return v
    }()
    
    private lazy var signButton: UIButton = {
        let v = UIButton()
        v.frame = CGRect(x: signbarView.width - 15 - 60, y: (signbarView.height-40)/2, width: 60, height: 40)
        v.setTitle("签到", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(rightTopAction), for: .touchUpInside)
        
        v.layer.cornerRadius = 3
        v.layer.backgroundColor = Color.red.cgColor
        
        signbarView.addSubview(v)
        return v
    }()
    
    private lazy var signbarView: UIView = {
        let v = UIView()
        v.bounds = CGRect(x: 0, y: 0, width: content.width, height: 50)
        content.addSubview(v)
        return v
    }()
    
    // 积分
    private lazy var integralLabel: UILabel = {
        let v = UILabel()
        v.textColor = .darkGray
        v.textAlignment = .left
        v.frame = CGRect(x: 15, y: 0, width: 150, height: signbarView.height)
        v.font = UIFont.systemFont(ofSize: 15)
        signbarView.addSubview(v)
        return v
    }()
    
    private lazy var textView: UITextView = {
        let v = UITextView()
        v.frame = CGRect(x: 0, y: calendarView.bottom, width: calendarView.width , height: 300)
        v.isEditable = false // 不允许编辑
        v.backgroundColor = .white
        v.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        content.addSubview(v)
        return v
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        calendarView.top = 0
        
        signbarView.top = calendarView.bottom
        signbarView.left = 0
        
        textView.top = signbarView.bottom
        
        integralLabel.left = 15
        signButton.right = signbarView.width - 15
        signButton.top = (signbarView.height - signButton.height) / 2
        
        var csize = content.contentSize
        csize.height = textView.bottom + 20
        content.contentSize = csize
    }
    
    private var model = FESignModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "签到"
        
        // 默认未签到
        self.freshSignButtonStatus(false)
        
        self.calendarView.dowithInitial()
        
        calendarView.didselectHandler = {[weak self] (item) in
            self?.clickCalendarDateItem(item)
        }
        
        self.loadSignRecordData()
    }

    //MARK: 点击日历签到
    private func clickCalendarDateItem(_ item: DateItem) {
        if !item.isToday {
            return
        }
        
        if item.signed {
            QMUITips.showInfo("您今天已经签到了", in: self.view)
            return
        }
        
        // sign
        self.dosign()
    }
    
    
    @objc
    private func rightTopAction() {
        if let titem = calendarView.model.data.first(where: {$0.isToday}), titem.signed {
            QMUITips.showInfo("您今天已经签到了", in: self.view)
            return
        }
         dosign()
    }

    //MARK: 获取签到数据
    private func loadSignRecordData() {
        model.loadSignRecordData { (flag, msg) in
            if !flag {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            let item = self.model.signRecord
            self.freshItemViews(item.newDay, item.integral, item.ruleText)
        }
    }
    
    private func freshItemViews(_ signDays: [String]? = nil, _ integral: String? = nil, _ rule: String? = nil) {
        if let sdays = signDays {
            self.calendarView.refreshCalendar(sdays)
            
            if let titem = calendarView.model.data.first(where: {$0.isToday}), titem.signed {
                // 如果今天已经签到 更新签到按钮状态
                self.freshSignButtonStatus(true)
            }
        }
        
        if let num = integral {
            let text = "积分：\(num)"
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes([.foregroundColor: Color.red, .font: UIFont.boldSystemFont(ofSize: 18)], range: NSRange(location: text.count - num.count, length: num.count))
            self.integralLabel.attributedText = attr
        }
        
        if let rule = rule {
            let title = "签到规则"
            let text2 = "\(title)\n\(rule)"
            let attr2 = NSMutableAttributedString(string: text2)
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 5.0
            attr2.addAttribute(.paragraphStyle, value: style, range: NSRange(location: 0, length: text2.count))
            
            attr2.addAttributes([.font : UIFont.systemFont(ofSize: 16), .foregroundColor: UIColor.darkGray], range: NSRange(location: 0, length: title.count))
            
            attr2.addAttributes([.font : UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.darkText], range: NSRange(location: title.count, length: rule.count))

            self.textView.attributedText = attr2
        }
    }
    
    private func freshSignButtonStatus(_ isSign: Bool) {
        let title = isSign ? "已签" : "签到"
        self.signButton.setTitle(title, for: .normal)
        self.signButton.isEnabled = isSign ? false : true
    }
    
    private func dosign() {
        model.dosign { (flag, msg) in
            if !flag {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            self.freshSignButtonStatus(true)
            
            let item = self.model.signItem
            if let money = Double(item.money), money > 0 {
//                QMUITips.showInfo("连续签到成功，奖励金额\(item.money)元", in: self.view)
                let layer = LoginGetMoneyLayerView()
                layer.money.text = "\(item.money)元"
                layer.tips.text = "签到红包"
                layer.show()
                
            } else if let num = Double(item.integral) {
                if let old_num = Double(self.model.signRecord.integral) {
                    self.model.signRecord.integral = "\(Int(old_num) + Int(num))"
                    self.freshItemViews(nil, self.model.signRecord.integral, nil)
                }
                
                QMUITips.showInfo("签到成功，奖励积分\(item.integral)", in: self.view)
            }
            
            if let index = self.calendarView.model.data.firstIndex(where: {$0.isToday}) {
                self.calendarView.model.data[index].signed = true
                self.calendarView.collectionView.reloadData()
            }
        }
    }
}

