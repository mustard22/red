//
//  FECenterMoneyListCell.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
/// 零钱列表cell
class FECenterMoneyListCell: YYTableViewCell {
    
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var typeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .left
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .right
        v.textColor = .gray
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var leftTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var midTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
}

extension FECenterMoneyListCell {
    public func update(_ item: MoneyListData.MoneyListItem) {
        typeLabel.text = "\(item.typeName)"
        
        midTop.text = "变动前\n\(item.before_balance)"
        rightTop.text = "变动后\n\(item.current_balance)"
        rightBottom.text = "\(item.time)"
        
        var text2 = "变动金额\n\(item.val)"
        if let before = Double(item.before_balance), let current = Double(item.current_balance) {
            if before < current {
                text2 = "+\(item.val)"
            } else if before > current {
                text2 = "-\(item.val)"
            } else {
                text2 = "\(item.val)"
            }
            
            let text = "变动金额\n\(text2)"
            let bvalue = before, cvalue = current
            let attr = NSMutableAttributedString(string: text)
            let value = cvalue - bvalue
            let color = (value > 0.0 ? Color.green : (value < 0.0 ? Color.red : UIColor.black))
            attr.addAttributes([.foregroundColor: color], range: (text as NSString).range(of: text2))
            leftTop.attributedText = attr
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        backview.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.top.equalTo(0)
            make.height.equalTo(30)
        }
        
        leftTop.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.top.equalTo(typeLabel.snp.bottom)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        midTop.snp.makeConstraints { (make) in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(leftTop.snp.top)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        rightTop.snp.makeConstraints { (make) in
            make.top.equalTo(leftTop.snp.top)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        rightBottom.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.bottom.equalTo(contentView.snp.bottom).offset(-5)
            make.size.equalTo(CGSize(width: 150, height: 20))
        }
    }
}


