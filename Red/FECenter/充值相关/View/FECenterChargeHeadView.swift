//
//  FECenterChargeHeadView.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

fileprivate var defaultItemSize: CGSize {
    get {
        return CGSize(width: 60 * kScreenScale, height: 80 * kScreenScale)
    }
}
fileprivate var defaultTopMargin: CGFloat {
    get {
        return 10 * kScreenScale
    }
}

class FECenterChargeHeadView: UIView {
    fileprivate var icon: String = ""
    fileprivate var title: String = ""
    fileprivate var icon2: String = ""
    fileprivate var title2: String = ""
    
    fileprivate lazy var leftItem: UIView = {
        let v = UIView()
        v.bounds = CGRect(x: 0, y: 0, width: self.width/2, height: self.height)
        let item = FECenterItemView.singleItem(defaultItemSize)
        var rect = item.0.frame
        rect.origin.x = (v.width - rect.size.width)/2
        rect.origin.y = (v.height - rect.size.height)/2
        item.0.frame = rect
        item.0.addTarget(self, action: #selector(handlerLeftButtonAction), for: .touchUpInside)
        v.addSubview(item.0)
        
        item.1.image = UIImage(named: icon)
        item.2.text = title
        self.addSubview(v)
        return v
    }()
    
    fileprivate lazy var rightItem: UIView = {
        let v = UIView()
        v.bounds = CGRect(x: 0, y: 0, width: self.width/2, height: self.height)
        let item = FECenterItemView.singleItem(defaultItemSize)
        var rect = item.0.frame
        rect.origin.x = (v.width - rect.size.width)/2
        rect.origin.y = (v.height - rect.size.height)/2
        item.0.frame = rect
        item.0.addTarget(self, action: #selector(handlerRightButtonAction), for: .touchUpInside)
        v.addSubview(item.0)
        
        item.1.image = UIImage(named: icon2)
        item.2.text = title2
        self.addSubview(v)
        return v
    }()
    
    fileprivate lazy var toplayer: CALayer = {
        let l = CALayer()
        l.frame = CGRect(x: 0, y: 0, width: leftItem.width, height: leftItem.height)
        l.backgroundColor = Color.red.cgColor
        l.opacity = 0.2
        return l
    }()
    
    fileprivate lazy var vlineLayer: CALayer = {
        let l = CALayer()
        l.frame = CGRect(x: self.width/2, y: 0, width: 1.0, height: leftItem.height)
        l.backgroundColor = UIColor.RGBColor(248, 248, 248).cgColor
        self.layer.addSublayer(l)
        return l
    }()
    
    fileprivate lazy var hlineLayer: CALayer = {
        let l = CALayer()
        l.frame = CGRect(x: 0, y: self.height-1, width: self.width, height: 1)
        l.backgroundColor = UIColor.RGBColor(248, 248, 248).cgColor
        self.layer.addSublayer(l)
        return l
    }()
    
    init(_ leftIcon: String, _ leftTitle: String, _ rightIcon: String, _ rightTitle: String, _ origin: CGPoint) {
        
        self.icon = leftIcon
        self.title = leftTitle
        self.icon2 = rightIcon
        self.title2 = rightTitle
        
        var rect = CGRect.zero
        rect.size = CGSize(width: kScreenSize.width, height: FECenterChargeHeadView.defaultHeight)
        rect.origin = origin
        super.init(frame: rect)
        
        self.backgroundColor = .white
        
        vlineLayer.isHidden = false
        hlineLayer.isHidden = false
        
        leftItem.left = 0
        leftItem.top = 0
        rightItem.right = self.right
        rightItem.top = 0
    }
    
    static var defaultHeight: CGFloat {
        return defaultItemSize.height + defaultTopMargin * 2
    }
    
    var clickHandler:((_ isLeft: Bool)->Void)?
    
    // 是否显示选中状态的标记（toplayer）
    var showSelectLayer: Bool = true
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension FECenterChargeHeadView {
    @objc func handlerLeftButtonAction() {
        if showSelectLayer {
            leftItem.layer.addSublayer(toplayer)
        }
        clickHandler?(true)
    }
    
    @objc func handlerRightButtonAction() {
        if showSelectLayer {
            rightItem.layer.addSublayer(toplayer)
        }
        clickHandler?(false)
    }
}


