//
//  FECenterItemView.swift
//  Red
//
//  Created by MAC on 2019/11/15.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

struct FECenterItemView {
    static func singleItem(_ size: CGSize) -> (UIButton, UIImageView, UILabel) {
        let b = UIButton(type: .custom)
        b.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        let img = UIImageView()
        img.frame = CGRect(x: 0, y: 0, width: size.width, height: size.width)
        img.isUserInteractionEnabled = false
        b.addSubview(img)
        
        let title = UILabel()
        title.textAlignment = .center
        title.font = UIFont.systemFont(ofSize: 14)
        title.textColor = .black
        title.frame = CGRect(x: 0, y: size.width, width: size.width, height: size.height-size.width)
        b.addSubview(title)
        return (b,img,title)
    }
}
