//
//  FECenterRedPackageListCell.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//


import UIKit
import QMUIKit

/// 红包记录列表cell
class FECenterRedPackageListCell: YYTableViewCell {
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.clipsToBounds = true
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var angleLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.boldSystemFont(ofSize: 12)
        v.textAlignment = .center
        v.text = "\n\n\n已领取"
        v.numberOfLines = 0
        v.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/4))
        v.textColor = .white
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var typeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .left
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .right
        v.textColor = .gray
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var leftTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var midTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
}

extension FECenterRedPackageListCell {
    //MARK: 设置发包cell
    private func setupViewsForSendPaper(_ m: SendPackageListData.SendPackageItem, _ isOwner: Bool = false) {
        typeLabel.text = m.room_name
        leftTop.text = "房间类型\n"
        midTop.text = "红包金额\n\(m.project_amount)"
        if let send = Double(m.project_amount), send > 0 {
            let text = "红包金额\n\(m.project_amount)"
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes([.foregroundColor: Color.red], range: (text as NSString).range(of: m.project_amount))
            midTop.attributedText = attr
        }
        rightTop.text = "红包名称\n\(m.project_amount)-\(m.packet_count)"
        rightBottom.text = m.created_at
        angleLabel.isHidden = false
        
        var text = ""
        var color: UIColor!
        switch m.status {
        case "1":
            text = "领取中"
            color = Color.green
        case "2":
            text = "已领完"
            color = Color.red
        default :
            text = "已过期"
            color = UIColor.gray
        }
        angleLabel.text = "\n\n\n\(text)"
        angleLabel.layer.backgroundColor = color.cgColor
        
        guard let room = RoomType.type(value: m.room_class, isOwner: isOwner) else {
            return
        }
        
        let typeName = room.name
        leftTop.text = "房间类型\n\(typeName)"
    }
    
    //MARK: 设置抢包cell
    private func setupViewsForFetchPaper(_ m: GetPackageListData.GetPackageItem, _ isOwner: Bool = false) {

        typeLabel.text = m.room_name
        leftTop.text = "房间类型\n"
        midTop.text = "红包金额\n\(m.project_amount)"
        rightTop.text = "抢包金额\n\(m.fetched_amount)"
        
        if let send = Double(m.project_amount), send > 0 {
            let text = "红包金额\n\(m.project_amount)"
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes([.foregroundColor: Color.red], range: (text as NSString).range(of: m.project_amount))
            midTop.attributedText = attr
        }
        
        if let earn = Double(m.fetched_amount), earn > 0 {
            let text = "抢包金额\n\(m.fetched_amount)"
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes([.foregroundColor: Color.green], range: (text as NSString).range(of: m.fetched_amount))
            rightTop.attributedText = attr
        }
        
        rightBottom.text = m.created_at
        
        var is_landmine = false
        if m.is_landmine == "1" || m.is_landmine == "true" {
            is_landmine = true
        }
        angleLabel.isHidden = is_landmine ? false : true
        
        guard let room = RoomType.type(value: m.room_class, isOwner: isOwner) else {
            return
        }
        
        let typeName = room.name
        leftTop.text = "房间类型\n\(typeName)"
        
        if !angleLabel.isHidden {
            var angleText = ""
            var bcolor = Color.red
            switch room {
            case .cattle:
                angleText = "赢"
                bcolor = Color.green
            case .singleThunder, .multipleThunder:
                angleText = "中雷"
            case .solitaire:
                angleText = "最小"
            default:
                angleLabel.isHidden = true
            }
            angleLabel.text = "\n\n\n\(angleText)"
            angleLabel.layer.backgroundColor = bcolor.cgColor
        }
    }
    
    public func update2(_ item: Any, _ isOwner: Bool = false) {
        if let m = item as? SendPackageListData.SendPackageItem {
            self.setupViewsForSendPaper(m, isOwner)
        }
        else if let m = item as? GetPackageListData.GetPackageItem {
            self.setupViewsForFetchPaper(m, isOwner)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        backview.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        
        angleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(backview.snp.right).offset(30)
            make.top.equalTo(-30)
            make.size.equalTo(CGSize(width: 60, height: 60))
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.top.equalTo(0)
            make.height.equalTo(30)
        }
        
        leftTop.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.top.equalTo(typeLabel.snp.bottom)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        midTop.snp.makeConstraints { (make) in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(leftTop.snp.top)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        rightTop.snp.makeConstraints { (make) in
            make.top.equalTo(leftTop.snp.top)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        rightBottom.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.bottom.equalTo(contentView.snp.bottom).offset(-5)
            make.size.equalTo(CGSize(width: 150, height: 20))
        }
    }
}


