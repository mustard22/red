//
//  FECenterOfflinePayView.swift
//  Red
//
//  Created by MAC on 2019/11/15.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FECenterOfflinePayView: UIView {

    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBAction func saveButtonAction(_ sender: Any) {
        self.saveHandler?(imageView.image)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.hide()
    }
    
    @IBAction func payButtonAction(_ sender: Any) {
        self.hide()
        self.payHandler?()
    }
    
    var payHandler: (()->Void)?
    var saveHandler: ((_ img: UIImage?)->Void)?
    
    func update(_ n: String, _ a: String, _ i: String) {
        nameLabel.text! += n
        accountLabel.text! += a
        imageView.setImage(with: URL(string: i))
    }
    
    func show() {
        let win = UIApplication.shared.keyWindow!
        self.frame = win.bounds
        self.backgroundColor = UIColor(white: 0, alpha: 0.3)
        win.addSubview(self)
    }
    
    func hide() {
        self.removeFromSuperview()
    }
}
