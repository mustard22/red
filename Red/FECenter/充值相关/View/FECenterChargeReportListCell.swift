//
//  FECenterChargeReportListCell.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//



import UIKit
import QMUIKit
/// 充值记录cell
class FECenterChargeReportListCell: YYTableViewCell {
    public enum ChargeReportListCellType {
        case charge, withdraw
    }
    
    public var type: ChargeReportListCellType = .charge
    
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.clipsToBounds = true
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var typeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .left
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var angleLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.boldSystemFont(ofSize: 12)
        v.textAlignment = .center
        v.text = "\n\n\n已领取"
        v.numberOfLines = 0
        v.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi/4))
        v.textColor = .white
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .right
        v.textColor = .gray
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var leftTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    
    lazy var rightTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
}

extension FECenterChargeReportListCell {
    public func update(_ m: ChargeListData.ChargeListItem) {
        typeLabel.text = "\(m.order_id)"
        leftTop.text = "充值金额\n\(m.amount)"
        rightTop.text = "到账金额\n\(m.real_amount)"
        rightBottom.text = "\(m.request_time)"
        
        angleLabel.isHidden = false
        angleLabel.text = "\n\n\n\(m.status == "0" ? "未处理":(m.status == "1" ? "已通过":"未通过"))"
        
        let color = (m.status == "0" ? Color.red : (m.status == "1" ? Color.green : UIColor.gray))
        angleLabel.layer.backgroundColor = color.cgColor
    }
    
    public func update2(_ m: WithdrawListData.WithdrawListItem) {
        angleLabel.isHidden = true
        typeLabel.text = m.order_id
        leftTop.text = "充值金额\n\(m.amount)"
        rightTop.text = "到账金额\n\(m.real_amount)"
        rightBottom.text = "\(m.request_time)"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        backview.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        
        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.top.equalTo(0)
            make.height.equalTo(30)
        }
        
        angleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(backview.snp.right).offset(30)
            make.top.equalTo(-30)
            make.size.equalTo(CGSize(width: 60, height: 60))
        }
        
        leftTop.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.top.equalTo(typeLabel.snp.bottom)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        
        rightTop.snp.makeConstraints { (make) in
            make.top.equalTo(leftTop.snp.top)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.size.equalTo(CGSize(width: 90, height: 50))
        }
        
        rightBottom.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.bottom.equalTo(contentView.snp.bottom).offset(-5)
            make.size.equalTo(CGSize(width: 150, height: 20))
        }
    }
}


