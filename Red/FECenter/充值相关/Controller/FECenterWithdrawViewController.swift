//
//  FECenterWithdrawViewController.swift
//  Red
//
//  Created by MAC on 2019/10/24.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 提现中心界面
class FECenterWithdrawViewController: FEBaseViewController {
    private var bankCardItem: BankCardItem?
    
    lazy var tableview: UITableView = {
        let v = UITableView()
        v.frame = view.bounds
        v.showsVerticalScrollIndicator = false
        v.separatorStyle = .none
        view.addSubview(v)
        return v
    }()
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.frame = CGRect(x: 0, y: 0, width: tableview.frame.size.width, height: tableview.frame.size.height)
        return v
    }()
    
    lazy var lefTopTips: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        v.text = "到账银行卡"
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var lefMidTips: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .left
        v.text = "提现金额"
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var inputTextField: QMUITextField = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "foot_more")
        imageView.size = CGSize(width: 20, height: 20)
        
        let v = QMUITextField("最低提现金额100元", font: UIFont.systemFont(ofSize: 14), color: Color.text)
        v.leftView = imageView
        v.keyboardType = .numberPad // 0-9
        v.leftViewMode = .always
        v.returnKeyType = .done
        v.textAlignment = .center
        v.qmui_borderPosition = .bottom
        v.qmui_borderColor = .darkGray
        v.delegate = self
        v.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightButton: QMUIButton = {
        let v = QMUIButton("添加银行卡", fontSize: 15)
        v.addTarget(self, action: #selector(handlerAddBankCard), for: .touchUpInside)
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var allWithdrawButton: QMUIButton = {
        let v = QMUIButton("全部提现", fontSize: 14)
        v.addTarget(self, action: #selector(handlerAllWithdraw), for: .touchUpInside)
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var submitButton: QMUIButton = {
        let v = QMUIButton("申请提现", fontSize: 18)
        v.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        v.setTitleColor(.white, for: .normal)
        v.addTarget(self, action: #selector(handlerSubmit), for: .touchUpInside)
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var moneyTips: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .left
        v.textColor = Color.text2
        v.text = "可提现金额：\(User.default.balance)元"
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var midBottomTips: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .center
        v.textColor = Color.text2
        v.text = "提示：每天最多提现五次"
        self.backview.addSubview(v)
        return v
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "提现中心"
        self.tableview.tableHeaderView = backview
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
         super.viewDidLayoutSubviews()
    }
    
    func setupUI() {
        lefTopTips.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 80, height: 40))
        }
        
        rightButton.snp.makeConstraints { (make) in
            make.left.equalTo(lefTopTips.snp.right).offset(20)
            make.top.equalTo(lefTopTips.snp.top)
            make.size.equalTo(CGSize(width: 120, height: 40))
        }
        
        lefMidTips.snp.makeConstraints { (make) in
            make.left.equalTo(lefTopTips.snp.left)
            make.top.equalTo(lefTopTips.snp.bottom).offset(10)
            make.size.equalTo(CGSize(width: 70, height: 20))
        }
        
        inputTextField.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(backview.snp.right).offset(-20)
            make.top.equalTo(lefMidTips.snp.bottom).offset(10)
            make.height.equalTo(40)
        }
        
        allWithdrawButton.snp.makeConstraints { (make) in
            make.right.equalTo(backview.snp.right).offset(-20)
            make.top.equalTo(inputTextField.snp.bottom).offset(10)
            make.height.equalTo(40)
            make.width.equalTo(60)
        }
        
        moneyTips.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(allWithdrawButton.snp.left).offset(-20)
            make.top.equalTo(allWithdrawButton.snp.top)
            make.height.equalTo(40)
        }
        
        submitButton.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(backview.snp.right).offset(-20)
            make.top.equalTo(allWithdrawButton.snp.bottom).offset(10)
            make.height.equalTo(40)
        }
        
        midBottomTips.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(backview.snp.right).offset(-20)
            make.top.equalTo(submitButton.snp.bottom).offset(10)
            make.height.equalTo(40)
        }
    }
}

//MARK: QMUITextFieldDelegate
extension FECenterWithdrawViewController: QMUITextFieldDelegate {
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
}


//MARK: Actions
extension FECenterWithdrawViewController {
    @objc func handlerAddBankCard() {
        let vc = FECenterBankListViewController()
        vc.selectBankCardHandler = {[weak self](item) in
            guard let self = self else {
                return
            }
            self.bankCardItem = item
            self.rightButton.setTitle(item.bank_name, for: .normal)
            self.rightButton.setTitleColor(.black, for: .normal)
        }
        self.push(to: vc)
    }
    
    // 提交
    @objc func handlerSubmit() {
        guard let _ = self.bankCardItem else {
            QMUITips.show(withText: "请选择银行卡", in: self.view)
            return
        }
        
        let input = self.inputTextField.text!
        guard input.count>0 else {
            QMUITips.show(withText: "请输入提现金额", in: self.view)
            return
        }
        
        guard Double(input)! <= Double(User.default.balance)! else {
            QMUITips.show(withText: "提现金额不能大于当前余额", in: self.view)
            return
        }

        guard Double(input)! >= 100.0 else {
            QMUITips.show(withText: "至少提现100元", in: self.view)
            return
        }
        
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        self.checkPasswd {[weak self] (pwd) in
            guard let self = self else {
                return
            }
            var param = UserWithdrawParam()
            param.amount = self.inputTextField.text!
            param.card_id = "\(self.bankCardItem!.id)"
            param.fund_password = pwd
            
            let load = QMUITips.showLoading("提交中..." ,in: self.view)
            HttpClient.default.userWithdraw(param, { (status, msg) in
                load.hide(animated: true)
                QMUITips.show(withText: msg, in: self.view)
                if status {
                    self.close()
                }
            })
        }
    }
    
    
    func checkPasswd(_ handler: @escaping ((_ pwd: String)->Void)) {
        // 先验证资金密码
        FEVerify.verifyFundPasswd(self) { (status, pwd, text) in
            if !status {
                QMUITips.show(withText: text, in: self.view)
                return
            }
            
            handler(pwd ?? "")
        }
    }
    
    @objc func handlerAllWithdraw() {
        self.inputTextField.text = "\(Int(Double(User.default.balance)!))"
    }
}
