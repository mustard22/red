//
//  FECenterChargeMoneyViewController.swift
//  Red
//
//  Created by MAC on 2019/10/24.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

private let backColor = UIColor(red: 212/255.0, green: 212/255.0, blue: 212/255.0, alpha: 1.0)

/// 充值界面
class FECenterChargeMoneyViewController: FEBaseViewController {
    private let text1 = "温馨提示（线下支付请填写账户信息到备注栏）"
    private let text2 = " \n为确保你的款项及时到账，请留意以下内容：\n\n1.若在存款中提示二维码已过期或者存入金额已满，请返回重新发起支付。\n\n2.站点每日都会产生大量存款订单，为确保每一位玩家的存款能够及时到账，请填写实际存款金额，以便于平台快速处理您的业务。\n\n3.支付时，请务必按照页面提示的金额进行支付，否则可能将导致订单延误或掉单。"
    
    private let leftMargin = 80
    private var leftListData:[PayClassItem.PayTypeItem]?
    
    private var datas: [PayClassItem]?
    /// 当前选中
    private var topSelectIndex = -1
    /// 左侧table选中index
    private var leftSelectIndex = -1
    
    private var customers: [FriendItem] = []
    
    /// 显示充值金额（固定6个）
    private var moneyButtons = [QMUIButton]()
    
    lazy var backtableview: UITableView = {
        let v = UITableView()
        v.frame = view.bounds
        v.showsVerticalScrollIndicator = false
        v.separatorStyle = .none
        v.delegate = self
        view.addSubview(v)
        return v
    }()
    
    lazy var customerListView: UIView = {
        let v = UIView()
        v.backgroundColor = backColor
        v.frame = CGRect(x: 0, y: kNaviBarHeight+topbarView.bottom+1, width: self.view.width, height: view.bottom - (kNaviBarHeight+topbarView.bottom+1))
        v.isHidden = true
        view.addSubview(v)
        return v
    }()
    
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = backColor
        v.frame = CGRect(x: 0, y: 0, width: backtableview.frame.size.width, height: backtableview.frame.size.height)
        return v
    }()
    
    
    lazy var moneyButtonBack: ChargeCenterMoneyBackView = {
        let v = ChargeCenterMoneyBackView()
        v.backgroundColor = .white
        v.frame = CGRect(x: leftTable.right, y: leftTable.top, width: backview.width - leftTable.right, height: 200)
        backview.addSubview(v)
        return v
    }()
    
    lazy var payButtonBack: ChargeCenterPayBackView = {
        let v = ChargeCenterPayBackView()
        v.frame = CGRect(x: leftTable.right, y: moneyButtonBack.bottom+1, width: moneyButtonBack.width , height: 174)
        v.backgroundColor = .white
        backview.addSubview(v)
        v.buttonActionHandler = {[weak self] (btn) in
            self?.dowithUserCharge()
        }
        return v
    }()
    
    lazy var textView: QMUITextView = {
        let v = QMUITextView()
        v.frame = CGRect(x: 0, y: payButtonBack.bottom+10, width: backview.width , height: backview.height-(payButtonBack.bottom+10))
        v.backgroundColor = .white
        v.isEditable = false
        v.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        backview.addSubview(v)
        
        var attr = NSMutableAttributedString(string: "\(text1)\n\(text2)")
        attr.addAttributes([.font : UIFont.systemFont(ofSize: 16), .foregroundColor: UIColor.black], range: NSRange(location: 0, length: text1.count))
        attr.addAttributes([.font : UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.text2], range: NSRange(location: text1.count, length: text2.count))
        v.attributedText = attr
        return v
    }()
    
    
    lazy var topbarView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.frame = CGRect(x: 0, y: 0, width: backview.width, height: 70)
        backview.addSubview(v)
        return v
    }()
    // 客服按钮
    lazy var customerButton: QMUIButton = {
        let v = QMUIButton()
        v.backgroundColor = Color.golden
        v.frame = CGRect(x: 0, y: 5, width: 80, height: 40)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.addTarget(self, action: #selector(handlerCustomerAction), for: .touchUpInside)
        topbarScroll.addSubview(v)
        
        let icon = UIImageView()
        icon.image = UIImage(named: "ic_tab_custom")
        icon.frame = CGRect(x: 0, y: 2.5, width: 35, height: 35)
        v.addSubview(icon)
        
        let lab = QMUILabel(frame: CGRect(x: 30, y: 0, width: 45, height: 40))
        lab.text = "充值客服"
        lab.textColor = .white
        lab.font = UIFont.systemFont(ofSize: 13)
        lab.textAlignment = .center
        lab.numberOfLines = 0
        v.addSubview(lab)
        
        return v
    }()
    
    let itemBtnSize: CGFloat = 50
    
    lazy var topbarScroll: UIScrollView = {
        let v = UIScrollView()
        v.frame = topbarView.bounds
        v.alwaysBounceHorizontal = true
        topbarView.addSubview(v)
        return v
    }()
    
    lazy var redline: UIView = {
        let v = UIView()
        v.backgroundColor = Color.red
        topbarView.addSubview(v)
        return v
    }()
    
    lazy var leftTable: UITableView = {
        let v = UITableView()
        v.frame = CGRect(x: 0, y: topbarView.bottom+1, width: 100, height: 200)
        v.backgroundColor = backColor
        v.showsVerticalScrollIndicator = false
//        v.separatorStyle = .none
        v.delegate = self
        v.dataSource = self
        v.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        return v
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "充值"
        self.backtableview.tableHeaderView = backview
        setupUI()
        
        getPayClassData()
        
        // 键盘
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


extension FECenterChargeMoneyViewController {
    //MARK: 获取支付分类数据
    func getPayClassData() {
        HttpClient.default.getPayClass {[weak self] (status, items) in
            guard let self = self, status else {
                return
            }
            self.datas = items
            self.refreshUI()
        }
    }
    
    func refreshUI() {
        let btnSize: CGFloat = self.itemBtnSize
        // 计算间距
        var hmargin: CGFloat = (self.view.width - (CGFloat(datas!.count) * btnSize + customerButton.width))/CGFloat(datas!.count+1+1)
        hmargin = CGFloat(Double.minimum(50.0, Double(hmargin)))
        
        customerButton.left = hmargin

        // topbarscroll
//        topbarScroll.contentSize = CGSize(width: btnSize*CGFloat(self.datas!.count), height: topbarScroll.height)
        for i in 0..<self.datas!.count {
            let btn = QMUIButton()
            let x = customerButton.right + hmargin + CGFloat(i)*(btnSize+hmargin)
            btn.frame = CGRect(x: x, y: 0, width: btnSize, height: btnSize)
            btn.tag = i
            btn.addTarget(self, action: #selector(payClassButtonAction(_:)), for: .touchUpInside)
            
            let imgv = UIImageView()
            imgv.setImage(with: URL(string: self.datas![i].img))
            imgv.frame = btn.bounds
            btn.addSubview(imgv)
            topbarScroll.addSubview(btn)
            
            let lab = UILabel()
            lab.text = "\(self.datas![i].title)"
            lab.font = UIFont.systemFont(ofSize: 12)
            lab.textAlignment = .center
            lab.size = CGSize(width: 80, height: 18)
            lab.centerX = btn.centerX
            lab.bottom = topbarScroll.bottom
            topbarScroll.addSubview(lab)
            
            // 默认选中第一个
            if i == 0 {
                redline.frame = CGRect(x: customerButton.right + hmargin, y: topbarScroll.height-1.0, width: topbarScroll.height, height: 2.0)
                self.payClassButtonAction(btn)
            }
        }
        
        let lab = UILabel()
        lab.text = "客服充值"
        lab.font = UIFont.systemFont(ofSize: 12)
        lab.textAlignment = .center
        lab.size = CGSize(width: 80, height: 18)
        lab.centerX = customerButton.centerX
        lab.bottom = topbarScroll.bottom
        topbarScroll.addSubview(lab)
    }
    
    func setupUI() {
        backview.addSubview(topbarView)
        backview.addSubview(leftTable)
        backview.addSubview(moneyButtonBack)
        backview.addSubview(payButtonBack)
        backview.addSubview(textView)
        
        leftTable.bottom = textView.top
        leftTable.top = topbarView.bottom+1
        
        topbarView.addSubview(topbarScroll)
    }
}


//MARK: 事件处理
extension FECenterChargeMoneyViewController {
    //MARK: 点击充值客服
    @objc func handlerCustomerAction() {
//        ContactCustomer.startContact(currentvc: self, enterChat: true)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.redline.centerX = self.customerButton.centerX
            self.customerListView.isHidden = false
        }) { (fin) in
            self.getCustomerData()
        }
    }
    
    //MARK: 客服
    func refreshCustomerListView() {
        customerListView.removeAllSubviews()
        
        let vmargin: CGFloat = 10.0
        let itemHeight: CGFloat = 70
        for (i, c) in customers.enumerated() {
            let v = UIView()
            v.backgroundColor = .white
            v.frame = CGRect(x: 0, y: vmargin + CGFloat(i)*(vmargin+itemHeight), width: customerListView.width, height: itemHeight)
            self.customerListView.addSubview(v)
            
            let imgv = UIImageView()
            imgv.setImage(with: URL(string: c.friend_avatar))
            imgv.frame = CGRect(x: vmargin, y: vmargin, width: itemHeight-vmargin*2, height: itemHeight-vmargin*2)
            imgv.backgroundColor = UIColor.RGBColor(240, 240, 240)
            v.addSubview(imgv)
            
            let name = UILabel()
            name.textAlignment = .left
            name.font = UIFont.systemFont(ofSize: 15)
            name.textColor = .black
            name.text = c.friend_nickname
            name.frame = CGRect(x: imgv.right+vmargin, y: imgv.top, width: 150, height: imgv.height)
            v.addSubview(name)
            
            let btn = UIButton(type: .custom)
            btn.tag = i
            btn.setTitle("立即联系", for: .normal)
            btn.setTitleColor(.white, for: .normal)
            btn.backgroundColor = Color.red
            btn.titleLabel!.font = UIFont.systemFont(ofSize: 15)
            btn.addTarget(self, action: #selector(selectCustomer(_:)), for: .touchUpInside)
            btn.frame = CGRect(x: v.width - 20 - 100, y: (itemHeight-40)/2, width: 100, height: 40)
            v.addSubview(btn)
        }
    }
    
    @objc func selectCustomer(_ btn: UIButton) {
        let item = customers[btn.tag]
        let vc = FEChatFriendViewController(chat_id: item.friend_id, chat_type: .customer)
        vc.friend = item
        self.push(to: vc)
    }
    
    func getCustomerData() {
        if self.customers.count == 2 {
            self.refreshCustomerListView()
            return
        }
        
        var p = FriendListParam()
        p.pageSize = "5"
        p.pageIndex = "1"
        HttpClient.default.getFriendList(param: p, showCache: true) {[weak self] (status, msg, data) in
            guard let items = data.data else {
                return
            }
            
            for item in items {
                // 选在线客服
                if item.friend_type != "3" {
                    continue
                }
                self?.customers.append(item)
                if self?.customers.count == 2 {
                    break
                }
            }
            
            self?.refreshCustomerListView()
        }
    }
    
    // 选择支付类
    @objc func payClassButtonAction(_ btn: QMUIButton) {
        customerListView.isHidden = true
        
        topSelectIndex = btn.tag
        leftSelectIndex = 0
        
        UIView.animate(withDuration: 0.2) {[weak self] in
            self?.redline.centerX = btn.centerX
        }
        
        let item = self.datas![btn.tag]
        leftListData = item.pay_type
        leftTable.reloadData()
        
        self.freshRightMoneyButtons()
    }
    
    // 刷新右边的6个按钮显示
    func freshRightMoneyButtons() {
        guard let datas = self.datas else {
            return
        }
        
        let payTypeItem = datas[topSelectIndex]
        if payTypeItem.pay_type.count <= leftSelectIndex {
            moneyButtonBack.isHidden = true
            payButtonBack.isHidden = true
            return
        }
        
        let item = payTypeItem.pay_type[leftSelectIndex]
        guard let min = Double(item.min_money) else {
            return
        }
        guard let max = Double(item.max_money) else {
            return
        }
        
        moneyButtonBack.isHidden = false
        payButtonBack.isHidden = false
        
        let margin = Int((max - min) / 5)
        var titles = [String]()
        for i in 0..<6 {
            let value = Int(min) + i*margin
            titles.append("\(value)")
        }
        self.moneyButtonBack.refeshViews(titles)
        
        payButtonBack.tips2.text = item.notice
        payButtonBack.tips.text = "单笔充值限额(元):\(min)~\(max)"
        // 线上支付不需要输入‘账号信息’
        payButtonBack.input.isHidden = item.pay_type == "1" ? true : false
    }
    
    //MARK: 点击支付按钮
    func dowithUserCharge() {
        self.view.endEditing(true)
        
        guard let input = moneyButtonBack.input.text, !input.isEmpty else {
            QMUITips.show(withText: "请输入金额", in: self.view)
            return
        }
        
        // 判断输入金额
        let item = datas![topSelectIndex].pay_type[leftSelectIndex]
        if let inputMoney = Double(input),
            let minMoney = Double(item.min_money),
            let maxMoney = Double(item.max_money)
        {
            if inputMoney < minMoney {
                QMUITips.show(withText: "至少充值\(item.min_money)元", in: self.view)
                return
            }
            
            if inputMoney > maxMoney {
                QMUITips.show(withText: "最多充值\(item.max_money)元", in: self.view)
                return
            }
        }
        
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        // 判断线下 线上
        let typeitem = datas![topSelectIndex].pay_type[leftSelectIndex]
        if typeitem.pay_type == "1" {
            // 线上
            var param = UserChargeParam()
            param.amount = input
            param.pay_class = "\(typeitem.pay_class)"
            param.pay_id = "\(typeitem.id)"
            param.remark = payButtonBack.input.text ?? ""
            self.payAction(param, false)
        }
        else if typeitem.pay_type == "2" {
            guard let remark = payButtonBack.input.text, remark.count>0 else {
                QMUITips.show(withText: "请输入账号信息", in: self.view)
                return
            }
            
            // 线下
            let view = Bundle.main.loadNibNamed("FECenterOfflinePayView", owner: self, options: nil)?.first as! FECenterOfflinePayView
            view.update(typeitem.account_name, typeitem.account, typeitem.pay_img)
            view.show()
            view.payHandler = {[weak self] in
                var param = UserChargeParam()
                param.amount = input
                param.pay_class = "\(typeitem.pay_class)"
                param.pay_id = "\(typeitem.id)"
                param.remark = remark
                self?.payAction(param, true)
            }
            view.saveHandler = {[weak self](img) in
                self?.saveImageToAlbum(img)
            }
        }
    }
}

extension FECenterChargeMoneyViewController {
    //MARK: 保存图片
    func saveImageToAlbum(_ img: UIImage?) {
        guard let img = img else {
            QMUITips.show(withText: "保存失败", in: self.view)
            return
        }
        
        PHPhotoLibrary.requestAuthorization { (status) in
            if (status == .notDetermined || status == .authorized) {
            UIImageWriteToSavedPhotosAlbum(img, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
            }
            else {
                DispatchQueue.main.async {
                    let ac = QMUIAlertController(title: "相册授权未开启", message: "请在系统设置中开启相册授权", preferredStyle: .alert)
                    let action = QMUIAlertAction(title: "知道了", style: .default, handler: nil)
                    ac.addAction(action)
                    ac.showWith(animated: true)
                }
            }
        }
    }
    
    
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if let e = error as NSError? {
            QMUITips.show(withText: "系统问题，保存失败")
            dprint(e)
        } else {
            QMUITips.show(withText: "保存成功")
        }
    }
    
    //MARK: 调支付接口
    func payAction(_ param: UserChargeParam, _ isOffline: Bool) {
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.userCharge(param) {[weak self] (status, msg, data) in
            load.hide(animated: true)
            guard let self = self else {
                return
            }
            if !status {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            
            if isOffline {
                // 线下
                QMUITips.show(withText: msg, in: self.view)
                self.close()
            } else {
                // 在线充值
                let vc = FEBaseWebViewController()
                vc.title = "在线充值"
                vc.url = data!.request_url
                self.push(to: vc)
            }
        }
    }
}

//MARK: 键盘通知
extension FECenterChargeMoneyViewController {
    @objc fileprivate func keyboardWillShow(_ not:NSNotification) {
        guard let info = not.userInfo as NSDictionary?,
            let value = info.object(forKey: "UIKeyboardFrameEndUserInfoKey") as! NSValue? else {
            return
        }
        
        let keyboardRect = value.cgRectValue
        let rect = payButtonBack.button.convert(payButtonBack.button.bounds, to: self.view)
        let offset = rect.origin.y + rect.size.height - (self.view.bottom - keyboardRect.size.height)
        if offset > 0 {
            self.backtableview.setContentOffset(CGPoint(x: 0, y: offset), animated: true)
        }
    }
    
    @objc fileprivate func keyboardWillHide(_ not:NSNotification) {
        self.backtableview.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.payButtonBack.input.resignFirstResponder()
    }
}

//MARK: 左侧列表代理
extension FECenterChargeMoneyViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let ld = leftListData {
            return ld.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        cell.textLabel!.text = leftListData![indexPath.row].name
        cell.textLabel!.font = UIFont.systemFont(ofSize: 14)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if leftSelectIndex == indexPath.row {
            return
        }
        
        leftSelectIndex = indexPath.row
        self.freshRightMoneyButtons()
    }
}


//MARK: ---------- 支付按钮view --------------
class ChargeCenterPayBackView: UIView, QMUITextFieldDelegate {
    // 点击支付按钮响应
    public var buttonActionHandler: ((_ btn: QMUIButton)->Void)?
    
    /// 显示充值金额范围（eg: 0~100）
    lazy var tips: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        v.text = ""
        self.addSubview(v)
        return v
    }()
    
    /// 提示线上线下支付
    lazy var tips2: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        v.text = ""
        self.addSubview(v)
        return v
    }()
    
    lazy var input: QMUITextField = {
        let v = QMUITextField()
        v.placeholder = "账号信息"
        v.keyboardType = .default
        v.returnKeyType = .done
        v.textAlignment = .left
        v.autocapitalizationType = .none
        v.font = UIFont.systemFont(ofSize: 15)
        v.qmui_borderPosition = .bottom
        v.tintColor = Color.red
        v.delegate = self
        self.addSubview(v)
        return v
    }()
    
    lazy var button: QMUIButton = {
        let v = QMUIButton()
        v.setTitle("支付", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.titleLabel!.font = UIFont.systemFont(ofSize: 18)
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        tips.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(self).offset(-20)
            make.top.equalTo(0)
            make.height.equalTo(40)
        }
        tips2.snp.makeConstraints { (make) in
            make.left.equalTo(tips)
            make.right.equalTo(tips)
            make.top.equalTo(tips.snp.bottom)
            make.height.equalTo(tips)
        }
        input.snp.makeConstraints { (make) in
            make.left.equalTo(tips)
            make.top.equalTo(tips2.snp.bottom)
            make.size.equalTo(tips)
        }
        button.snp.makeConstraints { (make) in
            make.left.equalTo(tips)
            make.right.equalTo(tips)
            make.bottom.equalTo(self.snp.bottom).offset(-10)
            make.height.equalTo(44)
        }
    }
    
    @objc func buttonAction(_ btn: QMUIButton) {
        if let handler = buttonActionHandler {
            handler(btn)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.input.resignFirstResponder()
        return true
    }
}


//MARK: -------------选择金额按钮view（6个按钮）------------------
class ChargeCenterMoneyBackView: UIView {
    private let selectColor = UIColor(red: 65/255.0, green: 142/255.0, blue: 247/255.0, alpha: 1.0)
    private let buttonCount = 6
    private let buttonSize = CGSize(width: 60, height: 30)
    lazy var buttons: [QMUIButton] = {
        var btns = [QMUIButton]()
        
        for i in 0..<buttonCount {
            let b = QMUIButton()
            b.tag = i
            b.setTitle("", for: .normal)
            b.setTitleColor(.black, for: .normal)
            b.titleLabel!.font = UIFont.systemFont(ofSize: 13)
            b.layer.cornerRadius = 5.0
            b.bounds = CGRect(x: 0, y: 0, width: buttonSize.width, height: buttonSize.height)
            b.backgroundColor = backColor
            b.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
            self.addSubview(b)
            btns.append(b)
        }
        
        return btns
    }()
    
    lazy var tips: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        v.text = "充值金额(元)"
        v.bounds = CGRect(x: 0, y: 0, width: 100, height: 30)
        self.addSubview(v)
        return v
    }()
    
    lazy var input: QMUITextField = {
        let v = QMUITextField()
        v.placeholder = "输入充值金额"
        v.keyboardType = .numberPad // 0-9
        v.returnKeyType = .done
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 15)
        v.qmui_borderPosition = .bottom
        v.qmui_borderColor = backColor
        v.tintColor = Color.red
        self.addSubview(v)
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let margin = (self.frame.size.width-CGFloat(buttonSize.width*3)) / 4
        for btn in buttons {
            btn.left = margin + CGFloat(btn.tag%3)*(btn.width+margin)
            btn.top = margin + CGFloat(btn.tag/3)*(btn.height+margin)
        }
        tips.left = margin
        tips.top = margin+CGFloat(buttonCount/3) * (margin + buttonSize.height)
        tips.height = 40
        input.frame = CGRect(x: tips.right+5, y: tips.top, width: 120, height: 40)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // 刷新
    func refeshViews(_ titles: [String]) {
        if titles.count != buttons.count {
            return
        }
        
        for i in 0..<buttons.count {
            let btn = buttons[i]
            btn.setTitle(titles[i], for: .normal)
            btn.backgroundColor = backColor
            btn.isSelected = false
        }
        // 默认选中第一个
        self.buttonAction(buttons[0])
    }
    
    @objc func buttonAction(_ btn: QMUIButton) {
        if btn.isSelected {
            return
        }
        
        for b in buttons {
            if b.isSelected {
                b.isSelected = false
                b.backgroundColor = backColor
                break
            }
        }
        
        btn.isSelected = true
        btn.backgroundColor = selectColor
        
        self.input.text = btn.currentTitle
    }
}
