//
//  FECenterRedPackageViewController.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//


import UIKit
import QMUIKit

//MARK: 红包记录
class FECenterRedPackageViewController: YYTableViewController {
    //MARK: var subviews
    private lazy var topbarView: UIView = {
        let v = UIView()
        self.view.addSubview(v)
        return v
    }()
    
    private lazy var topLeftBtn: QMUIButton = {
        let v = QMUIButton()
        v.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.setTitle("", for: .normal)
        v.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.topbarView.addSubview(v)
        return v
    }()
    
    private lazy var topRightBtn: QMUIButton = {
        let v = QMUIButton()
        v.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.setTitle("", for: .normal)
        v.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.topbarView.addSubview(v)
        return v
    }()
    
    private lazy var topvline: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        topbarView.addSubview(v)
        return v
    }()
    
    private lazy var sepline: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        topbarView.addSubview(v)
        return v
    }()
    
    private lazy var timeView: FECenterProxyTimeHeadView = {
        let v = FECenterProxyTimeHeadView(frame: CGRect(x: 0, y: topbarView.bottom, width: self.tableView.frame.width, height: topbarView.height))
        self.view.addSubview(v)
        v.clickHandler = {[weak self] (btn) in
            guard let self = self else {
                return
            }
            if btn == v.leftBtn {
                // start
                self.handlerStartTime(btn)
            } else {
                // end time
                self.handlerEndTime(btn)
            }
        }
        return v
    }()
    
    //MARK: model
    private var model = FECenterPaperRecordModel()
    
    //MARK: override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()

        self.topLeftBtn.setTitle(model.selectType.name, for: .normal)
        
        self.topRightBtn.isUserInteractionEnabled = HomePageModel.showOwnerGroup ? true : false // 不显示自建群时 按钮不可点击
        self.topRightBtn.setTitle(model.selectGroup.name, for: .normal)
        
        self.loadListData()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: FECenterRedPackageListCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FECenterRedPackageViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

//MARK: - set UI
extension FECenterRedPackageViewController {
    private func setupUI() {
        self.title = "红包记录"
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        topbarView.snp.makeConstraints { (make) in
            make.top.equalTo(kNaviBarHeight)
            make.left.equalTo(0)
            make.right.equalTo(self.view.snp.right)
            make.height.equalTo(40)
        }
        topLeftBtn.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(topbarView.snp.centerX)
            make.height.equalTo(topbarView)
        }
        topRightBtn.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(topbarView.snp.centerX)
            make.right.equalTo(topbarView)
            make.height.equalTo(topbarView)
        }
        topvline.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.width.equalTo(1)
            make.centerX.equalTo(topbarView)
            make.height.equalTo(topbarView)
        }
        sepline.snp.makeConstraints { (make) in
            make.top.equalTo(topbarView.bottom).offset(-1)
            make.left.equalTo(0)
            make.right.equalTo(self.view.snp.right)
            make.height.equalTo(1)
        }
        timeView.snp.makeConstraints { (make) in
            make.top.equalTo(topbarView.snp.bottom)
            make.left.equalTo(0)
            make.right.equalTo(self.view.snp.right)
            make.height.equalTo(90)
        }
    }
}

//MARK: - 点击事件
extension FECenterRedPackageViewController {
    //MARK: 选择开始时间
    private func handlerStartTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            if !hasChanged {
                return
            }
            
            self?.timeView.freshStartTime(time)
            self?.mj_head.beginRefreshing()
        }
        d.datePicker.maximumDate = Date().offsetOfDay(-1)
        d.show()
    }
    
    //MARK: 选择结束时间
    private func handlerEndTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            if !hasChanged {
                return
            }
            self?.timeView.freshEndTime(time)
            self?.mj_head.beginRefreshing()
        }
        d.show()
    }
    
    @objc
    private func handlerButtonAction(_ sender: UIButton) {
        //MARK: 点击记录类型
        if sender == topLeftBtn {
            let types = FECenterPaperRecordModel.RecordType.allCases
            let lastIndex = self.model.selectType.index
            let l =  FECenterLayerTableView.instance(types.map({$0.name}), lastIndex) {[weak self] (index) in
                if lastIndex == index {
                    return
                }
                self?.model.selectType = types[index]
                self?.topLeftBtn.setTitle(self?.model.selectType.name, for: .normal)
                self?.mj_head.beginRefreshing()
            }
            l.show()
        } else {
            //MARK: 点击群组类型
            let types = FECenterPaperRecordModel.GroupType.allCases
            let lastIndex = self.model.selectGroup.index
            let l =  FECenterLayerTableView.instance(types.map({$0.name}), self.model.selectGroup.index) {[weak self] (index) in
                if lastIndex == index {
                    return
                }
                self?.model.selectGroup = types[index]
                self?.topRightBtn.setTitle(self?.model.selectGroup.name, for: .normal)
                self?.mj_head.beginRefreshing()
            }
            l.show()
        }
    }
}

//MARK: - request datas
extension FECenterRedPackageViewController {
    @objc
    private func loadListData() {
        self.model.loadListData(from: self.timeView.startTime, to: self.timeView.endTime) {
            self.mj_head.endRefreshing()
            if self.model.showMore {
                self.tableView.mj_footer = self.mj_foot
            } else {
                self.tableView.mj_footer = nil
            }
            self.tableView.reloadData()
        }
    }

    @objc
    private func loadMoreListData() {
        self.model.loadMoreListData(from: self.timeView.startTime, to: self.timeView.endTime) {
            self.mj_foot.endRefreshing()
            if self.model.showMore {
                self.mj_foot.state = .idle
            } else {
                self.tableView.mj_footer = nil
            }
            self.tableView.reloadData()
        }
    }
}


//MARK: - set table
extension FECenterRedPackageViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FECenterRedPackageListCell.self)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreListData))
        self.tableView.mj_footer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = CGRect(x: 0, y: timeView.bottom, width: view.frame.size.width, height: view.frame.size.height - timeView.bottom - (kIsIphoneX ? 20 : 0))
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.model.listCount
        self.nodataView.text = count == 0 ? "暂无红包记录" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FECenterRedPackageListCell.self)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell0 = cell as! FECenterRedPackageListCell
        
        if let item = self.model.cellItem(with: indexPath.row) {
            cell0.update2(item, self.model.isOwner)
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
