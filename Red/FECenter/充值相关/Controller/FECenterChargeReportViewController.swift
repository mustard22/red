//
//  FECenterChargeReportViewController.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//


import UIKit
import QMUIKit

/// 充提记录
class FECenterChargeReportViewController: YYTableViewController {
    var datas = [ChargeListData.ChargeListItem]()
    var isFirstLoad = true
    var listData: ChargeListData?
    
    var datas2 = [WithdrawListData.WithdrawListItem]()
    var isFirstLoad2 = true
    var listData2: WithdrawListData?
    
    /// 0充值 提现1
    public var selectType = 0
    
    lazy var headview: FECenterChargeHeadView = {
        
        let v = FECenterChargeHeadView("icon_send_record", "充值记录", "icon_grab_record", "提现记录", CGPoint(x: 0, y: kNaviBarHeight))
        v.clickHandler = {[weak self](isLeft) in
            guard let self = self else {
                return
            }
            if isLeft {
                // 充值记录
                self.selectType = 0
            } else {
                // 提现记录
                self.selectType = 1
            }
            self.refreshListDataAfterButtonClicked()
        }
        self.view.addSubview(v)
        return v
    }()
    
    lazy var timeView: FECenterProxyTimeHeadView = {
        let v = FECenterProxyTimeHeadView(frame: CGRect(x: 0, y: headview.bottom, width: self.tableView.frame.width, height: headview.height))
        self.view.addSubview(v)
        v.clickHandler = {[weak self] (btn) in
            guard let self = self else {
                return
            }
            if btn == v.leftBtn {
                // start
                self.handlerStartTime(btn)
            } else {
                // end time
                self.handlerEndTime(btn)
            }
        }
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "充提记录"
        
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        if selectType == 0 {
            headview.handlerLeftButtonAction()
        } else {
            headview.handlerRightButtonAction()
        }
        
        headview.snp.remakeConstraints { (make) in
            make.top.equalTo(kNaviBarHeight)
            make.left.equalTo(0)
            make.size.equalTo(headview.size)
        }
        timeView.snp.remakeConstraints { (make) in
            make.top.equalTo(headview.bottom)
            make.left.equalTo(0)
            make.size.equalTo(headview.size)
        }
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        tableView.backgroundColor = .lightText
        tableView.registerCell(withClass: FECenterChargeReportListCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FECenterChargeReportViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

//MARK: 数据加载处理
extension FECenterChargeReportViewController {
    func refreshListDataAfterButtonClicked() {
        if !isFirstLoad, !isFirstLoad2 {
            self.tableView.reloadData()
            return
        }
        
        loadListData()
    }
    
    func handlerStartTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            guard let self = self else {
                return
            }
            
            if hasChanged {
                self.timeView.freshStartTime(time)
                self.mj_head.beginRefreshing()
            }
        }
        d.datePicker.maximumDate = Date().offsetOfDay(-1)
        d.show()
    }
    
    func handlerEndTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            guard let self = self else {
                return
            }
            
            if hasChanged {
                self.timeView.freshEndTime(time)
                self.mj_head.beginRefreshing()
            }
        }
        d.show()
    }
    
    @objc func loadListData() {
        if selectType == 0 {
            loadChargeReportData()
        } else {
            loadWithdrawData()
        }
    }
    
    @objc func loadMoreListData() {
        if selectType == 0 {
            loadMoreChargeReportData()
        } else {
            loadMoreWithdrawData()
        }
    }
    
    // 充值
    func loadChargeReportData() {
        // load list
        var param = ChargeListParam()
        param.start_time = self.timeView.startTime
        param.end_time = self.timeView.endTime
        param.pageIndex = "1"
        param.pageSize = "15"
        
        HttpClient.default.getChargeListData(param, { (status, data) in
            self.mj_head.endRefreshing()
            self.isFirstLoad = false
            if !status {
                return
            }
            self.listData = data
            self.datas.removeAll()
            self.datas.append(contentsOf: self.listData!.data)
            self.tableView.reloadData()
            
            if self.datas.count == 0 {
                self.tableView.mj_footer = nil
            } else {
                if self.datas.count >= 15 {
                    self.tableView.mj_footer = self.mj_foot
                }
            }
        })
    }
    
    // 提现
    func loadWithdrawData() {
        var param = WithdrawListParam()
        param.start_time = self.timeView.startTime
        param.end_time = self.timeView.endTime
        param.pageIndex = "1"
        
        HttpClient.default.getWithdrawListData(param, { (status, data) in
            self.mj_head.endRefreshing()
            self.isFirstLoad2 = false
            if !status {
                return
            }
            self.listData2 = data
            self.datas2.removeAll()
            self.datas2.append(contentsOf: self.listData2!.data)
            self.tableView.reloadData()
            
            if self.datas2.count == 0 {
                self.tableView.mj_footer = nil
            } else {
                if self.datas2.count >= 15 {
                    self.tableView.mj_footer = self.mj_foot
                }
            }
        })
    }
    
    func loadMoreChargeReportData() {
        // load list
        var param = ChargeListParam()
        param.start_time = self.timeView.startTime
        param.end_time = self.timeView.endTime
        param.pageIndex = "\(self.listData!.currentPage+1)"
        
        
        HttpClient.default.getChargeListData(param, { (status, data) in
            self.mj_foot.endRefreshing()
            if !status {
                return
            }
            self.listData = data
            self.datas.append(contentsOf: self.listData!.data)
            self.tableView.reloadData()
            
            if self.listData!.data.count < 15 {
                self.tableView.mj_footer = nil
            } else {
                self.mj_foot.state = .idle
            }
        })
        self.tableView.reloadData()
    }
    
    // 提现
    func loadMoreWithdrawData() {
        var param = WithdrawListParam()
        param.start_time = self.timeView.startTime
        param.end_time = self.timeView.endTime
        param.pageIndex = "\(self.listData2!.currentPage+1)"
        
        HttpClient.default.getWithdrawListData(param, { (status, data) in
            self.mj_foot.endRefreshing()
            if !status {
                return
            }
            self.listData2 = data
            self.datas2.append(contentsOf: self.listData2!.data)
            self.tableView.reloadData()
            
            if self.listData2!.data.count < 15 {
                self.tableView.mj_footer = nil
            } else {
                self.mj_foot.state = .idle
            }
        })
    }
}
extension FECenterChargeReportViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FECenterChargeReportListCell.self)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreListData))
        self.tableView.mj_footer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: timeView.bottom, width: view.frame.size.width, height: view.frame.size.height - timeView.bottom - (kIsIphoneX ? 20 : 0))
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if selectType == 0 {
            count = datas.count
        } else {
            count = datas2.count
        }
        
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FECenterChargeReportListCell.self, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell0 = cell as! FECenterChargeReportListCell
        if self.selectType == 0 {
            cell0.update(datas[indexPath.row])
        } else {
            cell0.update2(datas2[indexPath.row])
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
