//
//  FECenterMoneyDetailViewController.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//


import UIKit
import QMUIKit

/// 零钱明细
class FECenterMoneyDetailViewController: YYTableViewController {

    var listData: MoneyListData?
    
    var datas = [MoneyListData.MoneyListItem]()
    
    var types = [AccountChangeTypeItem]()
    var typeIndex = 0
    
    // 是什么类型的群
    let groupTypeDatas = [("系统群", "0"), ("自建群", "1")]
    var groupTypeIndex = 0
    
    lazy var topbarView: UIView = {
        let v = UIView()
        self.view.addSubview(v)
        return v
    }()
    
    lazy var topLeftBtn: QMUIButton = {
        let v = QMUIButton()
        v.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.setTitle("全部", for: .normal)
        v.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.topbarView.addSubview(v)
        return v
    }()
    
    lazy var topRightBtn: QMUIButton = {
        let v = QMUIButton()
        v.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.setTitle("系统群", for: .normal)
        v.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.topbarView.addSubview(v)
        return v
    }()
    
    lazy var topvline: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        topbarView.addSubview(v)
        return v
    }()
    
    lazy var sepline: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        topbarView.addSubview(v)
        return v
    }()
    
    lazy var headview: FECenterProxyTimeHeadView = {
        let v = FECenterProxyTimeHeadView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 90))
        self.view.addSubview(v)
        v.clickHandler = {[weak self] (btn) in
            guard let self = self else {
                return
            }
            if btn == v.leftBtn {
                // start
                self.handlerStartTime(btn)
            } else {
                // end time
                self.handlerEndTime(btn)
            }
        }
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "零钱明细"
        topbarView.snp.makeConstraints { (make) in
            make.top.equalTo(kNaviBarHeight)
            make.left.equalTo(0)
            make.right.equalTo(self.view.snp.right)
            make.height.equalTo(40)
        }
        topLeftBtn.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(topbarView.snp.centerX)
            make.height.equalTo(topbarView)
        }
        topRightBtn.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(topbarView.snp.centerX)
            make.right.equalTo(topbarView)
            make.height.equalTo(topbarView)
        }
        topvline.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.width.equalTo(1)
            make.centerX.equalTo(topbarView)
            make.height.equalTo(topbarView)
        }
        sepline.snp.makeConstraints { (make) in
            make.top.equalTo(topbarView.bottom).offset(-1)
            make.left.equalTo(0)
            make.right.equalTo(self.view.snp.right)
            make.height.equalTo(1)
        }
        headview.snp.makeConstraints { (make) in
            make.top.equalTo(topbarView.snp.bottom)
            make.left.equalTo(0)
            make.right.equalTo(self.view.snp.right)
            make.height.equalTo(90)
        }
        
        self.loadingMoneyTypeData()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        self.topRightBtn.isUserInteractionEnabled = HomePageModel.showOwnerGroup ? true : false // 不显示自建群时 按钮不可点击
    }
    
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: FECenterMoneyListCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FECenterMoneyDetailViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FECenterMoneyDetailViewController {
    func handlerStartTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            guard let self = self else {
                return
            }
            
            if hasChanged {
                self.headview.freshStartTime(time)
                self.mj_head.beginRefreshing()
            }
        }
        d.datePicker.maximumDate = Date().offsetOfDay(-1)
        d.show()
    }
    
    func handlerEndTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            guard let self = self else {
                return
            }
            
            if hasChanged {
                self.headview.freshEndTime(time)
                self.mj_head.beginRefreshing()
            }
        }
        d.show()
    }
    
    // 点击显示类型数据
    @objc func handlerButtonAction(_ sender: UIButton) {
        if sender == topLeftBtn {
            var ts = [String]()
            for s in types {
                ts.append(s.name)
            }
            
            let l =  FECenterLayerTableView.instance(ts, self.typeIndex) {[weak self] (index) in
                guard let self = self else {
                    return
                }
                
                if index == self.typeIndex {
                    return
                }
                
                self.typeIndex = index
                
                self.topLeftBtn.setTitle(ts[index], for: .normal)
                
                self.mj_head.beginRefreshing()
            }
            l.show()
        } else {
            
            let l =  FECenterLayerTableView.instance(self.groupTypeDatas.map({$0.0}), self.groupTypeIndex) {[weak self] (index) in
                guard let self = self else {
                    return
                }
                
                if index == self.groupTypeIndex {
                    return
                }
                
                self.groupTypeIndex = index
                
                self.topRightBtn.setTitle(self.groupTypeDatas[index].0, for: .normal)
                
                self.mj_head.beginRefreshing()
            }
            l.show()
        }
    }
    
    // 类型
    func loadingMoneyTypeData() {
        HttpClient.default.getMoneyType { [weak self] (status, items) in
            guard let self = self else {
                return
            }
            if !status {
                return
            }
            
            self.types = items
            
            var all = AccountChangeTypeItem()
            all.sign = "all"
            all.name = "全部"
            self.types.insert(all, at: 0)
            
            self.typeIndex = 0
            
            self.loadMoneyListData()
        }
    }
    
    @objc func loadMoneyListData() {
        if self.typeIndex >= self.types.count {
            self.mj_head.endRefreshing()
            return
        }
        
        // load list
        var param = MoneyListParam()
        param.is_owner = self.groupTypeDatas[self.groupTypeIndex].1
        param.type = self.types[self.typeIndex].sign
        param.start_time = self.headview.startTime
        param.end_time = self.headview.endTime
        param.pageIndex = "1"
        
        HttpClient.default.getMoneyListData(param, { (status, data) in
            self.mj_head.endRefreshing()
            if !status {
                return
            }
            self.listData = data
            self.datas.removeAll()
            self.datas.append(contentsOf: self.listData!.data)
            self.tableView.reloadData()
            
            if self.datas.count == 0 {
                self.tableView.mj_footer = nil
            } else {
                if self.datas.count >= 15 {
                    self.tableView.mj_footer = self.mj_foot
                }
            }
        })
    }
    
    
    @objc func loadMoreMoneyListData() {
        //TODO: load more list
        var param = MoneyListParam()
        param.is_owner = self.groupTypeDatas[self.groupTypeIndex].1
        param.type = self.types[self.typeIndex].sign
        param.start_time = self.headview.startTime
        param.end_time = self.headview.endTime
        param.pageIndex = "\(self.listData!.currentPage+1)"
        
        HttpClient.default.getMoneyListData(param, { (status, data) in
            self.mj_foot.endRefreshing()
            if !status {
                return
            }
            self.listData = data
            self.datas.append(contentsOf: self.listData!.data)
            self.tableView.reloadData()
            
            if self.listData!.data.count < 15 {
                self.tableView.mj_footer = nil
            } else {
                self.mj_foot.state = .idle
            }
        })
    }
}

extension FECenterMoneyDetailViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FECenterMoneyListCell.self)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoneyListData))
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreMoneyListData))
        self.tableView.mj_footer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: headview.bottom, width: view.frame.size.width, height: view.frame.size.height - headview.bottom - (kIsIphoneX ? 20 : 0))
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FECenterMoneyListCell.self, for: indexPath)
        cell.update(datas[indexPath.row])
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

