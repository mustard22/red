//
//  FECenterPaperRecordModel.swift
//  Red
//
//  Created by MAC on 2020/4/10.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

/// 红包记录列表model
public struct FECenterPaperRecordModel {
    public enum RecordType: Int, CaseIterable {
        case send = 0
        case fetch = 1
        
        public var name: String {
            switch self {
            case .send: return "发包记录"
            case .fetch: return "抢包记录"
            }
        }
        
        public var index: Int {
            switch self {
            case .send: return 0
            case .fetch: return 1
            }
        }
    }
    
    public enum GroupType: Int, CaseIterable {
        case sys = 0
        case owner = 1
        
        public var type: String {
            switch self {
            case .sys: return "0"
            case .owner: return "1"
            }
        }
        
        public var name: String {
            switch self {
            case .sys: return "系统群"
            case .owner: return "自建群"
            }
        }
        
        var index: Int {
            switch self {
            case .sys: return 0
            case .owner: return 1
            }
        }
    }
    
    // 发包记录数据
    var datas = [SendPackageListData.SendPackageItem]()
    var listData: SendPackageListData?
    
    // 抢包记录数据
    var datas2 = [GetPackageListData.GetPackageItem]()
    var listData2: GetPackageListData?
    
    /// 记录列表类型（发包 or 抢包）
    var selectType: RecordType = .send
    
    /// 群组类型（sys or owner）
    var selectGroup: GroupType = .sys
    
    /// 显示加载更多
    var showMore: Bool = false
    
    /// 当前选中记录类型列表对应的数据个数
    var listCount: Int {
        return selectType == .send ? datas.count : datas2.count
    }
    
    /// 当前选中的是自建群 还是系统群
    var isOwner: Bool {
        return selectGroup == .owner ? true : false
    }
    
    /// 每次请求数量
    private let loadCount = 15
}

//MARK: - public methods
extension FECenterPaperRecordModel {
    //MARK: 根据index返回一个cell显示的数据对象(发/抢包 data item)
    func cellItem(with index: Int) -> Any? {
        if self.selectType == .send {
            if index >= 0, index < self.datas.count {
                return self.datas[index]
            }
        } else {
            if index >= 0, index < self.datas2.count {
                return self.datas2[index]
            }
        }
        
        return nil
    }
    
    //MARK: 下拉请求数据
    mutating func loadListData(from startTime: String, to endTime: String, handler: @escaping (()->Void)) {
        if selectType == .send {
            loadSendPaperReportData(from: startTime, to: endTime, handler: handler)
        } else {
            loadFetchPaperData(from: startTime, to: endTime, handler: handler)
        }
    }
    
    //MARK: 上拉请求更多数据
    mutating func loadMoreListData(from startTime: String, to endTime: String, handler: @escaping (()->Void)) {
        if selectType == .send {
            loadMoreSendPaperReportData(from: startTime, to: endTime, handler: handler)
        } else {
            loadMoreFetchPaperData(from: startTime, to: endTime, handler: handler)
        }
    }
}


//MARK: - private methods
extension FECenterPaperRecordModel {
    //MARK: 下拉请求发包数据
    mutating private func loadSendPaperReportData(from startTime: String, to endTime: String, handler: @escaping (()->Void)) {
        var param = SendPackageParam()
        param.start_time = startTime
        param.end_time = endTime
        param.pageIndex = "1"
        param.type = self.selectGroup.type
        
        let unsafe = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.getSendPackageListData(param, { (status, data) in
            if !status {
                handler()
                return
            }
            
            unsafe.pointee.listData = data
            unsafe.pointee.datas.removeAll()
            unsafe.pointee.datas.append(contentsOf: unsafe.pointee.listData!.data)
            
            if unsafe.pointee.datas.count == 0 {
                unsafe.pointee.showMore = false
            } else {
                if unsafe.pointee.datas.count >= unsafe.pointee.loadCount {
                    unsafe.pointee.showMore = true
                }
            }
            
            handler()
        })
    }
    
    //MARK: 加载更多发包数据
    mutating private func loadMoreSendPaperReportData(from startTime: String, to endTime: String, handler: @escaping (()->Void)) {
        // load list
        var param = SendPackageParam()
        param.start_time = startTime
        param.end_time = endTime
        param.pageIndex = "\(self.listData!.currentPage+1)"
        param.type = self.selectGroup.type
    
        let unsafe = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.getSendPackageListData(param, { (status, data) in
            if !status {
                handler()
                return
            }
            unsafe.pointee.listData = data
            unsafe.pointee.datas.append(contentsOf: unsafe.pointee.listData!.data)

            if unsafe.pointee.listData!.data.count < unsafe.pointee.loadCount {
                unsafe.pointee.showMore = false
            } else {
                unsafe.pointee.showMore = true
            }
            
            handler()
        })
    }
    
    
    //MARK: 下拉请求抢包数据
    mutating private func loadFetchPaperData(from startTime: String, to endTime: String, handler: @escaping (()->Void)) {
        var param = GetPackageParam()
        param.start_time = startTime
        param.end_time = endTime
        param.pageIndex = "1"
        param.type = self.selectGroup.type
        
        let unsafe = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        
        HttpClient.default.getGetPackageListData(param, { (status, data) in
            if !status {
                handler()
                return
            }
            unsafe.pointee.listData2 = data
            unsafe.pointee.datas2.removeAll()
            unsafe.pointee.datas2.append(contentsOf: unsafe.pointee.listData2!.data)
            
            if unsafe.pointee.datas2.count == 0 {
                unsafe.pointee.showMore = false
            } else {
                if unsafe.pointee.datas2.count >= unsafe.pointee.loadCount {
                    unsafe.pointee.showMore = true
                }
            }
            
            handler()
        })
    }
    
    //MARK: 加载更多抢包数据
    mutating private func loadMoreFetchPaperData(from startTime: String, to endTime: String, handler: @escaping (()->Void)) {
        var param = GetPackageParam()
        param.start_time = startTime
        param.end_time = endTime
        param.pageIndex = "\(self.listData2!.currentPage+1)"
        param.type = self.selectGroup.type
        
        let unsafe = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.getGetPackageListData(param, { (status, data) in
            if !status {
                handler()
                return
            }
            unsafe.pointee.listData2 = data
            unsafe.pointee.datas2.append(contentsOf: unsafe.pointee.listData2!.data)
            
            if unsafe.pointee.listData2!.data.count < unsafe.pointee.loadCount {
                unsafe.pointee.showMore = false
            } else {
                unsafe.pointee.showMore = true
            }
            
            handler()
        })
    }
}

