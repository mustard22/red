//
//  FECenterInfoModel.swift
//  Red
//
//  Created by MAC on 2019/8/7.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation


enum cellType: Int {
    case listCellType
    case agecyCellType
    case chargeType
    case setType
}


public struct FEListgModel {
    let username: String
    let imageUrl: String
    let cellType: cellType
    
    init(username: String, imageUrl: String,cellType:cellType) {
        self.username = username
        self.imageUrl = imageUrl
        self.cellType = cellType
    }
}


public struct FEModel {
    var data:[FEListgModel]
    
    init(data:[FEListgModel]) {
        self.data = data
    }
}


public struct FEAddBankModel {
    public enum AddBankCellType {
        case click
        case input
    }
    
    var title: String
    var content: String
    var placeHolder: String?
    var cellType: AddBankCellType
    
    init(title:String, content:String, cellType:AddBankCellType, placeHolder: String? = nil) {
        self.title = title
        self.content = content
        self.cellType = cellType
        self.placeHolder = placeHolder
    }
}
