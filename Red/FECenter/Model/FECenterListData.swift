//
//  FECenterListData.swift
//  Red
//
//  Created by MAC on 2019/8/7.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

class FECenterListData {
    var listData = [FEListgModel]()
    
    func loadLates() {
    
        let listData = [
                FEListgModel(
                    username:"代理中心",
                    imageUrl:"picture1",
                    cellType:cellType.listCellType
                ),
                FEListgModel(
                    username:"充提记录",
                    imageUrl:"picture2",
                    cellType:cellType.listCellType
                ),
                FEListgModel(
                    username:"红包记录",
                    imageUrl:"picture3",
                    cellType:cellType.listCellType
                ),
                FEListgModel(
                    username:"零钱明细",
                    imageUrl:"picture4",
                    cellType:cellType.listCellType
                ),
                FEListgModel(
                    username:"设置中心",
                    imageUrl:"picture5",
                    cellType:cellType.listCellType
                )
        ]
        self.listData = listData
    }
    
    var agencyData = FEModel(data: [])
    func loadAgencys() {
        let agencyData =  FEModel(data:[
            FEListgModel(
                username:"代理规则",
                imageUrl:"icon_item_rule",
                cellType:cellType.agecyCellType
            ),
            FEListgModel(
                username:"推广赚钱",
                imageUrl:"icon_item_tgjc",
                cellType:cellType.agecyCellType
            ),
            
        ]
        )
        self.agencyData = agencyData
    }
    
    var agencyDatas = FEModel(data: [])
    func loadAgencyss() {
        let agencyData =  FEModel(data:[
            FEListgModel(
                username:"代理报表",
                imageUrl:"icon_item_wdbb",
                cellType:cellType.agecyCellType
            ),
            FEListgModel(
                username:"下级列表",
                imageUrl:"icon_item_xjwj",
                cellType:cellType.agecyCellType
            ),
            ]
        )
        self.agencyDatas = agencyData
    }
    
    var chargeData = FEModel(data:[])
    func loadCharge() {
        let charge = FEModel(data:[
            FEListgModel(
                username:"充值记录",
                imageUrl:"icon_send_record",
                cellType:cellType.chargeType
            ),
            FEListgModel(
                username:"提现记录",
                imageUrl:"icon_grab_record",
                cellType:cellType.chargeType
            )
        ]
      )
        self.chargeData = charge
    }
    var timeData = FEModel(data:[])
    func loadTime() {
        let charge = FEModel(data:[
            FEListgModel(
                username:"开始时间",
                imageUrl:"ic_bill_start",
                cellType:cellType.chargeType
            ),
            FEListgModel(
                username:"结束时间",
                imageUrl:"ic_bill_stop",
                cellType:cellType.chargeType
            ),
            ]
        )
        self.timeData = charge
    }
    
    
    var list = FEModel(data: [])
    
    func load() {
        /// 我的模块列表 小图标
        let list = FEModel(data: [
            FEListgModel(
                username:"代理中心",
                imageUrl:"ic_app_version",
                cellType:cellType.listCellType
            ),
            FEListgModel(
                username:"充提记录",
                imageUrl:"ic_report",
                cellType:cellType.listCellType
            ),
            FEListgModel(
                username:"红包记录",
                imageUrl:"hong_bao",
                cellType:cellType.listCellType
            ),
            FEListgModel(
                username:"零钱明细",
                imageUrl:"ic_withdraw",
                cellType:cellType.listCellType
            ),
            FEListgModel(
                username:"设置中心",
                imageUrl:"ic_setting",
                cellType:cellType.listCellType
            ),
            FEListgModel(
                username:"客服中心",
                imageUrl:"icon_bill_records",
                cellType:cellType.listCellType
            ),
            FEListgModel(
                username:"退出登录",
                imageUrl:"ic_exit",
                cellType:cellType.listCellType
            )
        ]
    )
        self.list = list
    }
    

}




struct FEMineListData {
    static var listDatas: [[FEMineListModel]] {
        let sources = [
            [
            ("活动中心", "ic_activity"),
            ("代理中心", "ic_app_version"),
            ("财务中心", "ic_report"),
            ("设置中心", "ic_setting")
            ],
            [
            ("客服中心", "icon_bill_records")
            ]
        ]

        return sources.map({$0.map({FEMineListModel(url: $0.1, title: $0.0)})})
    }
}

struct FEMineListModel {
    var imageUrl: String
    var title: String
    init(url: String, title: String) {
        self.imageUrl = url
        self.title = title
    }
}

//MARK: 设置中心列表事件类型
public enum SetCenterAction: String, CaseIterable {
    public typealias RawValue = String
    
    case loginPasswd = "登录密码"
    case moneyPasswd = "资金密码"
    case bankCard = "银行卡"
    case phoneNumber = "手机号"
    case email = "邮箱"
    case tipsVoice = "红包提示音"
    case version = "系统版本"
    case checkVersion = "检查新版本"
    case clearCache = "清理缓存"
    case logout = "退出登录"
    
}

public struct FESetCenterModel {
    var datas: (SetCenterAction, YYListItem.ActionType, String)
    var d = SetCenterAction.allCases
}
