//
//  FEActivityViewController.swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class FEActivityViewController: YYTableViewController {
    private lazy var rightTop: QMUIButton = {
        let v = QMUIButton()
        v.size = CGSize(width: 40, height: 40)
        v.addTarget(self, action: #selector(customerAction), for: .touchUpInside)
        
        let imgv = UIImageView()
        imgv.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        imgv.image = UIImage(named: "icon_recharge_kf")
        v.addSubview(imgv)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "活动"
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.rightTop)
        // first load
        laodActivityData()
    }
    
    
    private var listData: [Activity] = []
    
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        self.registerTableviewCell()
        tableView.reloadData()
    }
    
    func registerTableviewCell() {
        tableView.registerCell(withClass: FEActivityCollectionViewCell.self)
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FEActivityViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FEActivityViewController {
    /// 请求活动数据
    @objc func laodActivityData() {
        HttpClient.default.getActivityList { (isSuccess, message, lists) in
            self.mj_head.endRefreshing()
            if isSuccess {
                self.listData.removeAll()
                self.listData.append(contentsOf: lists)
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func customerAction() {
        ContactCustomer.startContact(currentvc: self, enterChat: true)
    }
}
extension FEActivityViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.registerTableviewCell()
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.laodActivityData))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = listData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withClass: FEActivityCollectionViewCell.self, for: indexPath)
        cell.update(model)
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // 点击跳转活动详情
        let detail = FEActivityDetailViewController()
        detail.item = listData[indexPath.row]
        self.push(to: detail)
    }
}
