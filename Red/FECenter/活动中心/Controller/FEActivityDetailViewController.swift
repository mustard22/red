//
//  FEActivityDetailViewController.swift
//  Red
//
//  Created by MAC on 2019/10/21.
//  Copyright © 2019 MAC. All rights reserved.
//


/// 活动详情界面
class FEActivityDetailViewController: FEBaseWebViewController {
    public var item: Activity?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.item?.title
        self.url = item?.url ?? ""
    }
}
