//
//  FEActivityCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/6.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FEActivityCollectionViewCell: YYTableViewCell {
    
    lazy var backView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var imgv:UIImageView = {
        let imageView:UIImageView = UIImageView()
        self.backView.addSubview(imageView)
        return imageView
    }()
    
    lazy var tagImage:UIImageView = {
        let tagImga:UIImageView = UIImageView()
        tagImga.image = UIImage(named: "rb_check")
        self.backView.addSubview(tagImga)
        return tagImga
    }()
    
    lazy var titleLabel:UILabel = {
        let titleLabel:UILabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        self.backView.addSubview(titleLabel)
        return titleLabel
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        backView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        
        tagImage.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.size.equalTo(CGSize(width: 8, height: 8))
            make.bottom.equalTo(backView.snp.bottom).offset(-16)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(tagImage.snp.right).offset(10)
            make.right.equalTo(backView.snp.right).offset(-20)
            make.height.equalTo(40)
            make.bottom.equalTo(backView.snp.bottom)
        }
        imgv.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(0)
            make.bottom.equalTo(titleLabel.snp.top)
            make.right.equalTo(backView.snp.right)
        }
    }
    
    func update(_ item: Activity) {
        titleLabel.text = item.title
        imgv.setImage(with: URL(string: item.act_img))
    }
}
