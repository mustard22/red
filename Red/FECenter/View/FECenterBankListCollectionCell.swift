//
//  FECenterBankListCollectionCell.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class FECenterBankListCollectionCell: YYTableViewCell {
    lazy var backView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var tagImage:UIImageView = {
        let tagImga:UIImageView = UIImageView()
        tagImga.image = UIImage(named: "rb_check")
        tagImga.backgroundColor = .white
        self.backView.addSubview(tagImga)
        return tagImga
    }()
    
    lazy var leftTop:UILabel = {
        let t:UILabel = UILabel()
        t.font = UIFont.boldSystemFont(ofSize: 14)
        t.textAlignment = .left
        t.textColor = .white
        self.backView.addSubview(t)
        return t
    }()
    lazy var leftBottom:UILabel = {
        let t:UILabel = UILabel()
        t.font = UIFont.boldSystemFont(ofSize: 14)
        t.textAlignment = .left
        t.textColor = .white
        self.backView.addSubview(t)
        return t
    }()
    lazy var rightTop:UILabel = {
        let t:UILabel = UILabel()
        t.font = UIFont.boldSystemFont(ofSize: 14)
        t.textAlignment = .right
        t.textColor = .white
        self.backView.addSubview(t)
        return t
    }()
    lazy var rightBottom:UILabel = {
        let t:UILabel = UILabel()
        t.font = UIFont.boldSystemFont(ofSize: 14)
        t.textAlignment = .right
        t.textColor = .white
        self.backView.addSubview(t)
        return t
    }()
    
    // h = 90
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        
        tagImage.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.size.equalTo(CGSize(width: 50, height: 50))
            make.bottom.equalTo(backView.snp.bottom).offset(-15)
        }
        
        rightTop.snp.makeConstraints { (make) in
            make.width.equalTo(120)
            make.right.equalTo(backView.snp.right).offset(-10)
            make.height.equalTo(20)
            make.top.equalTo(tagImage.snp.top)
        }
        
        rightBottom.snp.makeConstraints { (make) in
            make.width.equalTo(120)
            make.right.equalTo(backView.snp.right).offset(-10)
            make.height.equalTo(20)
            make.bottom.equalTo(tagImage.snp.bottom)
        }
        
        leftTop.snp.makeConstraints { (make) in
            make.left.equalTo(tagImage.snp.right).offset(10)
            make.right.equalTo(rightTop.snp.left).offset(-20)
            make.height.equalTo(20)
            make.top.equalTo(tagImage.snp.top)
        }
        leftBottom.snp.makeConstraints { (make) in
            make.left.equalTo(tagImage.snp.right).offset(10)
            make.right.equalTo(rightTop.snp.left).offset(-20)
            make.height.equalTo(20)
            make.bottom.equalTo(tagImage.snp.bottom)
        }
    }
    
    func update(_ item: BankCardItem) {
        leftTop.text = item.bank_name
        leftBottom.text = item.username
        rightTop.text = "***\(item.show_card)"
        rightBottom.text = item.create_time
        tagImage.kf.setImage(with: URL(string: item.logo))
        backView.backgroundColor = UIColor(hex:item.color.hexStringToInt())
    }
}


extension String {
    /// 字符串转16进制
    func hexStringToInt() -> Int {
        let str = self.uppercased()
        var sum = 0
        for i in str.utf8 {
            sum = sum * 16 + Int(i) - 48 // 0-9 从48开始
            if i >= 65 {                 // A-Z 从65开始，但有初始值10，所以应该是减去55
                sum -= 7
            }
        }
        return sum
    }
}
