//
//  FEOwnerGroupIncomeListCell.swift
//  Red
//
//  Created by MAC on 2020/3/12.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 自建群列表cell
class FEOwnerGroupIncomeListCell: YYTableViewCell {
    
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        self.contentView.addSubview(v)
        return v
    }()
    
    
    lazy var leftTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .left
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .right
        v.textColor = .gray
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var leftMid: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var midMid: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightMid: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
}

extension FEOwnerGroupIncomeListCell {
  
    public func update(_ item: OwnerGroupIncomeListItem) {
        leftTop.text = item.groupName + "(\(item.type))"
        leftMid.text = "当天累计发包\n\n\(item.send_amount)"
        midMid.text = "当天累计抢包\n\n\(item.fetch_amount)"
        rightMid.text = "当天累计收益\n\n\(item.earn_amount)"
        rightBottom.text = "\(item.add_time)"
        
        if let send = Double(item.send_amount), send > 0 {
            let text = "当天累计发包\n\n\(item.send_amount)"
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes([.foregroundColor: Color.red], range: (text as NSString).range(of: item.send_amount))
            leftMid.attributedText = attr
        }
        
        if let earn = Double(item.fetch_amount), earn > 0 {
            let text = "当天累计抢包\n\n\(item.fetch_amount)"
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes([.foregroundColor: Color.green], range: (text as NSString).range(of: item.fetch_amount))
            midMid.attributedText = attr
        }
        
        if let earn = Double(item.earn_amount), earn > 0 {
            let text = "当天累计收益\n\n\(item.earn_amount)"
            let attr = NSMutableAttributedString(string: text)
            attr.addAttributes([.foregroundColor: Color.green], range: (text as NSString).range(of: item.earn_amount))
            rightMid.attributedText = attr
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.backgroundColor = UIColor.RGBColor(240, 240, 240)
        
        backview.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView)
        }
        
        leftTop.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(contentView).offset(-10)
            make.top.equalTo(0)
            make.height.equalTo(35)
        }
        
        rightBottom.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.bottom.equalTo(contentView)
            make.size.equalTo(CGSize(width: 150, height: 30))
        }
        
        
        midMid.snp.makeConstraints { (make) in
            make.centerX.equalTo(contentView)
            make.top.equalTo(leftTop.snp.bottom)
            make.width.equalTo(contentView.width/3)
            make.bottom.equalTo(rightBottom.snp.top)
        }
        
        leftMid.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(midMid)
            make.size.equalTo(midMid)
        }
        
        rightMid.snp.makeConstraints { (make) in
            make.right.equalTo(contentView)
            make.top.equalTo(midMid)
            make.size.equalTo(midMid)
        }
    }
}
