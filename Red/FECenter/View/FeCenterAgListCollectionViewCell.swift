//
//  FeCenterAgListCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/28.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import Foundation
import QMUIKit
class FeCenterAgListCollectionViewCell: YYTableViewCell {
    
    lazy var titleName:QMUILabel = {
        let title = QMUILabel()
        title.text = "3号房间"
        title.font = UIFont.systemFont(ofSize: 15)
        return title
    }()
    
    lazy var roomType:QMUILabel = {
        let roomType = QMUILabel()
        roomType.text = "房间类型"
         roomType.font = UIFont.systemFont(ofSize: 13)
        return roomType
    }()
    
    lazy var amount:QMUILabel = {
        let amount = QMUILabel()
        amount.font = UIFont.systemFont(ofSize: 13)
        amount.text = "红包金额"
        return amount
    }()
    
    lazy var RedName:QMUILabel = {
        let RedName = QMUILabel()
        RedName.font = UIFont.systemFont(ofSize: 13)
        RedName.text = "红包名称"
        return RedName
    }()
    
    lazy var roomTypeData:QMUILabel = {
        let roomTypes = QMUILabel()
        roomTypes.text = "牛牛"
        roomTypes.font = UIFont.systemFont(ofSize: 13)
        return roomTypes
    }()
    
    lazy var amountData:QMUILabel = {
        let amounts = QMUILabel()
        amounts.font = UIFont.systemFont(ofSize: 13)
        amounts.text = "52430.23"
        return amounts
    }()
    
    lazy var RedNameData:QMUILabel = {
        let RedNames = QMUILabel()
        RedNames.font = UIFont.systemFont(ofSize: 13)
        RedNames.text = "的撒会计"
        return RedNames
    }()
    
    lazy var time:QMUILabel = {
        let time = QMUILabel()
        time.font = UIFont.systemFont(ofSize: 13)
        time.text = "2019-8-28"
        return time
    }()
   
   
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.addSubview(titleName)
        contentView.addSubview(roomType)
        contentView.addSubview(amount)
        contentView.addSubview(RedName)
        contentView.addSubview(roomTypeData)
        contentView.addSubview(amountData)
        contentView.addSubview(RedNameData)
         contentView.addSubview(time)
        
        titleName.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(20)
        }
        
        roomType.snp.makeConstraints { (make) in
            make.top.equalTo(45)
            make.left.equalTo(30)
        }
        roomTypeData.snp.makeConstraints { (make) in
            make.top.equalTo(80)
            make.left.equalTo(30)
        }
        amount.snp.makeConstraints { (make) in
            make.top.equalTo(45)
            make.left.equalTo(150)
        }
        amountData.snp.makeConstraints { (make) in
            make.top.equalTo(80)
            make.left.equalTo(150)
        }
        
        RedName.snp.makeConstraints { (make) in
            make.top.equalTo(45)
            make.left.equalTo(270)
        }
        RedNameData.snp.makeConstraints { (make) in
            make.top.equalTo(80)
            make.left.equalTo(270)
        }
        
        time.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.right).offset(-10)
            make.bottom.equalTo(contentView.snp.bottom).offset((-5))
        }
    }
}

