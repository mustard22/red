//
//  FECenterAddBankCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/10/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

enum AddBankCollectionViewCellType {
    case input
    case click
}

/// 添加银行卡列表cell
class FECenterAddBankCollectionViewCell: YYTableViewCell {
    
    var ctype: AddBankCollectionViewCellType = .click
    
    lazy var arrowImage: UIImageView = {
        let iconImg = UIImageView()
        self.contentView.addSubview(iconImg)
        return iconImg
    }()
    
    lazy var leftTitle: QMUILabel = {
        let title = QMUILabel()
        title.font = UIFont.systemFont(ofSize: 14)
        title.textAlignment = .left
        self.contentView.addSubview(title)
        return title
    }()
    
    lazy var rightTitle: QMUILabel = {
        let title = QMUILabel()
        title.font = UIFont.systemFont(ofSize: 14)
        title.textAlignment = .right
        self.contentView.addSubview(title)
        return title
    }()
    
    lazy var inputTF: QMUITextField = {
        let tf = QMUITextField()
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.qmui_borderColor = .black
        tf.returnKeyType = .next
        tf.qmui_borderPosition = .bottom
        tf.qmui_borderColor = .darkGray
        tf.delegate = self
        tf.autocapitalizationType = .none
        tf.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        self.contentView.addSubview(tf)
        return tf
    }()
    
    public var inputHandler:((_ input: String)->Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        leftTitle.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.width.equalTo(120)
            make.height.equalTo(20)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        arrowImage.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 8, height: 4))
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        rightTitle.snp.makeConstraints { (make) in
            make.left.equalTo(leftTitle.snp.right).offset(10.0)
            make.right.equalTo(arrowImage.snp.left).offset(-10)
            make.height.equalTo(20)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        inputTF.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.height.equalTo(40)
            make.centerY.equalTo(contentView.snp.centerY)
            make.width.equalTo(200)
        }
    }
    
    public func update(_ model: FEAddBankModel) {
        leftTitle.text = model.title
        
        if model.cellType == .click {
            inputTF.isHidden = true
            arrowImage.isHidden = false
            rightTitle.isHidden = false
            rightTitle.text = model.content
        } else {
            inputTF.isHidden = false
            arrowImage.isHidden = true
            rightTitle.isHidden = true
            inputTF.text = model.content
            inputTF.placeholder = model.placeHolder
        }
    }
}


extension FECenterAddBankCollectionViewCell: QMUITextFieldDelegate {
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {
        guard let handler = inputHandler else {
            return
        }
        
        handler(textfield.text!)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
}
