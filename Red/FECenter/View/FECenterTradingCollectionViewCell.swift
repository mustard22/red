//
//  FECenterTradingCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/7.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

public typealias FECenterTradingCollectionViewCellHandler = ((_ index: Int) -> Void)

/// 我的模块列表 充值中心+推广赚钱
class FECenterTradingCollectionViewCell: UICollectionViewCell {
    
    public var handler:FECenterTradingCollectionViewCellHandler?
    
    static var defaultHeight: CGFloat {
        get {
            return FECenterChargeHeadView.defaultHeight
        }
    }
    
    lazy var content: FECenterChargeHeadView = {
        let v = FECenterChargeHeadView("icon_item_proxy", "充值中心", "icon_item_fxzq", "推广赚钱", CGPoint(x: 0, y: 0))
        v.clickHandler = {[weak self](isLeft) in
            guard let self = self else {
                return
            }
            if isLeft {
                self.handler?(0)
            } else {
                self.handler?(1)
            }
        }
        
        v.showSelectLayer = false
        
        contentView.addSubview(v)
        return v
    }()

    
    override func layoutSubviews() {
        super.layoutSubviews()
        content.left = 0
        content.top = 0

        layoutIfNeeded()
    }
}

