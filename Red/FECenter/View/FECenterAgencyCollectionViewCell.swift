//
//  FECenterAgencyCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class FECenterAgencyCollectionViewCell: UICollectionViewCell {
    var item:FEListgModel!
    
    lazy var top_tpBut:QMUIButton = {
        //ico_talk_news_toparrows
        let btn = QMUIButton("",image:UIImage(named: ""),fontSize: 12)
        btn.imagePosition = .top
        btn.setTitleColor(Color.text, for: .normal)
        btn.spacingBetweenImageAndTitle = 14
        btn.isUserInteractionEnabled = false
        return btn
    }()
    
    func update(with item:FEListgModel?) {
        dprint("ss")
        top_tpBut.setTitle(item?.username, for: .normal)
        top_tpBut.setImage(UIImage(named: item!.imageUrl), for: .normal)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.addSubview(top_tpBut)
        top_tpBut.snp.makeConstraints { (make) in
            make.center.equalTo(contentView.snp.center)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }
}
