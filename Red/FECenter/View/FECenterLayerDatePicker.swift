//
//  FECenterLayerDatePicker.swift
//  Red
//
//  Created by MAC on 2019/10/23.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

/// 时间选择器
class FECenterLayerDatePicker: UIView {
    var hasChanged = false
    
    lazy var formatter: DateFormatter = {
        let d = DateFormatter()
        d.dateFormat = "yyyy-MM-dd"
        return d
    }()
    
    lazy var calender: Calendar = {
        let c = Calendar(identifier: Calendar.Identifier.gregorian)
        return c
    }()
    
    lazy var com: DateComponents = {
        let c = DateComponents()
        return c
    }()
    
    lazy var datePicker: UIDatePicker = {
        let v = UIDatePicker()
        v.locale = Locale(identifier: "zh")
        v.datePickerMode = .date
        v.backgroundColor = .white
        v.maximumDate = Date() // 默认最大日期是今天
        v.addTarget(self, action: #selector(handlerDateChange(_:)), for: .valueChanged)
        self.addSubview(v)
        return v
    }()
    
    public var handlerTimeChange: ((_ time: String, _ hasChanged:Bool)->Void)?
    
    public static func instance (_ clickHandler: ((_ time: String, _ hasChanged:Bool)->Void)?) -> FECenterLayerDatePicker {
        let view = FECenterLayerDatePicker(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor(white: 0, alpha: 0)
        view.handlerTimeChange = clickHandler
        
        view.datePicker.size = CGSize(width: view.width, height: view.width*0.75)
        view.datePicker.left = 0
        view.datePicker.top = view.bottom
        return view
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.hide()
    }
    
    public func show() {
        let win = UIApplication.shared.keyWindow
        win?.addSubview(self)
        
        UIView.animate(withDuration: 0.2) {
            self.datePicker.bottom = self.bottom
            self.backgroundColor = UIColor(white: 0, alpha: 0.4)
        }
    }
    
    public func hide() {
        self.handlerTimeChange?(self.formatter.string(from: datePicker.date), hasChanged)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.datePicker.top = self.bottom
            self.backgroundColor = UIColor(white: 0, alpha: 0)
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    @objc func handlerDateChange(_ d: UIDatePicker) {
        hasChanged = true
    }
}
