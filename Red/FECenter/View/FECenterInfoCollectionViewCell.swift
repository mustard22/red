//
//  FECenterInfoCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/7.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 个人中心的用户信息展示view
class FECenterInfoCollectionViewCell: UICollectionViewCell {
    
    lazy var backImage:UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "bg_me")
        contentView.addSubview(v)
        return v
    }()
    lazy var iconImage:UIImageView = {
       let v = UIImageView()
        v.clipsToBounds = true
        backImage.addSubview(v)
        return v
    }()
    lazy var angleLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.boldSystemFont(ofSize: 10)
        v.textAlignment = .center
        v.text = "v1"
        v.numberOfLines = 0
        v.transform = CGAffineTransform.init(rotationAngle: CGFloat(-Double.pi/4)) 
        v.backgroundColor = UIColor.RGBColor(205, 195, 110)
        v.textColor = Color.red
        iconImage.addSubview(v)
        return v
    }()
    
    lazy var nameLabel:QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .left
        backImage.addSubview(v)
        return v
    }()
    
    lazy var idLabel:QMUILabel = {
        let v = QMUILabel()
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 10)
        v.textAlignment = .left
        backImage.addSubview(v)
        return v
    }()
    
    lazy var sexImage:UIImageView = {
       let v = UIImageView()
        v.layer.backgroundColor = UIColor.white.cgColor
        v.layer.cornerRadius = 7.5
        v.layer.masksToBounds = true
        backImage.addSubview(v)
        return v
    }()
    
    lazy var agentLabel:QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 14)
        backImage.addSubview(v)
        return v
    }()
    
    lazy var amountOfCode:QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 14)
        backImage.addSubview(v)
        return v
    }()
    
    lazy var balanceLabel:QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 14)
        backImage.addSubview(v)
        return v
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backImage.snp.makeConstraints { (make) in
            make.size.equalTo(contentView)
            make.center.equalTo(contentView)
        }
        iconImage.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(56)
            make.size.equalTo(CGSize(width: 60, height: 60))
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.top.equalTo(iconImage)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        sexImage.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.right)
            make.centerY.equalTo(nameLabel.snp.centerY)
            make.size.equalTo(CGSize(width: 15, height: 15))
        }
        idLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.top.equalTo(nameLabel.snp.bottom)
            make.size.equalTo(CGSize(width: 80, height: 15))
        }
        
        agentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.bottom.equalTo(iconImage)
            make.size.equalTo(CGSize(width: 90, height: 20))
        }
        
        amountOfCode.snp.makeConstraints { (make) in
            make.right.equalTo(backImage.snp.right).offset(-15)
            make.top.equalTo(iconImage)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        
        balanceLabel.snp.makeConstraints { (make) in
            make.right.equalTo(backImage.snp.right).offset(-15)
            make.bottom.equalTo(iconImage)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        
        angleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(iconImage.snp.right).offset(20)
            make.bottom.equalTo(iconImage.snp.bottom).offset(20)
            make.size.equalTo(CGSize(width: 40, height: 40))
        }
    }
    
    public func update() {
        iconImage.setImage(with: URL(string: User.default.avatar))
        nameLabel.text = User.default.nickname
        idLabel.text = "ID:\(User.default.user_id)"
        
        sexImage.isHidden = User.default.gender < "1" ? true : false
        sexImage.image = UIImage(named: "\(User.default.gender)" == "0" ? "" : ("\(User.default.gender)" == "2" ? "nv" : "nan"))
        agentLabel.text = "\(User.default.proxy_level)"
        amountOfCode.text = "打码量:\(User.default.frozen_code)"
        balanceLabel.text = "金额:\(User.default.balance)"
        
        angleLabel.text = "V\(User.default.level)\n\n"
        
        nameLabel.sizeToFit()
        nameLabel.width = min(150, nameLabel.width+10)
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.top.equalTo(iconImage)
            make.size.equalTo(CGSize(width: nameLabel.width, height: 20))
        }
        sexImage.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.right)
            make.centerY.equalTo(nameLabel.snp.centerY)
            make.size.equalTo(CGSize(width: 15, height: 15))
        }
    }
}


private let MineHeadViewSize = CGSize(width: UIScreen.main.bounds.size.width, height: 100*kScreenScale)
class FEMineListHeadView: UIButton {
    private lazy var backImage:UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "bg_me")
        self.addSubview(v)
        return v
    }()
    private lazy var iconImage:UIImageView = {
       let v = UIImageView()
        v.clipsToBounds = true
        backImage.addSubview(v)
        return v
    }()
    private lazy var angleLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.boldSystemFont(ofSize: 10)
        v.textAlignment = .center
        v.text = "v1"
        v.numberOfLines = 0
        v.transform = CGAffineTransform.init(rotationAngle: CGFloat(-Double.pi/4))
        v.backgroundColor = UIColor.RGBColor(205, 195, 110)
        v.textColor = Color.red
        iconImage.addSubview(v)
        return v
    }()
    
    private lazy var nameLabel: QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        backImage.addSubview(v)
        return v
    }()
    
    private lazy var idLabel:QMUILabel = {
        let v = QMUILabel()
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        backImage.addSubview(v)
        return v
    }()
    
    private lazy var sexImage:UIImageView = {
       let v = UIImageView()
        v.layer.backgroundColor = UIColor.white.cgColor
        v.layer.cornerRadius = 7.5
        v.layer.masksToBounds = true
        backImage.addSubview(v)
        return v
    }()
    
    private lazy var agentLabel:QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.font = UIFont.systemFont(ofSize: 15)
        backImage.addSubview(v)
        return v
    }()
    
    private lazy var amountOfCode:QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 15)
        backImage.addSubview(v)
        return v
    }()
    
    private lazy var balanceLabel:QMUILabel = {
       let v = QMUILabel()
        v.textColor = .white
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 15)
        backImage.addSubview(v)
        return v
    }()
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: MineHeadViewSize.width, height: MineHeadViewSize.height))
        self.addTarget(self, action: #selector(clickAction), for: .touchUpInside)
        update()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var clickActionHandler: (()->Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setUI()
    }
}

extension FEMineListHeadView {
    @objc private func clickAction() {
        self.clickActionHandler?()
    }
    
    public func update() {
        iconImage.setImage(with: URL(string: User.default.avatar))
        nameLabel.text = User.default.nickname
        idLabel.text = "ID:\(User.default.user_id)"
        
        sexImage.isHidden = User.default.gender < "1" ? true : false
        sexImage.image = UIImage(named: "\(User.default.gender)" == "0" ? "" : ("\(User.default.gender)" == "2" ? "nv" : "nan"))
        agentLabel.text = "\(User.default.proxy_level)"
        amountOfCode.text = "打码量:\(User.default.frozen_code)"
        balanceLabel.text = "金额:\(User.default.balance)"
        
        angleLabel.text = "V\(User.default.level)\n\n"
        
        nameLabel.sizeToFit()
        let nameWidth = min(120, nameLabel.width+5)
        nameLabel.width = nameWidth
        
        nameLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.top.equalTo(iconImage)
            make.size.equalTo(CGSize(width: nameWidth, height: 20))
        }
        sexImage.snp.remakeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.right)
            make.centerY.equalTo(nameLabel.snp.centerY)
            make.size.equalTo(CGSize(width: 15, height: 15))
        }
        
        layoutIfNeeded()
    }
    
    private func setUI() {
        backImage.snp.makeConstraints { (make) in
            make.size.equalTo(self)
            make.center.equalTo(self)
        }
        iconImage.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(20*kScreenScale)
            make.size.equalTo(CGSize(width: 60, height: 60))
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.top.equalTo(iconImage)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        sexImage.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.right)
            make.centerY.equalTo(nameLabel.snp.centerY)
            make.size.equalTo(CGSize(width: 15, height: 15))
        }
        idLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.top.equalTo(nameLabel.snp.bottom)
            make.size.equalTo(CGSize(width: 80, height: 15))
        }
        
        agentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImage.snp.right).offset(15)
            make.bottom.equalTo(iconImage)
            make.size.equalTo(CGSize(width: 90, height: 20))
        }
        
        amountOfCode.snp.makeConstraints { (make) in
            make.right.equalTo(backImage.snp.right).offset(-15)
            make.top.equalTo(iconImage)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        
        balanceLabel.snp.makeConstraints { (make) in
            make.right.equalTo(backImage.snp.right).offset(-15)
            make.bottom.equalTo(iconImage)
            make.height.equalTo(20)
            make.left.equalTo(backImage.snp.centerX)
//            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        
        angleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(iconImage.snp.right).offset(20)
            make.bottom.equalTo(iconImage.snp.bottom).offset(20)
            make.size.equalTo(CGSize(width: 40, height: 40))
        }
    }
}
