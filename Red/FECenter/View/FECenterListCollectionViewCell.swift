//
//  FECenterListCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/7.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
class FECenterListCollectionViewCell: YYTableViewCell {
    
    lazy var iconImageViwe:UIImageView = {
        let iconImg = UIImageView()
        return iconImg
    }()
    
    lazy var titleLable:QMUILabel = {
       let title = QMUILabel()
        title.text = "邀请码"
        title.font = UIFont.systemFont(ofSize: 15)
        return title
    }()
    
    lazy var moreBtn:QMUIButton = {
        let btn = QMUIButton("",image: UIImage.qmui_image(with: .disclosureIndicator, size: CGSize(width: 9, height: 15), tintColor: Color.text3), fontSize: 14)
        btn.imagePosition = .right
        btn.setTitleColor(Color.text3, for: .normal)
        btn.spacingBetweenImageAndTitle = 3
        btn.isUserInteractionEnabled = false
        
        return btn
    }()
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.addSubview(iconImageViwe)
        contentView.addSubview(titleLable)
        contentView.addSubview(moreBtn)
        
        iconImageViwe.frame = CGRect(x: 15, y: 15, width: 20, height: 20)
        titleLable.frame = CGRect(x: 49, y: 15, width: 120, height: 19.5)
        moreBtn.snp.makeConstraints { (make) in
            make.right.equalTo(-10)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}
