//
//  FECenterLayerTableView.swift
//  Red
//
//  Created by MAC on 2019/10/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

/// 浮层列表(省份城市...)
class FECenterLayerTableView: UIView {
    
    private lazy var table: UITableView = {
        let t = UITableView(frame: self.bounds, style: .plain)
        t.delegate = self
        t.dataSource = self
        t.registerCell(withClass: UITableViewCell.self)
        self.addSubview(t)
        return t
    }()
    
    // 默认选中行
    private var defaultIndex: Int?
    
    private var datas: [String] = []
    
    private var clickHandler: ((_ index: Int)->Void)?
    
    private let defaultTableHeight: CGFloat = 400*kScreenScale
    
    private var rowHeight: CGFloat = 50 * kScreenScale
    public var defaultRowHeight: CGFloat {
        get {
            return rowHeight
        }
        set (newValue) {
            rowHeight = newValue
            table.height = min(rowHeight * CGFloat(datas.count), defaultTableHeight)
            table.bottom = self.bottom
        }
    }
    
    public static func instance (_ datas: [String], _ defaultIndex: Int = -1, _ clickHandler: ((_ index: Int)->Void)?)->FECenterLayerTableView {
        let view = FECenterLayerTableView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        view.datas = datas
        view.defaultIndex = defaultIndex >= 0 ? defaultIndex : nil
        view.clickHandler = clickHandler
        view.table.reloadData()
        
        if let index = view.defaultIndex {
            view.table.scrollToRow(at: IndexPath(row: index, section: 0), at: .middle, animated: false)
        }
        
        var rect = view.bounds
        rect.size.height = min(view.rowHeight * CGFloat(view.datas.count), view.defaultTableHeight)
        rect.origin.y -= ((kIsIphoneX ? 20 : 0)+rect.size.height)
        view.table.frame = rect
        return view
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.hide()
    }
    
    public func show() {
        let win = UIApplication.shared.keyWindow
        win?.addSubview(self)
        
        self.table.top = self.bottom
        UIView.animate(withDuration: 0.3) {
            self.table.bottom = self.bottom - (kIsIphoneX ? 20 : 0)
        }
    }
    
    public func hide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.table.top = self.bottom
        }) { (fin) in
            if fin {
                self.removeFromSuperview()
            }
        }
    }
}


extension FECenterLayerTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withClass: UITableViewCell.self, for: indexPath)
        
        c.textLabel?.frame = c.contentView.bounds
        c.textLabel?.numberOfLines = 0
        c.textLabel?.textAlignment = .center
        c.textLabel?.textColor = .darkGray
        c.textLabel?.text = datas[indexPath.row]
        
        if let index = self.defaultIndex, indexPath.row == index {
            c.textLabel?.textColor = Color.red
        }
        return c
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.clickHandler?(indexPath.row)
        self.hide()
    }
}
