//
//  FECenterUpdatePhoneViewController.swift
//  Red
//
//  Created by MAC on 2019/10/16.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

/// 更换手机号界面
class FECenterUpdatePhoneViewController: FEBaseViewController {
    
    public var phoneNumber: String = ""
    
    let icon: QMUIButton = {
        let i = QMUIButton(nil, image: UIImage(named: "icon_help_center"))
        i.isUserInteractionEnabled = false
        return i
    }()
    
    let phone: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = .black
        p.textAlignment = NSTextAlignment.center
        p.font = UIFont.systemFont(ofSize: 18)
        return p
    }()
    
    let tips: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = UIColor(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        p.textAlignment = NSTextAlignment.center
        p.font = UIFont.systemFont(ofSize: 14)
        return p
    }()
    
    let submit: QMUIButton = {
        let s = QMUIButton("更换手机号", fontSize: 15.0)
        s.setTitleColor(.white, for: .normal)
        s.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        s.addTarget(self, action: #selector(handlerSubmit(_:)), for: .touchUpInside)
        return s
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "重置手机号"
        self.view.backgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
        createUI()
    }
    
    @objc func handlerSubmit(_ btn: QMUIButton) {
        self.changePhone()
    }
    
    func createUI() {
        self.view.addSubview(icon)
        self.view.addSubview(phone)
        self.view.addSubview(tips)
        self.view.addSubview(submit)
        
        
        phone.text = "当前手机号 \(phoneNumber)"
        tips.text = "已将该手机号绑定账号，可用于手机账号登录方式"
        
        icon.snp.makeConstraints { (make) in
            make.width.equalTo(30)
            make.height.equalTo(30)
            
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(108)
        }
        
        phone.snp.makeConstraints { (make) in
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(40)
            
            make.left.equalTo(0)
            make.top.equalTo(icon.snp.bottom).offset(5.0)
        }
        
        tips.snp.makeConstraints { (make) in
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(20)
            
            make.left.equalTo(0)
            make.top.equalTo(phone.snp.bottom).offset(5)
        }
        
        submit.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
            make.top.equalTo(tips.snp.bottom).offset(20.0)
        }
    }
}

extension FECenterUpdatePhoneViewController {
    private func changePhone() {
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        // 先验证资金密码
        FEVerify.verifyFundPasswd(self) {[weak self] (status, pwd, text) in
            if !status {
                QMUITips.show(withText: text)
                return
            }
            
            let vc = FECenterSetNewPhoneViewController()
            vc.fund_pwd = pwd ?? ""
            vc.completionHandler = {[weak self](msg) in
                self?.phone.text = "当前手机号：\(msg)"
            }
            self?.push(to: vc)
        }
    }
}
