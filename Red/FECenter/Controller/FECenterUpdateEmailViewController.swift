//
//  FECenterUpdateEmailViewController.swift
//  Red
//
//  Created by MAC on 2019/10/18.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 更换邮箱界面
class FECenterUpdateEmailViewController: FEBaseViewController {
    
    public var email: String = ""
    
    lazy var icon: QMUIButton = {
        let i = QMUIButton(nil, image: UIImage(named: "icon_help_center"))
        i.isUserInteractionEnabled = false
        view.addSubview(i)
        return i
    }()
    
    lazy var phone: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = .black
        p.textAlignment = NSTextAlignment.center
        p.font = UIFont.systemFont(ofSize: 18)
        view.addSubview(p)
        return p
    }()
    
    lazy var tips: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.text = "已将该邮箱绑定账号"
        p.textColor = UIColor.darkText
        p.textAlignment = NSTextAlignment.center
        p.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(p)
        return p
    }()
    
    lazy var submit: QMUIButton = {
        let s = QMUIButton("添加邮箱", fontSize: 15.0)
        s.setTitleColor(.white, for: .normal)
        s.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        s.addTarget(self, action: #selector(handlerSubmit(_:)), for: .touchUpInside)
        view.addSubview(s)
        return s
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "重置邮箱"
        self.view.backgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
        createUI()
        
        self.refreshViews()
    }
    
    func createUI() {
        icon.snp.makeConstraints { (make) in
            make.width.equalTo(30)
            make.height.equalTo(30)
            
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(108)
        }
        
        phone.snp.makeConstraints { (make) in
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(40)
            make.left.equalTo(0)
            make.top.equalTo(icon.snp.bottom).offset(5.0)
        }
        
        tips.snp.makeConstraints { (make) in
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(18)
            make.left.equalTo(0)
            make.top.equalTo(phone.snp.bottom).offset(5.0)
        }
        
        submit.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
            make.top.equalTo(tips.snp.bottom).offset(20.0)
        }
    }
    
    func refreshViews() {
        if !email.isEmpty {
            tips.isHidden = false
            phone.text = "当前邮箱：\(email)"
            submit.setTitle("重置邮箱", for: .normal)
        } else {
            tips.isHidden = true
            phone.text = "当前账号没有绑定邮箱"
            submit.setTitle("添加邮箱", for: .normal)
        }
    }
}

//MARK: actions
extension FECenterUpdateEmailViewController {
    @objc func handlerSubmit(_ btn: QMUIButton) {
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        // 先验证资金密码
        FEVerify.verifyFundPasswd(self) {[weak self] (status, pwd, text) in
            if !status {
                QMUITips.show(withText: text)
                return
            }
            
            let vc = FECenterSetNewEmailViewController()
            vc.completionHandler = {[weak self](msg) in
                User.default.email = msg // 本地缓存
                self?.email = msg
                self?.refreshViews()
            }
            self?.push(to: vc)
        }
    }
}
