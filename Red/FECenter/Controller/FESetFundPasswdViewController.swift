//
//  FESetFundPasswdViewController.swift
//  Red
//
//  Created by MAC on 2019/12/20.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 设置资金密码界面
class FESetFundPasswdViewController: FEBaseViewController {
    private lazy var backTable: UITableView = {
        let v = UITableView()
        v.separatorStyle = .none
        v.frame = view.bounds
        v.backgroundColor = view.backgroundColor
        view.addSubview(v)
        return v
    }()
    
    private lazy var contentView: UIView = {
        let v = UIView()
        backTable.tableHeaderView = v
        return v
    }()
    
    lazy var currentTF: QMUITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请输入当前资金密码")
        v.isSecureTextEntry = true
        v.keyboardType = .numberPad
        v.maximumTextLength = 6 // 资金密码6位
        contentView.addSubview(v)
        return v
    }()
    lazy var newTF: QMUITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请输入新的资金密码")
        v.isSecureTextEntry = true
        v.keyboardType = .numberPad
        v.maximumTextLength = 6 // 资金密码6位
        contentView.addSubview(v)
        return v
    }()
    lazy var sureTF: QMUITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请确认新的资金密码")
        v.isSecureTextEntry = true
        v.keyboardType = .numberPad
        v.maximumTextLength = 6 // 资金密码6位
        v.returnKeyType = .done
        contentView.addSubview(v)
        return v
    }()
    
    lazy var submitBtn: QMUIButton = {
        let v = QMUIButton()
        v.backgroundColor = Color.red
        v.setTitleColor(.white, for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.setTitle("提交", for: .normal)
        v.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    /// 是否已经设置密码
    private var isSetted: Bool {
        return User.default.has_funds_password == "1" ? true : false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "资金密码"
        
        if isSetted {
            currentTF.isHidden = false
            currentTF.becomeFirstResponder()
        } else {
            currentTF.isHidden = true
            newTF.becomeFirstResponder()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setUI()
    }
    
    deinit {
        self.view.endEditing(true)
    }
}


//MARK: QMUITextFieldDelegate
extension FESetFundPasswdViewController: QMUITextFieldDelegate {
    
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if currentTF.isFirstResponder {
            currentTF.resignFirstResponder()
            newTF.becomeFirstResponder()
        }
        else if newTF.isFirstResponder {
            newTF.resignFirstResponder()
            sureTF.becomeFirstResponder()
        }
        else if sureTF.isFirstResponder {
            sureTF.resignFirstResponder()
            submitButtonAction()
        }
        
        return true
    }
}

extension FESetFundPasswdViewController {
    private func makeStyleTF(_ textField: QMUITextField? = nil, image: String, placeholder: String, rightBtn: Bool? = false) -> QMUITextField {
        let imageView = UIImageView()
        imageView.image = UIImage(named: image)
        imageView.size = CGSize(width: 20, height: 20)
        
        let textfield = textField ?? QMUITextField(placeholder, font: UIFont.systemFont(ofSize: 15), color: Color.text)
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
        textfield.font = UIFont.systemFont(ofSize: 15)
        textfield.placeholder = placeholder
        
        textfield.returnKeyType = .next
        textfield.delegate = self
        textfield.qmui_borderPosition = .bottom
        textfield.qmui_borderColor = .darkGray
        textfield.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        return textfield
    }
    private func setUI() {
        backTable.snp.makeConstraints { (make) in
            make.size.equalTo(self.view)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        contentView.snp.makeConstraints { (make) in
            make.size.equalTo(self.view)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        currentTF.snp.makeConstraints { (make) in
            make.top.equalTo(15)
            make.left.equalTo(15)
            make.right.equalTo(contentView.snp.right).offset(-15)
            make.height.equalTo(51)
        }
        newTF.snp.makeConstraints { (make) in
            make.top.equalTo(isSetted ? currentTF.snp.bottom : currentTF.snp.top)
            make.left.equalTo(currentTF)
            make.right.equalTo(currentTF)
            make.height.equalTo(currentTF)
        }
        sureTF.snp.makeConstraints { (make) in
            make.top.equalTo(newTF.snp.bottom)
            make.left.equalTo(currentTF)
            make.right.equalTo(currentTF)
            make.height.equalTo(currentTF)
        }
        
        submitBtn.snp.makeConstraints { (make) in
            make.top.equalTo(sureTF.snp.bottom).offset(20)
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.height.equalTo(44)
        }
    }
    
    //MARK: Action
    @objc func submitButtonAction() {
        if isSetted {
            // 修改
            changeFundPasswd()
        } else {
            // 第一次设置
            setFundPasswd()
        }
    }
    
    //MARK: 第一次设置资金密码
    func setFundPasswd() {
        guard let pwd = newTF.text, pwd.count>0 else {
            QMUITips.show(withText: "请输入资金密码", in: self.view)
            return
        }
        guard let sure = sureTF.text, sure.count>0 else {
            QMUITips.show(withText: "请输入确认资金密码", in: self.view)
            return
        }
        guard pwd == sure else {
            QMUITips.show(withText: "两次输入不一致", in: self.view)
            return
        }
        var item = SetFundPasswdParam()
        item.password = pwd
        item.sure_pwd = sure
        
        let load = QMUITips.showLoading(in: self.view)
        UserCenter.default.setFundPassword(item) {[weak self] (status, msg) in
            load.hide(animated: true)
            if status {
                User.default.has_funds_password = "1"
                self?.close()
            }
            QMUITips.show(withText: msg)
        }
    }
    
    //MARK: 修改资金密码
    func changeFundPasswd() {
        guard let current = currentTF.text, current.count>0 else {
            QMUITips.show(withText: "请输入当前资金密码", in: self.view)
            return
        }
        guard let pwd = newTF.text, pwd.count>0 else {
            QMUITips.show(withText: "请输入新的资金密码", in: self.view)
            return
        }
        guard let sure = sureTF.text, sure.count>0 else {
            QMUITips.show(withText: "请输入确认资金密码", in: self.view)
            return
        }
        guard pwd == sure else {
            QMUITips.show(withText: "确认密码与新密码不一致", in: self.view)
            return
        }
        var item = ChangeFundPasswdParam()
        item.old_password = current
        item.password = pwd
        item.sure_pwd = sure
        
        let load = QMUITips.showLoading(in: self.view)
        UserCenter.default.changeFundPassword(item) {[weak self] (status, msg) in
            load.hide(animated: true)
            if status {
                QMUITips.show(withText: msg.count>0 ? msg:"修改成功")
                self?.close()
            } else {
                QMUITips.show(withText: msg)
            }
        }
    }
}
