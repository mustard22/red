//
//  FECenterAddBankCardViewController.swift
//  Red
//
//  Created by MAC on 2019/10/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import SwiftyJSON

typealias ParseProvinceCityDataType = Dictionary<String, Dictionary<String, Dictionary<String, String>>>

struct Province {
    var name: String = ""
    var pid: String
    var cities: [City] = []
    
    init(_ pid: String) {
        self.pid = pid
    }
}

struct City {
    var name: String = ""
    var cid: String
    init(_ cid: String) {
        self.cid = cid
    }
}

class FECenterAddBankCardViewController: YYTableViewController {
    public var completionHandler: (()->Void)?
    
    private lazy var submitItem: BindCardParam = {
       var m = BindCardParam()
        m.bank_sign = (User.default.bank_list.first?.keys.first)!
        return m
    }()
    
    private var titles:[(String, String, FEAddBankModel.AddBankCellType, String)] = [("银行标识", "", .click, ""),("选择省份", "", .click, ""),("选择城市", "", .click, ""),("开户行名称", "", .input, "请输入开户行名称"),("持卡人姓名", "", .input, "请输入持卡人姓名"),("银行卡卡号", "", .input, "请输入银行卡号")]
    
    private lazy var datas: [FEAddBankModel] = {
        let d = [FEAddBankModel]()
        return d
    }()
    
    private var bankSelectIndex = 0
    private var provinceSelectIndex = 0
    private var citySelectIndex = 0
    
    private lazy var banks:[String] = {
        let banks  = User.default.bank_list
        var b = [String]()
        for d in banks {
            b.append(d.values.first!)
        }
        return b
    }()
    
    /// 所有的省份数据
    private lazy var provinces: [String] = {
        var p:[String] = allProvinceCityData.map { (m) in
            return m.name
        }
        return p
    }()
    
    /// 省份城市数据
    private lazy var allProvinceCityData: [Province] = {
        let d = self.parseJsonData()
        return d
    }()
    
    
    lazy var footView: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60)
        
        let b = QMUIButton("提交", fontSize: 18)
        b.frame = CGRect(x: 30, y: 10, width: v.frame.size.width-30*2, height: 40)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        b.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
        v.addSubview(b)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "添加银行卡"
        self.tableView.backgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
        
        // 设置默认显示
        titles[0].1 = banks[0]
        titles[1].1 = provinces[0]
        titles[2].1 = getCityNames(provinces[0])[0]
        
        datas = titles.map({FEAddBankModel(title: $0.0, content: $0.1, cellType: $0.2, placeHolder: $0.3)})
        
        self.tableView.reloadData()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: FECenterAddBankCollectionViewCell.self)
        tableView.reloadData()
    }
}

//MARK: 数据处理
extension FECenterAddBankCardViewController {
    // 获取省份ID
    func getProvinceID(_ pname: String) -> String {
        for (_,m) in allProvinceCityData.enumerated() {
            if m.name == pname {
                return m.pid
            }
        }
        return ""
    }
    
    // 获取当前省份下的所有城市
    func getCityNames(_ pname: String) -> [String] {
        var p: Province?
        for (_,m) in allProvinceCityData.enumerated() {
            if m.name == pname {
                p = m
                break
            }
        }
        if let item = p {
            return item.cities.map { (c) in
                return c.name
            }
        }
        
        return []
    }
    
    // 根据省份和城市，获取城市id
    func getCityID(_ pname: String, _ cname: String) -> String {
        var p: Province?
        for (_,m) in allProvinceCityData.enumerated() {
            if m.name == pname {
                p = m
                break
            }
        }
        guard let item = p else {
            return ""
        }
        
        for (_, c) in item.cities.enumerated() {
            if cname == c.name {
                return c.cid
            }
        }
        
        return ""
    }
    
    //MARK: 解析省份城市数据
    func parseJsonData() -> [Province] {
        let p = Bundle.main.path(forResource: "Address", ofType: "json")
        guard let path = p else {
            return []
        }
        
        let data = NSData(contentsOfFile: path)
        let obj = try? JSONSerialization.jsonObject(with: data! as Data, options: .mutableContainers)
        guard let nsdict = (obj as? Dictionary<String, Any>) else {
            return []
        }
       
        
        var alldata = [Province]()
        for (pid, value) in nsdict {
            var p = Province(pid)
            
            let dict = value as! Dictionary<String, Any>
            p.name = dict["name"] as! String
            
            let cityDict = dict["city"] as! Dictionary<String, String>
            for (cid, cname) in cityDict {
                var c = City(cid)
                c.name = cname
                p.cities.append(c)
            }
            p.cities.sort(by: {Int(Double($0.cid)!) < Int(Double($1.cid)!)})
            alldata.append(p)
        }
        
        alldata.sort(by: {Int(Double($0.pid)!) < Int(Double($1.pid)!)})
        return alldata
    }
    
    func handleProvinceCityData()-> ParseProvinceCityDataType {
        let p = Bundle.main.path(forResource: "Address", ofType: "json")
        guard let path = p else {
            return [:]
        }
        
        let data = NSData(contentsOfFile: path)
        
        let obj = try? JSON(data: data! as Data, options: .mutableContainers)
        
        guard let dict = obj?.dictionaryObject else {
            return [:]
        }

//        解析最后的数据格式
//        {
//        "pname": {"pid":{"cname":"cid","cname2":"cid2"}},
//        "pname": {"pid":{"cname":"cid","cname2":"cid2"}},
//        }
//        {
//            "pid":{
//                "name": "pname",
//                "city":{
//                    "cid":""
//                }
//            }
//        }
        
        var alldata = ParseProvinceCityDataType()
        for (k,v) in dict {
            var item: Dictionary<String, Dictionary<String,String>> = [:]
            let pid = k
            var name = ""
            
            for (k2,v2) in v as! Dictionary<String, Any> {
                var item2 = Dictionary<String,String>()
                if k2 == "name" {
                    name = v2 as! String
                } else {
                    let city = v2 as! Dictionary<String, String>
                    for (k3,v3) in city {
                        item2[v3] = k3
                    }
                    item[pid] = item2
                }
            }
            alldata[name] = item
        }
        
        return alldata
    }
}

//MARK: click actions
extension FECenterAddBankCardViewController {
    @objc func submitAction() {
        self.submitItem.province_id = self.getProvinceID(datas[1].content)
        self.submitItem.city_id = self.getCityID(self.datas[1].content, datas[2].content)
        
        for i in 3..<datas.count {
            if datas[i].content.count == 0 {
                return
            }
        }
        
        self.submitItem.branch = datas[3].content
        self.submitItem.true_name = datas[4].content
        self.submitItem.card_number = datas[5].content
        
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        // 检测资金密码
        self.checkFundPasswd {[weak self] (status, pwd) in
            guard let self = self, status == true else {
                return
            }
            
            self.submitItem.fund_password = pwd
            let load = QMUITips.showLoading(in: self.view)
            // 提交
            UserCenter.default.bindBankCard(self.submitItem) {[weak self] (status, msg) in
                load.hide(animated: true)
                QMUITips.show(withText: msg)
                guard let self = self else {
                    return
                }
                if let h = self.completionHandler {
                    h()
                }
                self.close()
            }
        }
    }
    
    private func checkFundPasswd(_ handler: ((_ status: Bool, _ pwd: String)->Void)?) {
        FEVerify.verifyFundPasswd(self) { (status, pwd, text) in
            if !status {
                QMUITips.show(withText: text, in: self.view)
                return
            }
            
            handler?(status, pwd ?? "")
        }
    }
}


// MARK: - QMUINavigationTitleViewDelegate
extension FECenterAddBankCardViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}


extension FECenterAddBankCardViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FECenterAddBankCollectionViewCell.self)
        self.tableView.tableFooterView = self.footView
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FECenterAddBankCollectionViewCell.self, for: indexPath)
        let m = datas[indexPath.row]
        if m.placeHolder!.count > 0 {
            cell.inputHandler = {[weak self](input) in
                guard let self = self else {
                    return
                }
                self.datas[indexPath.row].content = input
                dprint("\(self.datas[indexPath.row].title): \(input)")
            }
        }
        
        if datas.count-1 == indexPath.row {
            cell.inputTF.maximumTextLength = 16
        }
        cell.update(m)
        
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model = datas[indexPath.row]
        if model.cellType == .input {
            return
        }
        
        switch indexPath.row {
        case 0:
            self.showLayerListOfBank()
        case 1:
            self.showLayerListOfProvince()
        case 2:
            self.showLayerListOfCity()
        case 3,4,5:
            let cell = tableView.cellForRow(at: indexPath) as! FECenterAddBankCollectionViewCell
            cell.inputTF.becomeFirstResponder()
        default: break
        }
    }
}


//MARK: 显示浮层列表
extension FECenterAddBankCardViewController {
    /// 银行卡列表
    func showLayerListOfBank() {
        self.view.endEditing(true)
        
        let l =  FECenterLayerTableView.instance(self.banks, self.bankSelectIndex) {[weak self] (index) in
            guard let self = self else {
                return
            }
            
            self.datas[0].content = (self.banks[index])
            
            self.bankSelectIndex = index
            
            self.submitItem.bank_sign = User.default.bank_list[index].keys.first!
            
            self.tableView.reloadData()
        }
        l.show()
    }
    
    /// 省份列表
    func showLayerListOfProvince() {
        self.view.endEditing(true)
        
        let l =  FECenterLayerTableView.instance(self.provinces, self.provinceSelectIndex) {[weak self] (index) in
            guard let self = self else {
                return
            }
            
            var m = self.datas[1]
            if m.content == self.provinces[index] {
                return
            }
            
            self.provinceSelectIndex = index
            self.citySelectIndex = 0
            
            m.content = self.provinces[index]
            
            let citys = self.getCityNames(m.content)
            self.datas[2].content = citys[0]
            
            self.datas[1] = m
            self.tableView.reloadData()
        }
        l.show()
    }
    
    /// 城市列表
    func showLayerListOfCity() {
        self.view.endEditing(true)
        
        let citys = self.getCityNames(datas[1].content)
        let l =  FECenterLayerTableView.instance(citys, self.citySelectIndex) {[weak self] (index) in
            guard let self = self else {
                return
            }
            
            self.datas[2].content = citys[index]
            
            self.citySelectIndex = index
            
            self.tableView.reloadData()
        }
        l.show()
    }
}
