//
//  FECenterChargeCenterViewController.swift
//  Red
//
//  Created by MAC on 2019/10/15.
//  Copyright © 2019 MAC. All rights reserved.
//

/// 充值中心界面
import UIKit
import QMUIKit

class FECenterChargeCenterViewController: FEBaseViewController {
    
    private lazy var contentView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.frame = CGRect(x: 0, y: kNaviBarHeight, width: self.view.frame.width, height: 0)
        self.view.addSubview(v)
        return v
    }()
    
    // (图标，标题)
    private let items = [("icon_send_record", "充值中心"), ("icon_grab_record", "提现中心"), ("icon_cz_record", "充值记录"), ("icon_cz_record", "提现记录")]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "充值中心"
        
        createUI()
    }
}


extension FECenterChargeCenterViewController {
    private func createUI() {
        for i in 0..<items.count {
            if i%2 != 0 {
                continue
            }
            
            let v = FECenterChargeHeadView(items[i].0, items[i].1, items[i+1].0, items[i+1].1, CGPoint(x: 0, y: contentView.height))
            v.showSelectLayer = false
            v.clickHandler = {[weak self](isLeft) in
                guard let self = self else {
                    return
                }
                if isLeft {
                    self.handlerButtonAction(i)
                } else {
                    self.handlerButtonAction(i+1)
                }
            }
            
            contentView.addSubview(v)
            contentView.height += v.height
        }
    }
    
    func handlerButtonAction(_ tag: Int) {
        switch tag {
        case 0:
            // 充值
            let vc = FECenterChargeMoneyViewController()
            self.push(to: vc)
        case 1:
            // 提现
            let vc = FECenterWithdrawViewController()
            self.push(to: vc)
        case 2:
            // 充值记录
            let vc = FECenterChargeReportViewController()
            self.push(to: vc)
        case 3:
            // 提现记录
            let vc = FECenterChargeReportViewController()
            vc.selectType = 1
            self.push(to: vc)
        default:
            fatalError()
        }
    }
}
