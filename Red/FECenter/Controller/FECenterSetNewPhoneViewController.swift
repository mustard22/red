//
//  FECenterSetNewPhoneViewController.swift
//  Red
//
//  Created by MAC on 2019/10/18.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

/// 设置新的手机号界面(密码验证后的界面)
class FECenterSetNewPhoneViewController: FEBaseViewController {
    
    public var fund_pwd: String = ""
    public var completionHandler: ((_ msg: String)->Void)?
    
    private var timer: Timer?
    private var count = 60
    
    lazy var tips1: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = .black
        p.textAlignment = NSTextAlignment.left
        p.font = UIFont.systemFont(ofSize: 18)
        p.text = "请输入新的手机号"
        view.addSubview(p)
        return p
    }()
    
    lazy var tips2: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = UIColor(red: 145/255.0, green: 145/255.0, blue: 145/255.0, alpha: 1.0)
        p.textAlignment = NSTextAlignment.left
        p.font = UIFont.systemFont(ofSize: 14)
        p.text = "更换手机号后，下次登录可使用新的手机号登录"
        view.addSubview(p)
        return p
    }()
    
    lazy var tips3: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = UIColor(red: 145/255.0, green: 145/255.0, blue: 145/255.0, alpha: 1.0)
        p.textAlignment = NSTextAlignment.left
        p.font = UIFont.systemFont(ofSize: 14)
        p.text = "当前手机号：\(User.default.mobile)"
        view.addSubview(p)
        return p
    }()
    
    lazy var phoneTextField:QMUITextField = {
        let v = makeStyleTF(image: "iphone", placeholder: "请输入手机号", rightBtn: false)
        v.maximumTextLength = 11
        v.keyboardType = .numberPad
        view.addSubview(v)
        return v
    }()
    
    lazy var passwordTextField:QMUITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请输入短信验证码", rightBtn: true)
        v.maximumTextLength = 6
        v.keyboardType = .numberPad
        view.addSubview(v)
        return v
    }()
    
    // 短信验证码按钮
    lazy var smsButton: QMUIButton = {
        let b = QMUIButton(type: .custom)
        b.size = CGSize(width: 80*kScreenScale, height: 25*kScreenScale)
        b.setTitle("获取短信验证码", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        b.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        b.addTarget(self, action: #selector(getSmsCode(_:)), for: .touchUpInside)
        b.layer.cornerRadius = 5.0
        return b
    }()
    
    lazy var submit: QMUIButton = {
        let s = QMUIButton("提交", fontSize: 15.0)
        s.setTitleColor(.white, for: .normal)
        s.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        s.addTarget(self, action: #selector(handlerSubmit(_:)), for: .touchUpInside)
        view.addSubview(s)
        return s
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "重置手机号"
        self.view.backgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
        createUI()
        
        phoneTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.stopTimer()
    }
    
    
    func createUI() {
        tips1.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.width.equalTo(200)
            make.height.equalTo(20)
            make.top.equalTo(kNaviBarHeight+20)
        }
        
        tips2.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo(20)
            make.top.equalTo(tips1.snp.bottom).offset(10.0)
        }
        
        tips3.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo(20)
            make.top.equalTo(tips2.snp.bottom).offset(10.0)
        }
        
        phoneTextField.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
            make.top.equalTo(tips3.snp.bottom).offset(50)
        }
        
        passwordTextField.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
            make.top.equalTo(phoneTextField.snp.bottom).offset(5)
        }
        
        submit.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(40)
            make.right.equalTo(self.view.snp.right).offset(-40)
            make.top.equalTo(passwordTextField.snp.bottom).offset(25)
        }
    }
}


extension FECenterSetNewPhoneViewController: QMUITextFieldDelegate {
    @discardableResult
    private func makeStyleTF(_ textField: QMUITextField? = nil, image: String, placeholder: String, rightBtn: Bool) -> QMUITextField {
        let imageView = UIImageView()
        imageView.image = UIImage(named: image)
        imageView.size = CGSize(width: 20, height: 20)
        
        let textfield = textField ?? QMUITextField(placeholder, font: UIFont.systemFont(ofSize: 15), color: Color.text)
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
        if rightBtn {
            let view = UIView()
            view.size = self.smsButton.size
            view.addSubview(self.smsButton)
            textfield.rightView = view
            textfield.rightViewMode = .always
        }
        
        textfield.font = UIFont.systemFont(ofSize: 15)
        textfield.placeholder = placeholder
        
        textfield.returnKeyType = .next
        textfield.delegate = self
        textfield.qmui_borderPosition = .bottom
        textfield.qmui_borderColor = .darkGray
        textfield.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        return textfield
    }
    
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
}

//MARK: 按钮事件
extension FECenterSetNewPhoneViewController {
    // 获取验证码
    @objc func getSmsCode(_ btn: QMUIButton) {
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.GetSMSCode(phone: phone){[weak self](status,text) in
            load.hide(animated: true)
            if status {
                btn.isEnabled = false
                btn.backgroundColor = .lightGray
                self?.setTimer()
            }
            QMUITips.show(withText: text)
        }
    }
    
    // 提交
    @objc func handlerSubmit(_ btn: QMUIButton) {
        if fund_pwd == "" {
            return
        }
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        guard let sms = passwordTextField.text, sms.count == 6 else {
            QMUITips.show(withText: "验证码有误", in: self.view)
            return
        }
        
        var item = ChangePhoneParam()
        item.fund_password = fund_pwd
        item.mobile = phone
        item.verify_code = sms
        
        let load = QMUITips.showLoading(in: self.view)
        UserCenter.default.changePhone(item) {[weak self] (status, msg) in
            load.hide(animated: true)
            if status {
                if let h = self?.completionHandler {
                    h((self?.passwordTextField.text!)!)
                }
                
                if (self?.shouldPopViewController(byBackButtonOrPopGesture: false))! {
                    self?.close()
                }
            }
            QMUITips.show(withText: msg)
        }
    }
}

//MARK: 定时器
extension FECenterSetNewPhoneViewController {
    func setTimer() {
        self.stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(dowithTimer(_:)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        if let t = timer {
            t.invalidate()
            timer = nil
        }
    }
    
    @objc func dowithTimer(_ t: Timer) {
        count -= 1
        if count == 0 {
            smsButton.isEnabled = true
            smsButton.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
            self.stopTimer()
        } else {
            smsButton.setTitle("重新获取(\(count))", for: .disabled)
        }
    }
}
