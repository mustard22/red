//
//  FECenterShareMakeMoneyViewController.swift
//  Red
//
//  Created by MAC on 2019/10/15.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import YSQRCodeGenerator

/// 推广赚钱界面
class FECenterShareMakeMoneyViewController: FEBaseViewController {
    
    let backImageView: UIImageView = {
       let iv = UIImageView()
        iv.image = UIImage(named: "shareMakeMoneyBack")
        return iv
    }()
    
    let icon: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "shareMakeMoneyIcon")
        return iv
    }()
    
    let qrcodeImage: UIImageView = {
        let q = UIImageView()
        return q
    }()
    
    let invite: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = UIColor(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        p.textAlignment = NSTextAlignment.center
        p.textColor = .white
        p.font = UIFont.boldSystemFont(ofSize: 20)
        return p
    }()
    
    lazy var shareButton: QMUIButton = {
        let v = QMUIButton("分享", fontSize: 14)
        v.setTitleColor(.white, for: .normal)
        v.addTarget(self, action: #selector(handlerShare), for: .touchUpInside)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "推广赚钱"
    
        createUI()
        // Do any additional setup after loading the view.
        
        invite.text = "邀请码：\(User.default.invite_code)"
        DispatchQueue.main.async {[weak self] in
            self?.qrcodeImage.image = self?.createQRCode()
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.shareButton)
        // 注册监听
        self.registerAllObserver()
    }
    
    deinit {
        self.removeAllObserver()
    }
    
    private var sendUrl: String = User.default.download_url + "&version=\(UIApplication.appVersion)"
    
    /// 创建二维码
    func createQRCode()-> UIImage? {
        let qr = YSQRCodeGenerator.init()
        qr.content = sendUrl
        qr.setColorWithBack(.white, foregroundColor: .black)
        let img = qr.generate()
        return img
    }
    
    func createUI() {
        backImageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        icon.frame = CGRect(x: (self.view.frame.width-180)/2, y: 100, width: 180, height: 240)
        self.view.addSubview(backImageView)
        backImageView.addSubview(icon)
        backImageView.addSubview(qrcodeImage)
        backImageView.addSubview(invite)
        
        backImageView.snp.makeConstraints { (make) in
            make.center.equalTo(self.view.snp.center)
            make.size.equalTo(self.view.snp.size)
        }
        
        icon.snp.makeConstraints { (make) in
                make.centerX.equalTo(self.view.snp.centerX)
                make.top.equalTo(100)
                make.width.equalTo(180)
                make.height.equalTo(240)
        }
        
        invite.snp.makeConstraints { (make) in
            make.centerX.equalTo(backImageView.snp.centerX)
            make.bottom.equalTo(-30 + (kIsIphoneX ? -34 : 0))
            make.width.equalTo(backImageView.snp.width)
            make.height.equalTo(40)
        }
        
        qrcodeImage.snp.makeConstraints { (make) in
            make.centerX.equalTo(backImageView.snp.centerX)
            make.bottom.equalTo(invite.snp.top).offset(-30)
            make.width.equalTo(180)
            make.height.equalTo(180)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
}

//MARK: 通知监听 & 事件处理
extension FECenterShareMakeMoneyViewController {
    func registerAllObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(weixinShareResult(_:)), name: .weixinShareResult, object: nil)
    }
    
    func removeAllObserver() {
        NotificationCenter.default.removeObserver(self, name: .weixinShareResult, object: nil)
    }
    
    @objc func weixinShareResult(_ n: Notification) {
        QMUITips.show(withText: "分享成功", in: self.view)
    }
    
    @objc func handlerShare() {
        let sm = AppShare(text: "拼手气", image: UIImage(named: "AppIcon")?.jpegData(compressionQuality: 0.5), url: sendUrl)
        let share = ShareView(shareModel: sm)
        share.show()
    }
}
