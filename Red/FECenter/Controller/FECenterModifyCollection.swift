//
//  FECenterModifyCollection.swift
//  Red
//
//  Created by MAC on 2019/10/15.
//  Copyright © 2019 MAC. All rights reserved.
//

/// 修改用户信息界面
import UIKit
import QMUIKit

class CenterModifyCollection: YYListViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate {
    
    let titles = ["头像", "昵称", "性别", "手机号", "二维码"]
    let sex = [("0","保密"), ("1", "男"), ("2", "女")]
    
    var updateData: [(String, String)] = [("avatar",User.default.avatar), ("nickname",User.default.nickname), ("gender","\(User.default.gender)")]
    
    var newIconImage: UIImage?
    
    override func didInitialize() {
        super.didInitialize()
        self.title = "个人信息"
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.RGBColor(240, 240, 240)
        sections = [
            YYListSection(title:"",items:[
                YYListItem(title: titles[0], imageCorner: 0, action:.image, value:URL(string: "\(User.default.avatar)")),
                YYListItem(title: titles[1], value:"\(User.default.nickname)"),
                YYListItem(title: titles[2], value:"\(User.default.gender)" == "0" ? "保密" : ("\(User.default.gender)" == "1" ? "男" : "女")),
                YYListItem(title: titles[3], value:"\(User.default.mobile)"),
                YYListItem(title: titles[4], imageCorner: 0, action:.image, value:UIImage.init(named: "er"))])
        ]
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let footer = UIView()
        footer.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 90)
        self.tableView.tableFooterView = footer
        
        let subbtn = QMUIButton("提交", fontSize: 15)
        subbtn.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        subbtn.setTitleColor(.white, for: .normal)
        subbtn.addTarget(self, action: #selector(handlerSubmit(_:)), for: .touchUpInside)
        footer.addSubview(subbtn)
        
        subbtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(footer.snp.centerY)
            make.left.equalTo(50)
            make.right.equalTo(footer.snp.right).offset(-50)
            make.height.equalTo(40)
        }
    }
    
    // 处理
    override func didSelectetItem(_ item: YYListItem) {
        switch item.title {
        case titles[0]:
            handlerModifyAvatar()
            
        case titles[1]:
            handlerModifyNickname()
            
        case titles[2]:
            handlerModifySex()
            
        case titles[3]:
            handlerUpdatePhoneNumber(item)
            
        case titles[4]:
            handlerUserCard()
        
        default:
            QMUITips.show(withText: "\(item.title)", in: self.view)
        }
    }
}

//MARK: 点击事件处理
extension CenterModifyCollection {
    /// 提交事件
    @objc func handlerSubmit(_ btn: QMUIButton) {
        if let newimg = newIconImage {
            // 上传头像
            var item = UploadParam.init()
            item.file = newimg.jpegData(compressionQuality: 0.8)
            UserCenter.default.uploadFile(item) {[weak self] (item) in
                guard let self = self else {
                    return
                }
                
                if let item = item  {
                    self.updateData[0].1 = item.path
                    self.submitUpdateInfo()
                } else {
                    QMUITips.show(withText: "上传头像失败", in: self.view)
                }
            }
        } else {
            self.submitUpdateInfo()
        }
    }
    
    //MARK: 提交修改信息
    func submitUpdateInfo() {
        
        if updateData[0].1 == User.default.avatar, updateData[1].1 == User.default.nickname, updateData[2].1 == "\(User.default.gender)" {
//            dprint("update nothing")
            return
        }
        
        if updateData[0].1 == User.default.avatar {
            // 截取.com后面的给后台
            updateData[0].1 = updateData[0].1.components(separatedBy: ".com").last ?? User.default.avatar
        }
        
        let loagingView = QMUITips.showLoading(in: self.view)
        var item = UpdateInfoParam()
        item.avatar = updateData[0].1
        item.nickname = updateData[1].1
        item.gender = updateData[2].1
        
        UserCenter.default.updateUserInfo(item) {[weak self] (status, text) in
            loagingView.hide(animated: true)
            if status {
                // 更新成功 修改本地
                self?.close()
            } else {
                QMUITips.show(withText: text)
            }
        }
    }
    
    /// 换头像
    func handlerModifyAvatar() {
        FESelectPhotoManager.selectPhoto(with: self) {[weak self] (imgs) in
            guard let self = self, let image = imgs.first else {
                return
            }
            let i = FESelectPhotoManager.scaleToSize(image: image, size: CGSize(width: 60, height: 60))
            self.refreshIcon(i)
        }
    }
    
    /// 改昵称
    func handlerModifyNickname() {
        let actionsheet = QMUIAlertController(title: "输入新昵称", message: nil, preferredStyle: .alert)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.center
        actionsheet.alertTitleAttributes = [.font: UIFont.systemFont(ofSize: 18),
                                      .foregroundColor: UIColor.black,
                                      .paragraphStyle: paragraph]
        
        actionsheet.addTextField { (tf) in
            tf.placeholder = "请输入新昵称"
            tf.font = UIFont.systemFont(ofSize: 14)
        }
        
        let a1 = QMUIAlertAction(title: "确定", style: .default) {[weak self] (ac, action) in
            guard let self = self else {
                return
            }
            
            if let tfs = ac.textFields {
                self.refreshNickname(tfs[0].text!)
            }
        }
        let a2 = QMUIAlertAction(title: "取消", style: .cancel) {(ac, action) in
        }
        
        actionsheet.addAction(a1)
        actionsheet.addAction(a2)
        
        actionsheet.showWith(animated: true)
    }
    
    //MARK: 改性别
    func handlerModifySex() {
        let asheet = UIAlertController(title: "选择性别", message: nil, preferredStyle: .actionSheet)
        let a1 = UIAlertAction(title: sex[0].1, style: .default) {[weak self] (action) in
            guard let self = self else {
                return
            }
            self.refreshSex(self.sex[0].1)
        }
        let a2 = UIAlertAction(title: sex[1].1, style: .default) {[weak self] (action) in
            guard let self = self else {
                return
            }
            self.refreshSex(self.sex[1].1)
        }
        let a3 = UIAlertAction(title: sex[2].1, style: .default) {[weak self] (action) in
            guard let self = self else {
                return
            }
            self.refreshSex(self.sex[2].1)
        }
        
        let no = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        asheet.addAction(a1)
        asheet.addAction(a2)
        asheet.addAction(a3)
        asheet.addAction(no)
        self.present(asheet, animated: true, completion: nil)
    }
    
    /// 进入换手机号页面
    func handlerUpdatePhoneNumber(_ item: YYListItem) {
        let vc = FECenterUpdatePhoneViewController()
        vc.phoneNumber = item.value as! String
        self.push(to: vc)
    }
    
    /// 进入二维码页面(个人名片)
    func handlerUserCard() {
        let vc = FECenterUserCardViewController()
        self.push(to: vc)
    }
}


//MARK: 修改后刷新本地列表
extension CenterModifyCollection {
    // 刷新本地列表头像
    func refreshIcon(_ img: UIImage) {
        self.newIconImage = img
        let item = YYListItem(title: titles[0], imageCorner:0, action:.image, value:img)
        sections[0].items[0] = item
        self.tableView.reloadData()
    }
    
    // 刷新昵称
    func refreshNickname(_ name: String) {
        guard name.count>0 else {
            QMUITips.show(withText: "昵称不能为空", in: self.view)
            return
        }
        let item = YYListItem(title: titles[1], value: name)
        sections[0].items[1] = item
        self.tableView.reloadData()
        
        updateData[1].1 = name
    }
    
    // 刷新性别
    func refreshSex(_ sex: String) {
        let item = YYListItem(title: titles[2], value: sex)
        sections[0].items[2] = item
        self.tableView.reloadData()
        
        for item in self.sex {
            if item.1 == sex {
                updateData[2].1 = item.0
                break
            }
        }
    }
}
