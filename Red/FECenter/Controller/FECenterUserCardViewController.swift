//
//  FECenterUserCardViewController.swift
//  Red
//
//  Created by MAC on 2019/10/16.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import YSQRCodeGenerator

/// 个人名片界面
class FECenterUserCardViewController: FEBaseViewController {
    
    let qrcodeSize = CGSize(width: 200, height: 200)
    
    let contentView: UIView = {
        let i = UIView(frame: .zero)
        i.backgroundColor = .white
        return i
    }()
    /// 二维码
    let qrcodeImage: UIImageView = {
       let q = UIImageView(frame: .zero)
        return q
    }()
    /// 头像
    let icon: UIImageView = {
        let i = UIImageView()
        return i
    }()
    /// 昵称
    let nick: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = .black
        p.textAlignment = NSTextAlignment.left
        p.font = UIFont.systemFont(ofSize: 14)
        return p
    }()
    /// 邀请码
    let invite: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = UIColor(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        p.textAlignment = NSTextAlignment.center
        p.font = UIFont.systemFont(ofSize: 15)
        return p
    }()
    /// 底部提示
    let tips: QMUILabel = {
        let p = QMUILabel(frame: .zero)
        p.textColor = UIColor(red: 152/255.0, green: 152/255.0, blue: 152/255.0, alpha: 1.0)
        p.textAlignment = NSTextAlignment.center
        p.font = UIFont.systemFont(ofSize: 15)
        p.text = "扫一扫上面的二维码，快来一起领红包"
        return p
    }()

    
    private var sendUrl: String = User.default.download_url + "&version=\(UIApplication.appVersion)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "个人名片"
        self.view.backgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
        
        createUI()
        
        icon.setImage(with: URL(string: "\(User.default.avatar)"))
            
        nick.text = User.default.nickname
        invite.text = "邀请码：\(User.default.invite_code)"
        
        DispatchQueue.main.async {[weak self] in
            self?.qrcodeImage.image = self?.createQRCode()
        }
    }
    
    /// 创建二维码
    func createQRCode()-> UIImage? {
        let qr = YSQRCodeGenerator.init()
        qr.content = sendUrl
//        qr.icon = UIImage(named: "ic_contact_custom")
//        qr.iconSize = CGSize(width: 30, height: 30)
        qr.setColorWithBack(.white, foregroundColor: .black)
        let img = qr.generate()
        return img
    }
    
    func createUI() {
        self.view.addSubview(contentView)
        contentView.addSubview(icon)
        contentView.addSubview(nick)
        contentView.addSubview(qrcodeImage)
        contentView.addSubview(invite)
        contentView.addSubview(tips)
        
        contentView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
            make.top.equalTo(kNaviBarHeight+20)
            make.bottom.equalTo(self.view.snp.bottom).offset(-20 + (kIsIphoneX ? -20 : 0))
        }
        icon.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(20)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }
        nick.snp.makeConstraints { (make) in
            make.left.equalTo(icon.snp.right).offset(10)
            make.centerY.equalTo(icon.snp.centerY)
            make.size.equalTo(CGSize(width: 150, height: 20))
        }
        qrcodeImage.snp.makeConstraints {(make) in
            make.size.equalTo(qrcodeSize)
            make.center.equalTo(contentView.snp.center)
        }
        tips.snp.makeConstraints { (make) in
            make.width.equalTo(contentView.snp.width)
            make.height.equalTo(20)
            make.centerX.equalTo(contentView.snp.centerX)
            make.bottom.equalTo(contentView.snp.bottom).offset(-10)
        }
        invite.snp.makeConstraints { (make) in
            make.width.equalTo(200)
            make.height.equalTo(20)
            make.centerX.equalTo(contentView.snp.centerX)
            make.bottom.equalTo(tips.snp.top).offset(-5)
        }
    }
}

