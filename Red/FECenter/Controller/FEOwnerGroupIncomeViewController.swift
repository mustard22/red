//
//  FEOwnerGroupIncomeViewController.swift
//  Red
//
//  Created by MAC on 2020/3/10.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 自建群收益界面
class FEOwnerGroupIncomeViewController: YYTableViewController {
    private lazy var formatter: DateFormatter = {
        let d = DateFormatter()
        d.dateFormat = "yyyy-MM-dd"
        return d
    }()
    
    private var topdata = OwnerGroupIncomeTopData()
    
    private var typeIndex = 0
    private let groupTypeData = [("全部", "0"), ("牛牛房", "1"), ("普通房", "2"), ("单雷房", "3"), ("多雷房", "4"), ("接龙房", "5")]
    
    private var startTime = ""
    private var endTime = ""
    
    private var datas: [OwnerGroupIncomeListItem] = []
    
    lazy var topview: OwnerGroupIncomeHeadView = {
        let v = OwnerGroupIncomeHeadView(frame: .zero)
        v.update(start: self.formatter.string(from: Date().offsetOfDay(-1)), end: self.formatter.string(from: Date()))
        self.view.addSubview(v)
        
        v.buttonActionHandler = {[weak self] (action) in
            self?.topviewButtonAction(action: action)
        }
        
        return v
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "自建群收益"
        // Do any additional setup after loading the view.
        
        self.loadTopData()
        
        self.loadListData()
    }
    
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        self.registerAllCells()
        tableView.reloadData()
    }
    
    private func registerAllCells() {
        self.tableView.registerCell(withClass: FEOwnerGroupIncomeListCell.self)
    }
}

//MARK: 事件处理
extension FEOwnerGroupIncomeViewController {
    private func topviewButtonAction(action: Int) {
        if action == 0 {
            // start time
            let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
                guard let self = self else {
                    return
                }
                
                if hasChanged == false {
                    return
                }
                
                self.startTime = time
                self.topview.update(start: time)
                self.mj_head.beginRefreshing()
            }
            d.datePicker.maximumDate = Date().offsetOfDay(-1)
            d.show()
        } else if action == 1 {
            // end time
            let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
                guard let self = self else {
                    return
                }
                if hasChanged == false {
                    return
                }
                
                self.endTime = time
                self.topview.update(end: time)
                self.mj_head.beginRefreshing()
            }
            d.show()
        } else {
            // group type
            let l =  FECenterLayerTableView.instance(groupTypeData.map({$0.0}), self.typeIndex) {[weak self] (index) in
                guard let self = self else {
                    return
                }
                
                if index == self.typeIndex {
                    return
                }
                
                self.typeIndex = index
                
                self.topview.update(typeName: self.groupTypeData[index].0)
                
                self.mj_head.beginRefreshing()
            }
            l.show()
        }
    }
}

//MARK: 网络请求
extension FEOwnerGroupIncomeViewController {
    private func loadTopData() {
        HttpClient.default.ownerGroupIncomeTopData{(item) in
            self.topdata = item
            self.topview.update(flow: item.self_room_flow, income: item.self_room_profit)
        }
    }
    
    private func getRequstParam(_ isMore: Bool) -> OwnerGroupIncomeParam {
        var p = OwnerGroupIncomeParam()
        p.type = self.groupTypeData[self.typeIndex].1
        
        if self.startTime.isEmpty {
            self.startTime = self.topview.startButton.currentTitle ?? ""
        }
        p.start_time = self.startTime
        
        if self.endTime.isEmpty {
            self.endTime = self.topview.endButton.currentTitle ?? ""
        }
        p.end_time = self.endTime
        
        p.pageSize = "\(self.loadCount)"
        
        var index = 1
        if isMore {
            index = self.datas.count / self.loadCount + 1
        }
        p.pageIndex = "\(index)"
        return p
    }
    
    @objc private func loadListData() {
        let p = self.getRequstParam(false)
        HttpClient.default.ownerGroupIncomeList(param: p) {[weak self] (items) in
            guard let self = self else {
                return
            }
            self.mj_head.endRefreshing()
            
            self.datas.removeAll()
            self.datas.append(contentsOf: items)
            self.tableView.reloadData()
            
            if items.count >= self.loadCount {
                self.tableView.mj_footer = self.mj_foot
            } else {
                self.tableView.mj_footer = nil
            }
        }
    }
    
    @objc private func loadMoreListData() {
        let p = self.getRequstParam(true)
        HttpClient.default.ownerGroupIncomeList(param: p) {[weak self] (items) in
            guard let self = self else {
                return
            }
            self.mj_foot.endRefreshing()
            
            self.datas.append(contentsOf: items)
            self.tableView.reloadData()
            
            if items.count >= self.loadCount {
                self.mj_foot.state = .idle
            } else {
                self.tableView.mj_footer = nil
            }
        }
    }
}



extension FEOwnerGroupIncomeViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.backgroundColor = UIColor.RGBColor(240, 240, 240)
        
        self.registerAllCells()
        
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
        
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreListData))
        self.tableView.mj_footer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.topview.top = kNaviBarHeight
        tableView.frame = CGRect(x: 0, y: self.topview.bottom, width: view.frame.size.width, height: view.frame.size.height - self.topview.bottom - (kIsIphoneX ? 20 : 0))
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FEOwnerGroupIncomeListCell.self, for: indexPath)
        cell.update(datas[indexPath.row])
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}



//MARK: 收益列表界面顶部view
class OwnerGroupIncomeHeadView: UIView {
    private lazy var backImage: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "bg_me")
        self.addSubview(v)
        return v
    }()
    
    private lazy var flowLabel: UILabel = {
        let v = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 50))
        v.text = "总流水：0"
        v.textColor = .white
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 15)
        self.addSubview(v)

        return v
    }()
    
    private lazy var incomeLabel: UILabel = {
        let v = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 50))
        v.text = "总收益：0"
        v.textColor = .white
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 15)
        self.addSubview(v)
        return v
    }()
    
    public lazy var startButton: UIButton = {
        let v = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        v.setTitle("2020-03-09", for: .normal)
        v.setTitleColor(.white, for: .normal)
//        v.titleLabel!.textAlignment = .left
//        v.contentHorizontalAlignment = .left
        v.titleLabel!.font = UIFont.systemFont(ofSize: 15)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    public lazy var endButton: UIButton = {
        let v = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        v.setTitle("2020-03-09", for: .normal)
        v.setTitleColor(.white, for: .normal)
        //        v.titleLabel!.textAlignment = .left
//        v.contentHorizontalAlignment = .left
        v.titleLabel!.font = UIFont.systemFont(ofSize: 15)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    private lazy var typeButton: UIButton = {
        let v = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        v.setTitle("全部", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.titleLabel!.font = UIFont.systemFont(ofSize: 15)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    private lazy var line: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 1, alpha: 0.3)
        self.addSubview(v)
        return v
    }()
    
    /// 点击按钮事件回调（0开始时间 1结束时间 2群类型）
    public var buttonActionHandler: ((_ actionType: Int)->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension OwnerGroupIncomeHeadView {
    @objc func buttonAction(_ sender: UIButton) {
        if sender == startButton {
            buttonActionHandler?(0)
        } else if sender == endButton {
            buttonActionHandler?(1)
        } else {
            buttonActionHandler?(2)
        }
    }
    
    public func update(flow: String? = nil, income: String? = nil, start: String? = nil, end: String? = nil, typeName: String? = nil) {
        if let f = flow {
            flowLabel.text = "总流水：\(f)"
        }
        if let i = income {
            incomeLabel.text = "总收益：\(i)"
        }
        if let s = start {
            startButton.setTitle(s, for: .normal)
        }
        if let e = end {
            endButton.setTitle(e, for: .normal)
        }
        if let t = typeName {
            typeButton.setTitle(t, for: .normal)
        }
    }
    
    private func setupUI() {
        var f = frame
        f.origin.x = 0
        f.origin.y = 0
        f.size.width = kScreenSize.width
        f.size.height = 100
        self.frame = f
//        self.backgroundColor = Color.red
//        self.layer.opacity = 0.8
        
        backImage.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(0)
            make.size.equalTo(self)
        }
        
        flowLabel.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(0)
            make.height.equalTo(50)
            make.right.equalTo(self.snp.centerX)
        }
        
        incomeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.centerX)
            make.top.equalTo(0)
            make.height.equalTo(50)
            make.right.equalTo(self)
        }
        
        line.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(flowLabel.snp.bottom)
            make.height.equalTo(1)
            make.right.equalTo(self).offset(-20)
        }
        
        startButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.bottom.equalTo(self)
            make.height.equalTo(50)
            make.width.equalTo(self.width/3)
        }
        endButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self)
            make.height.equalTo(50)
            make.width.equalTo(self.width/3)
        }
        typeButton.snp.makeConstraints { (make) in
            make.right.equalTo(self)
            make.bottom.equalTo(self)
            make.height.equalTo(50)
            make.width.equalTo(self.width/3)
        }
        
    }
}
