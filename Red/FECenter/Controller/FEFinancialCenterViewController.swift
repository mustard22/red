//
//  FEFinancialCenterViewController.swift
//  Red
//
//  Created by MAC on 2020/3/30.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 财务中心界面
class FEFinancialCenterViewController: YYTableViewController {
    public enum Action: String, CaseIterable {
        case rechargeAndWithdraw = "充提记录"
        case redPaper = "红包记录"
        case ownerIncome = "自建群收益"
        case moneyDetail = "零钱明细"
       
        public var icon: String {
            switch self {
            case .rechargeAndWithdraw:
                return "ic_report"
            case .redPaper, .ownerIncome:
                return "hong_bao"
            case .moneyDetail:
                return "ic_withdraw"
            }
        }
    }
    
    private lazy var datas: [[Action]] = {
        var d = [[Action]]()
        d.append([.rechargeAndWithdraw, .redPaper, .moneyDetail])
        
        if HomePageModel.showOwnerGroup {
            d.append([.ownerIncome])
        }
        
        return d
    }()
    
    private lazy var rankBar: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        
        let img = UIImageView()
        img.frame = CGRect(x: 10, y: (v.height-20)/2, width: 5, height: 20)
        img.backgroundColor = Color.red
        v.addSubview(img)
        
        let lab = UILabel()
        lab.frame = CGRect(x: img.right+10, y: (v.height-20)/2, width: 200, height: 20)
        lab.text = "本月推广佣金排行"
        lab.textColor = .darkGray
        lab.textAlignment = .left
        lab.font = UIFont.systemFont(ofSize: 16)
        v.addSubview(lab)
        return v
    }()
    
    //MARK: 滚动排行表
    private lazy var labelCycleView: FECycleLabelView = {
        let v: FECycleLabelView = FECycleLabelView(self.rankDatas, CGRect(x: 0, y: rankBar.bottom, width: self.view.frame.width, height: 75))
        v.backgroundColor = .white
        v.layer.masksToBounds = true
        return v
    }()
    
    private lazy var headView: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: view.width, height: rankBar.height + labelCycleView.height + 10)
        v.addSubview(rankBar)
        v.addSubview(labelCycleView)
        return v
    }()
    
    // 每次显示行数
    private let noticeCount = 3
    
    private lazy var rankDatas: [[String]] = {
        let notices = User.default.proxy_notice
        guard notices.count > 0 else {
            return []
        }
        
        var array: [[String]] = []
        var section: [String]?
        for (i,n) in notices.enumerated() {
            if i%noticeCount == 0 {
                if var s = section, s.count > 0 {
                    array.append(s)
                }
                section = []
                section!.append("\(n.title),\(n.content),\(n.draw_water)")
            } else {
                if var s = section {
                    section!.append("\(n.title),\(n.content),\(n.draw_water)")
                }
            }
        }
        
        if var s = section, s.count > 0 {
            array.append(s)
        }
        return array
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "财务中心"
    }
        
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        self.registerCells()
        self.tableView.reloadData()
    }
}

extension FEFinancialCenterViewController {
    private func registerCells() {
        self.tableView.registerCell(withClass: YYTableViewCell.self)
    }
}


// MARK: - QMUINavigationTitleViewDelegate
extension FEFinancialCenterViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        self.tableView.changeAllCellsSelectStatuse(active)
    }
}


extension FEFinancialCenterViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override func didInitialize(with style: UITableView.Style) {
        super.didInitialize(with: .grouped)
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.RGBColor(240, 240, 240)
        self.tableView.backgroundColor = UIColor.RGBColor(240, 240, 240)
        self.tableView.tableHeaderView = self.headView
        self.registerCells()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        let count = datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return datas.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section > 0 ? 10 : 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas[section].count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 * kScreenScale
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: YYTableViewCell.self, for: indexPath)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let item = datas[indexPath.section][indexPath.row]
        cell.textLabel?.text = item.rawValue
        cell.imageView?.image = UIImage(named: item.icon)?.drawImage(size: CGSize(width: 20, height: 20))
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let action = datas[indexPath.section][indexPath.row]
        switch action {
        case .rechargeAndWithdraw:
            let vc = FECenterChargeReportViewController()
            self.push(to: vc, animated: true, completion: nil)
        case .redPaper:
            let vc = FECenterRedPackageViewController()
            self.push(to: vc, animated: true, completion: nil)
        case .ownerIncome:
            let vc = FEOwnerGroupIncomeViewController()
            self.push(to: vc, animated: true, completion: nil)
        case .moneyDetail:
            let vc = FECenterMoneyDetailViewController()
            self.push(to: vc, animated: true, completion: nil)
        }
    }
}

