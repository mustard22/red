//
//  CenterSetCollection.swift
//  Red
//
//  Created by MAC on 2019/8/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import Kingfisher

/// 设置中心
class CenterSetCollection: YYListViewController {
    
    override func didInitialize() {
        super.didInitialize()
        self.title = "设置"
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.RGBColor(240, 240, 240)
        
        let open = UserDefaults.Current.paperVoiceSwitch // 默认打开
        
        let cellPrompt = YYListItem(title: "红包提示音",action: .switch, value: open)
        cellPrompt.valueChangedBLock = { newValue in
            
            UserDefaults.Current.paperVoiceSwitch = newValue as! Bool
            
            self.reload(for: "红包提示音")
        }
        
        sections = [
            YYListSection(title:"",items:[
                YYListItem(title: "登录密码"),
                YYListItem(title: "资金密码"),
                YYListItem(title: "银行卡"),
                YYListItem(title: "手机号",value:"\(User.default.mobile)"),
                YYListItem(title: "邮箱",value:"\(User.default.email)"),
                cellPrompt,
                YYListItem(title: "系统版本",action:.none,value:"\(UIApplication.appVersion)"),
                YYListItem(title: "检查新版本"),
                YYListItem(title: "清理缓存"),
            ]),
            YYListSection(title:"",items:[
                YYListItem(title: "退出登录", action: .none)
            ])
        ]
    }
    
    // 列表点击事件
    override func didSelectetItem(_ item: YYListItem) {
        switch item.title {
        case "登录密码":
            self.handlerLoginPasswd()
        case "资金密码":
            self.handlerMoneyPasswd()
        case "银行卡":
            self.handlerBankCard()
        case "手机号":
            self.handlerPhone(item)
        case "邮箱":
            self.handlerEmail(item)
        case "检查新版本":
            self.handlerCheckAppVersion()
        case "清理缓存":
            self.handlerClearCache()
        case "退出登录":
            self.handlerLogout()
        default:
            break
        }
    }
}

//MARK: 事件处理
extension CenterSetCollection {
    /// 退出登录
    func handlerLogout() {
        let alert = UIAlertController(title: "确定要退出？", message: nil, preferredStyle: .alert)
        let no = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let ok = UIAlertAction(title: "确定", style: .default) { (a) in
            UserCenter.default.logout { (isSuccess, message) in
                guard isSuccess else {
                    dprint("退出登录失败：\(message)")
                    QMUITips.show(withText: message)
                    return
                }
                // 发送退出登录通知
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.onForceOffline, object: nil)
                }
            }
        }
        alert.addAction(no)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    /// 清理缓存
    func handlerClearCache() {
        let load = QMUITips.showLoading("清理中...", in: self.view)
        DispatchQueue.global().async {
            // 聊天记录
            MMKVManager.shared.removeAllCache()
            // 网络请求
            HttpClient.default.httpCache.clearAll()
            
            // 清理图片缓存
            KingfisherManager.shared.cache.clearDiskCache()
            
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                load.hide(animated: true, afterDelay: 0.1)
            })
        }
    }
    
    /// 检查新版本
    func handlerCheckAppVersion() {
        if UIApplication.appVersion >= UserCenter.default.lbanner.system_ios_version {
            QMUITips.show(withText: "已经是最新版本了")
            return
        }
        
        let alert = UIAlertController(title: "检查新版本", message: "最新版本\(UserCenter.default.lbanner.system_ios_version)，是否立即更新？", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "忽略", style: .cancel, handler: nil)
        let update = UIAlertAction(title: "立即更新", style: .default) { (action) in
            guard let url = URL(string: UserCenter.default.lbanner.download_url) else {
                QMUITips.show(withText: "数据异常")
                return
            }
            guard UIApplication.shared.canOpenURL(url) else {
                QMUITips.show(withText: "链接无法打开")
                return
            }
            UIApplication.shared.open(url, options: [.universalLinksOnly: false], completionHandler: nil)
        }
        alert.addAction(cancel)
        alert.addAction(update)
        self.present(alert, animated: true, completion: nil)
    }
    
    /// 登录密码
    func handlerLoginPasswd() {
        let vc = FESetLoginPasswdViewController()
        self.push(to: vc)
    }
    
    /// 资金密码
    func handlerMoneyPasswd() {
        let vc = FESetFundPasswdViewController()
        self.push(to: vc)
    }
    
    /// 银行卡
    func handlerBankCard() {
        let vc = FECenterBankListViewController()
        self.push(to: vc)
    }
    
    /// 手机号
    func handlerPhone(_ item: YYListItem) {
        let vc = FECenterUpdatePhoneViewController()
        vc.phoneNumber = item.value as! String
        self.push(to: vc)
    }
    
    /// 邮箱
    func handlerEmail(_ item: YYListItem) {
        let vc = FECenterUpdateEmailViewController()
        vc.email = item.value as! String
        self.push(to: vc)
    }
}
