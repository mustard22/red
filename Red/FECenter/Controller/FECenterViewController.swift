//
//  FECenterViewController.swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit


class FECenterViewController2: YYTableViewController {
    private lazy var infoView: FEMineListHeadView = {
        return FEMineListHeadView()
    }()
    
    private lazy var headview: UIView = {
        let v = UIView()
        
        let infoView = self.infoView
        infoView.clickActionHandler = {[weak self] in
            self?.clickInfoView()
        }
        v.addSubview(infoView)
        
        let itemView = FECenterChargeHeadView("icon_item_proxy", "充值中心", "icon_item_fxzq", "推广赚钱", CGPoint(x: 0, y: infoView.bottom + 8))
        itemView.showSelectLayer = false
        itemView.clickHandler = {[weak self] (isleft) in
            self?.clickItemView(isleft)
        }
        v.addSubview(itemView)
        
        v.frame = CGRect(x: 0, y: 0, width: self.tableView.width, height: itemView.bottom + 8)
        return v
    }()
    
    // 签到按钮
    private lazy var rightTop: UIButton = {
        let v = UIButton()
        v.showsTouchWhenHighlighted = true
        v.size = CGSize(width: 40, height: 40)
        
        v.setImage(UIImage(named: "ic_sign"), for: .normal)
        v.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        v.addTarget(self, action: #selector(rightTopAction), for: .touchUpInside)
        return v
    }()
    
    @objc
    private func rightTopAction() {
        let vc = FECenterSignViewController()
        self.push(to: vc, animated: true, completion: nil)
    }
    
    private lazy var datas = FEMineListData.listDatas
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "我的"
        self.tableView.backgroundColor = UIColor(r: 240, g: 240, b: 240)
        self.tableView.tableHeaderView = headview
        
        if UserCenter.default.lbanner.user_sign_switch == "1" {
            // ==1时显示签到按钮
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTop)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserCenter.default.getSectionInfo {[weak self] (status, _) in
            self?.infoView.update()
        }
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        registerCells()
        tableView.reloadData()
    }
}

extension FECenterViewController2 {
    private func clickInfoView() {
        self.push(to: CenterModifyCollection())
    }
    
    private func clickItemView(_ left: Bool) {
        if left {
            self.push(to: FECenterChargeCenterViewController())
        } else {
            self.push(to: FECenterShareMakeMoneyViewController())
        }
    }
}

extension FECenterViewController2: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FECenterViewController2 {
    private func registerCells() {
        tableView.registerCell(withClass: FECenterListCollectionViewCell.self)
    }
    
    
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.registerCells()
    }
    
    override func didInitialize(with style: UITableView.Style) {
        super.didInitialize(with: .grouped)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.height = view.height - (self.tabBarController?.tabBar.height ?? 0)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return datas.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section > 0 ? 10 : 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas[section].count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50*kScreenScale
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FECenterListCollectionViewCell.self, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let model = datas[indexPath.section][indexPath.row]
        let cell0 = cell as! FECenterListCollectionViewCell
        cell0.iconImageViwe.image = UIImage(named: model.imageUrl)
        cell0.titleLable.text = model.title
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dowithCellSelectAction(indexPath)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y - (-kNaviBarHeight)
        if offset < 0 {
            scrollView.contentOffset = CGPoint(x: 0, y: -kNaviBarHeight)
        }
    }
}

//MARK: cell选中事件处理
extension FECenterViewController2 {
    private func dowithCellSelectAction(_ index: IndexPath) {
        if index.section == 0 {
            switch index.row {
            case 0:
                // 进入活动中心界面
                let vc = FEActivityViewController()
                self.push(to: vc, animated: true, completion: nil)
            case 1:
                // 进入代理中心界面
                let vc = FECenterAgencyController()
                self.push(to: vc, animated: true, completion: nil)
            case 2:
                // 进入财务中心界面
                let vc = FEFinancialCenterViewController()
                self.push(to: vc, animated: true, completion: nil)
            case 3:
                // 进入设置中心
                let vc = CenterSetCollection()
                self.push(to: vc, animated: true, completion: nil)
            default:break
            }
        } else {
            if index.row == 0 {
                // 联系客服
                ContactCustomer.startContact(currentvc: self, enterChat: true)
            }
        }
    }
}
