//
//  FECenterBankListViewController.swift
//  Red
//
//  Created by MAC on 2019/10/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class FECenterBankListViewController: YYTableViewController {
    private var datas = [BankCardItem]()
    
    public var selectBankCardHandler: ((_ Item: BankCardItem)->Void)?

    private lazy var addBtn: QMUIButton = {
        let b = QMUIButton("添加银行卡", fontSize: 18)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        b.addTarget(self, action: #selector(addBankAction), for: .touchUpInside)
        view.addSubview(b)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "添加银行卡"
        self.loadBankCardData()
    }
        
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        registerCells()
        tableView.reloadData()
    }
}

extension FECenterBankListViewController {
    private func registerCells() {
        tableView.registerCell(withClass: FECenterBankListCollectionCell.self)
    }
    
    /// 获取银行卡数据
    @objc func loadBankCardData() {
        UserCenter.default.getBankCardList {[weak self] (status,msg,items) in
            guard let self = self else {
                return
            }
            
            self.mj_head.endRefreshing()
            
            if let items = items {
                self.datas.removeAll()
                self.datas.append(contentsOf: items)
                self.tableView.reloadData()
            } else {
                QMUITips.show(withText: msg, in: self.view)
            }
        }
    }
    
    @objc func addBankAction() {
        let vc = FECenterAddBankCardViewController(style: .plain)
        vc.completionHandler = {[weak self] in
            self?.mj_head.beginRefreshing()
        }
        self.push(to: vc)
    }
}


// MARK: - QMUINavigationTitleViewDelegate
extension FECenterBankListViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}


extension FECenterBankListViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.registerCells()
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadBankCardData))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var rect = view.bounds
        let addButtonSize = CGSize(width: rect.size.width-50*2, height: 40)
        rect.size.height -= addButtonSize.height + 20 + (kIsIphoneX ? 20 : 0)
        tableView.frame = rect
        
        rect.origin.x = (rect.size.width - addButtonSize.width)/2
        rect.origin.y = tableView.bottom + 10
        rect.size = addButtonSize
        addBtn.frame = rect
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FECenterBankListCollectionCell.self, for: indexPath)
        cell.update(datas[indexPath.row])
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let handler = self.selectBankCardHandler {
            handler(datas[indexPath.row])
            self.close()
        }
    }
}

