//
//  FECenterProxyReportCollectionCell.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit
/// 代理报表cell
class FECenterProxyReportCollectionCell: YYTableViewCell {
    
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var rightBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var leftTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var midBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var leftBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var rightTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var timeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .right
        v.textColor = .gray
        self.backview.addSubview(v)
        return v
    }()
}

extension FECenterProxyReportCollectionCell {
    public static func cellHeight() -> CGFloat {
        return 150.0
    }
    
    public func update(_ item: ProxyReportListData.ProxyReportListItem) {
        // 默认样式
        leftTop.text = "推广总数\n\(item.agent_subordinate_total)"
        rightTop.text = "推广返佣\n\(item.agent_brokerage_total)"
        
        leftBottom.text = "抢包返水\n\(item.agent_packet_total)"
        midBottom.text = "中雷返水\n\(item.agent_mine_total)"
        rightBottom.text = "牛牛返水\n\(item.agent_niureturn_total)"
        timeLabel.text = "\(item.day)"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        backview.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView)
        }
        
        leftTop.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: contentView.width/3, height: 50))
        }
        
        rightTop.snp.makeConstraints { (make) in
            make.top.equalTo(leftTop)
            make.right.equalTo(backview)
            make.size.equalTo(leftTop)
        }
        
        leftBottom.snp.makeConstraints { (make) in
            make.left.equalTo(leftTop)
            make.top.equalTo(leftTop.snp.bottom)
            make.size.equalTo(leftTop)
        }
        midBottom.snp.makeConstraints { (make) in
            make.centerX.equalTo(backview)
            make.top.equalTo(leftBottom)
            make.size.equalTo(leftBottom)
        }
        rightBottom.snp.makeConstraints { (make) in
            make.right.equalTo(backview)
            make.top.equalTo(leftBottom)
            make.size.equalTo(leftBottom)
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.right.equalTo(backview).offset(-10)
            make.bottom.equalTo(contentView).offset(-5)
            make.size.equalTo(CGSize(width: 150, height: 20))
        }
    }
}



//MARK: -
/// 代理报表cell2
class FECenterProxyReportCollectionCell2: YYTableViewCell {
    
    lazy var backview: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var firstLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        v.adjustsFontSizeToFitWidth = true
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var secondLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        v.adjustsFontSizeToFitWidth = true
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var thirdLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        v.adjustsFontSizeToFitWidth = true
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var fourthLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.numberOfLines = 0
        v.textAlignment = .center
        v.adjustsFontSizeToFitWidth = true
        self.backview.addSubview(v)
        return v
    }()
    
    lazy var timeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .right
        v.textColor = .gray
        self.backview.addSubview(v)
        return v
    }()
}

extension FECenterProxyReportCollectionCell2 {
    public static func cellHeight() -> CGFloat {
        return 90.0
    }
    
    public func update(_ item: ProxyReportListData.ProxyReportListItem) {
            
            firstLabel.text = "直营流水\n\(item.day_turnover)"
            secondLabel.text = "下级流水\n\(item.sub_day_turnover)"
            
            thirdLabel.text = "直营收益\n\(item.direct_income)"
            fourthLabel.text = "下级佣金\n\(item.sub_commission)"
            timeLabel.text = "\(item.day)"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        backview.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView)
            make.top.equalTo(10)
            make.bottom.equalTo(contentView)
        }
        
        firstLabel.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: contentView.width/4, height: 50))
        }
        secondLabel.snp.makeConstraints { (make) in
            make.left.equalTo(firstLabel.snp.right)
            make.top.equalTo(firstLabel)
            make.size.equalTo(firstLabel)
        }
        thirdLabel.snp.makeConstraints { (make) in
            make.left.equalTo(secondLabel.snp.right)
            make.top.equalTo(firstLabel)
            make.size.equalTo(firstLabel)
        }
        fourthLabel.snp.makeConstraints { (make) in
            make.left.equalTo(thirdLabel.snp.right)
            make.top.equalTo(firstLabel)
            make.size.equalTo(firstLabel)
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.right.equalTo(backview).offset(-10)
            make.bottom.equalTo(contentView)
            make.size.equalTo(CGSize(width: 150, height: 30))
        }
    }
}

