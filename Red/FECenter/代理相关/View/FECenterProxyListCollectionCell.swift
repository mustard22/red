//
//  FECenterProxyListCollectionCell.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

/// 下级列表cell
class FECenterProxyListCollectionCell: YYTableViewCell {
    lazy var backView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var icon: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.lightText
        backView.addSubview(v)
        return v
    }()
    
    lazy var phone: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        backView.addSubview(v)
        return v
    }()
    
    lazy var sex: UIImageView = {
        let v = UIImageView()
        backView.addSubview(v)
        return v
    }()
    
    lazy var midLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        backView.addSubview(v)
        return v
    }()
    
    lazy var leftBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .center
        v.numberOfLines = 0
        backView.addSubview(v)
        return v
    }()
    
    lazy var midBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .center
        v.numberOfLines = 0
        backView.addSubview(v)
        return v
    }()
    
    lazy var rightBottom: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .center
        v.numberOfLines = 0
        backView.addSubview(v)
        return v
    }()
}

extension FECenterProxyListCollectionCell {
    public func update(_ item: ProxyListChildData.ProxyChildItem.ChildItem) {
        icon.setImage(with: URL(string: item.avatar))
        phone.text = item.nickname
        if item.gender == "0" {
            sex.isHidden = true
        } else {
            sex.isHidden = false
            sex.image = UIImage(named: item.gender == "1" ? "nan" : "nv")
        }
        
        midLabel.text = "\(item.user_id)"
        
        leftBottom.text = "\(item.level)\n用户级别"
        midBottom.text = "\(item.project_water)\n红包流水"
        
        let attr = NSMutableAttributedString(string: "\(item.user_profit)\n贡献返水")
        let value = Double(item.user_profit)!
        let color = (value > 0.0 ? Color.green : (value < 0.0 ? Color.red : UIColor.black))
        attr.addAttributes([.foregroundColor: color], range: NSRange(location: 0, length: item.user_profit.count))
        rightBottom.attributedText = attr
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        backView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.top.equalTo(5)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        
        icon.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 45, height: 45))
        }
        
        phone.snp.makeConstraints { (make) in
            make.left.equalTo(icon.snp.right).offset(10)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        
        sex.snp.makeConstraints { (make) in
            make.left.equalTo(phone.snp.right).offset(10)
            make.centerY.equalTo(phone.snp.centerY)
            make.size.equalTo(CGSize(width: 8, height: 8))
        }
        
        midLabel.snp.makeConstraints { (make) in
            make.left.equalTo(icon.snp.right).offset(10)
            make.bottom.equalTo(icon.snp.bottom)
            make.size.equalTo(CGSize(width: 150, height: 20))
        }
        
        leftBottom.snp.makeConstraints { (make) in
            make.left.equalTo(icon.snp.left)
            make.bottom.equalTo(backView.snp.bottom).offset(-10)
            make.size.equalTo(CGSize(width: 90, height: 40))
        }
        
        midBottom.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView.snp.centerX)
            make.bottom.equalTo(backView.snp.bottom).offset(-10)
            make.size.equalTo(CGSize(width: 90, height: 40))
        }
        
        rightBottom.snp.makeConstraints { (make) in
            make.right.equalTo(backView.snp.right).offset(-icon.left)
            make.bottom.equalTo(backView.snp.bottom).offset(-10)
            make.size.equalTo(CGSize(width: 90, height: 40))
        }
    }
}
