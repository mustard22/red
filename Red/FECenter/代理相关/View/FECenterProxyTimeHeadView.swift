//
//  FECenterProxyTimeHeadView.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class FECenterProxyTimeHeadView: UIView {
    
    lazy var leftBtn: QMUIButton = {
        let btn = QMUIButton("开始时间",image:UIImage(named: "ic_bill_start"),fontSize: 12)
        btn.imagePosition = .top
        btn.setTitleColor(Color.text, for: .normal)
        btn.spacingBetweenImageAndTitle = 5.0
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 20, right: 50)
        btn.isUserInteractionEnabled = true
        btn.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.addSubview(btn)
        return btn
    }()
    
    lazy var vlineLayer: CALayer = {
        let l = CALayer()
        l.frame = CGRect(x: self.width/2, y: 0, width: 1.0, height: self.height)
        l.backgroundColor = UIColor.RGBColor(248, 248, 248).cgColor
        self.layer.addSublayer(l)
        return l
    }()
    lazy var hlineLayer: CALayer = {
        let l = CALayer()
        l.frame = CGRect(x: 0, y: 0, width: self.width, height: 1)
        l.backgroundColor = UIColor.RGBColor(248, 248, 248).cgColor
        self.layer.addSublayer(l)
        return l
    }()
    
    lazy var rightBtn: QMUIButton = {
        let btn = QMUIButton("结束时间",image:UIImage(named: "ic_bill_stop"),fontSize: 12)
        btn.imagePosition = .top
        btn.setTitleColor(Color.text, for: .normal)
        btn.spacingBetweenImageAndTitle = 5.0
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 20, right: 50)
        btn.isUserInteractionEnabled = true
        btn.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.addSubview(btn)
        return btn
    }()
    
    private var formatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "yyyy-MM-dd"
        return f
    }()
    
    var startTime: String = ""
    var endTime: String = ""
    
    var clickHandler:((_ sender: QMUIButton)->Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        leftBtn.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.centerY.equalTo(self.snp.centerY)
            make.size.equalTo(CGSize(width: 150, height: 70))
        }
        
        rightBtn.snp.makeConstraints { (make) in
            make.right.equalTo(self.snp.right).offset(-20)
            make.centerY.equalTo(self.snp.centerY)
            make.size.equalTo(CGSize(width: 150, height: 70))
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
        vlineLayer.isHidden = false
        hlineLayer.isHidden = false
        
        freshStartTime(formatter.string(from: Date().offsetOfDay(-1)))
        freshEndTime(formatter.string(from: Date()))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func freshStartTime(_ time: String) {
        startTime = time
        leftBtn.setTitle("开始时间：\(time)", for: .normal)
    }
    
    func freshEndTime(_ time: String) {
        endTime = time
        rightBtn.setTitle("结束时间：\(time)", for: .normal)
    }
}


extension FECenterProxyTimeHeadView {
    @objc func handlerButtonAction(_ btn: QMUIButton) {
        clickHandler?(btn)
    }
}


extension Date {
    // 返回对应偏移量的日期
    func offsetOfDay(_ offset: Int) -> Date {
        var com = DateComponents()
        com.day = offset
        let calender = Calendar(identifier: .gregorian)
        return calender.date(byAdding: com, to: Date()) ?? Date()
    }
}
