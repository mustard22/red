//
//  FECenterProxyListHeadView.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 下级列表headview
class FECenterProxyListHeadView: UIImageView {
//    private lazy var backImage: UIImageView = {
//        let v = UIImageView()
//        v.image = UIImage(named: "bg_me")
//        self.addSubview(v)
//        return v
//    }()
    
    lazy var leftTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "团队玩家：0人"
        v.textColor = .white
        self.addSubview(v)
        return v
    }()
    lazy var rightTop: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "直推成员：0人"
        v.textColor = .white
        self.addSubview(v)
        return v
    }()
    
    lazy var leftMid: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "用户昵称"
        v.textColor = .white
        self.addSubview(v)
        return v
    }()
    
    lazy var rightMid: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.text = "用户等级"
        v.textColor = .white
        self.addSubview(v)
        return v
    }()
    
    lazy var leftBottom: QMUITextField = {
        let v = QMUITextField()
        v.font = UIFont.systemFont(ofSize: 14)
        v.autocapitalizationType = .none
        v.placeholder = "点击输入"
        v.delegate = self
        v.layer.cornerRadius = 3
        v.backgroundColor = .white
        v.returnKeyType = .search
        v.tintColor = Color.red
        self.addSubview(v)
        return v
    }()
    
    lazy var midBottom: QMUIButton = {
        let v = QMUIButton()
        v.setTitle("全部", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        v.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    // search
    lazy var rightBottom: QMUIButton = {
        let v = QMUIButton()
        v.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        v.setBackgroundImage(UIImage(named: "icon_search"), for: .normal)
        v.addTarget(self, action: #selector(handlerButtonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    
    var inputHandler: ((_ input: String)->Void)?
    var selectHandler:(()->Void)?
    var searchHandler:((_ input: String, _ select: String)->Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()

        leftTop.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(20)
            make.size.equalTo(CGSize(width: 120, height: 20))
        }
        
        rightTop.snp.makeConstraints { (make) in
            make.left.equalTo(leftTop.snp.right).offset(60)
            make.top.equalTo(20)
            make.size.equalTo(CGSize(width: 120, height: 20))
        }
        
        leftMid.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(leftTop.snp.bottom).offset(20)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        
        rightMid.snp.makeConstraints { (make) in
            make.left.equalTo(rightTop.snp.left)
            make.top.equalTo(leftMid.snp.top)
            make.size.equalTo(CGSize(width: 100, height: 20))
        }
        
        leftBottom.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(leftMid.snp.bottom).offset(10)
            make.size.equalTo(CGSize(width: 120, height: 30))
        }
        
        midBottom.snp.makeConstraints { (make) in
            make.left.equalTo(rightTop.snp.left)
            make.top.equalTo(leftBottom.snp.top)
            make.size.equalTo(CGSize(width: 60, height: 30))
        }
        
        rightBottom.snp.makeConstraints { (make) in
            make.left.equalTo(midBottom.snp.right).offset(20)
            make.centerY.equalTo(midBottom.snp.centerY)
            make.size.equalTo(CGSize(width: 30, height: 30))
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.image = UIImage(named: "bg_me")
        self.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(_ item: ProxyListChildData.ProxyChildItem) {
        leftTop.text = "团队玩家：\(item.total_count)人"
        rightTop.text = "直推成员：\(item.child_count)人"
    }
}


extension FECenterProxyListHeadView {
    @objc func handlerButtonAction(_ btn: QMUIButton) {
        self.endEditing(true)
        
        if btn == midBottom {
            // vip
            selectHandler?()
        } else {
            // search
            searchHandler?(leftBottom.text!, midBottom.currentTitle!)
        }
    }
}


extension FECenterProxyListHeadView: QMUITextFieldDelegate {
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {
        guard let handler = inputHandler else {
            return
        }
        
        handler(textfield.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        searchHandler?(leftBottom.text!, midBottom.currentTitle!)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
}
