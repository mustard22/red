//
//  FECenterAgencyController.swift
//  Red
//
//  Created by MAC on 2019/8/8.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

/// 代理中心界面
class FECenterAgencyController: FEBaseViewController {
    lazy var content: UIScrollView = {
        let v = UIScrollView(frame: view.bounds)
        v.showsVerticalScrollIndicator = false
        v.alwaysBounceVertical = true
//        v.contentSize = v.bounds.size
        view.addSubview(v)
        return v
    }()
    
    private lazy var rankBar: UIView = {
        let v = UIView()
        v.backgroundColor = Color.white
        v.frame = CGRect(x: 0, y: 10, width: self.view.frame.width, height: 40)
        content.addSubview(v)
        
        let img = UIImageView()
        img.frame = CGRect(x: 10, y: (v.height-20)/2, width: 5, height: 20)
        img.backgroundColor = .red
        v.addSubview(img)
        
        let lab = UILabel()
        lab.frame = CGRect(x: img.right+10, y: (v.height-20)/2, width: 200, height: 20)
        lab.text = "本月推广佣金排行"
        lab.textColor = .darkGray
        lab.textAlignment = .left
        lab.font = UIFont.systemFont(ofSize: 16)
        v.addSubview(lab)
        return v
    }()
    
    //MARK: 滚动排行表
    private lazy var labelCycleView: FECycleLabelView = {
        let v: FECycleLabelView = FECycleLabelView(self.datas, CGRect(x: 0, y: rankBar.bottom, width: self.view.frame.width, height: 75))
        v.backgroundColor = .white
        v.layer.masksToBounds = true
        content.addSubview(v)
        return v
    }()
    
    lazy var headview: FECenterChargeHeadView = {
        let v = FECenterChargeHeadView("icon_item_wdbb", "代理报表", "icon_item_xjwj", "下级列表", CGPoint(x: 0, y: kNaviBarHeight))
        v.showSelectLayer = false
        v.clickHandler = {[weak self](isLeft) in
            if isLeft {
                // 代理报表
                self?.push(to: FECenterProxyReportViewController())
            } else {
                // 下级列表
                self?.push(to: FECenterProxyListViewController())
            }
        }
//        self.view.addSubview(v)
        content.addSubview(v)
        return v
    }()
    
    lazy var headview2: FECenterChargeHeadView = {
        let v = FECenterChargeHeadView("icon_item_rule", "代理规则", "icon_item_tgjc", "推广赚钱", CGPoint(x: 0, y: kNaviBarHeight))
        v.showSelectLayer = false
        v.clickHandler = {[weak self](isLeft) in
            if isLeft {
                // 代理规则
                self?.push(to: FECenterProxyRuleViewController())
            } else {
                // 推广赚钱
                self?.push(to: FECenterShareMakeMoneyViewController())
            }
        }
//        self.view.addSubview(v)
        content.addSubview(v)
        return v
    }()
    
    // 每次显示行数
    private let noticeCount = 3
    
    private lazy var datas: [[String]] = {
        let notices = User.default.proxy_notice
        guard notices.count > 0 else {
            return []
        }
        
        var array: [[String]] = []
        var section: [String]?
        for (i,n) in notices.enumerated() {
            if i%noticeCount == 0 {
                if var s = section, s.count > 0 {
                    array.append(s)
                }
                section = []
                section!.append("\(n.title),\(n.content),\(n.draw_water)")
            } else {
                if var s = section {
                    section!.append("\(n.title),\(n.content),\(n.draw_water)")
                }
            }
        }
        
        if var s = section, s.count > 0 {
            array.append(s)
        }
        return array
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "代理中心"
    }
    
    deinit {
        labelCycleView.stopTimer()
        labelCycleView.removeFromSuperview()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        headview.top = labelCycleView.bottom + 10
        headview2.top = headview.bottom
    }
}
