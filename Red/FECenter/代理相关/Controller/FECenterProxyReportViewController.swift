//
//  FECenterProxyReportViewController.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit



/// 代理报表界面
class FECenterProxyReportViewController: YYTableViewController {
    var datas = [ProxyReportListData.ProxyReportListItem]()
    var listData: ProxyReportListData = ProxyReportListData()
    
    private lazy var topView: FECenterProxyReportTopView = {
        let v = FECenterProxyReportTopView(left: "总流水：0", right: "总收益：0", showLine: true)
        self.view.addSubview(v)
        return v
    }()
    
    lazy var headview: FECenterProxyTimeHeadView = {
        let v = FECenterProxyTimeHeadView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 90))
        self.view.addSubview(v)
        v.clickHandler = {[weak self] (btn) in
            guard let self = self else {
                return
            }
            if btn == v.leftBtn {
                // start
                self.handlerStartTime(btn)
            } else {
                // end time
                self.handlerEndTime(btn)
            }
        }
        return v
    }()
    
    private var isShowCell1: Bool {
        if UserCenter.default.lbanner.rebate_mode == "2" {
            return false
        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "代理报表"
        
        topView.snp.makeConstraints { (make) in
            make.top.equalTo(kNaviBarHeight)
            make.left.equalTo(0)
            make.right.equalTo(self.view)
            make.height.equalTo(self.isShowCell1 ? 0 : topView.height)
        }
        headview.snp.makeConstraints { (make) in
            make.top.equalTo(topView.snp.bottom)
            make.left.equalTo(0)
            make.right.equalTo(self.view)
            make.height.equalTo(90)
        }
        
        self.mj_head.beginRefreshing()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        self.registerCells()
        
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FECenterProxyReportViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

//MARK: - 事件处理
extension FECenterProxyReportViewController {
    //MARK: 修改开始时间
    func handlerStartTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            guard let self = self else {
                return
            }
            
            if hasChanged {
                self.headview.freshStartTime(time)
                self.mj_head.beginRefreshing()
            }
        }
        d.datePicker.maximumDate = Date().offsetOfDay(-1)
        d.show()
    }
    
    //MARK: 修改结束时间
    func handlerEndTime(_ btn: QMUIButton) {
        let d = FECenterLayerDatePicker.instance{[weak self] (time, hasChanged) in
            guard let self = self else {
                return
            }
            
            if hasChanged {
                self.headview.freshEndTime(time)
                self.mj_head.beginRefreshing()
            }
        }
        d.show()
    }
    
    //MARK: load list
    @objc func loadListData() {
        var param = ProxyReportListParam()
        param.start_time = self.headview.startTime
        param.end_time = self.headview.endTime
        param.pageIndex = "1"
        
        HttpClient.default.getProxyReportList(param, {[weak self] (msg, data) in
            self?.mj_head.endRefreshing()
            
            guard let self = self, let item = data else {
                QMUITips.showInfo(msg)
                return
            }
            
            self.listData = item
            self.datas.removeAll()
            self.datas.append(contentsOf: item.data)
            
            self.topView.refreshTitles(left: "总流水：\(item.total_turnover)", right: "总收益：\(item.total_income)")
            
            self.tableView.reloadData()
            
            if self.datas.count == 0 {
                self.tableView.mj_footer = nil
            } else {
                if self.datas.count >= 15 {
                    self.tableView.mj_footer = self.mj_foot
                }
            }
        })
    }
    
    //MARK: load more list
    @objc func loadMoreListData() {
        var param = ProxyReportListParam()
        param.start_time = self.headview.startTime
        param.end_time = self.headview.endTime
        param.pageIndex = "\(self.listData.currentPage+1)"
        
        HttpClient.default.getProxyReportList(param, {[weak self] (msg, data) in
            self?.mj_foot.endRefreshing()
            guard let self = self, let item = data else {
                QMUITips.showInfo(msg)
                return
            }
            self.listData = item
            self.datas.append(contentsOf: item.data)
            self.tableView.reloadData()
            
            if item.data.count < 15 {
                self.tableView.mj_footer = nil
            } else {
                self.mj_foot.state = .idle
            }
        })
    }
}

//MARK: - tableView delegate & datasource
extension FECenterProxyReportViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: headview.bottom, width: view.frame.size.width, height: view.frame.size.height - headview.bottom - (kIsIphoneX ? 20 : 0))
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.registerCells()
        
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
        
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreListData))
        self.tableView.mj_footer = nil
    }
    
    private func registerCells() {
        if self.isShowCell1 {
            self.tableView.registerCell(withClass: FECenterProxyReportCollectionCell.self)
        } else {
            self.tableView.registerCell(withClass: FECenterProxyReportCollectionCell2.self)
        }
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.isShowCell1 {
            return FECenterProxyReportCollectionCell.cellHeight()
        }
        return FECenterProxyReportCollectionCell2.cellHeight()
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isShowCell1 {
            let cell = tableView.dequeueReusableCell(withClass: FECenterProxyReportCollectionCell.self, for: indexPath)
            cell.update(datas[indexPath.row])
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withClass: FECenterProxyReportCollectionCell2.self, for: indexPath)
        cell.update(datas[indexPath.row])
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}



//MARK: - Class: FECenterProxyReportTopView
class FECenterProxyReportTopView: UIView {
    
    private lazy var leftLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: 0, y: 0, width: self.width/2, height: self.height)
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        self.addSubview(v)
        return v
    }()
    private lazy var rightLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: leftLabel.width, y: 0, width: leftLabel.width, height: self.height)
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        self.addSubview(v)
        return v
    }()
    
    lazy var vline: UIView = {
        let v = UIView()
        v.frame = CGRect(x: leftLabel.width, y: 0, width: 1, height: self.height)
        v.layer.backgroundColor = UIColor.RGBColor(248, 248, 248).cgColor
        self.addSubview(v)
        return v
    }()
    
    lazy var hline: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: self.height-1.0, width: self.width, height: 1)
        v.layer.backgroundColor = UIColor.RGBColor(248, 248, 248).cgColor
        self.addSubview(v)
        return v
    }()
    
    private let defaultFrame: CGRect = CGRect(x: 0, y: 0, width: kScreenSize.width, height: 50)
    
    init(left: String? = nil, right: String? = nil, showLine: Bool = true) {
        
        super.init(frame: self.defaultFrame)
        
        self.refreshTitles(left: left, right: right)
        
        hline.isHidden = !showLine
        vline.isHidden = !showLine
    }
    
    public func refreshTitles(left: String? = nil, right: String? = nil) {
        if let tl = left {
            leftLabel.text = tl
        }
        
        if let tr = right {
            rightLabel.text = tr
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        leftLabel.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(0)
            make.height.equalTo(self)
            make.width.equalTo(self.width/2)
        }
        rightLabel.snp.makeConstraints { (make) in
            make.left.equalTo(leftLabel.snp.right)
            make.top.equalTo(leftLabel)
            make.size.equalTo(leftLabel)
        }
        hline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(self)
            make.height.equalTo(1.0)
            make.bottom.equalTo(self)
        }
        vline.snp.makeConstraints { (make) in
            make.left.equalTo(leftLabel.snp.right)
            make.width.equalTo(1.0)
            make.height.equalTo(self)
            make.top.equalTo(0)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
