//
//  FECenterProxyRuleViewController.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 代理规则界面
class FECenterProxyRuleViewController: FEBaseViewController {
    private let content = "1.点击【我的】->【推广赚钱】->右上角的【分享】，可分享到微信等其他平台，或者点击在【大厅】内各个房间右上角的【分享】邀请各大平台好友一起娱乐赚钱；\n2.点击【大厅】进入自建的各个房间，再点击右上角的【分享】可以邀请其他平台好友进群;\n3.友友在本平台注册时可将自己邀请码发送给好友填写注册获得注册佣金；\n4.邀请好友注册平台成功后系统会赠送直属以为各级代理相应的佣金作为收益；\n5.绑定上下级关系的好友在系统房间游戏将会获得下级的佣金或者系统的游戏返水"
    
//    private lazy var contentLabel: QMUILabel = {
//        let c = QMUILabel()
//        c.font = UIFont.systemFont(ofSize: 15)
//        c.text = content
//        c.numberOfLines = 0
//        c.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
//        view.addSubview(c)
//        return c
//    }()
    
    private lazy var textView: UITextView = {
        let v = UITextView()
        v.isEditable = false // 不允许编辑
        v.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        view.addSubview(v)
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attr = NSAttributedString(string: content, attributes: [.paragraphStyle: style, .font: UIFont.systemFont(ofSize: 15)])
        v.attributedText = attr
        return v
    }()
    
    private lazy var topImage: UIImageView = {
        let c = UIImageView()
        c.image = UIImage(named: "proxy_rule_image")
        view.addSubview(c)
        return c
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "代理规则"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setupUI()
    }
    
    private func setupUI() {
        var rect = UIScreen.main.bounds
        rect.origin.y = kNaviBarHeight
        
        rect.size.height = rect.size.width * 0.5
        topImage.frame = rect
        
        rect.origin.y += rect.size.height
        rect.size.height = view.height - rect.origin.y
        textView.frame = rect
    }
}
