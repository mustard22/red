//
//  FECenterProxyListViewController.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

/// 下级列表界面
class FECenterProxyListViewController: YYTableViewController {
    var currentPage = 1
    
    var listData: ProxyListChildData?
    
    var datas = [ProxyListChildData.ProxyChildItem.ChildItem]()
    
    var types = [ProxyListLevelItem]()
    var typeIndex = 0
    
    lazy var headview: FECenterProxyListHeadView = {
        let v = FECenterProxyListHeadView(frame: CGRect(x: 0, y: kNaviBarHeight, width: self.tableView.frame.width, height: 150))
        v.selectHandler = {[weak self] in
            guard let self = self else {
                return
            }
            var ts = [String]()
            for s in self.types {
                ts.append(s.alias)
            }
            
            // vip 列表
            let l = FECenterLayerTableView.instance(ts, self.typeIndex) {[weak self] (index) in
                guard let self = self else {
                    return
                }
                
                if index == self.typeIndex {
                    return
                }
                
                self.typeIndex = index
                
                self.headview.midBottom.setTitle(ts[index], for: .normal)
            }
            l.show()
        }
        v.searchHandler = {[weak self] (input, title) in
            guard let self = self else {
                return
            }
            self.mj_head.beginRefreshing()
        }
        self.view.addSubview(v)
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "下级列表"

        self.loadLevelData()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: FECenterAddBankCollectionViewCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FECenterProxyListViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FECenterProxyListViewController {
    // 类型
    func loadLevelData() {
        HttpClient.default.getLevelList { [weak self] (status, items) in
            guard let self = self else {
                return
            }
            if !status {
                return
            }
            
            self.types = items
            
//            let all = ProxyListLevelItem()
//            all.id = ""
//            all.alias = "全部"
//            self.types.insert(all, at: 0)
            
            self.typeIndex = 0
            
            self.loadingData()
        }
    }
    
    @objc func loadingData() {
        
        self.currentPage = 1
        
        var param = ProxyChildParam()
        param.level = "\(types[typeIndex].id)"
        param.username = self.headview.leftBottom.text
        param.page = currentPage
        HttpClient.default.getProxyChildList(param) {[weak self] (status, item) in
            self?.mj_head.endRefreshing()
            
            guard let self = self else {
                return
            }
            if !status {
                return
            }
            
            guard let data = item else {
                return
            }
            
            self.currentPage += 1
            
            self.listData = data
            self.headview.update(data.data)
            
            self.datas.removeAll()
            self.datas = item!.data.child
            self.tableView.reloadData()
            
            if self.datas.count == 0 {
                self.tableView.mj_footer = nil
            } else {
                if self.datas.count >= 15 {
                    self.tableView.mj_footer = self.mj_foot
                }
            }
        }
    }
    
    @objc func loadingMoreData() {
        var param = ProxyChildParam()
        param.level = "\(types[typeIndex].id)"
        param.username = self.headview.leftBottom.text
        param.page = currentPage
        HttpClient.default.getProxyChildList(param) {[weak self] (status, item) in
            self?.mj_head.endRefreshing()
            
            guard let self = self else {
                return
            }
            if !status {
                return
            }
            
            guard let data = item else {
                return
            }
            
            self.currentPage += 1
            
            self.listData = data
            self.datas.append(contentsOf: item!.data.child)
            self.tableView.reloadData()
            if item!.data.child.count < 15 {
                self.tableView.mj_footer = nil
            } else {
                self.mj_foot.state = .idle
            }
        }
    }
}
extension FECenterProxyListViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FECenterProxyListCollectionCell.self)
        
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadingData))
        
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadingMoreData))
        self.tableView.mj_footer = nil
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: headview.bottom, width: view.frame.size.width, height: view.frame.size.height - headview.bottom - (kIsIphoneX ? 20 : 0))
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FECenterProxyListCollectionCell.self, for: indexPath)
        cell.update(datas[indexPath.row])
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
