//
//  GroupFetchPaperAnimateLayerView.swift
//  Red
//
//  Created by MAC on 2019/11/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

//MARK: 中牛动画
/// 抢包中牛浮层view
class GroupFetchPaperAnimateLayerView1: UIView {
    lazy var backButton: QMUIButton = {
        let v = QMUIButton()
        v.isUserInteractionEnabled = true
        self.addSubview(v)
        return v
    }()
    
    lazy var showImage: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_group_cattle")
        backButton.addSubview(v)
        return v
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.loadUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor(white: 0, alpha: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    public func show() {
        let win = UIApplication.shared.keyWindow
        win?.addSubview(self)
        
        // 获取提示音资源路径
        if let path = Bundle.main.path(forResource: "cattle", ofType: "wav") {
            VoiceManager.shared.stopPlay()
            // 播放提示音
            VoiceManager.shared.play(path, nil)
        }
        

        self.showImage.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            self.showImage.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.backgroundColor = UIColor(white: 0, alpha: 0.5)
        }) { (fin) in
            self.showImage.transform = CGAffineTransform.identity
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.hide()
            }
        }
    }
    
    public func hide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.showImage.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            self.backgroundColor = UIColor(white: 0, alpha: 0)
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    func loadUI() {
        backButton.snp.makeConstraints { (make) in
            make.size.equalTo(self.snp.size)
            make.center.equalTo(self.snp.center)
        }
        showImage.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 150*kScreenScale, height: 125*kScreenScale))
            make.center.equalTo(backButton.snp.center)
        }
    }
}


//MARK: 中雷动画
/// 群组发雷包奖励信息浮层view
class GroupFetchPaperAnimateLayerView3: UIView {
    lazy var backButton: QMUIButton = {
        let v = QMUIButton()
        v.isUserInteractionEnabled = true
        self.addSubview(v)
        return v
    }()
    
    lazy var showImage: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_group_boom")
        backButton.addSubview(v)
        return v
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.loadUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor(white: 0, alpha: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    public func show() {
        let win = UIApplication.shared.keyWindow
        win?.addSubview(self)
        
        // 获取提示音资源路径
        if let path = Bundle.main.path(forResource: "boom", ofType: "wav") {
            VoiceManager.shared.stopPlay()
            // 播放提示音
            VoiceManager.shared.play(path, nil)
        }
        
        self.showImage.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            self.showImage.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.backgroundColor = UIColor(white: 0, alpha: 0.5)
        }) { (fin) in
            self.showImage.transform = CGAffineTransform.identity
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.hide()
            }
        }
    }
    
    public func hide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.showImage.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            self.backgroundColor = UIColor(white: 0, alpha: 0)
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    func loadUI() {
        backButton.snp.makeConstraints { (make) in
            make.size.equalTo(self.snp.size)
            make.center.equalTo(self.snp.center)
        }
        showImage.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 150*kScreenScale, height: 125*kScreenScale))
            make.center.equalTo(backButton.snp.center)
        }
    }
}


//MARK: 奖励动画（自己发的雷包 有人踩雷了）
/// 幸运雷
class GroupFetchPaperAnimateLayerView2: UIView {
    lazy var backButton: QMUIButton = {
        let v = QMUIButton()
        v.isUserInteractionEnabled = true
        self.addSubview(v)
        return v
    }()
    
    lazy var showImage: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_group_lucky")
        backButton.addSubview(v)
        return v
    }()
    
    lazy var luckLabel: QMUILabel = {
        let v = QMUILabel()
        v.textColor = .white
        v.textAlignment = .center
        v.font = UIFont.boldSystemFont(ofSize: 18)
        v.left = 20
        v.right = showImage.width - 20
        v.height = 40
        v.centerY = showImage.centerY
        showImage.addSubview(v)
        return v
    }()
    
    var luckyMoney: String = "" {
        didSet (value) {
            luckLabel.text = "幸运奖励：\(value)元"
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.loadUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor(white: 0, alpha: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    public func show() {
        let win = UIApplication.shared.keyWindow
        win?.addSubview(self)
        
        // 获取提示音资源路径
        if let path = Bundle.main.path(forResource: "win", ofType: "mp3") {
            VoiceManager.shared.stopPlay()
            // 播放提示音
            VoiceManager.shared.play(path, nil)
        }
        
        self.showImage.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            self.showImage.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.backgroundColor = UIColor(white: 0, alpha: 0.5)
        }) { (fin) in
            self.showImage.transform = CGAffineTransform.identity
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.hide()
            }
        }
    }
    
    public func hide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.showImage.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            self.backgroundColor = UIColor(white: 0, alpha: 0)
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    func loadUI() {
        backButton.snp.makeConstraints { (make) in
            make.size.equalTo(self.snp.size)
            make.center.equalTo(self.snp.center)
        }
        showImage.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 150*kScreenScale, height: 150*kScreenScale))
            make.center.equalTo(backButton.snp.center)
        }
    }
}
