//
//  GroupPaperDetailCell.swift
//  Red
//
//  Created by MAC on 2019/11/1.
//  Copyright © 2019 MAC. All rights reserved.
//
import QMUIKit
//MARK: 牛包cell
class GroupPaperDetailCell1: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var typeLabel: QMUILabel = {
        let v = QMUILabel()
        v.text = "庄家"
        v.textColor = .white
        v.layer.backgroundColor = Color.golden.cgColor
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .center
        v.layer.masksToBounds = true
        v.layer.cornerRadius = 10
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var flagImage: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_cow_victory")
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var headImage: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_cow_1")
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var moneyLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var timeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var firstLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .center
        v.text = "手气最佳"
        v.textColor = Color.golden
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
    
    /// 牛包详情是否显示成败（当所有包抢完时再显示 由外部控制）
    var showVictory = false
    
}

extension GroupPaperDetailCell1 {
    public func update(_ data: GroupPaperDetailData.ListItem, _ isOwner: Bool = false) {
        
        avatarImageView.setImage(with: URL(string: data.receive_img))
        nameLabel.text = data.receive_name
        timeLabel.text = data.receive_time
        moneyLabel.text = data.val
        
        // 设置手气最佳
        firstLabel.isHidden = true
        if data.is_first == "true" || data.is_first == "1" {
            firstLabel.isHidden = false
        }
        
        // 系统群没有免死号；自建群时，type为1是免死号
        if (isOwner && data.type == "1") {
            typeLabel.isHidden = true
            flagImage.isHidden = true
            headImage.isHidden = true
            return
        }
        
        // 标记庄家(系统群type为1是庄家；自建群type为3是庄家)
        var is_admin = false
        if (isOwner && data.type == "3") || (!isOwner && data.type == "1") {
            is_admin = true
        }
        if is_admin {
            typeLabel.isHidden = false
            flagImage.isHidden = true
        } else {
            typeLabel.isHidden = true
            if showVictory {
                flagImage.isHidden = false
            } else {
                flagImage.isHidden = true
            }
        }
        
        // true（相对于庄家输了） false（相对于庄家赢了）
        if !flagImage.isHidden {
            if data.is_landmine == "false" {
                // 胜
                flagImage.image = UIImage(named: "ic_cow_victory")
            } else {
                flagImage.image = UIImage(named: "ic_cow_defeate")
            }
        }
        
        if data.niu_number > "0" {
            headImage.isHidden = false
            headImage.image = UIImage(named: "ic_cow_\(data.niu_number)")
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.size.equalTo(CGSize(width: 60, height: 60))
            make.centerY.equalTo(contentView.snp.centerY)
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.top.equalTo(avatarImageView.snp.top)
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.width.equalTo(130)
            make.height.equalTo(20)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.right).offset(5)
            make.width.equalTo(40)
            make.height.equalTo(20)
            make.top.equalTo(nameLabel.snp.top)
        }
        flagImage.snp.makeConstraints { (make) in
            make.width.equalTo(25)
            make.height.equalTo(25)
            make.left.equalTo(timeLabel.snp.right).offset(10)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        headImage.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.left.equalTo(flagImage.snp.right).offset(10)
            make.centerY.equalTo(flagImage.snp.centerY)
        }
        moneyLabel.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.top.equalTo(nameLabel.snp.top)
        }
        
        firstLabel.snp.makeConstraints { (make) in
            make.width.equalTo(moneyLabel)
            make.height.equalTo(moneyLabel)
            make.centerY.equalTo(timeLabel)
            make.right.equalTo(moneyLabel)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.top.equalTo(0)
        }
    }
}


//MARK: 福利包详情cell
class GroupPaperDetailCell2: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var thunderLabel: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.font = UIFont.systemFont(ofSize: 16)
        v.textAlignment = .center
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var moneyLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var timeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var firstLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .center
        v.text = "手气最佳"
        v.textColor = Color.golden
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
}

extension GroupPaperDetailCell2 {
    public func update(_ data: GroupPaperDetailData.ListItem) {
        avatarImageView.setImage(with: URL(string: data.receive_img))
        nameLabel.text = data.receive_name
        timeLabel.text = data.receive_time
        moneyLabel.text = data.val
        
        // 设置手气最佳
        firstLabel.isHidden = true
        if data.is_first == "true" || data.is_first == "1" {
            firstLabel.isHidden = false
        }
        
//        if data.is_landmine == "true" {
//            thunderLabel.isHidden = false
//        } else {
//            thunderLabel.isHidden = true
//        }
        thunderLabel.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.size.equalTo(CGSize(width: 60, height: 60))
            make.centerY.equalTo(contentView.snp.centerY)
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.top.equalTo(avatarImageView.snp.top)
        }
        moneyLabel.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.top.equalTo(nameLabel.snp.top)
        }
        thunderLabel.snp.makeConstraints { (make) in
            make.right.equalTo(moneyLabel.snp.left).offset(-10)
            make.width.equalTo(30)
            make.height.equalTo(20)
            make.top.equalTo(nameLabel.snp.top)
        }
        timeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.width.equalTo(130)
            make.height.equalTo(20)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
        
        firstLabel.snp.makeConstraints { (make) in
            make.width.equalTo(moneyLabel)
            make.height.equalTo(moneyLabel)
            make.centerY.equalTo(timeLabel)
            make.right.equalTo(moneyLabel)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.top.equalTo(0)
        }
    }
}




//MARK: 雷包详情cell
class GroupPaperDetailCell3: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var thunderLabel: QMUILabel = {
        let v = QMUILabel()
        v.text = "💣"
        v.font = UIFont.systemFont(ofSize: 16)
        v.textAlignment = .center
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var moneyLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .right
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var timeLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var firstLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .right
        v.text = "手气最佳"
        v.textColor = Color.golden
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
}

extension GroupPaperDetailCell3 {
    public func update(_ data: GroupPaperDetailData.ListItem) {
        avatarImageView.setImage(with: URL(string: data.receive_img))
        nameLabel.text = data.receive_name
        timeLabel.text = data.receive_time
        
        thunderLabel.isHidden = true
        moneyLabel.text = data.val
        
        // 设置手气最佳
        firstLabel.isHidden = true
        if data.is_first == "true" || data.is_first == "1" {
            firstLabel.isHidden = false
        }
        
        if data.is_landmine == "true" || data.is_landmine == "1" {
            thunderLabel.isHidden = false
            // 标记雷号为红色
            let atext = data.val
            let attr = NSMutableAttributedString(string: atext)
            attr.addAttributes([.foregroundColor: Color.red], range: NSRange(location: atext.count-1, length: 1))
            moneyLabel.attributedText = attr
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.size.equalTo(CGSize(width: 60, height: 60))
            make.centerY.equalTo(contentView.snp.centerY)
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.top.equalTo(avatarImageView.snp.top)
        }
        moneyLabel.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.top.equalTo(nameLabel.snp.top)
        }
        thunderLabel.snp.makeConstraints { (make) in
            make.right.equalTo(moneyLabel.snp.left).offset(-10)
            make.width.equalTo(30)
            make.height.equalTo(20)
            make.top.equalTo(nameLabel.snp.top)
        }
        timeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.width.equalTo(130)
            make.height.equalTo(20)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
        firstLabel.snp.makeConstraints { (make) in
            make.width.equalTo(moneyLabel)
            make.height.equalTo(moneyLabel)
            make.centerY.equalTo(timeLabel)
            make.right.equalTo(moneyLabel)
        }
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.top.equalTo(0)
        }
    }
}
