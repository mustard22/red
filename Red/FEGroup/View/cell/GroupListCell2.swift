//
//  GroupListCell2.swift
//  Red
//
//  Created by MAC on 2020/4/11.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

final class ItemView: UIView {
    private lazy var button: UIButton = {
       let v = UIButton()
        v.frame = self.bounds
        v.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    @objc
    private func buttonAction() {
        buttonActionHandler?()
    }
    
    private lazy var imgv: UIImageView = {
        let v = UIImageView()
        self.addSubview(v)
        return v
    }()
    
    lazy var dotView: UIView = {
        let v = UIView()
        v.layer.backgroundColor = Color.red.cgColor
        v.size = CGSize(width: 8, height: 8)
        
        v.layer.cornerRadius = 4
        v.layer.masksToBounds = true
        
        v.isHidden = true
        self.addSubview(v)
        return v
    }()
    
    private lazy var btitle: UILabel = {
        let v = UILabel()
        v.textAlignment = .center
        v.textColor = .black
        v.font = UIFont.systemFont(ofSize: 14)
        self.addSubview(v)
        return v
    }()
    
    class var size: CGSize {
        return CGSize(width: 80, height: 60)
    }
    
    var buttonActionHandler: (()->Void)?
    
    init(orgin: CGPoint) {
        super.init(frame: CGRect(origin: orgin, size: ItemView.size))
        
        button.frame = self.bounds
        
        imgv.size = CGSize(width: 40, height: 40)
        imgv.left = (self.width - imgv.size.width)/2
        imgv.top = 0
        
        dotView.left = imgv.right
        dotView.top = imgv.top - dotView.height/2
        
        btitle.size = CGSize(width: self.width, height: 20)
        btitle.top = imgv.bottom
        btitle.left = 0
    }
    
    func refreshItem(_ imgUrl: String, _ title: String, _ showDot: Bool = false) {
        btitle.text = title
        
        if imgUrl.hasPrefix("http") {
            imgv.kf.setImage(with: URL(string: imgUrl))
        } else {
            imgv.image = UIImage(named: imgUrl)
        }
        
        if title.isEmpty {
            imgv.center = button.center
        } else {
            imgv.top = 0
        }
        
        dotView.isHidden = !showDot
    }
    
    func setImageScale(_ scale: CGFloat = 1.0) {
        if scale < 0 || scale > 1 {
            return
        }
        
        let center = imgv.center
        var size = imgv.bounds.size
        size.width *= scale
        size.height *= scale
        imgv.size = size
        imgv.center = center
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class GroupListCell2: UITableViewCell {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var itemsView: UIView!
    
    lazy var itemButtons: [ItemView] = {
        var bs = [ItemView]()
        var x: CGFloat = 0
        var y: CGFloat = 0
        let hmargin: CGFloat = (self.itemsView.width - 15*2 - ItemView.size.width*3)/CGFloat(2)
        let vmargin: CGFloat = 10
        for i in 0..<6 {
            x = 15 + CGFloat(i%3)*(ItemView.size.width + hmargin)
            y = 5 + CGFloat(i/3)*(ItemView.size.height + vmargin)
            
            let b = ItemView(orgin: CGPoint(x: x, y: y))
            b.tag = i
            b.isHidden = true
            bs.append(b)
            itemsView.addSubview(b)
            
            b.buttonActionHandler = {
                self.clickHandler?(i)
            }
        }
        return bs
    }()
    
    public var clickHandler:((_ index: Int)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        topView.backgroundColor = UIColor.RGBAColor(240, 240, 240, 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(_ images: [GroupType], _ title: String, _ type: String = FEGroupListCellModel2.SectionType.sys.type) {
        self.topLabel.text = title
        
        for i in 0..<itemButtons.count {
            let btn = itemButtons[i]
            btn.isHidden = true
            if i < images.count {
                btn.isHidden = false
                MessageTipsManager.shared.msgCountOfGroupType(roomClass: images[i].id, isSys: type) { (count) in
                    btn.refreshItem(images[i].logo, images[i].name, count > 0 ? true : false)
                }
            }
        }
 
        
        itemsView.height = 5 + (ItemView.size.height + 10)*2
    }
    
    
    static func height() -> CGFloat {
        var h: CGFloat = 30
        h += 5 + (ItemView.size.height + 10)*2
        return h
    }
}

