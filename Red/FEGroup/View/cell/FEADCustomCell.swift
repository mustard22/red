//
//  FEADCustomCell.swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FEADCustomCell: UICollectionViewCell {
    lazy var imageView:UIImageView = {
       let aimageView = UIImageView()
        return aimageView
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addSubview(imageView)
        imageView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    }
}

// 公告cell
class FEAnnouncementCell: UICollectionViewCell {
    lazy var titleLabel: UILabel = {
        let v = UILabel()
        v.isUserInteractionEnabled = true
        v.textColor = UIColor(red: 245/255.0, green: 24/255.0, blue: 92/255.0, alpha: 1.0)
        v.font = UIFont.systemFont(ofSize: 16)
        return v
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.addSubview(titleLabel)
        titleLabel.frame = CGRect(x: 40, y: 0, width: frame.width-40, height: frame.height)
    }
    
}

