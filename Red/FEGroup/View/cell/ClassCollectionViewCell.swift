//
//  ClassCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/6.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import Kingfisher
class ClassCollectionViewCell: YYTableViewCell {
    
    lazy var imagev:UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        contentView.addSubview(v)
        return v
    }()
    
    lazy var dotView: UIView = {
        let v = UIView()
        v.layer.backgroundColor = Color.red.cgColor
        
        v.layer.cornerRadius = 4
        v.layer.masksToBounds = true
        
        v.isHidden = true
        contentView.addSubview(v)
        return v
    }()
    
    lazy var titleLabel:UILabel = {
       let v = UILabel()
        v.font = UIFont.systemFont(ofSize: 18)
        contentView.addSubview(v)
        return v
    }()
    
    lazy var detailLabel:UILabel = {
       let v = UILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        contentView.addSubview(v)
        return v
    }()
    
    private lazy var sepline: UIImageView = {
        let c = UIImageView()
        c.backgroundColor = UIColor.RGBColor(248, 248, 248)
        contentView.addSubview(c)
        return c
    }()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imagev.snp.makeConstraints { (make) in
            make.width.height.equalTo(60)
            make.left.equalTo(10)
            make.centerY.equalTo(contentView)
        }
        dotView.snp.makeConstraints { (make) in
            make.width.height.equalTo(8)
            make.left.equalTo(imagev.snp.right)
            make.centerY.equalTo(imagev.snp.top)
            
            dotView.layer.cornerRadius = 8/2
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imagev.snp.right).offset(10)
            make.top.equalTo(imagev)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.height.equalTo(20)
        }
        detailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel)
            make.bottom.equalTo(imagev)
            make.right.equalTo(titleLabel)
            make.height.equalTo(20)
        }
        sepline.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(contentView)
            make.height.equalTo(1)
        }
    }
}
