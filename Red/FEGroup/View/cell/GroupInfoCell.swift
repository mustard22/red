//
//  GroupInfoCell.swift
//  Red
//
//  Created by MAC on 2019/10/28.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
//MARK: 群信息列表cell
class GroupInfoCell: YYTableViewCell {
    lazy var leftLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 16)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var rightLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 13)
        v.textAlignment = .left
        v.textColor = .gray
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var switchControl: UISwitch = {
        let v = UISwitch()
        v.isOn = false
        v.onTintColor = Color.red
        v.isHidden = true
        v.isUserInteractionEnabled = false
        v.addTarget(self, action: #selector(handleValueChangeEvent(sender:)), for: UIControl.Event.valueChanged)
        return v
    }()
    
    var switchValueChangedHandler: ((_ value: Bool)->Void)?
}

extension GroupInfoCell {
    // action 0:点击, 1:switch
    public func update(_ left: String, _ right: String, _ showLine: Bool = true, _ showArrow: Bool = true, _ action: Int? = 0, _ switchValue: Bool? = false) {
        leftLabel.text = left
        rightLabel.text = right
        
        self.accessoryType = showArrow ? .disclosureIndicator : .none
        
        if action == 1 {
            self.accessoryView = switchControl
            switchControl.isOn = switchValue!
            switchControl.isHidden = false
            rightLabel.isHidden = true
        } else {
            switchControl.isHidden = true
            rightLabel.isHidden = false
        }
    }
    
    @objc
    open func handleValueChangeEvent(sender: Any) {
        switchValueChangedHandler?(switchControl.isOn)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        leftLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.width.equalTo(90)
            make.top.equalTo(0)
            make.height.equalTo(contentView.snp.height)
        }

        rightLabel.snp.makeConstraints { (make) in
            make.left.equalTo(leftLabel.snp.right).offset(10)
            make.right.equalTo(contentView).offset(-10)
            make.centerY.equalTo(contentView.snp.centerY)
        }
    }
}




//MARK: - 群组成员列表cell
class GroupAllMemberCell: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 16)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var rightLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .right
        v.textColor = .darkGray
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var rightButton: QMUIButton = {
        let v = QMUIButton("操作", fontSize: 14)
        v.setTitleColor(UIColor.RGBColor(37, 155, 78), for: .normal)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.contentView.addSubview(v)
        return v
    }()
    
    @objc func buttonAction() {
        self.buttonHandler?(self)
    }
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
    
    public var buttonHandler: ((_ c: GroupAllMemberCell) -> Void)?
}

extension GroupAllMemberCell {
    func update(_ item: RoomUserItem, _ showRightButton: Bool? = false) {
        avatarImageView.setImage(with: URL(string: item.avatar), placeHolder: UIImage(named: "ic_user_default"))
        nameLabel.text = item.nickname
        
        self.rightButton.isHidden = true
        
        if item.is_admin == "1" {
            rightLabel.text = "群主"
            rightLabel.textColor = Color.red
        } else {
            if let showButton = showRightButton, showButton {
                self.rightButton.isHidden = false
            }
            
            if item.status == "2" && item.speak_status == "0" {
                rightLabel.text = "已拉黑"
                rightLabel.textColor = .black
            } else if item.status == "1" && item.speak_status == "1" {
                rightLabel.text = "已禁言"
                rightLabel.textColor = .red
            } else if item.status == "2" && item.speak_status == "1" {
                rightLabel.textColor = .black
                let text = "已禁言  已拉黑"
                let attr = NSMutableAttributedString(string: text)
                attr.addAttribute(NSAttributedString.Key.foregroundColor, value: Color.red, range: NSRange(location: 0, length: 3))
                rightLabel.attributedText = attr
            } else {
                rightLabel.text = ""
            }
        }
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }

        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(120)
            make.height.equalTo(40)
            make.centerY.equalTo(contentView)
        }
        
        rightButton.snp.makeConstraints { (make) in
            make.right.equalTo(contentView).offset(-5)
            make.height.equalTo(35)
            make.width.equalTo(40)
            make.centerY.equalTo(contentView)
        }
        
        rightLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.right).offset(10)
            if rightButton.isHidden {
                make.right.equalTo(rightButton).offset(-5)
            } else {
                make.right.equalTo(rightButton.snp.left).offset(-10)
            }
            make.height.equalTo(40)
            make.centerY.equalTo(contentView)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}
