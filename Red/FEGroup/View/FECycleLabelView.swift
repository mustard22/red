//
//  FECycleLabelView.swift
//  Red
//
//  Created by MAC on 2019/11/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FECycleLabelView: UIView {
    private lazy var table: UITableView = {
        let v = UITableView()
        v.frame = self.bounds
        v.backgroundColor = self.backgroundColor
        v.separatorStyle = .none
        v.showsHorizontalScrollIndicator = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = false
        v.delegate = self
        v.dataSource = self
        v.register(FEProxyNoticeCell.self, forCellReuseIdentifier: "FEProxyNoticeCell")
        v.register(FEProxyNoticeCell2.self, forCellReuseIdentifier: "FEProxyNoticeCell2")
        self.addSubview(v)
        return v
    }()
    
    private var showIndex = 0
    
    private var datas: [[String]] = []
    
    static let defaultHeight: CGFloat = 90
    
    private var timer: Timer?
    
    public var animateDirection: UITableView.RowAnimation = .top
    
    init(_ datas: [[String]], _ frame: CGRect) {
        self.datas = datas
        super.init(frame: frame)
        
        reloadDataOfSection()
        startTimer()
    }
    
    func startScrolling() {
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startTimer() {
        self.stopTimer()
        let t = Timer(timeInterval: 3, target: self, selector: #selector(dotimer), userInfo: nil, repeats: true)
        RunLoop.main.add(t, forMode: .common)
        timer = t
    }
    
    @objc func dotimer() {
        showIndex += 1
        if showIndex == datas.count {
            showIndex = 0
        }
        
        reloadDataOfSection()
    }
    
    private func reloadDataOfSection() {
        let indexset = IndexSet.init(integer: 0)
        self.table.reloadSections(indexset, with: animateDirection)
    }
    
    func stopTimer() {
        if let _ = timer {
            timer!.invalidate()
            timer = nil
        }
    }
    
    
    deinit {
        stopTimer()

        dprint("timer 已释放")
    }
}

extension FECycleLabelView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if datas.count == 0 {
            // 没数据
            return 0
        }
        return datas[showIndex].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if showIndex == 0 , indexPath.row < 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FEProxyNoticeCell2") as! FEProxyNoticeCell2
            cell.contentView.backgroundColor = self.backgroundColor
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "FEProxyNoticeCell") as! FEProxyNoticeCell
        cell.contentView.backgroundColor = self.backgroundColor
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if showIndex == 0 , indexPath.row < 3 {
            let cell0 = cell as! FEProxyNoticeCell2
            let arr = datas[showIndex]
            let str = arr[indexPath.row]
            let ss = str.components(separatedBy: ",")
            if ss.count == 3 {
                let img = UIImage(named: "ic_proxy_rank\(indexPath.row+1)")
                cell0.update(img,ss[0],ss[1],ss[2])
            }
            return
        }
        
        let cell0 = cell as! FEProxyNoticeCell
        let arr = datas[showIndex]
        let str = arr[indexPath.row]
        let ss = str.components(separatedBy: ",")
        if ss.count == 3 {
            cell0.update(ss[0],ss[1],ss[2])
        } else {
            cell0.update("","","")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FEProxyNoticeCell.defaultHeight
    }
}



class FEProxyNoticeCell: UITableViewCell {
    lazy var leftLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: margin, y: 0, width: 50, height: contentView.height)
        v.isUserInteractionEnabled = true
        v.textColor = UIColor.RGBColor(50, 50, 50)
        v.textAlignment = .left
        v.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var midLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: leftLabel.right + margin, y: 0, width: (contentView.width-margin*3-leftLabel.width)/2, height: contentView.height)
        v.isUserInteractionEnabled = true
        v.textColor = UIColor.RGBColor(50, 50, 50)
        v.textAlignment = .left
        v.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var rightLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: midLabel.right + margin, y: 0, width: (contentView.width-margin*3-leftLabel.width)/2, height: contentView.height)
        v.isUserInteractionEnabled = true
        v.textColor = UIColor.RGBColor(50, 50, 50)
        v.textAlignment = .left
        v.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(v)
        return v
    }()
    
    static let defaultHeight: CGFloat = 25
    private let margin: CGFloat = 10
    
    func update(_ left: String, _ mid: String, _ right: String) {
        self.leftLabel.text = left
        self.midLabel.text = mid
        self.rightLabel.text = right
    }
}


class FEProxyNoticeCell2: UITableViewCell {
    private lazy var rankImgv: UIImageView = {
        let v = UIImageView()
        v.frame = CGRect(x: margin, y: (contentView.height-20)/2, width: 20, height: 20)
        self.contentView.addSubview(v)
        return v
    }()
    
    private lazy var leftLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: rankImgv.right, y: 0, width: 50, height: contentView.height)
        v.isUserInteractionEnabled = true
        v.textColor = UIColor.RGBColor(50, 50, 50)
        v.textAlignment = .left
        v.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(v)
        return v
    }()
    
    private lazy var midLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: leftLabel.right + margin, y: 0, width: (contentView.width-margin*3-leftLabel.width-rankImgv.width)/2, height: contentView.height)
        v.isUserInteractionEnabled = true
        v.textColor = UIColor.RGBColor(50, 50, 50)
        v.textAlignment = .left
        v.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(v)
        return v
    }()
    
    private lazy var rightLabel: UILabel = {
        let v = UILabel()
        v.frame = CGRect(x: midLabel.right + margin, y: 0, width: (contentView.width-margin*3-leftLabel.width-rankImgv.width)/2, height: contentView.height)
        v.isUserInteractionEnabled = true
        v.textColor = UIColor.RGBColor(50, 50, 50)
        v.textAlignment = .left
        v.font = UIFont.systemFont(ofSize: 13)
        self.contentView.addSubview(v)
        return v
    }()
    
    static let defaultHeight: CGFloat = 25
    private let margin: CGFloat = 10
    
    func update(_ rankImg: UIImage?, _ left: String, _ mid: String, _ right: String) {
        self.rankImgv.image = rankImg
        self.leftLabel.text = left
        self.midLabel.text = mid
        self.rightLabel.text = right
    }
}
