//
//  GroupDetailHeadView.swift
//  Red
//
//  Created by MAC on 2020/1/10.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

//MARK: CLASS: topview
class GroupInfoTopView: UIView {
    
    lazy var buttons: [QMUIButton] = {
        let v = [QMUIButton]()
        return v
    }()
    
    lazy var avatars: [UIImageView] = {
        let v = [UIImageView]()
        return v
    }()
    
    lazy var bottomButton: QMUIButton = {
        let b = QMUIButton("", fontSize: 15)
        b.setTitleColor(.red, for: .normal)
        b.addTarget(self, action: #selector(allButtonAction(_:)), for: .touchUpInside)
        self.addSubview(b)
        return b
    }()
    
    var avatarButtonHandler: ((_ index: Int)->Void)?
    var allMemberHandler: (()->Void)?
    
    let rows = 2
    let cols = 5
    let itemSize: CGFloat = 50
    lazy var margin: CGFloat = (UIScreen.main.bounds.size.width - itemSize*CGFloat(cols))/(CGFloat(cols)+1)
    lazy var currentHeight = CGFloat(rows)*(itemSize+margin)+44
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func update(_ data: RoomUserData) {
        createViews()
        
        bottomButton.setTitle("全部群成员(\(data.total))>", for: .normal)
        
        for i in 0..<avatars.count {
            if i>data.member.count-1 {
                break
            }
            let url = data.member[i].avatar
            avatars[i].setImage(with: URL(string: url))
        }
    }
    
    private func createViews() {
        if buttons.count > 0 {
            return
        }
        
        bottomButton.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(self.snp.right)
            make.height.equalTo(44)
            make.bottom.equalTo(self.snp.bottom)
        }
        
        for i in 0..<rows*cols {
            let x = margin + CGFloat(i%cols) * (margin+itemSize)
            let y = margin + CGFloat(i/cols) * (margin+itemSize)
            let b = QMUIButton()
            b.tag = i
            b.frame = CGRect(x: x, y: y, width: itemSize, height: itemSize)
            b.addTarget(self, action: #selector(avatarButtonAction(_:)), for: .touchUpInside)
            self.addSubview(b)
            buttons.append(b)
            
            let imgv = UIImageView()
            b.addSubview(imgv)
            avatars.append(imgv)
            imgv.snp.makeConstraints { (make) in
                make.center.equalTo(b.snp.center)
                make.size.equalTo(b.snp.size)
            }
        }
    }
    
    @objc func avatarButtonAction(_ btn: QMUIButton) {
        if let handler = avatarButtonHandler {
            handler(btn.tag)
        }
    }
    
    @objc func allButtonAction(_ btn: QMUIButton) {
        if let handler = allMemberHandler {
            handler()
        }
    }
}
