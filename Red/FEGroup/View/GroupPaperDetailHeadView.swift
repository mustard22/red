//
//  GroupPaperDetailHeadView.swift
//  Red
//
//  Created by MAC on 2019/11/1.
//  Copyright © 2019 MAC. All rights reserved.
//
import QMUIKit

// 群组红包详情列表head view
class GroupPaperDetailHeadView: UIView {
    lazy var avatar: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(250, 250, 250)
        self.addSubview(v)
        return v
    }()
    
    lazy var name: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 18)
        v.textColor = UIColor.RGBColor(136,136,136)
        self.addSubview(v)
        return v
    }()
    lazy var count: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.textAlignment = .center
        v.textColor = .black
        v.font = UIFont.systemFont(ofSize: 20)
        self.addSubview(v)
        return v
    }()
    
    lazy var bottomBar: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        self.addSubview(v)
        return v
    }()
    lazy var status: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.textAlignment = .left
        v.textColor = UIColor.RGBColor(136,136,136)
        v.font = UIFont.systemFont(ofSize: 14)
        v.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        bottomBar.addSubview(v)
        return v
    }()
    
    lazy var thunderNumber: QMUILabel = {
        let v = QMUILabel()
        v.text = "雷号:"
        v.textAlignment = .right
        v.textColor = UIColor.darkGray
        v.font = UIFont.systemFont(ofSize: 14)
        v.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        bottomBar.addSubview(v)
        return v
    }()
    
    // 抢包停止倒计时
    lazy var fetchStopStatus: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.textAlignment = .center
        v.textColor = UIColor.RGBColor(136,136,136)
        v.font = UIFont.systemFont(ofSize: 15)
        v.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        v.isHidden = true
        self.addSubview(v)
        return v
    }()
    
    private var timer: Timer?
    
    var refreshHandler: (()->Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.loadUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.RGBColor(248, 248, 248)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startTimer(_ count: Int) {
        var d = count
        self.fetchStopStatus.text = "\(d)s后过期"
        timer = Timer(timeInterval: 1, repeats: true, block: {[weak self] (t) in
            guard let self = self else {
                return
            }
            
            d -= 1
            if d == 0 {
                self.stopTimer()
                self.fetchStopStatus.text = "抢包时间已过，禁止抢包"
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    // 告诉列表刷新
                    self.refreshHandler?()
                }
            } else {
                self.fetchStopStatus.text = "\(d)s后过期"
            }
        })
        // 避免倒计时期间，拖动列表使得倒计时失效
        RunLoop.main.add(timer!, forMode: RunLoop.Mode.common)
    }
    
    deinit {
        stopTimer()
        dprint("\(self): 计时停止")
    }
    
    func stopTimer() {
        if let _ = self.timer?.isValid {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    func update(_ item: GroupPaperDetailData.ProjectItem, _ duration: Int = 0) {
        self.height = bottomBar.bottom
        
        avatar.setImage(with: URL(string: item.user_avatar))
        name.text = item.nickname
        count.text = "\(item.amount)-\(item.packet_count)包"
        status.text = "已领取\(item.stat_fetched_count)/\(item.packet_count)，共\(item.stat_fetched_amount)/\(item.amount)元"
        
        thunderNumber.text = ""
        
        guard let type = RoomType.type(value: item.room_type) else {
            return
        }
        
        // 雷号
        if type == .singleThunder || type == .multipleThunder {
            // 标记雷号为红色
            let numberLen = item.landmine_number.count
            let atext = "雷号:\(item.landmine_number)"
            let attr = NSMutableAttributedString(string: atext)
            attr.addAttributes([.foregroundColor: Color.red], range: NSRange(location: atext.count-numberLen, length: numberLen))
            thunderNumber.attributedText = attr
        }
        
        // 只有 牛包|接龙 才倒计时
        if type != .cattle && type != .solitaire {
            return
        }
        
        fetchStopStatus.isHidden = false
        
        if duration <= 0 {
            self.fetchStopStatus.text = "抢包时间已过，禁止抢包"
            return
        }
        
        // 倒计时
        self.startTimer(duration)
    }
}


extension GroupPaperDetailHeadView {
    func loadUI() {
        avatar.snp.remakeConstraints { (make) in
            make.size.equalTo(CGSize(width: 80, height: 80))
            make.top.equalTo(20)
            make.centerX.equalTo(self.snp.centerX)
        }
        
        name.snp.remakeConstraints { (make) in
            make.top.equalTo(avatar.snp.bottom)
            make.left.equalTo(10)
            make.right.equalTo(self.snp.right).offset(-10)
            make.height.equalTo(40)
        }
        
        count.snp.remakeConstraints { (make) in
            make.size.equalTo(CGSize(width: 150, height: 40))
            make.top.equalTo(name.snp.bottom)
            make.centerX.equalTo(self.snp.centerX)
        }
        
        fetchStopStatus.snp.remakeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(self)
            make.top.equalTo(count.snp.bottom)
            make.height.equalTo(20)
        }
        
        bottomBar.snp.remakeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(self).offset(0)
            make.top.equalTo(fetchStopStatus.snp.bottom).offset(10)
            make.height.equalTo(30)
        }
        thunderNumber.snp.remakeConstraints { (make) in
            make.right.equalTo(bottomBar.snp.right).offset(-10)
            make.top.equalTo(0)
            make.height.equalTo(bottomBar.snp.height)
            make.width.equalTo(100)
        }
        status.snp.remakeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(thunderNumber.snp.left).offset(0)
            make.top.equalTo(0)
            make.height.equalTo(bottomBar.snp.height)
        }
    }
}

