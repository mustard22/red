//
//  GroupPaperInfoLayerView.swift
//  Red
//
//  Created by MAC on 2019/11/1.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

/// 群组红包信息浮层view
class GroupPaperInfoLayerView: UIView {
    enum GroupPaperInfoAction {
        case openPaper
        case detailPaper
    }
    
    static var isLayerHidden = true
    
    lazy var backButton: QMUIButton = {
        let v = QMUIButton()
        v.isUserInteractionEnabled = true
        v.addTarget(self, action: #selector(exitAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    lazy var contentBack: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_red_packet_bg")
        v.isUserInteractionEnabled = true
        backButton.addSubview(v)
        return v
    }()
    lazy var avatar: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(250, 250, 250)
        contentBack.addSubview(v)
        return v
    }()
    lazy var tips: QMUILabel = {
        let v = QMUILabel()
        v.text = "恭喜发财，大吉大利"
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 20)
        v.textColor = UIColor.RGBColor(249, 208, 146)
        contentBack.addSubview(v)
        return v
    }()
    lazy var status: QMUILabel = {
        let v = QMUILabel()
        v.text = "领取中"
        v.textAlignment = .center
        v.textColor = .white
        v.numberOfLines = 0
        v.font = UIFont.systemFont(ofSize: 15)
        contentBack.addSubview(v)
        return v
    }()
    lazy var money: QMUILabel = {
        let v = QMUILabel()
        v.text = "0.0"
        v.textAlignment = .center
        v.textColor = UIColor.RGBColor(249, 208, 146)
        v.font = UIFont.systemFont(ofSize: 18)
        contentBack.addSubview(v)
        return v
    }()
    lazy var open: QMUIButton = {
        let v = QMUIButton("",image:UIImage(named: "ic_red_packet_open"))
        v.isUserInteractionEnabled = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        contentBack.addSubview(v)
        return v
    }()
    
    lazy var detail: QMUIButton = {
        let v = QMUIButton("查看红包详情>",fontSize: 15)
        v.setTitleColor(UIColor.RGBColor(249, 208, 146), for: .normal)
        v.isUserInteractionEnabled = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        contentBack.addSubview(v)
        return v
    }()
    
    lazy var exit: QMUIButton = {
        let v = QMUIButton("X",fontSize: 25)
        v.setTitleColor(UIColor.RGBColor(203, 73, 49), for: .normal)
        v.isUserInteractionEnabled = true
        v.addTarget(self, action: #selector(exitAction(_:)), for: .touchUpInside)
        backButton.addSubview(v)
        return v
    }()
    
    var actionHandler: ((_ action: GroupPaperInfoAction)->Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.loadUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor(white: 0, alpha: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    class var shared: GroupPaperInfoLayerView {
        struct Single {
            static let instance: GroupPaperInfoLayerView = GroupPaperInfoLayerView(frame: .zero)
        }
        return Single.instance
    }
}

extension GroupPaperInfoLayerView {
    @objc func exitAction(_ btn: QMUIButton) {
        if btn == exit || btn == backButton {
            hide()
        }
    }
    
    @objc func buttonAction(_ btn: QMUIButton) {
        var action: GroupPaperInfoAction?
        if btn == open {
            action = .openPaper
        }
        else if btn == detail {
            action = .detailPaper
        }
        
        guard let a = action else {
            return
        }
        
        if let handler = self.actionHandler {
            handler(a)
            if a == .detailPaper {
                self.hide()
            } else {
                startOpenButtonAnimation()
            }
        }
    }
}


extension GroupPaperInfoLayerView {
    
    public func show(_ inView: UIView) {
        self.center = inView.center
        inView.addSubview(self)
        
        self.contentBack.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            self.exit.isHidden = false
            self.backgroundColor = UIColor(white: 0, alpha: 0.3)
            self.contentBack.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (fin) in
            self.contentBack.transform = CGAffineTransform.identity
        }
    }
    
    public func show() {
        let win = UIApplication.shared.keyWindow
        win?.addSubview(self)
        
        self.contentBack.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3, animations: {
            self.exit.isHidden = false
            self.backgroundColor = UIColor(white: 0, alpha: 0.3)
            self.contentBack.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (fin) in
            self.contentBack.transform = CGAffineTransform.identity
        }
    }
    
    public func hide() {
        // 移除前先判断动画
        removeOpenLayerAnimation()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.contentBack.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            self.backgroundColor = UIColor(white: 0, alpha: 0)
            self.exit.isHidden = true
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    // 移除‘打开’按钮的旋转动画并隐藏
    public func removeOpenLayerAnimation() {
        UIView.animate(withDuration: 0.5, animations: {}) { (fin) in
            if fin {
                self.open.layer.removeAnimation(forKey: "rotationAnimation")
                self.open.isHidden = true
            }
        }
    }
    
    // 旋转动画
    func startOpenButtonAnimation() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.y")
        // 旋转角度
        rotationAnimation.toValue = Double.pi
        // 每次旋转的时间（单位秒）
        rotationAnimation.duration = 0.5
        rotationAnimation.isCumulative = true
        //重复旋转的次数，如果你想要无数次，那么设置成MAXFLOAT
        rotationAnimation.repeatCount = MAXFLOAT
        open.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    // 抢包之后
    func updateAfterFetchPaper(_ money: String?, _ tips: String?) {
        removeOpenLayerAnimation()
        if let m = money {
            self.money.isHidden = false
            self.money.text = m
            status.text = "已领取"
            detail.isHidden = false
        } else {
            self.money.isHidden = true
            if let tips = tips {
                status.text = tips
            }
        }
    }
    
    //MARK: 抢包之前
    func updateBeforeFetchPaper(_ item: GroupPaperInfoItem) {
        avatar.setImage(with: URL(string: item.user_avatar))
        
        open.isHidden = true
        money.isHidden = true
        detail.isHidden = false
        
        if let m = Double(item.money), m > 0 {
            money.isHidden = false
            money.text = item.money
            status.text = "已领取"
        } else {
            if item.status == "1" {
                status.text = "领取中"
                detail.isHidden = true
                open.isHidden = false
            } else {
                status.text = item.status == "2" ? "已领完" : "已过期"
            }
        }
    }
    
    func loadUI() {
        backButton.snp.makeConstraints { (make) in
            make.size.equalTo(self.snp.size)
            make.center.equalTo(self.snp.center)
        }
        contentBack.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 300*kScreenScale, height: 400*kScreenScale))
            make.center.equalTo(backButton.snp.center)
        }
        exit.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 60, height: 60))
            make.centerX.equalTo(backButton.snp.centerX)
            make.top.equalTo(contentBack.snp.bottom).offset(5)
        }
        avatar.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 60, height: 60))
            make.top.equalTo(20)
            make.centerX.equalTo(contentBack.snp.centerX)
        }
        tips.snp.makeConstraints { (make) in
            make.top.equalTo(avatar.snp.bottom).offset(10)
            make.left.equalTo(10)
            make.right.equalTo(contentBack.snp.right).offset(-10)
            make.height.equalTo(40)
        }
        status.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.left.equalTo(20)
            make.right.equalTo(contentBack.snp.right).offset(-20)
            make.top.equalTo(tips.snp.bottom).offset(10)
        }
        money.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 120, height: 40))
            make.top.equalTo(status.snp.bottom).offset(10)
            make.centerX.equalTo(contentBack.snp.centerX)
        }
        detail.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 150, height: 50))
            make.bottom.equalTo(contentBack.snp.bottom)
            make.centerX.equalTo(contentBack.snp.centerX)
        }
        open.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 80, height: 80))
            make.bottom.equalTo(detail.snp.top).offset(-30)
            make.centerX.equalTo(contentBack.snp.centerX)
        }
    }
}
