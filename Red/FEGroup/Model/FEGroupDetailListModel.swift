//
//  FEGroupDetailListModel.swift
//  Red
//
//  Created by MAC on 2020/4/15.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

struct GroupDetailListModel {
    var groupType: GroupType = GroupType()
    var isSystem: Bool = true
    
    var datas: [GroupList] = []
    
    mutating func loadData(handler: @escaping (()->Void)) {
        let id = groupType.id
        
        let unsafeSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        if isSystem {
            HomePageModel.getGroupListOfSystem(class_id: id) { (items) in
                unsafeSelf.pointee.datas = items
                handler()
            }
        } else {
            HomePageModel.getGroupListOfOwner(class_id: id) { (items) in
                unsafeSelf.pointee.datas = items
                handler()
            }
        }
    }
}
