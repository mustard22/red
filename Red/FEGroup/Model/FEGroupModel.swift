//
//  FEGroupModel.swift
//  Red
//
//  Created by MAC on 2020/3/27.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

//MARK: enum - 房间类型枚举

///房间类型枚举
public enum RoomType: String, CaseIterable {
    public typealias RawValue = String
    /// 牛牛
    case cattle
    /// 福利
    case welfare
    /// 普通
    case general
    /// 单雷
    case singleThunder
    /// 多雷
    case multipleThunder
    /// 红包接龙
    case solitaire
}

extension RoomType {
    /// 类型名
    var name: String {
        switch self {
        case .cattle:
            return "牛房"
        case .welfare:
            return "福利房"
        case .general:
            return "普通房"
        case .singleThunder:
            return "单雷房"
        case .multipleThunder:
            return "多雷房"
        case .solitaire:
            return "接龙房"
        }
    }
    
    /// 类型值
    public var rawValue: String {
        switch self {
        case .cattle:
            return "1"
        case .welfare, .general:
            return "2"
        case .singleThunder:
            return "3"
        case .multipleThunder:
            return "4"
        case .solitaire:
            return "5"
        }
    }

    /// 房间类型值转类型枚举（String->RoomType?）
    /// - Parameter : isOwner true(自建群) false(系统群)
    static func type(value: String, isOwner: Bool = false) -> RoomType? {
        guard let _ = Double(value) else {
            return nil
        }
        
        switch value {
        case RoomType.cattle.rawValue:
            return .cattle
        case RoomType.welfare.rawValue, RoomType.general.rawValue:
            return isOwner ? .general : .welfare
        case RoomType.singleThunder.rawValue:
            return .singleThunder
        case RoomType.multipleThunder.rawValue:
            return .multipleThunder
        case RoomType.solitaire.rawValue:
            return .solitaire
        default: return nil
        }
    }
}


//MARK: 首页房间列表显示样式
public enum GroupListShowType {
    /// 默认不分类型 全部显示
    case `default`
    /// 按照类型区分所有房间（全部、单雷、多雷、接龙、牛、福利、自建）
    case allTypes
    /// 按系统、自建区分
    case roomType
    
    public var value: String {
        switch self {
        case .default: return "0"
        case .allTypes: return "1"
        case .roomType: return "2"
        }
    }
}


//MARK: - struct - 存储上一次发包金额
/// 存储上一次发包金额
/// - Note: 需求：系统房或自建房发包完成后，缓存上一次的发包金额
public struct FELastSendMoneyItem: Codable {
    /// key: group_id, value: money
    var lastMoney: [String: String]
}


//MARK: - struct - HomePageModel
public struct HomePageModel {
    // 系统群数据
    private var sysGroupItems: [GroupList] = []
    // 自建群数据
    private var ownerGroupItems: [GroupList] = []
    
    /// 处理后的列表数据
    var lists: [GroupList] = []
    
    var lists2: [FEGroupListCellModel2] = []
    
    /// 群类型数据
    var groupTypeDatas: [GroupType] = []
    /// 公告数据
    var notices: [HomeNoticeData] = []
    /// 轮播图数据
    var ads: [HomeBannerData.HomeBanner] = []
    
    // 选择群组类型（-1时，表示不分类型，显示全部）
    var selectGroupType = -1
    
    /// 设置列表显示样式
    static var listShowType: GroupListShowType {
        let showType = UserCenter.default.lbanner.room_class_show
        return showType == "1" ? .allTypes : (showType == "2" ? .roomType : .default)
    }
    
    /// 自建群开关是否打开
    static var showOwnerGroup: Bool {
        return UserCenter.default.lbanner.web_owner_group_switch == "1" ? true : false
    }
}

//MARK: instance methods
extension HomePageModel {
    private static func setList2(_ stypes: [FEGroupListCellModel2.SectionType], _ groupTypes: [GroupType]) -> [FEGroupListCellModel2] {
        return stypes.map({ (st) -> FEGroupListCellModel2 in
            var m = FEGroupListCellModel2()
            m.title = st.name
            m.type = st.type
            m.types = groupTypes.filter({$0.id > "0"})
            m.types.append(GroupType(id: "all", name: "", logo: "ic_group_all"))
            return m
        })
    }
    
    /// 请求首页所有数据（进来请求一次）
    public mutating func loadAllData(_ handler: @escaping (()->Void)) {
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HomePageModel.loadAllData {(tuple) in
            DispatchQueue.global().async {
                let (top, types, sys, owner) = tuple
                 unsafePointSelf.pointee.notices = top.notice ?? []
                 unsafePointSelf.pointee.ads = top.advert?.child ?? []
                 unsafePointSelf.pointee.groupTypeDatas = types
                
                // 样式2 数据
                if HomePageModel.listShowType == .roomType {
                    var stypes: [FEGroupListCellModel2.SectionType] = [.sys]
                    if HomePageModel.showOwnerGroup {
                        stypes.append(.owner)
                    }
                    unsafePointSelf.pointee.lists2 = HomePageModel.setList2(stypes, types)
                }
                
                 if types.isEmpty {
                     unsafePointSelf.pointee.selectGroupType = -1
                 } else {
                     unsafePointSelf.pointee.selectGroupType = 0
                 }
                
                unsafePointSelf.pointee.sysGroupItems = sys.map({HomePageModel.setupPlayMethodOfGroupList($0)})
                unsafePointSelf.pointee.ownerGroupItems = owner.map({HomePageModel.setupPlayMethodOfGroupList($0)})
                 
                unsafePointSelf.pointee.dowithGroupMessageTipsWhenFirstOpen()
                
                 unsafePointSelf.pointee.dowithListData {
                    
                    DispatchQueue.main.async {
                        handler()
                    }
                 }
            }
        }
    }
    
    //MARK: >>>>> 第一次打开（安装）APP，所有群默认显示消息红点提示 <<<<<
    private mutating func dowithGroupMessageTipsWhenFirstOpen() {
        guard UserDefaults.Current.userLoginTime == 1 else {
            return
        }
        
        // 系统群 群组提醒
        let items =  self.sysGroupItems.map({
            MessageTipsManager.GroupMsgTipsItem(id: $0.id, count: 1, type: $0.class, isSys: FEGroupListCellModel2.SectionType.sys.type)
            
        })
        MessageTipsManager.shared.groupMsgTips = items
    }
    
    /// 下拉刷新列表（仅仅是网络请求自建群数据）
    public mutating func refreshGroupList(_ handler: @escaping (()->Void)) {
        if !HomePageModel.showOwnerGroup || HomePageModel.listShowType == .roomType {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                handler()
            }
            return
        }
        
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HomePageModel.getGroupListOfOwner { (items) in
            unsafePointSelf.pointee.ownerGroupItems = items.map({HomePageModel.setupPlayMethodOfGroupList($0)})
            unsafePointSelf.pointee.dowithListData {
                handler()
            }
        }
    }
    
    /// 刷新列表前处理数据
    public mutating func dowithListData(_ handler: @escaping (()->Void)) {
        if HomePageModel.listShowType == .default {
            self.lists = self.sysGroupItems + self.ownerGroupItems
            handler()
            return
        }
        
        let unsafeSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        DispatchQueue.global().async {
            var lists: [GroupList] = []
            let item = unsafeSelf.pointee.groupTypeDatas[unsafeSelf.pointee.selectGroupType]
            if item.id == "-1" {
                // 自建群
                lists = unsafeSelf.pointee.ownerGroupItems
            } else if item.id == "0" {
                // 全部 = 自建群 + 系统群
                lists = unsafeSelf.pointee.sysGroupItems + unsafeSelf.pointee.ownerGroupItems
            } else {
                // 对应类型的群(雷，牛牛，福利)
                lists = unsafeSelf.pointee.sysGroupItems.filter({$0.class == item.id}) + unsafeSelf.pointee.ownerGroupItems.filter({$0.class == item.id})
            }
            
            unsafeSelf.pointee.lists = lists
            DispatchQueue.main.async {
                handler()
            }
        }
    }
    
    //MARK: 移除群组数据（被踢出群组时，刷新列表）
    public mutating func deleteOwnerGroup(_ gid: String, _ handler: @escaping (()->Void)) {
        // 根据group_id查找对应群的item
        guard let index = self.ownerGroupItems.firstIndex(where: {$0.id == gid}) else {
            return
        }
        
        SMManager.manager.deleteConversation(cid: gid, isGroup: true)
        self.ownerGroupItems.remove(at: index)
        
        self.dowithListData {
            handler()
        }
    }
    
    //MARK: 检测好友邀请的群信息(申请入群成功后，才会执行handler?())
    public func checkOwnerGroupFriendInvite(_ handler: (()->Void)? = nil) {
        dprint("------检测好友邀请的群信息-------")
        guard let info = UserDefaults.Common.ownerGroupFriendInvite else {
            return
        }
        
        dprint("------检测好友邀请的群信息: \(info)")
        
        let items = info.components(separatedBy: ",")
        if items.count != 2 {
            return
        }
        
        HttpClient.default.ownerGroupApplyRoom(group_id: items[0], parent_id: items[1]) { (flag, wmsg) in
            if flag {
                dprint("------检测好友邀请的群信息: 缓存数据已清理")
                UserDefaults.Common.ownerGroupFriendInvite = nil
                
                handler?()
            }
            else {
                dprint("------检测好友邀请的群信息: 加群失败; msg: \(wmsg.text)")
            }
        }
    }
    
    //MARK: 获取系统群详情数据
    public static func getRoomDetail(_ item: GroupList, _ input: String? = nil, handler: @escaping ((_ flag: Bool, _ msg: String, _ msg: roomMessage?)->Void)) {
        if item.is_system == "1" {
            let param = goRoomParam(room_id: item.id, room_passwd: input, room_class: item.class)
            HttpClient.default.roomMessageData(parame: param, completionHandler: { (flag, msg, data) in
                 handler(flag, msg, data)
            })
            return
        }
        
        HttpClient.default.ownerGroupRoomDetail(group_id: item.id, room_passwd: input ?? "0"){(flag, msg, item) in
           handler(flag, msg, item)
       }
    }
}

//MARK: HomePageModel's private & static methods
extension HomePageModel {
    /// 请求所有数据
    private static func loadAllData(_ listShowType: GroupListShowType = .default, _ handler: ((_ allData: (HomeTopData, [GroupType], [GroupList], [GroupList]))->Void)? = nil) {
        let queue = DispatchQueue.global()
        let group = DispatchGroup()
        
        var types: [GroupType] = []
        var sysGroups: [GroupList] = []
        var ownerGroups: [GroupList] = []
        var topData = HomeTopData()
        
        // 群类型数据
        queue.async(group: group) {
            let signal = DispatchSemaphore(value: 0)
            queue.async {
                HomePageModel.getGroupTypeData {(items) in
                    types = items
                    signal.signal()
                }
            }
            signal.wait()
        }
        
        // 系统群列表数据
//        if HomePageModel.listShowType != .roomType {
//            queue.async(group: group) {
//                let signal = DispatchSemaphore(value: 0)
//                queue.async {
//                    HomePageModel.getGroupListOfSystem {(items) in
//                        sysGroups = items
//                        signal.signal()
//                    }
//                }
//                signal.wait()
//            }
//        }
        queue.async(group: group) {
            let signal = DispatchSemaphore(value: 0)
            queue.async {
                HomePageModel.getGroupListOfSystem {(items) in
                    sysGroups = items
                    signal.signal()
                }
            }
            signal.wait()
        }
        
        // 自建群列表数据
        if HomePageModel.showOwnerGroup && HomePageModel.listShowType != .roomType {
            queue.async(group: group) {
                let signal = DispatchSemaphore(value: 0)
                queue.async {
                    HomePageModel.getGroupListOfOwner {(items) in
                        ownerGroups = items
                        signal.signal()
                    }
                }
                signal.wait()
            }
        }
        
        // 顶部数据
        queue.async(group: group) {
            let signal = DispatchSemaphore(value: 0)
            queue.async {
                HomePageModel.getTopdata {(item) in
                    topData = item
                    signal.signal()
                }
            }
            signal.wait()
        }
        
        group.notify(queue: DispatchQueue.main) {
            handler?((topData, types, sysGroups, ownerGroups))
        }
    }
    
    //MARK: 获取顶部的 轮播图和公告数据
    private static func getTopdata(resultHandler: ((_ item: HomeTopData)->Void)? = nil) {
        HttpClient.default.GroupAD {(isSuccess, message, data) in
            
            resultHandler?(data)
        }
    }
    
    //MARK: 获取自建群数据
    static func getGroupListOfOwner(class_id: String = "all", resultHandler: ((_ items: [GroupList])->Void)? = nil) {
        let cid = class_id.isEmpty ? "all" : class_id
        
            HttpClient.default.ownerGroupCreateRoomList(class_id: cid) { (isSuccess, message, lists) in
                resultHandler?(lists)
            }
    }
    
    //MARK: 获取群类型数据（雷，牛牛，福利）
    private static func getGroupTypeData(resultHandler: ((_ types: [GroupType])->Void)? = nil) {
        HttpClient.default.getGroupType {(status, msg, items) in
            var types = items
            if types.count > 0 {
                let all = GroupType(id: "0", name: "全部")
                types.insert(all, at: 0)
                
                if HomePageModel.showOwnerGroup {
                    let owner = GroupType(id: "-1", name: "自建房")
                    types.append(owner)
                }
            }

            resultHandler?(types)
        }
    }
    
    
    //MARK: 获取系统群数据
    static func getGroupListOfSystem(class_id: String = "all", resultHandler: ((_ items: [GroupList])->Void)? = nil) {
        let cid = class_id.isEmpty ? "all" : class_id
        HttpClient.default.GroupListData(class_id: cid) { (isSuccess, message, lists) in
            resultHandler?(lists)
        }
    }
    
    //MARK: 设置各种类型房间的玩法
    private static func setupPlayMethodOfGroupList(_ item : GroupList) -> GroupList {
        guard item.playMethodItem.url.isEmpty, let type = RoomType.type(value: item.class) else {
            return item
        }
        
        var item = item
        var url = "", title = ""
        switch type {
        case .cattle:
            url = "\(HttpHost.rootUrl)/rule/NiuniuRule1.html"
            title = "牛包玩法"
        case .welfare, .general, .solitaire:
            // 20191227暂时拿掉福利群的红包玩法
            url = ""//"\(HttpHost.rootUrl)/rule/MainRules.html"
            title = "红包玩法"
        case .singleThunder, .multipleThunder:
            url = "\(HttpHost.rootUrl)/rule/BoomRule1.html"
            title = "雷包玩法"
        }
        
        item.playMethodItem.url = url
        item.playMethodItem.title = title
        return item
    }
}



public struct FEGroupListCellModel2 {
    public enum SectionType: CaseIterable {
        case sys
        case owner
        
        var name: String {
            switch self {
            case .sys: return "系统"
            case .owner: return "自建"
            }
        }
        
        var type: String {
            switch self {
            case .sys: return "1"
            case .owner: return "2"
            }
        }
    }
    
    var title = "自建群"
    /// 1sys 2owner
    var type = ""
    var types: [GroupType] = []
}



/// 自建群分享时，缓存分享信息model
public struct FEShareOwnerGroupCircle: Codable {
    var groupId = ""
    var date: Date = Date()
    var count = 0
}
