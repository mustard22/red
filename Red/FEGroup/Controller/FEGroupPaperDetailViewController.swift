//
//  FEGroupPaperDetailViewController.swift
//  Red
//
//  Created by MAC on 2019/11/1.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

class FEGroupPaperDetailViewController: YYTableViewController {
    private var data: GroupPaperDetailData?
    
    private var listData: [GroupPaperDetailData.ListItem] = []
    /// 红包ID
    var project_id = ""
    /// 发包人ID
    var senderId = ""
    /// 群id
    var group_id = ""
    /// 房间类型（1牛包 2福利 34雷包）
    var groupClass: RoomType = .cattle
    
    lazy var headview: GroupPaperDetailHeadView = {
        let v = GroupPaperDetailHeadView(frame: CGRect(x: 0, y: 0, width: self.tableView.width, height: 250))
        switch self.groupClass {
        case .cattle, .solitaire:
            v.refreshHandler = {[weak self] in
                self?.startLoading()
            }
        default: break
        }
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "抢包详情"
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        switch self.groupClass {
        case .cattle, .solitaire:
            self.loadDetailData()
        default:
            self.startLoading()
        }
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        self.registerTableviewCell()
        tableView.reloadData()
    }
    
    func registerTableviewCell() {
        tableView.registerCell(withClass: GroupPaperDetailCell1.self)
        tableView.registerCell(withClass: GroupPaperDetailCell2.self)
        tableView.registerCell(withClass: GroupPaperDetailCell3.self)
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FEGroupPaperDetailViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FEGroupPaperDetailViewController {
    @objc func loadDetailData() {
        HttpClient.default.groupGetPaperDetail(project_id: project_id, senderId: senderId) {[weak self] (status, msg, item) in
            guard let self = self else {
                return
            }
            
            if self.tableView.mj_header != nil {
                self.mj_head.endRefreshing()
            }
            
            if var data = item {
                data.project.stat_fetched_count = "\(data.data.count)"
                self.data = data
                self.listData.removeAll()
                self.listData.append(contentsOf: data.data)
                self.refreshListData()
            } else {
                QMUITips.show(withText: msg, in: self.view)
            }
        }
    }
    
    func refreshListData() {
        guard let d = self.data else {
            return
        }
        
        // 牛包 或 接龙包
        guard (self.groupClass == .cattle || self.groupClass == .solitaire) else {
                headview.update(d.project)
                self.tableView.reloadData()
                return
        }
        
        // 详情倒计时
        let duration: Int = Int(Double(d.project.expire_time) ?? 0)
        headview.update(d.project, duration)
        
        if duration <= 0 {
            // 已经停止 未进行倒计时
            self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadDetailData))
        } else {
            // 倒计期间, 要过滤掉‘庄家’数据
            self.listData = self.listData.filter({$0.type != "1"})
        }
        self.tableView.reloadData()
    }
}
extension FEGroupPaperDetailViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.registerTableviewCell()
        self.tableView.tableHeaderView = headview
    }
    
    func startLoading() {
        if self.tableView.mj_header == nil {
            self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadDetailData))
        }
        self.mj_head.beginRefreshing()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = listData[indexPath.row]
        switch self.groupClass {
        case .cattle:
            let cell = tableView.dequeueReusableCell(withClass: GroupPaperDetailCell1.self, for: indexPath)
            cell.showVictory = isShowVictoryOfNiuniuPaper()
            cell.update(model)
            return cell
        case .welfare, .general, .solitaire:
            let cell = tableView.dequeueReusableCell(withClass: GroupPaperDetailCell2.self, for: indexPath)
            cell.update(model)
            return cell
        case .singleThunder, .multipleThunder:
            let cell = tableView.dequeueReusableCell(withClass: GroupPaperDetailCell3.self, for: indexPath)
            cell.update(model)
            return cell
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: 牛包房：判断是否显示胜利失败
    private func isShowVictoryOfNiuniuPaper() -> Bool {
        // 抢包数==包数 或 倒计时已完成（倒计时之前没有初始化下拉刷新）
        if data?.project.packet_count == data?.project.stat_fetched_count || self.tableView.mj_header != nil {
            return true
        }
        return false
    }
}
