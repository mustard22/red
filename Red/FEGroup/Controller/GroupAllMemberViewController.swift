//
//  GroupAllMemberViewController.swift
//  Red
//
//  Created by MAC on 2019/10/28.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

class GroupAllMemberViewController: YYTableViewController {
    var roomId: String?
    
    var listData: RoomUserData?
    
    var datas = [RoomUserItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.title = "群组成员"
        self.loadListData()
    }
    
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: GroupAllMemberCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension GroupAllMemberViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension GroupAllMemberViewController {
    @objc func loadListData() {
        // load list
        let loadCount = 15
        var param = GroupAllMemberParam()
        param.room_id = roomId!
        param.pageIndex = "1"
        param.pageSize = "\(loadCount)"
        
        HttpClient.default.getGroupAllMembers(parame: param, completionHandler: { (status, msg, data) in
            self.mj_head.endRefreshing()
            if !status {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            self.listData = data
            self.datas.removeAll()
            self.datas.append(contentsOf: self.listData!.member)
            self.tableView.reloadData()
            
            if self.datas.count == 0 {
                self.tableView.mj_footer = nil
            } else {
                if self.datas.count >= loadCount {
                    self.tableView.mj_footer = self.mj_foot
                }
            }
        })
    }
    
    
    @objc func loadMoreListData() {
        //TODO: load more list
        let loadCount = 15
        var param = GroupAllMemberParam()
        param.room_id = roomId!
        param.pageIndex = "\(Int(listData!.currentPage)!+1)"
        param.pageSize = "\(loadCount)"
        
        HttpClient.default.getGroupAllMembers(parame: param, completionHandler: { (status, msg, data) in
            self.mj_foot.endRefreshing()
            if !status {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            self.listData = data
            self.datas.append(contentsOf: self.listData!.member)
            self.tableView.reloadData()
            
            if data.member.count < loadCount {
                self.tableView.mj_footer = nil
            } else {
                self.mj_foot.state = .idle
            }
        })
    }
}

extension GroupAllMemberViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: GroupAllMemberCell.self)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
        
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreListData))
        self.tableView.mj_footer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: GroupAllMemberCell.self, for: indexPath)
        let item = datas[indexPath.row]
        cell.update(item)
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
