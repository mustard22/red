//
//  FEGroupDetailListViewController.swift
//  Red
//
//  Created by MAC on 2020/4/15.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class FEGroupDetailListViewController: YYTableViewController {

    var model = GroupDetailListModel()
    
    // 标记进入群聊界面的房间信息
    private var enterChatGroup = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = model.groupType.id == "all" ? "全部" : model.groupType.name
        
        registerAllNotifations()
        
        refreshListData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if enterChatGroup.isEmpty {
            return
        }
        
        tableView.reloadData()
        enterChatGroup = ""
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        self.registerAllCells()
        tableView.reloadData()
    }
    
    private func registerAllCells() {
        tableView.registerCell(withClass: ClassCollectionViewCell.self)
    }

    deinit {
        removeAllNotifations()
    }
}

//MARK: - 注册通知
extension FEGroupDetailListViewController {
    private func registerAllNotifations() {
        NotificationCenter.default.addObserver(self, selector: #selector(groupTipsNotification(_ :)), name: MessageTipsManager.groupMsgTipsNotificaiton, object: nil)
    }
    
    private func removeAllNotifations() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc
    private func groupTipsNotification(_ n: Notification) {
        guard let info = n.userInfo, let gid = info["id"] as? String, let index = model.datas.firstIndex(where: {$0.id == gid}) else {
            return
        }
        
        if gid == self.enterChatGroup {
            return
        }
        
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}

//MARK: - 网络请求
extension FEGroupDetailListViewController {
    //MARK: 下拉刷新
    @objc private func refreshListData() {
        model.loadData {
            self.mj_head.endRefreshing()
            self.tableView.reloadData()
        }
    }
}

extension FEGroupDetailListViewController {
    override open func initTableView() {
        super.initTableView()
        self.registerAllCells()
        tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(refreshListData))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - (kIsIphoneX ? 20 : 0))
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.model.datas.count
        self.nodataView.text = count == 0 ? "暂无房间数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ClassCollectionViewCell.self, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell0 = cell as! ClassCollectionViewCell
        
        let item = model.datas[indexPath.row]
        cell0.imagev.setImage(with: URL(string: item.img))
        cell0.titleLabel.text = item.name
        cell0.detailLabel.text = item.know
        
        // 判断有没有新消息提醒
        MessageTipsManager.shared.isContains(id: item.id, .group) { (t) in
            var showTips = false
            if t.1 > 0 {
                showTips = true
            }
            cell0.dotView.isHidden = !showTips
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.didselectCellAction(indexPath.row)
    }
}

//MARK: - 点击cell，进入群聊
extension FEGroupDetailListViewController {
    // didselect cell
    private func didselectCellAction(_ index: Int) {
        
        let m = self.model.datas[index]
        
        if !self.isNeedPasswd(m) {
            // 不需密码 直接请求数据进群
            self.doRequestRoomDetail(m)
            return
        }
        
        self.inputGroupPasswd { (input) in
            if input.isEmpty {
                QMUITips.show(withText: "密码错误", in: self.view)
                return
            }
            
            self.doRequestRoomDetail(m, input)
        }
    }
    
    //MARK: 请求群组详情数据，进入群聊界面
    private func doRequestRoomDetail(_ item: GroupList, _ input: String? = nil) {
        let load = QMUITips.showLoading(in: self.view)
        HomePageModel.getRoomDetail(item, input) { (flag, msg, data) in
            load.hide(animated: true)
            
            if flag == false {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            
            // 标记当前进入群聊界面的房间信息
            self.enterChatGroup = item.id
            
            if item.is_system == "1" {
                // 进入系统群
                let vc = FEChatGroupViewController(chat_id: item.id)
                vc.groupItem = item
                vc.roomMessage = data
                self.push(to: vc)
                return
            }
            
            // 进入自建群
            let vc = FEChatOwnerGroupViewController(chat_id: item.id)
            vc.groupItem = item
            if let detail = data {
                vc.groupDetail = detail
            }
            self.push(to: vc)
        }
    }
    
    //MARK: 判断点击的群组是否需要输入密码
    private func isNeedPasswd(_ item: GroupList) -> Bool {
        // 不需要输密码：群主或未设置
        if item.is_owner == "1" || item.room_passwd != "1" {
            return false
        }
        return true
    }
    
    
    //MARK: 弹框提示输入密码
    private func inputGroupPasswd(_ handler: @escaping ((_ input: String)->Void)) {
        let ac = UIAlertController(title: "密码验证", message: nil, preferredStyle: .alert)
        ac.addTextField { (tf) in
            tf.placeholder = "请输入群组密码"
            tf.isSecureTextEntry = true
            tf.keyboardType = .numberPad
            tf.returnKeyType = .done
        }
        
        let ok = UIAlertAction(title: "确定", style: .default) { (action) in
            var input = ""
            if let tfs = ac.textFields, let tf = tfs.first, let text = tf.text, text.count > 0 {
                input = text
            }
            handler(input)
        }
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        ac.addAction(ok)
        ac.addAction(cancel)
        self.present(ac)
    }
}
