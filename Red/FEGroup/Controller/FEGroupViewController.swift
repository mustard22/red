//
//  FEGroupViewController.swift
//  Red
//
//  Created by MAC on 2019/8/5.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 大厅界面
class FEGroupViewController: YYTableViewController {
    private lazy var headContentBackView: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: tableView.width, height: cyclePictureView.height+labelCycleView.height)
        v.addSubview(cyclePictureView)
        v.addSubview(labelCycleView)
        return v
    }()
    
    // 轮播图
    private lazy var cyclePictureView: FECyclePictureView = {
        let v: FECyclePictureView = initialImagesCycleView()
        return v
    }()
    
    //MARK: 公告view
    private lazy var labelCycleView: FECyclePictureView = {
        let v: FECyclePictureView = initialLabelCycleView()
        v.allowScrollWithFinger = false
        return v
    }()
    
    // 群类型菜单上的按钮
    private var groupTypeButtons: [QMUIButton] = []
    
    // 群类型菜单
    private lazy var groupTypeBar: UIScrollView = {
        let v = UIScrollView()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        v.frame = CGRect(x: 0, y: 0, width: self.tableView.width, height: 40*kScreenScale)
        v.contentSize = CGSize(width: v.width, height: v.height)
        v.showsHorizontalScrollIndicator = false
        
        return v
    }()
    
    // 建群按钮
    private lazy var rightTop: QMUIButton = {
        let v = QMUIButton("建群", fontSize: 15)
        v.setTitleColor(.white, for: .normal)
        v.showsTouchWhenHighlighted = true
        v.size = CGSize(width: 40, height: 40)
        v.addTarget(self, action: #selector(createGroupAction), for: .touchUpInside)
        return v
    }()
    
    // 客服按钮
    private lazy var leftTop: QMUIButton = {
        let v = QMUIButton()
        v.size = CGSize(width: 40, height: 40)
        v.addTarget(self, action: #selector(customerAction), for: .touchUpInside)
        
        let imgv = UIImageView()
        imgv.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        imgv.image = UIImage(named: "icon_recharge_kf")
        v.addSubview(imgv)
        return v
    }()
    
    private var homeModel = HomePageModel()
    
    // 标记进入群聊界面的房间信息
    private var enterChatGroup: String?
    
    // 标记进入群列表界面的类型信息
    private var enterGroupListType: (String, String)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "大厅"
        
        if HomePageModel.showOwnerGroup {
            // ==1时显示自建群
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTop)
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.leftTop)
        
        // load数据
        self.startLoadData()
        
        // 判断是否有好友邀请入群的信息
        self.homeModel.checkOwnerGroupFriendInvite {
            self.refreshListData()
        }
        
        self.registerAllNotifations()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let _ = enterChatGroup {
            tableView.reloadData()
            enterChatGroup = nil
        } else if let _ = enterGroupListType {
            tableView.reloadData()
            enterGroupListType = nil
        }
    }
    
    deinit {
        self.removeAllNotifations()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        self.registerAllCells()
        tableView.reloadData()
    }
}

//MARK: - some private methods
extension FEGroupViewController {
    // 刷新群类型菜单
    private func refreshGroupTypeBar() {
        let content = self.groupTypeBar
        let count = self.homeModel.groupTypeDatas.count
        // default 4
        var w: CGFloat = content.width / CGFloat(count)
        if count > 4 {
            w = 80
            content.contentSize = CGSize(width: w * CGFloat(count), height: content.height)
        }
        
        // 移除旧的按钮
        for (_, btn) in self.groupTypeButtons.enumerated() {
            btn.removeFromSuperview()
        }
        self.groupTypeButtons.removeAll()
        
        for (i, item) in self.homeModel.groupTypeDatas.enumerated() {
            let btn = QMUIButton(item.name, fontSize: 15)
            btn.frame = CGRect(x: CGFloat(i)*w, y: 0, width: w, height: content.height)
            btn.setTitleColor(.black, for: .normal)
            btn.setTitleColor(.red, for: .selected)
            btn.tag = i
            btn.addTarget(self, action: #selector(groupTypeSelectedAction(_:)), for: .touchUpInside)
            content.addSubview(btn)
            
            if i == self.homeModel.selectGroupType {
                btn.isSelected = true
            }
            
            self.groupTypeButtons.append(btn)
        }
    }
    
    private func registerAllCells() {
        tableView.registerCell(withClass: ClassCollectionViewCell.self)
        tableView.register(UINib(nibName: "GroupListCell2", bundle: nil), forCellReuseIdentifier: "GroupListCell2")
    }
    
    //MARK: 初始化图片轮播view
    private func initialImagesCycleView() -> FECyclePictureView {
        let v: FECyclePictureView = FECyclePictureView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100.5))
        v.direction = .left
        v.autoScrollDelay = 5
        v.didTapAtIndexHandle = {[weak self] index in
            guard let self = self else {
                return
            }
            self.clickBanner(index)
            
            //            let url = URL(string: "https://web.qa.58Red.com/course_detail/1/1176")!
            //            AppRouter.default.open(url)
            //            dprint("点击了第 \(index + 1) 张图片")
        }
        
        v.register([FEADCustomCell.self], identifiers: ["FEADCustomCell"]) { (collectionView, indexPath, picture) -> UICollectionViewCell in

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FEADCustomCell", for: indexPath) as! FEADCustomCell
            cell.layer.cornerRadius = 0
            cell.layer.masksToBounds = true
            if picture.hasPrefix("http") {
                cell.imageView.setImage(with: URL(string: picture))
            } else {
                cell.imageView.image = UIImage(named: picture)
            }
            return cell
        }
        return v
    }
    
    //MARK: 初始化公告轮播view
    private func initialLabelCycleView() -> FECyclePictureView {
        let v: FECyclePictureView = FECyclePictureView(frame: CGRect(x: 0, y: cyclePictureView.bottom, width: self.view.frame.width, height: 44))
        v.direction = .bottom
        v.autoScrollDelay = 5
        
        v.backgroundColor = .white
        v.didTapAtIndexHandle = {[weak self] index in
            guard let self = self else {
                return
            }
            self.clickScrollLabel(index)
        }
        
        v.register([FEAnnouncementCell.self], identifiers: ["FEAnnouncementCell"]) { (collectionView, indexPath, picture) -> UICollectionViewCell in

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FEAnnouncementCell", for: indexPath) as! FEAnnouncementCell
            cell.titleLabel.text = picture
            return cell
        }
        // 喇叭img
        let imgv = UIImageView(image: UIImage(named: "ic_notice"))
        imgv.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        v.addSubview(imgv)
        
        return v
    }
}

//MARK: - 注册通知
extension FEGroupViewController {
    private func registerAllNotifations() {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshOwnerGroupNotification), name: NSNotification.Name.refreshHomeOwnerGroupList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ownerGroupFriendInviteNotification), name: NSNotification.Name.ownerGroupFriendInvite, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(outedOwnerGroupNotification(_:)), name: SMManager.outedOwnerGroupNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshOwnerGroupNotification), name: SMManager.refreshOwnerGroupNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(groupTypeTipsNotification(_:)), name: MessageTipsManager.groupTypeMsgTipsNotificaiton, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(groupTipsNotification(_:)), name: MessageTipsManager.groupMsgTipsNotificaiton, object: nil)
    }
    
    private func removeAllNotifations() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    private func groupTypeTipsNotification(_ n: Notification) {
        if HomePageModel.listShowType != .roomType {
            return
        }
        
        //TODO: 处理房间类型消息提醒
        guard let info = n.userInfo, let style = info["style"] as? String, let gclass = info["class"] as? String else {
            return
        }
        
        if let tuple = self.enterGroupListType, tuple.0 == style {
            if tuple.1 == gclass || tuple.1 == "all" {
                return
            }
        }
        
        tableView.reloadData()
    }
    
    
    @objc
    private func groupTipsNotification(_ n: Notification) {
        if HomePageModel.listShowType == .roomType {
            return
        }
        
        guard let info = n.userInfo, let gid = info["id"] as? String, let index = homeModel.lists.firstIndex(where: {$0.id == gid}) else {
            return
        }
        
        if gid == self.enterChatGroup {
            return
        }
        
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
    
    // 刷新自建群列表通知
    @objc private func refreshOwnerGroupNotification() {
        self.refreshListData()
    }
    
    //MARK: Safari跳转APP时发送该通知
    @objc private func ownerGroupFriendInviteNotification() {
        // 判断是否有好友邀请入群的信息
        self.homeModel.checkOwnerGroupFriendInvite {
            self.refreshListData()
        }
    }
    
    //MARK: 被群主踢出群聊消息
    @objc private func outedOwnerGroupNotification(_ n: Notification) {
        guard let info = n.userInfo,
            let msg = info[SMManager.outedOwnerGroupNotification] as? Message,
            msg.body.userId == User.default.user_id,
            !msg.body.groupId.isEmpty else {
            return
        }
        
        if HomePageModel.listShowType == .roomType || !HomePageModel.showOwnerGroup {
            // 样式2 或 不显示自建群时
            return
        }
        
        // 根据group_id查找对应群的item
        guard let item = self.homeModel.lists.first(where: {$0.id == msg.body.groupId}) else {
            return
        }
        
        let alert = UIAlertController(title: "提示", message: "您已被移除群聊\"\(item.name)\"", preferredStyle: .alert)
        let ok = UIAlertAction(title: "知道了", style: .default) { (action) in
            self.homeModel.deleteOwnerGroup(msg.body.groupId) {
                // 处理完 刷新列表
                self.tableView.reloadData()
            }
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - 网络请求
extension FEGroupViewController {
    //MARK: 开始网络请求(进来加载一次)
    private func startLoadData() {
        let load = QMUITips.showLoading(in: self.view)
        self.homeModel.loadAllData {
            load.hide(animated: true, afterDelay: 0.2)
            
            if self.homeModel.ads.count > 0 {
                self.cyclePictureView.pictures = self.homeModel.ads.map({$0.url})
            }
            
            if self.homeModel.notices.count > 0 {
                self.labelCycleView.pictures = self.homeModel.notices.map({$0.title})
            }
            
            self.tableView.tableHeaderView = self.headContentBackView
            
            if HomePageModel.listShowType == .allTypes {
                self.refreshGroupTypeBar()
            }
//            if self.homeModel.selectGroupType != -1 {
//                self.refreshGroupTypeBar()
//            }
            self.tableView.reloadData()
        }
    }
    
    //MARK: 下拉刷新
    @objc private func refreshListData() {
        self.homeModel.refreshGroupList {
            self.mj_head.endRefreshing()
            self.tableView.reloadData()
        }
    }
}


//MARK: - 点击事件
extension FEGroupViewController {
    //MARK: 点击群类型菜单按钮
    @objc func groupTypeSelectedAction(_ sender: QMUIButton) {
        let currentSelect = self.homeModel.selectGroupType
        if sender.tag == currentSelect {
            // 过滤重复点击
            return
        }

        // 把旧选中的按钮置为非选中状态
        groupTypeButtons[currentSelect].isSelected = false
        // 当前点击的按钮置为选中状态
        sender.isSelected = true
        // 标记当前选中的index
        self.homeModel.selectGroupType = sender.tag
        
        // 筛选对应的数据及刷新列表
        self.homeModel.dowithListData {
            self.tableView.reloadData()
        }
    }
    
    //MARK: 点击客服
    @objc func customerAction() {
        ContactCustomer.startContact(currentvc: self, enterChat: true)
    }
    
    //MARK: 点击建群
    @objc func createGroupAction() {
        self.push(to: OwnerGroupCreateViewController())
    }
    
    //MARK: 点击轮播图
    func clickBanner(_ index: Int) {
        guard index < self.homeModel.ads.count else {
            return
        }
        
        let item = self.homeModel.ads[index]
        let vc = FEBaseWebViewController()
        vc.url = "\(item.link_url)"
        vc.title = "\(item.title)"
        self.push(to: vc)
    }
    
    //MARK: 点击公告
    func clickScrollLabel(_ index: Int) {
        // 检测数组溢出
        guard index >= 0, index < self.homeModel.notices.count else {
            return
        }
        // 弹框
        let content = self.homeModel.notices[index].content
        let ac = QMUIAlertController(title: "公告", message: content, preferredStyle: .alert)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.center
        ac.alertTitleAttributes = [.font: UIFont.systemFont(ofSize: 18),
                                   .foregroundColor: UIColor.black,
                                   .paragraphStyle: paragraph]
        let a1 = QMUIAlertAction(title: "确定", style: .default)
        ac.addAction(a1)
        ac.showWith(animated: true)
    }
}



// MARK: - QMUINavigationTitleViewDelegate
extension FEGroupViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FEGroupViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.registerAllCells()
        tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(refreshListData))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - (self.tabBarController?.tabBar.height ?? 0))
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if HomePageModel.listShowType == .roomType {
            return homeModel.lists2.count
        }
        return self.homeModel.lists.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if HomePageModel.listShowType == .roomType {
            return GroupListCell2.height()
        }
        return 80
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if HomePageModel.listShowType == .allTypes, self.homeModel.groupTypeDatas.count > 0 {
            return self.groupTypeBar.height
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if HomePageModel.listShowType == .allTypes, self.homeModel.groupTypeDatas.count > 0 {
            return self.groupTypeBar
        }
        return nil
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if HomePageModel.listShowType == .roomType {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupListCell2", for: indexPath) as! GroupListCell2
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withClass: ClassCollectionViewCell.self, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if HomePageModel.listShowType == .roomType {
            let cell0 = cell as! GroupListCell2
            let item = self.homeModel.lists2[indexPath.row]
            cell0.update(item.types, item.title, item.type)
            
            cell0.clickHandler = {(index) in
                // 进入群组列表
                let vc = FEGroupDetailListViewController()
                vc.model.groupType = item.types[index]
                vc.model.isSystem = item.type == "1" ? true : false
                self.push(to: vc, animated: true, completion: nil)
                
                self.enterGroupListType = (item.type, item.types[index].id)
            }
        } else {
            let cell0 = cell as! ClassCollectionViewCell
            let item = self.homeModel.lists[indexPath.row]
            cell0.imagev.setImage(with: URL(string: item.img))
            cell0.titleLabel.text = item.name
            cell0.detailLabel.text = item.know
            
            // 判断有没有新消息提醒
            MessageTipsManager.shared.isContains(id: item.id, .group) { (t) in
                var showTips = false
                if t.1 > 0 {
                    showTips = true
                }
                cell0.dotView.isHidden = !showTips
            }
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if HomePageModel.listShowType == .roomType {
            return
        }
        self.didselectCellAction(indexPath.row)
    }
}

//MARK: - 点击cell，进入群聊
extension FEGroupViewController {
    // didselect cell
    private func didselectCellAction(_ index: Int) {
        let model = self.homeModel.lists[index]
        
        if !self.isNeedPasswd(model) {
            // 不需密码 直接请求数据进群
            self.doRequestRoomDetail(model)
            return
        }
        
        self.inputGroupPasswd { (input) in
            if input.isEmpty {
                QMUITips.show(withText: "密码错误", in: self.view)
                return
            }
            
            self.doRequestRoomDetail(model, input)
        }
    }
    
    //MARK: 请求群组详情数据，进入群聊界面
    private func doRequestRoomDetail(_ item: GroupList, _ input: String? = nil) {
        let load = QMUITips.showLoading(in: self.view)
        HomePageModel.getRoomDetail(item, input) { (flag, msg, data) in
            load.hide(animated: true)
            
            if flag == false {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            
            self.enterChatGroup = item.id
            
            if item.is_system == "1" {
                // 进入系统群
                let vc = FEChatGroupViewController(chat_id: item.id)
                vc.groupItem = item
                vc.roomMessage = data
                self.push(to: vc)
                return
            }
            
            // 进入自建群
            let vc = FEChatOwnerGroupViewController(chat_id: item.id)
            vc.groupItem = item
            if let detail = data {
                vc.groupDetail = detail
            }
            self.push(to: vc)
        }
    }
    
    //MARK: 判断点击的群组是否需要输入密码
    private func isNeedPasswd(_ item: GroupList) -> Bool {
        // 不需要输密码：群主或未设置
        if item.is_owner == "1" || item.room_passwd != "1" {
            return false
        }
        return true
    }
    
    
    //MARK: 弹框提示输入密码
    private func inputGroupPasswd(_ handler: @escaping ((_ input: String)->Void)) {
        let ac = UIAlertController(title: "密码验证", message: nil, preferredStyle: .alert)
        ac.addTextField { (tf) in
            tf.placeholder = "请输入群组密码"
            tf.isSecureTextEntry = true
            tf.keyboardType = .numberPad
            tf.returnKeyType = .done
        }
        
        let ok = UIAlertAction(title: "确定", style: .default) { (action) in
            var input = ""
            if let tfs = ac.textFields, let tf = tfs.first, let text = tf.text, text.count > 0 {
                input = text
            }
            handler(input)
        }
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        ac.addAction(ok)
        ac.addAction(cancel)
        self.present(ac)
    }
}
