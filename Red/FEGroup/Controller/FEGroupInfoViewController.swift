//
//  FEGroupInfoViewController.swift
//  Red
//
//  Created by MAC on 2019/10/28.
//  Copyright © 2019 MAC. All rights reserved.
//


import UIKit
import QMUIKit

/// 群组信息界面
class FEGroupInfoViewController: YYTableViewController {
    
    var message: roomMessage?
    var groupItem: GroupList?
    

    /// 清空聊天记录block
    var clearChatRecordsHandler: (()->Void)?
    
    lazy var listData:[(String,String,Bool,Bool)] = {
        var d:[(String,String,Bool,Bool)] = []
        d.append(("群聊名称",message?.room_name ?? "",true,false))
        d.append(("群公告",message?.room_notice ?? "",true,false))
        d.append(("须知",message?.room_know ?? "",true,false))
        d.append(("群规",message?.room_rule ?? "",true,false))
        d.append(("玩法","",false,true))
        return d
    }()
    
    lazy var headview: GroupInfoTopView = {
        var rect = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 0)
        let v = GroupInfoTopView(frame: rect)
        rect.size.height = v.currentHeight
        v.frame = rect
        v.allMemberHandler = {[weak self] in
            self?.allMemberHandler()
        }
        v.avatarButtonHandler = {(index) in
            
        }
        return v
    }()
    
    lazy var footview: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 140)
        
        clearButton.frame = CGRect(x: 20, y: 20, width: self.tableView.frame.size.width-20*2, height: 40)
        v.addSubview(clearButton)
        
        exitButton.frame = CGRect(x: 20, y: clearButton.bottom+20, width: self.tableView.frame.size.width-20*2, height: 40)
        v.addSubview(exitButton)
        
        v.height = exitButton.bottom + 20
        return v
    }()
    
    private lazy var clearButton: QMUIButton = {
        let v = QMUIButton("清空聊天记录", fontSize: 15)
        v.backgroundColor = .white
        v.setTitleColor(Color.red, for: .normal)
        v.addTarget(self, action: #selector(clearButtonAction), for: .touchUpInside)
        v.layer.cornerRadius = 2
        return v
    }()
    
    lazy var exitButton: QMUIButton = {
        let v = QMUIButton("删除并退出", fontSize: 18)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.addTarget(self, action: #selector(exitButtonAction), for: .touchUpInside)
        v.layer.cornerRadius = 2
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "群组信息"
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        createUI()
        
        if let msg = message {
            headview.update(msg.room_user)
        }
        
        self.tableView.reloadData()
    }
    
    func createUI() {
        
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        tableView.backgroundColor = .lightText
        tableView.registerCell(withClass: GroupInfoCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FEGroupInfoViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
    
    //MARK: clear button action
    @objc func clearButtonAction() {
        let load = QMUITips.showLoading("清理中...", in: self.view)
        // 清理会话
        self.clearChatRecordsHandler?()
        SMManager.manager.deleteConversation(cid: self.groupItem!.id, isGroup: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            load.hide(animated: true)
            QMUITips.show(withText: "清理完毕", in: self.view)
        }
    }
    
    //MARK: exit button action
    @objc func exitButtonAction() {
        guard let msg = groupItem else {
            return
        }
        
        HttpClient.default.outGroup(param: msg.id) {[weak self] (status, text) in
            if !status {
                return
            }
            
            SMManager.manager.deleteConversation(cid: msg.id, isGroup: true)
            self?.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func allMemberHandler() {
        let vc = GroupAllMemberViewController()
        vc.roomId = groupItem?.id
        self.push(to: vc)
    }
}

extension FEGroupInfoViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: GroupInfoCell.self)
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.RGBColor(240, 240, 240)
        self.tableView.tableHeaderView = headview
        self.tableView.tableFooterView = footview
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50*kScreenScale
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: GroupInfoCell.self, for: indexPath)
        let t = listData[indexPath.row]
        cell.update(t.0, t.1, t.2, t.3)
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == listData.count-1 {
            // 玩法
            guard let item = groupItem, item.playMethodItem.url.hasPrefix("http") else {
                return
            }
            let vc = FEBaseWebViewController()
            vc.url = item.playMethodItem.url
            vc.title = item.playMethodItem.title
            self.push(to: vc)
        }
    }
}
