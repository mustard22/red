//
//  OwnerGroupInviteItem.swift
//  Red
//
//  Created by MAC on 2020/1/16.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

/// 自建群邀请成员列表item
public struct OwnerGroupInviteItem {
    var uid = ""
    var name = ""
    var avatar = ""
    /// 选中状态
    var flag = false
}
