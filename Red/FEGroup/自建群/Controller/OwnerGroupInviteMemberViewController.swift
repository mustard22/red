//
//  OwnerGroupInviteMemberViewController.swift
//  Red
//
//  Created by MAC on 2020/1/16.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

// 邀请成员界面
class OwnerGroupInviteMemberViewController: YYTableViewController {
    //MARK: - private vars

    private lazy var searchView: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: tableView.width, height: 50)
        
        let content = QMUIButton("搜索", fontSize: 15)
        content.frame = CGRect(x: 10, y: 10, width: v.width - 20, height: v.height-20)
        content.setTitleColor(.darkGray, for: .normal)
        content.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        content.layer.masksToBounds = true
        content.layer.cornerRadius = 2.0
        content.backgroundColor = .white
        v.addSubview(content)
        
        let img = UIImageView()
        img.image = UIImage(named: "icon_nav_search")
        img.frame = CGRect(x: content.width/2 - 40, y: (content.height-18)/2, width: 18, height: 18)
        content.addSubview(img)
        
        tableView.tableHeaderView = v
        return v
    }()
    
    // 右上角按钮（群房间）
    private lazy var rightTopButton: QMUIButton = {
       let v = QMUIButton("完成", fontSize: 14)
       v.setTitle("完成", for: .disabled)
       v.size = CGSize(width: 60, height: 40)
       v.backgroundColor = Color.red
       v.setTitleColor(.white, for: .normal)
        v.setTitleColor(.gray, for: .disabled)
       v.addTarget(self, action: #selector(handlerRightButton), for: .touchUpInside)
       v.isEnabled = false
       return v
    }()
    
    // 刷新table用的数据
    private var tempDatas: [OwnerGroupInviteItem] = []
    // 转换过滤后的数据
    private var datas = [OwnerGroupInviteItem]()
    
    //MARK: - public vars
    /// 是否群主
    public var isGroupAdmin = false
    /// 当前群成员
    public var members: [RoomUserItem] = []
    /// 群ID
    public var groupId: String = ""
    
    public var inviteMemberHandler: ((_ members: [RoomUserItem])->Void)?
    
    
     override func viewDidLoad() {
        super.viewDidLoad()
         
        if isGroupAdmin {
            self.rightTopButton.isEnabled = false
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTopButton)
        }
        
        self.title = "邀请好友"
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        self.registerAllCells()
        
        loadAllowInviteFriends()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        self.registerAllCells()
        tableView.reloadData()
    }
}

//MARK: 按钮事件
extension OwnerGroupInviteMemberViewController {
    // click search button
    @objc func searchAction() {
        let sv = FESearchView(searchType: .ownerGroupInviteFriend, groupId: self.groupId)
        sv.show()
        
        sv.selectHandler = {(obj) in
            let item = obj as! OwnerGroupInviteItem
            self.inviteFriendJoinGroup(uid: item.uid)
        }
    }

    @objc func handlerRightButton() {
        if !isGroupAdmin {
            return
        }
        
        // 群主强邀请
        let members = tempDatas.filter({$0.flag == true})
        let uids = members.map({$0.uid}).joined(separator: ",")
        if uids.isEmpty {
            return
        }
        
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.ownerGroupInviteMember(group_id: self.groupId, user_id: uids) { (flag, msg) in
            load.hide(animated: true)
            QMUITips.show(withText: msg)
            if !flag {
                return
            }
            
            self.close()
            let selects = members.map({(tmp)->RoomUserItem in
                var m = RoomUserItem()
                m.user_id = tmp.uid
                m.nickname = tmp.name
                m.avatar = tmp.avatar
                return m
            })
            
            self.inviteMemberHandler?(selects)
        }
    }
}

//MARK: 请求接口
extension OwnerGroupInviteMemberViewController {
    private func loadAllowInviteFriends() {
        let p = OwnerGroupInviteFriendListParam(groupId: self.groupId, pageIndex: "1", pageSize: "\(self.loadCount)", keyWord: "")
        HttpClient.default.ownerGroupAllowInviteFriends(param: p) { (item) in
            guard let items = item.data else {
                return
            }
            
            self.datas.removeAll()
            self.datas = items.map({(fitem) -> OwnerGroupInviteItem in
                var item = OwnerGroupInviteItem()
                item.name = fitem.friend_nickname
                item.uid = fitem.friend_id
                item.avatar = fitem.friend_avatar
                return item
            })
            
            self.tempDatas = self.datas
            self.tableView.reloadData()
            
            if items.count >= self.loadCount {
                self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreAllowInviteFriends))
            }
        }
    }
    
    @objc private func loadMoreAllowInviteFriends() {
        let index = datas.count/self.loadCount + 1
        let p = OwnerGroupInviteFriendListParam(groupId: self.groupId, pageIndex: "\(index)", pageSize: "\(self.loadCount)", keyWord: "")
        HttpClient.default.ownerGroupAllowInviteFriends(param: p) { (item) in
            guard let items = item.data else {
                return
            }
            self.datas.append(contentsOf: items.map({(fitem) -> OwnerGroupInviteItem in
                var item = OwnerGroupInviteItem()
                item.name = fitem.friend_nickname
                item.uid = fitem.friend_id
                item.avatar = fitem.friend_avatar
                return item
            }))
            self.tempDatas = self.datas
            self.tableView.reloadData()
            
            if items.count >= self.loadCount {
                self.mj_foot.state = .idle
            } else {
                self.tableView.mj_footer = nil
            }
        }
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension OwnerGroupInviteMemberViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

//MARK: table delegate & datasource
extension OwnerGroupInviteMemberViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.registerAllCells()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = UIScreen.main.bounds
        searchView.frame = CGRect(x: 0, y: 0, width: tableView.width, height: 50)
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.tempDatas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    private func registerAllCells() {
        self.tableView.register(UINib(nibName: "OwnerGroupInviteMemberCell", bundle: nil), forCellReuseIdentifier: "OwnerGroupInviteMemberCell")
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerGroupInviteMemberCell", for: indexPath) as! OwnerGroupInviteMemberCell
        
        let item = tempDatas[indexPath.row]
        cell.update(isSelected: item.flag, avatar: item.avatar, name: item.name)
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didselectCell(selectRow: indexPath)
    }
    
    private func didselectCell(selectRow indexPath: IndexPath) {
        if isGroupAdmin {
            tempDatas[indexPath.row].flag = !tempDatas[indexPath.row].flag
            tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
            
            let count = tempDatas.filter({$0.flag == true}).count
            if count > 0 {
                self.rightTopButton.isEnabled = true
                self.rightTopButton.setTitle("完成(\(count))", for: .normal)
            } else {
                self.rightTopButton.isEnabled = false
            }
            return
        }
        
        inviteFriendJoinGroup(uid: self.tempDatas[indexPath.row].uid)
    }
    
    private func inviteFriendJoinGroup(uid: String) {
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.ownerGroupShareGroup(group_id: self.groupId, user_id: uid) { (flag, msg) in
            load.hide(animated: true)
            QMUITips.show(withText: msg)
            if flag {
                self.close()
            }
        }
    }
    
    internal override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}
