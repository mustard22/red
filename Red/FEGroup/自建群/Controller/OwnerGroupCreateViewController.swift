//
//  OwnerGroupCreateViewController.swift
//  Red
//
//  Created by MAC on 2020/1/11.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class OwnerGroupCreateViewController: FEBaseViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var avatarButton: UIButton! // 头像按钮
    @IBOutlet weak var avatarImageView: UIImageView! // 头像图片
    @IBOutlet weak var typeButton: UIButton! // 房间类型按钮
    @IBOutlet weak var levelButton: UIButton! // 等级按钮
    @IBOutlet weak var selectButton: UIButton! // 密码开关按钮
    @IBOutlet weak var payRateButton: UIButton! // 发包配置按钮
    @IBOutlet weak var sendButton: UIButton! // 提交按钮
    @IBOutlet weak var nameTextField: UITextField! // 名称
    @IBOutlet weak var knowTextField: UITextField! // 须知
    @IBOutlet weak var ruleTextField: UITextField! // 规则
    @IBOutlet weak var noticeTextField: UITextField! // 公告
    @IBOutlet weak var minNumberTextField: UITextField! // 最小发包数
    @IBOutlet weak var maxNumberTextField: UITextField! // 最大发包数
    @IBOutlet weak var minMoneyTextField: UITextField! // 最小金额数
    @IBOutlet weak var maxMoneyTextField: UITextField! // 最大发包数
    @IBOutlet weak var passwdTextField: UITextField! // 密码
    @IBOutlet weak var balanceTextField: UITextField! // 余额
    
    //MARK: 自定义view
    /// 建群按钮
    private lazy var rightTop: QMUIButton = {
        let v = QMUIButton("说明", fontSize: 15)
        v.setTitleColor(.white, for: .normal)
        v.showsTouchWhenHighlighted = true
        v.size = CGSize(width: 70, height: 40)
        v.addTarget(self, action: #selector(rightTopButtonAction), for: .touchUpInside)
        return v
    }()
    
    //MARK: private vars
    /// 选中的房间类型
    private var roomType = ""
    /// 入群等级
    private var roomLevel = ""
    /// 发包配置（普通房传0，其他类型房间根据选择传）
    private var payRate = "0"
    
    /// 建群说明结构
    private var contentRule: OwnerGroupCreateRuleData?
    /// 房间类型数据
    private var typeDatas: [GroupType] = []
    /// 等级数据
    private var levelDatas: [ProxyListLevelItem] = []
    /// 雷房类型发包配置数据
    private var thunderPayRateDatas: [String]?
    /// 牛房类型发包配置数据
    private var niuPayRateDatas: [String]?
    
    /// 标记当前选中的输入框
    private var currentInput: UITextField?
    /// 是否创建群模式（true：创建群， false：编辑群）
    private var isCreateGroup = true
    /// 标记输入前的offset
    private var lastOffsetY: CGFloat = 0
    
    /// 创建群聊参数结构体
    private var createParam = OwnerCreateParam()
    
    
    //MARK: public vars
    /// 当前群信息(只有编辑群信息时传)
    public var message: roomMessage?
    public var groupItem: GroupList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        registerAllNotifications()
        
        self.getRoomTypeData()
        self.getRoomLevelData()
        self.getOwnerRule()
    }
    
    deinit {
        removeAllNotifications()
    }
}

//MARK: UI
extension OwnerGroupCreateViewController {
    func setupUI() {
        contentScroll.contentSize = CGSize(width: 0, height: sendButton.bottom + 50)
        self.lastOffsetY = self.contentScroll.contentOffset.y
        
        let qmtfs = [nameTextField, knowTextField, noticeTextField, ruleTextField, minNumberTextField, maxNumberTextField, minMoneyTextField, maxMoneyTextField, passwdTextField, balanceTextField]
        _ = qmtfs.map { (tf) in
            tf?.qmui_borderWidth = 1.0
            tf?.qmui_borderColor = UIColor.RGBColor(200, 200, 200)
            tf?.qmui_borderPosition = .bottom
        }
        
        sendButton.layer.cornerRadius = 5.0
        
        if let msg = self.message, let item = self.groupItem {
            self.isCreateGroup = false
            
            self.title = "编辑房间信息"
            
            self.createParam.group_id = item.id
            
            // edit room
            nameTextField.text = msg.room_name
            noticeTextField.text = msg.room_know
            knowTextField.text = msg.room_notice
            ruleTextField.text = msg.room_rule
            balanceTextField.text = msg.balance_limit
            
            minMoneyTextField.text = msg.min_money
            maxMoneyTextField.text = msg.max_money
            
            minNumberTextField.text = msg.min_package_num
            maxNumberTextField.text = msg.max_package_num
            
            if item.room_passwd == "0" {
                selectButton.isSelected = false
                passwdTextField.text = ""
                passwdTextField.isEnabled = false
            } else {
                selectButton.isSelected = true
                passwdTextField.text = ""
                passwdTextField.isEnabled = true
            }
            
            avatarImageView.setImage(with: URL(string: item.img), placeHolder: avatarImageView.image)
            
            typeButton.isEnabled = false
            
            self.roomType = item.class
            self.roomLevel = msg.level_limit

            payRateButton.superview!.isHidden = true
            if item.class == "2" {
                minNumberTextField.superview!.isHidden = false
                ruleTextField.isEnabled = true
            } else {
                minNumberTextField.superview!.isHidden = true
                ruleTextField.isEnabled = false
            }
        } else {
            // create room
            self.isCreateGroup = true
            self.title = "创建房间"
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTop)
        }
    }
}

extension OwnerGroupCreateViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.lastOffsetY = scrollView.contentOffset.y
    }
}

//MARK: - keyboard notifications
extension OwnerGroupCreateViewController {
    private func registerAllNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }
    
    private func removeAllNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc fileprivate func keyboardWillChangeFrameNotification(_ not:NSNotification) {
        guard let info = not.userInfo as NSDictionary?,
            let value = info.object(forKey: "UIKeyboardFrameEndUserInfoKey") as! NSValue? else {
            return
        }
        
        guard let focusView = self.currentInput else {
            return
        }
        
        self.lastOffsetY = self.contentScroll.contentOffset.y
        
        let keyboardRect = value.cgRectValue
        let rect = focusView.convert(focusView.bounds, to: self.view)
        let offset = rect.origin.y + rect.size.height - keyboardRect.origin.y
        if offset > 0 {
            self.contentScroll.setContentOffset(CGPoint(x: 0, y: self.lastOffsetY+offset), animated: true)
        }
    }
    
    @objc fileprivate func keyboardWillShow(_ not:NSNotification) {
        guard let info = not.userInfo as NSDictionary?,
            let value = info.object(forKey: "UIKeyboardFrameEndUserInfoKey") as! NSValue? else {
            return
        }
        
        guard let focusView = self.currentInput else {
            return
        }
        
        self.lastOffsetY = self.contentScroll.contentOffset.y
        
        let keyboardRect = value.cgRectValue
        let rect = focusView.convert(focusView.bounds, to: self.view)
        let offset = rect.origin.y + rect.size.height - keyboardRect.origin.y
        if offset > 0 {
            self.contentScroll.setContentOffset(CGPoint(x: 0, y: self.lastOffsetY+offset), animated: true)
        }
    }
    
    @objc fileprivate func keyboardWillHide(_ not:NSNotification) {
        self.contentScroll.setContentOffset(CGPoint(x: 0, y: self.lastOffsetY), animated: true)
    }
}

//MARK: - button actions
extension OwnerGroupCreateViewController {
    //MARK: 建群说明
    @objc func rightTopButtonAction() {
        var title = "建群说明"
        var content = ""
        if let item = self.contentRule {
            title = item.title
            content = item.content
        }
        
        let titleStyle = NSMutableParagraphStyle()
        titleStyle.alignment = .center
        
        let msgStyle = NSMutableParagraphStyle()
        msgStyle.alignment = .left
        
        let ac = QMUIAlertController(title: title, message: content, preferredStyle: .alert)
        ac.alertMessageAttributes = [.font: UIFont.systemFont(ofSize: 14),
        .foregroundColor: UIColor.darkGray,
        .paragraphStyle: msgStyle]
        ac.alertTitleAttributes = [.font: UIFont.systemFont(ofSize: 18),
                                   .foregroundColor: UIColor.black, .paragraphStyle: titleStyle]
        
        let a1 = QMUIAlertAction(title: "知道了", style: .default)
        ac.addAction(a1)
        ac.showWith(animated: true)
    }
    
    //MARK: scroll上的按钮事件
    @IBAction func buttonAction(_ sender: Any) {
        self.view.endEditing(true)
        
        switch sender as! UIButton {
        case avatarButton:
            dowithSelectPicture()
        case typeButton:
            dowithSelectRoomType()
        case levelButton:
            dowithSelectRoomLevel()
        case selectButton:
            dowithOpenInputPasswd()
        case payRateButton:
            dowithSelectPayRate()
        case sendButton:
            dowithSubmitAction()
        default: break
        }
    }
    
    //MARK: 选择图片
    private func dowithSelectPicture() {
        FESelectPhotoManager.selectPhoto(with: self) {[weak self] (imgs) in
            guard let self = self, let image = imgs.first else {
                return
            }
            
            self.avatarImageView.image = FESelectPhotoManager.scaleToSize(image: image, size: CGSize(width: 60, height: 60))
        }
    }
    
    //MARK: 选择房间类型
    private func dowithSelectRoomType() {
        let view = FECenterLayerTableView.instance(typeDatas.map({$0.name})) { (index) in
            let item = self.typeDatas[index]
            self.typeButton.setTitle(item.name, for: .normal)
            
            if self.roomType == item.id {
                return
            }
            
            self.roomType = item.id
            
            guard let type = RoomType.type(value: item.id, isOwner: true) else {
                return
            }
            
            switch type {
            case .welfare, .general, .solitaire:
                self.payRateButton.superview!.isHidden = true
                self.minNumberTextField.superview!.isHidden = false
//                self.sendButton.top -= self.payRateButton.superview!.height
                self.payRate = "0"
                self.ruleTextField.text = ""
                self.ruleTextField.isEnabled = true
            default:
                self.ruleTextField.isEnabled = false
                self.ruleTextField.text = "请查看该类系统房群规"
                self.minNumberTextField.superview!.isHidden = true
                self.payRateButton.setTitle("选择赔率", for: .normal)
                self.getRedPaperConfig(type: item.id)
                
                self.payRateButton.superview!.isHidden = false
//                self.sendButton.top += self.payRateButton.superview!.height
            }
            
//            if item.id == "2" {
//                // 普通群
//                self.payRateButton.superview!.isHidden = true
//                self.minNumberTextField.superview!.isHidden = false
////                self.sendButton.top -= self.payRateButton.superview!.height
//                self.payRate = "0"
//                self.ruleTextField.text = ""
//                self.ruleTextField.isEnabled = true
//            } else {
//                self.ruleTextField.isEnabled = false
//                self.ruleTextField.text = "请查看该类系统房群规"
//                self.minNumberTextField.superview!.isHidden = true
//                self.payRateButton.setTitle("选择赔率", for: .normal)
//                self.getRedPaperConfig(type: item.id)
//
//                self.payRateButton.superview!.isHidden = false
////                self.sendButton.top += self.payRateButton.superview!.height
//            }
        }
        view.show()
    }
    
    //MARK: 选择入群等级
    private func dowithSelectRoomLevel() {
        let view = FECenterLayerTableView.instance(self.levelDatas.map({$0.alias})) { (index) in
            let item = self.levelDatas[index]
            self.levelButton.setTitle(item.alias, for: .normal)
            
            if self.roomLevel == item.level {
                return
            }
            
            self.roomLevel = item.level
            
        }
        view.show()
    }
    
    //MARK: 输入密码开关
    private func dowithOpenInputPasswd() {
        selectButton.isSelected = !selectButton.isSelected
        passwdTextField.isEnabled = selectButton.isSelected
        if selectButton.isSelected {
            passwdTextField.becomeFirstResponder()
            passwdTextField.text = ""
        } else {
            passwdTextField.resignFirstResponder()
        }
    }
    
    //MARK: 选择赔率
    private func dowithSelectPayRate() {
        guard !self.roomType.isEmpty else {
            QMUITips.show(withText: "请选择房间类型", in: self.view)
            return
        }
        
        var datas: [String]!
        if self.roomType == "1" {
            datas = self.niuPayRateDatas
        } else {
            datas = self.thunderPayRateDatas
        }
        
        let layer = FECenterLayerTableView.instance(datas) { (index) in
            self.payRateButton.setTitle(datas[index], for: .normal)
            self.payRate = "\(index)"
        }
        layer.defaultRowHeight = 70
        layer.show()
    }
    
    //MARK: 提交事件（创建房间）
    private func dowithSubmitAction() {
        guard let avatar = avatarImageView.image else {
            QMUITips.show(withText: "请选择头像", in: self.view)
            return
        }
        
        guard let name = nameTextField.text, !name.isEmpty else {
            QMUITips.show(withText: "请输入房间名称", in: self.view)
            return
        }
        
        guard !roomType.isEmpty else {
            QMUITips.show(withText: "请选择房间类型", in: self.view)
            return
        }
        
        guard !roomLevel.isEmpty else {
            QMUITips.show(withText: "请选择入群等级", in: self.view)
            return
        }
        
        guard let notice = noticeTextField.text, !notice.isEmpty else {
            QMUITips.show(withText: "请输入房间公告", in: self.view)
            return
        }
        
        guard let know = knowTextField.text, !know.isEmpty else {
            QMUITips.show(withText: "请输入房间须知", in: self.view)
            return
        }
        
        guard let rule = ruleTextField.text, !know.isEmpty else {
            QMUITips.show(withText: "请输入群规", in: self.view)
            return
        }
        
        guard let minMoney = minMoneyTextField.text, !minMoney.isEmpty else {
            QMUITips.show(withText: "请输入最小发包金额", in: self.view)
            return
        }
        
        guard let maxMoney = maxMoneyTextField.text, !maxMoney.isEmpty else {
            QMUITips.show(withText: "请输入最大发包金额", in: self.view)
            return
        }
        
        guard let mim = Double(minMoney), let mam = Double(maxMoney), mim < mam else {
            QMUITips.show(withText: "输入的发包金额不合法", in: self.view)
            return
        }
        
        if selectButton.isSelected, let passwd = passwdTextField.text, passwd.isEmpty {
            QMUITips.show(withText: "请输入房间密码", in: self.view)
            return
        }
        
        if let superView = minNumberTextField.superview, superView.isHidden == false {
            guard let minNumber = minNumberTextField.text, !minNumber.isEmpty else {
                QMUITips.show(withText: "请输入最小发包数量", in: self.view)
                return
            }
            
            guard let maxNumber = maxNumberTextField.text, !maxNumber.isEmpty else {
                QMUITips.show(withText: "请输入最大发包数量", in: self.view)
                return
            }
            
            guard let minnum = Double(minNumber), minnum >= 1, let maxnum = Double(maxNumber), maxnum >= 1 else {
                QMUITips.show(withText: "发包数量不能为0", in: self.view)
                return
            }
            
            if minnum >= maxnum {
                QMUITips.show(withText: "最大发包数必须大于最小发包数", in: self.view)
                return
            }
        }
        
        
        if !self.payRateButton.superview!.isHidden, payRate.isEmpty {
            QMUITips.show(withText: "请选择发包赔率", in: self.view)
            return
        }
        
        createParam.name = name
        createParam.know = know
        createParam.notice = notice
        createParam.min_money = minMoney
        createParam.max_money = maxMoney
        
        createParam.min_package_num = minNumberTextField.text ?? "5"
        createParam.max_package_num = maxNumberTextField.text ?? "10"
        
        createParam.room_password = passwdTextField.isEnabled ? passwdTextField.text! : "0"
        createParam.room_class = self.roomType
        createParam.level_limit = self.roomLevel
        createParam.user_id = User.default.user_id
        createParam.room_packet_count = self.payRate
        createParam.rule = rule
        createParam.balance_limit = self.balanceTextField.text ?? "0"
        
        let loadView = QMUITips.showLoading("提交...", in: self.view)
        self.uploadImage(avatar) {[weak self] (url) in
            guard let self = self else {
                return
            }
            
            if url.isEmpty {
                loadView.hide(animated: true)
                QMUITips.show(withText: "上传头像失败", in: self.view)
                return
            }
            
            self.createParam.img = url
            self.submitParamToCreateRoom(loading: loadView)
        }
        
    }
}

//MARK: - 接口请求
extension OwnerGroupCreateViewController {
    //MARK: 上传头像
    func uploadImage(_ img: UIImage, _ handler:((_ url: String)->Void)?) {
        var item = UploadParam.init()
        item.file = img.jpegData(compressionQuality: 0.8)
        UserCenter.default.uploadFile(item) {(res) in
            handler?(res?.path ?? "")
        }
    }
    
    //MARK: 建群的规则说明
    private func getOwnerRule() {
        if !self.isCreateGroup {
            return
        }
        HttpClient.default.ownerGroupGetOwnerRule { (item) in
            self.contentRule = item
        }
    }
    
    //MARK: 获取房间类型数据
    private func getRoomTypeData() {
        HttpClient.default.getGroupType { (flag, text, items) in
            self.typeDatas = items
            if let index = self.typeDatas.firstIndex(where: {$0.id == "2"}) {
                self.typeDatas[index].name = "普通房"
                
                if !self.isCreateGroup, let index = self.typeDatas.firstIndex(where: {$0.id == self.groupItem!.class})   {
                    self.typeButton.setTitle(self.typeDatas[index].name, for: .disabled)
                }
            }
        }
    }
    
    //MARK: 获取等级数据
    private func getRoomLevelData() {
        HttpClient.default.getLevelList { (flag, items) in
            self.levelDatas = items
            
            if !self.isCreateGroup, let item = self.levelDatas.filter({$0.level == self.message!.level_limit}).first {
                self.levelButton.setTitle(item.alias, for: .normal)
            }
        }
    }
    
    //MARK: 获取赔率数据
    private func getRedPaperConfig(type: String) {
        if !self.isCreateGroup {
            return
        }
        
        guard let etype = RoomType.type(value: type, isOwner: true) else {
            return
        }
        
        if etype == .cattle, self.niuPayRateDatas != nil {
            return
        }
        
        if (etype == .singleThunder || etype == .multipleThunder), self.thunderPayRateDatas != nil {
            return
        }
        
        HttpClient.default.ownerGroupRedPacketConfig(type: type) {[weak self] (flag, msg, data) in
            guard let self = self else {
                return
            }
            guard let items = data else {
                return
            }
            
            let layerTableDatas = items.map({$0.joined(separator: " ")})
            
            switch etype {
            case .cattle:
                self.niuPayRateDatas = layerTableDatas
            case .singleThunder, .multipleThunder:
                self.thunderPayRateDatas = layerTableDatas
            default: break
            }
        }
    }
    
    //MARK: 创建(编辑)房间
    private func submitParamToCreateRoom(loading: QMUITips) {
        HttpClient.default.ownerGroupCreateRoom(param: self.createParam) {[weak self] (flag, msg) in
            loading.hide(animated: true)
            guard let self = self else {
                return
            }
            
            if flag == false {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            
            // 发送通知到首页，刷新自建群列表
            NotificationCenter.default.post(name: NSNotification.Name.refreshHomeOwnerGroupList, object: nil)
            if self.isCreateGroup {
                // 创建群成功 返回到上个界面
                QMUITips.show(withText: msg)
                self.close()
            } else {
                // 修改信息成功 返回到自建群列表(home page)
                let vc = UIAlertController(title: nil, message: "群组信息修改成功，需要退出群聊再进入", preferredStyle: .alert)
                let action = UIAlertAction(title: "知道了", style: .default) { (ac) in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                vc.addAction(action)
                self.present(vc)
            }
        }
    }
}


//MARK: UITextFieldDelegate
extension OwnerGroupCreateViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.tintColor = Color.red
        textField.qmui_borderColor = Color.red
        self.currentInput = textField
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = UIColor.RGBColor(200, 200, 200)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == self.nameTextField {
            noticeTextField.becomeFirstResponder()
        } else if textField == self.noticeTextField {
            knowTextField.becomeFirstResponder()
        } else if textField == self.knowTextField {
            ruleTextField.becomeFirstResponder()
        }
        return true
    }
}
