//
//  OwnerGroupSendPaperViewController.swift
//  Red
//
//  Created by MAC on 2020/1/8.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 关于发雷包（2020 01 03）
/// 在版本v1.x上，雷号有且仅有1个；
/// 在版本 >v2.0 上，雷号可以多选，最多3个。请求接口时多个雷号用逗号隔开，拼成字符串
///
//MARK: - 发包界面
class OwnerGroupSendPaperViewController: FEBaseViewController, UITableViewDelegate {
    private lazy var table: UITableView = {
        let v = UITableView()
        v.separatorStyle = .none
        v.frame = self.view.bounds
        v.delegate = (self as UITableViewDelegate)
        v.backgroundColor = UIColor.RGBColor(240, 240, 240)
        self.view.addSubview(v)
        return v
    }()
    
    var msg: roomMessage!
    var group: GroupList!
    
    var sendType: RoomType = .cattle
    
    lazy var headview: OwnerGroupSendThunderPackageView = {
        let v = OwnerGroupSendThunderPackageView(frame: self.view.bounds)
        table.tableHeaderView = v
        v.submitHandler = {[weak self] (money,count,number) in
            var p = OwnerSendPaperParam()
            p.amount = money
            p.count = count
            p.packet_number = number
            p.group_id = (self?.group.id)!
            self?.sendPackage(p)
        }
        return v
    }()
    
    lazy var headview2: OwnerGroupSendWelfarePackageView = {
        let v = OwnerGroupSendWelfarePackageView(frame: self.view.bounds)
        table.tableHeaderView = v
        v.submitHandler = {[weak self] (money,count) in
            var p = OwnerSendPaperParam()
            p.amount = money
            p.count = count
            p.packet_number = "-1"
            p.group_id = (self?.group.id)!
            self?.sendPackage(p)
        }
        return v
    }()
    
    public var sendPackageHandler: ((_ item: GroupSendPackageItem, _ param: OwnerSendPaperParam)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch sendType {
        case .singleThunder, .multipleThunder:
            self.title = "发雷包"
        case .welfare:
            self.title = "发福利包"
        case .cattle:
            self.title = "发牛牛包"
        case .general:
            self.title = "发普通包"
        case .solitaire:
            self.title = "发接龙包"
        }
        
        createUI()
        
        // 发包界面重新获取用户信息（可用余额）
        loadUserInfo()
    }
    
    //MARK: methods
    func loadUserInfo() {
        // 获取最新用户数据
        UserCenter.default.getSectionInfo {[weak self] (status, msg) in
            self?.createUI()
        }
    }
    
    func createUI() {
        if sendType == .singleThunder {
            headview.maxSelectNumberCount = 1
            headview.freshView(msg, group.id)
        } else if sendType == .multipleThunder {
            headview.maxSelectNumberCount = 3
            headview.freshView(msg, group.id)
        }
        else {
            headview2.freshView(msg, group.id)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    
    func sendPackage(_ param: OwnerSendPaperParam) {
        guard let amount = Double(param.amount), let money = Double(User.default.balance),  amount <= money else {
            QMUITips.show(withText: "余额不足", in: self.view)
            return
        }
        
        let load = QMUITips.showLoading("发包中...", in: self.view)
        HttpClient.default.ownerGroupSendPaper(param: param) { (status, msg, item) in
            load.hide(animated: true)
            if status, let data = item {
                if let handler = self.sendPackageHandler {
                    handler(data, param)
                }
                
                var lastItem: FELastSendMoneyItem?
                if var litem = MMKVManager.shared.getCache(FELastSendMoneyItem.self, MMKVCacheKey.lastSendPaperMoney) {
                    litem.lastMoney[param.group_id] = param.amount
                    lastItem = litem
                } else {
                    lastItem = FELastSendMoneyItem(lastMoney: [param.group_id: param.amount])
                }
                MMKVManager.shared.setCache(lastItem!, MMKVCacheKey.lastSendPaperMoney)

                self.close()
            } else {
                QMUITips.show(withText: "\(msg)", in: self.view)
            }
        }
    }
}


//MARK: ----------雷包view------------
class OwnerGroupSendThunderPackageView: UIView {
    private let leftMargin: CGFloat = 10.0
    
    private lazy var topPlaceHolder: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.textColor = UIColor.RGBColor(156, 156, 156)
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 15)
        self.addSubview(v)
        return v
    }()
    
    private lazy var useMoneyView: QMUITextField = {
        let v = QMUITextField()
        v.backgroundColor = .white
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 14)
        v.isEnabled = false
        v.tintColor = Color.red
        
        let left = QMUILabel()
        left.text = "可用余额:"
        left.textColor = UIColor.RGBColor(136, 136, 136)
        left.textAlignment = .center
        left.font = UIFont.systemFont(ofSize: 13)
        left.size = CGSize(width: 80, height: 20)
        v.leftView = left
        v.leftViewMode = .always
        
        let right = QMUILabel()
        right.text = "元"
        right.textColor = UIColor.RGBColor(156, 156, 156)
        right.textAlignment = .left
        right.font = UIFont.systemFont(ofSize: 14)
        right.size = CGSize(width: 20, height: 20)
        v.rightView = right

        v.leftViewMode = .always
        v.rightViewMode = .always
        self.addSubview(v)
        return v
    }()
    
    private lazy var inputMoneyView: QMUITextField = {
        let v = QMUITextField()
        v.backgroundColor = .white
        v.keyboardType = .numberPad
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 14)
        v.placeholder = "0"
        v.tintColor = Color.red
        
        let left = QMUILabel()
        left.text = "红包金额:"
        left.textColor = UIColor.RGBColor(136, 136, 136)
        left.textAlignment = .center
        left.font = UIFont.systemFont(ofSize: 13)
        left.size = CGSize(width: 80, height: 20)
        v.leftView = left
        
        let right = QMUILabel()
        right.text = "元"
        right.textColor = UIColor.RGBColor(156, 156, 156)
        right.textAlignment = .left
        right.font = UIFont.systemFont(ofSize: 14)
        right.size = CGSize(width: 20, height: 20)
        v.rightView = right

        v.leftViewMode = .always
        v.rightViewMode = .always
        self.addSubview(v)
        return v
    }()
    
    private lazy var numberButtons: [QMUIButton] = {
        return [QMUIButton]()
    }()
    
    private lazy var redPackageButtons: [QMUIButton] = {
        return [QMUIButton]()
    }()
    
    private lazy var numberView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.size = CGSize(width: self.width-leftMargin*2, height: 200)
        self.addSubview(v)
        
        let top = QMUILabel()
        top.text = "雷号"
        top.font = UIFont.systemFont(ofSize: 17)
        top.textAlignment = .left
        top.frame = CGRect(x: 10, y: 0, width: 80, height: 40)
        v.addSubview(top)
        
        let count = 10, cols = 5, rows = 2
        let itemSize: CGFloat = 40
        let margin = (v.frame.size.width - itemSize*CGFloat(cols))/CGFloat(cols+1)
        for i in 0..<count {
            let b = QMUIButton("\(i)", fontSize: 15)
            b.setTitleColor(UIColor.RGBColor(136, 136, 136), for: .normal)
            b.setTitleColor(.white, for: .selected)
            b.tag = i
            b.size = CGSize(width: itemSize, height: itemSize)
            b.layer.borderColor = UIColor.RGBColor(136, 136, 136).cgColor
            b.layer.borderWidth = 1.0
            b.layer.masksToBounds = true
            b.layer.cornerRadius = itemSize/2
            b.addTarget(self, action: #selector(numberButtonAction(_:)), for: .touchUpInside)
            v.addSubview(b)
            numberButtons.append(b)
            
            let x = margin + CGFloat(i%cols) * (margin+itemSize)
            let y = top.height + CGFloat(i/cols) * (margin+itemSize)
            b.snp.makeConstraints { (make) in
                make.top.equalTo(y)
                make.left.equalTo(x)
                make.size.equalTo(CGSize(width: itemSize, height: itemSize))
            }
        }
        
        v.size = CGSize(width: self.width-leftMargin*2, height: top.height + (margin+itemSize)*CGFloat(rows))
        
        return v
    }()
    
    private lazy var redPackageView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.size = CGSize(width: self.width-leftMargin*2, height: 200)
        self.addSubview(v)
        
        return v
    }()
    
    private lazy var bottomTips: QMUILabel = {
        let v = QMUILabel()
        v.text = "发包没抢完将在3分钟后自动退回!"
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        v.textColor = UIColor.RGBColor(136, 136, 136)
        self.addSubview(v)
        return v
    }()
    
    private lazy var submitButton: QMUIButton = {
        let v = QMUIButton("确认发送", fontSize: 18)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.addTarget(self, action: #selector(submitButtonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    private var groupInfo: roomMessage?
    
    var submitHandler: ((_ money: String, _ count: String, _ number: String)->Void)?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var rect = frame
        rect.origin.x = leftMargin
        rect.origin.y = 0
        rect.size.width -= leftMargin*2
        rect.size.height = 40
        topPlaceHolder.frame = rect
        
        rect.origin.y += rect.size.height
        useMoneyView.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        inputMoneyView.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        rect.size.height = numberView.height
        numberView.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        rect.size.height = redPackageView.height
        redPackageView.frame = rect
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private var packageCounts: [Int] = []
    //MARK: 雷号(多个雷号，如"1,3,7")
    private var thunderNumber: [String] = []
    // 红包数量
    private var count = ""
    
    /// 最多选择雷号数(单雷类型：1，多雷类型：3)
    public var maxSelectNumberCount = 1
    
    func freshView(_ msg: roomMessage, _ room_id: String = "") {
        groupInfo = msg
        
        useMoneyView.text = User.default.balance
        
        topPlaceHolder.text = "最大金额￥\(msg.max_money)，最小金额￥\(msg.min_money)"
        
        let items = msg.room_packet.keys.sorted(by: < )
        packageCounts = items
        
        let top = QMUILabel()
        top.text = "红包数量"
        top.font = UIFont.systemFont(ofSize: 17)
        top.textAlignment = .center
        top.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        redPackageView.addSubview(top)
        
        if let litem = MMKVManager.shared.getCache(FELastSendMoneyItem.self, MMKVCacheKey.lastSendPaperMoney), let cache_money = litem.lastMoney[room_id] {
            inputMoneyView.text = cache_money
        }
        
        let count = items.count
        let cols = 2
        let rows = count/cols + (count%cols == 0 ? 0 : 1)
        let margin: CGFloat = 30
        let bsize = CGSize(width: (redPackageView.width-margin*CGFloat((cols+1)))/CGFloat(cols), height: 30)
        var index = 0
        for item in items {
            let b = QMUIButton("\(item)包\(msg.room_packet[item] ?? "")倍", fontSize: 15)
            b.setTitleColor(UIColor.RGBColor(136, 136, 136), for: .normal)
            b.setTitleColor(.white, for: .selected)
            b.tag = index
            b.size = bsize
            b.layer.borderColor = UIColor.RGBColor(136, 136, 136).cgColor
            b.layer.borderWidth = 1.0
            b.layer.masksToBounds = true
            b.addTarget(self, action: #selector(redPackageButtonAction(_:)), for: .touchUpInside)
            redPackageView.addSubview(b)
            redPackageButtons.append(b)
            
            let x = margin + CGFloat(index%cols) * (margin+bsize.width)
            let y = top.height + CGFloat(index/cols) * (margin+bsize.height)
            b.snp.makeConstraints { (make) in
                make.top.equalTo(y)
                make.left.equalTo(x)
                make.size.equalTo(bsize)
            }
            
            index += 1
        }
        
        redPackageView.height = top.height + (margin+bsize.height)*CGFloat(rows)
        
        var rect = redPackageView.frame
        rect.origin.y += rect.size.height + leftMargin
        rect.size.height = 40
        bottomTips.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        rect.size.height = 40
        submitButton.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        self.height = rect.origin.y
    }
    
    @objc func redPackageButtonAction(_ btn: QMUIButton) {
        if btn.isSelected {
            return
        }
        for b in redPackageButtons {
            if b.isSelected {
                b.isSelected = false
                b.backgroundColor = .white
                b.layer.borderColor = UIColor.RGBColor(136, 136, 136).cgColor
                break
            }
        }
        btn.isSelected = true
        btn.backgroundColor = Color.red
        btn.layer.borderColor = Color.red.cgColor
        
        self.count = "\(packageCounts[btn.tag])"
    }
    
    // 选雷号
    @objc func numberButtonAction(_ btn: QMUIButton) {
        if thunderNumber.count == maxSelectNumberCount, btn.isSelected == false {
            QMUITips.show(withText: "最多选择\(maxSelectNumberCount)个雷号", in: self)
            return
        }
        
        btn.isSelected = !btn.isSelected
       
        if btn.isSelected {
            btn.backgroundColor = Color.red
            btn.layer.borderColor = Color.red.cgColor
            thunderNumber.append("\(btn.tag)")
        } else {
            btn.backgroundColor = .white
            btn.layer.borderColor = UIColor.RGBColor(136, 136, 136).cgColor
            thunderNumber = thunderNumber.filter({$0 != "\(btn.tag)"})
        }
    }
    
    // 提交事件
    @objc func submitButtonAction(_ btn: QMUIButton) {
        guard let input = inputMoneyView.text, input.count>0 else {
            QMUITips.show(withText: "红包金额不能为空")
            return
        }
        
        if let max = Double(groupInfo!.max_money), let min = Double(groupInfo!.min_money), let money = Double(input) {
            if Int(money) > Int(max) || Int(money) < Int(min) {
                QMUITips.show(withText: "最大金额￥\(Int(max))，最小金额￥\(Int(min))")
                return
            }
        }
        
        guard self.thunderNumber.count>0 else {
            QMUITips.show(withText: "请选择雷号")
            return
        }
        guard self.count.count>0 else {
            QMUITips.show(withText: "请选择红包数量")
            return
        }
        
        // 雷号排序
        self.thunderNumber.sort(by: <)
        
        if let handler = submitHandler {
            handler(input, count, thunderNumber.joined(separator: ","))
        }
    }
}


//MARK: -------------CLASS: 发福利/牛包view--------------
class OwnerGroupSendWelfarePackageView: UIView {
    private let leftMargin: CGFloat = 10.0
    
    private lazy var topMoney: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.textColor = .black
        v.textAlignment = .center
        v.font = UIFont.boldSystemFont(ofSize: 29)
        self.addSubview(v)
        return v
    }()
    
    private lazy var numberTipsLabel: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.numberOfLines = 0
        v.textColor = UIColor.RGBColor(156, 156, 156)
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 13)
        self.addSubview(v)
        return v
    }()
    
    private lazy var inputMoneyView: QMUITextField = {
        let v = QMUITextField()
        v.backgroundColor = .white
        v.placeholder = "0"
        v.keyboardType = .numberPad
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 14)
        v.tintColor = Color.red
        
        let left = QMUILabel()
        left.text = "红包金额:"
        left.textColor = UIColor.RGBColor(136, 136, 136)
        left.textAlignment = .center
        left.font = UIFont.systemFont(ofSize: 13)
        left.size = CGSize(width: 80, height: 20)
        v.leftView = left
        
        let right = QMUILabel()
        right.text = "元"
        right.textColor = UIColor.RGBColor(156, 156, 156)
        right.textAlignment = .left
        right.font = UIFont.systemFont(ofSize: 13)
        right.size = CGSize(width: 20, height: 20)
        v.rightView = right
        
        v.leftViewMode = .always
        v.rightViewMode = .always
        self.addSubview(v)
        return v
    }()
    
    private lazy var inputNumberView: QMUITextField = {
        let v = QMUITextField()
        v.backgroundColor = .white
        v.placeholder = "0"
        v.keyboardType = .numberPad
        v.textAlignment = .right
        v.font = UIFont.systemFont(ofSize: 14)
        v.tintColor = Color.red
        
        let left = QMUILabel()
        left.text = "红包个数:"
        left.textColor = UIColor.RGBColor(136, 136, 136)
        left.textAlignment = .center
        left.font = UIFont.systemFont(ofSize: 13)
        left.size = CGSize(width: 80, height: 20)
        v.leftView = left
        
        let right = QMUILabel()
        right.text = "个"
        right.textColor = UIColor.RGBColor(156, 156, 156)
        right.textAlignment = .left
        right.font = UIFont.systemFont(ofSize: 13)
        right.size = CGSize(width: 20, height: 20)
        v.rightView = right
        
        v.leftViewMode = .always
        v.rightViewMode = .always
        self.addSubview(v)
        return v
    }()
    
    private lazy var bottomTips: QMUILabel = {
        let v = QMUILabel()
        v.text = "发包没抢完将在3分钟后自动退回!"
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        v.textColor = UIColor.RGBColor(136, 136, 136)
        self.addSubview(v)
        return v
    }()
    
    private lazy var submitButton: QMUIButton = {
        let v = QMUIButton("确认发送", fontSize: 18)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.addTarget(self, action: #selector(submitButtonAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    private var groupInfo: roomMessage?
    
    var submitHandler: ((_ money: String, _ count: String )->Void)?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var rect = frame
        rect.origin.x = leftMargin
        rect.origin.y = leftMargin
        rect.size.width -= leftMargin*2
        rect.size.height = 60
        topMoney.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        rect.size.height = 40
        numberTipsLabel.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        rect.size.height = 40
        inputMoneyView.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        rect.size.height = 40
        inputMoneyView.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        inputNumberView.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        bottomTips.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        submitButton.frame = rect
        
        rect.origin.y += rect.size.height + leftMargin
        self.height = rect.origin.y
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func freshView(_ msg: roomMessage, _ room_id: String = "") {
        groupInfo = msg
        
        topMoney.text = "￥\(User.default.balance)"
        numberTipsLabel.text = "最大包数\(msg.max_package_num)个，最小包数\(msg.min_package_num)个"
        
        numberTipsLabel.text! += "\n最大金额￥\(msg.max_money)，最小金额￥\(msg.min_money)"

        if let litem = MMKVManager.shared.getCache(FELastSendMoneyItem.self, MMKVCacheKey.lastSendPaperMoney), let cache_money = litem.lastMoney[room_id] {
            inputMoneyView.text = cache_money
        }
    }
    
    @objc func submitButtonAction(_ btn: QMUIButton) {
        guard let input = inputMoneyView.text, input.count>0 else {
            QMUITips.show(withText: "红包金额不能为空")
            return
        }
        
        if let max = Double(groupInfo!.max_money), let min = Double(groupInfo!.min_money), let money = Double(input) {
            if Int(money) > Int(max) || Int(money) < Int(min) {
                QMUITips.show(withText: "最大金额￥\(Int(max))，最小金额￥\(Int(min))")
                return
            }
        }
        
        guard let count = inputNumberView.text, count.count>0 else {
            QMUITips.show(withText: "红包个数不能为空")
            return
        }
        
        if let max2 = Double(groupInfo!.max_package_num), let min2 = Double(groupInfo!.min_package_num), let count2 = Double(count) {
            if Int(count2) > Int(max2) || Int(count2) < Int(min2) {
                QMUITips.show(withText: "最大包数\(Int(max2))个，最小包数\(Int(min2))个")
                return
            }
        }
        
        if let handler = submitHandler {
            handler(input, count)
        }
    }
}
