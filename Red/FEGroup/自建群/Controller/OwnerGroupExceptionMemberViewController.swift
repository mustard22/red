//
//  OwnerGroupExceptionMemberViewController.swift
//  Red
//
//  Created by MAC on 2020/3/25.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class OwnerGroupExceptionMemberViewController: YYTableViewController {
    // 群ID
    var groupId = ""
    
    /// 解除黑名单
    var removeBlackHandler: ((_ uid: String)->Void)?
    
    /// 列表数据
    private var listMembers = [RoomUserItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.title = "黑名单"
        
        self.loadBlackList()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: OwnerGroupExceptionMemberCell.self)
        tableView.reloadData()
    }
}


extension OwnerGroupExceptionMemberViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: OwnerGroupExceptionMemberCell.self)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(loadBlackList))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = UIScreen.main.bounds
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.listMembers.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: OwnerGroupExceptionMemberCell.self, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell0 = cell as! OwnerGroupExceptionMemberCell
        let item = self.listMembers[indexPath.row]
        cell0.update(item)
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.didselectItem(with: indexPath.row)
    }
}

//MARK: 群成员管理
extension OwnerGroupExceptionMemberViewController {
    @objc private func loadBlackList() {
        HttpClient.default.ownerGroupBlackList(group_id: self.groupId) { (items) in
            self.mj_head.endRefreshing()
            self.listMembers = items
            self.tableView.reloadData()
        }
    }
    
    private func didselectItem(with index: Int) {
        let item = self.listMembers[index]
        
        var param = OwnerBlacklistParam()
        param.group_id = self.groupId
        param.user_id = item.user_id
        param.type = "3"
        
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.ownerGroupBlacklist(param: param){[weak self](flag, msg) in
            load.hide(animated: true)
            guard let self = self else {
                return
            }
            if !flag {
                QMUITips.show(withText: msg)
                return
            }
            
            QMUITips.show(withText: "已解除黑名单")
            self.listMembers.remove(at: index)
            self.tableView.reloadData()
        }
    }
}
