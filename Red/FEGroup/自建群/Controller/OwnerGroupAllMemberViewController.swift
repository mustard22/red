//
//  OwnerGroupAllMemberViewController.swift
//  Red
//
//  Created by MAC on 2020/1/10.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class OwnerGroupAllMemberViewController: YYTableViewController {
    private lazy var searchView: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: tableView.width, height: 50)
        
        let content = QMUIButton("搜索", fontSize: 15)
        content.frame = CGRect(x: 10, y: 10, width: v.width - 20, height: v.height-20)
        content.setTitleColor(.darkGray, for: .normal)
        content.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        content.layer.masksToBounds = true
        content.layer.cornerRadius = 2.0
        content.backgroundColor = .white
        v.addSubview(content)
        
        let img = UIImageView()
        img.image = UIImage(named: "icon_nav_search")
        img.frame = CGRect(x: content.width/2 - 40, y: (content.height-18)/2, width: 18, height: 18)
        content.addSubview(img)
        
        tableView.tableHeaderView = v
        return v
    }()
    
    // 群ID
    var groupId = ""
    // 是否群主
    var isGroupAdmin = false
    
    /// 全部成员列表状态更新(type: 1(取消)禁言 2(解除)黑名单 3踢出群聊)
    var updateMemberStatusHandler: ((_ type: Int, _ uid: String, _ value: String)->Void)?
    
    /// 列表数据
    private var listMembers = [RoomUserItem]()
    
    /// 请求的数据
    private var loadListDatas = [RoomUserItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.title = "群组成员"
        
        loadGroupMembers()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: GroupAllMemberCell.self)
        tableView.reloadData()
    }
}

//MARK: 网络请求
extension OwnerGroupAllMemberViewController {
    private func dowithRequest(_ param: OwnerGroupMemberParam, _ resultHandler: ((_ items: [RoomUserItem])->Void)? = nil) {
        HttpClient.default.ownerGroupMemberList(param: param) { (ms) in
            resultHandler?(ms.member)
        }
    }
    
    private func loadGroupMembers() {
        let p = OwnerGroupMemberParam(group_id: self.groupId, pageIndex: "1", pageSize: "\(self.loadCount)")
        self.dowithRequest(p) {[weak self] (items) in
            guard let self = self else {
                return
            }
            
            self.loadListDatas = items
            self.listMembers.removeAll()
            self.listMembers.append(contentsOf: items)
            self.tableView.reloadData()
            
            if items.count >= self.loadCount {
                self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreGroupMembers))
            }
        }
    }
    
    @objc private func loadMoreGroupMembers() {
        let index = self.listMembers.count/self.loadCount + 1
        let p = OwnerGroupMemberParam(group_id: self.groupId, pageIndex: "\(index)", pageSize: "\(self.loadCount)")
        self.dowithRequest(p) {[weak self] (items) in
            guard let self = self else {
                return
            }
            
            self.loadListDatas += items
            self.listMembers.append(contentsOf: items)
            self.tableView.reloadData()
            
            if items.count >= self.loadCount {
                self.mj_foot.state = .idle
            } else {
                self.tableView.mj_footer = nil
            }
        }
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension OwnerGroupAllMemberViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

//MARK: - button actions
extension OwnerGroupAllMemberViewController {
    @objc func searchAction() {
        let sv = FESearchView(searchType: .ownerGroupMember, groupId: self.groupId)
        sv.show()
        
        sv.selectHandler = {(obj) in
            let item = obj as! RoomUserItem
            self.selectOperationOfMember(item: item)
        }
    }
}


extension OwnerGroupAllMemberViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: GroupAllMemberCell.self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = UIScreen.main.bounds
        searchView.frame = CGRect(x: 0, y: 0, width: tableView.width, height: 50)
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.listMembers.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        return count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: GroupAllMemberCell.self, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell0 = cell as! GroupAllMemberCell
        let item = self.listMembers[indexPath.row]
        cell0.update(item, self.isGroupAdmin ? true : false)
        cell0.buttonHandler = {[weak self] (c) in
            guard let path = self?.tableView.indexPath(for: c) else {
                return
            }
            
            self?.didselectItem(with: path.row)
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.didselectItem(with: indexPath.row)
    }
}

//MARK: 群成员管理
extension OwnerGroupAllMemberViewController {
    private func didselectItem(with index: Int) {
        
        let item = self.listMembers[index]
        
        self.selectOperationOfMember(item: item)
    }
    
    private func selectOperationOfMember(item: RoomUserItem) {
        if !self.isGroupAdmin {
            return
        }
        
        if item.user_id == User.default.user_id {
            return
        }
        
        let speakTitle = item.speak_status == "0" ? "禁言" : "取消禁言"
        let blackTitle = item.status == "1" ? "加入黑名单" : "移除黑名单"
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let speak = UIAlertAction(title: speakTitle, style: .default) { (action) in
            self.setSpeakStatusOfMember(item: item)
        }
        
        let black = UIAlertAction(title: blackTitle, style: .default) { (action) in
            self.setBlackStatusOfMember(item: item)
        }
        
        let out = UIAlertAction(title: "踢出群聊", style: .default) { (action) in
            self.outRoomOfMember(item: item)
        }
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        alert.addAction(speak)
        alert.addAction(black)
        alert.addAction(out)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: 禁言/解禁操作
    private func setSpeakStatusOfMember(item: RoomUserItem) {
        var param = OwnerAdminMutedParam()
        param.group_id = self.groupId
        param.user_id = item.user_id
        param.speak_status = item.speak_status == "0" ? "1" : "0"
        
        HttpClient.default.ownerGroupMuted(param: param) {(flag, msg) in
            QMUITips.show(withText: msg)
            if !flag {
                return
            }
            
            self.updateMemberStatusHandler?(1, item.user_id, param.speak_status)
            
            // 更新当前界面数据
            if let index = self.listMembers.firstIndex(where: {$0.user_id == param.user_id}) {
                self.listMembers[index].speak_status = param.speak_status
                
                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        }
    }
    
    //MARK: 拉黑/解黑操作
    private func setBlackStatusOfMember(item: RoomUserItem) {
        var param = OwnerBlacklistParam()
        param.group_id = self.groupId
        param.user_id = item.user_id
        param.type = item.status == "1" ? "2" : "3"
        self.dowithBlackOrOutRoom(param)
    }
    
    //MARK: 踢出群聊操作
    private func outRoomOfMember(item: RoomUserItem) {
        var param = OwnerBlacklistParam()
        param.group_id = self.groupId
        param.user_id = item.user_id
        param.type = "1"
        self.dowithBlackOrOutRoom(param)
    }
    
    //MARK: 拉黑、踢群的网络请求操作
    private func dowithBlackOrOutRoom(_ param: OwnerBlacklistParam) {
        HttpClient.default.ownerGroupBlacklist(param: param){(flag, msg) in
            QMUITips.show(withText: msg)
            if !flag {
                return
            }
            
            var type = 3
            var value: String = ""
            switch param.type {
            case "1":
                type = 3
            case "2":
                type = 2
                value = "2"
            case "3":
                type = 2
                value = "1"
            default: return
            }
            
            self.updateMemberStatusHandler?(type, param.user_id, value)
            
            if let index = self.listMembers.firstIndex(where: {$0.user_id == param.user_id}) {
                if value == "" {
                    self.listMembers.remove(at: index)
                } else {
                    self.listMembers[index].status = value
                }
                
                if param.type == "1" {
                    self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .none)
                } else {
                    self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                }
            }
        }
    }
}
