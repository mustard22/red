//
//  OwnerGroupDetailViewController.swift
//  Red
//
//  Created by MAC on 2020/1/7.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 群组信息界面
class OwnerGroupDetailViewController: YYTableViewController {
    
    var message: roomMessage?
    var groupItem: GroupList?
    
    /// 清空聊天记录block
    var clearChatRecordsHandler: (()->Void)?
    
    // 是否群主
    private var isGroupAdmin = false
    
    lazy var listData:[(String,String,Bool,Bool)] = {
        var d:[(String,String,Bool,Bool)] = []
        // tuple("title", "desc", isShowLine, isShowArrow)
        d.append(("群聊名称",message?.room_name ?? "",true,false))
        d.append(("群公告",message?.room_notice ?? "",true,false))
        d.append(("须知",message?.room_know ?? "",true,false))
        d.append(("群规",message?.room_rule ?? "",true,false))
        d.append(("玩法","",true,true))
        return d
    }()
    
    lazy var headview: OwnerGroupDetailHeadView = {
        let v = OwnerGroupDetailHeadView(avatars: message?.room_user.member.map({$0.avatar}) ?? [])
        v.maxCols = 5
        v.maxRows = 2
        v.showAddButton = true
        v.allMemberHandler = {[weak self] in
            self?.allMemberHandler()
        }
        v.addHandler = {(index, count) in
            if index == count {
                self.addMember()
            }
        }
        return v
    }()
    
    private lazy var footview: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 140)
        
        clearButton.frame = CGRect(x: 20, y: 20, width: self.tableView.frame.size.width-20*2, height: 40)
        v.addSubview(clearButton)
        
        exitButton.frame = CGRect(x: 20, y: clearButton.bottom+20, width: self.tableView.frame.size.width-20*2, height: 40)
        v.addSubview(exitButton)
        
        v.height = exitButton.bottom + 20
        return v
    }()
    
    private lazy var clearButton: QMUIButton = {
        let v = QMUIButton("清空聊天记录", fontSize: 15)
        v.backgroundColor = .white
        v.setTitleColor(Color.red, for: .normal)
        v.addTarget(self, action: #selector(clearButtonAction), for: .touchUpInside)
        v.layer.cornerRadius = 2
        return v
    }()
    
    private lazy var exitButton: QMUIButton = {
        let v = QMUIButton("删除并退出", fontSize: 18)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.addTarget(self, action: #selector(exitButtonAction), for: .touchUpInside)
        v.layer.cornerRadius = 2
        return v
    }()
    
    // 建群按钮
    private lazy var rightTop: QMUIButton = {
        let v = QMUIButton("编辑", fontSize: 15)
        v.setTitleColor(.white, for: .normal)
        v.showsTouchWhenHighlighted = true
        v.size = CGSize(width: 50, height: 40)
        v.addTarget(self, action: #selector(rightTopButtonAction), for: .touchUpInside)
        return v
    }()
    
    var updateOwnerGroupHandler: ((_ roomDetail: roomMessage?, _ groupItem: GroupList?)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }
    
    func setUI() {
        self.title = "群组信息"
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        // 判断当前进群人是否群主
        if let groupItem = groupItem, groupItem.user_id == User.default.user_id {
            self.isGroupAdmin = true
            self.exitButton.setTitle("解散群", for: .normal)
            // 群主时，显示编辑q按钮
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTop)
            
            self.listData.append(("群禁言设置", "", true, false))
            self.listData.append(("黑名单", "异常用户", false, true))
        }

       self.tableView.reloadData()
    }
    
    
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        tableView.backgroundColor = .lightText
        tableView.registerCell(withClass: GroupInfoCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension OwnerGroupDetailViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
    
    //MARK: 邀请好友
    func addMember() {
        let vc = OwnerGroupInviteMemberViewController()
        vc.isGroupAdmin = self.isGroupAdmin
        vc.members = message?.room_user.member ?? []
        vc.groupId = groupItem?.id ?? ""
        self.push(to: vc, animated: true, completion: nil)
        
        vc.inviteMemberHandler = {(invites) in
            if let _ = self.message {
                self.message!.room_user.member.append(contentsOf: invites)
                self.headview.refreshItems(self.message!.room_user.member.map({$0.avatar}))
                self.tableView.tableHeaderView = self.headview
                self.tableView.reloadData()
                
                self.updateOwnerGroupInfo(detail: self.message!, group: nil)
            }
        }
    }
    
    //MARK: clear button action
    @objc func clearButtonAction() {
        let load = QMUITips.showLoading("清理中...", in: self.view)
        self.clearChatRecordsHandler?()
        SMManager.manager.deleteConversation(cid: self.groupItem!.id, isGroup: true)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            load.hide(animated: true)
            QMUITips.show(withText: "清理完毕", in: self.view)
        }
    }
    
    //MARK: 编辑群信息
    @objc func rightTopButtonAction() {
        let vc = OwnerGroupCreateViewController()
        vc.message = self.message
        vc.groupItem = self.groupItem
        self.push(to: vc)
    }
    
    //MARK: exit button action
    @objc func exitButtonAction() {
        let alert = UIAlertController(title: isGroupAdmin ? "解散群" : "退群", message: isGroupAdmin ? "解散房间后，对应数据会被清除且不可恢复，是否确定解散？" : "是否确定退群并清空数据？", preferredStyle: .alert)
        let cancel = UIAlertAction(title: isGroupAdmin ? "再想想" : "取消", style: .cancel, handler: nil)
        let ok = UIAlertAction(title: isGroupAdmin ? "解散" : "退出", style: .default) { (action) in
            self.doExitRequest()
        }
        alert.addAction(cancel)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: 退群接口
    private func doExitRequest() {
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.ownerGroupOutroom(param: groupItem?.id ?? "") {[weak self] (status, text) in
            load.hide(animated: true)
            guard let self = self else {
                return
            }
            if !status {
                QMUITips.show(withText: text, in: self.view)
                return
            }
            
            NotificationCenter.default.post(name: NSNotification.Name.refreshHomeOwnerGroupList, object: nil) // 刷新首页自建群列表
            
            SMManager.manager.deleteConversation(cid: self.groupItem!.id, isGroup: true)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func allMemberHandler() {
        let vc = OwnerGroupAllMemberViewController()
//        vc.datas = message?.room_user.member ?? []
        vc.isGroupAdmin = self.isGroupAdmin
        vc.groupId = groupItem?.id ?? ""
        self.push(to: vc)
        
        vc.updateMemberStatusHandler = {(type, uid, value) in
            if let detail = self.message, let index = detail.room_user.member.firstIndex(where: {$0.user_id == uid}) {
                if type != 3 {
                    return
                }
                
                var members = self.message!.room_user.member
                members.remove(at: index)
                self.message!.room_user.member = members
                self.headview.refreshItems(self.message!.room_user.member.map({$0.avatar}))
                self.tableView.tableHeaderView = self.headview
                self.tableView.reloadData()
                
                self.updateOwnerGroupInfo(detail: self.message, group: nil)
            }
        }
    }
}

extension OwnerGroupDetailViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.RGBColor(240, 240, 240)
        self.tableView.registerCell(withClass: GroupInfoCell.self)
        self.tableView.tableHeaderView = headview
        self.tableView.tableFooterView = footview
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50*kScreenScale
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: GroupInfoCell.self, for: indexPath)
        let t = listData[indexPath.row]
        if (indexPath.row == listData.count-2 && self.isGroupAdmin) {
             cell.update(t.0, "", true, false, 1, message?.speak_status == "1" ? true : false)
        } else {
            cell.update(t.0, t.1, t.2, t.3)
        }
        
        return cell
    }
    
    private func dowithAllMemberMuted(isOn: Bool) {
        var param = OwnerAdminMutedParam()
        param.group_id = self.groupItem?.id ?? ""
        param.user_id = "0"
        param.speak_status = isOn ? "1" : "0"
        
        HttpClient.default.ownerGroupMuted(param: param) {[weak self](flag, msg) in
            QMUITips.show(withText: msg)
            guard let self = self else {
                return
            }
            
            if flag {
                self.groupItem?.speak_status = param.speak_status
                self.message?.speak_status = param.speak_status
                
                self.updateOwnerGroupInfo(detail: self.message, group: self.groupItem)
            }
            self.tableView.reloadData()
        }
    }
    
    private func updateOwnerGroupInfo(detail: roomMessage?, group: GroupList?) {
        updateOwnerGroupHandler?(detail, group)
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (indexPath.row == listData.count-1 && !self.isGroupAdmin) || (indexPath.row == listData.count-3 && self.isGroupAdmin) {
            // 玩法
            guard let item = groupItem, item.playMethodItem.url.hasPrefix("http") else {
                return
            }
            let vc = FEBaseWebViewController()
            vc.url = item.playMethodItem.url
            vc.title = item.playMethodItem.title
            self.push(to: vc)
        } else if (indexPath.row == listData.count-2 && self.isGroupAdmin) {
            if groupItem?.user_id != User.default.user_id {
                QMUITips.show(withText: "只有群主或管理员才有操作权限", in: self.view)
                return
            }
            
            dowithAllMemberMuted(isOn: message?.speak_status == "1" ? false : true)
        } else if indexPath.row == listData.count-1 && self.isGroupAdmin {
            // 黑名单
            let vc = OwnerGroupExceptionMemberViewController()
            vc.groupId = self.groupItem?.id ?? ""
            self.push(to: vc)
        }
    }
}

