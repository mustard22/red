//
//  OwnerGroupListCell.swift
//  Red
//
//  Created by MAC on 2020/1/7.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class OwnerGroupListCell: YYTableViewCell {
    
    lazy var imagev:UIImageView = {
        let v = UIImageView()
        v.backgroundColor = .lightText
        contentView.addSubview(v)
        return v
    }()
    
    lazy var titleLabel:UILabel = {
       let v = UILabel()
        v.font = UIFont.systemFont(ofSize: 18)
        contentView.addSubview(v)
        return v
    }()
    
    lazy var detailLabel:UILabel = {
       let v = UILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        contentView.addSubview(v)
        return v
    }()
    
    private lazy var sepline: UIImageView = {
        let c = UIImageView()
        c.backgroundColor = UIColor.RGBColor(248, 248, 248)
        contentView.addSubview(c)
        return c
    }()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imagev.snp.makeConstraints { (make) in
            make.width.height.equalTo(60)
            make.left.equalTo(10)
            make.centerY.equalTo(contentView)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imagev.snp.right).offset(10)
            make.top.equalTo(imagev)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.height.equalTo(20)
        }
        detailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel)
            make.bottom.equalTo(imagev)
            make.right.equalTo(titleLabel)
            make.height.equalTo(20)
        }
        sepline.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(contentView)
            make.height.equalTo(1)
        }
    }
}
