//
//  OwnerGroupExceptionMemberCell.swift
//  Red
//
//  Created by MAC on 2020/3/25.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 黑名单列表cell
class OwnerGroupExceptionMemberCell: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 16)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var rightLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 14)
        v.textAlignment = .right
        v.textColor = .darkGray
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
}

extension OwnerGroupExceptionMemberCell {
    func update(_ item: RoomUserItem) {
        avatarImageView.setImage(with: URL(string: item.avatar), placeHolder: UIImage(named: "ic_user_default"))
        nameLabel.text = item.nickname
        
        rightLabel.text = "解除黑名单"
        rightLabel.textColor = .darkGray
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }

        rightLabel.snp.makeConstraints { (make) in
            make.width.equalTo(120)
            make.right.equalTo(contentView).offset(-10)
            make.height.equalTo(40)
            make.centerY.equalTo(contentView)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.right.equalTo(rightLabel.snp.left).offset(-10)
            make.height.equalTo(40)
            make.centerY.equalTo(contentView)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}
