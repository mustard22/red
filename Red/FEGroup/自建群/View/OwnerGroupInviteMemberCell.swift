//
//  OwnerGroupInviteMemberCell.swift
//  Red
//
//  Created by MAC on 2020/1/16.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 邀请成员cell
class OwnerGroupInviteMemberCell: YYTableViewCell {

    @IBOutlet weak var flagButton: UIButton!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var sepline: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sepline.backgroundColor = UIColor.RGBColor(248, 248, 248)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension OwnerGroupInviteMemberCell {
    func update(isSelected: Bool, avatar: String, name: String) {
        flagButton.isSelected = isSelected 
        avatarImageView.setImage(with: URL(string: avatar), placeHolder: UIImage(named: "ic_user_default"))
        nameLabel.text = name
    }
}
