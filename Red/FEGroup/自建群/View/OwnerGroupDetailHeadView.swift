//
//  OwnerGroupDetailHeadView.swift
//  Red
//
//  Created by MAC on 2020/1/9.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

class OwnerGroupDetailHeadView: UIView {
    // 图片数量
    private var avatars: [String] = []
    
    // 最多显示几行
    var maxRows: Int {
        get {
            return Int(rows)
        }
        
        set (newValue) {
            rows = CGFloat(newValue)
        }
    }
    
    var maxCols: Int {
        get {
            return Int(cols)
        }
        
        set (newValue) {
            cols = CGFloat(newValue)
        }
    }
    
    var maxItemSize: CGFloat {
        get {
            return itemSize
        }
        set (newValue) {
            itemSize = newValue
        }
    }
    
    var showAddButton: Bool {
        get {
            return !addButton.isHidden
        }
        
        set (newValue) {
            addButton.isHidden = !newValue
        }
    }
    
    var showMoreButton = true
    
    private var rows: CGFloat = 2
    private var cols: CGFloat = 5
    private var itemSize: CGFloat = 50
    
    private var margin: CGFloat {
        let swidth = UIScreen.main.bounds.size.width
        let margin = (swidth - itemSize * cols)/(cols+1)
        return margin
    }
    
    private lazy var addButton: QMUIButton = {
        let v = QMUIButton()
        v.setBackgroundImage(UIImage(named: "foot_more"), for: .normal)
        v.bounds = CGRect(x: 0, y: 0, width: itemSize, height: itemSize)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonClickedAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    private lazy var moreButton: QMUIButton = {
        let b = QMUIButton("全部成员", fontSize: 15)
        b.setTitleColor(.red, for: .normal)
        b.addTarget(self, action: #selector(buttonClickedAction(_:)), for: .touchUpInside)
        self.addSubview(b)
        return b
    }()
    
    init(avatars: [String]) {
        self.avatars = avatars
        super.init(frame: .zero)
        createItems()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 按钮事件
    var addHandler: ((_ index: Int, _ count: Int)->Void)?
    var allMemberHandler: (()->Void)?
    
    private var buttons: [QMUIButton] = []
    
    private func createItems() {
        for (_, b) in buttons.enumerated() {
            if b == self.addButton {
                continue
            }
            b.removeFromSuperview()
        }
        buttons.removeAll()
        
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: margin+rows*(itemSize+margin))
        
        for (i, avatar) in avatars.enumerated() {
            let x = margin + CGFloat(i % Int(cols)) * (itemSize + margin)
            let y = margin + CGFloat(i / Int(cols)) * (itemSize + margin)
            
            let btn = QMUIButton()
            btn.tag = i
            btn.frame = CGRect(x: x, y: y, width: itemSize, height: itemSize)
            btn.addTarget(self, action: #selector(buttonClickedAction(_:)), for: .touchUpInside)
            btn.kf.setBackgroundImage(with: URL(string: avatar), for: .normal, placeholder: UIImage(named: "ic_user_default"))
            self.addSubview(btn)
            buttons.append(btn)
            
            let currentRow = (i / Int(cols)) + 1
            if currentRow == Int(rows) {
                break
            }
        }
        
        if showAddButton {
            var x: CGFloat = margin
            var y: CGFloat = margin
            if let last = buttons.last {
                if buttons.count < Int(rows*cols) {
                    x = last.right + margin
                    y = last.top
                    if buttons.count % Int(cols) == 0 {
                        x = margin
                        y = last.bottom + margin
                    }
                } else {
                    let btn = buttons[Int(rows*cols)-1]
                    x = btn.right + margin
                    y = btn.top
                }
            }
            addButton.left = x
            addButton.top = y
            buttons.append(addButton)
            addButton.tag = avatars.count
        }
        
        // 判断是否显示更多按钮
        if avatars.count >= Int(rows * cols) || showMoreButton {
            // show
            let last = buttons.last!
            moreButton.frame = CGRect(x: 0, y: last.top + last.height + margin, width: self.width, height: 40)
            moreButton.setTitle("全部成员(\(avatars.count))>", for: .normal)
            
            self.height = moreButton.bottom
        } else {
            let last = buttons.last
            self.height = last != nil ? last!.bottom+margin : 0
        }
    }
    
    
    public func refreshItems(_ items: [String]) {
        avatars = items
        createItems()
    }
    
    @objc private func buttonClickedAction(_ sender: QMUIButton) {
        if sender == moreButton {
            // all member
            allMemberHandler?()
        } else {
            addHandler?(sender.tag, avatars.count)
        }
    }
}
