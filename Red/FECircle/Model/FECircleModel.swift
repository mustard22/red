//
//  FECircleModel.swift
//  Red
//
//  Created by MAC on 2020/4/1.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation

public enum CricleSegmentType: CaseIterable {
    case all
    case friend
    case me
    
    public var name: String {
        switch self {
        case .all:
            return "世界"
        case .friend:
            return "朋友"
        case .me:
            return "我的动态"
        }
    }
    
    public var type: String {
        switch self {
        case .all:
            return "1"
        case .friend:
            return "2"
        case .me:
            return "3"
        }
    }
}


/// 编辑自己的发布
public enum OwnerCricleEditType: CaseIterable {
    case all
    case friend
    case me
    case delete
    
    public var name: String {
        switch self {
        case .all:
            return "公开"
        case .friend:
            return "仅好友"
        case .me:
            return "私密"
        case .delete:
            return "删除"
        }
    }
    
    public var status: String {
        switch self {
        case .all:
            return "1"
        case .friend:
            return "2"
        case .me:
            return "3"
        case .delete:
            return "0"
        }
    }
}


//MARK: - 动态列表数据处理
public struct FECircleListModel {
    // 一次加载15条数据
    let loadCount = 15
    var datas: [FECircleListItem] = []
    // 全部 朋友 我的
    var type: CricleSegmentType = .all
    // 新评论数
    var newCommentNumber = ""
    
    public mutating func loadListData(_ handler: @escaping ((_ showMore: Bool , _ msg: String? )->Void)) {
        var param = FECircleListParam()
        param.status = type.type
        param.pageSize = "\(loadCount)"
        
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.circleList(param: param) { (item, msg) in
            guard let data = item else {
                return
            }
            
            unsafePointSelf.pointee.newCommentNumber = data.comment_total_nums
            unsafePointSelf.pointee.datas = data.data.map({ (item) -> FECircleListItem in
                var m = item
                m.setupLargePics()
                return m
            })
            handler(data.data.count < unsafePointSelf.pointee.loadCount ? false : true, msg)
        }
    }
    
    public mutating func loadMoreListData(_ handler: @escaping ((_ showMore: Bool , _ msg: String? )->Void)) {
        var param = FECircleListParam()
        param.status = type.type
        param.pageSize = "\(loadCount)"
        param.pageIndex = "\(datas.count/loadCount + 1)"
        
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.circleList(param: param) { (item, msg) in
            guard let data = item else {
                return
            }
            unsafePointSelf.pointee.datas.append(contentsOf: data.data.map({ (item) -> FECircleListItem in
                var m = item
                m.setupLargePics()
                return m
            }))
            handler(data.data.count < unsafePointSelf.pointee.loadCount ? false : true, msg)
        }
    }
    
    public mutating func editOwnerPublishItem(_ index: Int, _ status: OwnerCricleEditType, _ handler: @escaping (_ msg: String?)->Void) {
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        var item = self.datas[index]
        let p = FECricleUpdateTopicStatusParam(topic_id: item.id, status: status.status)
        HttpClient.default.circleEditPublishStatus(param: p) { (flag, msg) in
            if !flag {
                handler(msg)
                return
            }
            
            if status == .delete {
                unsafePointSelf.pointee.datas.remove(at: index)
            } else {
                item.status = status.status
                unsafePointSelf.pointee.datas[index] = item
            }
            handler(nil)
        }
    }
    
    public static func updateLikeNumber(topicId: String, handler: @escaping (_ isLike: Bool?, _ msg: String)->Void) {
        HttpClient.default.circleUpdateLikeNumber(topicId: topicId) { (isLike, msg) in
            handler(isLike, msg)
        }
    }
}

public struct FECircleListData: Codable {
    var currentPage = ""
    var data: [FECircleListItem] = []
    var comment_total_nums = "0"
}

public struct FECircleListItem: Codable {
    var nickname = "森勒斯"
    var user_avatar = "https://images.xaslswlkj.com//avatar/user/4.jpg"
    var date_time = "2020-04-02"
    var pics: [String] = []
    
    /// 新评论数
    var nem_comment_num = "0"
    
    var content = ""
    
    var click_num = "100"
    var com_num = "10"
    
    var gender = ""
    var is_click = "0"
    
    var uid = ""
    var id = ""
    var status = ""
    
    var share_group_id = "0"
    
    // 缓存frame
    var itemFrame: FECircleListCellFrameItem?
    
    // 大图链接（用过返回的pics处理获取'RE-'）
    var largePics: [String] = []
    
    mutating func setupLargePics() {
        if pics.isEmpty {
            return
        }
        
        largePics = pics.map({ (pic) -> String in
            let strs = pic.components(separatedBy: "RE-")
            return strs.joined(separator: "")
        })
    }
}


public struct FECircleListCellFrameItem: Codable {
    var content: CGRect?
    var image: CGRect?
    var bottomBar: CGRect?
    
    var cellHeight: CGFloat = 0
}



//MARK: - 详情页model(评论数据不分页 一次拿完)
public struct FECircleDetailModel {
    let loadCount = 5
    
    var detail = FECircleListItem()
    
    /// 评论列表data(true 显示第二层 false隐藏)
    var datas: [(FECircleCommentItem/* 第一层 */, [FECircleCommentItem]/* 第二层 */, Bool)] = []
    
    //MARK: 第一次加载一级评论
    mutating func loadFirstCommentData(handler: @escaping (_ flag: Bool, _ msg: String, _ showMore: Bool)->Void) {
        let p = FECircleCommentListParam(topic_id: detail.id, parent_id: detail.id, type: "0", pageIndex: "1")
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.circleGetCommentList(param: p) { (item, msg) in
            let showMore = false
            if let data = item {
                unsafePointSelf.pointee.datas = data.data.map({($0, [], false)})
//                if data.data.count >= unsafePointSelf.pointee.loadCount {
//                    showMore = true
//                }
                handler(true, msg, showMore)
            } else {
                handler(false, msg, showMore)
            }
        }
    }
    
    //MARK: 加载更多一级评论
    mutating func loadMoreFirstCommentData(handler: @escaping (_ flag: Bool, _ msg: String, _ showMore: Bool)->Void) {
        let p = FECircleCommentListParam(topic_id: detail.id, parent_id: detail.id, type: "0", pageIndex: "\(datas.count/loadCount + 1)")
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.circleGetCommentList(param: p) { (item, msg) in
            
            var showMore = false
            if let data = item {
                unsafePointSelf.pointee.datas.append(contentsOf: data.data.map({($0, [], false)}))
                if data.data.count >= unsafePointSelf.pointee.loadCount {
                    showMore = true
                }
                handler(true, msg, showMore)
            } else {
                handler(false, msg, showMore)
            }
        }
    }
    
    //MARK: 加载二级评论（不分页）
    mutating func loadSecondCommentData(section: Int, handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        let item = datas[section].0
        let p = FECircleCommentListParam(topic_id: detail.id, parent_id: item.id, type: "1", pageIndex: "1")
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.circleGetCommentList(param: p) { (item, msg) in
            if let data = item {
                unsafePointSelf.pointee.datas[section].1 = data.data
                handler(true, msg)
            } else {
                handler(false, msg)
            }
        }
    }
    
    //MARK: 删除评论
    mutating func deleteComment(_ section: Int? = nil, _ path: IndexPath?, handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        var isFirst = true
        var cid = ""
        if let sec = section {
            let item = datas[sec].0
            cid = item.id
        } else if let path = path {
            isFirst = false
            let item = datas[path.section].1[path.row]
            cid = item.id
        }
        
        let unsafePointSelf = UnsafeMutablePointer<Any>.unsafeMutablePointer(item: &self)
        HttpClient.default.circleDelteComment(comment_id: cid) { (flag, msg) in
            
            if isFirst {
                // del section
                unsafePointSelf.pointee.datas.remove(at: section!)
            } else {
                unsafePointSelf.pointee.datas[path!.section].1.remove(at: path!.row)
            }
            handler(flag, msg)
        }
    }
    
    func sendComment(param: FECircleCommentTopicParam, handler: @escaping (_ flag: Bool, _ msg: String)->Void) {
        HttpClient.default.circleCommentTopic(param: param) { (flag, msg) in
            handler(flag, msg)
        }
    }
}

public struct FECircleCommentListData: Codable {
    var data: [FECircleCommentItem] = []
}

/// 评论item
public struct FECircleCommentItem: Codable {
    var content = ""
    var date_time = ""
    var gender = ""
    var id = ""
    var nickname = ""
    var parent_id = ""
    var role_avatar = ""
    var role_gender = ""
    var role_nickname = ""
    var role_uid = ""
    var status = ""
    var tid = ""
    var type = ""
    var uid = ""
    var user_avatar = ""
    
    var itemFrame: FECircleCommentFrameItem?
}


public struct FECircleCommentFrameItem: Codable {
    var content: CGRect?
    
    var cellHeight: CGFloat = 0
}

