//
//  FECirclePublishViewController.swift
//  Red
//
//  Created by MAC on 2020/4/1.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 发布动态界面
class FECirclePublishViewController: FEBaseViewController {
    /// 动态发布类型枚举
    public enum PublishType: CaseIterable {
        case all
        case friend
        case me
        
        public var name: String {
            switch self {
            case .all :
                return "公开"
            case .friend :
                return "朋友"
            case .me :
                return "私密"
            }
        }
        
        public var status: String {
            switch self {
            case .all :
                return "1"
            case .friend :
                return "2"
            case .me :
                return "3"
            }
        }
    }
    
    private var type: PublishType = .all
    private let maxCount = 9
    
    @IBOutlet weak var content: UIScrollView!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var placeHolder: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var typeButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    private lazy var photoView: FECirclePublishSelectPhotosView = {
        let addview = FECirclePublishSelectPhotosView(maxRows: 3, maxCols: 3, maxWidth: kScreenSize.width-15*2, showAddButton: .showToHide, allowImageEdited: true, scale: 0.8)
        self.content.addSubview(addview)
        return addview
    }()
    
    private var inputSize = CGSize.zero
    private var defaultHeight: CGFloat = 100.0
    
    private var images: [UIImage] = []
    
    /// 是否第一次进来
    private var isFirstComing = true
    
    public var publishCompleteHandler: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dosomeInitial()
        
        self.registerAllNotifications()
    }
    
    deinit {
        self.removeAllNotifications()
        self.textView.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.isFirstComing {
            self.textView.becomeFirstResponder()
            self.isFirstComing = false
        }
    }
    
    private func dosomeInitial() {
        self.title = "发布动态"
        self.view.backgroundColor = .white
        
        sendButton.layer.backgroundColor = Color.red.cgColor
        sendButton.layer.cornerRadius = 2
        
        typeButton.setTitle(self.type.name, for: .normal)
        
        self.textView.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        self.defaultHeight = self.textView.height
        
        self.photoView.left = 15
        self.photoView.top = self.textView.bottom
        
        self.refreshSomeViewPosition()
        
        self.photoView.addButtonHandler = {[weak self] in
            self?.textView.resignFirstResponder()
            self?.selectPhotos()
        }
        
        self.photoView.touchItemHandler = { [weak self] (index, isTouch, touchItem) in
            self?.textView.resignFirstResponder()
            self?.touchPhotos(index, isTouch, touchItem)
        }
        
        self.content.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(tapHandle)))
    }
    
    @objc
    private func tapHandle() {
        self.textView.resignFirstResponder()
    }
}

extension FECirclePublishViewController {
    @IBAction func sendButtonAction(_ sender: UIButton) {
        self.textView.resignFirstResponder()
        
        if textView.text.isEmpty {
            QMUITips.showInfo("请输入发布内容", in: self.view)
            return
        }
        
        let load = QMUITips.showLoading("发布中...", in: self.view)
        if !images.isEmpty {
            self.uploadImages {[weak self] (url) in
                guard let self = self else {
                    load.hide(animated: true)
                    return
                }
                let p = FECirclePublishParam(status: self.type.status, content: self.textView.text, pics: url ?? "")
                self.doPublish(p, load)
            }
        } else {
            let p = FECirclePublishParam(status: type.status, content: textView.text, pics: "")
            doPublish(p, load)
        }
    }
    
    private func doPublish(_ p: FECirclePublishParam, _ load: QMUITips) {
        HttpClient.default.circlePublish(param: p) { (flag, msg) in
            load.hide(animated: true)
            if !flag {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            self.publishCompleteHandler?() // 告诉上级界面 发布成功
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func selectPhotos() {
        FESelectPhotoManager.selectPhoto(with: self, selectCount: self.maxCount - self.images.count) { (imgs) in
            self.images.append(contentsOf: imgs)
            self.photoView.refreshItems(nil, self.images)
            self.refreshSomeViewPosition()
        }
    }
    
    //MARK: 当选择图片view高度改变时，调整bottomView和发布按钮的位置
    private func refreshSomeViewPosition() {
        self.bottomView.top = self.photoView.bottom + 20
        self.sendButton.top = self.bottomView.bottom + 30
        
        var csize = content.size
        if sendButton.bottom > content.bottom {
            csize.height += 50
        }
        csize.width = 0
        content.contentSize = csize
    }
    
    private func touchPhotos(_ index: Int, _ isTouch: Bool, _ touchView: UIView?) {
        if isTouch {
            //MARK: scan big image
            let vc = FEShowBigPictureView2(images: images, index: index)
            self.present(vc, animated: true, completion: nil)
        } else {
            // delete
            images.remove(at: index)
            self.refreshSomeViewPosition()
        }
    }
    
    // 点击设置发布权限
    @IBAction func typeButtonAction(_ sender: UIButton) {
        self.textView.resignFirstResponder()
        
        let asheet = UIAlertController(title: "设置发布权限", message: nil, preferredStyle: .actionSheet)
        _ = PublishType.allCases.map({ (t) -> Void in
            let a = UIAlertAction(title: t.name, style: .default) {[weak self] (action) in
                self?.type = t
                self?.typeButton.setTitle(t.name, for: .normal)
            }
            asheet.addAction(a)
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        asheet.addAction(cancel)
        
        self.present(asheet, animated: true, completion: nil)
    }
    
    //MARK: 上传图片
    private func uploadImages(handler: @escaping (_ url: String?)->Void) {
        let queue = DispatchQueue.global()
        let group = DispatchGroup()
        
        var url: [String] = []
        
        // 群类型数据
        _ = self.images.map({(img) -> Void in
            queue.async(group: group) {
                let signal = DispatchSemaphore(value: 0)
                queue.async {
                   let jpgData = img.jpegData(compressionQuality: 0.6)
//                    dprint("压缩后的jpg size: \(jpgData?.count)")
                    HttpClient.default.circleUploadFile(jpgData) { (item) in
                        if let m = item {
                            url.append(m.path)
                        }
                        signal.signal()
                    }
                }
                signal.wait()
            }
        })
        
        group.notify(queue: DispatchQueue.main) {
            handler(url.isEmpty ? nil : url.joined(separator: ","))
        }
    }
}


extension FECirclePublishViewController: UITextViewDelegate {
    private func registerAllNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChangeNotification), name: UITextView.textDidChangeNotification, object: nil)
    }
    
    private func removeAllNotifications() {
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidChangeNotification, object: nil)
    }
    
    @objc
    private func textDidChangeNotification() {
        DispatchQueue.main.async {
            if self.textView.text.isEmpty {
                self.placeHolder.isHidden = false
            } else {
                self.placeHolder.isHidden = true
            }
            
            let size = self.textView.contentSize
            // 计算偏移量
            let offsetHeight = size.height - self.inputSize.height
            // 赋新值
            self.inputSize = size
            
            if offsetHeight == 0 {
                return
            }
            
            // -1高度减小 0不变化 1高度增加
            var type = 0
            if size.height <= self.defaultHeight {
                if offsetHeight < 0 && self.textView.height > self.defaultHeight {
                    type = -1
                }
            } else {
                type = offsetHeight > 0 ? 1 : -1
            }
            if type == 0 {
                return
            }
            UIView.animate(withDuration: 0.2) {
                self.textView.height += offsetHeight
                self.photoView.top = self.textView.bottom
                self.refreshSomeViewPosition()
            }
        }
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.2) {
            self.textView.resignFirstResponder()
        }
    }
}
