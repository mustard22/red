//
//  FECircleViewController.swift
//  Red
//
//  Created by MAC on 2020/3/30.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 圈子界面
class FECircleViewController: FEBaseViewController {
    
    private let segmentItems: [CricleSegmentType] = [.all, .friend]
    private var selectIndex = 0
    
    // 是否需要刷新数据
    private var isNeedRefreshData = false
    
    private lazy var segment: QMUISegmentedControl = {
        let v = QMUISegmentedControl(items: self.segmentItems.map({$0.name}))
        v.size = CGSize(width: 120, height: 30)
        v.selectedSegmentIndex = self.selectIndex
        v.tintColor = .white
        v.addTarget(self, action: #selector(segmentControlValueChangedAction(_:)), for: .valueChanged)
        return v
    }()
    
    private lazy var tables: [FECircleListViewController] = {
        let v1 = FECircleListViewController()
        v1.model.type = .all
        v1.refreshFinishedHandler = {[weak self] in
            self?.refreshBadgeView()
        }
        
        let v2 = FECircleListViewController()
        v2.model.type = .friend
        let ts = [v1,v2]
        return ts
    }()
    
    lazy var pagevc: FEBasePageViewController = {
        // 默认显示消息列表
        let vc = FEBasePageViewController(start: 0, headerHeight: 0, style: .scroll)
        vc.dataSource = self
        self.addChild(vc)
        self.view.addSubview(vc.view)
        return vc
    }()
    
    // 发布按钮
    private lazy var rightTop: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "ic_publish"), for: .normal)
        v.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
        v.imageEdgeInsets = UIEdgeInsets(top: 10, left: 7.5, bottom: 10, right: 7.5)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(rightTopButtonAction), for: .touchUpInside)
        return v
    }()
    
    private lazy var leftTop: UIButton = {
        let v = UIButton()
        v.setImage(UIImage(named: "ic_circle_tip"), for: .normal)
        v.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
        v.imageEdgeInsets = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16)
        
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(leftTopButtonAction), for: .touchUpInside)
        return v
    }()
    
    private lazy var badgeView: UILabel = {
        let v = UILabel()
        v.isHidden = true
        v.bounds = CGRect(x: 0, y: 0, width: 18, height: 18)
        v.textAlignment = .center
        v.textColor = Color.red
        v.font = UIFont.boldSystemFont(ofSize: 10)
        v.layer.backgroundColor = UIColor(white: 1, alpha: 0.9).cgColor
        v.layer.cornerRadius = v.height / 2
        v.layer.masksToBounds = true
        
        v.adjustsFontSizeToFitWidth = true
        
        leftTop.addSubview(v)
        v.right = leftTop.right
        v.top = 5
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dosomeInitial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshBadgeView()
    }
    
    //MARK: 刷新提示数
    private func refreshBadgeView() {
        if let table = tables.first, let num = Double(table.model.newCommentNumber), Int(num) > 0 {
            self.badgeView.text = table.model.newCommentNumber
            self.badgeView.isHidden = false
        } else {
            self.badgeView.text = ""
            self.badgeView.isHidden = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let rect = view.bounds
        pagevc.view.frame = rect
    }
    
    private func dosomeInitial() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTop)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.leftTop)
        self.navigationItem.titleView = self.segment
    }
}

extension FECircleViewController {
    private func setupSegmentControlStatus() {
        self.segment.selectedSegmentIndex = self.selectIndex
    }
    
    private func exchangeCricleListController() {
        self.pagevc.currentPage = self.selectIndex
    }
}

extension FECircleViewController {
    //MARK: 发布新动态
    @objc
    private func rightTopButtonAction() {
        let vc = FECirclePublishViewController(nibName: "FECirclePublishViewController", bundle: nil)
        self.push(to: vc, animated: true, completion: nil)
        
        vc.publishCompleteHandler = {[weak self] in
            // 发布成功 刷新当前列表
            guard let self = self, self.selectIndex >= 0, self.selectIndex < self.tables.count else {
                return
            }
            let tab = self.tables[self.selectIndex]
            tab.refreshListData()
        }
    }
    
    //MARK: 新评论提醒
    @objc
    private func leftTopButtonAction() {
        let vc = FECircleListViewController()
        vc.model.type = .me
        vc.title = vc.model.type.name
        self.push(to: vc, animated: true, completion: nil)
        
        if let table = tables.first {
            table.model.newCommentNumber = "0"
        }
    }
    
    @objc
    private func segmentControlValueChangedAction(_ seg: QMUISegmentedControl) {
        // 刷新
        self.selectIndex = seg.selectedSegmentIndex
        self.exchangeCricleListController()
    }
}


//MARK: - FEBasePageViewControllerDataSource
extension FECircleViewController: FEBasePageViewControllerDataSource {
    func numbersOfPage() -> Int {
        return self.segmentItems.count
    }
    
    func previewController(formPage index: Int) -> UIViewController {
        let vc = self.tables[index]
        vc.view.tag = index
        return vc
    }

    func currentPageIndex(index: Int) {
        self.selectIndex = index
        self.setupSegmentControlStatus()
    }
}
