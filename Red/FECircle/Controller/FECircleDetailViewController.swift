//
//  FECircleDetailViewController.swift
//  Red
//
//  Created by MAC on 2020/4/2.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 评论列表界面
class FECircleDetailViewController: YYTableViewController {
    private lazy var bottomBar: FECircleDetailBottomBar = {
        let v = Bundle.main.loadNibNamed("FECircleDetailBottomBar", owner: self, options: nil)?.first as! FECircleDetailBottomBar
        v.backgroundColor = .white
        v.top = kScreenSize.height - 60 - (kIsIphoneX ? 20 : 0)
        v.width = kScreenSize.width
        v.placeHolder = "评论"
        self.view.addSubview(v)
        return v
    }()
    
    var dmodel: FECircleDetailModel = FECircleDetailModel()
    
    private var commentParam = FECircleCommentTopicParam()

    private var commentSection: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "评论"
        
        self.loadFirstCommentList()
        
        self.bottomBar.sendHandler = {(text) in
            self.sendInputAction(text)
        }
        self.bottomBar.startEditHandler = {
            self.commentParam.topic_id = self.dmodel.detail.id
            self.commentParam.comment_id = self.dmodel.detail.id
            self.commentParam.type = "0"
            self.commentSection = nil
        }
        
        self.registerAllNotifications()
        
        self.setupHeadView()
    }
    
    deinit {
        self.removeAllNotifications()
    }
    
    private func sendInputAction(_ text: String) {
       guard !text.isEmpty else {
           return
       }
       
       commentParam.content = text

       dmodel.sendComment(param: commentParam) { (flag, msg) in
           if flag == false {
               QMUITips.showInfo(msg, in: self.view)
               return
           }
           
           self.domsomeAfterEndEditing()
           
           if let sec = self.commentSection {
               self.loadSecondCommentList(sec)
           } else {
               self.loadFirstCommentList()
           }
       }
    }
    
    private var headView: FECircleDetailTopView?
    
    private func setupHeadView() {
        if let _ = self.headView {
            return
        }
        
        guard let head = Bundle.main.loadNibNamed("FECircleDetailTopView", owner: self, options: nil)?.first as? FECircleDetailTopView else {
            return
        }
        
        dmodel.detail.itemFrame = FECircleDetailTopView.height(self.dmodel.detail)
        head.height = dmodel.detail.itemFrame!.cellHeight
        
        head.update(dmodel.detail)
        
        head.buttonHandler = {[weak self] (touchImage) in
            if let ti = touchImage {
                self?.scanBigImage(ti)
            } else {
                self?.inputCommentAction(nil, nil)
            }
        }
        
        
        self.headView = head
        
        self.tableView.tableHeaderView = self.headView
    }
    
    //MARK: 加载一级评论
    func loadFirstCommentList() {
        dmodel.loadFirstCommentData { (flag, msg, loadMore) in
            if flag == false {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            self.tableView.reloadData()
            
            if loadMore {
                self.tableView.mj_footer = self.mj_foot
            } else {
                self.tableView.mj_footer = nil
            }
        }
    }
    
    //MARK: 加载更多一级评论
    @objc
    func loadMoreFirstCommentList() {
        
        dmodel.loadMoreFirstCommentData { (flag, msg, loadMore) in
            self.mj_foot.endRefreshing()
            
            if flag == false {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            self.tableView.reloadData()
            
            if loadMore {
                self.tableView.mj_footer.state = .idle
            } else {
                self.tableView.mj_footer = nil
            }
        }
    }
    
    //MARK: 显示二级评论
    private func showSecondCommentList(_ section: Int) {
        let sec = section
        
        if sec < 0 || sec >= dmodel.datas.count {
            return
        }
        
        let sitem = dmodel.datas[sec]
        
        if sitem.2 == true {
            // show -> hide
            dmodel.datas[sec].2 = false
        } else {
            dmodel.datas[sec].2 = true
            // hide -> show
        }
        if sitem.1.count == 0 {
            // request data
            self.loadSecondCommentList(section)
        } else {
            self.refreshSectionsForList(IndexSet(arrayLiteral: section))
        }
    }
    
    //MARK: 加载二级评论
    func loadSecondCommentList(_ section: Int) {
        let sec = section
        
        if sec < 0 || sec >= dmodel.datas.count {
            return
        }
        
        // request data
        dmodel.loadSecondCommentData(section: sec) {[weak self] (flag, msg) in
            self?.refreshSectionsForList(IndexSet(arrayLiteral: section))
        }
    }
    
    //MARK: 删除二级评论
    private func deleteSecondCommentItem(_ path: IndexPath) {
        dmodel.deleteComment(nil, path) { (flag, msg) in
            if flag == false {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            self.refreshSectionsForList(IndexSet(arrayLiteral: path.section))
        }
    }
    
    //MARK: 删除一级评论
    private func deleteFirstCommentItem(_ section: Int) {
        dmodel.deleteComment(section, nil) { (flag, msg) in
            if flag == false {
                QMUITips.showInfo(msg, in: self.view)
                return
            }
            
            self.tableView.reloadData()
        }
    }
}


extension FECircleDetailViewController {
    private func registerAllCells() {
        self.tableView.register(UINib(nibName: "FECircleSecondListCell", bundle: nil), forCellReuseIdentifier: "FECircleSecondListCell")
        
        
        self.tableView.register(UINib(nibName: "FECircleCommentListHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FECircleCommentListHeader")
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.backgroundColor = UIColor.RGBColor(240, 240, 240)
//        self.tableView.separatorStyle = .singleLine
//        self.tableView.separatorColor = UIColor.systemGray
        
        self.registerAllCells()
        
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreFirstCommentList))
        self.tableView.mj_footer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - bottomBar.bounds.size.height)
        
    }
    
    override func didInitialize(with style: UITableView.Style) {
        super.didInitialize(with: .grouped)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        let dcount = dmodel.datas.count
        if dcount == 0 {
            self.nodataView.text = "\n\n暂无评论"
        } else {
            self.nodataView.text = ""
        }
        return dcount
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sitem = dmodel.datas[section]
        if sitem.2 == true {
            return sitem.1.count
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sitem = dmodel.datas[section]
        let frame = FECircleCommentListHeader.height(sitem.0)
        dmodel.datas[section].0.itemFrame = frame
        return frame.cellHeight
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sitem = dmodel.datas[section]
        let item = sitem.0
        let v = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FECircleCommentListHeader") as! FECircleCommentListHeader
        v.update(item)
        v.buttonHandler = {[weak self] (action) in
            self?.headerViewAction(section: section, action: action)
        }
        return v
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sitem = dmodel.datas[indexPath.section]
        var ritem = sitem.1[indexPath.row]
        let frame = FECircleSecondListCell.height(ritem)
        ritem.itemFrame = frame
        
        dmodel.datas[indexPath.section].1[indexPath.row] = ritem
        return frame.cellHeight
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FECircleSecondListCell", for: indexPath) as! FECircleSecondListCell
        cell.selectionStyle = .none
        
        let sitem = dmodel.datas[indexPath.section]
        let item = sitem.1[indexPath.row]
        cell.update(item)
        cell.buttonHandler = {[weak self] (action) in
            self?.cellAction(path: indexPath, action: action)
        }
        return cell
    }
    
    // reload sections
    private func refreshSectionsForList(_ sections: IndexSet) {
        UIView.performWithoutAnimation {
            self.tableView.reloadSections(sections, with: .none)
        }
    }
    
    //MARK: 浏览大图
    private func scanBigImage(_ touchImage: (Int, UIImage?)?) {
        guard let touimgv = touchImage, touimgv.0 < dmodel.detail.largePics.count else {
            return
        }
        
        let item = dmodel.detail
        let vc = FEShowBigPictureView2(images: item.largePics, index: touimgv.0, placeHolder: item.pics)
        self.present(vc, animated: true, completion: nil)
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.bottomBar.endEditing()
        domsomeAfterEndEditing()
    }
    
    func domsomeAfterEndEditing() {
        commentParam.reset()
    }
    
    //MARK: - header actions
    private func headerViewAction(section: Int, action: CircleCommentListHeaderAction) {
        switch action {
        case .more:
            //MARK: 显示二级评论
            self.showSecondCommentList(section)
        case .edit:
            //MARK: 写二级评论
            inputCommentAction(section, nil)
        case .delete:
            //MARK: 删除评论
            self.deleteFirstCommentItem(section)
        }
    }
    
    //MARK: - cell actions
    private func cellAction(path: IndexPath, action: CircleCommentListCellAction) {
        switch action {
        case .edit:
            //MARK: 回复二级评论
            inputCommentAction(nil, path)
        case .delete:
            //MARK: 删除二级评论
            self.deleteSecondCommentItem(path)
        }
    }
    
    private func inputCommentAction(_ section: Int?, _ path: IndexPath?) {
        if let sec = section {
            // 评论第一层
            let item = dmodel.datas[sec].0
            commentParam.topic_id = dmodel.detail.id
            commentParam.comment_id = item.id
            commentParam.type = "1"
            
            self.bottomBar.startEditing("回复\(item.nickname):")
            commentSection = sec
        } else if let path = path {
            // 评论第二层
            let item = dmodel.datas[path.section].1[path.row]
            commentParam.topic_id = dmodel.detail.id
            commentParam.comment_id = item.id
            commentParam.type = "1"
            
            self.bottomBar.startEditing("回复\(item.nickname):")
            commentSection = path.section
        } else {
            // 评论主题
            commentParam.topic_id = dmodel.detail.id
            commentParam.comment_id = dmodel.detail.id
            commentParam.type = "0"
            commentSection = nil
            
            self.bottomBar.startEditing("评论")
        }
    }
}


extension FECircleDetailViewController {
    private func registerAllNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }
    
    private func removeAllNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc fileprivate func keyboardWillChangeFrameNotification(_ not:NSNotification) {
        guard let info = not.userInfo as NSDictionary?,
            let value = info.object(forKey: "UIKeyboardFrameEndUserInfoKey") as! NSValue? else {
            return
        }
        
        let keyboardRect = value.cgRectValue
        UIView.animate(withDuration: 0.2) {
            self.bottomBar.bottom = keyboardRect.origin.y
        }
    }
    
    @objc fileprivate func keyboardWillShow(_ not:NSNotification) {
        guard let info = not.userInfo as NSDictionary?,
            let value = info.object(forKey: "UIKeyboardFrameEndUserInfoKey") as! NSValue? else {
            return
        }
        
        let keyboardRect = value.cgRectValue
        
        UIView.animate(withDuration: 0.2) {
            self.bottomBar.bottom = keyboardRect.origin.y
        }
    }
    
    @objc fileprivate func keyboardWillHide(_ not:NSNotification) {
        UIView.animate(withDuration: 0.2) {
            self.bottomBar.top = self.tableView.bottom
        }
    }
}
