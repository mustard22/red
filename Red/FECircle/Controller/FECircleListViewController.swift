//
//  FECircleListViewController.swift
//  Red
//
//  Created by MAC on 2020/4/1.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 圈子列表
class FECircleListViewController: YYTableViewController {
    
    public var model = FECircleListModel()
    
    // 下拉刷新完毕
    public var refreshFinishedHandler: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mj_head.beginRefreshing()
    }
}

extension FECircleListViewController {
    // 外部通知刷新
    public func refreshListData() {
        loadListData()
    }
    
    @objc
    private func loadListData() {
        self.tableView.mj_footer = nil
        self.model.loadListData {(showMore, msg)  in
            self.mj_head.endRefreshing()
            self.tableView.reloadData()
            
            if showMore {
                self.tableView.mj_footer = self.mj_foot
            }
            
            self.refreshFinishedHandler?()
        }
    }
    
    @objc
    private func loadMoreListData() {
        self.model.loadMoreListData {(showMore, msg)  in
            self.mj_foot.endRefreshing()
            self.tableView.reloadData()
            
            if showMore {
                self.tableView.mj_footer.state = .idle
            } else {
                self.tableView.mj_footer = nil
            }
        }
    }
}

extension FECircleListViewController {
    private func registerAllCells() {
        self.tableView.register(UINib(nibName: "FECircleListCell", bundle: nil), forCellReuseIdentifier: "FECircleListCell")
        self.tableView.register(UINib(nibName: "FECircleListCell2", bundle: nil), forCellReuseIdentifier: "FECircleListCell2")
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.backgroundColor = UIColor.RGBColor(240, 240, 240)
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.RGBColor(240, 240, 240)
        
        self.registerAllCells()
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
        
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreListData))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    override func didInitialize(with style: UITableView.Style) {
        super.didInitialize(with: .plain)
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if model.datas.isEmpty {
            self.nodataView.text = "暂无动态"
        } else {
            self.nodataView.text = ""
        }
        return model.datas.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = model.datas[indexPath.row]
        if let fitem = item.itemFrame {
            return fitem.cellHeight
        }
        
        var fitem: FECircleListCellFrameItem
        if item.share_group_id == "0" {
            fitem = FECircleListCell.cellHeight(item)
        } else {
            fitem = FECircleListCell2.cellHeight(item)
        }
        model.datas[indexPath.row].itemFrame = fitem
        return fitem.cellHeight
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = model.datas[indexPath.row]
        if item.share_group_id == "0" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FECircleListCell", for: indexPath) as! FECircleListCell
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FECircleListCell2", for: indexPath) as! FECircleListCell2
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let item = model.datas[indexPath.row]
        if item.share_group_id == "0" {
            let cell0 = cell as! FECircleListCell
            cell0.update(item, self.model.type == .me ? true : false)
            cell0.buttonHandler = {[weak self] (action, touchImage) in
                self?.cellClickedAction(action, touchImage, indexPath)
            }
        } else {
            let cell0 = cell as! FECircleListCell2
            cell0.update(item)
            cell0.buttonHandler = {[weak self] (action, touchImage) in
                self?.cellClickedAction(action, touchImage, indexPath)
            }
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
     
        enterDetail(indexPath.row)
    }
    
    private func enterDetail(_ index: Int) {
        let m = model.datas[index]
        if m.share_group_id != "0" {
            return
        }
        
        let vc = FECircleDetailViewController()
        vc.dmodel.detail = model.datas[index]
        self.push(to: vc, animated: true, completion: nil)
        
        if self.model.type == .me {
            model.datas[index].nem_comment_num = "0"
            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    
    //MARK: cell事件处理
    private func cellClickedAction(_ action: CircleListCellAction, _ touchImage: (Int, UIImage?)? = nil, _ indexPath: IndexPath) {
        let item = model.datas[indexPath.row]
        if action == .like {
            // 点赞
            FECircleListModel.updateLikeNumber(topicId: item.id) { (isLike, msg) in
                if let like = isLike {
                    var citem = item
                    citem.is_click = like ? "1" : "0"
                    let lcount = Double(citem.click_num) ?? 0
                    if like {
                        citem.click_num = "\(Int(lcount)+1)"
                    } else {
                        citem.click_num = "\(Int(lcount)-1)"
                    }
                    self.model.datas[indexPath.row] = citem
                    self.tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
                } else {
                    QMUITips.showInfo(msg)
                }
            }
        } else if action == .comment {
            // 进详情
            self.enterDetail(indexPath.row)
        } else if action == .edit {
            // 编辑状态
            let asheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            _ = OwnerCricleEditType.allCases.map({ (t) -> Void in
                let a = UIAlertAction(title: t.name, style: .default) {[weak self] (action) in
                    
                    guard let self = self else {
                        return
                    }
                    self.model.editOwnerPublishItem(indexPath.row, t, { (msg) in
                        if let text = msg {
                            QMUITips.showInfo(text, in: self.view)
                            return
                        }
                        
                        self.tableView.reloadData()
                    })
                }
                asheet.addAction(a)
            })
            
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            asheet.addAction(cancel)
            
            self.present(asheet, animated: true, completion: nil)
        } else if action == .touchImage {
            guard let touimgv = touchImage, touimgv.0 < item.largePics.count else {
                return
            }
            
            let item = self.model.datas[indexPath.row]
//            let pic = item.largePics[touimgv.0]
//
//            let content = FEShowBigPictureView(URL(string: pic), touimgv.1)
//            content.show()
            
            let vc = FEShowBigPictureView2(images: item.largePics, index: touimgv.0, placeHolder: item.pics)
            self.present(vc)
//            self.present(vc, animated: true, completion: nil)
        } else if action == .groupDetail {
            // group detail
            self.enterGroupDetail(item)
        }
    }
    
    private func enterGroupDetail(_ item: FECircleListItem) {
        if item.uid == User.default.user_id {
            // 直接进群聊
            enterChatPage(item)
            return
        }
        
        HttpClient.default.ownerGroupApplyRoom(group_id: item.share_group_id, parent_id: item.uid) { (flag, msg) in
            if msg.code == 8004 {
                // 已经在群里了
                self.enterChatPage(item)
            } else if msg.code == 0 {
                QMUITips.showInfo(msg.text, in: self.view)
                self.enterChatPage(item)
            } else {
                QMUITips.showInfo(msg.text, in: self.view)
            }
        }
    }
    
    // 自建群列表界面
    private func enterChatPage(_ item: FECircleListItem) {
        let vc = FEGroupDetailListViewController()
        vc.model.groupType = GroupType(id: "all", name: "全部", logo: "")
        vc.model.isSystem = false
        self.push(to: vc, animated: true, completion: nil)
    }
}

