//
//  FEImageView.swift
//  Red
//
//  Created by MAC on 2020/4/1.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

/// 图片对象
class FEImageView: UIView {
    
    public lazy var imageView: UIImageView = {
        let v = UIImageView()
        v.frame = self.bounds
        v.isUserInteractionEnabled = true
        v.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(tapHandle)))
        v.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
        self.addSubview(v)
        return v
    }()
    
    public lazy var rightTop: UIButton = {
        let v = UIButton(type: .custom)
        v.setTitle("X", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.titleLabel!.font = UIFont.systemFont(ofSize: 15)
        v.frame = CGRect(x: self.bounds.size.width - 40, y: 0, width: 40, height: 40)
        v.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        v.addTarget(self, action: #selector(rightTopButtonAction), for: .touchUpInside)
        v.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleLeftMargin]
        self.imageView.addSubview(v)
        return v
    }()

    public var style: FEImageViewStyle {
        get {
            return __style
        }
        set (value) {
            __style = value
        }
    }
    
    
    private var scale: CGFloat = 1.0
    
    public var clickedHandler: ((_ action: FEImageViewAction)->Void)?
    
    private var __rightTopImage: UIImage?
    private var __image: UIImage?
    private var __style: FEImageViewStyle = .default
    private var __itemSize = CGSize(width: 50, height: 50)
    
    init(frame: CGRect, style: FEImageViewStyle? = nil, image: UIImage? = nil, rightTop: UIImage? = nil, scale: CGFloat = 1.0) {
        if let s = style {
            self.__style = s
        }
        if let img = image {
            self.__image = img
        }
        if let img = rightTop {
            self.__rightTopImage = img
        }
        
        self.scale = scale
        
        super.init(frame: frame)
        
        dosomeInitial()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FEImageView {
    private func dosomeInitial() {
        // scale有效值 (0-1]
        if scale < 1.0, scale > 0 {
            let currentSize = self.bounds.size.width
            let size: CGFloat = currentSize * scale
            let preCenter = imageView.center
            imageView.bounds = CGRect(x: 0, y: 0, width: size, height: size)
            imageView.center = preCenter
            
            let size2: CGFloat = rightTop.bounds.size.width * scale
            rightTop.frame = CGRect(x: size - size2, y: 0, width: size2, height: size2)
        }
        
        if let img = __image {
            imageView.image = img
        }
        
        rightTop.isHidden = true
        if __style != .default {
            rightTop.isHidden = false
            if let img = __rightTopImage {
                rightTop.setBackgroundImage(img, for: .normal)
            }
        }
    }
    
    @objc
    private func tapHandle() {
        self.clickedHandler?(.touchImage)
    }
    
    
    @objc
    private func rightTopButtonAction() {
        self.clickedHandler?(.touchRightTop)
    }
}


//MARK: - 显示状态
public enum FEImageViewStyle {
    case `default`
    case delete
}

//MARK: - 点击事件类型
public enum FEImageViewAction {
    case touchImage
    case touchRightTop
}
