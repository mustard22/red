//
//  FECircleDetailTopView.swift
//  Red
//
//  Created by MAC on 2020/4/21.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

class FECircleDetailTopView: UIView {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var avatarButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBAction func buttonAction(_ sender: UIButton) {
        buttonHandler?(nil)
    }
    
    let maxWidth: CGFloat = kScreenSize.width - 70
    
    lazy var imagesView: FECirclePublishSelectPhotosView = {
        let v = FECirclePublishSelectPhotosView(hmargin: 5, vmargin: 5, maxRows: 3, maxCols: 3, maxWidth: maxWidth, showAddButton: .hide, allowImageEdited: false, images: nil, urls: nil)
        v.isHidden = true
        v.touchItemHandler = {[weak self](index, action, touchItem) in
            self?.buttonHandler?((index, v.imageView(with: index)?.imageView.image))
        }
        self.addSubview(v)
        return v
    }()
    
    var buttonHandler: ((_ touchImage: (Int, UIImage?)?)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        nameLabel.textColor = UIColor.init(hex: 0x143B77)
    }
    
    func update(_ item: FECircleListItem) {
        avatarButton.kf.setBackgroundImage(with: URL(string: item.user_avatar), for: .normal)
        nameLabel.text = item.nickname
        timeLabel.text = item.date_time
        contentLabel.isHidden = true
        imagesView.isHidden = true
        if let fitem = item.itemFrame {
            if let cframe = fitem.content {
                contentLabel.isHidden = false
                contentLabel.text = item.content
                contentLabel.frame = cframe
                
            }
            if let iframe = fitem.image {
                imagesView.isHidden = false
                imagesView.frame = iframe
                imagesView.refreshItems(item.pics, nil)
                
            }
            if let bframe = fitem.bottomBar {
                bottomView.frame = bframe
            }
        }
    }
    
    static func height(_ item: FECircleListItem) -> FECircleListCellFrameItem {
        if let fitem = item.itemFrame {
            return fitem
        }
        
        var fitem = FECircleListCellFrameItem()
        
        var h: CGFloat = 15 + 20
        let left: CGFloat = 10 + 40 + 10
        let right: CGFloat = 10
        let width: CGFloat = kScreenSize.width - left - right
        
        if !item.content.isEmpty {
            
            var rect = NSString(string: item.content).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18)], context: nil)
            rect.size.height += 10
            
            fitem.content = CGRect(x: left, y: h, width: width, height: rect.size.height)
            
            h += rect.size.height
        }
        
        let imgCount = item.pics.count
        if imgCount > 0 {
            let col = 3
            let margin: CGFloat = 5
            let row = imgCount/col + (imgCount % col == 0 ? 0 : 1)
            let itemsize: CGFloat = (width - CGFloat(col-1)*margin)/CGFloat(col)
            let imgViewHeight = margin + CGFloat(row) * (itemsize + margin)
            
            fitem.image = CGRect(x: left, y: h, width: width, height: imgViewHeight)
            
            h += imgViewHeight
        }
        
        fitem.bottomBar = CGRect(x: left, y: h, width: width, height: 40)
        h += 40
        
        fitem.cellHeight = h

        return fitem
    }
}
