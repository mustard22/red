//
//  FECirclePublishSelectPhotosView.swift
//  Red
//
//  Created by MAC on 2020/4/1.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 图片铺展view
class FECirclePublishSelectPhotosView: UIView {
    /// 添加按钮显示方式
    public enum AddButtonShowType {
        /// 一直显示
        case always
        /// 一直隐藏
        case hide
        /// 由显示到隐藏
        case showToHide
    }
    
    //MARK: private vars
    /// 最多显示行数
    private var rows: CGFloat = 3
    
    /// 每行最多显示个数
    private var cols: CGFloat = 3
    
    /// 图片item大小
    private var itemSize: CGFloat = 90
    
    /// self的宽度
    private var maxWidth: CGFloat = UIScreen.main.bounds.size.width
    
    /// 图片对象(UIImage)数组
    private var images: [UIImage] = []
    
    /// 图片资源的字符串数组（本地图片名字或网络图片链接）
    private var urls: [String] = []
    
    /// 添加按钮的显示方式
    private var showAddButton: AddButtonShowType = .showToHide
    
    /// 是否允许图片item可编辑（如：删除事件）
    private var allowImageEdited: Bool = false
    
    /// 水平方向间隔
    private var hmargin: CGFloat = 10
    
    /// 竖直方向间隔
    private var vmargin: CGFloat = 10
    
    /// 图片item数组
    private var buttons: [FEImageView] = []
    
    private lazy var addButton: FEImageView = {
        let v = FEImageView(frame: CGRect(x: 0, y: 0, width: itemSize, height: itemSize), scale: self.scale)
        v.clickedHandler = {(action) in
            self.addButtonAction()
        }
        self.addSubview(v)
        
        let lab = UILabel()
        lab.text = "+"
        lab.textAlignment = .center
        lab.textColor = UIColor(hex: 0xee963e)
        lab.font = UIFont.systemFont(ofSize: 60)
        lab.layer.borderColor = UIColor(hex: 0xee963e).cgColor
        lab.layer.borderWidth = 5
        lab.layer.cornerRadius = 10
        lab.layer.masksToBounds = true
        lab.frame = v.imageView.frame
        v.addSubview(lab)
        return v
    }()
    
    // 内容图片相对于item的比例(0,1]
    private var scale: CGFloat = 1.0
    
    //MARK: init methods
    /// 初始化方法
    /// - parameter : hmargin           水平方向的间距
    /// - parameter : vmargin           竖直方向的间距
    /// - parameter : maxRows           最大显示行数
    /// - parameter : maxCols           最大显示列数
    /// - parameter : maxWidth          view最大显示宽度
    /// - parameter : showAddButton     添加按钮的显示方式
    /// - parameter : allowImageEdited  item是否可编辑（如：删除）
    /// - parameter : images            要显示的图片对象数组[UIImage]
    /// - parameter : urls              要显示的图片链接数组[String]
    /// - parameter : scale             图片相对于item的缩放比例
    init(hmargin: CGFloat = 10, vmargin: CGFloat = 10,  maxRows: Int? = nil, maxCols: Int? = nil, maxWidth: CGFloat? = nil, showAddButton: AddButtonShowType? = nil, allowImageEdited: Bool? = nil, images: [UIImage]? = nil, urls: [String]? = nil, scale: CGFloat = 1.0) {
        self.hmargin = hmargin
        self.vmargin = vmargin
        self.rows = CGFloat(maxRows ?? 3)
        self.cols = CGFloat(maxCols ?? 3)
        self.maxWidth = maxWidth ?? UIScreen.main.bounds.size.width
        self.showAddButton = showAddButton ?? .showToHide
        self.allowImageEdited = allowImageEdited ?? false
        self.images = images ?? []
        self.urls = urls ?? []
        self.scale = scale
        super.init(frame: .zero)
        dosomeInitial()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: public vars
    /// add按钮事件
    public var addButtonHandler: (()->Void)?
    /// show large or delete action
    public var touchItemHandler: ((_ index: Int , _ isTouchImage: Bool, _ touchView: UIView?)->Void)?
    
    public var itemBackColor: UIColor = UIColor.RGBColor(248, 248, 248)
}

//MARK: - private methods
extension FECirclePublishSelectPhotosView {
    /// self的初始化设置
    private func dosomeInitial() {
        itemSize = (maxWidth - hmargin * (cols-1))/cols
        self.bounds = CGRect(x: 0, y: 0, width: maxWidth, height: vmargin + rows * (itemSize + vmargin))
        
        self.setupAllItems()
    }
    
    /// 创建所有的图片item
    private func setupAllItems() {
        // 先移除之前创建好的
        for (_, item) in buttons.enumerated() {
            item.removeFromSuperview()
        }
        buttons.removeAll()
        
        let itemCount = images.isEmpty ? (urls.isEmpty ? 0 : urls.count) : images.count
        for i in 0..<itemCount {
            if i > Int(cols)*Int(rows)-1 {
                break
            }
            
           _ = self.createImageView(i)
        }
        
        self.resetHeight()
    }
    
    /// 重新设置self的高度；刷新所有item位置；设置addButton位置
    private func resetHeight() {
        var y: CGFloat = 0
        var x: CGFloat = 0
        for (i, imgv) in buttons.enumerated() {
            x = CGFloat(i % Int(cols)) * (itemSize + hmargin)
            y = vmargin + CGFloat(i / Int(cols)) * (itemSize + vmargin)
            imgv.origin = CGPoint(x: x, y: y)
            imgv.tag = i
        }
        
        self.height = y == 0 ? 0 : (y + itemSize + vmargin)
        
        var rect = addButton.frame
        if buttons.count % Int(cols) == 0 {
            rect.origin.x = 0
            rect.origin.y = y == 0 ? vmargin : y + itemSize + vmargin
        } else {
            rect.origin.x = x + itemSize + hmargin
            rect.origin.y = y
        }
        addButton.frame = rect
        
        switch showAddButton {
        case .hide:
            addButton.isHidden = true
        case .always:
            addButton.isHidden = false
            
            var origin = addButton.frame.origin
            if buttons.count >= Int(cols)*Int(rows) {
                origin = buttons.last!.frame.origin
                buttons.last!.removeFromSuperview()
                buttons.removeLast()
            }
            var rect = addButton.frame
            rect.origin = origin
            addButton.frame = rect
            self.height = addButton.bottom + vmargin
        case .showToHide:
            if buttons.count >= Int(cols)*Int(rows) {
                addButton.isHidden = true
            } else {
                addButton.isHidden = false
                self.height = addButton.bottom + vmargin
            }
        }
    }
    
    /// 删除一个图片item
    private func deleteItem(_ index: Int) {
        if index < 0 || index >= buttons.count {
            return
        }
        
        buttons[index].removeFromSuperview()
        buttons.remove(at: index)
        
        self.resetHeight()
    }
    
    /// 创建一个图片item
    private func createImageView(_ i: Int) -> FEImageView {
        let btn = FEImageView(frame: CGRect(x: 0, y: 0, width: itemSize, height: itemSize), style: allowImageEdited ? .delete : .default, scale: self.scale)
        btn.imageView.backgroundColor = self.itemBackColor
        
        if !images.isEmpty {
            btn.imageView.image = images[i]
            btn.imageView.backgroundColor = UIColor(white: 1, alpha: 0)
        } else if !urls.isEmpty {
            let url = urls[i]
            if url.hasPrefix("http") {
                btn.imageView.kf.setImage(with: URL(string: url)) { (res) in
                    btn.imageView.backgroundColor = UIColor(white: 1, alpha: 0)
                }
//                btn.imageView.setImage(with: URL(string: url), placeHolder: nil)
            } else {
                btn.imageView.backgroundColor = UIColor(white: 1, alpha: 0)
                btn.imageView.image = UIImage(named: url)
            }
        }
        
        btn.clickedHandler = {[weak self] (action) in
            self?.touchItemAction(action, btn.tag)
        }
        
        self.addSubview(btn)
        self.buttons.append(btn)
        
        return btn
    }
    
    /// FEImageView（图片item）点击事件
    private func touchItemAction(_ action: FEImageViewAction, _ index: Int) {
        var touchView: UIView?
        var isTouch = true
        if action == .touchImage {
            // 放大预览
            isTouch = true
            touchView = self.buttons[index]
        } else if action == .touchRightTop {
            // 右上角的按钮事件（如：删除操作）
            isTouch = false
            self.deleteItem(index)
        }
        self.touchItemHandler?(index, isTouch, touchView)
    }
    
    /// 添加按钮点击事件
    @objc
    private func addButtonAction() {
        self.addButtonHandler?()
    }
}


//MARK: - public methods
extension FECirclePublishSelectPhotosView {
    /// 根据index获取点击的FEImageView
    public func imageView(with index: Int) -> FEImageView? {
        if index < 0 || index >= buttons.count {
            return nil
        }
        return self.buttons[index]
    }
    
    /** 刷新所有的图片item（优先显示UIImage对象）
    *  parameter urls : 字符串数组（本地图片名字 或 网络图片的链接）
    *  parameter images : UIImage数组
    */
    public func refreshItems(_ urls: [String]? = nil, _ images: [UIImage]? = nil) {
        if let imgs = images {
            self.images = imgs
        }
        else if let urls = urls {
            self.urls = urls
        }
        
        self.setupAllItems()
    }
    
    /** 添加一个新的图片item（优先显示UIImage对象）
    * parameter url : 本地图片名字 或 网络图片的链接
    * parameter image : 要显示的图片对象
    */
    public func addItem(url: String? = nil, image: UIImage? = nil) {
        if url == nil && image == nil {
            return
        }
        
        var index = -1
        if let img = image {
            self.images.append(img)
            index = self.images.count-1
        }
        else if let url = url {
            self.urls.append(url)
            index = self.urls.count-1
        }
        _ = self.createImageView(index)
        self.resetHeight()
    }
}
