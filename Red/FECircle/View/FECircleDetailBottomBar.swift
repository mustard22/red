//
//  FECircleDetailBottomBar.swift
//  Red
//
//  Created by MAC on 2020/4/7.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

/// 评论界面底部输入框
class FECircleDetailBottomBar: UIView, UITextFieldDelegate {
    @IBOutlet weak var topLine: UILabel!
    
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var editLine: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    
    // 点击该按钮进入编辑状态
    @IBOutlet weak var maskButton: UIButton!
    
    var placeHolder: String = "" {
        didSet {
            self.input.placeholder = placeHolder
        }
    }
    
    /// 点击发送按钮，反馈到上层界面（content：输入内容）
    var sendHandler: ((_ content: String)->Void)?
    /// 点击maskButton按钮，开始编辑，反馈到上层界面
    var startEditHandler: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
         
        self.editLine.backgroundColor = UIColor.RGBColor(240, 240, 240)
        self.topLine.backgroundColor = UIColor.RGBColor(240, 240, 240)
        
        self.sendButton.layer.cornerRadius = 5
        self.sendButton.layer.backgroundColor = Color.red.cgColor
    }
}


extension FECircleDetailBottomBar {
    @IBAction func buttonAction(_ sender: UIButton) {
        if sender == maskButton {
            self.startEditHandler?()
            self.startEditing()
            return
        }
        
        // 点击发送 结束编辑
        self.endEditing()
        // 判断输入内容
        guard let text = input.text, !text.isEmpty else {
            return
        }
        
        self.sendHandler?(text)
    }
    
    //MARK: 开始编辑
    public func startEditing(_ placeHolder: String? = nil) {
        UIView.animate(withDuration: 0.2, animations: {
            self.maskButton.isHidden = true
            self.editLine.backgroundColor = Color.red
        }) { (f) in
            self.input.becomeFirstResponder()
            self.input.placeholder = placeHolder ?? self.placeHolder
        }
    }
    
    //MARK: 结束编辑
    public func endEditing() {
        if !self.input.isFirstResponder {
            return
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.maskButton.isHidden = false
            self.editLine.backgroundColor = UIColor.RGBColor(240, 240, 240)
        }) { (f) in
            self.input.text = ""
            self.input.resignFirstResponder()
            self.input.placeholder = self.placeHolder
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        buttonAction(sendButton)
        return true
    }
}
