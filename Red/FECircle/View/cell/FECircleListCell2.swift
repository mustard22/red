//
//  FECircleListCell2.swift
//  Red
//
//  Created by MAC on 2020/4/16.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

class FECircleListCell2: UITableViewCell {

    @IBOutlet weak var avatarButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var groupIcon: UIImageView!
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBAction func buttonAction(_ sender: UIButton) {
        if sender == avatarButton {
            // user
        } else {
            // detail
            buttonHandler?(CircleListCellAction.groupDetail, nil)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        groupIcon.backgroundColor = UIColor.RGBColor(240, 240, 240)
        
        contentLabel.textColor = UIColor(hex: 0x014469)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var buttonHandler: ((_ action: CircleListCellAction, _ touchImage: (Int, UIImage?)?)->Void)?
    
    func update(_ item: FECircleListItem) {
        nameLabel.text = item.nickname
        
        if item.uid == User.default.user_id {
            
            if item.status == "1" || item.status == "2" {
                let status = item.status == "1" ? "(公开)" : "(仅好友)"
                let text = item.nickname + status
                let attr = NSMutableAttributedString(string: text)
                attr.addAttribute(.foregroundColor, value: UIColor.darkGray, range: NSRange(location: item.nickname.count, length: status.count))
                nameLabel.attributedText = attr
            }
        }
        
        avatarButton.kf.setBackgroundImage(with: URL(string: item.user_avatar), for: .normal)
        timeLabel.text = item.date_time
        
        let values = item.content.components(separatedBy: "GROUP".md5())
        if values.count == 2 {
            contentLabel.text = "\(values.first!)\n\(values.last!)"
        } else {
            contentLabel.text = ""
        }
        
        groupIcon.image = UIImage(named: "ic_picture_default")
        guard let pic = item.pics.first else {
            return
        }
        
        groupIcon.setImage(with: URL(string: pic), placeHolder: UIImage(named: "ic_picture_default"))
    }
    
    static func cellHeight(_ item: FECircleListItem) -> FECircleListCellFrameItem {
        if let fitem = item.itemFrame {
            return fitem
        }
        
        var fitem = FECircleListCellFrameItem()
        fitem.cellHeight = 185
        return fitem
    }
}

