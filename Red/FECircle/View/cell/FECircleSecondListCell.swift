//
//  FECircleSecondListCell.swift
//  Red
//
//  Created by MAC on 2020/4/3.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

public enum CircleCommentListCellAction {
    case edit
    case delete
}

/// 评论列表二级评论cell
class FECircleSecondListCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var topbarView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    
    var buttonHandler: ((_ action: CircleCommentListCellAction)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        editButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        deleteButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        
        backView.layer.backgroundColor = UIColor.RGBColor(249, 242, 242).cgColor
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        var type = CircleCommentListCellAction.edit
        if sender == editButton {
            type = .edit
        } else if sender == deleteButton {
            type = .delete
        }
        buttonHandler?(type)
    }
    
    func update(_ item: FECircleCommentItem) {
        avatarImageView.setImage(with: URL(string: item.user_avatar), placeHolder: nil)
        
        let text = "\(item.nickname)回复\(item.role_nickname):"
        let attr = NSMutableAttributedString(string: text)
        attr.addAttribute(.foregroundColor, value: UIColor(hex: 0x7BBCFF), range: NSRange(location: 0, length: item.nickname.count))
        attr.addAttribute(.foregroundColor, value: UIColor(hex: 0xE4AB08), range: NSRange(location: text.count-item.role_nickname.count-1, length: item.role_nickname.count))
        nameLabel.attributedText = attr
        
        contentLabel.text = item.content
        
        if item.uid == User.default.user_id {
            deleteButton.isHidden = false
        } else {
            deleteButton.isHidden = true
        }
        
        if let rect = item.itemFrame, let crect = rect.content {
            contentLabel.frame = crect
        }
    }
    
    static func height(_ item: FECircleCommentItem) -> FECircleCommentFrameItem {
        if let fitem = item.itemFrame {
            return fitem
        }
        
        var fitem = FECircleCommentFrameItem()
        
        var h: CGFloat = 5 + 50
        let left: CGFloat = 15 + 30 + 30
        let right: CGFloat = 15
        let width: CGFloat = kScreenSize.width - left - right
        
        if !item.content.isEmpty {
            let rect = NSString(string: item.content).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)], context: nil)
            
            fitem.content = CGRect(x: 30, y: h, width: width, height: rect.size.height)
            
            h += rect.size.height + 5
        }
        
        fitem.cellHeight = h
        return fitem
    }
}




