//
//  FECircleListCell.swift
//  Red
//
//  Created by MAC on 2020/4/2.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

public enum CircleListCellAction {
    case like
    case comment
    case edit
    case touchImage
    case groupDetail
}

/// 动态列表cell
class FECircleListCell: UITableViewCell {
    
    @IBOutlet weak var avatarButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBAction func buttonAction(_ sender: UIButton) {
        var action: CircleListCellAction = .like
        if sender == likeButton {
            action = .like
        } else if sender == commentButton {
            action = .comment
        } else if sender == editButton {
            action = .edit
        } else {
            
        }
        buttonHandler?(action, nil)
    }
    
    lazy var imagesView: FECirclePublishSelectPhotosView = {
        let v = FECirclePublishSelectPhotosView(hmargin: 5, vmargin: 5, maxRows: 3, maxCols: 3, maxWidth: self.bottomView.width, showAddButton: .hide, allowImageEdited: false, images: nil, urls: nil)
        v.isHidden = true
        v.touchItemHandler = {[weak self](index, action, touchItem) in
            self?.buttonHandler?(.touchImage, (index, v.imageView(with: index)?.imageView.image))
        }
        self.contentView.addSubview(v)
        return v
    }()
    
    var buttonHandler: ((_ action: CircleListCellAction, _ touchImage: (Int, UIImage?)?)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        editButton.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        commentButton.imageEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
        likeButton.imageEdgeInsets = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        
        nameLabel.textColor = UIColor.init(hex: 0x143B77)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(_ item: FECircleListItem, _ showNewComment: Bool = false) {
        nameLabel.text = item.nickname
        editButton.isHidden = true
        
        if item.uid == User.default.user_id {
            editButton.isHidden = false
            
            if item.status == "1" || item.status == "2" {
                let status = item.status == "1" ? "(公开)" : "(仅好友)"
                let text = item.nickname + status
                let attr = NSMutableAttributedString(string: text)
                attr.addAttribute(.foregroundColor, value: UIColor.darkGray, range: NSRange(location: item.nickname.count, length: status.count))
                nameLabel.attributedText = attr
            }
        }
        
        avatarButton.kf.setBackgroundImage(with: URL(string: item.user_avatar), for: .normal)
        timeLabel.text = item.date_time
        
        likeButton.isSelected = item.is_click == "1" ? true : false
        likeLabel.text = item.click_num
        
        commentLabel.text = item.com_num
        commentLabel.textColor = .black
        // 是否显示新增评论数
        if showNewComment, let newnum = Double(item.nem_comment_num), Int(newnum) > 0 {
            commentLabel.text = "+\(item.nem_comment_num)"
            commentLabel.textColor = Color.red
        }
        
        contentLabel.isHidden = true
        imagesView.isHidden = true
        if let fitem = item.itemFrame {
            if let cframe = fitem.content {
                contentLabel.isHidden = false
                contentLabel.text = item.content
                contentLabel.frame = cframe
            }
            if let iframe = fitem.image {
                imagesView.isHidden = false
                imagesView.frame = iframe
                imagesView.refreshItems(item.pics, nil)
            }
            
            if let bframe = fitem.bottomBar {
                bottomView.frame = bframe
            }
        }
    }
    
    static func cellHeight(_ item: FECircleListItem) -> FECircleListCellFrameItem {
        if let fitem = item.itemFrame {
            return fitem
        }
        
        var fitem = FECircleListCellFrameItem()
        
        var h: CGFloat = 15 + 20
        let left: CGFloat = 10 + 40 + 10
        let right: CGFloat = 10
        let width: CGFloat = kScreenSize.width - left - right
        
        if !item.content.isEmpty {
            
            var rect = NSString(string: item.content).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18)], context: nil)
            rect.size.height += 10
            
            fitem.content = CGRect(x: left, y: h, width: width, height: rect.size.height)
            
            h += rect.size.height
        }
        
        let imgCount = item.pics.count
        if imgCount > 0 {
            let col = 3
            let margin: CGFloat = 5
            let row = imgCount/col + (imgCount % col == 0 ? 0 : 1)
            let itemsize: CGFloat = (width - CGFloat(col-1)*margin)/CGFloat(col)
            let imgViewHeight = margin + CGFloat(row) * (itemsize + margin)
            
            fitem.image = CGRect(x: left, y: h, width: width, height: imgViewHeight)
            
            h += imgViewHeight
        }
        
        fitem.bottomBar = CGRect(x: left, y: h, width: width, height: 40)
        h += 40
        
        fitem.cellHeight = h
        return fitem
    }
}
