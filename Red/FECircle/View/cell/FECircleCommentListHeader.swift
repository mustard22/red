//
//  FECircleCommentListHeader.swift
//  Red
//
//  Created by MAC on 2020/4/3.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

/// 一级评论的事件类型枚举
public enum CircleCommentListHeaderAction {
    /// 更多
    case more
    /// 写评论
    case edit
    /// 删除
    case delete
}

/// 一级评论view
class FECircleCommentListHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var toplineView: UIView!
    
    @IBOutlet weak var topbarView: UIView!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBAction func buttonAction(_ sender: UIButton) {
        var type = CircleCommentListHeaderAction.more
        if sender == moreButton {
            type = .more
        } else if sender == editButton {
            type = .edit
        } else if sender == deleteButton {
            type = .delete
        }
        buttonHandler?(type)
    }
    
    
    var buttonHandler: ((_ action: CircleCommentListHeaderAction)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
        moreButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        editButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        deleteButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        
        nameLabel.textColor = UIColor(hex: 0x7BBCFF)
        toplineView.layer.backgroundColor = UIColor.RGBColor(240, 240, 240).cgColor
        
        deleteButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(30)
            make.right.equalTo(self.contentView).offset(-15)
            make.top.equalTo(10)
        }
        
        editButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(deleteButton)
            make.right.equalTo(deleteButton.snp.left).offset(-10)
            make.top.equalTo(deleteButton)
        }
        
        moreButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(deleteButton)
            make.right.equalTo(editButton.snp.left).offset(-10)
            make.top.equalTo(deleteButton)
        }
    }
    
    func update(_ item: FECircleCommentItem) {
//        toplineView.layer.backgroundColor = UIColor.RGBColor(240, 240, 240).cgColor
        avatarImageView.setImage(with: URL(string: item.user_avatar), placeHolder: nil)
        nameLabel.text = item.nickname
        contentLabel.text = item.content
        
        if item.uid == User.default.user_id {
            deleteButton.isHidden = false
        } else {
            deleteButton.isHidden = true
        }
        
        if let rect = item.itemFrame, let crect = rect.content {
            contentLabel.frame = crect
        }
    }
    
    static func height(_ item: FECircleCommentItem) -> FECircleCommentFrameItem {
        if let fitem = item.itemFrame {
            return fitem
        }
        
        var fitem = FECircleCommentFrameItem()
        
        var h: CGFloat = 5 + 50
        let left: CGFloat = 15 + 30
        let right: CGFloat = 15
        let width: CGFloat = kScreenSize.width - left - right
        
        if !item.content.isEmpty {
            let rect = NSString(string: item.content).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)], context: nil)
            
            fitem.content = CGRect(x: left, y: h, width: width, height: rect.size.height)
            
            h += rect.size.height + 5
        }
        
        fitem.cellHeight = h
        return fitem
    }
}
