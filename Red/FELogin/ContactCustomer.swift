//
//  ContactCustomer.swift
//  Red
//
//  Created by MAC on 2019/11/5.
//  Copyright © 2019 MAC. All rights reserved.
//
import QMUIKit
import Foundation

/// 进客服
class ContactCustomer {
    /** 联系客服（登录界面，大厅，活动，群聊，好友聊天，充值，我的）
     *  @param currentvc 当前发起联系客服事件的界面
     *  @param enterChat 联系客服的方式：默认打开客服链接（false）；进入客服聊天界面(true)
    */
    static func startContact(currentvc: UIViewController, enterChat: Bool = false) {
        // 当前处于未登录状态
        if User.default.isLogin == false {
            // 点击登录界面的客服
            QMUITips.show(withText: "客服不在线", in: currentvc.view)
            return
        }
        
        // 判断 客服链接 和 联系客服方式
        if enterChat == false {
            let banner = UserCenter.default.lbanner
            if banner.customer_link.hasPrefix("http") {
                currentvc.push(to: FELoginContactCustomerViewController())
            } else {
                QMUITips.show(withText: "客服不在线", in: currentvc.view)
            }
            return
        }
        
        
        // 两种情况走到这里
        // 1.联系客服方式为true
        // 2.联系客服方式为false，但客服链接为空
        var p = FriendListParam()
        p.pageSize = "5"
        p.pageIndex = "1"
        HttpClient.default.getFriendList(param: p, showCache: true) { (status, msg, data) in
            if status {
                let items = data.data!
                var customer: FriendItem?
                for item in items {
                    // 选在线客服
                    if item.friend_type == "3" && item.on_line == "true" {
                        customer = item
                        break
                    }
                }
                
                if customer == nil {
                    // 都不在线 选第一个
                    for item in items {
                        if item.friend_type == "3" {
                            customer = item
                            break
                        }
                    }
                }
                
                if let custom = customer {
                    let vc = FEChatFriendViewController(chat_id: custom.friend_id, chat_type: .customer)
                    vc.friend = custom
                    currentvc.push(to: vc)
                } else {
                    QMUITips.show(withText: "暂无客服", in: currentvc.view)
                }
                
            } else {
                
                QMUITips.show(withText: "暂无客服", in: currentvc.view)
            }
        }
    }
}
