//
//  FERegisterViewController.swift
//  Red
//
//  Created by MAC on 2019/8/8.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import SnapKit
class FERegisterViewController: FEBaseViewController {

    var parameter = RegistParam()
    
    private var timer: Timer?
    private var count = 60
    
    private lazy var backTable: UITableView = {
        let v = UITableView()
        v.separatorStyle = .none
        v.frame = view.bounds
        v.backgroundColor = view.backgroundColor
        v.delegate = self as UITableViewDelegate
        view.addSubview(v)
        return v
    }()
    
    private lazy var contentView: UIView = {
        let v = UIView()
        backTable.tableHeaderView = v
        return v
    }()
    
    lazy var phoneTextField:UITextField = {
        let v = makeStyleTF(image: "iphone", placeholder: "请输入手机号")
        v.keyboardType = UIKeyboardType.numberPad
        v.maximumTextLength = 11
        contentView.addSubview(v)
        return v
    }()
    
    lazy var smsButton: QMUIButton = {
        let b = QMUIButton(type: .custom)
        b.size = CGSize(width: 80*kScreenScale, height: 25*kScreenScale)
        b.setTitle("获取验证码", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        b.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        b.addTarget(self, action: #selector(handleSms(sender:)), for: .touchUpInside)
        b.layer.cornerRadius = 5.0
        return b
    }()
    
   
    lazy var smsTextField:UITextField = {
        let v = makeStyleTF(image: "safety", placeholder: "请输入验证码", rightBtn: true)
        v.keyboardType = .numberPad
        v.maximumTextLength = 6
        contentView.addSubview(v)
        return v
    }()
    
   
    lazy var passwordTextField:UITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请输入密码(字母和数字组合)")
        v.isSecureTextEntry = true
        v.autocapitalizationType = .none // 禁止默认首字母大写
        contentView.addSubview(v)
        return v
    }()
    
    lazy var againPasswordTextField: UITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请确认密码")
        v.isSecureTextEntry = true
        v.autocapitalizationType = .none // 禁止默认首字母大写
        contentView.addSubview(v)
        return v
    }()
    
   
    lazy var invitationCodeTextField: UITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请输入邀请码（选填）")
        v.maximumTextLength = 8 // 邀请码固定8位, GB5628E2
        v.autocapitalizationType = .none // 禁止默认首字母大写
        contentView.addSubview(v)
        return v
    }()
    
    lazy var registerBnt:QMUIButton = {
        let regist = QMUIButton()
        regist.backgroundColor = Color.red
        regist.setTitleColor(.white, for: .normal)
        regist.layer.cornerRadius = 5
        regist.layer.masksToBounds = true
        regist.setTitle("注册", for: .normal)
        regist.addTarget(self, action: #selector(handleRegister(sender:)), for: .touchUpInside)
        contentView.addSubview(regist)
        return regist
    }()
    
    
    @discardableResult
    private func makeStyleTF(_ textField: QMUITextField? = nil, image: String, placeholder: String, rightBtn: Bool? = false) -> QMUITextField {
        let imageView = UIImageView()
        imageView.image = UIImage(named: image)
        imageView.size = CGSize(width: 20, height: 20)
        
        let textfield = textField ?? QMUITextField(placeholder, font: UIFont.systemFont(ofSize: 15), color: Color.text)
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
        if rightBtn! {
            let view = UIView()
            view.size = self.smsButton.size
            view.addSubview(self.smsButton)
            textfield.rightView = view
            textfield.rightViewMode = .always
        }
        
        textfield.font = UIFont.systemFont(ofSize: 15)
        textfield.placeholder = placeholder
        
        textfield.returnKeyType = .next
        textfield.delegate = self
        textfield.qmui_borderPosition = .bottom
        textfield.qmui_borderColor = .darkGray
        textfield.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        return textfield
    }
    
    // 邀请码
    private var firstInstallCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.title = "注册"
        view.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
        
        if let code = UserDefaults.Common.firstInstallInviteCode {
            firstInstallCode = code
        }
        createUI()
        
        phoneTextField.becomeFirstResponder()
    }
    override func setupNavigationItems() {
        super.setupNavigationItems()
    }
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    func createUI() {
        backTable.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        contentView.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        phoneTextField.snp.makeConstraints { (make) in
            make.top.equalTo(40)
            make.left.equalTo(15)
            make.right.equalTo(contentView.snp.right).offset(-15)
            make.height.equalTo(51)
        }
        
        smsTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneTextField.snp.bottom)
            make.left.equalTo(phoneTextField.snp.left)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(smsTextField.snp.bottom)
            make.left.equalTo(phoneTextField.snp.left)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        againPasswordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp.bottom)
            make.left.equalTo(phoneTextField.snp.left)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        if self.firstInstallCode != nil {
            invitationCodeTextField.isHidden = true
            registerBnt.snp.makeConstraints { (make) in
                make.top.equalTo(againPasswordTextField.snp.bottom).offset(40)
                make.right.equalTo(contentView.snp.right).offset(-20)
                make.left.equalTo(20)
                make.height.equalTo(40)
            }
        } else {
            invitationCodeTextField.snp.makeConstraints { (make) in
                make.top.equalTo(againPasswordTextField.snp.bottom)
                make.left.equalTo(phoneTextField.snp.left)
                make.size.equalTo(phoneTextField.snp.size)
            }
            registerBnt.snp.makeConstraints { (make) in
                make.top.equalTo(invitationCodeTextField.snp.bottom).offset(40)
                make.right.equalTo(contentView.snp.right).offset(-20)
                make.left.equalTo(20)
                make.height.equalTo(40)
            }
        }
    }
    
    @objc func handleSms(sender _:Any) {
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
       
        HttpClient.default.GetSMSCode(phone: phoneTextField.text!) { (isSuccess, message) in
            
            if isSuccess {
                
                self.smsButton.isEnabled = false
                self.smsButton.backgroundColor = .lightGray
                self.setTimer()
            }
            QMUITips.show(withText: message, in: self.view)
        }
    }
    
    // 点击注册
    @objc func handleRegister(sender _:Any) {
        self.view.endEditing(true)
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        guard let sms = smsTextField.text, sms.count == 6 else {
            QMUITips.show(withText: "验证码有误", in: self.view)
            return
        }
        guard let pwd = passwordTextField.text, let againpwd = againPasswordTextField.text, pwd.count>0, againpwd.count>0, pwd == againpwd else {
            QMUITips.show(withText: "密码输入不合法", in: self.view)
            return
        }
        
        let invite_code = firstInstallCode ?? (invitationCodeTextField.text ?? "")
        
        parameter.mobile = phone
        parameter.verify_code = sms
        parameter.password = pwd
        parameter.sure_pwd = againpwd
        parameter.invite_code = invite_code.isEmpty ? "0" : invite_code // 没有输入传0
        
        let loagingView = QMUITips.showLoading(in: self.view)
        UserCenter.default.regist(parame: parameter) {[weak self] (isSuccess,message) in
            guard let self = self else {
                return
            }
            if isSuccess {
                if self.firstInstallCode != nil {
                    // 移除第一次安装缓存的邀请码
                    UserDefaults.Common.firstInstallInviteCode = nil
                }
//                QMUITips.show(withText: "注册成功", in: self.view)
                self.loginAfterRegister(loagingView)
            } else {
                loagingView.hide(animated: true)
                QMUITips.show(withText: "\(message)", in: self.view)
            }
        }
    }
    
    func loginAfterRegister(_ load: QMUITips) {
        /// 注册时填写了 账号，使用账号直接登录
        var parate = LoginParam()
        parate.type = .account
        parate.code = parameter.mobile
        parate.password = parameter.password
        
        UserCenter.default.login(parame:parate) { (isSuccess,message,info) in
            load.hide(animated: true)
            
            UserCenter.default.autoLogin({ (isSuccess,message) in
                if isSuccess {
                    self.close()
                    // 登录成功后初始化socket
                    FEWebSocketManager.initialWebSocket()
                    // 初始化缓存
                    MMKVManager.initial(User.default.user_id)
                    UIApplication.shared.switchRootViewController(FETabBarController())
                } else {
                    QMUITips.show(withText:  message, in: self.view)
                }
            })
        }
    }
}

//MARK: 定时器
extension FERegisterViewController {
    func setTimer() {
        self.stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(dowithTimer(_:)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        if let t = timer {
            t.invalidate()
            timer = nil
        }
    }
    
    @objc func dowithTimer(_ t: Timer) {
        count -= 1
        if count == 0 {
            smsButton.isEnabled = true
            smsButton.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
            self.stopTimer()
        } else {
            smsButton.setTitle("重新获取(\(count))", for: .disabled)
        }
    }
}

//MARK: QMUITextFieldDelegate
extension FERegisterViewController: QMUITextFieldDelegate {
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {

    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if passwordTextField.isFirstResponder {
            passwordTextField.resignFirstResponder()
            againPasswordTextField.becomeFirstResponder()
        }
        else if againPasswordTextField.isFirstResponder {
            againPasswordTextField.resignFirstResponder()
            if invitationCodeTextField.isHidden {
                handleRegister(sender: registerBnt)
            } else {
                invitationCodeTextField.becomeFirstResponder()
            }
        }
        else if invitationCodeTextField.isFirstResponder {
            invitationCodeTextField.resignFirstResponder()
            handleRegister(sender: registerBnt)
        }
        return true
    }
}

//MARK: scroll delegate
extension FERegisterViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

