//
//  FEBindingPhoneViewController.swift
//  Red
//
//  Created by MAC on 2019/10/26.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import SnapKit

/// 绑定手机
class FEBindingPhoneViewController: FEBaseViewController {
    private var timer: Timer?
    private var count = 60
    
    let parameter = RegistParam()
    
    private lazy var backTable: UITableView = {
        let v = UITableView()
        v.separatorStyle = .none
        v.frame = view.bounds
        v.backgroundColor = view.backgroundColor
        view.addSubview(v)
        return v
    }()
    
    private lazy var contentView: UIView = {
        let v = UIView()
        backTable.tableHeaderView = v
        return v
    }()
    
    lazy var phoneTextField:UITextField = {
        let v = makeStyleTF(image: "iphone", placeholder: "请输入手机号")
        v.keyboardType = .numberPad
        v.maximumTextLength = 11
        contentView.addSubview(v)
        return v
    }()
    
    // 短信验证码按钮
    lazy var smsButton: QMUIButton = {
        let b = QMUIButton(type: .custom)
        b.size = CGSize(width: 80*kScreenScale, height: 25*kScreenScale)
        b.setTitle("获取验证码", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        b.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        b.addTarget(self, action: #selector(getSmsCode(_:)), for: .touchUpInside)
        b.layer.cornerRadius = 5.0
        return b
    }()
    
    
    lazy var smsTextField:UITextField = {
        let textField = makeStyleTF(image: "safety", placeholder: "请输入验证码", rightBtn: true)
        textField.keyboardType = .numberPad
        contentView.addSubview(textField)
        return textField
    }()
    
    
    lazy var findPasswdBtn: QMUIButton = {
        let v = QMUIButton()
        v.backgroundColor = Color.red
        v.setTitleColor(.white, for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.setTitle("提交", for: .normal)
        v.addTarget(self, action: #selector(handleFindPasswd(sender:)), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    var bindPhoneHandler: ((_ bindPhone: String)->Void)?
    
    /// 是否成功绑定手机号
    private var isSuccessOfBindPhone = false
    
    @discardableResult
    private func makeStyleTF(_ textField: QMUITextField? = nil, image: String, placeholder: String, rightBtn: Bool? = false) -> QMUITextField {
        let imageView = UIImageView()
        imageView.image = UIImage(named: image)
        imageView.size = CGSize(width: 20, height: 20)
        
        let textfield = textField ?? QMUITextField(placeholder, font: UIFont.systemFont(ofSize: 15), color: Color.text)
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
        if rightBtn! {
            let view = UIView()
            view.size = self.smsButton.size
            view.addSubview(self.smsButton)
            textfield.rightView = view
            textfield.rightViewMode = .always
        }
        
        textfield.font = UIFont.systemFont(ofSize: 15)
        textfield.placeholder = placeholder
        
        textfield.returnKeyType = .next
        textfield.delegate = self
        textfield.qmui_borderPosition = .bottom
        textfield.qmui_borderColor = .darkGray
        textfield.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        return textfield
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "绑定手机号"
        view.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
        
        createUI()
        
        phoneTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isSuccessOfBindPhone == false {
            HttpClient.default.tokenData = ("","")
        }
    }
    
    func createUI() {
        
        backTable.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        contentView.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        phoneTextField.snp.makeConstraints { (make) in
            make.top.equalTo(40)
            make.left.equalTo(15)
            make.right.equalTo(contentView.snp.right).offset(-15)
            make.height.equalTo(51)
        }
        
        smsTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneTextField.snp.bottom)
            make.left.equalTo(phoneTextField.snp.left)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        findPasswdBtn.snp.makeConstraints { (make) in
            make.top.equalTo(smsTextField.snp.bottom).offset(40)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.left.equalTo(20)
            make.height.equalTo(40)
        }
        
    }
    
    @objc func handleFindPasswd(sender _:Any) {
        self.view.endEditing(true)
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        
        guard let sms = smsTextField.text, sms.count == 6 else {
            QMUITips.show(withText: "验证码有误", in: self.view)
            return
        }

        var param = BindPhoneParam()
        param.mobile = phoneTextField.text
        param.verify_code = smsTextField.text
        
        UserCenter.default.bindPhone(param, { (status, msg) in
            if status {
                QMUITips.show(withText: "绑定成功", in: self.view)
                if let handler = self.bindPhoneHandler {
                    self.isSuccessOfBindPhone = true
                    handler(param.mobile ?? "")
                    self.close()
                }
            } else {
                QMUITips.show(withText: "\(msg)")
            }
        })
    }
    
    // 获取验证码
    @objc func getSmsCode(_ btn: QMUIButton) {
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        
        HttpClient.default.GetSMSCode(phone: phoneTextField.text!) { (status, msg) in
            if status {
                self.smsButton.isEnabled = false
                self.smsButton.backgroundColor = .lightGray
                self.setTimer()
            }
            QMUITips.show(withText: msg, in: self.view)
        }
    }
}

extension FEBindingPhoneViewController: QMUITextFieldDelegate{
    
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
}



//MARK: 定时器
extension FEBindingPhoneViewController {
    func setTimer() {
        self.stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(dowithTimer(_:)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        if let t = timer {
            t.invalidate()
            timer = nil
        }
    }
    
    @objc func dowithTimer(_ t: Timer) {
        count -= 1
        if count == 0 {
            smsButton.isEnabled = true
            smsButton.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
            self.stopTimer()
        } else {
            smsButton.setTitle("重新获取(\(count))", for: .disabled)
        }
    }
}



