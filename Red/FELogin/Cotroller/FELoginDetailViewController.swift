//
//  FELoginDetailViewController.swift
//  Red
//
//  Created by MAC on 2019/8/20.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import SnapKit
import SwiftyJSON
class FELoginDetailViewController: FEBaseViewController {
    private var timer: Timer?
    private var count = 60
    
    private lazy var backTable: UITableView = {
        let v = UITableView()
        v.separatorStyle = .none
        v.frame = view.bounds
        v.backgroundColor = view.backgroundColor
        view.addSubview(v)
        return v
    }()
    
    private lazy var contentView: UIView = {
        let v = UIView()
        v.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(tapHandle)))
        backTable.tableHeaderView = v
        return v
    }()
    
    /// 密码输入框
    lazy var passwordTextField:QMUITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请输入密码")
        v.autocapitalizationType = .none // 禁止默认首字母大写
        v.isSecureTextEntry = true
        contentView.addSubview(v)
        return v
    }()
    
    
    /// 手机号输入框
    lazy var phoneTextField:QMUITextField = {
        let v = makeStyleTF(image: "iphone", placeholder: "请输入手机号")
        v.text = UserDefaults.Common.lastLoginMobile ?? ""
        v.keyboardType = .numberPad
        v.maximumTextLength = 11
        contentView.addSubview(v)
        return v
    }()
    
    /// 验证码输入框
    lazy var smsTextField:QMUITextField = {
        let v = makeStyleTF(image: "safety", placeholder: "请输入验证码", rightBtn: true)
        v.keyboardType = .numberPad
        v.isHidden = true
        v.maximumTextLength = 6
        contentView.addSubview(v)
        return v
    }()
    
    lazy var exchangeButton : QMUIButton = {
        let v = QMUIButton("验证码登录",fontSize: 14)
        v.setTitle("验证码登录", for: .normal)
        v.setTitleColor(Color.red, for: .normal)
        v.setTitleColor(Color.red, for: .selected)
        v.setTitle("账号密码登录", for: .selected)
        v.contentHorizontalAlignment = .left
        v.addTarget(self, action: #selector(handlerExchangeLoginType(_:)), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    lazy var registerButton : QMUIButton = {
        let v = QMUIButton("立即注册",fontSize: 14)
        v.setTitleColor(.black, for: .normal)
        v.addTarget(self, action: #selector(handlerRegister(_:)), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    lazy var findPasswdButton : QMUIButton = {
        let v = QMUIButton("找回密码?",fontSize: 14)
        v.setTitleColor(Color.red, for: .normal)
        v.addTarget(self, action: #selector(handlerFindPasswd(_:)), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    lazy var loginBut : QMUIButton = {
        let v = QMUIButton("登录",fontSize: 18)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.addTarget(self, action: #selector(handleLoginBtn(sender:)), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    // 短信验证码按钮
    lazy var smsButton: QMUIButton = {
        let b = QMUIButton(type: .custom)
        b.size = CGSize(width: 80*kScreenScale, height: 25*kScreenScale)
        b.setTitle("获取验证码", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        b.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        b.addTarget(self, action: #selector(getSmsCode(_:)), for: .touchUpInside)
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        return b
    }()
    
    // 用户申诉按钮
   private lazy var userAppealBtn: QMUIButton = {
       let v = QMUIButton("账号申诉", fontSize: 16)
       v.setTitleColor(.darkGray, for: .normal)
       v.addTarget(self, action: #selector(handlerUserAppeal), for: .touchUpInside)
       contentView.addSubview(v)
       return v
   }()
    
    private lazy var customerButton: QMUIButton = {
        let c = QMUIButton()
        c.backgroundColor = Color.red
        c.setTitleColor(.white, for: .normal)
        c.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        c.titleLabel?.numberOfLines = 0
        c.layer.cornerRadius = 5
        c.layer.masksToBounds = true
        c.setTitle("联系客服", for: .normal)
        
        c.addTarget(self, action: #selector(handlerCustomer), for: .touchUpInside)
        contentView.addSubview(c)
        return c
    }()
    
    private lazy var tipsLabel: QMUILabel = {
        let c = QMUILabel()
        c.textAlignment = .center
        c.textColor = UIColor.lightGray
        c.text = "其他登录方式"
        c.font = UIFont.systemFont(ofSize: 13)
        c.backgroundColor = view.backgroundColor
        contentView.addSubview(c)
        return c
    }()
    
    private lazy var sepline: UIImageView = {
        let c = UIImageView()
        c.backgroundColor = UIColor.RGBColor(248, 248, 248)
        contentView.addSubview(c)
        return c
    }()
    
    private lazy var wechatButton: QMUIButton = {
        let c = QMUIButton()
        c.setBackgroundImage(UIImage(named: "weixinLogin"), for: .normal)
        c.addTarget(self, action: #selector(handlerWechat), for: .touchUpInside)
        contentView.addSubview(c)
        return c
    }()
    
    /// 是否正在登录中(获取权限通知有时会发2次 避免此情况)
    var isLogining = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        
        self.setupUI()
        self.registerAllObserver()
        
        if let phone = phoneTextField.text, phone.count > 0 {
            passwordTextField.becomeFirstResponder()
        } else {
            phoneTextField.becomeFirstResponder()
        }
        
        //MARK: test code
//        passwordTextField.text = "c123456h"
    }
    
    deinit {
        self.removeAllObserver()
    }
}

//MARK: UI
extension FELoginDetailViewController {
    func setupUI() {
        self.title = "登录"
        view.backgroundColor = .white
        
        // 判断是否显示微信登录
        let data = UserCenter.default.lbanner
        if data.wechat_switch == "0" {
            tipsLabel.isHidden = true
            sepline.isHidden = true
            wechatButton.isHidden = true
        }
        
        backTable.snp.makeConstraints { (make) in
            make.size.equalTo(self.view)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        contentView.snp.makeConstraints { (make) in
            make.width.equalTo(backTable)
            make.height.equalTo(view.height - kNaviBarHeight - (kIsIphoneX ? 20 : 0))
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        phoneTextField.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.top.equalTo(20)
            make.height.equalTo(40)
        }
        smsTextField.snp.makeConstraints { (make) in
            make.left.equalTo(phoneTextField)
            make.top.equalTo(phoneTextField.snp.bottom).offset(10)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        passwordTextField.snp.makeConstraints { (make) in
            make.left.equalTo(phoneTextField.snp.left)
            make.top.equalTo(phoneTextField.snp.bottom).offset(10)
            make.size.equalTo(phoneTextField.snp.size)
        }
        exchangeButton.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.top.equalTo(passwordTextField.snp.bottom).offset(10)
            make.height.equalTo(40)
            make.width.equalTo(120)
        }
        
        findPasswdButton.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.top.equalTo(exchangeButton.snp.top)
            make.height.equalTo(40)
            make.width.equalTo(80)
        }
        
        registerButton.snp.makeConstraints { (make) in
            make.right.equalTo(findPasswdButton.snp.left).offset(-10)
            make.top.equalTo(exchangeButton.snp.top)
            make.height.equalTo(40)
            make.width.equalTo(80)
        }
        loginBut.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.top.equalTo(exchangeButton.snp.bottom).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-30)
            make.height.equalTo(40)
        }
        
        customerButton.snp.makeConstraints { (make) in
            make.top.equalTo(loginBut.snp.bottom).offset(30)
            make.right.equalTo(contentView.snp.right).offset(-1)
            make.size.equalTo(CGSize(width: 30, height: 90))
        }
        
        sepline.snp.makeConstraints { (make) in
            make.height.equalTo(1.0)
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.top.equalTo(customerButton.snp.bottom).offset(50)
        }
        
        tipsLabel.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 120, height: 21))
            make.center.equalTo(sepline.snp.center)
        }
        
        wechatButton.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.top.equalTo(tipsLabel.snp.bottom).offset(20)
            make.centerX.equalTo(sepline.snp.centerX)
        }
        
        userAppealBtn.snp.makeConstraints { (make) in
            make.width.equalTo(120)
            make.height.equalTo(35)
            make.bottom.equalTo(contentView).offset(-20)
            make.centerX.equalTo(contentView)
        }
    }
    
    @discardableResult
    private func makeStyleTF(_ textField: QMUITextField? = nil, image: String, placeholder: String, rightBtn: Bool? = false) -> QMUITextField {
        let imageView = UIImageView()
        imageView.image = UIImage(named: image)
        imageView.size = CGSize(width: 20, height: 20)
        
        let textfield = textField ?? QMUITextField(placeholder, font: UIFont.systemFont(ofSize: 15), color: Color.text)
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
        if rightBtn! {
            let view = UIView()
            view.size = self.smsButton.size
            view.addSubview(self.smsButton)
            textfield.rightView = view
            textfield.rightViewMode = .always
        }
        
        textfield.font = UIFont.systemFont(ofSize: 15)
        textfield.placeholder = placeholder
        
        textfield.returnKeyType = .next
        textfield.delegate = self
        textfield.qmui_borderPosition = .bottom
        textfield.qmui_borderColor = .darkGray
        textfield.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        return textfield
    }
}

//MARK: 注册观察
extension FELoginDetailViewController {
    func registerAllObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(checkWeixinAuth(_:)), name: .checkWeixinAuth, object: nil)
    }
    
    func removeAllObserver() {
        NotificationCenter.default.removeObserver(self, name: .checkWeixinAuth, object: nil)
    }
    
    @objc func checkWeixinAuth(_ n: Notification) {
        UserDefaults.Common.lastLoginMobile = nil
        
        let res = n.userInfo![NSNotification.Name.checkWeixinAuth] as! SendAuthResp
        guard let code = res.code, code.count > 0 else {
            QMUITips.show(withText: "授权失败", in: self.view)
            return
        }
        
        guard isLogining == false else {
            return
        }
        
        isLogining = true
        
        // weixin login
        var param = LoginParam()
        param.type = .wechat
        param.code = res.code

        if let invite_code = UserDefaults.Common.firstInstallInviteCode {
            param.invite_code = invite_code
        }
        UserCenter.default.login(parame:param) {[weak self] (isSuccess,message, info) in
            if param.invite_code != nil {
                // 移除第一次安装缓存的邀请码
                UserDefaults.Common.firstInstallInviteCode = nil
            }
            guard let self = self else {
                QMUITips.show(withText: "程序异常")
                return
            }
            guard let login = info else {
                QMUITips.show(withText: message, in: self.view)
                return
            }
            self.checkBindPhone(login)
        }
    }
    
    //MARK: 进首页前的设置
    func dosomeSettingBeforeEnterHome() {
        // 记录用户登录次数
        UserDefaults.Current.userLoginTime = UserDefaults.Current.userLoginTime + 1
        
        // 关闭登录界面 进入首页
        self.close()
        
        // 登录成功后初始化socket
        FEWebSocketManager.initialWebSocket()
        
        // 初始化缓存
        MMKVManager.initial(User.default.user_id)
        
        // 切换root vc
        UIApplication.shared.switchRootViewController(FETabBarController())
    }
    
    // 检测是否绑定手机号
    func checkBindPhone(_ info: Login) {
        if info.mobile.count>1 {
            UserDefaults.Common.lastLoginMobile = info.mobile
            
            self.dosomeSettingBeforeEnterHome()
            return
        }
        
        self.strongBindPhone()
    }
    
    //MARK: 第一次微信登录强绑手机号
    func strongBindPhone() {
        let vc = FEBindingPhoneViewController()
        vc.bindPhoneHandler = {[weak self] (bindPhone) in
            // 保存默认密码
            UserDefaults.Common.firstWXLoginPasswd = "red123456"
            // 缓存绑定的手机号
            UserDefaults.Common.lastLoginMobile = bindPhone
            
            // 走这里说明绑定成功
            guard let self = self else {
                return
            }
            
            self.dosomeSettingBeforeEnterHome()
        }
        self.push(to: vc)
    }
}


//MARK: QMUITextFieldDelegate
extension FELoginDetailViewController: QMUITextFieldDelegate {
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {

    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
}

extension FELoginDetailViewController {
    // 单击view 结束编辑
    @objc func tapHandle() {
        self.view.endEditing(true)
    }
    
    //MARK: 点击微信登录
    @objc func handlerWechat() {
        if WXApi.isWXAppSupport() == false {
            QMUITips.show(withText: "微信版本不支持", in: self.view)
            return
        }
        
        // 判断是否安装微信
        guard WXApi.isWXAppInstalled() else {
            QMUITips.show(withText: "微信尚未安装", in: self.view)
            return
        }
        
        let resAuth = SendAuthReq()
        resAuth.state = "wx_oauth_authorization_state"
        resAuth.scope = "snsapi_userinfo"
//        WXApi.send(resAuth, completion: nil)
        WXApi.sendAuthReq(resAuth, viewController: self, delegate: nil) { (flag) in
            // flag == true, 微信已经收到权限请求
            dprint("auth flag = \(flag)")
        }
    }
    
    //MARK: 点击联系客服
    @objc func handlerCustomer() {
        ContactCustomer.startContact(currentvc: self)
    }
    
    //MARK: 切换登录方式
    @objc func handlerExchangeLoginType(_ btn: QMUIButton) {
        btn.isSelected = !btn.isSelected
        
//        var left = -20 - passwordTextField.width
//        var right = contentView.width + 20
//
//        if btn.isSelected {
//            // 验证码登录
//            left = -20 - passwordTextField.width
//            right = 20
//        } else {
//            // 密码登录
//            left = 20
//            right = contentView.width + 20
//        }
        
        UIView.animate(withDuration: 0.3, animations: {
            if btn.isSelected {
                // 验证码登录
                self.smsTextField.isHidden  = false
                self.passwordTextField.isHidden = true
                self.passwordTextField.right = -20
            } else {
                // 密码登录
                self.smsTextField.isHidden  = true
                self.passwordTextField.isHidden = false
                self.passwordTextField.right = 20
            }
        }) { (flag) in
            if self.passwordTextField.isFirstResponder {
                self.passwordTextField.resignFirstResponder()
                self.smsTextField.becomeFirstResponder()
            }
            else if self.smsTextField.isFirstResponder {
                self.smsTextField.resignFirstResponder()
                self.passwordTextField.becomeFirstResponder()
            }
        }
    }
    
    //MARK: 立即注册
    @objc func handlerRegister(_ btn: QMUIButton) {
        let data = UserCenter.default.lbanner
        guard data.mobile_register_switch == "1" else {
            handlerWechat()
            return
        }
        
        let vc = FERegisterViewController()
        self.push(to: vc)
    }
    
    //MARK: 找回密码
    @objc func handlerFindPasswd(_ btn: QMUIButton) {
        let vc = FERetrievePasswordViewController()
        self.push(to: vc)
    }
    
    //MARK: 点击登录按钮
    @objc func handleLoginBtn(sender _:Any) {
        self.view.endEditing(true)
        
        // test account 123456789 123456
        var parate = LoginParam()
        
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        
        if exchangeButton.isSelected {
            // 验证码登录
            parate.type = .sms
            parate.code = phone
            guard let sms = smsTextField.text, sms.count == 6 else {
                QMUITips.show(withText: "验证码不合法", in: self.view)
                return
            }
            parate.verify_code = sms
        } else {
            // 密码登录
            parate.type = .account
            parate.code = phone
            guard let pwd = passwordTextField.text, pwd.count >= 6 else {
                QMUITips.show(withText: "密码不合法", in: self.view)
                return
            }
            parate.password = pwd
        }
        
        let loagingView = QMUITips.showLoading(in: self.view)
        UserCenter.default.login(parame:parate) { (isSuccess,message,info) in
            loagingView.hide(animated: true)
            if isSuccess {
                // 手机登录
                UserDefaults.Common.lastLoginMobile = self.phoneTextField.text
                
                self.dosomeSettingBeforeEnterHome()
            } else {
                QMUITips.show(withText: "\(message)", in: self.view)
            }
        }
    }
    
    // 点击用户申诉
    @objc func handlerUserAppeal() {
        let vc = FEUserAppealViewController()
        self.push(to: vc)
    }
}



//MARK: 按钮事件
extension FELoginDetailViewController {
    //MARK: 获取验证码
    @objc func getSmsCode(_ btn: QMUIButton) {
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        
        HttpClient.default.GetSMSCode(phone: phoneTextField.text!) { (status, msg) in
            if status {
                self.smsButton.isEnabled = false
                self.smsButton.backgroundColor = .lightGray
                self.setTimer()
            }
            QMUITips.show(withText: msg, in: self.view)
        }
    }
}

//MARK: 定时器
extension FELoginDetailViewController {
    func setTimer() {
        self.stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(dowithTimer(_:)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        if let t = timer {
            t.invalidate()
            timer = nil
        }
    }
    
    @objc func dowithTimer(_ t: Timer) {
        count -= 1
        if count == 0 {
            smsButton.isEnabled = true
            smsButton.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
            self.stopTimer()
        } else {
            smsButton.setTitle("重新获取(\(count))", for: .disabled)
        }
    }
}

