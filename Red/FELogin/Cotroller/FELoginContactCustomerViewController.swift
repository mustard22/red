//
//  FELoginContactCustomerViewController.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//

/// 联系客服界面
class FELoginContactCustomerViewController: FEBaseWebViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "客服"
        self.url = UserCenter.default.lbanner.customer_link
    }
}
