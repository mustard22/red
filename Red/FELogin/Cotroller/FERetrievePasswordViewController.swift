//
//  FERetrievePasswordViewController.swift
//  Red
//
//  Created by MAC on 2019/10/25.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
import SnapKit

/// 找回密码
class FERetrievePasswordViewController: FEBaseViewController {
    private var timer: Timer?
    private var count = 60
    
    let parameter = RegistParam()
    
    private lazy var backTable: UITableView = {
        let v = UITableView()
        v.separatorStyle = .none
        v.frame = view.bounds
        v.backgroundColor = view.backgroundColor
        view.addSubview(v)
        return v
    }()
    
    private lazy var contentView: UIView = {
        let v = UIView()
        backTable.tableHeaderView = v
        return v
    }()
    
    lazy var phoneTextField:UITextField = {
        let v = makeStyleTF(image: "iphone", placeholder: "请输入手机号")
        v.keyboardType = .numberPad
        v.maximumTextLength = 11
        contentView.addSubview(v)
        return v
    }()
    
    lazy var smsButton: QMUIButton = {
        let b = QMUIButton(type: .custom)
        b.size = CGSize(width: 80*kScreenScale, height: 25*kScreenScale)
        b.setTitle("获取验证码", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        b.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
        b.addTarget(self, action: #selector(handleSms(sender:)), for: .touchUpInside)
        b.layer.cornerRadius = 5.0
        return b
    }()
    
    
    lazy var smsTextField:UITextField = {
        let v = makeStyleTF(image: "safety", placeholder: "请输入验证码", rightBtn: true)
        v.keyboardType = .numberPad
        v.maximumTextLength = 6
        contentView.addSubview(v)
        return v
    }()
    
    
    lazy var passwordTextField:UITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请输入密码(字母和数字组合)")
        v.isSecureTextEntry = true
        v.autocapitalizationType = .none // 禁止默认首字母大写
        contentView.addSubview(v)
        return v
    }()
    
    lazy var againPasswordTextField:UITextField = {
        let v = makeStyleTF(image: "password", placeholder: "请确认密码")
        v.isSecureTextEntry = true
        v.autocapitalizationType = .none // 禁止默认首字母大写
        v.returnKeyType = .done
        contentView.addSubview(v)
        return v
    }()
    
    lazy var findPasswdBtn: QMUIButton = {
        let v = QMUIButton()
        v.backgroundColor = Color.red
        v.setTitleColor(.white, for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.setTitle("找回密码", for: .normal)
        v.addTarget(self, action: #selector(handleFindPasswd(sender:)), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    
    @discardableResult
    private func makeStyleTF(_ textField: QMUITextField? = nil, image: String, placeholder: String, rightBtn: Bool? = false) -> QMUITextField {
        let imageView = UIImageView()
        imageView.image = UIImage(named: image)
        imageView.size = CGSize(width: 20, height: 20)
        
        let textfield = textField ?? QMUITextField(placeholder, font: UIFont.systemFont(ofSize: 15), color: Color.text)
        textfield.leftView = imageView
        textfield.leftViewMode = .always
        
        if rightBtn! {
            let view = UIView()
            view.size = self.smsButton.size
            view.addSubview(self.smsButton)
            textfield.rightView = view
            textfield.rightViewMode = .always
        }
        
        textfield.font = UIFont.systemFont(ofSize: 15)
        textfield.placeholder = placeholder
        
        textfield.returnKeyType = .next
        textfield.delegate = self
        textfield.qmui_borderPosition = .bottom
        textfield.qmui_borderColor = .darkGray
        textfield.addTarget(self, action: #selector(handleEditChangedEvent(sender:)), for: .editingChanged)
        return textfield
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "找回密码"
        view.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
        
        createUI()
        
        phoneTextField.becomeFirstResponder()
    }
    
    func createUI() {
        
        backTable.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        contentView.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        phoneTextField.snp.makeConstraints { (make) in
            make.top.equalTo(40)
            make.left.equalTo(15)
            make.right.equalTo(contentView.snp.right).offset(-15)
            make.height.equalTo(51)
        }
        
        smsTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneTextField.snp.bottom)
            make.left.equalTo(phoneTextField.snp.left)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(smsTextField.snp.bottom)
            make.left.equalTo(phoneTextField.snp.left)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        againPasswordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp.bottom)
            make.left.equalTo(phoneTextField.snp.left)
            make.size.equalTo(phoneTextField.snp.size)
        }
        
        
        findPasswdBtn.snp.makeConstraints { (make) in
            make.top.equalTo(againPasswordTextField.snp.bottom).offset(40)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.left.equalTo(20)
            make.height.equalTo(40)
        }
        
    }
    
    
    @objc func handleSms(sender _:Any) {
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        QMUITips.show(withText: "获取成功", in: self.view)
        HttpClient.default.GetSMSCode(phone: phoneTextField.text!) { (isSuccess, message) in
            QMUITips.show(withText: message)
            if isSuccess {
                self.smsButton.isEnabled = false
                self.smsButton.backgroundColor = .lightGray
                self.setTimer()
            }
        }
    }
    
    @objc func handleFindPasswd(sender _:Any) {
        self.view.endEditing(true)
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的手机号", in: self.view)
            return
        }
        guard let sms = smsTextField.text, sms.count == 6 else {
            QMUITips.show(withText: "验证码有误", in: self.view)
            return
        }
        guard let pwd = passwordTextField.text, let againpwd = againPasswordTextField.text, pwd.count>=6, againpwd.count>=6, pwd == againpwd else {
            QMUITips.show(withText: "密码输入不合法", in: self.view)
            return
        }
        
        var param = FindPasswdParam()
        param.mobile = phone
        param.verify_code = sms
        param.password = pwd
        param.sure_pwd = againpwd
        
        UserCenter.default.findPasswd(param: param, { (status, msg) in
            if status {
                QMUITips.show(withText: "密码修改成功", in: self.view)
                self.close()
            } else {
                QMUITips.show(withText: "\(msg)", in: self.view)
            }
        })
    }
    
}

extension FERetrievePasswordViewController: QMUITextFieldDelegate {
    
    @objc private func handleEditChangedEvent(sender textfield: QMUITextField) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.qmui_borderColor = Color.red
        textField.tintColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if passwordTextField.isFirstResponder {
            passwordTextField.resignFirstResponder()
            againPasswordTextField.becomeFirstResponder()
        }
        else if againPasswordTextField.isFirstResponder {
            againPasswordTextField.resignFirstResponder()
            handleFindPasswd(sender: findPasswdBtn)
        }
        return true
    }
}

//MARK: 定时器
extension FERetrievePasswordViewController {
    func setTimer() {
        self.stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(dowithTimer(_:)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        if let t = timer {
            t.invalidate()
            timer = nil
        }
    }
    
    @objc func dowithTimer(_ t: Timer) {
        count -= 1
        if count == 0 {
            smsButton.isEnabled = true
            smsButton.backgroundColor = UIColor(red: 244/255.0, green: 52/255.0, blue: 81/255.0, alpha: 1.0)
            self.stopTimer()
        } else {
            smsButton.setTitle("重新获取(\(count))", for: .disabled)
        }
    }
}

