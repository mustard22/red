//
//  FEUserAppealViewController.swift
//  Red
//
//  Created by MAC on 2020/1/3.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 申诉界面
class FEUserAppealViewController: FEBaseViewController {
    private lazy var tap: UITapGestureRecognizer = {
        let g = UITapGestureRecognizer(target: self, action: #selector(singleTap))
        return g
    }()
    
    private lazy var scroll: UIScrollView = {
        let v = UIScrollView()
        v.frame = view.bounds
        v.alwaysBounceVertical = true
        v.delegate = self
        view.addSubview(v)
        
        v.addGestureRecognizer(self.tap)
        return v
    }()
    
    private lazy var textView: QMUITextView = {
        let v = QMUITextView(frame: .zero)
        v.placeholder = "请输入申诉理由(100字以内)"
        v.placeholderColor = .gray
        v.maximumTextLength = 100
        v.font = UIFont.systemFont(ofSize: 15)
        v.delegate = self
        scroll.addSubview(v)
        return v
    }()
    
    private lazy var phoneTextField: QMUITextField = {
        let v = QMUITextField("请输入账号/手机号", font: UIFont.systemFont(ofSize: 15), color: UIColor.darkGray)
        v.delegate = self
        v.qmui_borderPosition = .bottom
        v.qmui_borderColor = .darkGray
        v.maximumTextLength = 11
        v.keyboardType = .numbersAndPunctuation
        v.returnKeyType = .next
        scroll.addSubview(v)
        return v
    }()
    
    private lazy var emailTextField: QMUITextField = {
        let v = QMUITextField("请输入邮箱", font: UIFont.systemFont(ofSize: 15), color: UIColor.darkGray)
        v.returnKeyType = .send
        v.keyboardType = .URL
        v.delegate = self
        v.qmui_borderPosition = .bottom
        v.qmui_borderColor = .darkGray
        scroll.addSubview(v)
        return v
    }()
    
    private lazy var submitButton: QMUIButton = {
        let v = QMUIButton("提交", fontSize: 16)
        v.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
        v.showsTouchWhenHighlighted = true
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        
        v.layer.cornerRadius = 5.0
        scroll.addSubview(v)
        return v
    }()
    
    deinit {
        textView.delegate = nil
        phoneTextField.delegate = nil
        emailTextField.delegate = nil
        scroll.delegate = nil
        scroll.removeGestureRecognizer(tap)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "账号申诉"
        textView.becomeFirstResponder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        createUI()
    }
}

//MARK: - keyboard notifications
extension FEUserAppealViewController {
    @objc fileprivate func keyboardWillShow(_ not:NSNotification) {
        guard let info = not.userInfo as NSDictionary?,
            let value = info.object(forKey: "UIKeyboardFrameEndUserInfoKey") as! NSValue? else {
            return
        }
        
        let keyboardRect = value.cgRectValue
        let rect = submitButton.convert(submitButton.bounds, to: self.view)
        let offset = rect.origin.y + rect.size.height - (self.view.bottom - keyboardRect.size.height)
        if offset > 0 {
            self.scroll.setContentOffset(CGPoint(x: 0, y: -kNaviBarHeight+offset), animated: true)
        }
    }
    
    @objc fileprivate func keyboardWillHide(_ not:NSNotification) {
        self.scroll.setContentOffset(CGPoint(x: 0, y: -kNaviBarHeight), animated: true)
    }
}

//MARK: - some methods(gesture, button action, set UI)
extension FEUserAppealViewController {
    // dowith tap gesture
    @objc private func singleTap() {
        view.endEditing(true)
    }
    
    @objc private func submitAction() {
        view.endEditing(true)
        
        guard let reason = textView.text, reason.count >= 5 else {
            QMUITips.show(withText: "申诉理由长度至少为5", in: self.view)
            return
        }
        
        guard let phone = phoneTextField.text, FEVerify.phoneNumber(phone) else {
            QMUITips.show(withText: "请输入正确的账号/手机号", in: self.view)
            return
        }
        
        guard let email = emailTextField.text, FEVerify.email(email) else {
            QMUITips.show(withText: "请输入正确的邮箱地址", in: self.view)
            return
        }
        
        let param = ["content": reason, "mobile": phone, "email": email]
        let load = QMUITips.showLoading("提交中...", in: self.view)
        HttpClient.default.userAppeal(param: param) {(status, msg) in
            load.hide(animated: true)
            if status {
                QMUITips.show(withText: "申诉已提交")
                self.close()
                return
            }
            
            QMUITips.show(withText: "\(msg)", in: self.view)
        }
    }
    
    private func createUI() {
        scroll.snp.makeConstraints { (m) in
            m.left.equalTo(0)
            m.top.equalTo(0)
            m.right.equalTo(view)
            m.bottom.equalTo(view)
        }
        
        textView.snp.makeConstraints { (m) in
            m.left.equalTo(30)
            m.top.equalTo(30)
            m.right.equalTo(view.snp.right).offset(-30)
            m.height.equalTo(120)
        }
        
        phoneTextField.snp.makeConstraints { (m) in
            m.left.equalTo(textView)
            m.right.equalTo(textView)
            m.top.equalTo(textView.snp.bottom).offset(20)
            m.height.equalTo(50)
        }
        
        emailTextField.snp.makeConstraints { (m) in
            m.size.equalTo(phoneTextField)
            m.left.equalTo(phoneTextField)
            m.top.equalTo(phoneTextField.snp.bottom).offset(10)
            m.height.equalTo(50)
        }
        
        submitButton.snp.makeConstraints { (m) in
            m.left.equalTo(emailTextField.snp.left).offset(10)
            m.right.equalTo(emailTextField.snp.right).offset(-10)
            m.top.equalTo(emailTextField.snp.bottom).offset(50)
            m.height.equalTo(44)
        }
    }
}

extension FEUserAppealViewController: UIScrollViewDelegate {
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    internal func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

//MARK: - QMUITextViewDelegate
extension FEUserAppealViewController: QMUITextViewDelegate {
    func textViewShouldReturn(_ textView: QMUITextView!) -> Bool {
        textView.resignFirstResponder()
        phoneTextField.becomeFirstResponder()
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.tintColor = Color.red
        return true
    }
}


//MARK: - QMUITextFieldDelegate
extension FEUserAppealViewController: QMUITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if phoneTextField.isFirstResponder {
            phoneTextField.resignFirstResponder()
            emailTextField.becomeFirstResponder()
        }
        else if emailTextField.isFirstResponder {
            emailTextField.resignFirstResponder()
            
            // submit action
            self.submitAction()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.tintColor = Color.red
        textField.qmui_borderColor = Color.red
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.qmui_borderColor = .darkGray
    }
}
