//
//  FELoginViewController.swift
//  Red
//
//  Created by MAC on 2019/8/8.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import SnapKit
import QMUIKit

class FELoginViewController: FEBaseViewController {
    private lazy var backTable: UITableView = {
        let v = UITableView()
        v.separatorStyle = .none
        v.frame = view.bounds
        v.backgroundColor = view.backgroundColor
        view.addSubview(v)
        return v
    }()
    
    private lazy var contentView: UIView = {
        let v = UIView()
        backTable.tableHeaderView = v
        return v
    }()
    
    private lazy var imageBackView: UIView = {
        let v = UIView()
        v.frame = CGRect(x: 20, y: 20, width: kScreenSize.width-20*2, height: 120)
        v.backgroundColor = .white
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        contentView.addSubview(v)
        return v
    }()
    
    private lazy var customerButton: QMUIButton = {
        let c = QMUIButton()
        c.backgroundColor = Color.red
        c.setTitleColor(.white, for: .normal)
        c.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        c.titleLabel?.numberOfLines = 0
        c.layer.cornerRadius = 5
        c.layer.masksToBounds = true
        c.setTitle("联系客服", for: .normal)
    
        c.addTarget(self, action: #selector(handlerCustomer), for: .touchUpInside)
        contentView.addSubview(c)
        return c
    }()
    
    private lazy var tipsLabel: QMUILabel = {
        let c = QMUILabel()
        c.textAlignment = .center
        c.textColor = UIColor.lightGray
        c.text = "其他登录方式"
        c.font = UIFont.systemFont(ofSize: 13)
        c.backgroundColor = view.backgroundColor
        contentView.addSubview(c)
        return c
    }()
    
    private lazy var sepline: UIImageView = {
        let c = UIImageView()
        c.backgroundColor = UIColor.RGBColor(248, 248, 248)
        contentView.addSubview(c)
        return c
    }()
    
    private lazy var wechatButton: QMUIButton = {
        let c = QMUIButton()
        c.setBackgroundImage(UIImage(named: "weixinLogin"), for: .normal)
        c.addTarget(self, action: #selector(handlerWechat), for: .touchUpInside)
        contentView.addSubview(c)
        return c
    }()
    
    private lazy var loginBtn:QMUIButton = {
        let login = QMUIButton()
        login.backgroundColor = Color.red
        login.setTitleColor(.white, for: .normal)
        login.layer.cornerRadius = 5
        login.layer.masksToBounds = true
        login.setTitle("登录", for: .normal)
        login.addTarget(self, action: #selector(handleLogin(sender:)), for: .touchUpInside)
        contentView.addSubview(login)
        return login
    }()
    private lazy var registeredBtn:QMUIButton = {
        let v = QMUIButton()
        v.backgroundColor = .white
        v.setTitleColor(.black, for: .normal)
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.setTitle("注册", for: .normal)
        v.addTarget(self, action: #selector(handleRegister(sender:)), for: .touchUpInside)
        contentView.addSubview(v)
        return v
    }()
    
    /// 是否正在登录中(获取权限通知有时会发2次 避免此情况)
    fileprivate var isLogining = false
    
    /// 是否允许自动登录（一般只有从launch界面设置为true）
    var isAllowAutoLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        self.title = "注册"
        
        self.registerAllObserver()
        
        self.dorequest()
    }
    
    deinit {
        removeAllObserver()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.createUI()
    }
}

extension FELoginViewController {
    // 显示轮播图
    func refreshUI() {
        let data = UserCenter.default.lbanner
        // 判断是否显示微信登录
        if data.wechat_switch == "0" {
            tipsLabel.isHidden = true
            sepline.isHidden = true
            wechatButton.isHidden = true
        }
        
        // 轮播图图片链接
        let ps = data.child.map {$0.url}
        // 轮播图
        let cyclePicture: FECyclePictureView = FECyclePictureView(frame: CGRect(x: 5, y: 5, width: self.imageBackView.frame.size.width - 10, height: self.imageBackView.frame.size.height-5*2), pictures: ps)
        cyclePicture.direction = .left
        cyclePicture.autoScrollDelay = 5
        cyclePicture.didTapAtIndexHandle = {[weak self] index in
            guard let self = self else {
                return
            }
            // 点击轮播图
            self.handleCyclePictureClicked(index)
        }
        
        self.imageBackView.addSubview(cyclePicture)
    }
    
    func createUI() {
        backTable.snp.makeConstraints { (make) in
            make.size.equalTo(self.view)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        contentView.snp.makeConstraints { (make) in
            make.width.equalTo(backTable)
            make.height.equalTo(view.height - kNaviBarHeight - (kIsIphoneX ? 20 : 0))
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        imageBackView.snp.makeConstraints { (make) in
            make.height.equalTo(120)
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.top.equalTo(20)
        }
        
        // 登录按钮
        loginBtn.snp.makeConstraints { (make) in
            make.top.equalTo(imageBackView.snp.bottom).offset(20)
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.height.equalTo(40)
        }
        
        // 注册按钮
        registeredBtn.snp.makeConstraints { (make) in
            make.top.equalTo(loginBtn.snp.bottom).offset(20)
            make.left.equalTo(20)
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.height.equalTo(40)
        }
        
        customerButton.snp.makeConstraints { (make) in
            make.top.equalTo(registeredBtn.snp.bottom).offset(30)
            make.right.equalTo(view.snp.right).offset(-1)
            make.size.equalTo(CGSize(width: 30, height: 90))
        }
        
        sepline.snp.makeConstraints { (make) in
            make.height.equalTo(1.0)
            make.left.equalTo(20)
            make.right.equalTo(view.snp.right).offset(-20)
            make.top.equalTo(customerButton.snp.bottom).offset(50)
        }
        
        tipsLabel.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 120, height: 21))
            make.center.equalTo(sepline.snp.center)
        }
        
        wechatButton.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.top.equalTo(tipsLabel.snp.bottom).offset(20)
            make.centerX.equalTo(sepline.snp.centerX)
        }
    }
    
}

//MARK: 网络请求
extension FELoginViewController {
    private func dorequest() {
        let load = QMUITips.showLoading(in: self.view)
        self.loadLoginBanner {
            self.startAutoLogin {
                load.hide(animated: true)
            }
        }
    }
    
    func loadLoginBanner(_ handler: @escaping (()->Void)) {
        UserCenter.default.getCyclePicture {(status, text) in
            
            self.refreshUI()
            
            handler()
        }
    }
    
    //MARK: 自动登录
    func startAutoLogin(_ handler: @escaping (()->Void)) {
        if !isAllowAutoLogin {
            handler()
            return
        }
        
        self.isAllowAutoLogin = false
        
        // 检测token数据
        guard let tdata = HttpClient.default.tokenData, tdata.1.count > 0, let _ = UserDefaults.Common.lastLoginMobile else {
            handler()
            return
        }
        
        UserCenter.default.autoLogin { (isSuccess, message) in
            
            handler()
            
            guard isSuccess else {
                QMUITips.show(withText: "登录过期，请重新登录", in: self.view)
                return
            }
            
            guard User.default.mobile.count > 1 else {
                // 微信第一次登录后没有绑定手机号
                QMUITips.show(withText: "您尚未绑定手机号", in: self.view)
                self.strongBindPhone()
                return
            }
            
            // token验证成功 进入首页
            self.dosomeSettingBeforeEnterHome()
        }
    }
}

//MARK: 注册观察
extension FELoginViewController {
    func registerAllObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(checkWeixinAuth(_:)), name: .checkWeixinAuth, object: nil)
    }
    
    func removeAllObserver() {
        NotificationCenter.default.removeObserver(self, name: .checkWeixinAuth, object: nil)
    }
    
    @objc func checkWeixinAuth(_ n: Notification) {
        UserDefaults.Common.lastLoginMobile = nil
        
        let res = n.userInfo![NSNotification.Name.checkWeixinAuth] as! SendAuthResp
        
        guard let code = res.code, code.count > 0 else {
            QMUITips.show(withText: "授权失败", in: self.view)
            return
        }
        
        // 避免重复登录
        guard isLogining == false else {
            return
        }

        isLogining = true
        
        // weixin login
        var param = LoginParam()
        param.type = .wechat
        param.code = code
        if let invite_code = UserDefaults.Common.firstInstallInviteCode {
            param.invite_code = invite_code
        }
        UserCenter.default.login(parame:param) {[weak self] (isSuccess,message, info) in
            guard let self = self else {
                QMUITips.show(withText: "程序异常")
                return
            }
            guard let login = info else {
                QMUITips.show(withText: "\(message)", in: self.view)
                return
            }
            if param.invite_code != nil {
                // 移除第一次安装缓存的邀请码
                UserDefaults.Common.firstInstallInviteCode = nil
            }
            
            self.checkBindPhone(login)
        }
    }
    
    //MARK: 进首页前的设置
    func dosomeSettingBeforeEnterHome() {
        // 记录用户登录次数
        UserDefaults.Current.userLoginTime = UserDefaults.Current.userLoginTime + 1
        
        // 关闭登录界面 进入首页
        self.close()
        
        // 登录成功后初始化socket
        FEWebSocketManager.initialWebSocket()
        
        // socket 消息管理
        MMKVManager.initial(User.default.user_id)
        
        // 切换root vc
        UIApplication.shared.switchRootViewController(FETabBarController())
    }
    
    // 检测是否绑定手机号
    func checkBindPhone(_ info: Login) {
        if info.mobile.count > 1 {
            UserDefaults.Common.lastLoginMobile = info.mobile
            
            self.dosomeSettingBeforeEnterHome()
            return
        }
        
        self.strongBindPhone()
    }
    
    //MARK: 第一次微信登录强绑手机号
    func strongBindPhone() {
        let vc = FEBindingPhoneViewController()
        vc.bindPhoneHandler = {[weak self] (bindPhone) in
            // 走这里说明绑定成功
            // 保存默认密码
            UserDefaults.Common.firstWXLoginPasswd = "red123456"
            // 缓存绑定的手机号
            UserDefaults.Common.lastLoginMobile = bindPhone
            
            guard let self = self else {
                return
            }
            
            self.dosomeSettingBeforeEnterHome()
        }
        self.push(to: vc)
    }
}

//MARK: - Click Actions
extension FELoginViewController {
    //MARK: 点击微信登录
    @objc func handlerWechat() {
        // 判断是否安装微信
        guard WXApi.isWXAppInstalled() else {
            QMUITips.show(withText: "微信尚未安装", in: self.view)
            return
        }
        
        guard WXApi.isWXAppSupport() else {
            QMUITips.show(withText: "微信版本不支持", in: self.view)
            return
        }
        
        let resAuth = SendAuthReq()
        resAuth.state = "wx_oauth_authorization_state"
        resAuth.scope = "snsapi_userinfo"
//        WXApi.send(resAuth, completion: nil)
        WXApi.sendAuthReq(resAuth, viewController: self, delegate: nil) { (flag) in
            // flag == true, 微信已经收到权限请求
            dprint("auth flag = \(flag)")
        }
    }
    
    // 点击联系客服
    @objc func handlerCustomer() {
        ContactCustomer.startContact(currentvc: self)
    }
    
    // 登录事件处理
    @objc func handleLogin(sender _: Any) {
        
        let vc = FELoginDetailViewController()
        self.push(to: vc)
    }
    
    // 注册事件处理
    @objc func handleRegister(sender _:Any) {
        let data = UserCenter.default.lbanner
        guard data.mobile_register_switch == "1" else {
            handlerWechat()
            return
        }
        
        let vc = FERegisterViewController()
        self.push(to: vc)
    }
    
    // 点击轮播图事件
    func handleCyclePictureClicked(_ index: Int) {
        let vc = FELoginCyclePictureDetailViewController()
        vc.item = UserCenter.default.lbanner.child[index]
        self.push(to: vc)
    }
}
