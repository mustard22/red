//
//  FELoginCyclePictureDetailViewController.swift
//  Red
//
//  Created by MAC on 2019/10/22.
//  Copyright © 2019 MAC. All rights reserved.
//


/// 登录界面轮播图详情界面
class FELoginCyclePictureDetailViewController: FEBaseWebViewController {
    public var item: LoginChildBanner?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.item?.title
        self.url = item?.link_url ?? ""
    }
}
