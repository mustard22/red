//
//  LoginGetMoneyLayerView.swift
//  Red
//
//  Created by MAC on 2019/12/11.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 新用户首次登录 金额 奖励领取提醒浮层
class LoginGetMoneyLayerView: UIView {
    private let exitButtonSize: CGFloat = 50
    
    lazy var backButton: QMUIButton = {
        let v = QMUIButton()
        v.addTarget(self, action: #selector(exitAction(_:)), for: .touchUpInside)
        self.addSubview(v)
        return v
    }()
    
    lazy var exitButton: QMUIButton = {
        let v = QMUIButton()
        v.titleLabel!.font = UIFont.boldSystemFont(ofSize: 30)
        v.setTitle("X", for: .normal)
        v.setTitleColor(.white, for: .normal)
        v.showsTouchWhenHighlighted = true
        v.bounds = CGRect(x: 0, y: 0, width: exitButtonSize, height: exitButtonSize)
        v.addTarget(self, action: #selector(exitAction(_:)), for: .touchUpInside)
        
        v.layer.cornerRadius = exitButtonSize/2
        v.layer.borderColor = UIColor.white.cgColor
        v.layer.borderWidth = 2
        v.layer.masksToBounds = true
        backButton.addSubview(v)
        return v
    }()
    
    lazy var contentBack: UIImageView = {
        let v = UIImageView()
        v.image = UIImage(named: "ic_money_newuser")
        v.isUserInteractionEnabled = true
        backButton.addSubview(v)
        return v
    }()
    
    lazy var tips: QMUILabel = {
        let v = QMUILabel()
        v.text = "恭喜获得新用户红包"
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 18)
        v.textColor = Color.red
        contentBack.addSubview(v)
        return v
    }()

    lazy var money: QMUILabel = {
        let v = QMUILabel()
        v.text = "0.0"
        v.textAlignment = .center
        v.textColor = Color.red
        v.font = UIFont.boldSystemFont(ofSize: 36)
        contentBack.addSubview(v)
        return v
    }()
    
    lazy var detail: QMUIButton = {
        let v = QMUIButton("立即领取",fontSize: 18)
        v.size = CGSize(width: 150*kScreenScale, height: 30*kScreenScale)
        v.setTitleColor(Color.red, for: .normal)
        v.isUserInteractionEnabled = true
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        v.layer.masksToBounds = true
        v.layer.cornerRadius = 15*kScreenScale
        v.layer.backgroundColor = UIColor.RGBColor(252, 234, 198).cgColor
        contentBack.addSubview(v)
        return v
    }()
    
    // 点击立即领取按钮
    var getMoneyHandler: ((_ isObtain: Bool)->Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.loadUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor(white: 0, alpha: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LoginGetMoneyLayerView {
    @objc func buttonAction(_ btn: QMUIButton) {
        hide()
        getMoneyHandler?(true)
    }
    
    @objc func exitAction(_ btn: QMUIButton) {
        hide()
        getMoneyHandler?(false)
    }
}


extension LoginGetMoneyLayerView {
    
    public func show(_ inView: UIView) {
        self.center = inView.center
        inView.addSubview(self)
        
        self.exitButton.isHidden = true
        self.contentBack.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.3, animations: {
            self.exitButton.isHidden = false
            self.backgroundColor = UIColor(white: 0, alpha: 0.3)
            self.contentBack.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (fin) in
            self.contentBack.transform = CGAffineTransform.identity
        }
    }
    
    public func show() {
        let win = UIApplication.shared.keyWindow
        win?.addSubview(self)
        
        self.exitButton.isHidden = true
        self.contentBack.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.3, animations: {
            self.exitButton.isHidden = false
            self.backgroundColor = UIColor(white: 0, alpha: 0.3)
            self.contentBack.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (fin) in
            self.contentBack.transform = CGAffineTransform.identity
        }
    }
    
    public func hide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.exitButton.isHidden = true
            self.contentBack.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            self.backgroundColor = UIColor(white: 0, alpha: 0)
        }) { (fin) in
            self.removeFromSuperview()
        }
    }
    
    func loadUI() {
        backButton.snp.makeConstraints { (make) in
            make.size.equalTo(self.snp.size)
            make.center.equalTo(self.snp.center)
        }
        contentBack.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 375*kScreenScale, height: 440*kScreenScale))
            make.center.equalTo(backButton.snp.center)
        }
        tips.snp.makeConstraints { (make) in
            make.top.equalTo(contentBack.snp.top).offset(120*kScreenScale)
            make.left.equalTo(contentBack)
            make.right.equalTo(contentBack)
            make.height.equalTo(50)
        }
        money.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.left.equalTo(0)
            make.right.equalTo(contentBack)
            make.top.equalTo(tips.snp.bottom).offset(20*kScreenScale)
        }
        detail.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 150*kScreenScale, height: 30*kScreenScale))
            make.bottom.equalTo(contentBack.snp.bottom).offset(-(100*kScreenScale))
            make.centerX.equalTo(contentBack)
        }
        
        exitButton.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: exitButtonSize, height: exitButtonSize))
            make.top.equalTo(contentBack.snp.bottom)
            make.centerX.equalTo(contentBack)
        }
    }
}
