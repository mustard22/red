//
//  FEFriendListCell.swift
//  Red
//
//  Created by MAC on 2019/12/26.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

//MARK: 好友列表cell
class FEFriendListCell: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 16)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var button: QMUIButton = {
        let v = QMUIButton()
        v.highlightedBackgroundColor = UIColor(white: 0, alpha: 0.3)
        v.showsTouchWhenHighlighted = true
        v.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.contentView.addSubview(v)
        return v
    }()
    lazy var buttonImage: UIImageView = {
        let v = UIImageView(image: UIImage(named: "ic_friend_remove"))
        button.addSubview(v)
        return v
    }()
    
    @objc private func buttonAction() {
        self.buttonActionHandler?()
    }
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
    
    var buttonActionHandler: (() -> Void)?
}

extension FEFriendListCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }
        
        button.snp.makeConstraints { (make) in
            make.right.equalTo(contentView).offset(-10)
            make.width.height.equalTo(40)
            make.centerY.equalTo(contentView)
        }
        
        buttonImage.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
            make.center.equalTo(button)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.right.equalTo(button.snp.left).offset(-5)
            make.height.equalTo(40)
            make.centerY.equalTo(contentView)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}
