//
//  FEFriendMessageListCell.swift
//  Red
//
//  Created by MAC on 2019/11/2.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit
/// 群组成员列表cell
class FEFriendMessageListCell: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var msgLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        v.textColor = .gray
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var redDot: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = Color.red
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var longPress: UILongPressGestureRecognizer = {
        let ges = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction))
        ges.allowableMovement = 5
        return ges
    }()
    
    var longPressHandler: ((_ c: FEFriendMessageListCell)->Void)?
}

extension FEFriendMessageListCell {
    @objc private func longPressAction() {
        longPressHandler?(self)
    }
    
    public func update(_ item: ChatMessage, _ showLine: Bool? = true) {
        if item.from_id == User.default.user_id {
            // 自己发送
            avatarImageView.setImage(with: URL(string: item.to_avatar))
            nameLabel.text = item.to_nickname
        } else {
            // 好友发送的
            avatarImageView.setImage(with: URL(string: item.from_avatar))
            nameLabel.text = item.from_nickname
        }
        
        if item.type != "2" {
            msgLabel.text = item.content
        } else {
            msgLabel.text = "[转账]"
        }
        
        sepline.isHidden = !showLine!
        
    }
    
    public func update2(_ item: Conversation, _ showLine: Bool? = true) {
        if let img = item.cavatar, item.cid.count>0 {
            avatarImageView.setImage(with: URL(string: img), placeHolder: UIImage(named: "ic_user_default"))
        }
        nameLabel.text = item.cname ?? ""
        
        guard let msg = item.msgs.last else {
            msgLabel.text = ""
            return
        }
        
        switch msg.type {
        case MessageType.friendTransfer.rawValue:
            msgLabel.text = "[转账]"
        case MessageType.ownerFriendInvite.rawValue:
            msgLabel.text = "[邀请入群]"
        default:
            if msg.body.content.hasPrefix("img:::http") {
                msgLabel.text = "[图片]"
            } else {
                msgLabel.text = msg.body.content
            }
        }
        
        sepline.isHidden = !showLine!
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.addGestureRecognizer(longPress)
        
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.centerY.equalTo(contentView.snp.centerY)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.height.equalTo(20)
            make.top.equalTo(avatarImageView.snp.top)
        }
        msgLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
        
        redDot.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-50)
            make.width.height.equalTo(10)
            make.centerY.equalTo(contentView.snp.centerY)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}

