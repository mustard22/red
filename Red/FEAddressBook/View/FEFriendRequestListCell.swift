//
//  FEFriendRequestListCell.swift
//  Red
//
//  Created by MAC on 2019/11/2.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 好友请求列表cell
class FEFriendRequestListCell: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var idLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var msgLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var agreeBtn: QMUIButton = {
        let v = QMUIButton("同意", fontSize: 18)
        v.setTitleColor(UIColor.RGBColor(37, 155, 78), for: .normal)
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var rejectBtn: QMUIButton = {
        let v = QMUIButton("拒绝", fontSize: 18)
        v.setTitleColor(Color.red, for: .normal)
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        v.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
    
    var clickHandler: ((_ btn: QMUIButton)->Void)?
}

extension FEFriendRequestListCell {
    public func update(_ item: FriendRequestListData.RequestItem, _ showLine: Bool? = true) {
        avatarImageView.setImage(with: URL(string: item.friend_avatar))
        nameLabel.text = item.friend_nickname
        idLabel.text = "ID:\(item.friend_id)"
        agreeBtn.isHidden = true
        rejectBtn.isHidden = true
        msgLabel.isHidden = false
        if item.status == "0" {
            agreeBtn.isHidden = false
            rejectBtn.isHidden = false
            msgLabel.isHidden = true
        }
        else if item.status == "1" {
            // 已同意
            msgLabel.text = "已同意"
            msgLabel.textColor = .black
        }
        else if item.status == "2" {
            // 已拒绝
            msgLabel.text = "已拒绝"
            msgLabel.textColor = Color.red
        }
        else {
        }
        sepline.isHidden = !showLine!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.centerY.equalTo(contentView.snp.centerY)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(120)
            make.height.equalTo(20)
            make.top.equalTo(avatarImageView.snp.top)
        }
        idLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
        msgLabel.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.centerY.equalTo(avatarImageView.snp.centerY)
        }
        
        agreeBtn.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 50, height: 30))
            make.centerX.equalTo(msgLabel.snp.centerX)
            make.top.equalTo(5)
        }
        
        rejectBtn.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 50, height: 30))
            make.centerX.equalTo(msgLabel.snp.centerX)
            make.bottom.equalTo(contentView.snp.bottom).offset(-5)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
    
    @objc func buttonAction(_ btn: QMUIButton) {
        guard let handler = clickHandler else {
            return
        }
        handler(btn)
    }
}


