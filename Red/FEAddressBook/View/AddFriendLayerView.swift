//
//  AddFriendLayerView.swift
//  Red
//
//  Created by MAC on 2020/1/6.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 添加好友浮层
class AddFriendLayerView: UIView {
    private lazy var greenDot: UILabel = {
        let dot = UILabel()
        dot.backgroundColor = .green
        dot.bounds = CGRect(x: 0, y: 0, width: 10, height: 10)
        dot.layer.masksToBounds = true
        dot.layer.cornerRadius = 5
        dot.isHidden = true
        return dot
    }()
    private lazy var layerView: UIView = {
        let v = UIView()
        v.layer.backgroundColor = Color.red.cgColor
        v.layer.opacity = 0.95
        v.layer.masksToBounds = true
        v.layer.cornerRadius = 2
        v.frame = CGRect(x: self.width-120, y: kNaviBarHeight + 2.0, width: 120, height: 89)
        self.addSubview(v)
        
        var rect = v.frame
        rect.origin.x = 0
        rect.origin.y = 0
        rect.size.height = 44
        
        let b1 = QMUIButton("添加好友", fontSize: 15)
        b1.frame = rect
        b1.tag = 0
//        b1.backgroundColor = Color.red
        b1.setTitleColor(.white, for: .normal)
        b1.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        v.addSubview(b1)
        
        let line = UIView()
        line.frame = CGRect(x: 5, y: rect.origin.y+rect.size.height, width: rect.size.width-10, height: 1)
        line.backgroundColor = UIColor(white: 1, alpha: 0.8)
        v.addSubview(line)
        
        rect.origin.y += rect.size.height+1
        let b2 = QMUIButton("请求列表", fontSize: 15)
        b2.tag = 1
        b2.frame = rect
//        b2.backgroundColor = Color.red
        b2.setTitleColor(.white, for: .normal)
        b2.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        v.addSubview(b2)
        
        b2.addSubview(greenDot)
        greenDot.right = rect.size.width - 10
        greenDot.top = 10
        return v
    }()
    
    var showDot: Bool {
        get {
            return !greenDot.isHidden
        }
        set (newValue) {
            greenDot.isHidden = !newValue
        }
    }
    
    public var clickHandler: ((_ tag: Int)->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = UIScreen.main.bounds
        layerView.snp.makeConstraints { (make) in
            make.top.equalTo(kNaviBarHeight+2.0)
            make.right.equalTo(self.snp.right).offset(-5)
            make.size.equalTo(CGSize(width: 120, height: 89))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func hide() {
        self.removeFromSuperview()
    }
    
    func show() {
        let win = UIApplication.shared.keyWindow
        self.frame = win!.bounds
        win?.addSubview(self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.hide()
    }
    
    @objc func buttonAction(_ btn: QMUIButton) {
        self.hide()
        if let handler = clickHandler {
            handler(btn.tag)
        }
    }
}
