//
//  FEAddSearchFriendCell.swift
//  Red
//
//  Created by MAC on 2019/11/2.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit
/// 群组成员列表cell
class FEAddSearchFriendCell: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var idLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        v.textColor = .gray
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var addBtn: QMUIButton = {
        let v = QMUIButton("+", fontSize: 40)
        v.setTitleColor(UIColor.RGBColor(37, 155, 78), for: .normal)
        self.contentView.addSubview(v)
        return v
    }()
}

extension FEAddSearchFriendCell {
    public func update(_ item: SearchFriendItem) {
        avatarImageView.setImage(with: URL(string: item.friend_avatar))
        nameLabel.text = item.friend_nickname
        idLabel.text = "ID:\(item.id)"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.centerY.equalTo(contentView.snp.centerY)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.height.equalTo(20)
            make.top.equalTo(avatarImageView.snp.top)
        }
        idLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
        addBtn.snp.makeConstraints { (make) in
            make.width.height.equalTo(40)
            make.right.equalTo(contentView.snp.right).offset(-40)
            make.centerY.equalTo(avatarImageView.snp.centerY)
        }
    }
}

