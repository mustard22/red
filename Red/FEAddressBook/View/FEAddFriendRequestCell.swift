//
//  FEAddFriendRequestCell.swift
//  Red
//
//  Created by MAC on 2019/12/26.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 添加好友请求cell
class FEAddFriendRequestCell: YYTableViewCell {
    lazy var avatarImageView: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.RGBColor(220, 220, 220)
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var nameLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .left
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var idLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 12)
        v.textAlignment = .left
        v.textColor = .gray
        self.contentView.addSubview(v)
        return v
    }()
    
    lazy var msgLabel: QMUILabel = {
        let v = QMUILabel()
        v.font = UIFont.systemFont(ofSize: 15)
        v.textAlignment = .center
        self.contentView.addSubview(v)
        return v
    }()
    

    lazy var sepline: QMUILabel = {
        let v = QMUILabel()
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.contentView.addSubview(v)
        return v
    }()
}

extension FEAddFriendRequestCell {
    public func update(_ item: FriendRequestListData.RequestItem, _ showLine: Bool? = true) {
        avatarImageView.setImage(with: URL(string: item.friend_avatar))
        nameLabel.text = item.friend_nickname
        idLabel.text = "ID:\(item.friend_id)"
        
        switch item.status {
        case "0":
            msgLabel.text = "未处理"
            msgLabel.textColor = .gray
        case "1":
            msgLabel.text = "已同意"
            msgLabel.textColor = .black
        case "2":
            msgLabel.text = "已拒绝"
            msgLabel.textColor = Color.red
        default: break
        }

        sepline.isHidden = !showLine!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.centerY.equalTo(contentView.snp.centerY)
            make.size.equalTo(CGSize(width: 50, height: 50))
        }
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(120)
            make.height.equalTo(20)
            make.top.equalTo(avatarImageView.snp.top)
        }
        idLabel.snp.makeConstraints { (make) in
            make.left.equalTo(avatarImageView.snp.right).offset(10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.bottom.equalTo(avatarImageView.snp.bottom)
        }
        msgLabel.snp.makeConstraints { (make) in
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.centerY.equalTo(avatarImageView.snp.centerY)
        }
        
        sepline.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(contentView.snp.right)
            make.height.equalTo(1.0)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}


