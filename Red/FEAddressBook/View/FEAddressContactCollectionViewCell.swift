//
//  FEAddressContactCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/9.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FEAddressContactCollectionViewCell: UICollectionViewCell {
    lazy var iconImage:UIImageView = {
        let iconImage = UIImageView()
        return iconImage
    }()
    
    lazy var titleLabel:UILabel = {
        let titleLabel = UILabel()
        titleLabel.textColor = .black
        return titleLabel
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.addSubview(titleLabel)
        contentView.addSubview(iconImage)
        contentView.backgroundColor = .white
        iconImage.frame = CGRect(x: 20, y: 5, width: 40, height: 40)
        titleLabel.frame = CGRect(x: 80, y: 15.5, width: 180, height: 19.5)
        
    }
}
