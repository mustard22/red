//
//  FEAddressTitleCollectionViewCell.swift
//  Red
//
//  Created by MAC on 2019/8/9.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

class FEAddressTitleCollectionViewCell: UICollectionViewCell {
    lazy var titleLabel:UILabel = {
       let titleLabel = UILabel()
        titleLabel.textColor = .black
        return titleLabel
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.addSubview(titleLabel)
        contentView.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0)
        titleLabel.frame = CGRect(x: 15, y: 4, width: 104, height: 21)
    }
}
