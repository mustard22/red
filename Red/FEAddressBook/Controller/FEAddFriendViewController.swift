//
//  FEAddFriendViewController.swift
//  Red
//
//  Created by MAC on 2019/10/29.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit


/// 添加好友界面
class FEAddFriendViewController: FEBaseViewController {
    private lazy var searchView: QMUITextField = {
        let v = QMUITextField()
        v.backgroundColor = .white
        v.placeholder = "好友ID/手机号"
        v.keyboardType = .numberPad
        v.maximumTextLength = 10
        v.textAlignment = .center
        v.font = UIFont.systemFont(ofSize: 15)
        v.frame = CGRect(x: 0, y: 0, width: tableView.width, height: 44)
        
        let lback = UIView()
        lback.size = CGSize(width: 50, height: 20)
        let left = UIImageView()
        left.image = UIImage(named: "icon_nav_search")
        left.frame = CGRect(x: 15, y: 0, width: 20, height: 20)
        lback.addSubview(left)
        v.leftView = lback
        
        let rback = UIView()
        rback.size = CGSize(width: 90, height: 30)
        
        let right = QMUIButton()
        right.frame = CGRect(x: 15, y: 0, width: 60, height: 30)
        right.backgroundColor = Color.red
        right.titleLabel!.font = UIFont.systemFont(ofSize: 14)
        right.setTitleColor(.white, for: .normal)
        right.setTitle("搜索", for: .normal)
        right.layer.masksToBounds = true
        right.layer.cornerRadius = 5.0
        right.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        right.showsTouchWhenHighlighted = true
        rback.addSubview(right)
        v.rightView = rback
        
        v.leftViewMode = .always
        v.rightViewMode = .always
        v.qmui_borderColor = UIColor(r: 156, g: 156, b: 156)
        v.qmui_borderWidth = 1.0
        v.delegate = self
        tableView.tableHeaderView = v
        return v
    }()
    
    private lazy var tableView: UITableView = {
        let v = UITableView(frame: self.view.bounds, style: .plain)
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        v.separatorStyle = .none
        v.delegate = self
        v.dataSource = self
        self.view.addSubview(v)
        return v
    }()
    
    /// 已经搜索成功的key
    private var searchKey = ""
    
    /// 搜索结果
    private var datas = [SearchFriendItem]()
    
    /// 添加历史
    private var requestDatas = [FriendRequestListData.RequestItem]()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = UIScreen.main.bounds
        searchView.frame = CGRect(x: 0, y: 0, width: tableView.width, height: 50)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "添加好友"
        
        self.registerAllCells()
        
        loadFriendRequestData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchView.becomeFirstResponder()
    }
}

//MARK: 按钮事件
extension FEAddFriendViewController {
    // click search button
    @objc private func searchAction() {
        searchView.resignFirstResponder()
        
        self.searchFriends(searchView.text ?? "")
    }
    
    // click plus('+') button
    @objc private func addFriendAction(_ btn: UIButton) {
        searchView.resignFirstResponder()
        
        self.addFriend(btn.tag)
    }
}

//MARK: 请求接口
extension FEAddFriendViewController {
    // 搜索好友
    private func searchFriends(_ keyword: String) {
        guard keyword.count > 0 else {
            QMUITips.show(withText: "请输入用户ID或手机号", in: self.view)
            return
        }
        
        // 避免重复搜索一个关键字（减少接口请求）
        guard keyword != self.searchKey else {
            return
        }
        
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.searchFriend(param: keyword) {[weak self] (status, msg, array) in
            load.hide(animated: true)
            guard let self = self else {
                return
            }
            
            if !status {
                QMUITips.show(withText: msg, in: self.view)
                return
            }
            
            // 存储关键字
            self.searchKey = keyword
            
            self.datas.removeAll()
            if let arr = array, arr.count > 0 {
                self.datas = arr
            } else {
                QMUITips.show(withText: "未搜到，换个关键字试试", in: self.view)
            }
            self.tableView.reloadData()
        }
    }
    
    // 添加好友操作
    private func addFriend(_ index: Int) {
        guard index >= 0, index < datas.count else {
            return
        }
        
        let m = datas[index]
        let p = ["friend_id": m.id, "reason": ""]
        
        HttpClient.default.addFriend(param: p) {[weak self] (status, msg, _) in
            QMUITips.show(withText: msg)
            if status {
                self?.close()
            }
        }
    }
    
    // 请求好友数据
    private func loadFriendRequestData() {
        HttpClient.default.friendRequestList(param: "2") { (status, msg, data) in
            guard let data = data, status else {
                return
            }
            
            self.requestDatas = data.data
            self.tableView.reloadData()
        }
    }
}

//MARK: QMUITextFieldDelegate
extension FEAddFriendViewController: QMUITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchKey = ""
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.text != searchKey {
            searchKey = ""
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.tintColor = Color.red
        return true
    }
}

//MARK: table delegate & datasource
extension FEAddFriendViewController: UITableViewDelegate, UITableViewDataSource {
    private func registerAllCells() {
        self.tableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "UITableViewHeaderFooterView")
        self.tableView.register(FEAddFriendRequestCell.self, forCellReuseIdentifier: "FEAddFriendRequestCell")
        self.tableView.register(FEAddSearchFriendCell.self, forCellReuseIdentifier: "FEAddSearchFriendCell")
    }
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        if datas.count > 0 && requestDatas.count > 0 {
            return 2
        }
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return datas.count == 0 ? requestDatas.count : datas.count
        }
        return requestDatas.count
    }
    
    internal func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if datas.count > 0 || requestDatas.count > 0 {
            return 35
        }
        return 0
    }
    
    internal func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "UITableViewHeaderFooterView")!
        header.contentView.backgroundColor = self.tableView.backgroundColor
        return header
    }
    
    internal func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        var title = "搜索结果"
        if datas.count == 0 || section > 0 {
            title = "添加好友历史"
        }
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel!.text = title
        header.textLabel!.font = UIFont.systemFont(ofSize: 14)
        header.textLabel!.textColor = .darkGray
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0, datas.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FEAddSearchFriendCell", for: indexPath) as! FEAddSearchFriendCell
            cell.update(datas[indexPath.row])
            cell.addBtn.tag = cell.tag
            cell.addBtn.addTarget(self, action: #selector(addFriendAction(_:)), for: .touchUpInside)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FEAddFriendRequestCell", for: indexPath) as! FEAddFriendRequestCell
        cell.update(self.requestDatas[indexPath.row])
        return cell
    }
    
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
}
