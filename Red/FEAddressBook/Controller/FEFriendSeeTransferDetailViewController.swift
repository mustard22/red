//
//  FEFriendSeeTransferDetailViewController.swift
//  Red
//
//  Created by MAC on 2019/11/1.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

/// 查看转账详情界面（点击自己给好友的转账）
class FEFriendSeeTransferDetailViewController: FEBaseViewController {
    lazy var table: UITableView = {
        let v = UITableView()
        v.frame = view.bounds
        v.separatorStyle = .none
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        view.addSubview(v)
        return v
    }()
    lazy var content: UIView = {
        let v = UIView()
        table.tableHeaderView = v
        return v
    }()
    
    lazy var type: QMUILabel = {
        let v = QMUILabel()
        v.text = "转账金额"
        v.textAlignment = .center
        v.textColor = UIColor.RGBColor(136, 136, 136)
        v.font = UIFont.systemFont(ofSize: 14)
        content.addSubview(v)
        return v
    }()
    
    
    lazy var money: QMUILabel = {
        let v = QMUILabel()
        v.text = "0.00"
        v.textAlignment = .center
        v.textColor = .black
        v.font = UIFont.systemFont(ofSize: 20)
        content.addSubview(v)
        return v
    }()
    
    lazy var tips: QMUILabel = {
        let v = QMUILabel()
        v.text = "好友转账"
        v.textAlignment = .center
        v.textColor = UIColor.RGBColor(136, 136, 136)
        v.font = UIFont.systemFont(ofSize: 14)
        content.addSubview(v)
        return v
    }()
    
    
    private lazy var submitButton: QMUIButton = {
        let v = QMUIButton("领取转账", fontSize: 18)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
        content.addSubview(v)
        return v
    }()
    
    private lazy var detail: QMUIButton = {
        let v = QMUIButton("零钱明细", fontSize: 15)
        v.addTarget(self, action: #selector(detailButtonAction), for: .touchUpInside)
        content.addSubview(v)
        return v
    }()
    
    
    var msg: Message?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "转账详情"
        
        loadUI()
        
        checkTransferReceiveStatus()
    }
}

extension FEFriendSeeTransferDetailViewController {
    func checkTransferReceiveStatus() {
        guard let msg = self.msg else {
            return
        }
        
        money.text = msg.body.transfer_amount
        submitButton.isEnabled = false

        HttpClient.default.friendTransferReceiveStatus(param: msg.body.id) {[weak self] (status, text, data) in
            guard let self = self else {
                return
            }
            if let item = data {
                var title = ""
                if item.transfer_status == "0" {
                    // 未领取
                    if msg.direction == MessageDirection.receive.rawValue {
                        title = "领取转账"
                        self.submitButton.isEnabled = true
                    } else {
                        title = "好友未领取"
                    }
                    
                } else if item.transfer_status == "1" {
                    // 已领取
                    title = msg.direction == MessageDirection.receive.rawValue ? "已领取" : "好友已领取"
                } else {
                    // 已过期
                    title = "转账已过期"
                }
                self.submitButton.setTitle(title, for: .normal)
            } else {
                QMUITips.show(withText: text, in: self.view)
            }
        }
    }
    
    func loadUI() {
        table.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        content.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        
        type.snp.makeConstraints { (make) in
            make.top.equalTo(50)
            make.size.equalTo(CGSize(width: 150, height: 20))
            make.centerX.equalTo(content.snp.centerX)
        }
        money.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 200, height: 30))
            make.top.equalTo(type.snp.bottom).offset(10)
            make.centerX.equalTo(content.snp.centerX)
        }
        tips.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 120, height: 40))
            make.top.equalTo(money.snp.bottom).offset(20)
            make.centerX.equalTo(content.snp.centerX)
        }
        
        submitButton.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(30)
            make.right.equalTo(content.snp.right).offset(-30)
            make.top.equalTo(tips.snp.bottom).offset(40)
        }
        detail.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(30)
            make.right.equalTo(content.snp.right).offset(-30)
            make.top.equalTo(submitButton.snp.bottom).offset(40)
        }
    }
    
    @objc func detailButtonAction() {
        let vc = FECenterMoneyDetailViewController()
        self.push(to: vc)
    }
    
    @objc func submitButtonAction() {
        HttpClient.default.receiveFriendTransfer(param: msg!.body.id) {[weak self] (status, text, _) in
            guard let self = self else {
                return
            }
            if status {
                self.submitButton.setTitle("已领取", for: .normal)
                self.submitButton.isEnabled = false
            }
            QMUITips.show(withText: text, in: self.view)
        }
    }
}

