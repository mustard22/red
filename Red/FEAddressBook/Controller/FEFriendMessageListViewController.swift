//
//  FEFriendMessageListViewController.swift
//  Red
//
//  Created by MAC on 2019/11/2.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

fileprivate let loadCount = 20

// 好友消息列表
class FEFriendMessageListViewController: YYTableViewController {
    var datas = [Conversation]()
    
    var data: MessageListData?
    lazy var listDatas: [ChatMessage] = {
        return []
    }()
    
    var editStyle = UITableViewCell.EditingStyle.delete
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        
        reloadConversations()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addressBookMessageTipsNotification), name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshRecentChatNotification(n:)), name: SMManager.refreshRecentChatsNotification, object: nil)
        // 管理员上线 下线
        NotificationCenter.default.addObserver(self, selector: #selector(adminOnlineNotification(_:)), name: SMManager.adminOnlineNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adminOfflineNotification(_:)), name: SMManager.adminOfflineNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
        NotificationCenter.default.removeObserver(self, name: SMManager.refreshRecentChatsNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: SMManager.adminOnlineNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: SMManager.adminOfflineNotification, object: nil)
    }
    
    @objc func addressBookMessageTipsNotification() {
        reloadConversations()
    }
    
    @objc func adminOfflineNotification(_ n: Notification) {
        guard let info = n.userInfo, let uid = info["uid"] as? String else {
            return
        }
        
        guard let index = self.datas.firstIndex(where: {$0.cid == uid}) else {
            return
        }
        
        self.datas.remove(at: index)
        self.tableView.reloadData()
    }
    
    @objc func adminOnlineNotification(_ n: Notification) {
        guard let info = n.userInfo, let uid = info["uid"] as? String else {
            return
        }
        
        if let _ = self.datas.firstIndex(where: {$0.cid == uid}) {
            return
        }
        
        guard let recents = SMManager.manager.getRecentFriendChats(), let admin = recents.filter({$0.cid == uid}).first, !admin.msgs.isEmpty else {
            return
        }
        
        self.datas.insert(admin, at: 0)
        self.tableView.reloadData()
    }
    
    @objc func refreshRecentChatNotification(n: Notification) {
        guard let info = n.userInfo,
            let con = info[SMManager.refreshRecentChatsNotification] as? Conversation, !con.msgs.isEmpty else {
            return
        }
        
        datas = datas.filter({$0.cid != con.cid})
        datas.insert(con, at: 0)
        self.tableView.reloadData()
    }
    
    // 获取管理员数据
    private func getAdmins() -> [String] {
        return SMManager.manager.onlineAdmins.map({$0.body.id})
    }
    
    func reloadConversations() {
        var recents: [Conversation] = SMManager.manager.getRecentFriendChats() ?? []
        let admins = self.getAdmins()
        recents = recents.filter { (c) -> Bool in
            if let ctype = c.type, ctype == ConversationType.admin.rawValue, !admins.contains(c.cid) {
                return false
            }
            return true
        }
        
        self.datas.removeAll()
        self.datas = recents.filter({!$0.msgs.isEmpty})
        
        self.tableView.reloadData()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: FEFriendMessageListCell.self)
        tableView.reloadData()
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FEFriendMessageListViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FEFriendMessageListViewController {
    @objc func loadListData() {
        reloadConversations()
        self.mj_head.endRefreshing()
    }
}

extension FEFriendMessageListViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        tableView.allowsMultipleSelection = true
        
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FEFriendMessageListCell.self)
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FEFriendMessageListCell.self, for: indexPath)
        let item = datas[indexPath.row]//listDatas[indexPath.row]
        //TODO: update
        cell.update2(item)
        
        MessageTipsManager.shared.isContains(id: item.cid, .friend) { (t) in
            cell.redDot.isHidden = t.1 > 0 ? false : true
        }

        cell.longPressHandler = {(c) in
            self.dowithLongPressAction(indexPath)
        }
        return cell
    }
    
    //MARK: 长按cell 删除会话缓存
    private func dowithLongPressAction(_ indexPath: IndexPath) {
        if indexPath.row < 0 || indexPath.row >= datas.count {
            return
        }
        
        let asheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let a1 = UIAlertAction(title: "删除", style: .default) { (action) in
            let cid = self.datas[indexPath.row].cid
            MessageTipsManager.shared.removeTips(cid)
            SMManager.manager.deleteConversation(cid: cid, isGroup: false)
            
            self.datas.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
            
            NotificationCenter.default.post(name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
        }
        let cancel = UIAlertAction(title: "取消", style: .cancel)
        
        asheet.addAction(a1)
        asheet.addAction(cancel)
        self.present(asheet, animated: true, completion: nil)
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = datas[indexPath.row]//listDatas[indexPath.row]
        
        var friend = FriendItem()
        friend.friend_id = item.cid
        friend.friend_nickname = item.cname ?? ""
        friend.friend_avatar = item.cavatar ?? ""
        
        var type: ChatViewType = .friend
        if let ctype = item.type {
            switch ctype {
            case ConversationType.customer.rawValue:
                type = .customer
            case ConversationType.admin.rawValue:
                type = .admin
            default: type = .friend
            }
        }
        
        let vc = FEChatFriendViewController(chat_id: friend.friend_id, chat_type: type)
        vc.friend = friend
        self.push(to: vc)
    }
}


