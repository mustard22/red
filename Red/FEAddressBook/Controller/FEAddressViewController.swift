//
//  FEAddressViewController.swift
//  Red
//
//  Created by MAC on 2019/8/9.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit


class FEAddressViewController: FEBaseViewController {
    private let vcs = [FEFriendListViewController(), FEFriendMessageListViewController()]
    private let titles = ["通讯录", "消息"]
    
    lazy var pagevc: FEBasePageViewController = {
        // 默认显示消息列表
        let vc = FEBasePageViewController(start: 1, headerHeight: 44, style: .scroll)
        vc.dataSource = self
        self.addChild(vc)
        self.view.addSubview(vc.view)
        return vc
    }()
    
    // 右上角‘+’按钮
    private lazy var rightTop: QMUIButton = {
        let v = QMUIButton("", image: "")
        v.size = CGSize(width: 60, height: 44)
        v.addTarget(self, action: #selector(addFriendAction), for: .touchUpInside)
        
        let imgv = UIImageView()
        imgv.frame = CGRect(x: 20, y: 12, width: 20, height: 20)
        imgv.image = UIImage(named: "icon_switch_shortcut")
        v.addSubview(imgv)
        
        // 绿点view
        v.addSubview(greenDot)
        greenDot.frame = CGRect(x: imgv.right+5, y: imgv.top-10, width: 10, height: 10)
        return v
    }()
    
    // 好友请求绿点提示(默认隐藏)
    private lazy var greenDot: UILabel = {
        let dot = UILabel()
        dot.backgroundColor = .green
        dot.layer.masksToBounds = true
        dot.layer.cornerRadius = 5
        dot.isHidden = true
        return dot
    }()
    
    // 红点提醒
    private lazy var redDot: UILabel = {
        let v = UILabel()
        v.bounds = CGRect(x: 0, y: 0, width: 10*kScreenScale, height: 10*kScreenScale)
        v.backgroundColor = Color.red
        v.layer.masksToBounds = true
        v.layer.cornerRadius = v.height/2
        
        if let backView = self.pagevc.menuButton(with: 1) {
            v.top = 5.0
            v.right = backView.width/2 + 30
            backView.addSubview(v)
        }
        
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "通讯"
        view.backgroundColor = UIColor.white
        
        self.edgesForExtendedLayout = .init(rawValue: 0)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightTop)
        
        // 注册通知
        self.registerAllNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addressBookMessageTipsNotification()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let rect = view.bounds
        pagevc.view.frame = rect
    }
    
    deinit {
        self.removeAllNotifications()
    }
    
}

//MARK: - click actions
extension FEAddressViewController {
    // 点击右上角按钮
    @objc private func addFriendAction() {
        let layer = AddFriendLayerView(frame: .zero)
        layer.showDot = !self.greenDot.isHidden
        layer.show()
        layer.clickHandler = {[weak self](tag) in
            if tag == 0 {
                // add friend
                self?.addFriend()
            } else {
                // friend requests
                self?.msgList()
            }
        }
    }
    
    // add friend
    private func addFriend() {
        self.push(to: FEAddFriendViewController())
    }
    
    // friend requests
    private func msgList() {
        let vc = FEFriendRequestListViewController()
        self.push(to: vc)
        
        self.greenDot.isHidden = true
    }
}

//MARK: - notifications
extension FEAddressViewController {
    private func registerAllNotifications() {
        // 新消息提醒通知
        NotificationCenter.default.addObserver(self, selector: #selector(addressBookMessageTipsNotification), name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
        // 新的好友请求通知
        NotificationCenter.default.addObserver(self, selector: #selector(friendRequestNotification), name: SMManager.friendRequestNotification, object: nil)
    }
    
    private func removeAllNotifications() {
        NotificationCenter.default.removeObserver(self, name: MessageTipsManager.friendMsgTipsNotificaiton, object: nil)
        NotificationCenter.default.removeObserver(self, name: SMManager.friendRequestNotification, object: nil)
    }
    
    @objc func friendRequestNotification() {
        self.greenDot.isHidden = false
    }
    
    @objc func addressBookMessageTipsNotification() {
        guard let nav = self.navigationController else {
            return
        }
        
        let count = MessageTipsManager.shared.msgCount()
        if count == 0 {
            nav.tabBarItem.badgeValue = nil
            redDot.isHidden = true
        } else {
            redDot.isHidden = false
            nav.tabBarItem.badgeValue = "\(count)"
            nav.tabBarItem.badgeColor = Color.red
        }
    }
}

//MARK: - FEBasePageViewControllerDataSource
extension FEAddressViewController: FEBasePageViewControllerDataSource {
    func numbersOfPage() -> Int {
        return titles.count
    }
    
    func previewController(formPage index: Int) -> UIViewController {
        let vc = self.vcs[index]
        vc.view.tag = index
        return vc
    }
    
    func itemText(index: Int) -> String {
        return self.titles[index]
    }
}
