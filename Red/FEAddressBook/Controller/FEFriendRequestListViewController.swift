//
//  FEFriendRequestListViewController.swift
//  Red
//
//  Created by MAC on 2019/11/2.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

extension Notification.Name {
    /// 好友请求提醒
    static let friendRequestTips = Notification.Name.init("friendRequestTips")
}

// 好友列表
class FEFriendRequestListViewController: YYTableViewController {
    
    var listData: FriendRequestListData?{
        get {
            return nil
        }
        set (newValue) {
            datas.removeAll()
            guard let newdata = newValue else {
                return
            }
            
            datas.append(contentsOf: newdata.data)
            self.tableView.reloadData()
        }
    }
    
    var datas = [FriendRequestListData.RequestItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.title = "好友请求"
        
        loadFriendRequestData()
    }
    
    deinit {
        listData = nil
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: FEFriendRequestListCell.self)
        tableView.reloadData()
    }
    
    // 好友请求数据
    func loadFriendRequestData() {
        HttpClient.default.friendRequestList { (status, msg, data) in
            guard let data = data, status else {
                return
            }
            self.listData = data
        }
    }
}

// MARK: - QMUINavigationTitleViewDelegate
extension FEFriendRequestListViewController: QMUINavigationTitleViewDelegate {
    public func didChangedActive(_ active: Bool, for titleView: QMUINavigationTitleView!) {
        tableView.changeAllCellsSelectStatuse(active)
    }
}

extension FEFriendRequestListViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func setupNavigationItems() {
        super.setupNavigationItems()
        titleView?.delegate = self as QMUINavigationTitleViewDelegate
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FEFriendRequestListCell.self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if datas.count == 0 {
            self.nodataView.text = "暂无好友请求"
        } else {
            self.nodataView.text = ""
        }
        return datas.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FEFriendRequestListCell.self, for: indexPath)
        let item = datas[indexPath.row]
        cell.update(item)
        cell.clickHandler = {[weak self] (btn) in
            guard let self = self else {
                return
            }
            if btn == cell.agreeBtn {
                self.dowithFriendRequest(indexPath.row, true)
            } else {
                self.dowithFriendRequest(indexPath.row, false)
            }
        }
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func dowithFriendRequest(_ index: Int, _ agree: Bool) {
        let item = datas[index]
        
        let p = ["id": "\(item.id)", "reason": "", "status": agree ? "1" : "2"]
        
        HttpClient.default.applyFriend(param: p) {[weak self] (status, msg, _) in
            guard let self = self else {
                return
            }
            QMUITips.show(withText: msg, in: self.view)
            if status {
                if agree {
                    // 同意成功后，发送通知到好友列表界面刷新
                    NotificationCenter.default.post(name: .refreshAddressBookFriendList, object: nil)
                }
                // 刷新本地数据
                self.datas[index].status = agree ? "1" : "2"
                self.tableView.reloadData()
            }
        }
    }
}


