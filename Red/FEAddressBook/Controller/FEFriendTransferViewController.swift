//
//  FEFriendTransferViewController.swift
//  Red
//
//  Created by MAC on 2019/10/31.
//  Copyright © 2019 MAC. All rights reserved.
//
import UIKit
import QMUIKit

/// 转账界面
class FEFriendTransferViewController: FEBaseViewController {
    lazy var table: UITableView = {
        let v = UITableView()
        v.frame = view.bounds
        v.separatorStyle = .none
        v.backgroundColor = UIColor.RGBColor(248, 248, 248)
        v.delegate = self
        view.addSubview(v)
        return v
    }()
    lazy var content: UIView = {
        let v = UIView()
        table.tableHeaderView = v
        return v
    }()
    
    lazy var avatar: UIImageView = {
        let v = UIImageView()
        content.addSubview(v)
        return v
    }()
    
    lazy var name: QMUILabel = {
        let v = QMUILabel()
        v.text = ""
        v.textAlignment = .center
        v.textColor = UIColor.RGBColor(136, 136, 136)
        v.font = UIFont.systemFont(ofSize: 14)
        content.addSubview(v)
        return v
    }()
    
    lazy var tips: QMUILabel = {
        let v = QMUILabel()
        v.text = "转账金额"
        v.textColor = UIColor.RGBColor(136, 136, 136)
        v.font = UIFont.systemFont(ofSize: 14)
        content.addSubview(v)
        return v
    }()
    
    private lazy var inputMoney: QMUITextField = {
        let v = QMUITextField()
        v.backgroundColor = .white
        v.placeholder = "0"
        v.keyboardType = .numberPad
        v.textAlignment = .left
        v.font = UIFont.systemFont(ofSize: 14)
        v.tintColor = Color.red
        
        let left = QMUILabel()
        left.text = "￥"
        left.textColor = UIColor.RGBColor(136, 136, 136)
        left.textAlignment = .center
        left.font = UIFont.systemFont(ofSize: 14)
        left.size = CGSize(width: 50, height: 20)
        v.leftView = left
        
        v.leftViewMode = .always
        content.addSubview(v)
        return v
    }()
    
    private lazy var remark: QMUITextField = {
        let v = QMUITextField()
        v.backgroundColor = .white
        v.placeholder = "备注信息（50字以内）"
        v.textAlignment = .left
        v.autocapitalizationType = .none
        v.font = UIFont.systemFont(ofSize: 15)
        v.maximumTextLength = 50
        v.tintColor = Color.red
        content.addSubview(v)
        return v
    }()
    
    private lazy var submitButton: QMUIButton = {
        let v = QMUIButton("确认转账", fontSize: 18)
        v.setTitleColor(.white, for: .normal)
        v.backgroundColor = Color.red
        v.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
        content.addSubview(v)
        return v
    }()
    
    
    var friendItem: FriendItem? {
        didSet {
            self.loadUserInfo()
        }
    }
    
    public var transferHandler: ((_ item: FriendTransferItem)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "转账"

        loadUI()
        
        guard let item = friendItem else {
            return
        }
        
        avatar.setImage(with: URL(string: item.friend_avatar))
        name.text = item.friend_nickname
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        inputMoney.becomeFirstResponder()
    }
    
    func loadUserInfo() {
        // 获取最新用户数据
        UserCenter.default.getSectionInfo()
    }
}

extension FEFriendTransferViewController : UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

extension FEFriendTransferViewController {
    func loadUI() {
        table.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        content.snp.makeConstraints { (make) in
            make.size.equalTo(self.view.snp.size)
            make.left.equalTo(0)
            make.top.equalTo(0)
        }
        
        avatar.snp.makeConstraints { (make) in
            make.top.equalTo(50)
            make.size.equalTo(CGSize(width: 50, height: 50))
            make.centerX.equalTo(content.snp.centerX)
        }
        name.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 200, height: 40))
            make.top.equalTo(avatar.snp.bottom).offset(10)
            make.centerX.equalTo(avatar.snp.centerX)
        }
        tips.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 120, height: 40))
            make.left.equalTo(20)
            make.top.equalTo(name.snp.bottom).offset(20)
        }
        
        inputMoney.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(20)
            make.right.equalTo(content.snp.right).offset(-20)
            make.top.equalTo(tips.snp.bottom).offset(20)
        }
        remark.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(20)
            make.right.equalTo(content.snp.right).offset(-20)
            make.top.equalTo(inputMoney.snp.bottom).offset(20)
        }
        
        submitButton.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(20)
            make.right.equalTo(content.snp.right).offset(-20)
            make.top.equalTo(remark.snp.bottom).offset(40)
        }
    }
    
    @objc func submitButtonAction() {
        guard let input = inputMoney.text, input.count>0 else {
            QMUITips.show(withText: "请输入转账金额")
            return
        }
        
        guard let money = Double(input), let allMoney = Double(User.default.balance), money <= allMoney else {
            QMUITips.show(withText: "余额不足")
            return
        }
        
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        // 没设置密码 先跳转到设置资金密码界面
        if User.default.has_funds_password != "1" {
            let vc = FESetFundPasswdViewController()
            self.push(to: vc)
            return
        }
        
        // 先验证资金密码
        FEVerify.verifyFundPasswd(self) {[weak self] (status, pwd, text) in
            guard let self = self, status else {
                QMUITips.show(withText: text)
                return
            }
            
            let load = QMUITips.showLoading(in: self.view)
            var param = FriendTransferParam()
            param.to_id = self.friendItem?.friend_id
            param.amount = input
            param.remark = self.remark.text!
            param.fund_password = pwd
            HttpClient.default.friendTransfer(param: param, handler: {[weak self] (status2, msg, res) in
                load.hide(animated: true)
                QMUITips.show(withText: msg)
                
                if !status2 {return}
                
                // 转账接口调用成功
                if let handler = self?.transferHandler, let item = res {
                    handler(item)
                    self?.close()
                }
            })
        }
    }
}
