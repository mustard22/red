//
//  FEFriendListViewController.swift
//  Red
//
//  Created by MAC on 2019/10/28.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit
import QMUIKit

// 好友列表
class FEFriendListViewController: YYTableViewController {
    private var listData: FriendListData?
    
    private var datas: [[FriendItem]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.RGBColor(248, 248, 248)
        self.title = "通讯"
        // load friend datas
        self.loadListData()
        // register notifications
        self.registerAllNotifications()
    }
    
    deinit {
        self.removeAllNotifications()
    }
    
    override public func themChanged(form oldTheme: ThemeProtocol?, to newTheme: ThemeProtocol?) {
        super.themChanged(form: oldTheme, to: newTheme)
        
        tableView.registerCell(withClass: FEFriendListCell.self)
        tableView.reloadData()
    }
}

//MARK: notifications
extension FEFriendListViewController {
    private func registerAllNotifications() {
        // 管理员上线 下线
        NotificationCenter.default.addObserver(self, selector: #selector(adminOnlineNotification), name: SMManager.adminOnlineNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adminOfflineNotification), name: SMManager.adminOfflineNotification, object: nil)
        
        // 别人加自己
        NotificationCenter.default.addObserver(self, selector: #selector(refreshAddressBookFriendListNotification), name: .refreshAddressBookFriendList, object: nil)
        
        // 自己加别人
        NotificationCenter.default.addObserver(self, selector: #selector(refreshAddressBookFriendListNotification), name: SMManager.friendAgreeRequestNotification, object: nil)
    }
    
    private func removeAllNotifications() {
        NotificationCenter.default.removeObserver(self, name: SMManager.adminOnlineNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: SMManager.adminOfflineNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .refreshAddressBookFriendList, object: nil)
        NotificationCenter.default.removeObserver(self, name: SMManager.friendAgreeRequestNotification, object: nil)
    }
    
    @objc func adminOfflineNotification() {
        refreshFriendDatas()
    }
    
    @objc func adminOnlineNotification() {
        refreshFriendDatas()
    }
    
    @objc func refreshAddressBookFriendListNotification() {
        if self.tableView.mj_footer == nil {
            self.loadListData()
        } else {
            self.loadMoreListData()
        }
    }
}

// MARK: - 列表数据处理
extension FEFriendListViewController {
    // 获取管理员数据
    private func getAdmins() -> [FriendItem] {
        let admins = SMManager.manager.onlineAdmins.map({(admin) -> FriendItem in
            var item = FriendItem()
            item.friend_id = admin.body.id
            item.friend_nickname = admin.body.username
            item.friend_type = "5"
            item.friend_avatar = admin.body.avatar
            return item
        })
        return admins
    }
    
    // 刷新列表数据
    private func refreshFriendDatas() {
        self.datas.removeAll()
        
        // 管理员数据
        self.datas.append(getAdmins())
        
        if let items = self.listData?.data {
            // 客服数据
            let citems = items.filter({$0.friend_type == "3"})
            self.datas.append(citems)
            // 好友
            let fitems = items.filter({$0.friend_type != "3"})
            self.datas.append(fitems)
        }
        
        // 过滤空数组
        self.datas = self.datas.filter({$0.count > 0})
        self.tableView.reloadData()
    }
}

//MARK: - 网络请求
extension FEFriendListViewController {
    @objc func loadListData() {
        // load list
        self.nodataView.text = ""
        
        var p = FriendListParam()
        p.pageSize = "30"
        p.pageIndex = "1"
        HttpClient.default.getFriendList(param: p,completionHandler: { (status, msg, data) in
          
            self.mj_head.endRefreshing()
            guard status else {
                return
            }
            
            self.listData = data
            self.refreshFriendDatas()
            
            if self.datas.count == 0 {
                self.tableView.mj_footer = nil
            } else {
                if self.datas.count >= 30 {
                    self.tableView.mj_footer = self.mj_foot
                }
            }
        })
    }
    
    
    @objc func loadMoreListData() {
        var p = FriendListParam()
        p.pageSize = "30"
        p.pageIndex = "\(Int(listData!.currentPage)!+1)"

        HttpClient.default.getFriendList(param: p, completionHandler: { (status, msg, data) in
            self.mj_foot.endRefreshing()
            guard status, let datas = data.data else {
                return
            }
            self.listData = data
            
            var friends = self.datas.last!
            friends.append(contentsOf: datas)
            self.datas.removeLast()
            self.datas.append(friends)
            self.tableView.reloadData()

            if datas.count < 30 {
                self.tableView.mj_footer = nil
            } else {
                self.mj_foot.state = .idle
            }
        })
    }
}

//MARK: - override QMUITableView's methods
extension FEFriendListViewController {
    override open func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.isEditing = editing
        titleView?.isUserInteractionEnabled = editing
        titleView?.isActive = false
        navigationController?.setToolbarHidden(!editing, animated: true)
    }
    
    override open func initTableView() {
        super.initTableView()
        self.tableView.registerCell(withClass: FEFriendListCell.self)
        
        self.mj_head.setRefreshingTarget(self, refreshingAction: #selector(self.loadListData))
        
        self.mj_foot.setRefreshingTarget(self, refreshingAction: #selector(self.loadMoreListData))
        self.tableView.mj_footer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = datas.count
        self.nodataView.text = count == 0 ? "暂无数据" : ""
        
        let items = self.datas[section]
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let items = self.datas[section]
        let title = items.first!.friend_type == "3" ? "客服" : (items.first!.friend_type == "5" ? "管理员" : "好友")
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.width, height: 30))
        view.backgroundColor = self.tableView.backgroundColor
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 200, height: view.height))
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        label.text = title
        view.addSubview(label)
        return view
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.datas.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let items = self.datas[indexPath.section]
        let item = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withClass: FEFriendListCell.self, for: indexPath)
        cell.avatarImageView.setImage(with: URL(string: item.friend_avatar), placeHolder: UIImage(named: "ic_user_default"))
        cell.nameLabel.text = item.remark_name.isEmpty ?  item.friend_nickname : item.remark_name
        cell.button.isHidden = (item.friend_type != "3" && item.friend_type != "5") ? false : true
        cell.buttonActionHandler = {[weak self] in
            self?.dowithRemove(item, indexPath)
        }
        return cell
    }
    
    private func dowithRemove(_ item: FriendItem, _ path: IndexPath) {
        let alert = UIAlertController(title: nil, message: "好友管理", preferredStyle: .actionSheet)
        
        let a1 = UIAlertAction(title: "修改备注", style: .default) { (action) in
            self.setRemark(item, path)
        }
        alert.addAction(a1)
        
        let a2 = UIAlertAction(title: "清除聊天记录", style: .default) { (action) in
            let load = QMUITips.showLoading("清除中...", in: self.view)
            DispatchQueue.global().async {
                SMManager.manager.deleteConversation(cid: item.friend_id, isGroup: false)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                    load.hide(animated: true)
                    QMUITips.showInfo("聊天记录已清除", in: self.view)
                }
            }
        }
        alert.addAction(a2)
        
        let a3 = UIAlertAction(title: "删除好友", style: .default) { (action) in
            self.deleteFriend(item, path)
        }
        alert.addAction(a3)
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(cancel)
        
        self.present(alert)
    }
    
    //MARK: 备注
    private func setRemark(_ item: FriendItem, _ path: IndexPath) {
        let alert = UIAlertController(title: nil, message: "修改备注", preferredStyle: .alert)
        alert.addTextField { (tf) in
            tf.placeholder = "请输入新备注"
        }
        let a2 = UIAlertAction(title: "确定", style: .default) {[weak self] (action) in
            guard let self = self, let tfs = alert.textFields, let tf = tfs.first else {
                return
            }
            
            guard let input = tf.text, !input.isEmpty else {
                QMUITips.showInfo("输入不能为空", in: self.view)
                return
            }
            
            self.dowithSubmitRemark(item, path, input)
        }
        alert.addAction(a2)
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(cancel)
        
        self.present(alert)
    }
    
    // 修改备注接口
    private func dowithSubmitRemark(_ item: FriendItem, _ path: IndexPath, _ mark: String) {
        let p = FriendUpdateRemarkParam(friendId: item.friend_id, remarkName: mark)
        
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.updateFriendRemark(param: p) { (flag, msg) in
            load.hide(animated: true)
            if flag {
                self.datas[path.section][path.row].remark_name = mark
                self.tableView.reloadRows(at: [path], with: .none)
            }
            QMUITips.showInfo(msg, in: self.view)
        }
    }
    
    //MARK: 删除好友
    private func deleteFriend(_ item: FriendItem, _ path: IndexPath) {
        let load = QMUITips.showLoading(in: self.view)
        HttpClient.default.deleteFriend(friendId: item.friend_id) { (flag, msg) in
            load.hide(animated: true)
            QMUITips.showInfo(msg, in: self.view)
            if !flag {
                return
            }
            
            var friends = self.datas[path.section]
            friends.remove(at: path.row)
            if friends.isEmpty {
                self.datas.remove(at: path.section)
            } else {
                self.datas[path.section] = friends
            }
            
            self.tableView.reloadData()
            
            DispatchQueue.global().async {
                SMManager.manager.deleteConversation(cid: item.friend_id, isGroup: false)
                DispatchQueue.main.async {
                    
                }
            }
        }
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let items = self.datas[indexPath.section]
        let item = items[indexPath.row]
        
        var type: ChatViewType
        switch item.friend_type {
        case "3":
            type = .customer
        case "5":
            type = .admin
        default: type = .friend
        }
        let vc = FEChatFriendViewController(chat_id: item.friend_id, chat_type: type)
        vc.friend = item
        self.push(to: vc, animated: true, completion: nil)
    }
}

