//
//  FEAddressModel.swift
//  Red
//
//  Created by MAC on 2019/8/9.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation

class FEAddressModel: NSObject {
    
        let username: String
        let imageUrl: String
    
        init(username: String, imageUrl: String) {
            self.username = username
            self.imageUrl = imageUrl
           
        }
}

class FEAddressDataModel:NSObject {
    var data:[FEAddressModel]
    let title:String
    init(data:[FEAddressModel], title:String) {
        self.data = data
        self.title = title
    }
}
